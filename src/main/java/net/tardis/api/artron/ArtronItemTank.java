package net.tardis.api.artron;

import net.minecraft.nbt.FloatTag;
import net.tardis.mod.cap.items.IItemArtronTankCap;

public class ArtronItemTank implements IItemArtronTankCap {

    private final float maxArtron;
    private float artron;

    public ArtronItemTank(float maxArtron){
        this.maxArtron = maxArtron;
    }

    @Override
    public FloatTag serializeNBT() {
        return FloatTag.valueOf(this.artron);
    }

    @Override
    public void deserializeNBT(FloatTag nbt) {
        this.artron = nbt.getAsFloat();
    }

    @Override
    public float fillArtron(float artronToFill, boolean simulate) {
        final float taken = Math.min(artronToFill, this.maxArtron - this.artron);
        if(!simulate){
            this.artron += taken;
        }
        return taken;
    }

    @Override
    public float takeArtron(float artronToRecieve, boolean simulate) {
        final float pulled = Math.min(artronToRecieve, this.artron);
        if(!simulate){
            this.artron -= pulled;
        }
        return pulled;
    }

    @Override
    public float getStoredArtron() {
        return this.artron;
    }

    @Override
    public float getMaxArtron() {
        return this.maxArtron;
    }
}
