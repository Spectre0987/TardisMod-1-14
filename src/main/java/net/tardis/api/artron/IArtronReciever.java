package net.tardis.api.artron;

public interface IArtronReciever {

    /**
     * Fill this thing with artron
     * @param artronToFill - Amount to add
     * @param simulate
     * @return Amount that was actually added
     */
    float fillArtron(float artronToFill, boolean simulate);

}
