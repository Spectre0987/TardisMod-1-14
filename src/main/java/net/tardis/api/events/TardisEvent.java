package net.tardis.api.events;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraftforge.eventbus.api.Cancelable;
import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.landing.TardisLandingContext;
import net.tardis.mod.misc.tardis.TardisNotifications;


public class TardisEvent extends Event {

    private final ITardisLevel tardis;

    public TardisEvent(ITardisLevel tardis){
        this.tardis = tardis;
    }

    public ITardisLevel getTARDIS(){
        return this.tardis;
    }

    @Cancelable
    public static class TardisInitLandEvent extends TardisEvent{

        private TardisLandingContext context;

        public TardisInitLandEvent(ITardisLevel tardis, TardisLandingContext context){
            super(tardis);
            this.context = context;
        }

        public void setContext(TardisLandingContext context){
            this.context = context;
        }

        public TardisLandingContext getContext(){
            return this.context;
        }
    }

    public static class TardisSpeedCalcEvent extends TardisEvent{

        float blocksPerSec;

        public TardisSpeedCalcEvent(ITardisLevel tardis, float blocksPerSec) {
            super(tardis);
            this.blocksPerSec = blocksPerSec;
        }

        public void setNewSpeed(float blocksPerSec){
            this.blocksPerSec = blocksPerSec;
        }

        public float getSpeed(){
            return this.blocksPerSec;
        }
    }

    @Cancelable
    public static class TardisTravelDimensionCheck extends TardisEvent{

        public final ResourceKey<Level> targetWorld;

        private boolean forceAllow = false;


        public TardisTravelDimensionCheck(ITardisLevel tardis, ResourceKey<Level> targetWorld) {
            super(tardis);
            this.targetWorld = targetWorld;
        }

        /**
         * Call this to force the ability to select and land in this dimension
         */
        public void forceAllowed(){
            this.forceAllow = true;
        }

        public boolean isForceAllowed(){
            return this.forceAllow;
        }
    }

    @Cancelable
    public static class TardisAddArtronTrailEvent extends TardisEvent{

        public TardisAddArtronTrailEvent(ITardisLevel tardis) {
            super(tardis);
        }
    }

    public static class CalcRechargeEvent extends TardisEvent{

        float rechargeRate;

        public CalcRechargeEvent(ITardisLevel tardis, float rechargeRate) {
            super(tardis);
            this.rechargeRate = rechargeRate;
        }

        public void setRechargeRate(float newRate){
            this.rechargeRate = newRate;
        }

        public float getRechargeRate(){
            return this.rechargeRate;
        }
    }

    public static class TardisCreatedEvent extends TardisEvent{

        public TardisCreatedEvent(ITardisLevel tardis){
            super(tardis);
        }

    }

    public static class TardisLoadEvent extends TardisEvent{

        private final CompoundTag tag;

        public TardisLoadEvent(ITardisLevel tardis, CompoundTag tag){
            super(tardis);
            this.tag = tag;
        }

        public CompoundTag getTag(){
            return this.tag;
        }

    }

    public static class TardisSaveEvent extends TardisEvent{

        private final CompoundTag tag;

        public TardisSaveEvent(ITardisLevel tardis, CompoundTag tag){
            super(tardis);
            this.tag = tag;
        }

        public CompoundTag getTag(){
            return this.tag;
        }

    }

    /**
     * Sent whenever a notification is displayed
     */
    @Cancelable
    public static class TardisNotificationEvent extends TardisEvent{

        public final TardisNotifications.TardisNotif notif;

        public TardisNotificationEvent(ITardisLevel tardis, TardisNotifications.TardisNotif notif) {
            super(tardis);
            this.notif = notif;
        }
    }

    public static abstract class EnterEvent extends TardisEvent{

        private final Entity entity;

        public EnterEvent(ITardisLevel tardis, Entity entity){
            super(tardis);
            this.entity = entity;
        }

        public Entity getEntity(){
            return this.entity;
        }

        @Cancelable
        public static class Pre extends EnterEvent{

            public Pre(ITardisLevel tardis, Entity entity){
                super(tardis, entity);
            }

        }

        public static class Post extends EnterEvent{

            public Post(ITardisLevel tardis, Entity entity){
                super(tardis, entity);
            }

        }

    }

    public static abstract class ExitEvent extends TardisEvent{

        private final Entity entity;

        public ExitEvent(ITardisLevel tardis, Entity entity){
            super(tardis);
            this.entity = entity;
        }

        public Entity getEntity(){
            return this.entity;
        }

        @Cancelable
        public static class Pre extends ExitEvent{

            public Pre(ITardisLevel tardis, Entity entity){
                super(tardis, entity);
            }

        }

    }

}
