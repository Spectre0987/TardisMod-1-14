package net.tardis.api.events;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.ars.ARSRoom;

public class TardisARSRoomProccessEvent extends Event {

    public final ServerLevelAccessor accessor;
    public final BlockPos pos;
    public final ARSRoom room;
    public final StructureTemplate roomTemplate;

    public TardisARSRoomProccessEvent(ServerLevelAccessor level, BlockPos pos, ARSRoom arsRoom, StructureTemplate room){
        this.accessor = level;
        this.pos = pos;
        this.room = arsRoom;
        this.roomTemplate = room;
    }

}
