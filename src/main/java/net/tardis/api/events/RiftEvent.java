package net.tardis.api.events;

import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.cap.rifts.Rift;
import org.jetbrains.annotations.Nullable;

public class RiftEvent extends Event {

    final Level level;
    final ChunkPos pos;
    Rift rift;

    public RiftEvent(Level level, ChunkPos pos, Rift rift){
        this.level = level;
        this.pos = pos;
        this.rift = rift;

    }

    public Level getLevel(){
        return this.level;
    }

    public ChunkPos getPos(){
        return this.pos;
    }

    public Rift getRift(){
        return this.rift;
    }

    public static class Spawn extends RiftEvent{

        public Spawn(Level level, ChunkPos pos, Rift rift) {
            super(level, pos, rift);
        }

        public void setRift(@Nullable Rift rift){
            this.rift = rift;
        }
    }

    public static class Closed extends RiftEvent{

        public Closed(Level level, ChunkPos pos, Rift rift) {
            super(level, pos, rift);
        }
    }

}
