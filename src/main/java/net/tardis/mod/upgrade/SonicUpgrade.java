package net.tardis.mod.upgrade;

import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.item.SonicUpgradeItem;
import net.tardis.mod.item.components.UpgradeItem;
import net.tardis.mod.upgrade.types.UpgradeType;

public abstract class SonicUpgrade extends Upgrade<ISonicCapability>{

    public SonicUpgrade(UpgradeType<ISonicCapability, ?> type, ISonicCapability instance) {
        super(type, instance);
    }

    @Override
    public boolean damage(DamageSource damage) {
        return false;
    }

    @Override
    public void onBreak() {

    }

    @Override
    public void onTick() {

    }

    @Override
    public boolean canBeUsed() {
        for(int i = 0; i < this.getInstance().getUpgradeInv().getSlots(); ++i){
            final ItemStack stack = this.getInstance().getUpgradeInv().getStackInSlot(i);
            if(stack.getItem() instanceof SonicUpgradeItem<?> item && item.getUpgradeType() == this.getType()){
                return true;
            }
        }
        return false;
    }
}
