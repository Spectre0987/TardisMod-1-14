package net.tardis.mod.upgrade.types;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.tardis.BaseTardisUpgrade;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class TardisUpgradeType<T extends BaseTardisUpgrade> extends UpgradeType<ITardisLevel, T>{

    public TardisUpgradeType(Predicate<ItemStack> test, BiFunction<UpgradeType<ITardisLevel, T>, ITardisLevel, T> factory) {
        super(test, factory);
    }

    public boolean canBeUsed(ITardisLevel tardis){
        ItemStackHandler inv = tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.UPGRADES);
        for(int i = 0; i < inv.getSlots(); ++i){
            final ItemStack testingStack = inv.getStackInSlot(i);
            if(this.test.test(testingStack)){
                if(testingStack.getItem() instanceof IEngineToggleable toggle && !toggle.isActive(tardis)){
                    return false;
                }
                return true;
            }
        }
        return false;
    }
}
