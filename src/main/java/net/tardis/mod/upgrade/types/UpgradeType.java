package net.tardis.mod.upgrade.types;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.upgrade.Upgrade;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class UpgradeType<C, U extends Upgrade<C>> {

    final BiFunction<UpgradeType<C, U>, C, U> factory;
    final Predicate<ItemStack> test;

    public UpgradeType(Predicate<ItemStack> test, BiFunction<UpgradeType<C, U>, C, U> factory){
        this.factory = factory;
        this.test = test;
    }

    public U create(C instance){
        return this.factory.apply(this, instance);
    }

    public boolean isValid(ItemStack stack){
        return this.test.test(stack);
    }

}
