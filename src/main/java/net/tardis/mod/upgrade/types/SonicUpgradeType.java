package net.tardis.mod.upgrade.types;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.upgrade.Upgrade;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class SonicUpgradeType<U extends Upgrade<ISonicCapability>> extends UpgradeType<ISonicCapability, U>{
    public SonicUpgradeType(Predicate<ItemStack> test, BiFunction<UpgradeType<ISonicCapability, U>, ISonicCapability, U> factory) {
        super(test, factory);
    }

    public boolean canBeUsed(ISonicCapability cap){
        return cap.getUpgrade(this).isPresent() &&
                cap.getUpgrade(this).get().canBeUsed();
    }
}
