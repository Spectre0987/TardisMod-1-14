package net.tardis.mod.upgrade.sonic;

import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.upgrade.SonicUpgrade;
import net.tardis.mod.upgrade.types.UpgradeType;

public class EmptySonicUpgrade extends SonicUpgrade {
    public EmptySonicUpgrade(UpgradeType<ISonicCapability, ?> type, ISonicCapability instance) {
        super(type, instance);
    }
}
