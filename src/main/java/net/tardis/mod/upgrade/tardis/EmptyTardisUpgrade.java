package net.tardis.mod.upgrade.tardis;

import net.minecraft.world.damagesource.DamageSource;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.upgrade.types.UpgradeType;

/**
 * Used for upgrade that have no logic of their own
 */
public class EmptyTardisUpgrade extends BaseTardisUpgrade{
    public EmptyTardisUpgrade(UpgradeType<ITardisLevel, ?> type, ITardisLevel instance) {
        super(type, instance);
    }

    @Override
    public boolean damage(DamageSource damage) {
        return false;
    }

    @Override
    public void onBreak() {

    }

    @Override
    public void onTick() {

    }
}
