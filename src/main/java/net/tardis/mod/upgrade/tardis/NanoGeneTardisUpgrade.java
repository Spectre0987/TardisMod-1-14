package net.tardis.mod.upgrade.tardis;

import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.entities.ITardisPlayer;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.upgrade.types.UpgradeType;

public class NanoGeneTardisUpgrade extends BaseTardisUpgrade{

    public NanoGeneTardisUpgrade(UpgradeType<ITardisLevel, ?> type, ITardisLevel instance) {
        super(type, instance);
    }

    @Override
    public boolean damage(DamageSource damage) {
        return false;
    }

    @Override
    public void onBreak() {}

    @Override
    public void onTick() {

        if(!getInstance().isClient() && this.getInstance().getLevel().getGameTime() % 20 == 0){
            for(Player player : getInstance().getLevel().players()){
                player.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 40, 0, true, false));
            }
        }

    }

}
