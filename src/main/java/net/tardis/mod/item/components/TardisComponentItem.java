package net.tardis.mod.item.components;

import net.minecraft.world.item.Item;
import net.tardis.mod.creative_tabs.Tabs;

public class TardisComponentItem extends Item {

    final ComponentType componentType;

    public TardisComponentItem(Properties p_41383_, ComponentType type) {
        super(p_41383_);
        this.componentType = type;
    }

    public ComponentType getComponentType(){
        return this.componentType;
    }

    public enum ComponentType{
        UPGRADE,
        SUBSYSTEM,
        CAPACITOR;
    }
}
