package net.tardis.mod.item.components;


import net.minecraft.ChatFormatting;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.tardis.api.artron.ArtronItemTank;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.registry.ItemRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ArtronCapacitorItem extends Item implements IArtronCapacitorItem{

    public static final String TOOLTIP = Constants.Translation.makeItemTooltip(ItemRegistry.ARTRON_CAP_BASIC.getId());
    private final int maxArtron;

    public ArtronCapacitorItem(Properties pProperties, int maxArtron) {
        super(pProperties);
        this.maxArtron = maxArtron;
    }

    @Override
    public int getMaxArtron(ItemStack stack) {
        return this.maxArtron;
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced) {
        super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
        pTooltipComponents.add(Component.translatable(TOOLTIP, this.maxArtron).withStyle(Style.EMPTY.applyFormat(ChatFormatting.DARK_AQUA)));
        pStack.getCapability(Capabilities.ARTRON_TANK_ITEM).ifPresent(cap -> {
            pTooltipComponents.add(Constants.Translation.translateArtron(cap.getStoredArtron(), cap.getMaxArtron()));
        });
    }

    @Override
    public @Nullable ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundTag nbt) {
        return new GenericProvider<>(Capabilities.ARTRON_TANK_ITEM, new ArtronItemTank((float)this.maxArtron));
    }

    @Override
    public @Nullable CompoundTag getShareTag(ItemStack stack) {
        final CompoundTag tag = stack.getOrCreateTag();
        if(tag != null)
            stack.getCapability(Capabilities.ARTRON_TANK_ITEM).ifPresent(tank -> tag.put("artron_cap", tank.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundTag tag) {
        super.readShareTag(stack, tag);
        if(tag != null){
            stack.getCapability(Capabilities.ARTRON_TANK_ITEM).ifPresent(tank -> tank.deserializeNBT((FloatTag) tag.get("artron_cap")));
        }
    }
}
