package net.tardis.mod.item.components;

import net.minecraft.world.item.ItemStack;

public interface IArtronCapacitorItem {


    int getMaxArtron(ItemStack stack);

}
