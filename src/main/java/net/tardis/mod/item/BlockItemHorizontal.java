package net.tardis.mod.item;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import org.jetbrains.annotations.Nullable;

public class BlockItemHorizontal extends BlockItem {
    public BlockItemHorizontal(Block pBlock, Properties pProperties) {
        super(pBlock, pProperties);
    }

    @Nullable
    @Override
    protected BlockState getPlacementState(BlockPlaceContext pContext) {
        return super.getPlacementState(pContext).setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getPlayer().getDirection().getOpposite());
    }
}
