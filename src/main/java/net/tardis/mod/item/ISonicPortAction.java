package net.tardis.mod.item;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;

public interface ISonicPortAction {

    ItemStack onAddedToPort(ItemStack stack, ITardisLevel tardis);

}
