package net.tardis.mod.item.misc;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Holder;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.misc.ToolType;

import java.util.Optional;

public record TardisTool(ToolType type, Item item, float scrapChance, ParticleType<?> part, Optional<Holder<SoundEvent>> toolSound, int toolSoundLoopTime) {

    public static final Codec<TardisTool> CODEC = RecordCodecBuilder.create(instance ->
        instance.group(
                ToolType.CODEC.fieldOf("tool").forGetter(TardisTool::type),
                ForgeRegistries.ITEMS.getCodec().fieldOf("item").forGetter(TardisTool::item),
                Codec.FLOAT.fieldOf("scrap_chance").forGetter(TardisTool::scrapChance),
                ForgeRegistries.PARTICLE_TYPES.getCodec().fieldOf("particle").forGetter(TardisTool::part),
                SoundEvent.CODEC.optionalFieldOf("sound").forGetter(TardisTool::toolSound),
                Codec.INT.fieldOf("sound_loop_time").forGetter(TardisTool::toolSoundLoopTime)
        ).apply(instance, TardisTool::new)
    );

}
