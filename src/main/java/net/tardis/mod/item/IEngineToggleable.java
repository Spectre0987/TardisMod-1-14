package net.tardis.mod.item;

import net.tardis.mod.cap.level.ITardisLevel;

public interface IEngineToggleable {

    void onToggle(ITardisLevel tardis, boolean active);
    boolean isActive(ITardisLevel tardis);

}
