package net.tardis.mod.item.tools;

import net.minecraft.client.resources.sounds.Sound;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.misc.ToolType;
import org.apache.logging.log4j.Level;

public class PistonHammerItem extends Item {
    public PistonHammerItem(Properties pProperties) {
        super(pProperties);
    }

    public static PistonHammerItem create(){
        return new PistonHammerItem(
                BasicProps.Item.ONE.get().durability(300)
        );
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
        if(!entity.level.isClientSide()){
            entity.level.playSound(null, entity.getOnPos(), SoundEvents.PISTON_EXTEND, SoundSource.PLAYERS, 1.0F, 1.0F);
        }
        return super.onEntitySwing(stack, entity);
    }

    @Override
    public boolean hurtEnemy(ItemStack pStack, LivingEntity pTarget, LivingEntity pAttacker) {
        pTarget.setDeltaMovement(pTarget.getDeltaMovement().add(pAttacker.getLookAngle().scale(3)));
        return super.hurtEnemy(pStack, pTarget, pAttacker);
    }
}
