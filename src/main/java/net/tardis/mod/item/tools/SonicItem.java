package net.tardis.mod.item.tools;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.cap.items.SonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.client.item.SpecialItemData;
import net.tardis.mod.item.MultipleCapabilityProvider;
import net.tardis.mod.misc.IAttunable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class SonicItem extends Item implements IAttunable {
    public SonicItem(Properties pProperties) {
        super(pProperties);
    }

    public static SonicItem create(){
        return new SonicItem(BasicProps.Item.ONE.get()
                .durability(1000)
                .setNoRepair()
        );
    }

    @Override
    public void initializeClient(Consumer<IClientItemExtensions> consumer) {
        consumer.accept(new SpecialItemData());
    }

    @Override
    public @Nullable ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundTag nbt) {
        return new MultipleCapabilityProvider<>(new SonicCapability(stack))
                .add(Capabilities.SONIC)
                .add(ForgeCapabilities.ENERGY);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level pLevel, Player pPlayer, InteractionHand pUsedHand) {
        final ItemStack stack = pPlayer.getItemInHand(pUsedHand);
        stack.getCapability(Capabilities.SONIC).ifPresent(sonic -> {
            if(pPlayer.isShiftKeyDown()){
                if(pPlayer instanceof ServerPlayer player){
                    Network.sendTo(player, new OpenGuiDataMessage(GuiDatas.SONIC.create().withHand(pUsedHand,
                            ItemFunctionRegistry.getAllFor(SonicFunctionType.class).stream()
                                    .filter(type -> sonic.getFunctionByType(type).isPresent() &&
                                            sonic.getFunctionByType(type).get().shouldDisplay(sonic))
                                    .toList()
                    )));
                }
            }
            else {
                sonic.onUse(pLevel, pPlayer, pUsedHand);
                pPlayer.startUsingItem(pUsedHand);
            }
        });
        return super.use(pLevel, pPlayer, pUsedHand);
    }

    @Override
    public InteractionResult useOn(UseOnContext pContext) {
        pContext.getItemInHand().getCapability(Capabilities.SONIC).ifPresent(sonic -> {
            sonic.useOn(pContext);
            pContext.getPlayer().startUsingItem(pContext.getHand());
        });
        return InteractionResult.sidedSuccess(pContext.getLevel().isClientSide());
    }

    @Override
    public ItemStack onAttuned(ITardisLevel tardis, ItemStack stack) {
        stack.getCapability(Capabilities.SONIC).ifPresent(sonic -> {
            sonic.setBoundTARDIS(tardis.getId());
        });
        return stack;
    }

    @Override
    public int getTicksToAttune() {
        return 10 * 20;
    }

    @Override
    public Optional<ResourceKey<Level>> getAttunedTardis(ItemStack stack) {
        return stack.getCapability(Capabilities.SONIC).map(son -> son.getBoundTARDIS()).orElse(Optional.empty());
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced) {
        super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
        Capabilities.getCap(Capabilities.SONIC, pStack).ifPresent(sonic -> {
            pTooltipComponents.add(Constants.Translation.translatePower(sonic.getEnergyStored(), sonic.getMaxEnergyStored()));
            sonic.getBoundTARDIS().ifPresent(tardis -> {
                pTooltipComponents.add(Constants.Translation.makeTardisTranslation(tardis));
            });
        });
    }

    @Override
    public int getUseDuration(ItemStack pStack) {
        return 72000;
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return oldStack.getItem() != newStack.getItem();
    }

    @Override
    public @Nullable CompoundTag getShareTag(ItemStack stack) {
        final CompoundTag tag = super.getShareTag(stack);
        if(tag != null)
            stack.getCapability(Capabilities.SONIC).ifPresent(sonic -> tag.put("sonic_tag", sonic.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundTag nbt) {
        super.readShareTag(stack, nbt);
        if(nbt != null && nbt.contains("sonic_tag")){
            stack.getCapability(Capabilities.SONIC).ifPresent(sonic -> sonic.deserializeNBT(nbt.getCompound("sonic_tag")));
        }
    }


}
