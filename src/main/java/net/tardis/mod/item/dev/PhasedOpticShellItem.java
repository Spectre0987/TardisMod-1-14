package net.tardis.mod.item.dev;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.ItemBasedSteering;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.client.gui.datas.PhasedOpticShellItemGuiData;
import net.tardis.mod.config.Config;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

import java.util.Optional;

public class PhasedOpticShellItem extends Item {
    public PhasedOpticShellItem() {
        super(BasicProps.Item.ONE.get());
    }

    public static void setPosition(ItemStack stack, BlockPos pos){
        final CompoundTag tag = stack.getOrCreateTag();
        if(tag.contains("first_pos")){
            tag.putLong("second_pos", pos.asLong());
        }
        else tag.putLong("first_pos", pos.asLong());
    }

    public static Optional<BlockPos> getPosition(ItemStack stack, boolean first){
        if(stack.getTag() == null)
            return Optional.empty();

        final CompoundTag tag = stack.getTag();

        if(first && tag.contains("first_pos")){
            return Optional.of(BlockPos.of(tag.getLong("first_pos")));
        }
        if(!first && tag.contains("second_pos")){
            return Optional.of(BlockPos.of(tag.getLong("second_pos")));
        }
        return Optional.empty();
    }

    public static boolean hasBoth(ItemStack stack){
        return stack.hasTag() && stack.getTag().contains("first_pos") && stack.getTag().contains("second_pos");
    }

    @Override
    public InteractionResult useOn(UseOnContext pContext) {

        if(pContext.getPlayer().isShiftKeyDown()){
            if(!pContext.getLevel().isClientSide()){
                PhasedOpticShellItemGuiData.createFromItem(pContext.getHand(), pContext.getItemInHand(), pContext.getClickedPos()).ifPresent(data -> {
                    Network.sendTo((ServerPlayer) pContext.getPlayer(), new OpenGuiDataMessage(data));
                });
            }
        }
        else{
            setPosition(pContext.getItemInHand(), pContext.getClickedPos().relative(pContext.getClickedFace()));
        }

        return InteractionResult.sidedSuccess(pContext.getLevel().isClientSide);
    }
}
