package net.tardis.mod.item;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.tardis.mod.client.item.SpecialItemData;

import java.util.function.Consumer;

public class SpecialBlockItem extends BlockItem {

    public SpecialBlockItem(Block pBlock, Properties pProperties) {
        super(pBlock, pProperties);
    }

    @Override
    public void initializeClient(Consumer<IClientItemExtensions> consumer) {
        consumer.accept(new SpecialItemData());
    }
}
