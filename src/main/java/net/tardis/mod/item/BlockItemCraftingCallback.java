package net.tardis.mod.item;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.tardis.mod.client.item.SpecialItemData;
import org.apache.commons.lang3.function.TriFunction;
import org.apache.logging.log4j.util.TriConsumer;

import java.util.function.Consumer;

public class BlockItemCraftingCallback extends BlockItem {

    TriConsumer<ItemStack, Level, Player> craftingCallback;
    boolean isSpecial = false;

    public BlockItemCraftingCallback(Block pBlock, Properties pProperties) {
        super(pBlock, pProperties);
    }

    public BlockItemCraftingCallback withCallback(TriConsumer<ItemStack, Level, Player> callback){
        this.craftingCallback = callback;
        return this;
    }

    public BlockItemCraftingCallback setSpecial(){
        this.isSpecial = true;
        return this;
    }

    @Override
    public void onCraftedBy(ItemStack pStack, Level pLevel, Player pPlayer) {
        super.onCraftedBy(pStack, pLevel, pPlayer);
        if(craftingCallback != null)
            craftingCallback.accept(pStack, pLevel, pPlayer);
    }

    @Override
    public void initializeClient(Consumer<IClientItemExtensions> consumer) {
        super.initializeClient(consumer);
        consumer.accept(new SpecialItemData());
    }
}
