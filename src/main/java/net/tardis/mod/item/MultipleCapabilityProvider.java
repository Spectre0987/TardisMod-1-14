package net.tardis.mod.item;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

/**
 * For one provider that reads as multiple
 */
public class MultipleCapabilityProvider<T extends INBTSerializable<CompoundTag>> implements ICapabilitySerializable<CompoundTag> {

    final T instance;
    final LazyOptional<T> holder;
    final List<Capability<?>> caps = new ArrayList<>();

    public MultipleCapabilityProvider(T instance){
        this.instance = instance;
        this.holder = LazyOptional.of(() -> this.instance);
    }

    public <I> MultipleCapabilityProvider<T> add(Capability<I> cap){
        this.caps.add(cap);
        return this;
    }


    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(this.caps.contains(cap)){
            return this.holder.cast();
        }
        return LazyOptional.empty();
    }

    @Override
    public CompoundTag serializeNBT() {
        return this.instance.serializeNBT();
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        if(nbt != null)
            this.instance.deserializeNBT(nbt);
    }
}
