package net.tardis.mod.item;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class DataCrystalItem extends Item implements ISonicPortAction{


    public DataCrystalItem(Properties pProperties) {
        super(pProperties);
    }

    public static DataCrystalItem create(){
        return new DataCrystalItem(
                BasicProps.Item.ONE.get()
        );
    }

    @Override
    public ItemStack onAddedToPort(ItemStack stack, ITardisLevel tardis) {
        getUnlock(stack).ifPresent(unlock -> {
            tardis.getUnlockHandler().unlock(unlock);
            setUnlockSchmeatic(stack, null);
        });
        return stack;
    }

    public static void setUnlockSchmeatic(ItemStack stack, @Nullable ResourceKey<?> key){
        Helper.writeResourceKey(stack.getOrCreateTag(), "unlock", key);
    }

    public static Optional<ResourceKey<?>> getUnlock(ItemStack stack){
        return Helper.readResourceKey(stack.getTag(), "unlock");
    }

}
