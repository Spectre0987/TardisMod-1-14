package net.tardis.mod.item;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.IAttunable;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class KeyItem extends Item implements IAttunable {

    public KeyItem(Properties pProperties) {
        super(pProperties);
    }

    public static KeyItem create(){
        return new KeyItem(BasicProps.Item.ONE.get());
    }

    public static void setKey(ItemStack stack, ResourceKey<Level> tardisKey){
        stack.getOrCreateTag().putString("tardis_key", tardisKey.location().toString());
    }

    public static Optional<ResourceKey<Level>> getKeyData(ItemStack stack){
        if(stack.getTag() != null && stack.getTag().contains("tardis_key")){
            return Optional.of(ResourceKey.create(Registries.DIMENSION, new ResourceLocation(stack.getTag().getString("tardis_key"))));
        }
        return Optional.empty();
    }

    @Override
    public ItemStack onAttuned(ITardisLevel tardis, ItemStack stack) {
        setKey(stack, tardis.getLevel().dimension());
        return stack;
    }

    @Override
    public int getTicksToAttune() {
        return 200;
    }

    @Override
    public Optional<ResourceKey<Level>> getAttunedTardis(ItemStack stack) {
        return KeyItem.getKeyData(stack);
    }
}
