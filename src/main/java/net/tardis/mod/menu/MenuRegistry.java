package net.tardis.mod.menu;

import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.menu.quantiscope.CraftingQuantiscopeMenu;
import net.tardis.mod.menu.quantiscope.SonicQuantiscopeMenu;
import net.tardis.mod.menu.quantiscope.SonicUpgradeQuantiscopeMenu;

public class MenuRegistry {

    public static final DeferredRegister<MenuType<?>> MENUS = DeferredRegister.create(ForgeRegistries.MENU_TYPES, Tardis.MODID);

    public static final RegistryObject<MenuType<DraftingTableMenu>> DRAFTING_TABLE = MENUS.register("drafting_table", () -> create(DraftingTableMenu::new));
    public static final RegistryObject<MenuType<EngineMenu>> TARDIS_ENGINE = MENUS.register("engine", () -> create(EngineMenu::new));
    public static final RegistryObject<MenuType<AlembicMenu>> ALEMBIC = MENUS.register("alembic", () -> create(AlembicMenu::new));
    public static final RegistryObject<MenuType<ScoopVaultMenu>> SCOOP_VAULT = MENUS.register("scoop_vault", () -> create(ScoopVaultMenu::new));
    public static final RegistryObject<MenuType<CraftingQuantiscopeMenu>> QUANTISCOPE_WELD = MENUS.register("quantiscope/weld", () -> create(CraftingQuantiscopeMenu::new));
    public static final RegistryObject<MenuType<SonicQuantiscopeMenu>> QUANTISCOPE_SONIC = MENUS.register("quantiscope/sonic", () -> create(SonicQuantiscopeMenu::new));
    public static final RegistryObject<MenuType<SonicUpgradeQuantiscopeMenu>> QUANTISCOPE_SONIC_UPGRADE = MENUS.register("quantiscope/sonic_upgrade", () -> create(SonicUpgradeQuantiscopeMenu::new));
    public static final RegistryObject<MenuType<ARSMenu>> ARS = MENUS.register("ars", () -> create(ARSMenu::new));


    public static <T extends AbstractContainerMenu> MenuType<T> create(IContainerFactory<T> factory){
        return new MenuType<T>(factory, FeatureFlags.VANILLA_SET);
    }

}
