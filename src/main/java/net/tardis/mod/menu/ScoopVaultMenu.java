package net.tardis.mod.menu;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.InventoryHelper;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ScoopVaultMenu extends AbstractContainerMenu {

    ItemStackHandler scoopInv;

    protected ScoopVaultMenu(@Nullable MenuType<?> pMenuType, int pContainerId) {
        super(pMenuType, pContainerId);
    }

    //Server
    public ScoopVaultMenu(int window, Inventory inv, ITardisLevel tardis){
        this(MenuRegistry.SCOOP_VAULT.get(), window);
        this.setup(inv, tardis);
    }

    //Client
    public ScoopVaultMenu(int windowId, Inventory inv, FriendlyByteBuf data){
        this(MenuRegistry.SCOOP_VAULT.get(), windowId);
        Capabilities.getCap(Capabilities.TARDIS, inv.player.level).ifPresent(tardis -> {
            setup(inv, tardis);
        });
    }

    public void setup(Inventory inv, ITardisLevel tardis){

        final int startX = 8,
                startY = 18;
        int slotId = 0;

        this.scoopInv = tardis.getInteriorManager().getTemporalScoopInventory();

        for(int y = 0; y < 6; ++y){
            for(int x = 0; x < 9; ++x){
                this.addSlot(new FilteredSlotItemHandler(this.scoopInv, item -> false, slotId, startX + x * 18, startY + y * 18));
                slotId++;
            }
        }

        for(Slot s : GuiHelper.getPlayerSlots(inv, 8, 140)){
            this.addSlot(s);
        }
    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {
        Slot slot = this.getSlot(pIndex);
        if(slot instanceof SlotItemHandler itemSlot && itemSlot.getItemHandler() == this.scoopInv){
            pPlayer.getInventory().add(this.scoopInv.extractItem(slot.getContainerSlot(), 64, false));
        }
        return ItemStack.EMPTY;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
