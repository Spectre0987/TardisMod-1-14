package net.tardis.mod.menu;

import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import org.jetbrains.annotations.Nullable;

public record DefaultMenuProvider<T extends AbstractContainerMenu>(IMenuProvider<T> provider) implements MenuProvider {


    @Override
    public Component getDisplayName() {
        return Component.empty();
    }

    @Nullable
    @Override
    public T createMenu(int pContainerId, Inventory pPlayerInventory, Player pPlayer) {
        return this.provider.create(pContainerId, pPlayerInventory);
    }

    public static interface IMenuProvider<T extends AbstractContainerMenu>{
        T create(int id, Inventory inv);
    }
}
