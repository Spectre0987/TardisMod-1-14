package net.tardis.mod.menu;

import it.unimi.dsi.fastutil.ints.Int2BooleanArrayMap;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerSynchronizer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.item.components.ArtronCapacitorItem;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.item.components.UpgradeItem;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;
import net.tardis.mod.misc.IAttunable;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateTardisEngineContents;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class EngineMenu extends AbstractContainerMenu {

    public Direction side = Direction.NORTH;
    private ITardisLevel tardis;
    private Optional<Player> player = Optional.empty();
    private Int2BooleanArrayMap activeSlots = new Int2BooleanArrayMap();

    public static final SubsystemType<?>[] TYPE_ORDER = {
            SubsystemRegistry.FLIGHT_TYPE.get(),
            SubsystemRegistry.NAV_COM.get(),
            SubsystemRegistry.CHAMELEON_TYPE.get(),
            SubsystemRegistry.TEMPORAL_GRACE.get(),
            SubsystemRegistry.FLUID_LINK_TYPE.get(),
            SubsystemRegistry.STABILIZERS.get(),
            SubsystemRegistry.ANTENNA.get(),
            SubsystemRegistry.SHIELD.get()
    };

    public final ContainerData ATTUNEMENT_SIDE_CONTAINER = new ContainerData() {
        @Override
        public int get(int pIndex) {
            return tardis.getEngine().attunementTicksFlown;
        }

        @Override
        public void set(int pIndex, int pValue) {
            tardis.getEngine().attunementTicksFlown = pValue;
        }

        @Override
        public int getCount() {
            return 1;
        }
    };


    protected EngineMenu(int pContainerId, Inventory inv) {
        super(MenuRegistry.TARDIS_ENGINE.get(), pContainerId);
    }

    public EngineMenu(int id, Inventory inv, ITardisLevel tardis, Direction dir){
        this(id, inv);
        this.init(inv, inv.player.level, tardis, dir);
    }

    //Client constructor
    public EngineMenu(int id, Inventory inv, FriendlyByteBuf buf){
        this(id, inv);

        this.activeSlots.clear();

        final Direction side = buf.readEnum(Direction.class);
        final int toggleSize = buf.readInt();
        for(int i = 0; i < toggleSize; ++i){
            this.activeSlots.put(buf.readInt(), buf.readBoolean());
        }
        Capabilities.getCap(Capabilities.TARDIS, inv.player.getLevel()).ifPresent(tardis -> {
            this.init(inv, inv.player.level, tardis, side);
        });
    }

    public void init(Inventory inv, Level level, ITardisLevel tardis, Direction dir){
        this.player = Optional.of(inv.player);
        this.side = dir;
        this.tardis = tardis;
        final ItemStackHandler items = tardis.getEngine().getInventoryFor(dir);
        this.setupEngineSlots(TardisEngine.EngineSide.getForDirection(dir).orElse(TardisEngine.EngineSide.SUBSYSTEMS), items);

        if(TardisEngine.EngineSide.CHARGING.getSide() == dir){
            this.addDataSlots(this.ATTUNEMENT_SIDE_CONTAINER);
        }


        //Player inv

        for(Slot s : GuiHelper.getPlayerSlots(inv, 8, 84)){
            this.addSlot(s);
        }
    }

    public void setupEngineSlots(TardisEngine.EngineSide side, ItemStackHandler items){
        if(side == TardisEngine.EngineSide.SUBSYSTEMS){
            int id = 0;
            for(int y = 0; y < 2; ++y){
                for(int x = 0; x < 4; ++x){


                    //Create the filter based on system slot
                    final int currId = id;
                    final Predicate<ItemStack> filter =
                            TYPE_ORDER.length > currId ?
                                    stack -> stack.isEmpty() || (stack.getItem() instanceof SubsystemItem sysItem && sysItem.getSubsytemType() == TYPE_ORDER[currId]) :
                                    stack -> true;

                    this.addSlot(new FilteredSlotItemHandler(items, filter, id, 18 + (int)Math.floor(x * 42.0), 18 + (y * 21)));
                    ++id;
                }
            }
        }
        else if (side == TardisEngine.EngineSide.UPGRADES){
            int slotId = 0;
            for(int y = 0; y < 2; ++y){
                for(int x = 0; x < 5; ++x){
                    this.addSlot(new FilteredSlotItemHandler(items, i -> i.getItem() instanceof UpgradeItem<?,?>, slotId, 16 + (int)Math.ceil(x * 32.5), 18 + (y * 21)));
                    ++slotId;
                }
            }
        }
        else if(side == TardisEngine.EngineSide.CHARGING){
            for(int i = 0; i < 4; ++i){
                this.addSlot(new FilteredSlotItemHandler(items, stack -> stack.getCapability(ForgeCapabilities.ENERGY).isPresent(), i, 32 + (i / 2) * 22, 16 + (i % 2) * 22));
            }
            this.addSlot(new FilteredSlotItemHandler(items, stack -> stack.getItem() instanceof IAttunable, 4, 119, 27));
        }
        else if(side == TardisEngine.EngineSide.CAPACITORS){
            int id = 0;

            final Predicate<ItemStack> capTest = stack -> stack.getItem() instanceof ArtronCapacitorItem;
            final Predicate<ItemStack> crystalTest = stack -> ItemArtronValueReloader.ArtronValue.getArtronValue(stack) > 0;

            for(int y = 0; y < 2; ++y){
                for(int x = 0; x < 5; ++x){
                    if(id >= 9)
                        return;
                    this.addSlot(new FilteredSlotItemHandler(items, id == 4 ? crystalTest : capTest, id, 19 + (x * 31), 18 + y * 21));
                    ++id;
                }
            }
        }
    }

    public Direction getSide(){
        return this.side;
    }

    public int getAttunementTicks(){
        return this.ATTUNEMENT_SIDE_CONTAINER.get(0);
    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {
        final Slot slot = getSlot(pIndex);
        //Engine to player
        if(slot instanceof SlotItemHandler itemSlot){
            if(pPlayer.getInventory().add(itemSlot.getItem())){
                itemSlot.set(ItemStack.EMPTY);
                return ItemStack.EMPTY;
            }
        }
        //Player to Engine
        else{
            for(Slot engineSlot : this.slots){
                if(engineSlot instanceof SlotItemHandler e){
                    ItemStack leftOver = e.safeInsert(slot.getItem());
                    slot.set(leftOver);
                }
            }
        }

        return ItemStack.EMPTY;
    }

    public static Int2BooleanArrayMap buildToggleList(Level level, Direction side){
        Int2BooleanArrayMap map = new Int2BooleanArrayMap();
        Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
            final ItemStackHandler inv = tardis.getEngine().getInventoryFor(side);
            for(int i = 0; i < inv.getSlots(); ++i){
                if(inv.getStackInSlot(i).getItem() instanceof IEngineToggleable toggle){
                    map.put(i, toggle.isActive(tardis));
                }
            }
        });
        return map;
    }

    public boolean isSlotActive(int slotId){
        if(this.activeSlots.containsKey(slotId)){
            return this.activeSlots.get(slotId);
        }
        return false;
    }


    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
