package net.tardis.mod.menu.quantiscope;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.Slot;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.SonicQuantiscopeSetting;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.menu.MenuRegistry;

public class SonicQuantiscopeMenu extends BaseQuantiscopeMenu<SonicQuantiscopeSetting> {
    protected SonicQuantiscopeMenu(int pContainerId) {
        super(MenuRegistry.QUANTISCOPE_SONIC, pContainerId);
    }

    //Server
    public SonicQuantiscopeMenu(int id, Inventory inv, SonicQuantiscopeSetting setting){
        this(id);
        this.setup(inv, setting);
    }

    //Client
    public SonicQuantiscopeMenu(int id, Inventory inv, FriendlyByteBuf buf){
        this(id);
        if(inv.player.level.getBlockEntity(buf.readBlockPos()) instanceof QuantiscopeTile tile){
            if(tile.getCurrentSetting() instanceof SonicQuantiscopeSetting sonic){
                this.setup(inv, sonic);
            }
        }
    }

    @Override
    public void setup(Inventory player, SonicQuantiscopeSetting setting) {
        this.setting = setting;

        ItemStackHandler inv = setting.getInventory().get();

        this.addSlot(new SlotItemHandler(inv, 0, 42, 47));

        for(Slot slot : GuiHelper.getPlayerSlots(player, 8, 84)){
            this.addSlot(slot);
        }
    }
}
