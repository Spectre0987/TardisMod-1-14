package net.tardis.mod.menu.quantiscope;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.Slot;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.machines.quantiscope_settings.SonicUpgradeQuantiscopeSetting;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.item.SonicUpgradeItem;
import net.tardis.mod.item.tools.SonicItem;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;

public class SonicUpgradeQuantiscopeMenu extends BaseQuantiscopeMenu<SonicUpgradeQuantiscopeSetting>{
    protected SonicUpgradeQuantiscopeMenu(int pContainerId) {
        super(MenuRegistry.QUANTISCOPE_SONIC_UPGRADE, pContainerId);
    }

    //Server
    public SonicUpgradeQuantiscopeMenu(int id, Inventory inv, SonicUpgradeQuantiscopeSetting setting){
        this(id);
        this.setup(inv, setting);
    }

    //Client
    public SonicUpgradeQuantiscopeMenu(int id, Inventory inv, FriendlyByteBuf buf){
        this(id);
        getSettingFrom(inv, buf).ifPresent(set -> this.setup(inv, (SonicUpgradeQuantiscopeSetting) set));
    }

    @Override
    public void setup(Inventory player, SonicUpgradeQuantiscopeSetting setting) {
        this.setting = setting;

        final ItemStackHandler inv = setting.getInventory().get();

        for(int i = 0; i < 6; ++i){
            this.addSlot(new FilteredSlotItemHandler(inv, stack -> stack.getItem()instanceof SonicUpgradeItem<?>, i, 30 + (i * 20), 15));
        }
        this.addSlot(new FilteredSlotItemHandler(inv, stack -> stack.getItem() instanceof SonicItem,6, 42, 47));

        for(Slot s : GuiHelper.getPlayerSlots(player, 8, 84)){
            this.addSlot(s);
        }
    }
}
