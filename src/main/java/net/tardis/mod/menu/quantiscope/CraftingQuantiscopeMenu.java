package net.tardis.mod.menu.quantiscope;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.CraftQuantiscopeSetting;
import net.tardis.mod.blockentities.machines.quantiscope_settings.QuantiscopeSetting;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.menu.MenuRegistry;

import java.util.Optional;

public class CraftingQuantiscopeMenu extends BaseQuantiscopeMenu<CraftQuantiscopeSetting>{

    private CraftingQuantiscopeMenu(int id){
        super(MenuRegistry.QUANTISCOPE_WELD, id);
    }

    //Server
    public CraftingQuantiscopeMenu(int id, Inventory inv, CraftQuantiscopeSetting setting){
        this(id);
        this.setup(inv, setting);
    }

    public CraftingQuantiscopeMenu(int id, Inventory player, FriendlyByteBuf buf){
        this(id);
        if(player.player.level.getBlockEntity(buf.readBlockPos()) instanceof QuantiscopeTile tile){
            this.setup(player, (CraftQuantiscopeSetting) tile.getCurrentSetting());
        }
    }

    @Override
    public void setup(Inventory player, CraftQuantiscopeSetting setting) {
        this.setting = setting;
        this.addDataSlots(setting.getContainerData());

        final ItemStackHandler tile = setting.getInventory().get();

        this.addSlot(new SlotItemHandler(tile, 0, 29, 9));
        this.addSlot(new SlotItemHandler(tile, 1, 50, 9));
        this.addSlot(new SlotItemHandler(tile, 2, 16, 29));
        this.addSlot(new SlotItemHandler(tile, 3, 29, 49));
        this.addSlot(new SlotItemHandler(tile, 4, 50, 49));
        this.addSlot(new SlotItemHandler(tile, 5, 64, 29));
        this.addSlot(new SlotItemHandler(tile, 6, 120, 29));

        for(Slot slot : GuiHelper.getPlayerSlots(player, 8, 84)){
            this.addSlot(slot);
        }
    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {

        final Slot slot = this.getSlot(pIndex);

        if(slot.container == pPlayer.getInventory()){
            for(Slot newSlot : this.slots){
                if(newSlot instanceof SlotItemHandler s){
                    if(s.mayPlace(slot.getItem())){
                        slot.set(s.safeInsert(slot.getItem()));
                    }
                }
            }
        }
        //From machine to player
        else {
            if(pPlayer.getInventory().add(slot.getItem())){
                slot.set(ItemStack.EMPTY);
            }
        }

        return ItemStack.EMPTY;
    }
}
