package net.tardis.mod.menu;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.*;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;
import org.jetbrains.annotations.Nullable;

public class AlembicMenu extends AbstractContainerMenu {

    private AlembicBlockEntity alembic;

    public AlembicMenu(@Nullable MenuType<?> pMenuType, int pContainerId) {
        super(pMenuType, pContainerId);
    }

    //Client constructor
    public AlembicMenu(int id, Inventory playerInv, FriendlyByteBuf buf){
        this(MenuRegistry.ALEMBIC.get(), id);
        if(playerInv.player.level.getBlockEntity(buf.readBlockPos()) instanceof AlembicBlockEntity alembic){
            setupSlots(alembic, playerInv);
        }
    }

    //Server
    public AlembicMenu(int id, Inventory inv, AlembicBlockEntity alembic){
        this(MenuRegistry.ALEMBIC.get(), id);
        this.setupSlots(alembic, inv);
    }

    public void setupSlots(AlembicBlockEntity alembic, Inventory inv){
        this.alembic = alembic;

        this.addDataSlots(alembic.containerData);

        //Water fillup
        this.addSlot(new FilteredSlotItemHandler(alembic.inventory, item -> item.getCapability(ForgeCapabilities.FLUID_HANDLER_ITEM).isPresent(), 0, 25, 11));
        this.addSlot(new FilteredSlotItemHandler(alembic.inventory, item -> false, 1, 25, 47));

        //Ingredient
        this.addSlot(new SlotItemHandler(alembic.inventory, 2, 62, 11));
        //Fuel
        this.addSlot(new FilteredSlotItemHandler(alembic.inventory, item -> ForgeHooks.getBurnTime(item, RecipeType.SMELTING) > 0, 3, 62, 47));
        //Fluid Result Slots
        this.addSlot(new SlotItemHandler(alembic.inventory, 4, 140, 11));
        this.addSlot(new FilteredSlotItemHandler(alembic.inventory, item -> false, 5, 140, 47));

        GuiHelper.getPlayerSlots(inv, 8, 84).forEach(this::addSlot);
    }
    public AlembicBlockEntity getAlembic(){
        return this.alembic;
    }

    @Override
    public ItemStack quickMoveStack(Player player, int slotId) {
        final Slot slot = this.getSlot(slotId);
        //From Player to alembic
        if(slot.container == player.getInventory()){
            //Attempt fuel first
            final ItemStack fuelStack = slot.getItem();
            ItemStack leftOver = this.alembic.inventory.insertItem(3, slot.getItem(), false);
            if(leftOver != fuelStack){
                slot.set(leftOver);
                return ItemStack.EMPTY;
            }

            for(Slot s : this.slots){
                if(s instanceof SlotItemHandler itemSlot){
                    if(itemSlot.mayPlace(slot.getItem())){
                        if(itemSlot.getItem().isEmpty() || itemSlot.getItem().getCount() < itemSlot.getItem().getMaxStackSize()){
                            slot.set(itemSlot.safeInsert(slot.getItem()));

                            return ItemStack.EMPTY;
                        }
                    }
                }
            }

        }
        //From Alembic to player
        if(!slot.getItem().isEmpty()){
            if(player.getInventory().add(slot.getItem())){
                slot.set(ItemStack.EMPTY);
                return ItemStack.EMPTY;
            }

        }

        return ItemStack.EMPTY;
    }

    @Override
    public boolean stillValid(Player player) {
        return stillValid(ContainerLevelAccess.create(player.level, this.alembic.getBlockPos()), player, BlockRegistry.ALEMBIC.get()) ||
                stillValid(ContainerLevelAccess.create(player.level, this.alembic.getBlockPos()), player, BlockRegistry.ALEMBIC_STILL.get());
    }
}
