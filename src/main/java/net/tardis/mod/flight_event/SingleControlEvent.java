package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;

import java.util.function.Supplier;

public abstract class SingleControlEvent extends FlightEvent{

    final Supplier<? extends ControlType<?>> controlType;
    final boolean overrrides;

    public SingleControlEvent(FlightEventType type, ITardisLevel level, Supplier<? extends ControlType<?>> controlType, boolean overrides) {
        super(type, level);
        this.controlType = controlType;
        this.overrrides = overrides;
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {
        if(control.getType() == this.controlType.get()){
            this.complete();
        }
        return control.getType() == this.controlType.get() && overrrides;
    }
}
