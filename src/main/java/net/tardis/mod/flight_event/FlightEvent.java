package net.tardis.mod.flight_event;

import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageSources;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.network.packets.TardisUpdateMessage;
import net.tardis.mod.network.packets.tardis.TardisFlightEventData;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.sound.SoundRegistry;

import java.lang.ref.Reference;
import java.util.function.Predicate;

public abstract class FlightEvent extends TypeHolder<FlightEventType> {

    public final ITardisLevel tardis;
    private boolean isComplete = false;
    private Predicate<ControlData<?>> controlsToOverride = data -> false;

    public FlightEvent(FlightEventType type, ITardisLevel level){
        super(type);
        this.tardis = level;
    }

    /**
     *
     * @param control - the control that was hit
     * @return true if this event overrides the default control action
     */
    public abstract boolean onControlUse(ControlData<?> control);

    public void onControlPostUse(ControlData<?> control){}

    public abstract void onStart();

    public void onFail(){

        if(!tardis.isClient()){

            final Holder.Reference<DamageType> damage = tardis.getLevel().registryAccess().registry(Registries.DAMAGE_TYPE).get().getHolderOrThrow(DamageTypes.EXPLOSION);

            this.tardis.damageTardis(new DamageSource(damage), 10);
            tardis.getLevel().players().forEach(p -> {
                p.getCapability(Capabilities.PLAYER).ifPresent(data -> {
                    data.startShaking(1, 40);
                });
            });
            this.tardis.getLastPilot().ifPresent(id -> {
                tardis.getEmotionalHandler().modLoyalty(id, -10);
            });
        }

    }

    /**
     * Call this when the event is finished correctly
     */
    public void complete(){
        this.isComplete = true;

        if(!this.tardis.getLevel().isClientSide()){
            this.tardis.update(TardisUpdateMessage.FLIGHT_EVENT);
        }
        this.tardis.getLastPilot().ifPresent(id -> {
            tardis.getEmotionalHandler().modLoyalty(id, 1);
        });

    }

    public boolean isComplete(){
        return this.isComplete;
    }

    public boolean shouldStopDefaultBehaviour(ControlData<?> data){
        return this.controlsToOverride.test(data);
    }

    public float getTimeMult(){
        return 1.0F;
    }

}
