package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataFloat;

import java.util.function.Predicate;

public class TimeWindsEvent extends FlightEvent{

    public static final Predicate<ITardisLevel> SHOULD_HAPPEN = tardis -> tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get() < 1.0F;

    private float oldThrottleValue;

    public TimeWindsEvent(FlightEventType type, ITardisLevel level) {
        super(type, level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {

        if(control.getType() == ControlRegistry.THROTTLE.get() && control instanceof ControlDataFloat f){

                this.complete(); //If the user has decreased throttle, do not kill them
        }

        return false; //We're returning false because we want it to actually effect the throttle like normal too
    }

    @Override
    public void onStart() {
        this.oldThrottleValue = this.tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get();
    }

    @Override
    public void onFail() {
        super.onFail();
        this.tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).set(0.1F);

    }
}
