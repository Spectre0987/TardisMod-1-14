package net.tardis.mod.flight_event;

import net.minecraft.core.Direction;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.registry.SubsystemRegistry;

import java.util.function.Supplier;

public class AxisMissFlightEvent extends SingleControlEvent{

    final Direction.Axis axis;

    public AxisMissFlightEvent(FlightEventType type, ITardisLevel level, Supplier<? extends ControlType<?>> controlType, Direction.Axis axis) {
        super(type, level, controlType, true);
        this.axis = axis;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onFail() {
        super.onFail();
        this.tardis.getSubsystem(SubsystemRegistry.NAV_COM.get()).ifPresent(sys -> {
            sys.damage(3 + tardis.getLevel().random.nextInt(5));
        });
        this.tardis.setDestination(this.tardis.getDestination().randomizeOnAxis(this.tardis.getLevel().random, this.axis, 20));
    }
}
