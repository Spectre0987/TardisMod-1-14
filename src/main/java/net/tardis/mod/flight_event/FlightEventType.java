package net.tardis.mod.flight_event;

import com.mojang.datafixers.kinds.Const;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.FlightEventRegistry;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class FlightEventType {

    public final BiFunction<FlightEventType, ITardisLevel, FlightEvent> supplier;
    public final Predicate<ITardisLevel> applies;
    /**
     *  1 - very common, 0.1 happens at 10% chance of other events
     *  if zero or below, this won't be given out randomly at all and must be tiggered in another way
     * **/
    public final float chance;
    public boolean isNatural = true;

    public FlightEventType(float baseChance, BiFunction<FlightEventType, ITardisLevel,FlightEvent> supplier, Predicate<ITardisLevel> applies){
        this.supplier = supplier;
        this.applies = applies;
        this.chance = baseChance;
    }

    public FlightEventType(float baseChance, BiFunction<FlightEventType, ITardisLevel, FlightEvent> supplier){
        this(baseChance, supplier, t -> true);
    }

    public FlightEvent create(ITardisLevel level){
        return this.supplier.apply(this, level);
    }

    public boolean canApply(ITardisLevel level){
        return this.applies.test(level);
    }

    public boolean isNatural(){
        return this.isNatural;
    }

    public FlightEventType notNatural(){
        this.isNatural = false;
        return this;
    }

    public Component makeTrans(){
        return Component.translatable(
                Constants.Translation.makeGenericTranslation(FlightEventRegistry.REGISTRY.get(), this)
        );
    }

}
