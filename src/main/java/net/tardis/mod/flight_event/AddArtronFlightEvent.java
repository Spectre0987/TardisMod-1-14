package net.tardis.mod.flight_event;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.registry.ControlRegistry;

import java.util.function.Predicate;

public class AddArtronFlightEvent extends FlightEvent{

    public static final int ARTRON_GAIN = 25;
    public static final Predicate<ITardisLevel> SHOULD_HAPPEN = tardis ->
        tardis.getFuelHandler().getStoredArtron() < tardis.getFuelHandler().getMaxArtron();

    public AddArtronFlightEvent(FlightEventType type, ITardisLevel level) {
        super(type, level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {

        if(control.getType() == ControlRegistry.REFUELER.get()){
            this.tardis.getFuelHandler().fillArtron(ARTRON_GAIN, false);
            this.complete();
            return true; // Cancel toggling the refuler
        }

        return false;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onFail() {
        //Nothing bad happens on fail
    }
}
