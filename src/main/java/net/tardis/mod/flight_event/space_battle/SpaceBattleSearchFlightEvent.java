package net.tardis.mod.flight_event.space_battle;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.FlightEventRegistry;
import net.tardis.mod.registry.JsonRegistries;

public class SpaceBattleSearchFlightEvent extends FlightEvent {
    public SpaceBattleSearchFlightEvent(FlightEventType type, ITardisLevel level) {
        super(type, level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {
        if(control.getType() == ControlRegistry.COMMUNICATOR.get()){
            this.complete();
            this.tardis.setCurrentFlightEvent(FlightEventRegistry.SPACE_EVENT_ESCAPE.get());

            //Unlock random schematic
            this.tardis.getUnlockHandler().unlock(
                    tardis.getLevel().registryAccess().registryOrThrow(JsonRegistries.SCHEMATIC_LOOT)
                            .get(Helper.createRL("space_battle"))
                            .generate(tardis.getLevel().getRandom())
            );

            return true;
        }
        return false;
    }

    @Override
    public void onFail() {
        this.tardis.setCurrentFlightEvent(FlightEventRegistry.SPACE_EVENT_ESCAPE.get());
    }

    @Override
    public void onStart() {

    }
}
