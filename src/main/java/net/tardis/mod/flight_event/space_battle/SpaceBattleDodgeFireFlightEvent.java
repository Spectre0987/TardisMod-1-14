package net.tardis.mod.flight_event.space_battle;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.FlightEventRegistry;

public class SpaceBattleDodgeFireFlightEvent extends FlightEvent {
    public SpaceBattleDodgeFireFlightEvent(FlightEventType type, ITardisLevel level) {
        super(type, level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {
        if(control.getType() == ControlRegistry.RANDOMIZER.get()){
            this.complete();
            this.tardis.setCurrentFlightEvent(FlightEventRegistry.SPACE_EVENT_SEARCH.get());
            return true;
        }
        return false;
    }

    @Override
    public void onFail() {
        super.onFail();
        //When you fail to dodge, you have a 25% chance to have to dodge again, otherwise continue with the event
        tardis.setCurrentFlightEvent(tardis.getLevel().random.nextFloat() < 0.25F ? FlightEventRegistry.SPACE_EVENT_SEARCH.get() : this.getType());
    }

    @Override
    public void complete() {
        super.complete();
    }

    @Override
    public void onStart() {

    }
}
