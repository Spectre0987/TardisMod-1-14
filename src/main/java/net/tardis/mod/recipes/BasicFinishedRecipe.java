package net.tardis.mod.recipes;

import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.Nullable;

public class BasicFinishedRecipe<T extends BasicRecipeBuilder> implements FinishedRecipe {

    final T builder;
    final RecipeSerializer<?> serializer;
    final ResourceLocation key;

    public BasicFinishedRecipe(T builder, RecipeSerializer<?> serializer){
        this.builder = builder;
        this.serializer = serializer;

        ResourceLocation serializerKey = ForgeRegistries.RECIPE_SERIALIZERS.getKey(serializer);
        key = builder.id.withPrefix(serializerKey.getPath() + "/");
    }

    @Override
    public void serializeRecipeData(JsonObject pJson) {
        this.builder.createJson(pJson);
    }

    @Override
    public ResourceLocation getId() {
        return this.key;
    }

    @Override
    public RecipeSerializer<?> getType() {
        return this.serializer;
    }

    @Nullable
    @Override
    public JsonObject serializeAdvancement() {
        builder.advancement
                .parent(RecipeBuilder.ROOT_RECIPE_ADVANCEMENT)
                .addCriterion(
                "has_recipe",
                RecipeUnlockedTrigger.unlocked(this.getId()))
                    .rewards(AdvancementRewards.Builder.recipe(this.getId()))
                    .requirements(RequirementsStrategy.OR);
        return builder.advancement.serializeToJson();
    }

    @Nullable
    @Override
    public ResourceLocation getAdvancementId() {
        return getId().withPrefix("recipes/");
    }
}
