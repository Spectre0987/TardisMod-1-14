package net.tardis.mod.recipes;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.Container;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.CraftQuantiscopeSetting;
import net.tardis.mod.helpers.JsonHelper;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class QuantiscopeCraftingRecipe implements Recipe<Container> {

    private final ResourceLocation id;

    public final Ingredient mainComponentIngredient;
    public final NonNullList<Ingredient> ingredients = NonNullList.withSize(5, Ingredient.EMPTY);
    final ItemStack result;


    public QuantiscopeCraftingRecipe(ResourceLocation id, ItemStack result, Ingredient main, Ingredient... ings){
        this.id = id;
        this.mainComponentIngredient = main;
        this.result = result;
        for(int i = 0; i < ings.length; ++i){
            this.ingredients.set(i, ings[i]);
        }
    }

    @Override
    public boolean matches(Container container, Level level) {

        if(!this.mainComponentIngredient.test(container.getItem(5))){
            return false;
        }
        //Other
        List<Integer> used = new ArrayList<>();
        for(Ingredient ing : this.ingredients){
            boolean has = false;
            for(int i = 0; i < 5; ++i){

                //If we've already matched this ingredient, skip further checking
                if(has)
                    continue;
                //If we've matched on this slot before, keep looking
                if(used.contains(i))
                    continue;
                if(ing.test(container.getItem(i))){
                    used.add(i);
                    has = true;
                }
            }
            if(!has)
                return false;
        }

        return container.canPlaceItem(6, this.result.copy());
    }

    @Override
    public ItemStack assemble(Container inv, RegistryAccess reg) {
        for(int i = 0; i <= 5; ++i){
            //inv.getObject().getInventory().get().extractItem(i, 1, false);
            inv.removeItem(i, 1);
        }
        return this.result.copy();
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return true;
    }

    @Override
    public ItemStack getResultItem(RegistryAccess reg) {
        return this.result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return RecipeRegistry.QUANTISCOPE_CRAFTING.get();
    }

    @Override
    public RecipeType<?> getType() {
        return RecipeRegistry.QUANTISCOPE_CRAFTING_TYPE;
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        final NonNullList<Ingredient> list = NonNullList.create();
        list.add(this.mainComponentIngredient);
        list.addAll(this.ingredients);
        return list;
    }

    public static class Serialzier implements RecipeSerializer<QuantiscopeCraftingRecipe> {

        @Override
        public QuantiscopeCraftingRecipe fromJson(ResourceLocation id, JsonObject root) {

            final JsonObject resultObj = root.getAsJsonObject("result");

            ItemStack result = JsonHelper.readItemStack(resultObj);

            Ingredient mainIng = Ingredient.fromJson(root.get("main_component"));

            final List<Ingredient> otherIng = new ArrayList<>();
            for(JsonElement ele : root.getAsJsonArray("other_ingredients")){
                otherIng.add(Ingredient.fromJson(ele));
            }

            return new QuantiscopeCraftingRecipe(id, result, mainIng, otherIng.toArray(new Ingredient[0]));
        }

        @Override
        public @Nullable QuantiscopeCraftingRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
            final ItemStack result = buf.readItem();
            final Ingredient mainIng = Ingredient.fromNetwork(buf);
            final int otherSize = buf.readInt();
            final Ingredient[] others = new Ingredient[otherSize];
            for(int i = 0; i < otherSize; ++i){
                others[i] = Ingredient.fromNetwork(buf);
            }
            return new QuantiscopeCraftingRecipe(id, result, mainIng, others);
        }

        @Override
        public void toNetwork(FriendlyByteBuf buf, QuantiscopeCraftingRecipe recipe) {
            buf.writeItemStack(recipe.result, true);
            recipe.mainComponentIngredient.toNetwork(buf);
            buf.writeInt(recipe.ingredients.size());
            for(Ingredient i : recipe.ingredients){
                i.toNetwork(buf);
            }
        }
    }

    public static class Builder extends BasicRecipeBuilder{

        final ItemStack result;
        Ingredient main;
        List<Ingredient> other = new ArrayList<>();

        private Builder(ResourceLocation id, ItemStack result){
            super(id);
            this.result = result;
        }

        public static Builder create(ResourceLocation id, ItemStack result){
            return new Builder(id, result);
        }

        public Builder unlockedBy(String key, CriterionTriggerInstance crit){
            this.advancement.addCriterion(key, crit);
            return this;
        }

        @Override
        public void save(Consumer<FinishedRecipe> consumer, RecipeSerializer<?> serializer){
            consumer.accept(new BasicFinishedRecipe<>(this, serializer));
        }

        public void save(Consumer<FinishedRecipe> consumer){
            this.save(consumer, RecipeRegistry.QUANTISCOPE_CRAFTING.get());
        }

        @Override
        public JsonObject createJson(JsonObject root) {

            root.add("result", JsonHelper.writeItemStack(this.result));
            root.add("main_component", this.main.toJson());
            JsonArray others = new JsonArray();
            for(Ingredient ing : this.other){
                others.add(ing.toJson());
            }
            root.add("other_ingredients", others);

            return root;
        }

        public Builder withMain(Ingredient main){
            this.main = main;
            return this;
        }

        public Builder withMain(ItemLike main){
            return this.withMain(Ingredient.of(main));
        }

        public Builder withMain(TagKey<Item> item){
            return this.withMain(Ingredient.of(item));
        }

        public Builder with(Ingredient ing) {
            this.other.add(ing);
            return this;
        }

        public Builder with(ItemLike item){
            return this.with(Ingredient.of(item));
        }

        public Builder with(TagKey<Item> tag){
            return this.with(Ingredient.of(tag));
        }
    }
}
