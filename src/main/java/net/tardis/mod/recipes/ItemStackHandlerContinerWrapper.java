package net.tardis.mod.recipes;

import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class ItemStackHandlerContinerWrapper<T> implements Container {

    final ItemStackHandler handler;
    private final T object;
    private Predicate<Player> isValidFor = player -> true;
    private Consumer<T> onOpen = (t) -> {};
    private Consumer<T> onClose = (t) -> {};
    private Consumer<T> setChanged;

    public ItemStackHandlerContinerWrapper(ItemStackHandler handler, T object, Consumer<T> setChanged){
        this.handler = handler;
        this.setChanged = setChanged;
        this.object = object;
    }

    public ItemStackHandlerContinerWrapper<T> onOpen(Consumer<T> action){
        this.onOpen = action;
        return this;
    }

    public ItemStackHandlerContinerWrapper<T> onClose(Consumer<T> action){
        this.onClose = action;
        return this;
    }

    @Override
    public int getContainerSize() {
        return handler.getSlots();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < this.handler.getSlots(); ++i){
            if(!this.handler.getStackInSlot(i).isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getItem(int i) {
        return this.handler.getStackInSlot(i);
    }

    @Override
    public ItemStack removeItem(int slot, int amount) {
        ItemStack extracted = this.handler.extractItem(slot, amount, false);
        this.setChanged();
        return extracted;
    }

    @Override
    public ItemStack removeItemNoUpdate(int slot) {
        ItemStack result = this.handler.getStackInSlot(slot);
        this.handler.setStackInSlot(slot, ItemStack.EMPTY);
        return result;
    }

    @Override
    public void setItem(int slot, ItemStack itemStack) {
        this.handler.setStackInSlot(slot, itemStack);
        this.setChanged();
    }

    @Override
    public void setChanged() {
        this.setChanged.accept(this.object);
    }

    @Override
    public boolean stillValid(Player player) {
        return this.isValidFor.test(player);
    }

    @Override
    public void clearContent() {
        for(int i = 0; i < this.getContainerSize(); ++i){
            this.handler.setStackInSlot(i, ItemStack.EMPTY);
        }
        this.setChanged();
    }

    @Override
    public void startOpen(Player pPlayer) {
        Container.super.startOpen(pPlayer);
        this.onOpen.accept(object);
    }

    @Override
    public void stopOpen(Player pPlayer) {
        Container.super.stopOpen(pPlayer);
        this.onClose.accept(object);
    }

    public T getObject(){
        return this.object;
    }
}
