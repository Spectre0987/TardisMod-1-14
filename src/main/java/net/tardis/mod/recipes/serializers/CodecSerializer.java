package net.tardis.mod.recipes.serializers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.tardis.mod.Tardis;
import org.jetbrains.annotations.Nullable;

public class CodecSerializer<T extends Recipe<?>> implements RecipeSerializer<T> {

    final Codec<T> codec;

    public CodecSerializer(Codec<T> codec){
        this.codec = codec;
    }


    @Override
    public T fromJson(ResourceLocation rl, JsonObject json) {
        return this.codec.decode(JsonOps.INSTANCE, json).getOrThrow(false, Tardis.LOGGER::warn).getFirst();
    }

    @Override
    public @Nullable T fromNetwork(ResourceLocation rl, FriendlyByteBuf buf) {
        return this.codec.decode(JsonOps.INSTANCE, JsonParser.parseString(buf.readUtf())).getOrThrow(false, Tardis.LOGGER::warn).getFirst();
    }

    @Override
    public void toNetwork(FriendlyByteBuf buf, T recipe) {
        buf.writeUtf(this.codec.encodeStart(JsonOps.INSTANCE, recipe).getOrThrow(false, Tardis.LOGGER::warn).toString());
    }
}
