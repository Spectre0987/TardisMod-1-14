package net.tardis.mod.recipes;

import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.RecipeSerializer;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

public abstract class BasicRecipeBuilder {

    public final ResourceLocation id;
    public Advancement.Builder advancement = Advancement.Builder.advancement();

    public BasicRecipeBuilder(ResourceLocation id){
        this.id = id;
    }

    public abstract void save(Consumer<FinishedRecipe> pFinishedRecipeConsumer, RecipeSerializer<?> serializer);

    public abstract JsonObject createJson(JsonObject root);
}
