package net.tardis.mod.recipes;

import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.recipes.serializers.DraftingTableRecipeSerializer;

public class RecipeRegistry {

    public static final DeferredRegister<RecipeSerializer<?>> RECIPES = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, Tardis.MODID);

    //Recipe Types
    public static final RecipeType<DraftingTableRecipe> DRAFTING_TABLE_TYPE = RecipeType.simple(Helper.createRL("drafting_table"));
    public static final RecipeType<AlembicRecipe> ALEMBIC_TYPE = RecipeType.simple(Helper.createRL("alembic"));
    public static final RecipeType<AlembicBottlingRecipe> ALEMBIC_BOTTLING_TYPE = RecipeType.simple(Helper.createRL("alembic_bottle"));
    public static final RecipeType<QuantiscopeCraftingRecipe> QUANTISCOPE_CRAFTING_TYPE = RecipeType.simple(Helper.createRL("quantiscope/crafting"));

    //Recipe Serializers
    public static final RegistryObject<RecipeSerializer<DraftingTableRecipe>> DRAFTING_TABLE_SERIALIZER = RECIPES.register("drafting_table", DraftingTableRecipeSerializer::new);
    public static final RegistryObject<AlembicRecipe.Serializer> ALEMBIC_SERIALIZER = RECIPES.register("alembic", AlembicRecipe.Serializer::new);
    public static final RegistryObject<AlembicBottlingRecipe.Serializer> ALEMBIC_BOTTLE_RECIPE = RECIPES.register("alembic_bottling", AlembicBottlingRecipe.Serializer::new);
    public static final RegistryObject<QuantiscopeCraftingRecipe.Serialzier> QUANTISCOPE_CRAFTING = RECIPES.register("quantiscope/crafting", QuantiscopeCraftingRecipe.Serialzier::new);



}
