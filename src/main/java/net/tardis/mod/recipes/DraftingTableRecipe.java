package net.tardis.mod.recipes;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.core.RegistryAccess;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.JsonHelper;
import net.tardis.mod.misc.ToolType;

import java.util.*;
import java.util.function.Consumer;

public class DraftingTableRecipe implements Recipe<DraftingTableTile> {

    private final ResourceLocation id;
    public final List<String> pattern = new ArrayList<>();
    public final HashMap<Character, Ingredient> key = new HashMap<>();
    public final EnumMap<ToolType, Integer> tools = new EnumMap<>(ToolType.class);
    private final ItemStack result;
    public final ItemStack[] slagResults;

    private int width;
    private int height;

    public DraftingTableRecipe(ResourceLocation id, List<String> pattern, HashMap<Character, Ingredient> keys, EnumMap<ToolType, Integer> tools, ItemStack result, ItemStack[] slagResult){
        this.id = id;
        this.pattern.addAll(pattern);
        this.key.putAll(keys);
        this.tools.putAll(tools);
        this.result = result;
        this.slagResults = slagResult;
        this.setSizeByPattern(pattern);
    }

    @Override
    public boolean matches(DraftingTableTile table, Level level) {

        Ingredient[] ingredients = fullList();
        for(int slot = 0; slot < 9; ++slot){
            if(!ingredients[slot].test(table.getItem(slot))) {
                return false;
            }
        }
        return table.getInventory().insertItem(9, this.result.copy(), true).isEmpty();
    }

    @Override
    public ItemStack assemble(DraftingTableTile tile, RegistryAccess p_267165_) {
        for(int i = 0; i < 9; ++i){
            tile.inventory.extractItem(0, 1, false);
        }
        return this.result.copy();
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return pWidth >= this.width && pHeight >= this.height;
    }

    @Override
    public ItemStack getResultItem(RegistryAccess p_267052_) {
        return this.result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get();
    }

    @Override
    public RecipeType<?> getType() {
        return RecipeRegistry.DRAFTING_TABLE_TYPE;
    }

    public Ingredient[] fullList(){
        Ingredient[] ings = new Ingredient[9];
        int i = 0;
        for(String line : this.pattern){
            for(Ingredient ing : this.makePatternArray(line)){
                ings[i] = ing;
                ++i;
            }
        }
        return ings;
    }
    public Ingredient[] makePatternArray(String line){

        char[] chars = line.toCharArray();

        Ingredient[] ingredients = new Ingredient[3];
        for(int i = 0; i < 3; ++i){
            if(chars.length <= i || chars[i] == ' ')
                ingredients[i] = Ingredient.EMPTY;
            else {
                if(this.key.containsKey(chars[i]))
                    ingredients[i] = this.key.get(chars[i]);
                else return null; //If there is a key we don't recognise, complain lol
            }
        }
        return ingredients;
    }

    public void setSizeByPattern(List<String> pattern){
        this.height = pattern.size();

        int width = 0;
        for(String line : pattern){
            int lineWidth = line.length();
            if(width < lineWidth)
                width = lineWidth;
        }
        this.width = width;

    }

    public Set<ToolType> getToolRequirements() {
        return this.tools.keySet();
    }

    public int getToolRequirement(ToolType type){
        return this.tools.get(type);
    }

    public static class Builder extends BasicRecipeBuilder {

        final EnumMap<ToolType, Integer> toolTypes = new EnumMap<>(ToolType.class);
        final List<String> pattern = new ArrayList<>();
        final HashMap<Character, Ingredient> map = new HashMap<>();
        final ItemStack result;
        final List<ItemStack> slagResult = new ArrayList<>();
        public Builder(ResourceLocation id, ItemStack result) {
            super(id);
            this.result = result;
        }

        public static Builder create(ResourceLocation key, ItemStack result){
            return new Builder(key, result);
        }

        public Builder toolType(ToolType type, int time){
            toolTypes.put(type, time);
            return this;
        }

        public Builder slag(ItemStack... stacks){
            Arrays.stream(stacks).forEach(this.slagResult::add);
            return this;
        }

        public Builder pattern(String line){
            this.pattern.add(line);
            return this;
        }

        public Builder unlockedBy(String name, CriterionTriggerInstance trigger){
            this.advancement.addCriterion(name, trigger);
            return this;
        }

        public Builder map(char c, Ingredient ing){
            this.map.put(c, ing);
            return this;
        }
        public Builder map(char c, ItemLike ing){
            return map(c, Ingredient.of(ing));
        }
        public Builder map(char c, TagKey<Item> ing){
            return map(c, Ingredient.of(ing));
        }

        @Override
        public void save(Consumer<FinishedRecipe> pFinishedRecipeConsumer, RecipeSerializer<?> serializer) {
            pFinishedRecipeConsumer.accept(new BasicFinishedRecipe<>(this, serializer));
        }

        public void save(Consumer<FinishedRecipe> consumer){
            save(consumer, RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get());
        }

        @Override
        public JsonObject createJson(JsonObject root) {

            //Serialize tools
            JsonArray tools = new JsonArray();
            for(Map.Entry<ToolType, Integer> entry : this.toolTypes.entrySet()){
                JsonObject tool = new JsonObject();
                tool.add("tool", new JsonPrimitive(entry.getKey().getName()));
                tool.add("time", new JsonPrimitive(entry.getValue()));
                tools.add(tool);
            }
            root.add("tools", tools);

            //Serialize patter
            JsonArray pattern = new JsonArray();
            for(int i = 0; i < this.pattern.size(); ++i){
                pattern.add(i < this.pattern.size() ? this.pattern.get(i) : "   ");
            }
            root.add("pattern", pattern);

            //Serialize Map
            JsonObject keys = new JsonObject();
            for(Map.Entry<Character, Ingredient> ing : this.map.entrySet()){
                keys.add(ing.getKey().toString(), ing.getValue().toJson());
            }
            root.add("keys", keys);

            //Serialize slag
            if(!this.slagResult.isEmpty()){
                JsonArray slag = new JsonArray();

                for(ItemStack result : this.slagResult){
                    slag.add(JsonHelper.writeItemStack(result));
                }

                root.add("slag", slag);
            }

            //Serialize result
            root.add("result", JsonHelper.writeItemStack(this.result));

            return root;
        }
    }
}
