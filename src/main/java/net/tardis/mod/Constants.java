package net.tardis.mod;

import net.minecraft.core.Registry;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.registries.IForgeRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.resource_listener.server.TardisNames;

import java.util.function.Predicate;

public class Constants {

    public static String getPathFromJsonReg(ResourceKey<? extends Registry<?>> reg, ResourceLocation key) {
        return reg.location().getNamespace() + "/" + reg.location().getPath() + "/" + key.getPath() + ".json";
    }

    public static class Translation{

        public static final Component MUST_BE_USED_IN_TARDIS = Component.translatable("block.message." + Tardis.MODID + ".must_be_used_in_tardis");
        public static final Component MUST_BE_ATTUNED = Component.translatable("tardis.feedback." + Tardis.MODID + ".must_be_attuned");
        public static final String POWER_STORED = Tardis.MODID + ".item.tooltips.power";
        public static final String ARTRON_STORED = Tardis.MODID + ".item.tooltip.artron_stored";
        public static final String NOT_ENOUGH_POWER = "feedback." + Tardis.MODID + ".fe_low";

        public static String makeGuiTitleTranslation(String title){
            return "screen." + Tardis.MODID + ".title." + title;
        }

        public static String makeItemTooltip(ResourceLocation id, String... extra) {
            final StringBuilder builder = new StringBuilder("tooltip." + id.getNamespace() + ".item." + id.getPath().replace('/', '.'));
            for(String s : extra){
                builder.append("." + s);
            }
            return builder.toString();
        }

        public static Component makeTardisTranslation(ResourceKey<Level> key) {

            String name = key.location().toString();

            if(TardisNames.CLIENT_NAMES.containsKey(key)){
                name = TardisNames.CLIENT_NAMES.get(key);
            }

            return Component.translatable("tooltip.tardis.bound_tardis", name);
        }

        public static String makeGenericTranslation(ResourceKey<? extends Registry<?>> reg, ResourceLocation val){
            return reg.location().getPath().replace("/", ".") +
                    "." + val.getNamespace() +
                    "." + (val.getNamespace().equals(reg.location().getNamespace()) ? "" : reg.location().getNamespace() + ".") +
                    val.getPath().replace("/", ".");
        }

        public static <T, V extends T> String makeGenericTranslation(Registry<T> reg, V val){
            ResourceLocation valRl = reg.getKey(val);
            return makeGenericTranslation(reg.key(), valRl);
        }

        public static <T, V extends T> String makeGenericTranslation(IForgeRegistry<T> reg, V val){
            return makeGenericTranslation(reg.getRegistryKey(), reg.getKey(val));
        }


        public static Component translatePower(int energyStored, int maxEnergyStored) {
            return Component.translatable(POWER_STORED, energyStored, maxEnergyStored);
        }

        public static Component translatePower(IEnergyStorage storage) {
            return translatePower(storage.getEnergyStored(), storage.getMaxEnergyStored());
        }

        public static Component translateArtron(float storedArtron, float maxArtron) {
            return Component.translatable(ARTRON_STORED, storedArtron, maxArtron);
        }
    }

    public static class Predicates{

        public static Predicate<ItemStack> isItem(ItemLike item){
            return stack -> stack.getItem() == item;
        }

        public static <T> Predicate<ItemStack> itemExtends(Class<T> clazz){
            return stack -> clazz.isInstance(stack.getItem());
        }

    }

    public static class CapabilityKeys{

        public static final ResourceLocation PLAYER = Helper.createRL("player");
        public static final ResourceLocation TARDIS_LEVEL = Helper.createRL("tardis");

        public static final ResourceLocation GENERAL_WORLD = Helper.createRL("general");
        public static final ResourceLocation CHUNK_CAP = Helper.createRL("main_chunk");
        public static final ResourceLocation TARDIS_CHUNK = Helper.createRL("tardis_chunk");
    }

}
