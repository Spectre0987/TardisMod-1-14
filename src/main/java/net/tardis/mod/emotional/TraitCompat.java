package net.tardis.mod.emotional;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public record TraitCompat(ResourceLocation traitId, List<ResourceLocation> incompatibleTraits) {

    public static final Codec<TraitCompat> CODEC = RecordCodecBuilder.create(i ->
            i.group(
                    ResourceLocation.CODEC.fieldOf("trait").forGetter(TraitCompat::traitId),
                    ResourceLocation.CODEC.listOf().fieldOf("incompatible_traits").forGetter(TraitCompat::incompatibleTraits)
            ).apply(i, TraitCompat::new)
    );

    public boolean isCompatible(ResourceLocation otherType){
        return !this.incompatibleTraits().contains(otherType);
    }

    public static boolean isCompatible(@Nullable TraitCompat compat, ResourceLocation otherTrait){
        if(compat == null)
            return true;

        return compat.isCompatible(otherTrait);

    }

}
