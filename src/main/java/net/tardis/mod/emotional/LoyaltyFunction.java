package net.tardis.mod.emotional;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import org.jetbrains.annotations.Nullable;

public abstract class LoyaltyFunction {


    public abstract boolean isValid(ITardisLevel tardis);
    public abstract boolean isValid(ITardisLevel tardis, @Nullable Player player);


}
