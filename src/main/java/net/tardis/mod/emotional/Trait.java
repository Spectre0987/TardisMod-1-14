package net.tardis.mod.emotional;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.registry.TraitRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class Trait extends TypeHolder<Codec<? extends Trait>> {
    public static final MapCodec<List<Pair<ObjectOrTagCodec<Item>, Integer>>> ITEM_CODEC = Codec.list(
            Codec.pair(
                    ObjectOrTagCodec.createCodec(ForgeRegistries.ITEMS).fieldOf("item").codec(),
                    Codec.INT.fieldOf("loyalty").codec()
            )
    ).fieldOf("liked_items");

    public List<Pair<ObjectOrTagCodec<Item>, Integer>> likedItems = new ArrayList<>();

    public Trait(Codec<? extends Trait> type, List<Pair<ObjectOrTagCodec<Item>, Integer>> likedItems) {
        super(type);
        this.likedItems = likedItems;
    }

    public abstract void affectLanding(ITardisLevel tardis);

    public abstract void onCrewSecond(ITardisLevel tardis, List<Player> crew);

    /**
     * Ticks every mc hour
     */
    public abstract void onLandedHour(ITardisLevel tardis, ServerLevel landedLevel, Biome currentBiome, BlockPos location);

    public int getLikedAmount(ItemLike item){
        for(Pair<ObjectOrTagCodec<Item>, Integer> loyalty : this.likedItems){
            if(!loyalty.getFirst().isTag()){
                if(loyalty.getFirst().getObject() == item.asItem())
                    return loyalty.getSecond();
            }
        }
        return -1;
    }
    public int getLikedAmount(TagKey<Item> tag){
        for(Pair<ObjectOrTagCodec<Item>, Integer> loyalty : this.likedItems){
            if(loyalty.getFirst().isTag() && loyalty.getFirst().getTag().location().equals(tag.location()))
                return loyalty.getSecond();
        }
        return -1;
    }

    public List<Pair<ObjectOrTagCodec<Item>, Integer>> getLikedItems(){
        return this.likedItems;
    }
}
