package net.tardis.mod.emotional;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.resource_listener.server.TraitListener;

import java.util.*;
import java.util.function.Predicate;

public class EmotionalHandler implements INBTSerializable<CompoundTag> {

    public static final int TRAIT_SLOTS = 3;
    public static final int TICKS_IN_HOUR = 1000;
    private final List<Trait> traits = new ArrayList<Trait>();
    private final HashMap<UUID, Integer> loyalties = new HashMap<>();
    private final ITardisLevel tardis;

    private int mood = 0;

    public EmotionalHandler(ITardisLevel tardis){
        this.tardis = tardis;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();

        //Serialize Traits
        ListTag traitList = new ListTag();
        for(Trait trait : this.traits){
            Helper.getKeyFromValueHashMap(TraitListener.traits, trait).ifPresent(key -> {
                traitList.add(StringTag.valueOf(key.toString()));
            });
        }
        tag.put("trait_list", traitList);

        //Serialize Loyalty
        ListTag loyalties = new ListTag();
        for(Map.Entry<UUID, Integer> entry : this.loyalties.entrySet()){
            final CompoundTag t = new CompoundTag();
            t.putUUID("player", entry.getKey());
            t.putInt("amount", entry.getValue());
            loyalties.add(t);
        }
        tag.put("loyalty", loyalties);

        return tag;
    }


    @Override
    public void deserializeNBT(CompoundTag nbt) {

        final ListTag traitList = nbt.getList("trait_list", Tag.TAG_STRING);
        final ListTag loyaltyList = nbt.getList("loyalty", Tag.TAG_COMPOUND);

        //Clear out old data
        this.loyalties.clear();
        this.traits.clear();

        //Load Trait data
        for(int i = 0; i < traitList.size(); ++i){

            if(TRAIT_SLOTS <= i){
                break;// if out of trait slots, stop reading
            }
            ResourceLocation id = new ResourceLocation(traitList.getString(i));
            if(TraitListener.traits.containsKey(id)){
                this.traits.add(TraitListener.traits.get(id));
            }
        }

        //Load loyalties
        for(int i = 0; i < loyaltyList.size(); ++i){
            final CompoundTag t = loyaltyList.getCompound(i);
            this.loyalties.put(t.getUUID("player"), t.getInt("amount"));
        }
    }

    public void setTraits(Trait... types){
        this.traits.clear();
        for(Trait t : types){
            this.traits.add(t);
        }
    }

    public void setLoyalties(Map<UUID, Integer> loyalties){
        this.loyalties.clear();
        this.loyalties.putAll(loyalties);
    }

    public List<Trait> getTraits(){
        if(traits.size() == 0) {
            this.setTraits(generateRandomTraits(this.tardis.getLevel().getRandom()).toArray(new Trait[]{}));
        }
        return this.traits;
    }

    public void tick(){

        //Tick traits every mc hour
        if(!this.tardis.getFlightState().isFlying() && !tardis.isClient()){

            final ServerLevel landedWorld = tardis.getLevel().getServer().getLevel(tardis.getLocation().getLevel());

            if(tardis.getLevel().getGameTime() % TICKS_IN_HOUR == 0) {

                final Biome landedBiome = tardis.getLevel().getBiome(tardis.getLocation().getPos()).get();

                for (Trait t : this.traits) {
                    if (t != null) {
                        t.onLandedHour(tardis, landedWorld, landedBiome, tardis.getLocation().getPos());
                    }
                }
            }

            //Increase mood if near another TARDIS
            if(tardis.getLevel().getGameTime() % (10 * 20) == 0 && tardis.getExteriorExtraData().isNearAnotherTardis()){
                this.modMood(1, 0, 75);
            }
        }
    }

    public int modMood(int deltaMood){
        this.mood += deltaMood;
        return this.mood;
    }

    public int modMood(int deltaMood, int min, int max){
        if(this.mood + deltaMood < min || this.mood + deltaMood > max){
            return mood;
        }
        return modMood(deltaMood);
    }

    public Optional<Integer> getLoyalty(UUID player){
        if(this.loyalties.containsKey(player)){
            return Optional.of(this.loyalties.get(player));
        }
        return Optional.empty();
    }

    /**
     *
     * @param player
     * @param mod - Amount to change by
     * @return - The total loyalty this TARDIS has with this player now
     */
    public int modLoyalty(UUID player, int mod){
        final int newValue = this.loyalties.getOrDefault(player, 0) + mod;
        this.loyalties.put(player, newValue);
        return newValue;
    }

    public static List<Trait> generateRandomTraits(RandomSource rand){

        final List<Trait> types = new ArrayList<>();
        final Predicate<Trait> compatFilter = potentialTrait -> {

            //Can not have duplicates
            if(types.contains(potentialTrait)){
                return false;
            }

            return true;
        };

        for(int i = 0; i < TRAIT_SLOTS; ++i){
            final List<Trait> candidates = TraitListener.traits.values().stream()
                    .filter(compatFilter)
                    .toList();

            if(candidates.isEmpty()){ //If there are no more traits to give this
                break;
            }

            types.add(candidates.get(rand.nextInt(candidates.size())));

        }
        return types;
    }

    public HashMap<UUID, Integer> getLoyalties() {
        return this.loyalties;
    }
}
