package net.tardis.mod.emotional.traits;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.TraitRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LikeUseItemTrait extends Trait {

    public static final Codec<LikeUseItemTrait> CODEC = RecordCodecBuilder.create(i ->
                i.group(
                        TraitRegistry.REGISTRY.get().getCodec().fieldOf("type").forGetter(LikeUseItemTrait::getType),
                        ITEM_CODEC.forGetter(LikeUseItemTrait::getLikedItems),
                        LikedData.CODEC.listOf().fieldOf("liked_used").forGetter(t -> t.likedItems)
                ).apply(i, LikeUseItemTrait::new)
            );

    List<LikedData> likedItems = new ArrayList<>();

    public LikeUseItemTrait(Codec<? extends Trait> type, List<Pair<ObjectOrTagCodec<Item>, Integer>> likedItems, List<LikedData> likedUsedItems) {
        super(type, likedItems);
        this.likedItems.addAll(likedUsedItems);
    }

    @Override
    public void affectLanding(ITardisLevel tardis) {

    }

    @Override
    public void onCrewSecond(ITardisLevel tardis, List<Player> crew) {

    }

    @Override
    public void onLandedHour(ITardisLevel tardisLevel, ServerLevel landedLevel, Biome currentBiome, BlockPos location) {

    }

    public Optional<LikedData> getLikedData(ItemStack stack){
        for(LikedData i : this.likedItems){
            if(i.item().matches(stack.getItem())){
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }
    public static record LikedData(ObjectOrTagCodec<Item> item, int mood, int loyalty){

        public static final Codec<LikedData> CODEC = RecordCodecBuilder.create(i ->
                    i.group(
                            ObjectOrTagCodec.createCodec(ForgeRegistries.ITEMS).fieldOf("item").forGetter(LikedData::item),
                            Codec.INT.fieldOf("mood").forGetter(LikedData::mood),
                            Codec.INT.fieldOf("loyalty").forGetter(LikedData::mood)
                    ).apply(i, LikedData::new)
                );

    }
}
