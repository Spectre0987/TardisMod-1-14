package net.tardis.mod.emotional.traits;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.TraitRegistry;

import java.util.List;

public class LikeCreatureTrait extends Trait {

    public static Codec<LikeCreatureTrait> CODEC = RecordCodecBuilder.create(i ->
                i.group(
                        TraitRegistry.REGISTRY.get().getCodec().fieldOf("type").forGetter(LikeCreatureTrait::getType),
                        Trait.ITEM_CODEC.forGetter(LikeCreatureTrait::getLikedItems),
                        ObjectOrTagCodec.createCodec(ForgeRegistries.ENTITY_TYPES).fieldOf("liked_creature").forGetter(LikeCreatureTrait::getLikedType)
                ).apply(i, LikeCreatureTrait::new)
            );

    final ObjectOrTagCodec<EntityType<?>> likedEntity;

    public LikeCreatureTrait(Codec<? extends Trait> type, List<Pair<ObjectOrTagCodec<Item>, Integer>> likedItems, ObjectOrTagCodec<EntityType<?>> likedType) {
        super(type, likedItems);
        this.likedEntity = likedType;
    }

    @Override
    public void affectLanding(ITardisLevel tardis) {

    }

    @Override
    public void onCrewSecond(ITardisLevel tardis, List<Player> crew) {

    }

    @Override
    public void onLandedHour(ITardisLevel tardis, ServerLevel landedLevel, Biome currentBiome, BlockPos location) {

    }

    public ObjectOrTagCodec<EntityType<?>> getLikedType() {
        return this.likedEntity;
    }
}
