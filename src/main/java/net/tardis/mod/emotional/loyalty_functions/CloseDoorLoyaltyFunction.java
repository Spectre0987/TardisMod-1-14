package net.tardis.mod.emotional.loyalty_functions;

import net.minecraft.world.entity.player.Player;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.enums.DoorState;

public class CloseDoorLoyaltyFunction extends LoyaltyFunction{
    public CloseDoorLoyaltyFunction() {
        super(moreThan(50));
    }


    public void saveFromVoid(ITardisLevel tardis, Player player, TardisEvent.ExitEvent.Pre preEvent){
        if(tardis.isInVortex()){
            tardis.getInteriorManager().getDoorHandler().setDoorState(tardis.getInteriorManager().getDoorHandler().validDoorStates,
                        DoorState.CLOSED
                    );
            tardis.getInteriorManager().getDoorHandler().updateClients(tardis.getLevel());
            tardis.getInteriorManager().getDoorHandler().save();
            preEvent.setCanceled(true);
        }
    }


}
