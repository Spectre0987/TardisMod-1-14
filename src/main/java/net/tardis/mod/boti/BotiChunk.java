package net.tardis.mod.boti;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.IEncodeable;
import org.apache.logging.log4j.Level;

import java.util.HashMap;
import java.util.Map;

public class BotiChunk implements IEncodeable {

    public static final int CHUNK_XZ_SIZE = 16;
    public static final int CHUNK_Y_SIZE = 32;
    private final ChunkPos pos;
    private int yLevel = 64;
    public final Map<BlockPos, BlockState> blocks = new HashMap<>();

    public BotiChunk(ChunkPos pos){
        this.pos = pos;
    }

    public ChunkPos getPos(){
        return this.pos;
    }

    public void createAround(ServerLevel level, ChunkPos cPos, int yLevel){
        this.yLevel = yLevel;
        final LevelChunk chunk = level.getChunk(cPos.x, cPos.z);
        BlockPos.betweenClosedStream(new BlockPos(0, yLevel - CHUNK_Y_SIZE / 2, 0), new BlockPos(15, yLevel + CHUNK_Y_SIZE / 2, 15)).forEach(pos -> {
            try {
                this.addBlock(pos, chunk.getBlockState(pos));
            }
            catch(Exception e){
                Tardis.LOGGER.log(Level.ERROR, e);
            }
        });
    }

    public boolean hasChanged(ServerLevel level, BlockPos pos){
        return false;
    }

    public void addBlock(BlockPos chunkPos, BlockState state) throws Exception{
        if(chunkPos.getX() >= 16 || chunkPos.getZ() >= 16){
            throw new Exception("BOTI Chunk Pos out of bounds! %s, %s".formatted(chunkPos, state));
        }
        this.blocks.put(chunkPos, state);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.blocks.size());

    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        final int size = buf.readInt();
    }
}
