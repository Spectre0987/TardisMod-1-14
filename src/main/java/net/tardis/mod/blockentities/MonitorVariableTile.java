package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateDynamicMonitorBounds;

import java.util.ArrayList;
import java.util.List;

public class MonitorVariableTile extends BaseMonitorTile{

    float[] monitorBounds;

    public MonitorVariableTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public MonitorVariableTile(BlockPos pos, BlockState state){
        this(TileRegistry.MONITOR_VARIABLE.get(), pos, state);
    }

    @Override
    public float[] getBounds() {
        if(this.isMainRenderer()){
            return getMonitorBounds();
        }
        return new float[]{1.0F, 1.0F};
    }

    public float[] getMonitorBounds(){
        if(this.monitorBounds == null){
            this.monitorBounds = getBoundsFromConnected(this.gatherConnectedMonitorPos(new ArrayList<>(), this.getBlockPos(), this.buildSearchList()));
        }
        return this.monitorBounds;
    }

    public void refreshBounds(){
        this.monitorBounds = null;
        if(getLevel() != null && !getLevel().isClientSide()){
            Network.sendToTracking(this, new UpdateDynamicMonitorBounds(this.getBlockPos()));
        }
    }

    public float[] getBoundsFromConnected(List<BlockPos> connected){
        int width = Integer.MIN_VALUE,
                height = Integer.MIN_VALUE;

        final Direction.Axis facingAxis = this.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING).getClockWise().getAxis();

        for(BlockPos pos : connected){
            int w = Math.abs(this.getBlockPos().get(facingAxis) - pos.get(facingAxis)),
                    h = Math.abs(this.getBlockPos().getY() - pos.getY());
            if(w > width)
                width = w;
            if(h > height)
                height = h;
        }
        return new float[]{width + 0.75F, height + 1};
    }


    public boolean isMainRenderer(){
        final Direction facing = this.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING);
        return getLevel().getBlockState(this.getBlockPos().relative(facing.getClockWise())).getBlock() != BlockRegistry.VARIABLE_MONITOR.get() &&
                getLevel().getBlockState(this.getBlockPos().above()).getBlock() != BlockRegistry.VARIABLE_MONITOR.get();
    }

    @Override
    public boolean displayText() {
        return isMainRenderer();
    }

    public List<BlockPos> gatherConnectedMonitorPos(List<BlockPos> others, BlockPos testPos, List<Direction> searchDirections){
        if(getLevel().getBlockState(testPos).getBlock() == BlockRegistry.VARIABLE_MONITOR.get()){
            others.add(testPos);
            for(Direction dir : searchDirections){
                if(!others.contains(testPos.relative(dir))){
                    gatherConnectedMonitorPos(others, testPos.relative(dir), searchDirections);
                }
            }
        }
        return others;
    }

    public List<Direction> buildSearchList(){
        final BlockState state = this.getBlockState();
        if(state.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
            final Direction facing = state.getValue(BlockStateProperties.HORIZONTAL_FACING);
            final List<Direction> dirs = new ArrayList<>();
            dirs.add(Direction.DOWN);
            dirs.add(Direction.UP);
            dirs.add(facing.getClockWise());
            dirs.add(facing.getCounterClockWise());
            return dirs;
        }
        return new ArrayList<>();
    }

    @Override
    public AABB getRenderBoundingBox() {
        float[] bounds = this.getMonitorBounds();
        return super.getRenderBoundingBox().inflate(bounds[0], bounds[1], bounds[0]);
    }
}
