package net.tardis.mod.blockentities.multiblock;

import com.mojang.datafixers.kinds.IdF;
import net.minecraft.core.BlockPos;
import net.tardis.mod.registry.BlockRegistry;

public class MultiblockDatas {

    public static final MultiblockData TWO_BLOCK = MultiblockData.Builder.create()
            .add(new BlockPos(0, 1, 0), () -> BlockRegistry.MULTIBLOCK.get().defaultBlockState())
            .build();

    public static final MultiblockData BROKEN_EXTERIOR = MultiblockData.Builder.create()
            .addBasic(new BlockPos(0, 1, 0))
            .addBasic(new BlockPos(0, 2, 0))
            .build();

    public static final MultiblockData BIG_DOOR = MultiblockData.Builder.create()
            .addBasic(new BlockPos(0, 1, 0))
            .addBasic(new BlockPos(0, 2, 0))
            .addBasic(new BlockPos(0, 3, 0))

            .addBasic(new BlockPos(1, 0, 0))
            .addBasic(new BlockPos(1, 1, 0))
            .addBasic(new BlockPos(1, 2, 0))
            .addBasic(new BlockPos(1, 3, 0))

            .addBasic(new BlockPos(-1, 0, 0))
            .addBasic(new BlockPos(-1, 1, 0))
            .addBasic(new BlockPos(-1, 2, 0))
            .addBasic(new BlockPos(-1, 3, 0))
            .build();

    public static final MultiblockData STILL = MultiblockData.Builder.create()
            .addBasic(new BlockPos(0, 1, 0))
            .add(new BlockPos(-1, 1, 0), () -> BlockRegistry.MULTIBLOCK_REDIR_FLUID.get().defaultBlockState())
            .build();
    public static final MultiblockData FABRICATOR = MultiblockData.Builder.create()
            .addBasic(new BlockPos(0, 1, 0))
            .addBasic(new BlockPos(0, 2, 0))
            .build();
}
