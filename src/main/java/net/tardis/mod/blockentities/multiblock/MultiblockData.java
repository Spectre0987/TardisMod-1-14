package net.tardis.mod.blockentities.multiblock;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.IMultiblock;
import net.tardis.mod.helpers.WorldHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public record MultiblockData(HashMap<BlockPos, Supplier<BlockState>> childBlocks) {

    public void placeBlocks(LevelAccessor accessor, BlockPos masterPos, Rotation rot){

        for(Map.Entry<BlockPos, Supplier<BlockState>> entry : this.childBlocks().entrySet()){
            final BlockPos childPos = masterPos.offset(WorldHelper.rotateBlockPos(entry.getKey(), rot));
            accessor.setBlock(childPos, entry.getValue().get(), 3);
            if(accessor.getBlockEntity(childPos) instanceof IMultiblock<?> multi){
                multi.setMasterPos(masterPos.subtract(childPos)); //Set Relative Master Position
            }
        }

    }

    public void placeBlocks(LevelAccessor accessor, BlockPos masterPos){
        this.placeBlocks(accessor, masterPos, Rotation.NONE);
    }


    public void destroy(LevelAccessor accessor, BlockPos master, Rotation rot){
        for(BlockPos pos : WorldHelper.rotateBlockPositions(this.childBlocks().keySet(), rot)){
            accessor.setBlock(master.offset(pos), Blocks.AIR.defaultBlockState(), 3);
        }
    }

    public void destroy(LevelAccessor accessor, BlockPos master){
        this.destroy(accessor, master, Rotation.NONE);
    }

    public static class Builder{

        final HashMap<BlockPos, Supplier<BlockState>> offsets = new HashMap<>();

        public static Builder create(){
            return new Builder();
        }

        public Builder add(BlockPos pos, Supplier<BlockState> state){
            offsets.put(pos, state);
            return this;
        }

        /**
         *
         * @param pos - offset from master tile's position
         *         Same as {@link Builder#add(BlockPos, Supplier)} but with the supplier filled in with a {@link BlockRegistry#MULTIBLOCK} pre-filled
         * @return
         */
        public Builder addBasic(BlockPos pos){
            return add(pos, () -> BlockRegistry.MULTIBLOCK.get().defaultBlockState());
        }

        public MultiblockData build(){
            return new MultiblockData(offsets);
        }

    }

}
