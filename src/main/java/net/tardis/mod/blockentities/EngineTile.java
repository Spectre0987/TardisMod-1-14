package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class EngineTile extends BlockEntity {

    public EngineTile(BlockPos pPos, BlockState pBlockState) {
        this(TileRegistry.ENGINE.get(), pPos, pBlockState);
    }

    public EngineTile(BlockEntityType<?> type, BlockPos pos, BlockState state){
        super(type, pos, state);
    }

    @Override
    public AABB getRenderBoundingBox() {
        return new AABB(this.getBlockPos(), this.getBlockPos().offset(1, 3, 1));
    }
}
