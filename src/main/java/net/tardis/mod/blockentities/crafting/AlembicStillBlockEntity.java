package net.tardis.mod.blockentities.crafting;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import net.tardis.mod.fluids.FluidTankWithCallback;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AlembicFluidChangeMessage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class AlembicStillBlockEntity extends AlembicBlockEntity implements IMultiblockMaster {

    private MultiblockData multiData = MultiblockDatas.STILL;

    private final LazyOptional<IFluidHandler> craftingTankHolder;
    private final LazyOptional<IFluidHandler> resultTankHolder;

    private LazyOptional<IItemHandler> outputInv = LazyOptional.empty();

    public AlembicStillBlockEntity(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);

        this.craftingTank = new FluidTankWithCallback(10000)
                .withCallback(tank -> {
                    if(!getLevel().isClientSide()){
                        Network.sendNear(getLevel().dimension(),getBlockPos(), 10, new AlembicFluidChangeMessage(getBlockPos(), false, tank.getFluid()));
                    }
                });
        this.resultTank =  new FluidTankWithCallback(10000)
                .withCallback(tank -> {
                    if(!getLevel().isClientSide()){
                        Network.sendNear(getLevel().dimension(),getBlockPos(), 10, new AlembicFluidChangeMessage(getBlockPos(), true, tank.getFluid()));
                    }
                });

        this.craftingTankHolder = LazyOptional.of(this::getCraftingTank);
        this.resultTankHolder = LazyOptional.of(this::getResultTank);
    }

    public AlembicStillBlockEntity(BlockPos pos, BlockState state){
        this(TileRegistry.ALEMBIC_STILL.get(), pos, state);
    }


    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(cap == ForgeCapabilities.FLUID_HANDLER)
            return this.craftingTankHolder.cast();
        return super.getCapability(cap, side);
    }

    @Override
    public void setMasterData(MultiblockData data) {
        this.multiData = data;
    }

    @Override
    public List<BlockPos> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public MultiblockData getMultiblockData() {
        return this.multiData;
    }

    @Override
    public <T> LazyOptional<T> getRedirectedCap(Capability<T> cap, Direction side) {
        if(cap == ForgeCapabilities.FLUID_HANDLER)
            return this.resultTankHolder.cast();
        return LazyOptional.empty();
    }

    public static void tick(Level level, BlockPos pos, BlockState state, AlembicStillBlockEntity alembic){
        AlembicBlockEntity.tick(level, pos, state, alembic);

        //Every second, try to output
        if(level.getGameTime() % 20 == 6 && !alembic.getInventory().getStackInSlot(5).isEmpty()){
            //If we know of a valid inv, fill it
            if(alembic.outputInv.isPresent()){
                alembic.outputInv.ifPresent(inv -> {
                    ItemStack outputItem = alembic.getInventory().getStackInSlot(5);
                    for(int i = 0; i < inv.getSlots(); ++i){
                        if(outputItem.isEmpty())
                            break;
                        outputItem = inv.insertItem(i, outputItem, false);
                    }
                    alembic.getInventory().setStackInSlot(5, outputItem);
                });
            }
            //Otherwise, attempt to gather a valid output inv below
            else{
                final BlockEntity outputTE = level.getBlockEntity(pos.relative(state.getValue(BlockStateProperties.HORIZONTAL_FACING).getCounterClockWise()));
                if(outputTE != null)
                    alembic.outputInv = outputTE.getCapability(ForgeCapabilities.ITEM_HANDLER);
            }
        }

    }
}
