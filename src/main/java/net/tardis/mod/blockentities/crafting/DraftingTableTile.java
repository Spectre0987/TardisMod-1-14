package net.tardis.mod.blockentities.crafting;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BaseContainerBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.item.misc.TardisTool;
import net.tardis.mod.menu.DraftingTableMenu;
import net.tardis.mod.misc.IAcceptTools;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.misc.ToolHandler;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.DraftingTableRecipe;
import net.tardis.mod.recipes.RecipeRegistry;

import javax.annotation.Nullable;

public class DraftingTableTile extends BaseContainerBlockEntity implements IAcceptTools, IBreakLogic {

    public final ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(10)
            .onChange(slot -> {
                if(getLevel() != null && !getLevel().isClientSide())
                    this.setChanged();
            });
    private DraftingTableRecipe recipe;
    private final ToolHandler tools = new ToolHandler();

    public DraftingTableTile(BlockPos p_155229_, BlockState p_155230_) {
        super(TileRegistry.DRAFTING_TABLE.get(), p_155229_, p_155230_);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("inventory", this.inventory.serializeNBT());
    }

    @Override
    protected Component getDefaultName() {
        return BlockRegistry.DRAFTING_TABLE.get().getName();
    }

    @Override
    protected AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory) {
        return new DraftingTableMenu(pContainerId, pInventory, this);
    }



    public void findRecipe(){
        for(DraftingTableRecipe rec : level.getRecipeManager().getAllRecipesFor(RecipeRegistry.DRAFTING_TABLE_TYPE)){
            if(rec.matches(this, this.level)){
                this.recipe = rec;
                return;
            }
        }
    }

    public boolean checkToolWork(){
        if(this.isRecipeValid()){
            for(ToolType type : this.recipe.getToolRequirements()){
                if(this.tools.getWorkFromTool(type) < this.recipe.getToolRequirement(type)){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void finishCrafting(float scrapChance){
        this.tools.clear();
        for(int slot = 0; slot < 9; ++slot){
            this.inventory.extractItem(slot, 1, false);
        }

        //Check for normal or slag
        if(level.random.nextFloat() < scrapChance && recipe.slagResults.length > 0){
            for(ItemStack stack : recipe.slagResults){
                if(!level.isClientSide)
                    level.addFreshEntity(new ItemEntity(level, getBlockPos().getX() + 0.5, getBlockPos().getY() + 1, getBlockPos().getZ() + 0.5, stack.copy()));
            }
            return;
        }
        this.inventory.insertItem(9, recipe.getResultItem(level.registryAccess()).copy(), false);
    }

    public boolean isRecipeValid(){
        if(this.recipe == null)
            return false;
        return this.recipe.matches(this, this.level);
    }

    @Override
    public int getContainerSize() {
        return this.inventory.getSlots();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < this.inventory.getSlots(); ++i){
            if(!this.inventory.getStackInSlot(i).isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getItem(int pSlot) {
        return this.inventory.getStackInSlot(pSlot);
    }

    @Override
    public ItemStack removeItem(int pSlot, int pAmount) {
        this.setChanged();
        return this.inventory.getStackInSlot(pSlot).split(pAmount);
    }

    @Override
    public ItemStack removeItemNoUpdate(int pSlot) {
        ItemStack stack = this.inventory.getStackInSlot(pSlot);
        this.inventory.setStackInSlot(pSlot, ItemStack.EMPTY);
        this.setChanged();
        return stack;
    }

    @Override
    public void setItem(int pSlot, ItemStack pStack) {
        this.inventory.setStackInSlot(pSlot, pStack);
        this.setChanged();
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }

    @Nullable
    public DraftingTableRecipe getRecipe(){
        if(this.isRecipeValid())
            return this.recipe;
        this.findRecipe();
        return this.recipe;

    }

    @Override
    public void clearContent() {
        for(int i = 0; i < this.inventory.getSlots(); ++i){
            this.inventory.setStackInSlot(i, ItemStack.EMPTY);
        }
        this.tools.clear();
        this.setChanged();
    }

    @Override
    public boolean doWork(Level level, Vec3 hitPos, ItemStack toolUsed) {

        if(!tools.doToolWork(level, hitPos, toolUsed))
            return false;

        if(!this.isRecipeValid())
            this.findRecipe();

        if(this.isRecipeValid()){

            float scrapChance = 0;
            for(TardisTool tools : this.tools.getToolFromItem(toolUsed, getLevel().registryAccess())){
                if(tools.scrapChance() > scrapChance){
                    scrapChance = tools.scrapChance();
                }
            }

            if(this.checkToolWork())
                this.finishCrafting(scrapChance);
            return true;
        }
        else this.tools.clear();
        return false;
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
       return ClientboundBlockEntityDataPacket.create(this);
    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void onBroken() {
        WorldHelper.dropItems(this.level, this.getBlockPos(), this.inventory);
    }
}
