package net.tardis.mod.blockentities;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.crafting.AlembicStillBlockEntity;
import net.tardis.mod.blockentities.exteriors.*;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.BrokenExteriorBlock;
import net.tardis.mod.blockentities.consoles.*;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.blockentities.dev.TardisStructureTile;
import net.tardis.mod.blockentities.machines.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class TileRegistry {

    public static final DeferredRegister<BlockEntityType<?>> TYPES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, Tardis.MODID);

    public static final RegistryObject<BlockEntityType<InteriorDoorTile>> INTERIOR_DOOR = TYPES.register("interior_door", () -> create(InteriorDoorTile::new, BlockRegistry.INTERIOR_DOOR.get()));

    //Consoles
    public static final RegistryObject<BlockEntityType<ConsoleTile>> G8_CONSOLE = TYPES.register("g8_console", () -> create(G8ConsoleTile::new, BlockRegistry.G8_CONSOLE.get()));
    public static final RegistryObject<BlockEntityType<SteamConsoleTile>> STEAM_CONSOLE = TYPES.register("steam_console", () -> create(SteamConsoleTile::new, BlockRegistry.STEAM_CONSOLE.get()));
    public static final RegistryObject<BlockEntityType<NouveauConsoleTile>> NOUVEAU_CONSOLE = TYPES.register("neuveau_console", () -> create(NouveauConsoleTile::new, BlockRegistry.NEUVEAU_CONSOLE.get()));
    public static final RegistryObject<BlockEntityType<GalvanicTile>> GALVANIC_CONSOLE = TYPES.register("galvanic_console", () -> create(GalvanicTile::new, BlockRegistry.GALVANIC_CONSOLE.get()));
    public static final RegistryObject<BlockEntityType<NemoConsole>> NEMO_CONSOLE = TYPES.register("nemo_console", () -> create(NemoConsole::new, BlockRegistry.NEMO_CONSOLE.get()));

    //Exteriors
    public static final RegistryObject<BlockEntityType<SteamExteriorTile>> STEAM_EXTERIOR = TYPES.register("steam_exterior", () -> create(SteamExteriorTile::new, BlockRegistry.STEAM_EXTERIOR.get()));
    public static final RegistryObject<BlockEntityType<TTCapsuleExteriorTile>> TT_CAPSULE = TYPES.register("tt_capsule", () -> create(TTCapsuleExteriorTile::new, BlockRegistry.TT_CAPSULE.get()));
    public static final RegistryObject<BlockEntityType<PoliceBoxExteriorTile>> POLICE_BOX_EXTERIOR = TYPES.register("police_box", () -> create(PoliceBoxExteriorTile::new, BlockRegistry.POLICE_BOX_EXTERIOR.get()));
    public static final RegistryObject<BlockEntityType<ChameleonExteriorTile>> CHAMELEON_EXTERIOR = TYPES.register("chameleon_exterior", () -> create(ChameleonExteriorTile::new, BlockRegistry.CHAMELEON_EXTERIOR.get()));
    public static final RegistryObject<BlockEntityType<CoffinExteriorTile>> COFFIN = TYPES.register("coffin_exterior", () -> create(
            CoffinExteriorTile::new,
            BlockRegistry.COFFIN_EXTERIOR.get()
    ));
    public static final RegistryObject<BlockEntityType<OctaExteriorTile>> OCTA_EXTERIOR = TYPES.register("octa_exterior", () -> create(
            OctaExteriorTile::new,
            BlockRegistry.OCTA_EXTERIOR.get()
    ));
    public static final RegistryObject<BlockEntityType<TrunkExteriorTile>> TRUNK_EXTERIOR = TYPES.register("trunk_exterior", () -> create(
            TrunkExteriorTile::new,
            BlockRegistry.TRUNK_EXTERIOR.get()
    ));
    public static final RegistryObject<BlockEntityType<SpruceExteriorTile>> SPRUCE_EXTERIOR = TYPES.register("spruce_exterior", () -> create(
            SpruceExteriorTile::new,
            BlockRegistry.SPRUCE_EXTERIOR.get()
    ));

    //Machines
    public static final RegistryObject<BlockEntityType<DraftingTableTile>> DRAFTING_TABLE = TYPES.register("drafting_table", () -> create(DraftingTableTile::new, BlockRegistry.DRAFTING_TABLE.get()));
    public static final RegistryObject<BlockEntityType<BaseMonitorTile>> BASIC_MONITOR = TYPES.register("monitor", () -> create(BaseMonitorTile::new,
            BlockRegistry.STEAM_MONITOR.get(),
            BlockRegistry.EYE_MONITOR.get(),
            BlockRegistry.RCA_MONITOR.get()
    ));
    public static final RegistryObject<BlockEntityType<MultiblockChildTile>> MULTIBLOCK = TYPES.register("multiblock", () -> create(MultiblockChildTile::new, BlockRegistry.MULTIBLOCK.get()));
    public static final RegistryObject<BlockEntityType<BrokenExteriorTile>> BROKEN_EXTERIOR = TYPES.register("broken_exterior", () -> create(BrokenExteriorTile::new, collectAllMatching(b -> b instanceof BrokenExteriorBlock)));
    public static final RegistryObject<BlockEntityType<EngineTile>> ENGINE = TYPES.register("engine", () -> create(EngineTile::new,
            BlockRegistry.ENGINE_STEAM.get()
    ));
    public static final RegistryObject<BlockEntityType<RoofEngineTile>> ROOF_ENGINE = TYPES.register("roof_engine", () -> create(RoofEngineTile::new,
            BlockRegistry.ENGINE_ROOF.get()
    ));
    public static final RegistryObject<BlockEntityType<AlembicBlockEntity>> ALEMBIC = TYPES.register("alembic", () -> create(AlembicBlockEntity::create, BlockRegistry.ALEMBIC.get()));
    public static final RegistryObject<BlockEntityType<ARSPanelTile>> ARS_PANEL = TYPES.register("ars_panel", () -> create(ARSPanelTile::new, BlockRegistry.ARS_PANEL.get()));
    public static final RegistryObject<BlockEntityType<TardisStructureTile>> STRUCTURE = TYPES.register("structure", () -> create(TardisStructureTile::new, BlockRegistry.CORRIDOR_STRUCTURE_SAVER.get()));

    public static final RegistryObject<BlockEntityType<DedicationPlaqueTile>> DEDICATION_PLAQUE = TYPES.register("dedication_plaque", () -> create(
            DedicationPlaqueTile::new,
            BlockRegistry.DEDICATION_PLAQUE.get()
    ));
    public static final RegistryObject<BlockEntityType<QuantiscopeTile>> QUANTISCOPE = TYPES.register("quantiscope", () -> create(
            QuantiscopeTile::new,
            BlockRegistry.QUANTISCOPE.get()
    ));
    public static final RegistryObject<BlockEntityType<RiftPylonTile>> RIFT_PYLON = TYPES.register("rift_pylon", () -> create(
            RiftPylonTile::new,
            BlockRegistry.RIFT_PYLON.get()
    ));

    public static final RegistryObject<BlockEntityType<RiftCollectorTile>> RIFT_COLLECTOR = TYPES.register("rift_collector", () -> create(
            RiftCollectorTile::new,
            BlockRegistry.RIFT_COLLECTOR.get()
    ));

    public static final RegistryObject<BlockEntityType<MatterBufferTile>> MATTER_BUFFER = TYPES.register("spectrometer", () -> create(
            MatterBufferTile::new,
            BlockRegistry.MATTER_BUFFER.get()
    ));
    public static final RegistryObject<BlockEntityType<TemporalScoopTile>> TEMPORAL_SCOOP = TYPES.register("temporal_scoop", () -> create(
            TemporalScoopTile::new,
            BlockRegistry.SCOOP_VAULT.get()
    ));
    public static final RegistryObject<BlockEntityType<RoundelTapTile>> ROUNDEL_TAP = TYPES.register("roundel_tap", () -> create(
            RoundelTapTile::new,
            BlockRegistry.ROUNDEL_TAP.get()
    ));
    public static final RegistryObject<BlockEntityType<AlembicStillBlockEntity>> ALEMBIC_STILL = TYPES.register("alembic_still", () -> create(
            AlembicStillBlockEntity::new,
            BlockRegistry.ALEMBIC_STILL.get()
    ));
    public static final RegistryObject<BlockEntityType<FabricatorTile>> FABRICATOR_MACHINE = TYPES.register("fabricator", () -> create(
            FabricatorTile::new,
            BlockRegistry.FABRICATOR_MACHINE.get()
    ));
    public static final RegistryObject<BlockEntityType<TeleportTubeTile>> TELEPORT_TUBE = TYPES.register("teleport_tube", () -> create(
            TeleportTubeTile::new,
            BlockRegistry.TELEPORT_TUBE.get()
    ));


    public static final RegistryObject<BlockEntityType<DisguisedBlockTile>> DISGUISED_BLOCK = TYPES.register("disguised_block", () -> create(
            DisguisedBlockTile::new,
                BlockRegistry.DISGUISED_BLOCK.get(),
                BlockRegistry.CHAMELEON_AUXILLARY.get()
    ));
    public static final RegistryObject<BlockEntityType<PlaqueTile>> ITEM_PLAQUE = TYPES.register("item_plaque", () -> create(
            PlaqueTile::new,
            BlockRegistry.ITEM_PLAQUE.get()
    ));
    public static final RegistryObject<BlockEntityType<BigDoorTile>> BIG_DOOR = TYPES.register("big_door", () -> create(
            BigDoorTile::new,
            BlockRegistry.BIG_DOOR.get()
    ));
    public static final RegistryObject<BlockEntityType<TardisLandingPadTile>> LANDING_PAD = TYPES.register("landing_pad", () -> create(
            TardisLandingPadTile::new,
            BlockRegistry.LANDING_PAD.get()
    ));

    public static final RegistryObject<BlockEntityType<MonitorVariableTile>> MONITOR_VARIABLE = TYPES.register("monitor_variable", () -> create(
            MonitorVariableTile::new,
            BlockRegistry.VARIABLE_MONITOR.get()
    ));



    public static final RegistryObject<BlockEntityType<RendererTestTile>> RENDERER_TEST = TYPES.register("renderer_test", () -> create(
            RendererTestTile::new, BlockRegistry.RENDERER_TEST.get()
    ));

    public static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<T> factory, Block... blocks){
        return BlockEntityType.Builder.of(factory, blocks).build(null);
    }

    public static Block[] collectAllMatching(Predicate<Block> blockPred){
        List<Block> blocks = new ArrayList<>();
        ForgeRegistries.BLOCKS.getValues().stream().filter(blockPred).forEach(blocks::add);
        return blocks.toArray(new Block[0]);
    }

}
