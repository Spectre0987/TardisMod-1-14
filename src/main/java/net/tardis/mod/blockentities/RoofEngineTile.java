package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class RoofEngineTile extends EngineTile{

    final AABB renderBox;

    public RoofEngineTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.ROOF_ENGINE.get(), pPos, pBlockState);
        this.renderBox = new AABB(0, -2, 0, 1, 1, 1).move(this.getBlockPos());
    }

    @Override
    public AABB getRenderBoundingBox() {
        return renderBox;
    }
}
