package net.tardis.mod.blockentities.consoles;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class NemoConsole extends ConsoleTile{
    public NemoConsole(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public NemoConsole(BlockPos pos, BlockState state) {
        super(TileRegistry.NEMO_CONSOLE.get(), pos, state);
    }
}
