package net.tardis.mod.blockentities.consoles;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.block.ConsoleBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ITextureVariantBlockEntity;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ConsoleTile extends BlockEntity implements ITextureVariantBlockEntity {

    public final AnimationState rotorAnimationState = new AnimationState();
    private List<UUID> controlEntities = new ArrayList<>();
    public final AABB renderBox = WorldHelper.getCenteredAABB(BlockPos.ZERO, 3);
    public Optional<ResourceLocation> texVariant = Optional.empty();

    public ConsoleTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public void clientTick(){

        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                //rotor animations
                if(tardis.isInVortex()){
                    this.rotorAnimationState.startIfStopped((int)tardis.getAnimationTicks());
                }
                else this.rotorAnimationState.stop();
            });
        }
    }

    public void addControl(ControlEntity control){
        this.controlEntities.add(control.getUUID());
    }

    public void killAllControls(){
        if(!level.isClientSide){
            for(UUID id : this.controlEntities){
                Entity e = ((ServerLevel)level).getEntity(id);
                if(e != null)
                    e.kill();
            }
        }
        this.controlEntities.clear();
    }

    @Override
    public void onLoad() {
        super.onLoad();

        if(level != null && !level.isClientSide){
            if(this.getBlockState().getBlock() instanceof ConsoleBlock console){
                console.clearPreexistingControls(this, level, getBlockPos());
                console.placeControls(console.getControlPositionData(this), this, this.level, this.getBlockPos());
            }
        }

    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, ConsoleTile pBlockEntity){
        pBlockEntity.clientTick();
    }

    @Override
    public void load(CompoundTag pTag) {
        super.load(pTag);

        if(pTag.contains("control_list")){
            this.controlEntities.clear();
            ListTag controls = pTag.getList("control_list", Tag.TAG_STRING);
            for(Tag tag : controls){
                UUID id = UUID.fromString(((StringTag)tag).getAsString());
                this.controlEntities.add(id);
            }
        }
        this.texVariant = Helper.readOptionalNBT(pTag, "tex_var", (t, n) -> new ResourceLocation(t.getString(n)));

    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);

        ListTag controlList = new ListTag();
        for(UUID id : this.controlEntities){
            controlList.add(StringTag.valueOf(id.toString()));
        }
        pTag.put("control_list", controlList);
        this.texVariant.ifPresent(tex -> pTag.putString("tex_var", tex.toString()));
    }

    @Override
    public AABB getRenderBoundingBox() {
        return renderBox.move(this.getBlockPos());
    }

    @Override
    public Optional<TextureVariant> getTextureVariant() {
        return this.texVariant.map(this.getLevel().registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS)::get);
    }

    @Override
    public void setTextureVariant(@Nullable TextureVariant variant) {
        if(variant != null){
            this.texVariant = this.getLevel().registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).getResourceKey(variant).map(ResourceKey::location);
        }
        else this.texVariant = Optional.empty();
        this.updateTextureVariant();
        this.setChanged();
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }
}
