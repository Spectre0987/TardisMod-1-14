package net.tardis.mod.blockentities.consoles;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class NouveauConsoleTile extends ConsoleTile{
    public NouveauConsoleTile(BlockPos pos, BlockState state) {
        super(TileRegistry.NOUVEAU_CONSOLE.get(), pos, state);
    }
}
