package net.tardis.mod.blockentities.consoles;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class GalvanicTile extends ConsoleTile{
    public GalvanicTile(BlockPos pos, BlockState state) {
        super(TileRegistry.GALVANIC_CONSOLE.get(), pos, state);
    }
}
