package net.tardis.mod.blockentities;

public interface IBreakLogic {

    void onBroken();
}
