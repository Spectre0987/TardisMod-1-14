package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.ars.RoomEntry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.misc.*;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.world.data.ARSRoomLevelData;

import java.util.Optional;

public class ARSPanelTile extends BlockEntity implements IAcceptTools {

    public static final int PROGRESS_FINISH_TICKS = 100;
    public static final Component NEEDS_WELD_TRANS = Component.translatable("tiles." + Tardis.MODID + ".ars_panel.needs_weld");

    private final ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(4);
    private final ToolHandler toolHandler = new ToolHandler();

    private BlockPos spawnPos;
    private ResourceLocation arsRoom;

    public final ContainerData containerData = new ContainerData() {
        @Override
        public int get(int i) {
            return toolHandler.getWorkFromTool(ToolType.WELDING);
        }

        @Override
        public void set(int i, int i1) {

        }

        @Override
        public int getCount() {
            return 1;
        }
    };


    public ARSPanelTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.ARS_PANEL.get(), pPos, pBlockState);
        this.inventory.onChange(slot -> this.setChanged());
    }


    @Override
    public void load(CompoundTag tag) {
        super.load(tag);

        this.inventory.deserializeNBT(tag.getCompound("inventory"));
        this.spawnPos = BlockPos.of(tag.getLong("spawn_pos"));
        this.arsRoom = new ResourceLocation(tag.getString("room"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("inventory", this.inventory.serializeNBT());
        if(this.spawnPos != null)
            tag.putLong("spawn_pos", this.spawnPos.asLong());
        if(this.arsRoom != null)
            tag.putString("room", this.arsRoom.toString());
    }

    public void onFixed(){
        //Unlock all extras for this room
        Capabilities.getCap(Capabilities.TARDIS, this.getLevel()).ifPresent(tardis -> {
            getRoom().ifPresent(room -> {
                //unlock extra
                for(ResourceKey<?> key : room.getExtraUnlocks()){
                    tardis.getUnlockHandler().unlock(key);
                }
            });
        });

        for(int i = 0; i < this.inventory.getSlots(); ++i){
            this.inventory.extractItem(i, 1, false);
        }

        //Place room
        if(level instanceof ServerLevel serverLevel){
            this.getRoom().ifPresent(room -> {

                //Unlock parent if this has one, or itself if no parent
                Capabilities.getCap(Capabilities.TARDIS, this.getLevel()).ifPresent(tardis -> {

                    //Create a parent resource key if this has a parent, else get this rooms resource key
                    ResourceKey<ARSRoom> unlockKey = room.getParent().map(rl -> ResourceKey.create(JsonRegistries.ARS_ROOM_REGISTRY, rl))
                            .or(() -> serverLevel.registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getResourceKey(room)).get();

                    tardis.getUnlockHandler().unlock(unlockKey);

                });

                room.getParent().ifPresent(parent -> {

                    final ARSRoom parentRoom = this.level.registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).get(parent);
                    final Optional<RoomEntry> entryHolder = ARSRoomLevelData.getData(serverLevel).getRoomFor(this.getBlockPos());
                    if(parentRoom != null && entryHolder.isPresent()){
                        //Spawns the parent room at the location of the original room - the original room's offset, then lets the new room do it's own offset
                        parentRoom.spawnRoom(serverLevel, entryHolder.get().start.below(room.getYOffset()), serverLevel.getStructureManager(), ARSRoom.DEFAULT_SETTINGS);
                    }
                });
            });
        }
    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    public Optional<ARSRoom> getRoom(){

        if(this.level instanceof ServerLevel level){
            Optional<RoomEntry> entryHolder = ARSRoomLevelData.getData(level).getRoomFor(this.getBlockPos());
            if(entryHolder.isPresent()){
                return level.registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getOptional(entryHolder.get().room);
            }
        }
        return Optional.empty();
    }

    public static boolean matchesRoomRepair(ARSPanelTile tile){
        for(int i = 0; i < 3; ++i){
            if(tile.getInventory().getStackInSlot(i).getItem() != ItemRegistry.DATA_CRYSTAL.get()){
                return false;
            }
        }
        return tile.getInventory().getStackInSlot(3).getItem() == ItemRegistry.FLUID_LINKS.get();
    }

    @Override
    public boolean doWork(Level level, Vec3 hit, ItemStack toolUsed) {

        if(matchesRoomRepair(this)){
            if(this.toolHandler.getWorkFromTool(ToolType.WELDING) >= PROGRESS_FINISH_TICKS){
                this.onFixed();
            }
            return this.toolHandler.doToolWork(level, hit, toolUsed);
        }

        return false;
    }

    /**
     * Checks if this is ready for welding, and notifies player
     * @param pPlayer
     */
    public void displayMissingWork(Player pPlayer) {
        if(matchesRoomRepair(this) && this.toolHandler.getWorkFromTool(ToolType.WELDING) < PROGRESS_FINISH_TICKS){
            pPlayer.sendSystemMessage(NEEDS_WELD_TRANS);
        }
    }
}
