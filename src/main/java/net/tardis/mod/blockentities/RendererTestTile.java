package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class RendererTestTile extends BlockEntity {
    public RendererTestTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public RendererTestTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.RENDERER_TEST.get(), pPos, pBlockState);
    }


}
