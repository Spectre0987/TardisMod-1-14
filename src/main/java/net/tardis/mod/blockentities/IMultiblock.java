package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;

import java.util.Optional;

public interface IMultiblock<T> {

    /**
     *
     * @return gets the offset from this block's position to the master tile
     */
    BlockPos getMasterPos();
    void setMasterPos(BlockPos masterPos);

}
