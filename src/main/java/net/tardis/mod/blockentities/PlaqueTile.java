package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

public class PlaqueTile extends BlockEntity implements IBreakLogic{

    public ItemStack stack = ItemStack.EMPTY;

    public PlaqueTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public PlaqueTile(BlockPos pPos, BlockState pBlockState) {
        this(TileRegistry.ITEM_PLAQUE.get(), pPos, pBlockState);
    }

    public ItemStack setItem(ItemStack stack){
        this.setChanged();
        final ItemStack oldStack = this.stack;
        this.stack = stack;
        return oldStack;
    }

    public ItemStack getItem(){
        return this.stack;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.stack = ItemStack.of(tag.getCompound("item_held"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("item_held", this.stack.serializeNBT());
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void onBroken() {
        if(!getLevel().isClientSide() && !this.getItem().isEmpty()){
            final Vec3 center = WorldHelper.centerOfBlockPos(this.getBlockPos(), true);
            getLevel().addFreshEntity(new ItemEntity(getLevel(), center.x(), center.y(), center.z(), getItem()));
        }
    }
}
