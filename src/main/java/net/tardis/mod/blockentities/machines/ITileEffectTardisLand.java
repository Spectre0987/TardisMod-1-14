package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.tardis.mod.cap.level.ITardisLevel;

public interface ITileEffectTardisLand<T extends BlockEntity> {

    boolean canLandInArea(ITardisLevel tardis);
    int getRange();

    /**
     *
     * @param tardis
     * @param dest TARDIS will absolutely land here if this doesn't function change it
     * @return
     */
    BlockPos modifyLandingPos(ITardisLevel tardis, BlockPos dest);

    default boolean isInRange(BlockPos pos){
        return ((BlockEntity)this).getBlockPos().distSqr(pos) <= (getRange() * getRange());
    }

}
