package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.level.ITardisLevel;

public class TardisLandingPadTile extends BlockEntity implements ITileEffectTardisLand {
    public TardisLandingPadTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public TardisLandingPadTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.LANDING_PAD.get(), pPos, pBlockState);
    }

    @Override
    public boolean canLandInArea(ITardisLevel tardis) {
        return true;
    }

    @Override
    public int getRange() {
        return 32;
    }

    @Override
    public BlockPos modifyLandingPos(ITardisLevel tardis, BlockPos dest) {
        if(getLevel().getBlockState(this.getBlockPos().above()).canBeReplaced())
            return this.getBlockPos().above();
        return dest;
    }
}
