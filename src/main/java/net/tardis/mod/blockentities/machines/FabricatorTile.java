package net.tardis.mod.blockentities.machines;

import com.google.common.collect.Lists;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.CraftingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RangedWrapper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.quantiscope_settings.CraftQuantiscopeSetting;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.misc.SimpleHandlerContainerWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.FabricatorSetCraftingItemMessage;
import net.tardis.mod.recipes.ItemStackHandlerContinerWrapper;
import net.tardis.mod.recipes.QuantiscopeCraftingRecipe;
import net.tardis.mod.recipes.RecipeRegistry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class FabricatorTile extends BlockEntity implements IMultiblockMaster {

    public static final AABB RENDER_BOX = new AABB(0, 0, 0, 1, 3, 1);

    MultiblockData data;
    public AnimationState craftingAnimState = new AnimationState();
    private final ItemStackHandler craftingInv = new ItemStackHandlerWithListener(7)
            .onChange(s -> this.setChanged());

    final RangedWrapper resultInvWrapper = new RangedWrapper(
            this.craftingInv, 6, 7
    );

    protected final LazyOptional<IItemHandler> craftingInvHolder = LazyOptional.of(() -> this.craftingInv);
    protected final LazyOptional<IItemHandler> resultInvHolder = LazyOptional.of(() -> resultInvWrapper);

    protected Optional<Item> craftingItem = Optional.empty();
    protected Optional<? extends Recipe<?>> validRecipe = Optional.empty();

    final SimpleHandlerContainerWrapper wrapper = new SimpleHandlerContainerWrapper(this.craftingInv, this::setChanged);

    public FabricatorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public FabricatorTile(BlockPos pPos, BlockState pBlockState) {
        this(TileRegistry.FABRICATOR_MACHINE.get(), pPos, pBlockState);
    }

    @Override
    public void setMasterData(MultiblockData data) {
        this.data = data;
    }

    @Override
    public List<BlockPos> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public MultiblockData getMultiblockData() {
        return this.data;
    }

    public void setCraftingItem(@Nullable Item item){
        this.craftingItem = Helper.nullableToOptional(item);
        this.setChanged();
        if(getLevel() != null && !getLevel().isClientSide()){
            Network.sendToTracking(this, new FabricatorSetCraftingItemMessage(this.getBlockPos(), this.craftingItem));
        }
    }

    public Optional<Item> getCraftingItem(){
        return this.craftingItem;
    }

    public void serverTick(){
        findRecipe();

        validRecipe.ifPresent(r -> {
            if(allMatch(r)){
                for(Ingredient i : r.getIngredients()){
                    this.removeForIngredient(i);
                }
                this.craftingInv.insertItem(6, r.getResultItem(level.registryAccess()), false);
            }
        });
    }

    public boolean removeForIngredient(Ingredient ing){
        for(int i = 0; i < 6; ++i){
            if(ing.test(craftingInv.getStackInSlot(i))){
                craftingInv.extractItem(i, 1, false);
                return true;
            }
        }
        return false;
    }

    public boolean allMatch(Recipe<?> r){
        NonNullList<Ingredient> required = NonNullList.create();
        r.getIngredients().stream().filter(i -> !i.isEmpty()).forEach(required::add);

        HashMap<Integer, AtomicInteger> used = new HashMap<>();
        for(Ingredient i : required){
            boolean found = false;
            for(int index = 0; index < 6; ++index){
                if(i.test(craftingInv.getStackInSlot(index))){
                    found = true;
                    used.computeIfAbsent(index, ii -> new AtomicInteger()).incrementAndGet();
                }
            }
            //If any ingredient used to many times
            for(int slot : used.keySet()){
                if(craftingInv.getStackInSlot(slot).getCount() < used.get(slot).get()){
                    return false;
                }
            }
            //If no matching ingredient is found
            if(!found)
                return false;
        }
        return true;
    }

    public void findRecipe(){

        if(this.craftingItem.isEmpty()){
            if(this.validRecipe.isPresent())
                this.validRecipe = Optional.empty();
            return;
        }

        final List<Recipe<?>> recipes = new ArrayList<>();
        recipes.addAll(level.getRecipeManager().getAllRecipesFor(RecipeRegistry.QUANTISCOPE_CRAFTING_TYPE));
        recipes.addAll(level.getRecipeManager().getAllRecipesFor(RecipeType.CRAFTING));

        this.validRecipe = recipes.stream()
                .filter(r -> r.getResultItem(level.registryAccess()).getItem() == this.craftingItem.get())
                //.filter(r -> r.matches(this.wrapper, level))
                .findFirst();
    }


    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(ForgeCapabilities.ITEM_HANDLER == cap){
            if(this.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING).getCounterClockWise() != side && side != Direction.DOWN){
                return this.craftingInvHolder.cast();
            }
            return this.resultInvHolder.cast();
        }
        return super.getCapability(cap, side);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.craftingInv.deserializeNBT(tag.getCompound("inv"));
        this.craftingItem = Helper.readRegistryFromString(tag, ForgeRegistries.ITEMS, "crafting_item");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("inv", this.craftingInv.serializeNBT());
        getCraftingItem().ifPresent(item -> Helper.writeRegistryToNBT(tag, ForgeRegistries.ITEMS, item, "crafting_item"));
    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
        super.onDataPacket(net, pkt);
        //this.deserializeNBT(pkt.getTag());
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public AABB getRenderBoundingBox() {
        return RENDER_BOX.move(this.getBlockPos());
    }
}
