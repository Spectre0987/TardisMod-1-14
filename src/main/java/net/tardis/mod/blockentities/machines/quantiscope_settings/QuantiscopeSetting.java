package net.tardis.mod.blockentities.machines.quantiscope_settings;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.MenuConstructor;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.quantiscope.SonicUpgradeQuantiscopeMenu;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.BiFunction;

public abstract class QuantiscopeSetting implements INBTSerializable<CompoundTag> {
    public static HashMap<ResourceLocation, BiFunction<ResourceLocation, QuantiscopeTile, ? extends QuantiscopeSetting>> ALL_SETTINGS = new HashMap<>();

    static{
        register(Helper.createRL("craft"), CraftQuantiscopeSetting::new);
        register(Helper.createRL("sonic"), SonicQuantiscopeSetting::new);
        register(Helper.createRL("sonic_upgrade"), SonicUpgradeQuantiscopeSetting::new);
    }

    final ResourceLocation id;
    final QuantiscopeTile parent;
    private Component title;

    public QuantiscopeSetting(ResourceLocation id, QuantiscopeTile tile){
        this.id = id;
        this.parent = tile;
    }

    public abstract void tick();
    public abstract Optional<MenuConstructor> getMenu();
    public abstract Optional<ItemStackHandler> getInventory();

    public ContainerData getContainerData(){
        return null;
    }

    public QuantiscopeTile getParent(){
        return this.parent;
    }

    public ResourceLocation getId(){
        return this.id;
    }
    public Component getTitle(){
        if(this.title == null){
            this.title = createTitle(this.getId());
        }
        return this.title;
    }

    public static Component createTitle(ResourceLocation loc){
        return Component.translatable("quantiscope." + loc.getNamespace() + ".setting." + loc.getPath().replace('/', '.'));
    }

    //Static stuff

    public static void register(ResourceLocation id, BiFunction<ResourceLocation, QuantiscopeTile, QuantiscopeSetting> setting){
        ALL_SETTINGS.put(id, setting);
    }

    public static QuantiscopeSetting create(ResourceLocation id, QuantiscopeTile parent){
        return ALL_SETTINGS.get(id).apply(id, parent);
    }


}
