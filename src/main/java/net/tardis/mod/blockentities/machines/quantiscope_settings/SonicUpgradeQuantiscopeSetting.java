package net.tardis.mod.blockentities.machines.quantiscope_settings;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.MenuConstructor;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.jarjar.nio.util.Lazy;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.menu.quantiscope.SonicUpgradeQuantiscopeMenu;
import net.tardis.mod.misc.ItemStackHandlerWithListener;

import java.util.Optional;

public class SonicUpgradeQuantiscopeSetting extends QuantiscopeSetting{

    final ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(7);

    public SonicUpgradeQuantiscopeSetting(ResourceLocation id, QuantiscopeTile tile) {
        super(id, tile);
        inventory.onChange(slot -> {
            if(slot == 6){ //Sonic slot
                LazyOptional<ISonicCapability> sonicHolder = inventory.getStackInSlot(slot).getCapability(Capabilities.SONIC);
                if(sonicHolder.isPresent()){ //If this slot holds a sonic
                    ISonicCapability sonic = sonicHolder.orElseThrow(NullPointerException::new);
                    for(int i = 0; i < 6; ++i){
                        ItemStack sonicItem = sonic.getUpgradeInv().extractItem(i, 1, false);
                        if(!sonicItem.isEmpty())
                            this.inventory.insertItem(i, sonicItem, false);
                    }
                }
            }
        });
    }

    @Override
    public void tick() {

    }

    @Override
    public Optional<MenuConstructor> getMenu() {
        return Optional.of((winId, inv, player) -> new SonicUpgradeQuantiscopeMenu(winId, inv, this));
    }

    @Override
    public Optional<ItemStackHandler> getInventory() {
        return Optional.of(this.inventory);
    }

    @Override
    public CompoundTag serializeNBT() {
        return this.inventory.serializeNBT();
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.inventory.deserializeNBT(tag);
    }
}
