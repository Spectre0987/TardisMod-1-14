package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.chunks.ITardisChunkCap;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.tiles.TileRedirectInventoryCapability;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TemporalScoopTile extends BlockEntity {

    final TileRedirectInventoryCapability<TemporalScoopTile> cap;
    final LazyOptional<IItemHandler> capHolder;

    public TemporalScoopTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
        this.cap = new TileRedirectInventoryCapability<>(this, tile -> {

            final LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, tile.getLevel());
            if(!tardisHolder.isPresent()){
                return new ItemStackHandler(0);
            }

            return tardisHolder.orElseThrow(NullPointerException::new).getInteriorManager().getTemporalScoopInventory();

        });
        this.capHolder = LazyOptional.of(() -> cap);
    }

    public TemporalScoopTile(BlockPos pos, BlockState state){
        this(TileRegistry.TEMPORAL_SCOOP.get(), pos, state);
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {

        if(cap == ForgeCapabilities.ITEM_HANDLER)
            return this.capHolder.cast();

        return super.getCapability(cap, side);
    }
}
