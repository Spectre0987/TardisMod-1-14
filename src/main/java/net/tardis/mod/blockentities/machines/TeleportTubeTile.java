package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ChunkLoader;
import net.tardis.mod.misc.tardis.TubeEntry;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TeleportTubeEntityMessage;
import net.tardis.mod.sound.SoundRegistry;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class TeleportTubeTile extends BlockEntity {

    UUID teleportId;
    AABB teleportBox;
    LazyOptional<ITardisLevel> tardis;

    Entity teleportingEntity;
    int teleportingEntityTicks = 0;
    public final HashMap<UUID, Long> recentArrivals = new HashMap<>();

    public TeleportTubeTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public TeleportTubeTile(BlockPos pos, BlockState state){
        this(TileRegistry.TELEPORT_TUBE.get(), pos, state);
    }

    @Override
    public void saveToItem(ItemStack stack) {
        super.saveToItem(stack);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("teleport_id"))
            this.teleportId = UUID.fromString(tag.getString("teleport_id"));
    }

    public AABB getTeleportBox(){
        if(this.teleportBox == null)
            this.teleportBox = new AABB(0.1, 0, 0.1, 0.9, 2, 0.9)
                    .move(this.getBlockPos());
        return this.teleportBox;
    }

    /**
     *
     * @return the TARDIS this is in, cached
     */
    public LazyOptional<ITardisLevel> getTardis(){
        if(this.tardis == null){
            this.tardis = Capabilities.getCap(Capabilities.TARDIS, this.getLevel());
        }
        return this.tardis;
    }

    public void setTeleportingEntity(@Nullable Entity entity){
        if(entity == null){
            this.teleportingEntity = null;
            this.teleportingEntityTicks = 0;
        }
        else {
            this.teleportingEntity = entity;
            //Load the chunk of the other teleporter
            if(level instanceof ServerLevel server){
                this.getTargetPos(Capabilities.getCap(Capabilities.TARDIS, level)).ifPresent(pos -> {
                    final ChunkPos cPos = new ChunkPos(pos);
                    server.getChunkSource().addRegionTicket(ChunkLoader.TELEPORT_END_POINT, cPos, 1, cPos, true);
                    server.getChunkSource().getChunkFuture(cPos.x, cPos.z, ChunkStatus.FULL, false).thenAccept(either -> {
                        either.left().ifPresent(ca -> {
                            Tardis.LOGGER.debug("Chunk loaded!!!");
                            ca.getBlockEntity(pos, TileRegistry.TELEPORT_TUBE.get()).ifPresent(otherTube -> otherTube.addRecentArrivial(entity.getUUID()));
                        });
                    });

                });
            }
        }
        if(!getLevel().isClientSide()) {
            Network.sendToTracking(this, new TeleportTubeEntityMessage(this.getBlockPos(), entity != null ? entity.getId() : -1));
        }
    }

    private void addRecentArrivial(UUID uuid) {
        this.recentArrivals.put(uuid, level.getGameTime() + (8 * 20));
    }


    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(this.teleportId != null)
            tag.putString("teleport_id", this.teleportId.toString());
    }

    public boolean teleportEntity(Entity target){
        final Optional<BlockPos> posHolder = getTargetPos();
        if(posHolder.isPresent()){
            if(!getLevel().isClientSide()){
                final Vec3 position = WorldHelper.centerOfBlockPos(posHolder.get());
                target.teleportTo(position.x(), position.y(), position.z());
            }
            return true;
        }
        return false;
    }

    public void serverTick(){

        //Remove teleported entities that are expired
        this.recentArrivals.entrySet().removeIf(entry -> entry.getValue() < level.getGameTime());

        this.getTardis().ifPresent(tardis -> {
            this.getTeleportId().ifPresent(id -> {
                List<Entity> entitiesIn = getLevel().getEntitiesOfClass(Entity.class, this.getTeleportBox());
                //Do not teleport recent arrivals
                entitiesIn.removeIf(e -> this.recentArrivals.containsKey(e.getUUID()));
                //If we're teleporting an entity that is no longer in this hit box, stop
                if(this.teleportingEntity != null && !entitiesIn.contains(this.teleportingEntity)){
                    this.setTeleportingEntity(null);
                    return;
                }

                //If we don't have an entity to teleport, find one
                if(teleportingEntity == null){
                    if(entitiesIn.size() > 0)
                        this.setTeleportingEntity(entitiesIn.get(0));
                    return;
                }

                ++this.teleportingEntityTicks;
                //Move up
                if(this.teleportingEntityTicks > 30){
                    this.teleportingEntity.setDeltaMovement(this.teleportingEntity.getDeltaMovement().add(0, 0.1, 0));
                }
                if(this.teleportingEntityTicks == 20)
                    getLevel().playSound(null, this.getBlockPos(), SoundRegistry.TUBE.get(), SoundSource.BLOCKS, 1.0F, 1.0F);

                if(this.teleportingEntityTicks > 60){
                    this.teleportEntity(this.teleportingEntity);
                    this.setTeleportingEntity(null);
                }
            });
        });
    }

    public void clientTick(){

        if(this.teleportingEntity != null){
            ++this.teleportingEntityTicks;

            int angle = getLevel().getRandom().nextInt(360);
            Vec3 pos = WorldHelper.centerOfBlockPos(getBlockPos(), true)
                    .add(Mth.sin((float)Math.toRadians(angle)) * 0.5, 0, Math.cos(Math.toRadians(angle)) * 0.5);
            getLevel().addParticle(ParticleTypes.CLOUD, pos.x(), pos.y(), pos.z, 0, 0.1, 0);

            if(this.teleportingEntityTicks > 50){
                this.teleportingEntity.setDeltaMovement(this.teleportingEntity.getDeltaMovement().add(0, .3, 0));
            }
        }
        else this.teleportingEntityTicks = 0;

    }

    public Optional<UUID> getTeleportId(){
        return Helper.nullableToOptional(this.teleportId);
    }

    public Optional<BlockPos> getTargetPos(){
        return getTargetPos(Capabilities.getCap(Capabilities.TARDIS, getLevel()));
    }

    public Optional<BlockPos> getTargetPos(LazyOptional<ITardisLevel> tardisHolder){
        if(tardisHolder.isPresent() && this.teleportId != null){
            final ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
            final Optional<TubeEntry> tubeHolder = tardis.getInteriorManager().getTubeEntry(this.teleportId);
            if(tubeHolder.isPresent()){
                final TubeEntry tube = tubeHolder.get();

                if(tube.getFirstPosition().isPresent() && !this.getBlockPos().equals(tube.getFirstPosition().get()))
                    return tube.getFirstPosition();

                if(tube.getSecondPos().isPresent() && !this.getBlockPos().equals(tube.getSecondPos().get()))
                    return tube.getSecondPos();

                return Optional.empty();
            }
        }
        return Optional.empty();
    }
}
