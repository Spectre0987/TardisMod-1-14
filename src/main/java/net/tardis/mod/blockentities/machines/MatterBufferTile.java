package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MatterBufferUpdateInvMessage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class MatterBufferTile extends BlockEntity implements IBreakLogic {

    private final ItemStackHandlerWithListener inventory;
    private final LazyOptional<IItemHandler> invHolder;
    private int progressTicks = 0;

    public MatterBufferTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
        this.inventory = new ItemStackHandlerWithListener(1)
                .onChange(slot -> {
                    if(!level.isClientSide())
                        Network.sendToTracking(this, new MatterBufferUpdateInvMessage(this.getBlockPos(), this.getInventory()));
                });
        this.invHolder = LazyOptional.of(this::getInventory);
    }

    public MatterBufferTile(BlockPos pos, BlockState state){
        this(TileRegistry.MATTER_BUFFER.get(), pos, state);
    }

    public void tick(){

        final ItemStack stack = this.inventory.getStackInSlot(0);
        if(!stack.isEmpty()){
            ++progressTicks;
            if(this.progressTicks >= 200){
                this.progressTicks = 0;
                Capabilities.getCap(Capabilities.TARDIS, this.level).ifPresent(tardis -> {
                    tardis.getInteriorManager().modMatterBuffer(stack.getCount());
                });
                this.inventory.setStackInSlot(0, ItemStack.EMPTY);
            }
        }
        else progressTicks = 0;

    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("inventory", this.inventory.serializeNBT());
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
    }

    @Override
    public void onBroken() {
        WorldHelper.dropItems(this.level, this.getBlockPos(), this.inventory);
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(cap == ForgeCapabilities.ITEM_HANDLER)
            return this.invHolder.cast();
        return super.getCapability(cap, side);
    }
}
