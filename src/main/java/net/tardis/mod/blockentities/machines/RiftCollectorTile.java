package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.api.artron.IArtronStorage;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.chunks.ChunkCapability;
import net.tardis.mod.cap.chunks.IChunkCap;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.RiftCollectorItemMessage;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

/**
 * Used for charging items from rifts, or vise versa
 */
public class RiftCollectorTile extends BlockEntity implements IArtronStorage, IBreakLogic {

    public static final float MAX_ARTRON = 512;
    public static final int TIME_TO_BURN = 80; //4 Seconds

    private float artron;
    private ItemStackHandler inventory = new ItemStackHandlerWithListener(1)
            .onChange(s -> {
                this.setChanged();
                this.sendUpdate();
            });
    private long lastBurnedItem = 0;
    private Optional<Rift> currentRift = Optional.empty();
    private boolean safiesEngaged = true;

    public RiftCollectorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public RiftCollectorTile(BlockPos pos, BlockState state){
        this(TileRegistry.RIFT_COLLECTOR.get(), pos, state);
    }

    //Only called on server
    public void tick(){

        if((this.currentRift = this.getCurrentRift()).isEmpty())
            return;

        final Rift rift = this.currentRift.get();

        //If the current item can be burned for artron, try to fill the rift with this item
        final float stackArtronVal = this.getCurrentItemArtronValue();
        if(stackArtronVal > 0){

            //If the rift is as full as it can get naturally and we have the safety engaged, stop
            if(rift.isNaturallyFull() && this.safiesEngaged){
                return;
            }

            //Otherwise, try to fill the rift

            float totalTaken = rift.fillArtron(stackArtronVal, false);

            //If the rift accepted any of this item's artron, consume it
            if(totalTaken > 0){
                this.lastBurnedItem = getLevel().getGameTime() ;//
                this.inventory.getStackInSlot(0).shrink(1);
                this.setChanged();
                this.sendUpdate();
            }
            return;
        }
        //Fill battery items from interal buffer
        this.getInventory().getStackInSlot(0).getCapability(Capabilities.ARTRON_TANK_ITEM).ifPresent(item -> {
            float accepted = item.fillArtron(Math.min(this.artron, 1.0F), true);
            if(accepted > 0){
                item.fillArtron(this.takeArtron(accepted, false), false);
            }
        });
    }

    public void sendUpdate(){
        if(this.level != null && !this.getLevel().isClientSide()){
            Network.sendToTracking(this, new RiftCollectorItemMessage(this.getBlockPos(), this.getInventory()));
        }
    }

    public Optional<Rift> getCurrentRift(){
        //If we have a non-closed rift cached, return that
        if(this.currentRift.isPresent() && !this.currentRift.get().isClosed())
            return this.currentRift;

        //Find closest Rift otherwise

        double dist = Integer.MAX_VALUE;
        Rift rift = null;

        for(ChunkPos pos : ChunkPos.rangeClosed(new ChunkPos(this.getBlockPos()), 3).toList()){
            final LazyOptional<IChunkCap> capHolder = this.getLevel().getChunk(pos.x, pos.z).getCapability(Capabilities.CHUNK);
            if(capHolder.isPresent()){
                Optional<Rift> r = capHolder.orElseThrow(NullPointerException::new).getRift();
                if(r.isPresent()){
                    double nextDist = r.get().getWorldPos().distSqr(this.getBlockPos());
                    if(nextDist < dist){
                        dist = nextDist;
                        rift = r.get();
                    }
                }
            }
        }
        return rift != null ? Optional.of(rift) : Optional.empty();
    }

    /**
     *
     * @param stack Stack to add to the CollectorTile
     * @return The ItemStack already held by this tile
     */
    public ItemStack setItemStack(ItemStack stack){
        final ItemStack heldStack = this.getInventory().getStackInSlot(0);
        this.inventory.setStackInSlot(0, stack.copy());
        this.setChanged();
        return heldStack;
    }

    public ItemStack getItemStack(){
        return this.inventory.getStackInSlot(0);
    }

    public void setSafety(boolean on){
        this.safiesEngaged = on;
        this.setChanged();
    }

    public boolean areSafetiesEngaged(){
        return this.safiesEngaged;
    }

    /**
     *
     * @return If the current item is consumable for artron
     */
    public float getCurrentItemArtronValue(){
        return ItemArtronValueReloader.ArtronValue.getArtronValue(this.inventory.getStackInSlot(0));
    }

    @Override
    public void load(CompoundTag pTag) {
        super.load(pTag);
        this.artron = pTag.getFloat("artron");
        this.inventory.deserializeNBT(pTag.getCompound("inventory"));
        this.safiesEngaged = pTag.getBoolean("safeties");
    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);
        pTag.putFloat("artron", this.artron);
        pTag.put("inventory", this.inventory.serializeNBT());
        pTag.putBoolean("safeties", this.safiesEngaged);
    }

    @Override
    public float fillArtron(float artronToFill, boolean simulate) {

        final float room = MAX_ARTRON - this.getStoredArtron();
        final float amtToFill = artronToFill > room ? room : artronToFill;

        if(!simulate){
            this.artron += amtToFill;
            this.setChanged();
        }

        return amtToFill;
    }

    @Override
    public float takeArtron(float artronToRecieve, boolean simulate) {

        final float taken = artronToRecieve > artron ? artron : artronToRecieve;

        if(!simulate){
            this.artron -= taken;
            this.setChanged();
        }

        return taken;
    }

    @Override
    public float getStoredArtron() {
        return this.artron;
    }

    @Override
    public float getMaxArtron() {
        return MAX_ARTRON;
    }

    /**
     *  Slot 0 - fuel / battery item
     * @return This tile's inventory
     */
    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Override
    public void onBroken() {
        WorldHelper.dropItems(level, this.getBlockPos(), this.inventory);
    }

    public boolean shouldAcceptArtron(){
        return this.getInventory().getStackInSlot(0).getCapability(Capabilities.ARTRON_TANK_ITEM).isPresent();
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }
}
