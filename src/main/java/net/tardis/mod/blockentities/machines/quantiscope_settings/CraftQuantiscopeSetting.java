package net.tardis.mod.blockentities.machines.quantiscope_settings;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.MenuConstructor;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.menu.quantiscope.CraftingQuantiscopeMenu;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.recipes.ItemStackHandlerContinerWrapper;
import net.tardis.mod.recipes.QuantiscopeCraftingRecipe;
import net.tardis.mod.recipes.RecipeRegistry;
import net.tardis.mod.sound.SoundRegistry;

import java.util.Optional;

public class CraftQuantiscopeSetting extends QuantiscopeSetting{

    private int progress = 0;
    private QuantiscopeCraftingRecipe currentRecipe;
    private ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(7);
    private ItemStackHandlerContinerWrapper<CraftQuantiscopeSetting> container;

    private ContainerData data = new ContainerData() {
        @Override
        public int get(int i) {
            return progress;
        }

        @Override
        public void set(int i, int i1) {
            progress = i1;
        }

        @Override
        public int getCount() {
            return 1;
        }
    };

    public CraftQuantiscopeSetting(ResourceLocation id, QuantiscopeTile tile) {
        super(id, tile);
        inventory.onChange(s -> tile.setChanged());
        this.container = new ItemStackHandlerContinerWrapper<>(inventory, this, t -> t.getParent().setChanged());

    }

    @Override
    public void tick() {

        findRecipeIfMissing();

        if(this.currentRecipe != null){
            --this.progress;

            if(progress % 60 == 0){
                getParent().getLevel().playSound(null, this.getParent().getBlockPos(), SoundRegistry.ELECTRIC_SPARK.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
            }

            if(progress <= 0){
                ItemStack result = this.currentRecipe.assemble(this.container, getParent().getLevel().registryAccess());
                if(!getParent().getLevel().players().isEmpty())
                    result.onCraftedBy(this.parent.getLevel(), this.parent.getLevel().players().get(0), 1);
                if(!result.isEmpty()){
                    this.inventory.insertItem(6, result, false);
                }
                this.currentRecipe = null;
            }

        }

    }

    public void findRecipeIfMissing(){

        if(this.currentRecipe != null && this.currentRecipe.matches(container, parent.getLevel())){
            return;
        }

        for(QuantiscopeCraftingRecipe recipe : getParent().getLevel().getRecipeManager().getAllRecipesFor(RecipeRegistry.QUANTISCOPE_CRAFTING_TYPE)){
            if(recipe.matches(this.container, getParent().getLevel())){
                this.progress = 200;
                this.currentRecipe = recipe;
                return;
            }
        }
        this.currentRecipe = null;
    }

    @Override
    public Optional<MenuConstructor> getMenu() {
        return Optional.of((windowId, inv, player) -> new CraftingQuantiscopeMenu(windowId, inv, this));
    }

    @Override
    public Optional<ItemStackHandler> getInventory() {
        return Optional.of(this.inventory);
    }

    public int getCraftingTicksLeft(){
        return this.progress;
    }

    @Override
    public ContainerData getContainerData() {
        return this.data;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.put("inventory", this.inventory.serializeNBT());
        tag.putInt("progress", this.progress);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
        this.progress = tag.getInt("progress");
    }
}
