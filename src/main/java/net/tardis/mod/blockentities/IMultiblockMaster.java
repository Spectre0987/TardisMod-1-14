package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IMultiblockMaster {

    void setMasterData(MultiblockData data);

    List<BlockPos> getChildren();

    MultiblockData getMultiblockData();

    default @NotNull <T> LazyOptional<T> getRedirectedCap(Capability<T> cap, @Nullable Direction side){
        return LazyOptional.empty();
    }
}
