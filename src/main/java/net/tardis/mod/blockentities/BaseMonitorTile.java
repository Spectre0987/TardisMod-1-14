package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.monitor_world_text.MonitorEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BaseMonitorTile extends BlockEntity {

    public List<Component> monitorText = new ArrayList<>();

    public BaseMonitorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public BaseMonitorTile(BlockPos pos, BlockState state){
        this(TileRegistry.BASIC_MONITOR.get(), pos, state);
    }

    public void clientTick(){

        if(this.level != null){
            this.level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

                this.monitorText = gatherWorldText(tardis);

            });
        }

    }

    public boolean displayText(){
        return true;
    }

    public static List<Component> gatherWorldText(ITardisLevel tardis){
        final List<Component> text = new ArrayList<>();

        //If overrides are present
        final Optional<MonitorEntry> masterOverrideEntry = MonitorEntry.ENTRIES.stream().filter(e ->
                        e.shouldDisplay(tardis) && e.getOverrideType() == MonitorEntry.OverrideType.ALL)
                .sorted((e1, e2) -> e1.overridePriority() > e2.overridePriority() ? 1 : -1)
                .findFirst();

        if(masterOverrideEntry.isPresent()){
            text.addAll(masterOverrideEntry.get().getText(tardis)); //only display the override with the highest priority
            return text;
        }

        //select overrides that can be on the screen with other shared overrides
        List<MonitorEntry> sharedOverrides = MonitorEntry.ENTRIES.stream()
                .filter(e -> e.shouldDisplay(tardis) && e.getOverrideType() == MonitorEntry.OverrideType.SHARED)
                .toList();
        if(!sharedOverrides.isEmpty()){
            for(MonitorEntry e : sharedOverrides){
                text.addAll(e.getText(tardis));
            }
            return text;
        }

        //Normal Entries
        for(MonitorEntry entry : MonitorEntry.ENTRIES){
            if(entry.shouldDisplay(tardis)){
                text.addAll(entry.getText(tardis));
            }
        }
        return text;
    }

    public int getTextColor(){
        return 0xFFFFFF;
    }

    public boolean shouldTextGlow(){
        return false;
    }

    public float[] getBounds(){
        return new float[]{9.5F / 16.0F, 0.6F};
    }


}
