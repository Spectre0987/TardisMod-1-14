package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;

import java.util.ArrayList;
import java.util.List;

public class BigDoorTile extends BlockEntity implements IMultiblockMaster {

    private MultiblockData data = MultiblockDatas.BIG_DOOR;
    private List<BlockPos> children = new ArrayList<>();
    public AnimationState openingState = new AnimationState();
    public AnimationState closingState = new AnimationState();

    public BigDoorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public BigDoorTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.BIG_DOOR.get(), pPos, pBlockState);
    }

    @Override
    public void load(CompoundTag pTag) {
        super.load(pTag);
    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);
    }

    @Override
    public void setMasterData(MultiblockData data) {
        this.data = data;
        this.setChanged();
    }

    public void startAnimation(boolean opening){
        this.openingState.start((int)getLevel().getGameTime() + 1);
        this.closingState.start((int)getLevel().getGameTime() + 1);
    }

    @Override
    public List<BlockPos> getChildren() {
        return this.children;
    }

    @Override
    public MultiblockData getMultiblockData() {
        return this.data;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        this.startAnimation(false);
    }
}
