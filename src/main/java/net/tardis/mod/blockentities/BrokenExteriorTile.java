package net.tardis.mod.blockentities;

import com.mojang.datafixers.util.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.dimension.DimensionHelper;
import net.tardis.mod.emotional.EmotionalHandler;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.ExteriorRegistry;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.resource_listener.server.TraitListener;

import java.util.*;

public class BrokenExteriorTile extends BlockEntity implements IMultiblockMaster {

    public static final int STARTING_LOYALTY = -100; // When it's in it's broken state, it hates everything until you gain it's trust
    public Optional<ExteriorType> baseExterior = Optional.empty();
    private final List<Trait> traits = new ArrayList<>();
    private MultiblockData multiblockData;

    public final Map<UUID, Integer> loyalties = new HashMap<>();


    public BrokenExteriorTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public BrokenExteriorTile(BlockPos pPos, BlockState pBlockState) {
        this(TileRegistry.BROKEN_EXTERIOR.get(), pPos, pBlockState);
    }

    public void setExterior(ExteriorType exterior){
        this.baseExterior = Optional.of(exterior);
    }

    public Optional<ExteriorType> getExterior(){
        return this.baseExterior;
    }

    /**
     *
     * @param player
     * @param stack
     * @param simulate
     * @return - amount added, or would be if simulate is false
     */
    public int unlockWith(Player player, ItemStack stack, boolean simulate){
        //Create a new list with defaults + trait added unlock items
        List<Pair<Integer, Ingredient>> unlock = createUnlockItemList();

        //Check the list
        for(Pair<Integer, Ingredient> entry : unlock){
            if(entry.getSecond().test(stack)){
                if(!simulate){
                    stack.shrink(1);
                    modLoyalty(player, entry.getFirst());
                }
                return entry.getFirst();
            }
        }
        return 0;
    }

    public List<Pair<Integer, Ingredient>> createUnlockItemList(){
        List<Pair<Integer, Ingredient>> unlock = new ArrayList<>();
        this.getTraits().stream().forEach(t -> {
            t.getLikedItems().forEach(like -> {

                Ingredient ing;

                if(like.getFirst().isTag()){
                    ing = Ingredient.of(like.getFirst().getTag());
                }
                else ing = Ingredient.of(like.getFirst().getObject());

                if(ing != null && !ing.isEmpty())
                    unlock.add(new Pair<>(like.getSecond(), ing));
            });
        });
        return unlock;
    }

    public void modLoyalty(Player player, int add){
        int loyalty = this.loyalties.getOrDefault(player.getUUID(), STARTING_LOYALTY);
        this.loyalties.put(player.getUUID(), loyalty + add);

        if(!level.isClientSide() && this.loyalties.get(player.getUUID()) >= 0){
            createInteriorDimension(this.level, this.getBlockPos());
        }

    }

    public void createInteriorDimension(Level level, BlockPos pos){
        if(!level.isClientSide){
            level.getServer().execute(() -> {
                ServerLevel tardisLevel = DimensionHelper.createTardis(level.getServer());
                tardisLevel.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                    SpaceTimeCoord coord = new SpaceTimeCoord(level.dimension(), pos);
                    cap.setDestination(coord);
                    cap.setLocation(coord);
                    cap.getControlDataOrCreate(ControlRegistry.FACING.get()).set(WorldHelper.getHorizontalFacing(this.getBlockState()));
                    cap.setExterior(this.baseExterior.get(), true);
                    cap.getExterior().setPosition(level, pos);
                    //cap.getEmotionalHandler().setTraits(this.getTraits().toArray(new TraitType[0]));
                    cap.getEmotionalHandler().setLoyalties(this.loyalties);
                    tardisLevel.getChunk(0, 0); //Make world gen generate test structure
                    setupNewTardis(cap);
                });

            });
        }
    }

    public void setupNewTardis(ITardisLevel tardis){
        MinecraftForge.EVENT_BUS.post(new TardisEvent.TardisCreatedEvent(tardis));

        //Add caps
        for(int i = 0; i < tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.CAPACITORS).getSlots(); ++i){
            if(level.random.nextFloat() < 0.1F){
                tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.CAPACITORS).setStackInSlot(i, new ItemStack(ItemRegistry.ARTRON_CAP_LEAKY.get()));
            }
        }

        tardis.getControlDataOrCreate(ControlRegistry.SONIC_PORT.get()).set(new ItemStack(ItemRegistry.SONIC.get()));

    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        this.getExterior().ifPresent(type -> Helper.writeRegistryToNBT(tag, ExteriorRegistry.REGISTRY.get(), type, "exterior"));

        ListTag loyalty = new ListTag();
        for(Map.Entry<UUID, Integer> entry : this.loyalties.entrySet()){
            CompoundTag lTag = new CompoundTag();
            lTag.putUUID("player", entry.getKey());
            lTag.putInt("loyalty", entry.getValue());
            loyalty.add(lTag);
        }
        tag.put("loyalties", loyalty);


        ListTag traitsTag = new ListTag();
        for(Trait trait : this.traits){
            Helper.getKeyFromValueHashMap(TraitListener.traits, trait).ifPresent(id -> {
                traitsTag.add(StringTag.valueOf(id.toString()));
            });
        }
        tag.put("traits", traitsTag);

    }
    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        //Clear old data
        this.loyalties.clear();
        this.traits.clear();

        //Load new

        this.baseExterior = Helper.readRegistryFromString(tag, ExteriorRegistry.REGISTRY.get(), "exterior");

        ListTag loyaltyTag = tag.getList("loyalties", ListTag.TAG_COMPOUND);
        for(Tag t : loyaltyTag){
            CompoundTag l = (CompoundTag) t;
            this.loyalties.put(l.getUUID("player"), l.getInt("loyalty"));
        }

        ListTag traitsList = tag.getList("traits", Tag.TAG_STRING);
        for(int i = 0; i < traitsList.size(); ++i){
            final ResourceLocation traitLoc = new ResourceLocation(traitsList.getString(i));
            if(TraitListener.traits.containsKey(traitLoc)){
                this.traits.add(TraitListener.traits.get(traitLoc));
            }
        }
    }

    public List<Trait> getTraits(){
        if(this.traits.isEmpty()){
            this.setTraits(EmotionalHandler.generateRandomTraits(this.level.random));
        }
        return this.traits;
    }

    @Override
    public AABB getRenderBoundingBox() {
        return WorldHelper.getCenteredAABB(this.getBlockPos(), 2);
    }


    public void setTraits(List<Trait> traits) {
        this.traits.clear();
        this.traits.addAll(traits);
        this.setChanged();
        if(this.level != null && !this.level.isClientSide()){
           // Network.sendToTracking(this, new BrokenExteriorTraitsMessage(this.getBlockPos(), this.traits));
        }
    }

    @Override
    public void setMasterData(MultiblockData data) {
        this.multiblockData = data;
        this.setChanged();
    }

    @Override
    public List<BlockPos> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public MultiblockData getMultiblockData() {
        return this.multiblockData;
    }
}
