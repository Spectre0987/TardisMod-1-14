package net.tardis.mod.blockentities.exteriors;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;

public interface IExteriorObject {

    void setInterior(ResourceKey<Level> interior);

    Level getLevel();

}
