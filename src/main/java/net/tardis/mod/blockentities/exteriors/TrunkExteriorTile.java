package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class TrunkExteriorTile extends ExteriorTile{


    public TrunkExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public TrunkExteriorTile(BlockPos pos, BlockState state) {
        super(TileRegistry.TRUNK_EXTERIOR.get(), pos, state);
    }
}
