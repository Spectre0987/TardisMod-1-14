package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class CoffinExteriorTile extends ExteriorTile{
    public CoffinExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public CoffinExteriorTile(BlockPos pos, BlockState state) {
        this(TileRegistry.COFFIN.get(), pos, state);
    }
}
