package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;

public class PoliceBoxExteriorTile extends ExteriorTile {
    public PoliceBoxExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public PoliceBoxExteriorTile(BlockPos pos, BlockState state){
        this(TileRegistry.POLICE_BOX_EXTERIOR.get(), pos, state);
    }
}
