package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.animations.demat.DematAnimation;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.IHaveMatterState;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ExteriorMatterStateMessage;
import net.tardis.mod.network.packets.UpdateChameleonDisguseMessage;
import net.tardis.mod.registry.DematAnimationRegistry;
import org.jetbrains.annotations.Nullable;

public class DisguisedBlockTile extends BlockEntity implements IDisguisedBlock, IHaveMatterState {

    BlockState disguisedState;
    BlockEntity disguisedEntity;

    //For demat stuff
    private final MatterStateHandler matterStateHandler = new MatterStateHandler(() -> WorldHelper.centerOfBlockPos(this.getBlockPos()))
            .setChangedAction(() -> {
                this.setChanged();
                if(!getLevel().isClientSide())
                    Network.sendToTracking(this, new ExteriorMatterStateMessage(this.getBlockPos(), this.getMatterStateHandler()));
            });

    public DisguisedBlockTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public DisguisedBlockTile(BlockPos pPos, BlockState pBlockState) {
        this(TileRegistry.DISGUISED_BLOCK.get(), pPos, pBlockState);
    }

    @Nullable
    public BlockState getDisguisedState(){
        return this.disguisedState;
    }

    @Override
    public BlockEntity getDisguisedTile() {
        return this.disguisedEntity;
    }

    public void setDisguisedEntity(@Nullable BlockEntity entity){
        this.disguisedEntity = entity;
        this.setChanged();
    }

    public void setDisguisedState(BlockState state){
        this.disguisedState = state;
        this.setChanged();
        if(getLevel() instanceof ServerLevel server){
            server.getLightEngine().checkBlock(getBlockPos());
            Network.sendToTracking(this, new UpdateChameleonDisguseMessage(this.getBlockPos(), state));
        }
    }

    @Override
    public void setDisguisedTile(BlockEntity entity) {
        this.disguisedEntity = entity;
        this.setChanged();
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("disguised_state")){
            this.disguisedState = BlockState.CODEC.parse(NbtOps.INSTANCE, tag.get("disguised_state")).getOrThrow(true, Tardis.LOGGER::warn);

            if(tag.contains("disguised_entity")){
                CompoundTag entityTag = tag.getCompound("disguised_entity");
                if(entityTag.contains("id")){
                    BlockEntityType<?> tileType = ForgeRegistries.BLOCK_ENTITY_TYPES.getValue(new ResourceLocation(entityTag.getString("id")));
                    BlockEntity tile = tileType.create(getBlockPos(), this.disguisedState);
                    tile.deserializeNBT(entityTag);
                    this.setDisguisedEntity(tile);
                }
            }
        }
        this.matterStateHandler.deserializeNBT(tag.getCompound("matter_state"));

    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(this.disguisedState != null){
            tag.put("disguised_state", BlockState.CODEC.encodeStart(NbtOps.INSTANCE, this.disguisedState).get().orThrow());
            if(this.disguisedEntity != null){
                tag.put("disguised_entity", this.disguisedEntity.serializeNBT());
            }
        }
        tag.put("matter_state", this.matterStateHandler.serializeNBT());
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
    }

    @Override
    public MatterStateHandler getMatterStateHandler() {
        return this.matterStateHandler;
    }
}
