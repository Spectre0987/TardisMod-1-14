package net.tardis.mod.entity;

public interface IDrivable {

    void drive(float turnAmount, float speed);
    float getMaxSpeed();
    void stop();
}
