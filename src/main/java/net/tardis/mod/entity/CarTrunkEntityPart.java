package net.tardis.mod.entity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.entity.PartEntity;
import net.tardis.mod.misc.TeleportEntry;
import net.tardis.mod.misc.tardis.InteriorDoorData;

public class CarTrunkEntityPart<T extends CarExteriorEntity> extends PartEntity<T> {

    final EntityDimensions size;

    public CarTrunkEntityPart(T parent, float size) {
        super(parent);
        this.size = EntityDimensions.fixed(size, size);
        this.refreshDimensions();
    }

    @Override
    protected void defineSynchedData() {

    }

    @Override
    public boolean isPickable() {
        return true;
    }

    @Override
    public InteractionResult interact(Player pPlayer, InteractionHand pHand) {
        return this.getParent().getDoorHandler().onInteract(pPlayer, pPlayer.getItemInHand(pHand), pHand).isSuccess() ?
                InteractionResult.sidedSuccess(pPlayer.level.isClientSide()) : InteractionResult.PASS;
    }

    @Override
    public void tick() {
        super.tick();

        if(getParent().cachedTardis != null && !this.level.isClientSide()){
            getParent().getTeleportHandler().tick((ServerLevel) this.getLevel());
            /*
            for(Entity e : getLevel().getEntities(this, this.size.makeBoundingBox(this.position()))){

                if(e == null || this.is(e) || e.is(this))
                    return;

                final InteriorDoorData doorData = this.getParent().cachedTardis.getInteriorManager().getMainInteriorDoor();
                Vec3 pos = doorData.placeAtMe(e, getParent().cachedTardis);
                TeleportEntry.add(new TeleportEntry(e, (ServerLevel) getParent().cachedTardis.getLevel(), pos, doorData.getRotation(getParent().cachedTardis)));
            }
             */
        }

    }

    @Override
    public boolean is(Entity pEntity) {
        return this == pEntity || this.getParent() == pEntity;
    }

    @Override
    public boolean shouldBeSaved() {
        return false;
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag pCompound) {

    }

    @Override
    protected void addAdditionalSaveData(CompoundTag pCompound) {

    }

    @Override
    public EntityDimensions getDimensions(Pose pPose) {
        return this.size;
    }
}
