package net.tardis.mod.entity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.level.Level;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.network.NetworkHooks;

public class ChairEntity extends Entity implements IEntityAdditionalSpawnData {

    public int ticksEmpty = 0;
    public static EntityDataAccessor<Float> CHAIR_HEIGHT = SynchedEntityData.defineId(ChairEntity.class, EntityDataSerializers.FLOAT);

    public ChairEntity(EntityType<?> pEntityType, Level pLevel) {
        super(pEntityType, pLevel);
    }

    public ChairEntity setHeight(float height){
        this.getEntityData().set(CHAIR_HEIGHT, height);
        return this;
    }

    @Override
    public EntityDimensions getDimensions(Pose pPose) {
        return EntityDimensions.scalable(1.0F, this.getEntityData().get(CHAIR_HEIGHT));
    }

    @Override
    public void tick() {
        super.tick();

        if(this.tickCount == 10){
            this.refreshDimensions();
        }

        if(this.getPassengers().isEmpty()){
            ++ticksEmpty;
            if(ticksEmpty > 20){
                this.remove(RemovalReason.KILLED);
            }
        }

    }

    @Override
    protected void defineSynchedData() {
        this.entityData.define(CHAIR_HEIGHT, 1.0F);
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compoundTag) {
        this.getEntityData().set(CHAIR_HEIGHT, compoundTag.getFloat("chair_height"));
    }

    @Override
    public void writeSpawnData(FriendlyByteBuf buf) {
        buf.writeFloat(this.getEntityData().get(CHAIR_HEIGHT));
    }

    @Override
    public void readSpawnData(FriendlyByteBuf buf) {
        this.getEntityData().set(CHAIR_HEIGHT, buf.readFloat());
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compoundTag) {
        compoundTag.putFloat("chair_height", this.getEntityData().get(CHAIR_HEIGHT));
    }

    @Override
    public Packet<ClientGamePacketListener> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public void positionRider(Entity pPassenger) {
        super.positionRider(pPassenger);
        pPassenger.setYRot(this.getYRot());
    }

    @Override
    public boolean shouldBeSaved() {
        return false;
    }
}
