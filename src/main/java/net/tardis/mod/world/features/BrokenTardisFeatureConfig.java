package net.tardis.mod.world.features;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

import java.util.List;
import java.util.stream.Stream;

public record BrokenTardisFeatureConfig(int width, int height, int chance, BlockState blockToPlace, List<TagKey<Biome>> blackListedBiomes) implements FeatureConfiguration {

    public static final Codec<BrokenTardisFeatureConfig> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    Codec.INT.fieldOf("width").forGetter(BrokenTardisFeatureConfig::width),
                    Codec.INT.fieldOf("height").forGetter(BrokenTardisFeatureConfig::height),
                    Codec.INT.fieldOf("chance").forGetter(BrokenTardisFeatureConfig::chance),
                    BlockState.CODEC.fieldOf("block_to_place").forGetter(BrokenTardisFeatureConfig::blockToPlace),
                    TagKey.codec(Registries.BIOME).listOf().fieldOf("blacklisted_biomes").forGetter(BrokenTardisFeatureConfig::blackListedBiomes)

            ).apply(instance, BrokenTardisFeatureConfig::new)
    );
}
