package net.tardis.mod.world.features;

import com.mojang.serialization.Codec;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.tardis.mod.registry.BlockRegistry;

public class SingleBlockFeature extends Feature<BrokenTardisFeatureConfig> {


    public SingleBlockFeature(Codec<BrokenTardisFeatureConfig> pCodec) {
        super(pCodec);
    }

    @Override
    public boolean place(FeaturePlaceContext<BrokenTardisFeatureConfig> pContext) {

        if(pContext.config().chance() < pContext.random().nextInt(100))
            return false;

        for(TagKey<Biome> tag : pContext.config().blackListedBiomes()){
            if(pContext.level().getBiome(pContext.origin()).is(tag)){
                return false;
            }
        }

        System.out.println("TARDIS Spawned at %s".formatted(pContext.origin().toString()));

        setBlock(pContext.level(), pContext.origin(), pContext.config().blockToPlace());
        return true;
    }
}
