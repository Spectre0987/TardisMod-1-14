package net.tardis.mod.world.features.configs;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

import java.util.List;

public record XionFeatureConfig(int tries, int xzRadius, int minCrystals, int maxCrystals, boolean canPlaceInWater, List<TagKey<Biome>> blacklistedBiome) implements FeatureConfiguration {

    public static final Codec<XionFeatureConfig> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    Codec.INT.fieldOf("tries").forGetter(XionFeatureConfig::tries),
                    Codec.INT.fieldOf("xz_radius").forGetter(XionFeatureConfig::xzRadius),
                    Codec.INT.fieldOf("min_crystals").forGetter(XionFeatureConfig::minCrystals),
                    Codec.INT.fieldOf("max_crystals").forGetter(XionFeatureConfig::maxCrystals),
                    Codec.BOOL.fieldOf("can_place_in_water").forGetter(XionFeatureConfig::canPlaceInWater),
                    TagKey.codec(Registries.BIOME).listOf().fieldOf("blacklisted_biomes").forGetter(XionFeatureConfig::blacklistedBiome)
            ).apply(instance, XionFeatureConfig::new)
    );

}
