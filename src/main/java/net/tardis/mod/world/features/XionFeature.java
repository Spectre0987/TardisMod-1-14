package net.tardis.mod.world.features;

import com.mojang.serialization.Codec;
import net.minecraft.core.BlockPos;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.world.features.configs.XionFeatureConfig;

import java.util.Optional;

/**
 * Handles placing Xion Crystals in the world
 **/
public class XionFeature extends Feature<XionFeatureConfig> {

    public XionFeature(Codec<XionFeatureConfig> pCodec) {
        super(pCodec);
    }

    @Override
    public boolean place(FeaturePlaceContext<XionFeatureConfig> pContext) {

        BlockPos startPos = pContext.origin();
        final ObjectHolder<Boolean> placedAtLeastOne = new ObjectHolder<>(false);

        for(int y = pContext.level().getHeight(Heightmap.Types.WORLD_SURFACE_WG, startPos.getX(), startPos.getZ()); y < pContext.level().getMinBuildHeight(); --y){
            final BlockPos p = new BlockPos(startPos.getX(), y, startPos.getZ());
            if(isValid(pContext.level(), startPos, pContext.config())){
                startPos = p;
                break;
            }
        }

        for(int t = 0; t < pContext.config().tries(); ++t){
            findEmptyY(pContext.level(), WorldHelper.getRandomBlockPos(startPos, pContext.random(), pContext.config().xzRadius()), 5).ifPresent(pos -> {
                if(isValid(pContext.level(), pos, pContext.config())){
                    pContext.level().setBlock(pos, BlockRegistry.XION.get().defaultBlockState(), 2);
                    placedAtLeastOne.set(true);
                }
            });
        }

        return placedAtLeastOne.get();
    }

    public Optional<BlockPos> findEmptyY(LevelAccessor level, BlockPos start, int searchRadius){
        //search up
        for(int y = 0; y < searchRadius; ++y){
            final BlockPos newPos = new BlockPos(start.getX(), start.getY() + y, start.getZ());
            if(level.getBlockState(newPos).isAir())
                return Optional.of(newPos);
        }

        //search down
        for(int y = 0; y < searchRadius; ++y){
            final BlockPos newPos = new BlockPos(start.getX(), start.getY() - y, start.getZ());
            if(level.getBlockState(newPos).isAir()){
                return Optional.of(newPos);
            }
        }

        return Optional.empty();
    }

    /**
     *
     * @param level
     * @param pos - Position to place the xion crystals
     * @return true if we can place a crystal at this position
     */
    public static boolean isValid(LevelReader level, BlockPos pos, XionFeatureConfig config){

        for(TagKey<Biome> biome : config.blacklistedBiome()){
            if(level.getBiome(pos).is(biome)){
                return false;
            }
        }

        final BlockState state = level.getBlockState(pos);
        final BlockState belowState = level.getBlockState(pos.below());
        final boolean canPlaceInWater = config.canPlaceInWater();

        if(level.canSeeSky(pos))
            return false;

        return (state.isAir() || (state.getBlock() == Blocks.WATER && canPlaceInWater)) &&
                belowState.isCollisionShapeFullBlock(level, pos); //Makes sure it places on a solid block
    }
}
