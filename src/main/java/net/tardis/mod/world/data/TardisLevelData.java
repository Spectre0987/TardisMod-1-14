package net.tardis.mod.world.data;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.saveddata.SavedData;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.misc.tardis.vortex.VortexPheonomena;
import net.tardis.mod.registry.VortexPhenomenaRegistry;

import java.util.*;

public class TardisLevelData extends SavedData {

    public static final String DATA_NAME = Tardis.MODID + ":main_tardis";
    public static final int VP_CHANCE = 750;
    public static final int VP_CHUNK_RADIUS = 10;
    final ServerLevel overworld;

    public HashMap<ChunkPos, Optional<VortexPheonomena>> phenomena = new HashMap<ChunkPos, Optional<VortexPheonomena>>();
    public List<ChunkPos> tickingPhenomena = new ArrayList<>();

    public TardisLevelData(ServerLevel overworld){
        this.overworld = overworld;
    }

    @Override
    public CompoundTag save(CompoundTag tag) {

        ListTag vps = new ListTag();
        for(Map.Entry<ChunkPos, Optional<VortexPheonomena>> vp : this.phenomena.entrySet()){
            final CompoundTag vpTag = new CompoundTag();
            vpTag.putLong("pos", vp.getKey().toLong());
            vp.getValue().ifPresent(v -> vpTag.put("data", v.serializeNBT()));
            vps.add(vpTag);
        }

        tag.put("vortex_phenomenas", vps);

        return tag;
    }

    public static TardisLevelData load(ServerLevel level, CompoundTag tag){
        TardisLevelData data = new TardisLevelData(level);

        final ListTag vps = tag.getList("vortex_phenomenas", Tag.TAG_COMPOUND);
        for(int i = 0; i < vps.size(); ++i){
            final CompoundTag vpTag = vps.getCompound(i);

            ChunkPos pos = new ChunkPos(vpTag.getLong("pos"));
            if(vpTag.contains("data")){
                data.phenomena.put(pos, VortexPheonomena.of(pos, vpTag.getCompound("data")));
            }
            else data.phenomena.put(pos, Optional.empty());
        }

        return data;
    }

    public void addPhenomena(ChunkPos pos, Optional<VortexPheonomena> phenomena){
        this.phenomena.put(pos, phenomena);
        this.setDirty();
    }

    public Optional<VortexPheonomena> getOrGeneratePhenomena(ChunkPos pos, boolean shouldGenerateIfMissing){

        if(this.phenomena.containsKey(pos) || !shouldGenerateIfMissing){
            return this.phenomena.getOrDefault(pos, Optional.empty());
        }

        //Generate
        final RandomSource rand = this.overworld.getRandom();
        //1 in 1000 chance to generate phenomena
        if(rand.nextInt(VP_CHANCE) == 7){

            //Generate Vortex Phenomena
            final List<ResourceLocation> typeLocations = VortexPhenomenaRegistry.REGISTRY.get().getKeys().stream().toList();
            //Get random Vortex Phenomena Type
            final VortexPhenomenaType<?> type = VortexPhenomenaRegistry.REGISTRY.get().getValue(
                    typeLocations.get(rand.nextInt(typeLocations.size()))
            );

            VortexPheonomena vp = type.create(pos);
            vp.onGenerated(overworld);
            final Optional<VortexPheonomena> vpHolder = Optional.of(vp);
            this.addPhenomena(pos, vpHolder);
            return vpHolder;

        }
        //add this to the list so we don't attempt to generate this chunk again
        this.addPhenomena(pos, Optional.empty());
        return Optional.empty();
    }

    public void doVortexPhenomenaStuff(ITardisLevel tardis){
        if(tardis.getCurrentCourse().isEmpty())
            return;
        final SpaceTimeCoord coord = tardis.getCurrentCourse().get().getCurrentFlightLocation(tardis.getDistanceTraveled());
        final ChunkPos pos = new ChunkPos(coord.getPos());

        //Get chunks in radius of this TARDIS' current flight location
        ChunkPos.rangeClosed(pos, VP_CHUNK_RADIUS).forEach(p -> {
            getOrGeneratePhenomena(p, true).ifPresent(vp -> {
                if(vp.isIn(coord)){
                    vp.checkin(tardis, overworld.getGameTime());
                    if(!this.tickingPhenomena.contains(p)){
                        this.tickingPhenomena.add(p);
                    }
                }
            });
        });
    }

    /**
     * For packets so you can see phenomenas on the flight course screen
     * @param tardis
     * @param chunkRadius
     * @return
     */
    public Map<ChunkPos, VortexPhenomenaDTO> getTypesNear(ITardisLevel tardis, int chunkRadius){
        //Get Tardis Location
        SpaceTimeCoord coord = tardis.getLocation();
        if(tardis.getCurrentCourse().isPresent()){
            coord = tardis.getCurrentCourse().get().getCurrentFlightLocation(tardis.getDistanceTraveled());
        }
        final ChunkPos pos = new ChunkPos(coord.getPos());

        //Find all valid VPs near this TARDISes Location
        final Map<ChunkPos, VortexPhenomenaDTO> map = new HashMap<>();

        ChunkPos.rangeClosed(pos, chunkRadius).forEach(p -> {
            if(this.phenomena.containsKey(p)){
                this.phenomena.get(p).ifPresent(vp -> {
                    map.put(p, VortexPhenomenaDTO.fromPhenomena(vp));
                });
            }
        });

        return map;
    }

    public void tickActivePhenomena(){

        List<ChunkPos> stopTicking = new ArrayList<>();
        for(ChunkPos pos : this.tickingPhenomena){
            this.getOrGeneratePhenomena(pos, false).ifPresent(vp -> {
                if(!vp.tick(overworld.getGameTime())){
                    stopTicking.add(pos);
                }
            });
        }
        this.tickingPhenomena.removeIf(stopTicking::contains);
    }

    public static TardisLevelData get(ServerLevel level){
        return level.getServer().overworld().getDataStorage().computeIfAbsent((tag) -> TardisLevelData.load(level, tag), () -> new TardisLevelData(level), DATA_NAME);
    }

    @Override
    public boolean isDirty() {
        if(super.isDirty()){
            return true;
        }

        return this.phenomena.values().stream().anyMatch(opt -> opt.isPresent() && opt.get().isDirty());
    }

    public record TardisCheckin(ITardisLevel tardis, long timeCheckedIn){

        public static final int CHECKIN_EXPIRE_TIME = 20;//One second

        public boolean expired(long currentGameTime){
            return currentGameTime >= this.timeCheckedIn() + CHECKIN_EXPIRE_TIME;
        }

    }

    public record VortexPhenomenaDTO(VortexPhenomenaType<?> type, List<Component> extraText, int radius){

        public void encode(FriendlyByteBuf buf){
            buf.writeRegistryId(VortexPhenomenaRegistry.REGISTRY.get(), this.type());
            buf.writeInt(this.extraText.size());
            for(int i = 0; i < this.extraText().size(); ++i){
                final int length = this.extraText().get(i).getString().length();
                buf.writeInt(length);
                buf.writeUtf(this.extraText.get(i).getString(), length);
            }
            buf.writeInt(radius);
        }

        public static VortexPhenomenaDTO fromPhenomena(VortexPheonomena vp){
            return new VortexPhenomenaDTO(vp.getType(), new ArrayList<>(), vp.getRadius());
        }

        public static VortexPhenomenaDTO fromNetwork(FriendlyByteBuf buf){
            final VortexPhenomenaType<?> type = buf.readRegistryId();
            final int textSize = buf.readInt();
            final List<Component> text = new ArrayList<>();
            for(int i = 0; i < textSize; ++i){
                final int size = buf.readInt();
                text.add(Component.literal(buf.readUtf(size)));
            }
            return new VortexPhenomenaDTO(type, text, buf.readInt());
        }

    }
}
