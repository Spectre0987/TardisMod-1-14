package net.tardis.mod.world.data;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.saveddata.SavedData;
import net.tardis.mod.ars.RoomEntry;
import net.tardis.mod.helpers.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ARSRoomLevelData extends SavedData {

    public static final String ID = Helper.createRL("ars_room_data").toString();

    public final List<RoomEntry> entries = new ArrayList<>();

    @Override
    public CompoundTag save(CompoundTag tag) {
        ListTag entryList = new ListTag();
        for(RoomEntry room : this.entries){
            entryList.add(room.serializeNBT());
        }
        tag.put("rooms", entryList);
        return tag;
    }

    public void load(CompoundTag tag){
        this.entries.clear();
        final ListTag entryList = tag.getList("rooms", ListTag.TAG_COMPOUND);
        for(int i = 0; i < entryList.size(); ++i){
            this.entries.add(new RoomEntry(entryList.getCompound(i)));
        }
    }

    public static ARSRoomLevelData getData(ServerLevel level){
        return level.getDataStorage().computeIfAbsent(ARSRoomLevelData::createFromTag, ARSRoomLevelData::new, ID);
    }

    public static ARSRoomLevelData createFromTag(CompoundTag tag){
        final ARSRoomLevelData data = new ARSRoomLevelData();
        data.load(tag);
        return data;
    }

    public void addRoomEntry(ResourceLocation key, BlockPos start, Vec3i size) {
        Optional<RoomEntry> existingRoom = this.entries.stream()
                .filter(e -> e.start.equals(start))
                .findAny();
        if(existingRoom.isPresent()){
            existingRoom.get().setRoom(key);
        }
        else this.entries.add(new RoomEntry(key, start, size));
        this.setDirty();

    }

    @Override
    public boolean isDirty() {
        for(RoomEntry entry : this.entries){
            if(entry.isDirty())
                return true;
        }
        return super.isDirty();
    }

    public Optional<RoomEntry> getRoomFor(BlockPos pos) {
        return this.entries.parallelStream().filter(e -> e.isInside(pos)).findAny();


    }

    public void removeRoomsAt(BlockPos pos){
        this.entries.removeIf(r -> r.isInside(pos));
        this.setDirty();
    }
}
