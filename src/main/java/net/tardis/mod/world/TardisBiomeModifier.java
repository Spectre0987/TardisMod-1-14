package net.tardis.mod.world;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.world.BiomeModifier;
import net.minecraftforge.common.world.ModifiableBiomeInfo;

public record TardisBiomeModifier(HolderSet<PlacedFeature> features) implements BiomeModifier {

    public static final Codec<TardisBiomeModifier> CODEC = RecordCodecBuilder.create(instance -> {
        return instance.group(
                PlacedFeature.LIST_CODEC.fieldOf("features").forGetter(TardisBiomeModifier::features)
        ).apply(instance, TardisBiomeModifier::new);
    });

    @Override
    public void modify(Holder<Biome> biome, Phase phase, ModifiableBiomeInfo.BiomeInfo.Builder builder) {
        if(phase == Phase.ADD){
            this.features.stream().forEach(placed -> builder.getGenerationSettings()
                    .addFeature(GenerationStep.Decoration.UNDERGROUND_STRUCTURES, placed));
        }
    }

    @Override
    public Codec<? extends BiomeModifier> codec() {
        return CODEC;
    }
}
