package net.tardis.mod.world.placements;

import net.minecraft.client.model.geom.PartPose;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.placement.PlacementContext;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.tardis.mod.world.WorldRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class OnBlockPlacement extends PlacementModifier {

    public final int searchRange;
    public OnBlockPlacement(int placement){
        this.searchRange = placement;
    }

    @Override
    public Stream<BlockPos> getPositions(PlacementContext pContext, RandomSource pRandom, BlockPos pPos) {

        final BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos(pPos.getX(), pPos.getY(), pPos.getZ());
        for(int i = pos.getY(); i > pContext.getMinBuildHeight(); --i){ //Search downwards for open space to place this exterior
            if(isGood(pContext, pos)){
                return Stream.of(pos);
            }
            pos.below(1);
        }

        return Stream.of();
    }

    public boolean isGood(PlacementContext context, BlockPos pos){
        pos = pos.immutable();
        final BlockState state = context.getBlockState(pos);
        final BlockState lowerState = context.getBlockState(pos.relative(Direction.DOWN));

        return state.isAir() && lowerState.isCollisionShapeFullBlock(context.getLevel().getLevel(), pos.relative(Direction.DOWN));

    }

    @Override
    public PlacementModifierType<?> type() {
        return WorldRegistry.ON_BLOCK_PLACEMENT.get();
    }
}
