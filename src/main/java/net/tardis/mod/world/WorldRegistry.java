package net.tardis.mod.world;

import com.mojang.serialization.Codec;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.minecraftforge.common.world.BiomeModifier;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.world.features.SingleBlockFeature;
import net.tardis.mod.world.features.BrokenTardisFeatureConfig;
import net.tardis.mod.world.features.XionFeature;
import net.tardis.mod.world.features.configs.XionFeatureConfig;
import net.tardis.mod.world.placements.OnBlockPlacementModiferType;

public class WorldRegistry {

    public static final DeferredRegister<Codec<? extends ChunkGenerator>> CHUNK_GENERATORS = DeferredRegister.create(Registries.CHUNK_GENERATOR, Tardis.MODID);
    public static final DeferredRegister<PlacementModifierType<?>> PLACEMENTS = DeferredRegister.create(Registries.PLACEMENT_MODIFIER_TYPE, Tardis.MODID);
    public static final DeferredRegister<Feature<?>> FEATURES = DeferredRegister.create(Registries.FEATURE, Tardis.MODID);
    public static final DeferredRegister<Codec<? extends BiomeModifier>> BIOME_MODIFIERS = DeferredRegister.create(ForgeRegistries.Keys.BIOME_MODIFIER_SERIALIZERS, Tardis.MODID);

    //Chunk generators
    public static final RegistryObject<Codec<? extends ChunkGenerator>> TARDIS_CHUNK_GENERATOR = CHUNK_GENERATORS.register("tardis", () -> TardisChunkGenerator.CODEC);

    //Features
    public static final RegistryObject<SingleBlockFeature> BROKEN_TARDIS_FEATURE = FEATURES.register("broken_tardis", () -> new SingleBlockFeature(BrokenTardisFeatureConfig.CODEC));
    public static final RegistryObject<XionFeature> XION_PATCH_FEATURE = FEATURES.register("xion_patch", () -> new XionFeature(XionFeatureConfig.CODEC));
    //Biomes
    public static final ResourceKey<Biome> TARDIS_BIOME = ResourceKey.create(Registries.BIOME, Helper.createRL("tardis"));

    public static final RegistryObject<Codec<TardisBiomeModifier>> DEFAULT_BIOME_MOD = BIOME_MODIFIERS.register("default", () -> TardisBiomeModifier.CODEC);

    public static final RegistryObject<PlacementModifierType<?>> ON_BLOCK_PLACEMENT = PLACEMENTS.register("on_block", () -> new OnBlockPlacementModiferType());

    public static void registerRegistries(IEventBus bus){
        CHUNK_GENERATORS.register(bus);
        PLACEMENTS.register(bus);
        FEATURES.register(bus);
        BIOME_MODIFIERS.register(bus);

    }


}
