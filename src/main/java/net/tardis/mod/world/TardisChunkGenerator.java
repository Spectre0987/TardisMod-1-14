package net.tardis.mod.world;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.*;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.FixedBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.SingleThreadedRandomSource;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.ars.ARSRoom;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class TardisChunkGenerator extends ChunkGenerator {

    public static final Codec<TardisChunkGenerator> CODEC = RecordCodecBuilder.create(instance -> {
        return instance.group(
                RegistryOps.retrieveRegistryLookup(Registries.BIOME).forGetter(gen -> gen.biomeReg),
                RegistryOps.retrieveRegistryLookup(JsonRegistries.ARS_ROOM_REGISTRY).forGetter(gen -> gen.arsRoom)
        ).apply(instance, TardisChunkGenerator::new);
    });

    public final HolderLookup.RegistryLookup<Biome> biomeReg;
    public final HolderLookup.RegistryLookup<ARSRoom> arsRoom;
    public final RandomSource random;

    // Some parameter values.
    public final int distanceBetweenRooms = 9;
    public static final int arsChunkSize = 3;
    public  static final int chunkSize = 16;


    public TardisChunkGenerator(HolderLookup.RegistryLookup<Biome> biomeReg, HolderLookup.RegistryLookup<ARSRoom> arsRooms) {
        super(new FixedBiomeSource(biomeReg.getOrThrow(WorldRegistry.TARDIS_BIOME)));
        this.biomeReg = biomeReg;
        this.arsRoom = arsRooms;
        this.random = new SingleThreadedRandomSource(0l);
    }

    @Override
    public void applyBiomeDecoration(WorldGenLevel pLevel, ChunkAccess pChunk, StructureManager pStructureManager) {

        if (pChunk.getPos().x == 0 && pChunk.getPos().z == 0) { //Always a console room at 0, 0
            placePieceInWorld(pLevel, getRandomPiece(ARSRoom.Type.ROOM).get().get(), pChunk);
        } else {
            // For each ARS chunk size interval.
            if(pChunk.getPos().x % arsChunkSize == 0 && pChunk.getPos().z % arsChunkSize == 0){

                // Determine if the piece is a room or a corridor
                //ARSRoom pieceToPlace = (isChunkAtRoomInterval(pChunk.getPos())) ? getRandomRoomPiece().get().get().getRoomLocation() : getRandomCorridorPiece().getResourceLocation();
                getRandomPiece(isChunkAtRoomInterval(pChunk.getPos()) ? ARSRoom.Type.ROOM : ARSRoom.Type.CORRIDOR).ifPresent(holder -> {
                    placePieceInWorld(pLevel, holder.get(), pChunk);
                });
            }
        }
    }

    private void placePieceInWorld(WorldGenLevel level, ARSRoom room, ChunkAccess pChunk) {
        room.spawnRoom(level, ARSRoom.getSpawnPos(pChunk.getPos()), level.getLevel().getStructureManager(), new StructurePlaceSettings());
    }

    /**
     * Determines if the chunk is a room chunk
     * @param pos the position of the chunk
     * @return is the chunk a room chunk
     */
    private boolean isChunkAtRoomInterval(ChunkPos pos) {
        return pos.x % distanceBetweenRooms == 0 && pos.z % distanceBetweenRooms == 0;
    }

    /**
     * Fetch a random room piece to populate a chunk
     * @return random room ARS piece from the registry.
     */
    private Optional<Holder.Reference<ARSRoom>> getRandomPiece(ARSRoom.Type type) {
        if(type == null){
            return this.arsRoom.listElements().findAny();
        }
        List<Holder.Reference<ARSRoom>> roomList = this.arsRoom.listElements().filter(room -> room.get().getType() == type).filter(room -> room.get().generatesNaturally()).toList();
        return Optional.of(roomList.get(this.random.nextInt(roomList.size())));
    }



    @Override
    public void createReferences(WorldGenLevel pLevel, StructureManager pStructureManager, ChunkAccess pChunk) {
        //super.createReferences(pLevel, pStructureManager, pChunk);
    }

    @Override
    public Codec<TardisChunkGenerator> codec() {
        return CODEC;
    }

    @Override
    public void applyCarvers(WorldGenRegion p_223043_, long p_223044_, RandomState p_223045_, BiomeManager p_223046_, StructureManager p_223047_, ChunkAccess p_223048_, GenerationStep.Carving p_223049_) {

    }

    @Override
    public void buildSurface(WorldGenRegion p_223050_, StructureManager p_223051_, RandomState p_223052_, ChunkAccess p_223053_) {

    }

    @Override
    public void spawnOriginalMobs(WorldGenRegion p_62167_) {}

    @Override
    public int getGenDepth() {
        return 384;
    }

    @Override
    public CompletableFuture<ChunkAccess> fillFromNoise(Executor executor, Blender p_223210_, RandomState p_223211_, StructureManager p_223212_, ChunkAccess access) {
        return CompletableFuture.completedFuture(access);
    }

    @Override
    public int getSeaLevel() {
        return -63;
    }

    @Override
    public int getMinY() {
        return 0;
    }

    @Override
    public int getBaseHeight(int p_223032_, int p_223033_, Heightmap.Types p_223034_, LevelHeightAccessor p_223035_, RandomState p_223036_) {
        return 0;
    }

    @Override
    public NoiseColumn getBaseColumn(int p_223028_, int p_223029_, LevelHeightAccessor level, RandomState p_223031_) {

        BlockState[] states = new BlockState[level.getHeight()];
        for(int i = 0; i < states.length; ++i){
            states[i] = Blocks.AIR.defaultBlockState();
        }

        //return new NoiseColumn(this.getMinY(), states);

        return new NoiseColumn(0, new BlockState[0]);
    }

    @Override
    public void addDebugScreenInfo(List<String> p_223175_, RandomState p_223176_, BlockPos p_223177_) {}
}
