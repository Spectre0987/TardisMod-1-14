package net.tardis.mod.sound;

import net.minecraft.core.registries.Registries;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;

public class SoundRegistry {

    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(Registries.SOUND_EVENT, Tardis.MODID);

    //Control Sounds
    public static final RegistryObject<SoundEvent> AXIS_CONTROL_SOUND = register("axis_control_sound");
    public static final RegistryObject<SoundEvent> RANDOMIZER_CONTROL = register("randomizer");
    public static final RegistryObject<SoundEvent> CONTROL_GENERIC = register("generic_control");
    public static final RegistryObject<SoundEvent> SUBSYSTEM_ON = register("subsystem_on");
    public static final RegistryObject<SoundEvent> SUBSYSTEM_OFF = register("subsystem_off");
    public static final RegistryObject<SoundEvent> REFUEL_ON = register("refuel_on");
    public static final RegistryObject<SoundEvent> REFUEL_OFF = register("refuel_off");
    public static final RegistryObject<SoundEvent> FAST_RETURN = register("fast_return");
    public static final RegistryObject<SoundEvent> HANDBRAKE_ON = register("handbrake_engage");
    public static final RegistryObject<SoundEvent> HANDBRAKE_OFF = register("handbrake_disengage");

    //Exterior sounds
    public static final RegistryObject<SoundEvent> DOOR_OPEN = register("door_open");
    public static final RegistryObject<SoundEvent> DOOR_CLOSE = register("door_close");
    public static final RegistryObject<SoundEvent> DOOR_LOCK = register("door_lock");
    public static final RegistryObject<SoundEvent> DOOR_UNLOCK = register("door_unlock");

    //Tardis Sounds
    public static final RegistryObject<SoundEvent> TAKEOFF_FAIL = register("takeoff_fail");
    public static final RegistryObject<SoundEvent> DEFAULT_TARDIS_TAKEOFF = register("default_tardis_takeoff");
    public static final RegistryObject<SoundEvent> DEFAULT_TARDIS_LOOP = register("default_tardis_loop");
    public static final RegistryObject<SoundEvent> DEFAULT_TARDIS_LAND = register("default_tardis_land");
    public static final RegistryObject<SoundEvent> MASTER_TARDIS_LAND = register("master_tardis_land");
    public static final RegistryObject<SoundEvent> MASTER_TARDIS_TAKEOFF = register("master_tardis_takeoff");
    public static final RegistryObject<SoundEvent> TV_TARDIS_TAKEOFF = register("tv_tardis_takeoff");
    public static final RegistryObject<SoundEvent> TV_TARDIS_LAND = register("tv_tardis_land");
    public static final RegistryObject<SoundEvent> JUNK_TARDIS_TAKEOFF = register("junk_tardis_takeoff");
    public static final RegistryObject<SoundEvent> JUNK_TARDIS_LAND = register("junk_tardis_land");
    public static final RegistryObject<SoundEvent> TARDIS_POWER_ON = register("tardis_power_on");
    public static final RegistryObject<SoundEvent> TARDIS_POWER_OFF = register("tardis_shut_down");
    public static final RegistryObject<SoundEvent> TARDIS_LAND_COMPLETE = register("tardis_land_complete");
    public static final RegistryObject<SoundEvent> CLOISTER_BELL = register("cloister_bell");
    public static final RegistryObject<SoundEvent> TARDIS_IMPACT = register("tardis_impact");
    public static final RegistryObject<SoundEvent> COMMUNICATOR_BELL = register("controls/com_bell");
    public static final RegistryObject<SoundEvent> TARDIS_CREAK = register("creak");

    //Item sounds
    public static final RegistryObject<SoundEvent> SONIC_FAIL = register("sonic/fail");
    public static final RegistryObject<SoundEvent> SONIC_INTERACT = register("sonic/interact");
    public static final RegistryObject<SoundEvent> SONIC_MODE_CHANGE = register("sonic/mode_change");
    public static final RegistryObject<SoundEvent> SIMPLE_BEEP = register("simple_beep");
    public static final RegistryObject<SoundEvent> WELDING = register("welding");

    //Block Sounds
    public static final RegistryObject<SoundEvent> STEAM_HISS = register("steam_hiss");
    public static final RegistryObject<SoundEvent> ELECTRIC_SPARK = register("electric_spark");
    public static final RegistryObject<SoundEvent> DOOR_FAILED_LOCKED = register("door_failed_locked");
    public static final RegistryObject<SoundEvent> SLIDING_DOOR = register("slidingdoor");
    public static final RegistryObject<SoundEvent> TUBE = register("tube");


    //Music
    public static final RegistryObject<SoundEvent> FIRST_ENTRANCE_MUSIC = register("music/first_entrance");


    public static RegistryObject<SoundEvent> register(String name){
        return SOUNDS.register(name, () -> SoundEvent.createVariableRangeEvent(Helper.createRL(name)));
    }

}
