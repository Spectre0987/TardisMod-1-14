package net.tardis.mod.cap.level;

import com.google.common.collect.Lists;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.advancement.TardisTriggers;
import net.tardis.mod.blockentities.machines.ITileEffectTardisLand;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.Config;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.misc.landing.LandingHelper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.misc.landing.TardisLandingContext;
import net.tardis.mod.misc.tardis.*;
import net.tardis.mod.emotional.EmotionalHandler;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SetTardisNameMessage;
import net.tardis.mod.network.packets.TardisFlightStateMessage;
import net.tardis.mod.network.packets.TardisUpdateMessage;
import net.tardis.mod.network.packets.UpdateTARDISNotifMessage;
import net.tardis.mod.network.packets.tardis.TardisDataType;
import net.tardis.mod.registry.*;
import net.tardis.mod.resource_listener.server.TardisNames;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;
import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.tardis.BaseTardisUpgrade;
import net.tardis.mod.upgrade.types.TardisUpgradeType;
import net.tardis.mod.upgrade.types.UpgradeType;
import net.tardis.mod.world.data.TardisLevelData;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class TardisCap implements ITardisLevel{

    public static final int LAND_SEARCH_TIMEOUT_TICKS = 100;

    private final Level level;
    private SpaceTimeCoord location = SpaceTimeCoord.ZERO;
    private SpaceTimeCoord destination = SpaceTimeCoord.ZERO;
    private Optional<FlightCourse> flightCourse = Optional.empty();
    private final HashMap<ControlType<?>, ControlData<?>> controlData = new HashMap<>();
    private final HashMap<TardisUpgradeType<?>, BaseTardisUpgrade> upgrades = new HashMap<>();
    private Exterior exterior;
    private final InteriorManager interiorManager;
    private int ticksInVortex = 0;
    private FlightState flightState = FlightState.LANDED;
    private Optional<FlightEvent> flightEvent = Optional.empty();
    private int flightEventTicksExpire;
    private final FuelHandler fuelHandler;
    private final TardisEngine engine;
    private final EmotionalHandler emotionalHandler;
    private final ExteriorDataHandler exteriorDataHandler;
    private final Communicator communicator;
    private final TardisUnlockManager unlockHandler;
    private final Map<SubsystemType<?>, Subsystem> systems = new HashMap<>();
    private Optional<UUID> lastPilot = Optional.empty();
    private float distanceTraveled = 0;
    private String tardisName;
    public int ticksInVortexToLand = -1;
    public boolean firstTick = true; //Do not save this
    public boolean hasFailedToLand = false;
    private long animationTicks = 0l;

    public List<IFlightDurationEffect> effects = new ArrayList<>();
    public List<TardisNotifications.TardisNotifWrapper> notifications = new ArrayList<>();

    private final List<Runnable> tickers = new ArrayList<>();
    private final List<Supplier<Boolean>> flightTickers = new ArrayList<>();
    private int vortexTicksLandingSearchStart = 0;

    public TardisCap(Level level){
        this.level = level;

        this.interiorManager = new InteriorManager(this);
        this.fuelHandler = new FuelHandler(this);
        this.engine = new TardisEngine(this);
        this.emotionalHandler = new EmotionalHandler(this);
        this.exteriorDataHandler = new ExteriorDataHandler(this);
        this.communicator = new Communicator(this);
        this.unlockHandler = new TardisUnlockManager(this);
    }

    @Override
    public Level getLevel() {
        return this.level;
    }

    @Override
    public SpaceTimeCoord getLocation() {

        if(this.isInVortex() && this.getCurrentCourse().isPresent()){
            final FlightCourse course = this.getCurrentCourse().get();
            return course.getCurrentFlightLocation(this.distanceTraveled);
        }

        return this.location;
    }

    @Override
    public SpaceTimeCoord getDestination() {
        return this.destination;
    }

    @Override
    public Optional<FlightCourse> getCurrentCourse() {
        return this.flightCourse;
    }

    @Override
    public String getTardisName() {

        if(!this.isClient() && (tardisName == null || tardisName.isEmpty())){
            this.tardisName = TardisNames.generateTardisName(level.getServer());
            Network.sendPacketToAll(new SetTardisNameMessage(this.level.dimension(), this.tardisName));
        }

        return this.tardisName;
    }

    @Override
    public float getDistanceTraveled() {
        return this.distanceTraveled;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {

        if(this.isLanding()) {
            return;
        }

        if(this.isInVortex() && !this.isClient()){
            flightCourse = Optional.of(this.flightCourse.orElse(new FlightCourse())
                .addPoint(this, coord));
            this.hasFailedToLand = false;
            if(this.flightState == FlightState.SEARCHING_FOR_LANDING){
                this.flightState = FlightState.FLYING;
            }
        }
        this.destination = coord;
        update(TardisUpdateMessage.UPDATE_LOC);
    }

    @Override
    public void setDestination(ResourceKey<Level> level, BlockPos pos) {
        this.setDestination(new SpaceTimeCoord(level, pos));
    }

    @Override
    public void setLocation(SpaceTimeCoord coord) {
        this.location = coord;
        update(TardisUpdateMessage.UPDATE_LOC);
    }

    @Override
    public void setExterior(ExteriorType type, boolean placeExterior) {

        Optional<ServerLevel> locationLevel = Optional.empty();
        if(!this.isClient()){
            locationLevel = Helper.nullableToOptional(this.level.getServer().getLevel(this.location.getLevel()));
        }

        if(this.exterior != null && exterior.getType() != type && placeExterior){
            //If there is an old exterior, handle disposal and setting up the new one
            locationLevel.ifPresent(l -> {
                this.exterior.delete(l, this.location.getPos());
            });
        }

        this.exterior = type.create(this);

        if(!this.isInVortex() && placeExterior)
            locationLevel.ifPresent(l -> this.exterior.setPosition(l, this.location.getPos()));

        this.update(TardisUpdateMessage.EXTERIOR_CHANGE);
    }

    @Override
    public Exterior getExterior() {
        if(exterior == null)
            this.exterior = ExteriorRegistry.TT_CAPSULE.get().create(this);
        return this.exterior;
    }

    @Override
    public void setFlightCourse(@Nullable FlightCourse newCourse) {
        this.flightCourse = newCourse != null ? Optional.of(newCourse) : Optional.empty();
        if(this.isInVortex()){
            this.setFlightState(this.ticksInVortex, this.ticksInVortexToLand, 0, this.flightState, this.flightCourse);
        }
    }

    @Override
    public void addTicker(Runnable ticker) {

    }

    @Override
    public void addFlightTicker(Runnable ticker) {

    }

    @Override
    public void playCloister() {
        this.playSoundToAll(SoundRegistry.CLOISTER_BELL.get(), SoundSource.MASTER, 1.0F);
        if(getLevel() instanceof ServerLevel server){
            ServerLevel outside = server.getServer().getLevel(this.location.getLevel());
            if(outside != null){
                outside.playSound(null, this.location.getPos(), SoundRegistry.CLOISTER_BELL.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
            }
        }
    }

    @Override
    public long getAnimationTicks() {
        return this.animationTicks;
    }

    @Override
    public int ticksInVortex() {
        return this.ticksInVortex;
    }


    @Override
    public boolean isTraveling() {

        if(this.calcTravelSpeed() <= 0)
            return false;

        if(this.flightCourse.isEmpty()){
            return false;
        }
        return !this.flightCourse.get().isComplete(this.distanceTraveled);
    }

    @Override
    public boolean isInVortex() {
        return this.ticksInVortex > 0;
    }

    @Override
    public Optional<FlightEvent> getCurrentFlightEvent() {
        return this.flightEvent;
    }

    public void startFlightEvent(){

        //No flight events while taking off or landing
        if(this.isTakingOffOrLanding())
            return;

        //If stabilizers are active, and exist cancel
        if(this.getControlDataOrCreate(ControlRegistry.STABILIZERS.get()).get() && SubsystemRegistry.STABILIZERS.get().canBeUsed(this)){
            this.setCurrentFlightEvent(null);
            return;
        }

        //If we have an old event that is not complete, fail it
        if(this.flightEvent.isPresent() && !this.flightEvent.get().isComplete()){
            this.flightEvent.get().onFail();
            this.setCurrentFlightEvent(null);
        }

        //If we are not actually moving through the vortex, don't give flight events
        if(!this.isTraveling()){
            return;
        }

        this.setCurrentFlightEvent(FlightEventRegistry.getRandomType(this));
    }

    public void setCurrentFlightEvent(@Nullable FlightEventType type){
        int ticksToComplete = Mth.ceil((5 * 20) + (1.0F - this.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get()) * 5 * 20);

        if(type != null){
            final FlightEvent event = type.create(this);
            event.onStart();
            this.flightEvent = Optional.of(event);
            ticksToComplete *= event.getTimeMult();

        }
        else {
            this.flightEvent = Optional.empty();
        }
        this.flightEventTicksExpire = this.ticksInVortex + ticksToComplete;

        //Notify players of the new event
        if(!level.isClientSide()){

            update(TardisUpdateMessage.FLIGHT_EVENT);

           this.getCurrentFlightEvent().ifPresent(event -> {
               level.players().forEach(player -> {
                   player.sendSystemMessage(event.getType().makeTrans());
               });
           });
        }
    }

    public void damageTardis(DamageSource source, int amount){

        //If we have an active shield, damage that instead
        if(SubsystemRegistry.SHIELD.get().canBeUsed(this)){
            this.getSubsystem(SubsystemRegistry.SHIELD.get()).ifPresent(s -> {
                s.damage(Math.max(Mth.floor(amount * s.getDamageResistance()), 1));
            });
            return;
        }

        playSoundToAll(SoundRegistry.TARDIS_IMPACT.get(), SoundSource.MASTER, 1.0F);

        //If no shield, damage components
        for(Subsystem s : this.systems.values()){
            s.damage(amount);
        }

    }

    @Override
    public void takeoff() {

        if(this.isClient()) //Do nothing on client
            return;

        if(this.getFuelHandler().getStoredArtron() <= 0 && !this.isClient()){ // Stop take off if there's no fuel
            playSoundToAll(SoundRegistry.TAKEOFF_FAIL.get(), SoundSource.AMBIENT, 1.0F);
            return;
        }

        //Check if any flight systems are blocking flight
        for(SubsystemType<?> type : SubsystemRegistry.TYPE_REGISTRY.get().getValues()){
            if(type.isRequiredForFlight && !type.canBeUsed(this)){ //If we don't have this system, but it's required for flight
                playSoundToAll(SoundRegistry.TAKEOFF_FAIL.get(), SoundSource.AMBIENT, 1.0F);
                return;
            }
            //If we do have this system, but it's blocking flight
            if(this.getSubsystem(type).isPresent() && !this.getSubsystem(type).get().canFly()){
                playSoundToAll(SoundRegistry.TAKEOFF_FAIL.get(), SoundSource.AMBIENT, 1.0F);
                return;
            }
        }



        if(this.flightCourse.isEmpty()){
            this.flightCourse = Optional.of(new FlightCourse().addPoint(this, this.getDestination()));
        }
        this.setFlightState(0, 0, 0, FlightState.FLYING, this.flightCourse);
        this.setCurrentFlightEvent(null);
        this.getExteriorExtraData().setInRift(false);
        this.getInteriorManager().setHasFlown();
        //Fire trigger for advancement
        for(Player player : level.players()){
            TardisTriggers.TAKE_OFF_TARDIS.trigger((ServerPlayer) player);
        }
        this.upgrades.values().stream().filter(BaseTardisUpgrade::canBeUsed).forEach(BaseTardisUpgrade::onTakeoff);
        this.systems.values().stream().filter(Subsystem::canBeUsed).forEach(Subsystem::onTakeoff);

        if(!this.level.isClientSide){

            //Add current location to the fast return swtich
            this.getControlDataOrCreate(ControlRegistry.FAST_RETURN.get()).set(this.getLocation());
            this.getControlDataOrCreate(ControlRegistry.REFUELER.get()).set(false);

            ServerLevel destination = this.level.getServer().getLevel(this.getLocation().getLevel());
            this.getExterior().demat(destination);
            destination.playSound(null, this.getLocation().getPos(), this.getInteriorManager().getSoundScheme().getTakeoff().event(), SoundSource.AMBIENT, 1.0F, 1.0F);
            this.playSoundToAll(this.getInteriorManager().getSoundScheme().getTakeoff().event(), SoundSource.AMBIENT, 1.0F);

            this.getInteriorManager().setHasFlown();
        }

    }

    public void crashInsideSelf(){
        //Try to land near last pilot
        if(lastPilot.isPresent()){
            Player pilot = this.level.getPlayerByUUID(this.lastPilot.get());
            if(pilot != null){
                this.setDestination(new SpaceTimeCoord(this.level, WorldHelper.getRandomBlockPos(pilot.getOnPos(), pilot.getRandom(), 6, 1)));
            }
        }
        else{
            this.setDestination(new SpaceTimeCoord(this.level, WorldHelper.getRandomBlockPos(BlockPos.ZERO, level.random, 20, 100)));
        }
        this.initLanding(new TardisLandingContext(this.exterior, false, false, true), true);
    }

    public void crash(TardisLandingContext context){
        if(this.isLanding())
            return;
        this.damageTardis(null, 10);
        //Try to crash normally
        this.initLanding(context, true);
    }

    @Override
    public void sendNotification(int notificationId) {

        final TardisNotifications.TardisNotif notification = TardisNotifications.getById(notificationId);

        if(MinecraftForge.EVENT_BUS.post(new TardisEvent.TardisNotificationEvent(this, notification))){
            return;
        }

        this.notifications.add(new TardisNotifications.TardisNotifWrapper(notification,
                this.level.getGameTime() + notification.ticksToDisplay()));

        if(!this.isClient()){
            Network.sendPacketToDimension(this.level.dimension(), new UpdateTARDISNotifMessage(notification.id()));
        }

    }

    public List<TardisNotifications.TardisNotif> getActiveNotifications(){
        final List<TardisNotifications.TardisNotif> list = new ArrayList<>();
        final long gameTime = level.getGameTime();

        for(int i = 0; i < this.notifications.size(); ++i){
            if(gameTime >= this.notifications.get(i).worldTimeToExpire()){
                this.notifications.remove(i);
            }
            else list.add(notifications.get(i).notif());
        }

        return list;
    }

    @Override
    public ResourceKey<Level> getId() {
        return getLevel().dimension();
    }

    @Override
    public void initLanding(boolean force) {
        initLanding(TardisLandingContext.basic(this), force);
    }

    @Override
    public void initLanding(TardisLandingContext context, boolean force) {

        if(this.flightState == FlightState.SEARCHING_FOR_LANDING) {
            if(this.vortexTicksLandingSearchStart + LAND_SEARCH_TIMEOUT_TICKS < this.ticksInVortex){
                //Cancel landing if timeout exceeded
                this.setFlightState(this.ticksInVortex, 0, this.distanceTraveled, FlightState.FLYING, this.flightCourse);
            }
            return;
        }

        if(this.isClient())
            return;

        this.setFlightState(this.ticksInVortex, this.ticksInVortexToLand, this.distanceTraveled, FlightState.SEARCHING_FOR_LANDING, this.getCurrentCourse());

        if(!this.flightCourse.get().isComplete(this.distanceTraveled))
            this.destination = this.getLocation();

        final TardisEvent.TardisInitLandEvent event = new TardisEvent.TardisInitLandEvent(this, context);
        if(MinecraftForge.EVENT_BUS.post(event)){
            Helper.doServerTask(getLevel().getServer(), this::playFailToLandEffects);
            return; // If someone cancels the landing event, fail to land
        }
        context = event.getContext(); //Allow addons to overwrite this landing context with a custom one
        this.upgrades.values().stream().filter(BaseTardisUpgrade::canBeUsed).forEach(BaseTardisUpgrade::onLandAttempt);

        //Get Landing pos from TardisLandingContext
        this.calculateLandingPosition(context, pos -> {

            if(pos.isPresent()) {

                //See if anything at the destination would prevent this from landing
                final ChunkPos chunkPos = new ChunkPos(pos.get());
                final ServerLevel destLevel = this.getLevel().getServer().getLevel(this.destination.getLevel());
                final ObjectHolder<BlockPos> landingPos = new ObjectHolder<>(pos.get());
                final ObjectHolder<Boolean> bigCancel = new ObjectHolder<>(false);

                ChunkPos.rangeClosed(chunkPos, 3).forEach(cp -> {
                        for(BlockEntity te : destLevel.getChunk(cp.x, cp.z).getBlockEntities().values()){
                            if(te instanceof ITileEffectTardisLand land){
                                //Skip processing if we are out of range
                                if(!land.isInRange(pos.get())){
                                    continue;
                                }

                                //Fail if block tells us to
                                if(!land.canLandInArea(this)){
                                    Helper.doServerTask(getLevel().getServer(), this::playFailToLandEffects);
                                    bigCancel.set(true);
                                    return;
                                }

                                final BlockPos newPos = land.modifyLandingPos(this, pos.get());
                                if(!newPos.equals(pos.get())){
                                    landingPos.set(newPos);
                                    return;//Break out of lambda and let the rest of the function continue
                                }

                            }
                        }
                });

                //if something at this position stops us landing, cancel landing
                if(bigCancel.get())
                    return;

                this.destination = this.location = new SpaceTimeCoord(this.destination.getLevel(), landingPos.get());
                this.playSoundToAll(this.getInteriorManager().getSoundScheme().getLand().event(), SoundSource.AMBIENT, 1.0F);
                this.placeExterior();

                for (IFlightDurationEffect effect : this.effects) {
                    effect.onTardisLand(this);
                }
                this.effects.clear();
            }
            else {
                Helper.doServerTask(getLevel().getServer(), this::playFailToLandEffects);
                if(force){
                    if(getLevel().dimensionTypeId().equals(this.destination.getLevel())){
                        this.destination = getLocation();
                        this.placeExterior();
                    }
                    else Helper.doServerTask(getLevel().getServer(), this::crashInsideSelf);
                }
            }
        });
    }

    public void playFailToLandEffects(){
        if(this.hasFailedToLand)
            return; //No need to play / notifiy if we already have
        this.hasFailedToLand = true;
        this.setFlightState(this.ticksInVortex, 0, this.distanceTraveled, FlightState.FLYING, this.getCurrentCourse());
        this.sendNotification(TardisNotifications.CANT_LAND);
        if(!isClient()) {
            this.playSoundToAll(SoundRegistry.TAKEOFF_FAIL.get(), SoundSource.PLAYERS, 1.0F);
        }
    }

    public void placeExterior(){
        if(!this.level.isClientSide){
            this.ticksInVortexToLand = this.ticksInVortex + this.getInteriorManager().getSoundScheme().getLand().time();
            this.setFlightState(this.ticksInVortex, this.ticksInVortexToLand, this.distanceTraveled, FlightState.FLYING, this.getCurrentCourse());
            this.setCurrentFlightEvent(null);
            final ServerLevel destination = this.level.getServer().getLevel(this.destination.getLevel());
            final ChunkPos chunkLandingPos = new ChunkPos(this.destination.getPos());

            this.upgrades.values().stream().filter(BaseTardisUpgrade::canBeUsed).forEach(BaseTardisUpgrade::onLandComplete);
            this.systems.values().stream().filter(Subsystem::canBeUsed).forEach(Subsystem::onLand);

            destination.getChunk(chunkLandingPos.x, chunkLandingPos.z);
            this.getExterior().remat(destination);
            this.getExteriorExtraData().onLanded(destination, this.destination.getPos());



            destination.playSound(null, this.getDestination().getPos(),
                    this.getInteriorManager().getSoundScheme().getLand().event(),
                    SoundSource.AMBIENT, 1.0F, 1.0F);

            this.setLocation(this.getDestination());

            destination.getCapability(Capabilities.GENERAL_LEVEL).ifPresent(cap -> cap.addArtronTrail(this));
        }
    }

    @Override
    public void completeLanding() {
        this.setFlightState(0, -1, 0, FlightState.LANDED, Optional.empty());
        this.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).set(0.0F);
        this.setLocation(this.getDestination());
        this.update(TardisUpdateMessage.UPDATE_LOC);
        this.playSoundToAll(SoundRegistry.TARDIS_LAND_COMPLETE.get(), SoundSource.PLAYERS, 1.0F);

    }

    /** Empty if we can't land near here */
    public void calculateLandingPosition(TardisLandingContext context, Consumer<Optional<BlockPos>> positionConsumer){
        if(this.level.isClientSide) {
            positionConsumer.accept(Optional.empty());
            return;
        }

        //Redirect with traits
        for(Trait t : this.getEmotionalHandler().getTraits()){
            if(t != null){
                t.affectLanding(this);
            }
        }

        LandingHelper.addLandingSearch(this, context, positionConsumer);

    }

    //Calculates the TARDIS' Current speed in blocks per second
    public float calcTravelSpeed(){
        float base = Config.Server.TARDIS_BASE_SPEED.get() * this.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get();

        final TardisEvent.TardisSpeedCalcEvent event = new TardisEvent.TardisSpeedCalcEvent(this, base);
        MinecraftForge.EVENT_BUS.post(event);

        float speed = event.getSpeed();
        for(BaseTardisUpgrade upgrade : this.upgrades.values()){
            speed *= upgrade.speedMod();
        }

        return speed;
    }

    @Override
    public boolean isTakingOffOrLanding() {
        return this.isLanding() || this.isTakingOff();
    }

    public boolean isTakingOff(){
        if(this.flightState != FlightState.FLYING)
            return false;
        return this.ticksInVortex < this.getInteriorManager().getSoundScheme().getTakeoff().time();
    }

    public boolean isLanding(){

        if(this.flightState != FlightState.FLYING)
            return false;

        if(this.ticksInVortexToLand <= 0)
            return false;

        return this.ticksInVortex < this.ticksInVortexToLand;
    }

    @Override
    public void flightTick() {
        ++this.ticksInVortex;

        //Do no further processing on client
        if(this.isClient()){
            return;
        }

        //If landing, stop further processing
        if(this.ticksInVortexToLand > 0 || this.flightState == FlightState.LANDED){
            if(this.ticksInVortex >= this.ticksInVortexToLand || ticksInVortexToLand <= 0)
                this.completeLanding();
            return;
        }

        //Subsystems
        for(Subsystem system : this.systems.values()){
            system.onFlightTick();
            if(system.getType().isRequiredForFlight && system.isBrokenOrOff()){
                this.crash(TardisLandingContext.basic(this));
            }
        }
        //Flight effects
        for(IFlightDurationEffect effect : this.effects){
            effect.onFlightTick(this);
        }

        //Buildable upgrades
        for(Supplier<Boolean> sup : this.flightTickers){
            //If this returns false, crash
            if(!sup.get()){
                this.crash(TardisLandingContext.basic(this));
            }
        }

        //Get VPs in range and make them affect the tardis if they should
        if(getLevel() instanceof ServerLevel level){
            TardisLevelData.get(level).doVortexPhenomenaStuff(this);
        }

        boolean isStabilized = this.getControlDataOrCreate(ControlRegistry.STABILIZERS.get()).get() &&
                SubsystemRegistry.STABILIZERS.get().canBeUsed(this);

        //Travel Distance every second
        if(this.ticksInVortex % 20 == 0 &&
                this.flightCourse.isPresent() &&
                !this.flightCourse.get().isComplete(this.distanceTraveled) &&
                !this.isTakingOff()){

            this.hasFailedToLand = false;
            this.distanceTraveled += this.calcTravelSpeed();
            this.setFlightState(this.ticksInVortex, this.ticksInVortexToLand, this.distanceTraveled, this.flightState, this.flightCourse);
            if(!this.getFuelHandler().flightTick(this.calcTravelSpeed())){
                crash(TardisLandingContext.basic(this));
            }
        }

        //Automatically land when destination is reached if not stabilized
        if(isStabilized){
            if(this.flightCourse.isEmpty() || this.flightCourse.get().isComplete(this.distanceTraveled) && this.flightState == FlightState.FLYING && !this.hasFailedToLand){
                initLanding(false);
            }
        }

        //Complete flight events that have been resolved
        if(flightEventTicksExpire < this.ticksInVortex){

            //If there were no previous events, just start a new one
            if(this.flightEvent.isEmpty()){
                startFlightEvent();
                return;
            }

            this.flightEvent.ifPresent(event -> {
                //If there is a current event, fail if it hasn't been completed
                if(!event.isComplete()){
                    event.onFail();
                }
                startFlightEvent();
            });
        }

    }

    @Override
    public InteriorManager getInteriorManager() {
        return this.interiorManager;
    }

    @Override
    public TardisEngine getEngine() {
        return this.engine;
    }

    @Override
    public FuelHandler getFuelHandler() {
        return this.fuelHandler;
    }

    @Override
    public EmotionalHandler getEmotionalHandler() {
        return this.emotionalHandler;
    }

    @Override
    public FlightState getFlightState() {
        return this.flightState;
    }

    @Override
    public TardisUnlockManager getUnlockHandler() {
        return this.unlockHandler;
    }

    @Override
    public ExteriorDataHandler getExteriorExtraData() {
        return this.exteriorDataHandler;
    }

    @Override
    public <D extends ControlData<?>> D getControlDataOrCreate(ControlType<D> type) {
        Objects.requireNonNull(type); //Throw an error if the type is null
        if(!this.controlData.containsKey(type)){
            D data = type.createData(this);
            this.controlData.put(type, data);
            return data;
        }

        return (D)this.controlData.get(type);
    }

    @Override
    public boolean isClient(){
        return this.level.isClientSide;
    }

    @Override
    public void tick() {
        this.animationTicks++;

        if(this.firstTick && this.isClient()){
            this.firstTick = false;
            for(ControlData<?> data : this.controlData.values()){
                data.playAnimation((int)animationTicks);
            }
        }

        for(UpgradeType<?, ?> type : UpgradeRegistry.REGISTRY.get()){
            if(type instanceof TardisUpgradeType<?> tardisType){
                if(tardisType.canBeUsed(this)){
                    this.getUpgrade(tardisType).ifPresent(upgrade -> {
                        if(upgrade.canBeUsed()) {
                            upgrade.onTick();
                        }
                    });
                }
            }
        }

        this.getInteriorManager().tick();
        this.getEmotionalHandler().tick();
        this.getEngine().tick();
        this.getExteriorExtraData().tick();

        if(this.flightState.isFlying()) {
            this.flightTick();
        }
        else {
            ControlDataBool isRefuling = this.getControlDataOrCreate(ControlRegistry.REFUELER.get());

            if(isRefuling.get()){
                if(!SubsystemRegistry.FLUID_LINK_TYPE.get().canBeUsed(this)){
                    this.getControlDataOrCreate(ControlRegistry.REFUELER.get()).set(false);
                }
                else this.getFuelHandler().refuelTick();
            }

        }

    }

    @Override
    public void playSoundToAll(SoundEvent event, SoundSource source, float vol) {
        if(!this.isClient()){
            for(Player player : this.getLevel().players()){
                this.getLevel().playSound(null, player.getOnPos(), event, source, vol, 1.0F);
            }
        }
    }

    @Override
    public void setFlightState(int vortexTicks, int landTick, float distanceTraveled, FlightState state, Optional<FlightCourse> course) {
        this.ticksInVortex = vortexTicks;
        this.ticksInVortexToLand = landTick;
        this.distanceTraveled = distanceTraveled;
        this.flightState = state;
        this.flightCourse = course;
        if(!this.level.isClientSide()){
            Network.sendPacketToDimension(this.level.dimension(), new TardisFlightStateMessage(this.ticksInVortex, this.ticksInVortexToLand, this.distanceTraveled, this.flightState, this.flightCourse));

            if(flightState == FlightState.SEARCHING_FOR_LANDING)
                this.vortexTicksLandingSearchStart = vortexTicks;
        }

    }

    @Override
    public void update(TardisDataType<?> type) {
        if(level != null && !level.isClientSide) {
            Network.sendPacketToDimension(this.level.dimension(), new TardisUpdateMessage(type.create(this)));
        }
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();

        tag.put("location", this.location.serializeNBT());
        tag.put("destination", this.destination.serializeNBT());
        this.flightCourse.ifPresent(course -> tag.put("flight_course", course.serializeNBT()));
        tag.putInt("flight_ticks", this.ticksInVortex);
        tag.putInt("flight_state", this.flightState.ordinal());
        tag.putFloat("distance_traveled", this.distanceTraveled);
        tag.putString("exterior", ExteriorRegistry.REGISTRY.get().getKey(this.getExterior().getType()).toString());
        tag.put("exterior_data", this.exterior.serializeNBT());
        tag.put("interior_manager", this.interiorManager.serializeNBT());
        tag.put("emotional", this.emotionalHandler.serializeNBT());
        tag.put("fuel", this.fuelHandler.serializeNBT());
        tag.put("engine", this.engine.serializeNBT());
        tag.put("exterior_extra", this.exteriorDataHandler.serializeNBT());
        tag.put("communicator", this.communicator.serializeNBT());
        this.lastPilot.ifPresent(id -> tag.putUUID("last_pilot", id));
        tag.put("unlocks", this.unlockHandler.serializeNBT());
        tag.putString("tardis_name", this.getTardisName());
        //Control Saving
        ListTag controlTag = new ListTag();
        for(ControlData<?> data : this.controlData.values()){
            CompoundTag control = data.serializeNBT();
            if(control != null && !control.isEmpty()){
                control.putString("id", ControlRegistry.REGISTRY.get().getKey(data.getType()).toString());
                controlTag.add(control);
            }
        }
        tag.put("controls", controlTag);

        //Subsystem saving
        ListTag subsystemTag = new ListTag();
        for(Subsystem system : this.systems.values()){
            subsystemTag.add(system.serializeNBT());
        }
        tag.put("subsystems", subsystemTag);

        //Upgrades
        ListTag upgradeList = new ListTag();
        for(Upgrade<?> upgrades : this.upgrades.values()){
            upgradeList.add(upgrades.serializeNBT());
        }
        tag.put("upgrades", upgradeList);

        MinecraftForge.EVENT_BUS.post(new TardisEvent.TardisSaveEvent(this, tag));
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        systems.clear();
        upgrades.clear();

        this.location = SpaceTimeCoord.of(nbt.getCompound("location"));
        this.destination = SpaceTimeCoord.of(nbt.getCompound("destination"));
        if(nbt.contains("flight_course")){
            FlightCourse course = new FlightCourse();
            course.deserializeNBT(nbt.getList("flight_course", Tag.TAG_COMPOUND));
            this.flightCourse = Optional.of(course);
        }
        this.ticksInVortex = nbt.getInt("flight_ticks");
        this.flightState = FlightState.values()[nbt.getInt("flight_state")];
        if(this.flightState == FlightState.SEARCHING_FOR_LANDING)
            this.flightState = FlightState.FLYING;
        this.distanceTraveled = nbt.getFloat("distance_traveled");
        this.setExterior(ExteriorRegistry.REGISTRY.get().getValue(new ResourceLocation(nbt.getString("exterior"))), false);
        this.exterior.deserializeNBT(nbt.getCompound("exterior_data"));
        this.interiorManager.deserializeNBT(nbt.getCompound("interior_manager"));
        this.emotionalHandler.deserializeNBT(nbt.getCompound("emotional"));
        if(nbt.contains("fuel") && nbt.get("fuel") instanceof FloatTag tag)
            this.fuelHandler.deserializeNBT(tag);
        this.communicator.deserializeNBT(nbt.getCompound("communicator"));
        this.engine.deserializeNBT(nbt.getCompound("engine"));
        this.exteriorDataHandler.deserializeNBT(nbt.getCompound("exterior_extra"));
        this.lastPilot = Helper.readOptionalNBT(nbt, "last_pilot", CompoundTag::getUUID);
        this.unlockHandler.deserializeNBT(nbt.getList("unlocks", Tag.TAG_COMPOUND));
        this.tardisName = nbt.getString("tardis_name");

        //Control data
        ListTag controlTag = nbt.getList("controls", Tag.TAG_COMPOUND);
        for(Tag tag : controlTag){
            CompoundTag controlData = (CompoundTag) tag;
            ControlType<?> type = ControlRegistry.REGISTRY.get().getValue(new ResourceLocation(controlData.getString("id")));
            if(type != null){
                this.getControlDataOrCreate(type).deserializeNBT(controlData);
            }
        }

        //Subsystem
        this.createSubsystems();
        ListTag subsystemTagList = nbt.getList("subsystems", Tag.TAG_COMPOUND);
        for(int i = 0; i < subsystemTagList.size(); ++i){
            CompoundTag tag = subsystemTagList.getCompound(i);
            Helper.readRegistryFromString(tag, SubsystemRegistry.TYPE_REGISTRY.get(), "type").ifPresent(type -> {
                this.getSubsystem(type).ifPresent(sys -> {
                    sys.deserializeNBT(tag);
                });
            });
        }

        //Upgrades
        ListTag upgradeList = nbt.getList("upgrades", Tag.TAG_COMPOUND);
        for(int i = 0; i < upgradeList.size(); ++i){
            final CompoundTag tag = upgradeList.getCompound(i);
            Helper.readRegistryFromString(tag, UpgradeRegistry.REGISTRY.get(), "type").ifPresent(type -> {
                if(type instanceof TardisUpgradeType<?> tType){
                    final BaseTardisUpgrade upgrade = tType.create(this);
                    upgrade.deserializeNBT(tag);
                    this.upgrades.put(tType, upgrade);
                }
            });
        }

        MinecraftForge.EVENT_BUS.post(new TardisEvent.TardisLoadEvent(this, nbt));
    }

    @Override
    public List<Upgrade<ITardisLevel>> getUpgrades() {
        return Lists.newArrayList(this.upgrades.values());
    }

    public void createSubsystems(){
        this.systems.clear();
        for(SubsystemType<?> type : SubsystemRegistry.TYPE_REGISTRY.get()){
            type.createSubsystem(this).ifPresent(sys -> {
                this.systems.put(type, sys);
            });
        }
    }

    @Override
    public Optional<UUID> getLastPilot() {
        return this.lastPilot;
    }

    @Override
    public void setLastPilot(Player player) {
        this.setLastPilot(Optional.of(player.getUUID()));
    }

    @Override
    public void setLastPilot(Optional<UUID> id) {
        this.lastPilot = id;
    }

    @Override
    public <T extends Subsystem> Optional<T> getSubsystem(SubsystemType<T> type) {

        if(!this.systems.containsKey(type)){

            //Try to create the subsystem if it's not there
            type.createSubsystem(this).ifPresent(sys -> {
                this.systems.put(type, sys);
            });
        }

        //If this subsystem still doesn't exist after attempting to create it
        if(!this.systems.containsKey(type)){
            return Optional.empty();
        }

        Subsystem s = this.systems.get(type);

        if(s.isBroken()){
            this.systems.remove(type);
            return Optional.empty();
        }

        return Optional.of((T)s);
    }

    @Override
    public <T extends Upgrade<ITardisLevel>> Optional<T> getUpgrade(UpgradeType<ITardisLevel, T> type) {
        if(this.upgrades.containsKey(type)) {
            if(!this.upgrades.get(type).getItem().isEmpty()) {
                return Optional.of((T) this.upgrades.get(type));
            }
            //If the item attached to the upgrade does not exist, remove it from the list
            this.upgrades.remove(type);
            return Optional.empty();
        }

        //Attempt to create this upgrade
        if(type instanceof TardisUpgradeType<? extends BaseTardisUpgrade> tt){
            BaseTardisUpgrade upgrade = tt.create(this);
            this.upgrades.put(tt, upgrade);
            return Optional.of((T)upgrade);
        }

        return Optional.empty();
    }

    //Adds effects that last for the duration of the current flight
    @Override
    public void addFlightDurationEffect(IFlightDurationEffect effect){
        this.effects.add(effect);
    }

    public static boolean IsFlightEventActive(Optional<FlightEvent> currEvent){
        if(currEvent.isEmpty())
            return false;
        return !currEvent.get().isComplete();
    }
}
