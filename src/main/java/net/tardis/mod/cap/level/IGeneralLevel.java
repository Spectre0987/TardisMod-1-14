package net.tardis.mod.cap.level;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.misc.SpaceTimeCoord;

import java.util.Optional;

public interface IGeneralLevel extends INBTSerializable<CompoundTag> {

    void addArtronTrail(ITardisLevel tardis);
    Optional<SpaceTimeCoord> getArtronTrail(ResourceKey<Level> tardis);


    void tick();
}
