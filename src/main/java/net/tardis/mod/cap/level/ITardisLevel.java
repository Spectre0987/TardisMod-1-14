package net.tardis.mod.cap.level;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.IUpgradeable;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.misc.landing.TardisLandingContext;
import net.tardis.mod.misc.tardis.*;
import net.tardis.mod.emotional.EmotionalHandler;
import net.tardis.mod.network.packets.tardis.TardisData;
import net.tardis.mod.network.packets.tardis.TardisDataType;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;
import net.tardis.mod.upgrade.Upgrade;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/** An attachment to Levels to handle most Tardis logic.*/
public interface ITardisLevel extends INBTSerializable<CompoundTag>, IUpgradeable<ITardisLevel> {

    //Getters and setters

    Level getLevel();
    SpaceTimeCoord getLocation();
    SpaceTimeCoord getDestination();

    /**
     *
     * @return The current Flight Course this TARDIS is Programmed with
     */
    Optional<FlightCourse> getCurrentCourse();

    /**
     *
     * @return Friendly name for the TARDIS
     */
    String getTardisName();

    /**
     * How far (Block distance) this TARDIS has traveled along it's path
     * @return
     */
    float getDistanceTraveled();

    /**
     * If this tardis is landing, this does nothing
     *
     * If this tardis is in flight, the supplied coord will be added to the end of the current {@link FlightCourse}
     *
     * @param coord - Coord to set the current end point to
     */
    void setDestination(SpaceTimeCoord coord);

    /**
     * See {@link ITardisLevel#setDestination(SpaceTimeCoord)}
     */
    void setDestination(ResourceKey<Level> level, BlockPos pos);

    /**
     *  Don't touch this, it will break shit if you don't know what you're doing
     * @param coord
     */
    void setLocation(SpaceTimeCoord coord);
    void setExterior(ExteriorType type, boolean placeBlocks);
    void setCurrentFlightEvent(@Nullable FlightEventType type);
    Exterior getExterior();
    void addFlightDurationEffect(IFlightDurationEffect effect);

    /**
     *
     * @return How many ticks this TARDIS has been in flight overall
     */
    int ticksInVortex();

    /**
     *
     * @return - True if the TARDIS is traveling through the vortex
     */
    boolean isTraveling();

    /**
     *
     * @return True if the TARDIS is curently in the vortex whether or not it is traveling
     */
    boolean isInVortex();
    boolean isTakingOffOrLanding();


    boolean isClient();

    Optional<FlightEvent> getCurrentFlightEvent();

    //Functional

    void takeoff();

    /**
     * Attempts to land the TARDIS with the supplied Context
     * @param context - {@link TardisLandingContext} to try to land with
     * @return - false if we couldn't land
     */
    void initLanding(TardisLandingContext context, boolean force);
    void initLanding(boolean force);

    /**
     * Called once landing is locked in
     */
    void completeLanding();

    /**
     * Called once a tick for every tick this TARDIS is in the Vortex, moving or not
     */
    void flightTick();

    InteriorManager getInteriorManager();
    TardisEngine getEngine();
    FuelHandler getFuelHandler();
    EmotionalHandler getEmotionalHandler();

    FlightState getFlightState();
    TardisUnlockManager getUnlockHandler();
    ExteriorDataHandler getExteriorExtraData();

    <D extends ControlData<?>> D getControlDataOrCreate(ControlType<D> type);

    @Override
    List<Upgrade<ITardisLevel>> getUpgrades();

    <T extends Subsystem> Optional<T> getSubsystem(SubsystemType<T> type);

    /**
     * Builds list of subsystems
     */
    void createSubsystems();

    /**
     *
     * @return - The last player to have interacted with the console controls
     */
    Optional<UUID> getLastPilot();
    void setLastPilot(Player player);
    void setLastPilot(Optional<UUID> id);

    /**
     * Called every world tick
     */
    void tick();

    /**
     * Plays the given sound to everyone inside this TARDIS
     * @param event - {@link SoundEvent} to play
     * @param source - The sound channel to play this on
     * @param vol - The Volume at which to play this
     */
    void playSoundToAll(SoundEvent event, SoundSource source, float vol);

    /**
     * Ooof, ouch, owie
     * Damages a TARDIS' Subsystems, splitting the damage evenly across them, unless a shield is installed, in which case it will tank the damage for it
     * @param source - Damage source, currently unused
     * @param amount - How much to hurt the subsystems collectively, amount / subsystem installed, but at least 1
     */
    void damageTardis(DamageSource source, int amount);

    /**
     * Set basic flight data and send it to clients, if called on the client, just set them
     */
    void setFlightState(int flightTicks, int landTick, float distanceTraveled, FlightState state, Optional<FlightCourse> course);

    /**
     * Sends a {@link TardisData} to all clients inside this TARDIS
     * Handles side checking and all that jazz
     * @param data
     */
    void update(TardisDataType<?> data);

    /**
     * Like a landing, but violent
     * @param context
     */
    void crash(TardisLandingContext context);

    /**
     * Sends a notification / event
     * Networking /Side checks automatically handled
     * @param notificationId
     */
    void sendNotification(int notificationId);

    /**
     * Remove all old notifications and add return active ones as a list
     * @return
     */
    List<TardisNotifications.TardisNotif> getActiveNotifications();

    ResourceKey<Level> getId();

    void setFlightCourse(FlightCourse newCourse);

    void addTicker(Runnable ticker);
    void addFlightTicker(Runnable ticker);

    void playCloister();

    long getAnimationTicks();

}


