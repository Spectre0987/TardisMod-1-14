package net.tardis.mod.cap.entities;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;

public interface ITardisPlayer extends INBTSerializable<CompoundTag> {

    void startShaking(int power, int ticks);

    void tick();

}
