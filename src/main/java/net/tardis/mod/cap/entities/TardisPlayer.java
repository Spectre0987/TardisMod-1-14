package net.tardis.mod.cap.entities;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.StartShakeMessage;
import org.apache.logging.log4j.core.jmx.Server;

public class TardisPlayer implements ITardisPlayer{

    final Player player;
    int shakeTicks = 0;
    int shakePower = 0;

    public TardisPlayer(Player player){
        this.player = player;
    }

    @Override
    public void startShaking(int power, int ticks) {
        this.shakePower = power;
        this.shakeTicks = ticks;

        if(!player.level.isClientSide){
            Network.sendTo((ServerPlayer) player, new StartShakeMessage(power, ticks));
        }
    }

    @Override
    public void tick() {
        if(this.shakeTicks > 0){
            --this.shakeTicks;

            player.setYHeadRot(player.getYHeadRot() + Helper.randomIntWithNegative(player.getRandom(), this.shakePower));
            player.setXRot(player.getXRot() + Helper.randomIntWithNegative(player.getRandom(), this.shakePower));

        }
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
