package net.tardis.mod.cap.chunks;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.rifts.Rift;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface IChunkCap extends INBTSerializable<CompoundTag> {

    Optional<Rift> getRift();
    void onGenerated();
    void tick();

    LevelChunk getChunk();

    void setRift(@Nullable Rift rift);
}
