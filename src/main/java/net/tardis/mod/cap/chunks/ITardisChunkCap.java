package net.tardis.mod.cap.chunks;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;

public interface ITardisChunkCap extends INBTSerializable<CompoundTag> {

    void addTardisLightBlock(BlockPos pos);
    void updateLighting();

}
