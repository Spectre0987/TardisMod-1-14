package net.tardis.mod.cap.chunks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Tags;
import net.tardis.api.events.RiftEvent;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.RiftMessage;
import net.tardis.mod.tags.BiomeTags;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ChunkCapability implements IChunkCap{

    private static final int RIFT_CHANCE = 1; //1% chance to be a rift

    private final LevelChunk chunk;
    private Optional<Rift> rift = Optional.empty();


    public ChunkCapability(LevelChunk chunk){
        this.chunk = chunk;
    }

    @Override
    public Optional<Rift> getRift() {
        return this.rift;
    }

    @Override
    public void onGenerated() {

        final RandomSource rand = this.chunk.getLevel().random;

        //Stop parsing in blocked biomes
        Holder<Biome> biome = this.chunk.getLevel().getBiome(this.getChunk().getPos().getMiddleBlockPosition(100));
        if(biome.is(BiomeTags.TARDIS_GEN_BLACKLIST) || biome.is(Tags.Biomes.IS_VOID)){
            return;
        }

        //Generate a rift
        if(rand.nextInt(1000) <= RIFT_CHANCE){

            final int relX = rand.nextInt(16),
                relZ = rand.nextInt(16);

            this.setRift(
                    new Rift(this.chunk,
                            new BlockPos(
                                relX,
                                this.chunk.getHeight(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, relX, relZ),
                                relZ
                            )
                    )
            );
        }
    }

    @Override
    public void tick() {
        this.rift.ifPresent(r -> {
            if(r.isClosed()){
                MinecraftForge.EVENT_BUS.post(new RiftEvent.Closed(this.chunk.getLevel(), this.chunk.getPos(), r));
                this.setRift(null);
            }
            else r.tick(this.chunk.getLevel());
        });

    }

    @Override
    public LevelChunk getChunk() {
        return this.chunk;
    }

    @Override
    public void setRift(@Nullable Rift rift) {
        final Rift oldRift = this.rift.isPresent() ? this.rift.get() : null;
        this.rift = Helper.nullableToOptional(rift);
        this.rift.ifPresent(Rift::onAdded);

        if(oldRift != null){
            oldRift.onClosed(false);
        }

        if(!this.chunk.getLevel().isClientSide()){
            boolean hasRift = rift != null;
            Network.sendToTracking(this.chunk, new RiftMessage(this.chunk.getPos(), hasRift, hasRift ? rift.serializeNBT() : null));
        }
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        this.rift.ifPresent(r -> tag.put("rift", r.serializeNBT()));
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

        this.rift = Helper.readOptionalNBT(tag, "rift", (t, s) ->
                new Rift(this.chunk, t.getCompound(s))
        );
    }
}
