package net.tardis.mod.cap;

import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;

import java.util.List;
import java.util.Optional;

public interface IHaveItemFunctions<C, T extends ItemFunctionType<C>> {

    ItemFunction<C> setFunction(T type);
    Optional<ItemFunction<C>> getCurrentFunction();
    Optional<ItemFunction<C>> getFunctionByType(T type);

    List<T> getAllValidTypes();

}
