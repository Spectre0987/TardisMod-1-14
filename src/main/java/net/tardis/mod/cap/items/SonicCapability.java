package net.tardis.mod.cap.items;

import com.google.common.collect.Lists;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.sonic.AbstractSonicFunction;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.item.SonicUpgradeItem;
import net.tardis.mod.registry.SonicPartRegistry;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;
import net.tardis.mod.upgrade.SonicUpgrade;
import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.types.SonicUpgradeType;
import net.tardis.mod.upgrade.types.UpgradeType;
import org.jetbrains.annotations.Nullable;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class SonicCapability implements ISonicCapability {

    final ItemStack stack;
    final EnumMap<SonicPartSlot, SonicPartRegistry.SonicPartType> parts = new EnumMap<>(SonicPartSlot.class);
    final HashMap<SonicUpgradeType<?>, SonicUpgrade> upgrades = new HashMap<>();
    Optional<SonicFunctionType> currentFunctionType = Optional.empty();
    final ItemStackHandler upgradeInventory = new ItemStackHandler(6);
    private int energy = 0;
    public HashMap<SonicFunctionType, AbstractSonicFunction> functions = new HashMap<>();
    private Optional<ResourceKey<Level>> boundTardis = Optional.empty();

    public SonicCapability(ItemStack stack){
        this.stack = stack;
    }

    @Override
    public SonicPartRegistry.SonicPartType getSonicPart(SonicPartSlot slot) {
        return this.parts.getOrDefault(slot, SonicPartRegistry.MK1.get(slot).get());
    }

    @Override
    public void setSonicPart(SonicPartSlot slot, SonicPartRegistry.SonicPartType point) {
        this.parts.put(slot, point);
    }

    @Override
    public InteractionResultHolder<ItemStack> onUse(Level level, Player player, InteractionHand hand) {
        this.getCurrentFunction().ifPresent(func -> {
            func.onUse(player, player.getItemInHand(hand), hand);
        });
        return new InteractionResultHolder<>(InteractionResult.sidedSuccess(level.isClientSide), player.getItemInHand(hand));
    }

    @Override
    public void useOn(UseOnContext pContext) {
        this.getCurrentFunction().ifPresent(func -> {
            func.onRightClickBlock(pContext);
        });
    }

    @Override
    public ItemStack getItem() {
        return this.stack;
    }

    @Override
    public ItemStackHandler getUpgradeInv() {
        return this.upgradeInventory;
    }

    @Override
    public Optional<ResourceKey<Level>> getBoundTARDIS() {
        return this.boundTardis;
    }

    @Override
    public void setBoundTARDIS(@Nullable ResourceKey<Level> tardis) {
        this.boundTardis = Helper.nullableToOptional(tardis);

    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        this.currentFunctionType.ifPresent(func -> {
            Helper.writeRegistryToNBT(tag, ItemFunctionRegistry.REGISTRY.get(), func, "current_func_type");
        });
        tag.put("upgrade_inv", this.upgradeInventory.serializeNBT());

        final CompoundTag partsTag = new CompoundTag();
        for(SonicPartSlot slot : SonicPartSlot.values()){
            Helper.writeRegistryToNBT(partsTag, SonicPartRegistry.REGISTRY.get(), this.getSonicPart(slot), slot.name().toLowerCase());
        }
        tag.put("parts", partsTag);
        tag.putInt("energy", this.energy);
        this.boundTardis.ifPresent(key -> tag.putString("bound_tardis", key.location().toString()));

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.upgrades.clear();
        this.parts.clear();

        if(nbt.contains("current_func_type"))
            this.currentFunctionType = Helper.readRegistryFromString(nbt, ItemFunctionRegistry.REGISTRY.get(), "current_func_type")
                    .map(v -> (SonicFunctionType)v);
        this.upgradeInventory.deserializeNBT(nbt.getCompound("upgrade_inv"));

        final CompoundTag parts = nbt.getCompound("parts");
        for(SonicPartSlot slot : SonicPartSlot.values()){
            Helper.readRegistryFromString(parts, SonicPartRegistry.REGISTRY.get(), slot.name().toLowerCase()).ifPresent(part -> {
                this.setSonicPart(part.getSlot(), part);
            });
        }
        this.energy = nbt.getInt("energy");
        this.boundTardis = Helper.readOptionalNBT(nbt, "bound_tardis", (t, n) -> ResourceKey.create(Registries.DIMENSION, new ResourceLocation(t.getString(n))));
    }

    @Override
    public ItemFunction<ISonicCapability> setFunction(SonicFunctionType type) {
        this.currentFunctionType = Helper.nullableToOptional(type);
        return getFunctionByType(type).get();
    }

    @Override
    public Optional<ItemFunction<ISonicCapability>> getCurrentFunction() {
        return this.currentFunctionType.map(t -> getFunctionByType(t).get());
    }

    @Override
    public Optional<ItemFunction<ISonicCapability>> getFunctionByType(SonicFunctionType type) {
        if(this.functions.containsKey(type)){
            return Optional.of(this.functions.get(type));
        }
        //Create if absent
        ItemFunction<ISonicCapability> func = type.create(this);
        if(func instanceof AbstractSonicFunction t){
            this.functions.put(type, t);
            return Optional.of(t);
        }
        return Optional.empty();
    }

    @Override
    public List<SonicFunctionType> getAllValidTypes() {
        return ItemFunctionRegistry.getAllFor(SonicFunctionType.class);
    }

    @Override
    public List<Upgrade<ISonicCapability>> getUpgrades() {
        return Lists.newArrayList(this.upgrades.values());
    }

    @Override
    public <U extends Upgrade<ISonicCapability>> Optional<U> getUpgrade(UpgradeType<ISonicCapability, U> type) {
        if(upgrades.containsKey(type)){
            return Optional.of((U)this.upgrades.get(type));
        }
        //Else if it can be created from items
        for(int i = 0; i < this.upgradeInventory.getSlots(); ++i){
            final ItemStack stack = this.upgradeInventory.getStackInSlot(i);
            if(stack.getItem() instanceof SonicUpgradeItem<?> item && item.getUpgradeType() == type){
                SonicUpgrade up = (SonicUpgrade) type.create(this);
                this.upgrades.put((SonicUpgradeType<?>) type, up);
                return Optional.of((U)up);
            }
        }
        return Optional.empty();
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        final int take = Math.min(maxReceive, this.getMaxEnergyStored() - this.getEnergyStored());
        if(!simulate)
            this.energy += take;
        return take;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        final int take = Math.max(maxExtract, 0);
        if(!simulate){
            this.energy -= take;
        }
        return take;
    }

    @Override
    public int getEnergyStored() {
        return this.energy;
    }

    @Override
    public int getMaxEnergyStored() {
        return 1000;
    }

    @Override
    public boolean canExtract() {
        return false;
    }

    @Override
    public boolean canReceive() {
        return true;
    }
}
