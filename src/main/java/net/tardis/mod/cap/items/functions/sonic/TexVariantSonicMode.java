package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.misc.ITextureVariantBlockEntity;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;
import net.tardis.mod.registry.JsonRegistries;

public class TexVariantSonicMode extends AbstractSonicFunction{
    public TexVariantSonicMode(ItemFunctionType<ISonicCapability> type, ISonicCapability parent) {
        super(type, parent);
    }

    @Override
    int getPowerCost() {
        return 0;
    }

    @Override
    public int timeToRun() {
        return -1;
    }

    @Override
    public boolean shouldDisplay(ISonicCapability parent) {
        return true;
    }

    @Override
    public boolean isInstant() {
        return true;
    }

    @Override
    public void onRightClickBlock(UseOnContext context) {
        if(!context.getLevel().isClientSide() && context.getLevel().getBlockEntity(context.getClickedPos()) instanceof ITextureVariantBlockEntity t){
            final ResourceKey<? extends BlockEntityType<?>> id = ForgeRegistries.BLOCK_ENTITY_TYPES.getResourceKey(context.getLevel().getBlockEntity(context.getClickedPos()).getType()).get();
            final Registry<TextureVariant> varReg = context.getLevel().registryAccess().registry(JsonRegistries.TEXTURE_VARIANTS).get();

            Network.sendTo((ServerPlayer) context.getPlayer(), new OpenGuiDataMessage(GuiDatas.SONIC_TEX_VAR.create()
                    .withVariants(context.getClickedPos(),
                            context.getLevel().getBlockState(context.getClickedPos()),
                            varReg.stream()
                                    .filter(v -> v.key().equals(id))
                                    .map(v -> varReg.getKey(v))
                                    .toList()
                    )));
        }
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
