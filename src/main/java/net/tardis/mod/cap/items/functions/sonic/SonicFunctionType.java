package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class SonicFunctionType extends ItemFunctionType<ISonicCapability> {

    final ItemStack display;

    public SonicFunctionType(Predicate<ItemStack> itemToApplyTo, BiFunction<ItemFunctionType<ISonicCapability>, ISonicCapability, ? extends ItemFunction<ISonicCapability>> builder, ItemStack display) {
        super(itemToApplyTo, builder);
        this.display = display;
    }

    public ItemStack getDisplayItem(){
        return this.display;
    }
}
