package net.tardis.mod.cap.items;

import net.minecraft.nbt.FloatTag;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.api.artron.IArtronStorage;

public interface IItemArtronTankCap extends IArtronStorage, INBTSerializable<FloatTag> {
}
