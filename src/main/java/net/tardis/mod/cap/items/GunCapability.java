package net.tardis.mod.cap.items;

import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.Function;

public class GunCapability implements IGunCapability{

    private final int shots;
    private final int cooldown;
    private final int maxEnergy;
    private final int energyPerShot;
    private final Function<Player, ? extends Projectile> projectileFactory;
    private final ItemStack stack;
    private int energy;


    public GunCapability(ItemStack stack, int maxEnergy, int energyPerShot, int shots, int cooldown, @Nullable Function<Player, ? extends Projectile> projectileFactory){
        this.stack = stack;
        this.shots = shots;
        this.energyPerShot = energyPerShot;
        this.maxEnergy = maxEnergy;
        this.cooldown = cooldown;
        this.projectileFactory = projectileFactory;
    }

    public GunCapability(ItemStack stack, int maxEnergy, int energyPerShot, int shots, int cooldown){
        this(stack, maxEnergy, energyPerShot, shots, cooldown, null);
    }

    @Override
    public ItemStack getItem() {
        return this.stack;
    }

    @Override
    public int shotsBeforeCooldown() {
        return this.shots;
    }

    @Override
    public int cooldownTime() {
        return this.cooldown;
    }

    @Override
    public boolean shoot(Player shooter, InteractionHand hand) {
        if(this.getEnergyStored() >= this.energyPerShot){
            this.extractEnergy(this.energyPerShot, false);

            if(!shooter.level.isClientSide){
                //For rays
                if(this.projectileFactory == null){
                    Optional<LivingEntity> targetHolder = WorldHelper.rayTrace(shooter, LivingEntity.class, 64);
                    targetHolder.ifPresent(target -> {
                        target.hurt(new DamageSource(shooter.level.registryAccess().registry(Registries.DAMAGE_TYPE).get().getHolderOrThrow(DamageTypes.PLAYER_ATTACK)), 15.0F);
                    });
                    return true;
                }
                Projectile proj = this.projectileFactory.apply(shooter);
                shooter.level.addFreshEntity(proj);
                return true;
            }

        }
        return false;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putInt("power", this.energy);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.energy = tag.getInt("power");
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        final int taken = Math.min(maxReceive, this.getMaxEnergyStored() - this.energy);
        if(!simulate)
            this.energy += taken;
        return taken;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        final int given = Math.min(maxExtract, this.energy);
        if(!simulate)
            this.energy -= given;
        return given;
    }

    @Override
    public int getEnergyStored() {
        return this.energy;
    }

    @Override
    public int getMaxEnergyStored() {
        return this.maxEnergy;
    }

    @Override
    public boolean canExtract() {
        return false;
    }

    @Override
    public boolean canReceive() {
        return true;
    }
}
