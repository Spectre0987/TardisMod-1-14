package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.ItemFunctionOpenGuiBase;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;

import java.util.Optional;
import java.util.function.BiFunction;

public class CFLSubsystemInfoFunc extends ItemFunctionOpenGuiBase<ICFLTool> {

    public CFLSubsystemInfoFunc(ItemFunctionType<ICFLTool> type, ICFLTool parent) {
        super(type, parent);
    }
    @Override
    public BiFunction<Player, InteractionHand, Optional<GuiData>> guiToOpen() {
        return (player, hand) -> {

            ResourceKey<Level> boundTardis = this.getParent().getBoundTARDISKey().get();
            ServerLevel tardisLevel = player.level.getServer().getLevel(boundTardis);
            LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, tardisLevel);
            if(tardisHolder.isPresent()){
                return Optional.of(
                        GuiDatas.CFL_SUBSYSTEM_INFO.create()
                                .fromTardis(tardisHolder.orElseThrow(NullPointerException::new))
                                .setHand(hand)
                );
            }

            return Optional.empty();
        };
    }

    @Override
    public boolean shouldDisplay(ICFLTool parent) {
        return parent.getBoundTARDISKey().isPresent();
    }
}
