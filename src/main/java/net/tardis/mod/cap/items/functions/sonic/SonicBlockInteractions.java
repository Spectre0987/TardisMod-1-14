package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.TntBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SonicBlockInteractions {

    public static List<Interaction> INTERACTIONS = new ArrayList<>();

    public static Interaction TNT = register(state -> state.getBlock() instanceof TntBlock,
            (target, pos, sonic, level, player, hand) -> {
                if(target.getBlock() instanceof TntBlock tnt){
                    TntBlock.explode(level, pos);
                    level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
                    return true;
                }
                return false;
            }
    );
    public static Interaction TRAPDOOR = register(state -> state.getBlock() instanceof TrapDoorBlock,
            (target, pos, sonic, level, player, hand) -> {
                if(target.hasProperty(TrapDoorBlock.OPEN)){
                    level.setBlock(pos, target.cycle(TrapDoorBlock.OPEN), 3);
                    return true;
                }
                return false;
            });
    public static Interaction GLASS = register(state -> state.is(Tags.Blocks.GLASS),
            (target, pos, sonic, level, player, hand) -> {
                if(!level.isClientSide()){
                    level.destroyBlock(pos, true);
                }
                return true;
            }
    );

    /**
     * MODDERS! Call this in {@link net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent}!
     * @param shouldUseOn is this state is valid for this interaction
     * @param action what to do if this block is valid
     * @return {@link Interaction} that was registered
     */
    public static Interaction register(Predicate<BlockState> shouldUseOn, ISonicBlockAction action){
        final Interaction interaction = new Interaction(shouldUseOn, action);
        INTERACTIONS.add(interaction);
        return interaction;
    }



    public record Interaction(Predicate<BlockState> targetState, ISonicBlockAction action){}

    public interface ISonicBlockAction{
        /**
         *
         * @param target
         * @param pos
         * @param sonic
         * @param level
         * @param player
         * @param hand
         * @return True if this action succeeded, false if it failed
         */
        boolean useOnBlock(BlockState target, BlockPos pos, ItemStack sonic, Level level, Player player, InteractionHand hand);
    }
}
