package net.tardis.mod.cap.items.functions;

import com.mojang.datafixers.kinds.Const;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.Constants;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @param <T> what kind of capability to attach to
 */
public class ItemFunctionType<T> {

    final Predicate<ItemStack> itemToApplyTo;
    final BiFunction<ItemFunctionType<T>, T, ? extends ItemFunction<T>> builder;
    private Component defaultTranslation;


    public ItemFunctionType(Predicate<ItemStack> itemToApplyTo, BiFunction<ItemFunctionType<T>, T, ? extends ItemFunction<T>> builder){
        this.itemToApplyTo = itemToApplyTo;
        this.builder = builder;
    }

    public ItemFunction<T> create(T cap){
        return this.builder.apply(this, cap);
    }

    public boolean canApplyTo(ItemStack stack){
        return this.itemToApplyTo.test(stack);
    }

    public Component getDefaultTranslation(){
        if(this.defaultTranslation == null){
            this.defaultTranslation = Component.translatable(Constants.Translation.makeGenericTranslation(
                    ItemFunctionRegistry.REGISTRY.get(), this
            ));
        }
        return this.defaultTranslation;
    }

}
