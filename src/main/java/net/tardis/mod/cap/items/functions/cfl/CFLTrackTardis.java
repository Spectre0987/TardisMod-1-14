package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ItemTrackingCoordMessage;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.Optional;

public class CFLTrackTardis extends ItemFunction<ICFLTool> implements ITrackingFunction{

    public static Component TRACKING_TITLE = Component.translatable("sonic.tracking.tardis")
            .withStyle(Style.EMPTY.withColor(Color.darkGray.getRGB()));

    private Optional<SpaceTimeCoord> trackingCoord = Optional.empty();


    public CFLTrackTardis(ItemFunctionType<ICFLTool> type, ICFLTool parent) {
        super(type, parent);
    }

    @Override
    public int timeToRun() {
        return -1;
    }

    @Override
    public boolean shouldDisplay(ICFLTool parent) {
        return parent.getBoundTARDISKey().isPresent();
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public void onSelected(Player player, InteractionHand hand) {
        super.onSelected(player, hand);

        this.getParent().getBoundTARDISKey().ifPresent(levelKey -> {
            if(player.getLevel() instanceof ServerLevel server){
                ServerLevel tardisLevel = server.getServer().getLevel(levelKey);
                if(tardisLevel != null){
                    tardisLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                        this.setTrackingPos(tardis.getLocation());
                        this.update(player, hand);
                    });
                }
            }
        });
    }

    @Override
    public Component getDisplayName() {
        return this.getType().getDefaultTranslation();
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.trackingCoord = Helper.readOptionalNBT(nbt, "tracking_coord", (t, n) -> SpaceTimeCoord.of(t.getCompound(n)));
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = super.serializeNBT();
        this.trackingCoord.ifPresent(c -> tag.put("tracking_coord", c.serializeNBT()));
        return tag;
    }

    @Override
    public Component getTrackingTypeTitle() {
        return TRACKING_TITLE;
    }

    @Override
    public Optional<SpaceTimeCoord> getTrackingPos() {
        return this.trackingCoord;
    }

    @Override
    public void setTrackingPos(@Nullable SpaceTimeCoord coord){
        if(coord == null){
            this.trackingCoord = Optional.empty();
        }
        this.trackingCoord = Optional.of(coord);
    }

    public void update(Player player, InteractionHand hand){
        if(!player.level.isClientSide()){
            Network.sendTo((ServerPlayer) player, new ItemTrackingCoordMessage(hand, this.trackingCoord));
        }
    }
}
