package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.config.Config;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.PlayMovingSoundMessage;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.tags.BlockTags;

public class BlockSonicFunction extends AbstractSonicFunction {

    public static final Component NAME = Component.translatable("item_functions." + Tardis.MODID + ".sonic.block");
    public static final int COST = 10;

    public BlockSonicFunction(ItemFunctionType<ISonicCapability> type, ISonicCapability parent) {
        super(type, parent);
    }

    @Override
    int getPowerCost() {
        return 10;
    }

    @Override
    public int timeToRun() {
        return -1;
    }

    @Override
    public boolean shouldDisplay(ISonicCapability parent) {
        return true;
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Component getDisplayName() {
        return NAME;
    }

    @Override
    public void onRightClickBlock(UseOnContext context) {
        final BlockState state = context.getLevel().getBlockState(context.getClickedPos());
        useSonicOnBlock(state, context.getLevel().clip(WorldHelper.clipContext(context.getPlayer(), 10)), context.getItemInHand(), context.getPlayer(), context.getHand());
    }

    public boolean useSonicOnBlock(BlockState targetState, BlockHitResult result, ItemStack sonic, Player player, InteractionHand hand){

        if(!validateAndUsePower(player)){
            return false;
        }

        //If blacklisted, stop all processing
        if(targetState.is(BlockTags.SONIC_BLACKLIST)){
            return false;
        }

        //For blocks implementing ISonicAction
        if(targetState.getBlock() instanceof SonicBlockInteractions.ISonicBlockAction act){
            if(act.useOnBlock(targetState, result.getBlockPos(), sonic, player.level, player, hand)){
                return true;
            }
            playMovingSound(player, false);
            return false;
        }

        //Process Special Interactions
        for(SonicBlockInteractions.Interaction i : SonicBlockInteractions.INTERACTIONS){
            if(i.targetState().test(targetState)){
                if(i.action().useOnBlock(targetState, result.getBlockPos(), sonic, player.level, player, hand)){
                    return true;
                }
                playMovingSound(player, false);
                return false;
            }
        }
        //Attempt to right-click normally
        final InteractionResult interactionResult = targetState.use(player.level, player, hand, result);

        return interactionResult != InteractionResult.FAIL;
    }

    @Override
    public void onUse(Player player, ItemStack stack, InteractionHand hand) {
        super.onUse(player, stack, hand);

        BlockHitResult result = player.level.clip(WorldHelper.clipContext(player, Config.Server.SONIC_INTERACT_DIST.get()));
        if(result != null && this.getParent().getEnergyStored() >= COST){
            useSonicOnBlock(player.level.getBlockState(result.getBlockPos()), result, stack, player, hand);
            playMovingSound(player, true);
        }
        else playMovingSound(player, false);

    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
