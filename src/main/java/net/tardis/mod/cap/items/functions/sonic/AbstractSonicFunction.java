package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.PlayMovingSoundMessage;
import net.tardis.mod.sound.SoundRegistry;

import javax.annotation.Nullable;

public abstract class AbstractSonicFunction extends ItemFunction<ISonicCapability> {

    public AbstractSonicFunction(ItemFunctionType<ISonicCapability> type, ISonicCapability parent) {
        super(type, parent);
    }

    abstract int getPowerCost();

    public boolean validateAndUsePower(@Nullable Player playerToNotify){
        if(getParent().getEnergyStored() >= this.getPowerCost()){
            getParent().extractEnergy(getPowerCost(), false);
            return true;
        }
        if(playerToNotify != null){
            playerToNotify.sendSystemMessage(Component.translatable(Constants.Translation.NOT_ENOUGH_POWER, getPowerCost()));
        }
        return false;
    }

    public void playMovingSound(Player player, boolean success){
        if(!player.level.isClientSide()){
            Network.sendToTracking(player, new PlayMovingSoundMessage(
                    success ? SoundRegistry.SONIC_INTERACT.get() : SoundRegistry.SONIC_FAIL.get(),
                    SoundSource.PLAYERS,
                    player.getId(),
                    1.0F, false
            ));
        }
    }

}
