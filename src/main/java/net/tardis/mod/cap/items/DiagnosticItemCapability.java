package net.tardis.mod.cap.items;

import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.tardis.mod.blockentities.BrokenExteriorTile;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.items.functions.cfl.CFLFunctionType;
import net.tardis.mod.cap.items.functions.cfl.ITrackingFunction;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;
import net.tardis.mod.sound.SoundRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class DiagnosticItemCapability implements ICFLTool {

    public static final double TARGET_ANGLE_LEEWAY = 10.0; // +- 10 degrees

    final ItemStack stack;
    Optional<ResourceKey<Level>> boundTardis = Optional.empty();
    Optional<ItemFunction<ICFLTool>> currentFunction = Optional.empty();

    boolean isPointedAtTarget = false;
    long trackingDetectedTime = 0l;

    final HashMap<CFLFunctionType, ItemFunction<ICFLTool>> functions = new HashMap<>();

    public DiagnosticItemCapability(ItemStack stack){
        this.stack = stack;
    }

    @Override
    public Optional<ResourceKey<Level>> getBoundTARDISKey() {
        return this.boundTardis;
    }

    @Override
    public void tick(Player player) {
        this.currentFunction.ifPresent(func -> func.tick(player));

        //Compass feature
        boolean currentlyDetecting = isPointingAtTarget(player);
        //If this has changed, update the variable
        if(currentlyDetecting != this.isPointedAtTarget){
            this.isPointedAtTarget = currentlyDetecting;
            if(currentlyDetecting){
                this.trackingDetectedTime = player.level.getGameTime();
            }
        }

        if(this.isPointedAtTarget && (player.level.getGameTime() - trackingDetectedTime) % 80 == 0){
            player.playSound(SoundRegistry.AXIS_CONTROL_SOUND.get());
        }

    }

    public boolean isPointingAtTarget(Player player){

        if(this.currentFunction.isEmpty()){
            return false;
        }
        ItemFunction<?> func = this.currentFunction.get();
        SpaceTimeCoord coord = null;

        if(func instanceof ITrackingFunction trackFunc){
            coord = trackFunc.getTrackingPos().orElse(null);
        }

        if(coord == null){
            return false;
        }

        //calculate tracking coord to work as a compass
        double minX = player.getX(),
                minZ = player.getZ(),
                maxX = coord.getPos().getX() + 0.5,
                maxZ = coord.getPos().getZ() + 0.5; //+0.5 to center the block pos

        double angle = correctAngle(Math.toDegrees(Math.atan2(minX - maxX, maxZ - minZ)));
        double playerAngle = correctAngle(player.yHeadRot);
        double angleDiff = angle - playerAngle;
        return playerAngle > angle - TARGET_ANGLE_LEEWAY && playerAngle < angle + TARGET_ANGLE_LEEWAY;
    }


    @Override
    public ItemStack getItem(){
        return this.stack;
    }

    public double correctAngle(double oldAngle){
        oldAngle = oldAngle % 360.0;
        if(oldAngle < 0){
            oldAngle += 360.0;
        }
        return oldAngle % 360.0;
    }

    @Override
    public InteractionResult useOn(UseOnContext context) {

        BlockEntity tile = context.getLevel().getBlockEntity(context.getClickedPos());
        if(tile instanceof BrokenExteriorTile broken){
            if(!context.getLevel().isClientSide()){
                Network.sendTo((ServerPlayer) context.getPlayer(), new OpenGuiDataMessage(GuiDatas.DIAG_BROKEN_INFO.create().setFromTile(broken)));
            }
            return InteractionResult.sidedSuccess(context.getLevel().isClientSide());
        }

        return InteractionResult.PASS;
    }

    @Override
    public void attuneTo(@Nullable ResourceKey<Level> dimension) {
        this.boundTardis = Helper.nullableToOptional(dimension);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player p, InteractionHand hand) {

        if(!world.isClientSide()){
            Network.sendTo((ServerPlayer) p, new OpenGuiDataMessage(GuiDatas.DIAGNOSTIC_MAIN.create().setTardis(this.boundTardis).setHand(hand)));
        }

        return InteractionResultHolder.pass(this.stack);
    }

    @Override
    public boolean isPointedAtTarget() {
        return this.isPointedAtTarget;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        this.boundTardis.ifPresent(tardis -> tag.putString("tardis", tardis.location().toString()));

        ListTag functionList = new ListTag();
        for(Map.Entry<CFLFunctionType, ItemFunction<ICFLTool>> f : this.functions.entrySet()){
            final ResourceLocation typeLoc = ItemFunctionRegistry.REGISTRY.get().getKey(f.getKey());
            CompoundTag fTag = f.getValue().serializeNBT();
            fTag.putString("type_id", typeLoc.toString());
            functionList.add(fTag);
        }
        tag.put("functions", functionList);
        this.currentFunction.ifPresent(func -> Helper.writeRegistryToNBT(tag, ItemFunctionRegistry.REGISTRY.get(), func.getType(), "current_function"));

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.fillFunctionList();

        this.boundTardis = Helper.readOptionalNBT(tag, "tardis", (t, k) -> ResourceKey.create(Registries.DIMENSION, new ResourceLocation(t.getString(k))));

        //Load Current Function
        Helper.readRegistryFromString(tag, ItemFunctionRegistry.REGISTRY.get(), "current_function").ifPresent(type -> {
            if(type instanceof CFLFunctionType t){
                this.setFunction(t);
            }
        });

        //Load all function data
        ListTag functionList = tag.getList("functions", ListTag.TAG_COMPOUND);
        for(int i = 0; i < functionList.size(); ++i){
            final CompoundTag fTag = functionList.getCompound(i);
            ResourceLocation typeLoc = new ResourceLocation(fTag.getString("type_id"));
            if(ItemFunctionRegistry.REGISTRY.get().getValue(typeLoc) instanceof CFLFunctionType type){
                this.getFunctionByType(type).ifPresent(func -> func.deserializeNBT(fTag));
            }
        }
    }

    public void fillFunctionList(){
        this.functions.clear();
        this.getAllValidTypes().stream().forEach(type -> {
            this.functions.put(type, type.create(this));
        });
    }

    @Override
    public ItemFunction<ICFLTool> setFunction(@Nullable CFLFunctionType type) {
        this.currentFunction.ifPresent(func -> {
            if(func.isCurrentlyRunning()){
                func.onEnd(true);
            }
        });

        if(type == null){
            this.currentFunction = Optional.empty();
            return null;
        }
        final ItemFunction<ICFLTool> func = type.create(this);
        this.currentFunction = Optional.of(func);
        return func;
    }

    @Override
    public Optional<ItemFunction<ICFLTool>> getCurrentFunction() {
        return this.currentFunction;
    }

    @Override
    public Optional<ItemFunction<ICFLTool>> getFunctionByType(CFLFunctionType type) {
        if(this.functions.containsKey(type)){
            return Optional.of(this.functions.get(type));
        }
        ItemFunction<ICFLTool> func = type.create(this);
        this.functions.put(type, func);
        return Optional.of(func);
    }

    @Override
    public List<CFLFunctionType> getAllValidTypes() {
        return ItemFunctionRegistry.REGISTRY.get().getValues().stream().filter(t -> t instanceof CFLFunctionType).map(t -> (CFLFunctionType)t).toList();
    }
}
