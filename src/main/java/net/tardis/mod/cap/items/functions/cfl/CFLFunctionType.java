package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class CFLFunctionType extends ItemFunctionType<ICFLTool> {
    public CFLFunctionType(Predicate<ItemStack> itemToApplyTo, BiFunction<ItemFunctionType<ICFLTool>, ICFLTool, ? extends ItemFunction<ICFLTool>> builder) {
        super(itemToApplyTo, builder);
    }
}
