package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.network.chat.Component;
import net.tardis.mod.misc.SpaceTimeCoord;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface ITrackingFunction {

    Component getTrackingTypeTitle();
    Optional<SpaceTimeCoord> getTrackingPos();
    void setTrackingPos(@Nullable SpaceTimeCoord coord);

}
