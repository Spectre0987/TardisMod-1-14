package net.tardis.mod.cap.items.functions;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Constants;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.TypeHolder;

public abstract class ItemFunction<C> extends TypeHolder<ItemFunctionType> implements INBTSerializable<CompoundTag> {

    private final C parent;
    protected int ticksLeft = 0;
    protected String defaultNameTransKey;

    public ItemFunction(ItemFunctionType<C> type, C parent) {
        super(type);
        this.parent = parent;
    }

    /**
     *
     * @return -1 if this should run until it is turned off, else the number of ticks this function runs for
     */
    public abstract int timeToRun();
    public abstract boolean shouldDisplay(C parent);
    public abstract boolean isInstant();
    public Component getDisplayName(){
        if(this.defaultNameTransKey == null){
            this.defaultNameTransKey = Constants.Translation.makeGenericTranslation(ItemFunctionRegistry.REGISTRY.get(), this.getType());
        }
        return Component.translatable(this.defaultNameTransKey);
    }
    /**
     * Do not override this, use {@link ItemFunction#doTickAction instead!}
     * @param player
     */
    public final void tick(Player player){

        if(this.timeToRun() == -1){ //If this should always run
            this.doTickAction(player);
            return;
        }

        //If this runs for a limited amount of time
        if(!this.isInstant() && this.ticksLeft > 0){
            this.ticksLeft--;
            if(ticksLeft == 0){
                this.onEnd(false);
            }
            else this.doTickAction(player);
        }
    }

    public void onRightClickBlock(UseOnContext context){}
    public void onUse(Player player, ItemStack stack, InteractionHand hand){}
    public void onSelected(Player player, InteractionHand hand){}

    /**
     *
     * @return if this function can start or not
     */
    public boolean onStart(){
        return true;
    }
    public void doTickAction(Player player){}
    public void onEnd(boolean wasCanceled){}

    public boolean isCurrentlyRunning(){
        if(!this.isInstant()){

            if(this.timeToRun() == -1){
                return true;
            }
            return this.ticksLeft >=0;

        }
        return false;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        Helper.writeRegistryToNBT(tag, ItemFunctionRegistry.REGISTRY.get(), this.getType(), "type");
        return tag;
    }

    public C getParent(){
        return this.parent;
    }
}
