package net.tardis.mod.cap.items.functions;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

import java.util.Optional;
import java.util.function.BiFunction;

public abstract class ItemFunctionOpenGuiBase<C> extends ItemFunction<C>{
    public ItemFunctionOpenGuiBase(ItemFunctionType<C> type, C parent) {
        super(type, parent);
    }

    @Override
    public int timeToRun() {
        return 0;
    }

    @Override
    public boolean shouldDisplay(C parent) {
        return true;
    }

    @Override
    public boolean isInstant() {
        return true;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }

    @Override
    public void onSelected(Player player, InteractionHand hand) {
        super.onSelected(player, hand);
        if(!player.level.isClientSide()){
            this.guiToOpen().apply(player, hand).ifPresent(info -> {
                Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(info));
            });

        }
    }

    public abstract BiFunction<Player, InteractionHand, Optional<GuiData>> guiToOpen();
}
