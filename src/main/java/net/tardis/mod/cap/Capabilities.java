package net.tardis.mod.cap;

import net.minecraftforge.common.capabilities.*;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.chunks.IChunkCap;
import net.tardis.mod.cap.chunks.ITardisChunkCap;
import net.tardis.mod.cap.entities.ITardisPlayer;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.IItemArtronTankCap;
import net.tardis.mod.cap.items.IRemoteItemCapability;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.level.IGeneralLevel;
import net.tardis.mod.cap.level.ITardisLevel;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Capabilities {

    public static final Capability<ITardisLevel> TARDIS = CapabilityManager.get(new CapabilityToken<ITardisLevel>() {});
    public static final Capability<IGeneralLevel> GENERAL_LEVEL = CapabilityManager.get(new CapabilityToken<IGeneralLevel>() {});
    public static final Capability<ICFLTool> CFL = CapabilityManager.get(new CapabilityToken<ICFLTool>() {});
    public static final Capability<ITardisPlayer> PLAYER = CapabilityManager.get(new CapabilityToken<ITardisPlayer>() {});
    public static final Capability<ISonicCapability> SONIC = CapabilityManager.get(new CapabilityToken<ISonicCapability>() {});
    public static final Capability<IChunkCap> CHUNK = CapabilityManager.get(new CapabilityToken<IChunkCap>() {});
    public static final Capability<ITardisChunkCap> TARDIS_CHUNK = CapabilityManager.get(new CapabilityToken<ITardisChunkCap>(){});
    public static final Capability<IItemArtronTankCap> ARTRON_TANK_ITEM = CapabilityManager.get(new CapabilityToken<IItemArtronTankCap>() {});
    public static final Capability<IRemoteItemCapability> REMOTE = CapabilityManager.get(new CapabilityToken<IRemoteItemCapability>() {});

    @SubscribeEvent
    public static void register(RegisterCapabilitiesEvent event){
        event.register(ITardisLevel.class);
        event.register(ICFLTool.class);
        event.register(IGeneralLevel.class);
        event.register(ITardisPlayer.class);
        event.register(ISonicCapability.class);
        event.register(IChunkCap.class);
        event.register(ITardisChunkCap.class);
        event.register(IItemArtronTankCap.class);
        event.register(IRemoteItemCapability.class);
    }
    public static <T, O extends ICapabilityProvider> LazyOptional<T> getCap(Capability<T> cap, O object) {
        if(object == null)
            return LazyOptional.empty();

        return object.getCapability(cap);
    }
}
