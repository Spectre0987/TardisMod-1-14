package net.tardis.mod.subsystem;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.Predicate;

public class ShieldSubsystem extends Subsystem{

    final float damageResistance;
    boolean forceFieldActive = false;

    public ShieldSubsystem(SubsystemType<?> type, Predicate<ItemStack> stackTest, ITardisLevel level, float damageResistance) {
        super(type, stackTest, level);
        this.damageResistance = damageResistance;
    }

    public float getDamageResistance(){
        return this.damageResistance;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = super.serializeNBT();
        tag.putBoolean("forcefield", this.forceFieldActive);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        super.deserializeNBT(tag);
        this.forceFieldActive = tag.getBoolean("forcefield");
    }

    public void setForceFieldActive(boolean active){
        this.forceFieldActive = true;
        //TODO: Sync
    }
}
