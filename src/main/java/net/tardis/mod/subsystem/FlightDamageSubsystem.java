package net.tardis.mod.subsystem;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.Predicate;

public class FlightDamageSubsystem extends BasicSubsystem{

    /**
     * Damage this component every X ticks in flight
     */
    final int damageInterval;

    public FlightDamageSubsystem(SubsystemType type, Predicate<ItemStack> stackTest, ITardisLevel level, int damageInterval) {
        super(type, stackTest, level);
        this.damageInterval = damageInterval;
    }

    @Override
    public void onFlightTick() {
        super.onFlightTick();
        if(this.getTardis().isTraveling() && this.getTardis().ticksInVortex() % this.damageInterval == 0){
            this.getItem().hurt(1, this.getTardis().getLevel().getRandom(), null);
        }
    }
}
