package net.tardis.mod.exterior;

import com.google.common.collect.Lists;
import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.blockentities.exteriors.ChameleonExteriorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.DelayedServerTask;
import net.tardis.mod.misc.PhasedShellDisguise;
import net.tardis.mod.registry.JsonRegistries;

import java.util.*;

public class ChameleonExterior extends TileExterior{

    public final PhasedShellDisguise fallback;

    private PhasedShellDisguise currentDisguise;

    public ChameleonExterior(ExteriorType type, ITardisLevel level, BlockState state) {
        super(type, level, state);

        Map<BlockPos, BlockState> disguise = new HashMap<>();
        disguise.put(BlockPos.ZERO, Blocks.STONE.defaultBlockState());
        disguise.put(BlockPos.ZERO.above(), Blocks.STONE.defaultBlockState());
        this.fallback = new PhasedShellDisguise(disguise, Lists.newArrayList(BiomeTags.IS_OVERWORLD), Optional.empty());
    }

    @Override
    public void setPosition(Level level, BlockPos pos) {
        this.currentDisguise = getValidDisguise(level, pos);
        super.setPosition(level, pos.immutable());
        this.placeShell((ServerLevel) level, this.currentDisguise.disguise());


        DelayedServerTask.schedule(level.getServer(), 2, () -> {

            if(level.getBlockEntity(pos) instanceof ChameleonExteriorTile ext){
                PhasedShellDisguise dis = this.currentDisguise;
                for(Map.Entry<BlockPos, BlockState> entry : dis.disguise().entrySet()){
                    if(level.getBlockEntity(pos.offset(WorldHelper.rotateBlockPos(entry.getKey(), rotFromFacing()))) instanceof IDisguisedBlock d){
                        d.setDisguisedState(entry.getValue().rotate(rotFromFacing()));
                    }
                }
            }
        });

    }

    @Override
    public List<BlockPos> addToAtriumList(List<BlockPos> shell) {
        shell.addAll(this.currentDisguise.disguise().keySet().stream().map(p -> WorldHelper.rotateBlockPos(p, rotFromFacing())).toList());
        return shell;
    }

    public PhasedShellDisguise getValidDisguise(Level level, BlockPos pos){
        final Registry<Biome> biomeReg = level.registryAccess().registryOrThrow(Registries.BIOME);
        final Registry<Structure> structReg = level.registryAccess().registryOrThrow(Registries.STRUCTURE);

        final List<PhasedShellDisguise> validDisguises = new ArrayList<>();

        for(PhasedShellDisguise dis : level.registryAccess().registryOrThrow(JsonRegistries.POS_DISGUISE)){

            //Check structures
            if(!dis.isInValidStructure((ServerLevel) level, pos, structReg))
                continue;

            if(dis.isInBiome(biomeReg, level.getBiome(pos))){
                return dis;
            }
        }
        return validDisguises.size() > 0 ? validDisguises.get(level.getRandom().nextInt(validDisguises.size())) : this.fallback;
    }

    @Override
    public void remat(Level level) {
        super.remat(level);
    }

    @Override
    public void delete(Level level, BlockPos pos) {
        super.delete(level, pos);
    }
}
