package net.tardis.mod.exterior;

import com.mojang.datafixers.kinds.Const;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.registry.ExteriorRegistry;

import java.util.function.BiFunction;
public class ExteriorType {

    private final BiFunction<ExteriorType, ITardisLevel, Exterior> factory;
    private final DoorState[] validDoorStates;
    private Component translation;
    private boolean mustBeUnlocked = false;

    public ExteriorType(BiFunction<ExteriorType, ITardisLevel, Exterior> factory, DoorState... doorStates){
        this.factory = factory;
        this.validDoorStates = doorStates.length == 0 ? DoorState.values() : doorStates;
    }

    public Exterior create(ITardisLevel level){
        return this.factory.apply(this, level);
    }

    public DoorState[] getDoorStates(){
        return this.validDoorStates;
    }

    public ExteriorType setMustBeUnlocked(){
        this.mustBeUnlocked = true;
        return this;
    }

    public boolean mustBeUnlocked(){
        return this.mustBeUnlocked;
    }

    public Component getTranslation(){
        if(this.translation == null){
            this.translation = Component.translatable(
                    Constants.Translation.makeGenericTranslation(ExteriorRegistry.REGISTRY.get(), this)
            );
        }
        return this.translation;
    }

}
