package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.blockentities.exteriors.IExteriorObject;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.*;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.registry.ControlRegistry;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

public class EntityExterior extends Exterior{

    private final Function<Level, Entity> factory;
    private UUID exteriorID;

    public EntityExterior(ExteriorType type, ITardisLevel level, Function<Level, Entity> factory) {
        super(type, level);
        this.factory = factory;
    }

    @Override
    public void demat(Level level) {
        if(!level.isClientSide()){
            Entity e = ((ServerLevel)level).getEntity(this.exteriorID);
            if(e instanceof IHaveMatterState matter) {
                matter.getMatterStateHandler().startMatterState(level, MatterState.DEMAT,
                        this.tardis.getInteriorManager().getSoundScheme().getTakeoff().time(),
                        tardis.getInteriorManager().getDematAnimation()
                );
            }
            else if(e != null)
                e.kill();
        }
    }

    @Override
    public void remat(Level level) {
        if(!level.isClientSide){
            this.setPosition(level, tardis.getDestination().getPos());
            DelayedServerTask.schedule(level.getServer(), 7, () -> {
                Entity e = ((ServerLevel)level).getEntity(this.exteriorID);
                if(e instanceof IHaveMatterState state){
                    state.getMatterStateHandler().startMatterState(
                            level, MatterState.REMAT,
                            tardis.getInteriorManager().getSoundScheme().getLand().time(),
                            tardis.getInteriorManager().getDematAnimation()
                    );
                }
            });
        }
    }

    @Override
    public void setPosition(Level level, BlockPos pos) {
        if(!level.isClientSide()){
            ChunkLoader.addTicketToExterior((ServerLevel) level, pos);
            DelayedServerTask.schedule(level.getServer(), 5, () -> {

                if(this.exteriorID != null){
                    final Entity oldExterior = ((ServerLevel)level).getEntity(this.exteriorID);
                    if(oldExterior != null && oldExterior.isAlive()){
                        oldExterior.kill();
                    }
                }

                final Entity e = this.factory.apply(level);
                e.setPos(Helper.blockPosToVec3(pos, false));
                e.setYRot(this.tardis.getControlDataOrCreate(ControlRegistry.FACING.get()).get().toYRot());
                this.exteriorID = e.getUUID();

                if(e instanceof IExteriorObject ext){
                    ext.setInterior(this.tardis.getId());
                }
                level.addFreshEntity(e);
            });
        }
    }

    @Override
    public void delete(Level level, BlockPos pos) {
        ChunkLoader.addTicketToExterior((ServerLevel) level, pos);
        DelayedServerTask.schedule(level.getServer(), 5, () -> {
            Entity e = ((ServerLevel)level).getEntity(this.exteriorID);
            if(e != null)
                e.kill();
        });
    }

    @Override
    public void fly(Vec3 motion) {

    }

    @Override
    public void placeEntityAt(ServerLevel exteriorLevel, Entity e) {
        ChunkLoader.addTicketToExterior(exteriorLevel, this.tardis.getLocation().getPos());
        DelayedServerTask.schedule(exteriorLevel.getServer(), 5, () -> {
            Entity exteriorEntity = exteriorLevel.getEntity(this.exteriorID);
            if(exteriorEntity != null){
                if(exteriorEntity instanceof ITeleportEntities<?> teleporter && teleporter.getTeleportHandler().placeAtMe(e).isPresent()){
                    e.setPos(teleporter.getTeleportHandler().placeAtMe(e).get().position());
                }
                else e.setPos(exteriorEntity.position());
            }
            else e.setPos(WorldHelper.centerOfBlockPos(this.tardis.getLocation().getPos()));
            TeleportEntry.add(new TeleportEntry(e, exteriorLevel, e.position(), e.getYHeadRot()));
        });
    }

    @Override
    public TeleportEntry.LocationData getEntityTeleportTarget(ServerLevel exteriorLevel, Entity e) {
        ChunkLoader.addTicketToExterior(exteriorLevel, this.tardis);
        Entity exterior = exteriorLevel.getEntity(this.exteriorID);
        if(exterior != null){
            if(exterior instanceof ITeleportEntities<?> teleport){
                Optional<TeleportEntry.LocationData> data = teleport.getTeleportHandler().placeAtMe(e);
                if(data.isPresent())
                    return data.get();
            }
            return new TeleportEntry.LocationData(exterior.position(), exterior.getYRot());
        }
        return new TeleportEntry.LocationData(WorldHelper.centerOfBlockPos(tardis.getLocation().getPos()), 0);
    }

    @Override
    public void playMoodChangedEffects(Level level, SpaceTimeCoord location, boolean isPositive) {

    }

    @Override
    public void setDoorHandler(DoorHandler other) {
        ServerLevel outside = (ServerLevel) tardis.getLevel();
        Entity e = outside.getEntity(this.exteriorID);
        if(e instanceof IDoor door){
            door.getDoorHandler().update(other, outside);
        }
    }

    @Override
    public Optional<DoorHandler> getDoorHandler() {
        ServerLevel outside = (ServerLevel) this.tardis.getLevel();
        Entity e = outside.getEntity(this.exteriorID);
        if(e instanceof IDoor door){
            return Optional.of(door.getDoorHandler());
        }
        return Optional.empty();
    }

    @Override
    public AABB getSize() {
        return new AABB(0, 0, 0, 1, 2, 1);
    }

    @Override
    public TeleportHandler<?> getTeleportHandler() {
        if(tardis.getLevel() instanceof ServerLevel level && this.exteriorID != null){
            ServerLevel dest = level.getServer().getLevel(this.tardis.getLocation().getLevel());
            if(dest.getEntity(this.exteriorID) instanceof ITeleportEntities<?> teleporter){
                return teleporter.getTeleportHandler();
            }
        }
        return null;
    }


    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        if(this.exteriorID != null)
            tag.putString("exterior_uuid", this.exteriorID.toString());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        if(nbt.contains("exterior_uuid"))
            this.exteriorID = UUID.fromString(nbt.getString("exterior_uuid"));
    }
}
