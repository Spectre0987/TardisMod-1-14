package net.tardis.mod.exterior.data;

import net.tardis.mod.blockentities.exteriors.IExteriorObject;

import java.util.function.BiFunction;

public record ExteriorDataType<T>(BiFunction<ExteriorDataType<T>, IExteriorObject, T> create) {

    public T create(IExteriorObject exterior){
        return this.create().apply(this, exterior);
    }

}
