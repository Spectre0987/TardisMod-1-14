package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.ExteriorBlock;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.*;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.misc.tardis.world_structures.AtriumWorldStructure;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.registry.UpgradeRegistry;

import java.util.*;

public class TileExterior extends Exterior{

    final BlockState exteriorState;
    final AABB size;

    public TileExterior(ExteriorType type, ITardisLevel level, BlockState state) {
        super(type, level);
        this.exteriorState = state;
        this.size = new AABB(0, 0, 0, 1, 3, 1);
    }

    @Override
    public void demat(Level level) {

        ChunkLoader.addTicketToExterior((ServerLevel) level, tardis.getLocation().getPos());
        if(level.getBlockEntity(tardis.getLocation().getPos()) instanceof ExteriorTile exterior){
            exterior.getMatterStateHandler().startMatterState(level, MatterState.DEMAT,
                    tardis.getInteriorManager().getSoundScheme().getTakeoff().time(),
                    tardis.getInteriorManager().getDematAnimation());
        }
    }

    @Override
    public void remat(Level level) {
        this.setPosition(level, tardis.getDestination().getPos());
        if(!level.isClientSide){
            final BlockPos pos = tardis.getDestination().getPos().immutable();
            level.getServer().execute(() -> {
                BlockEntity genericExt = level.getBlockEntity(pos);
                if(genericExt instanceof IHaveMatterState exterior){
                    exterior.getMatterStateHandler().startMatterState(level, MatterState.REMAT,
                            tardis.getInteriorManager().getSoundScheme().getLand().time(),
                            tardis.getInteriorManager().getDematAnimation());
                }
            });
        }
    }
    public void placeShell(ServerLevel level, Map<BlockPos, BlockState> positions){

        final Rotation rot = rotFromFacing();

        for(Map.Entry<BlockPos, BlockState> entry : positions.entrySet()){

            final BlockPos worldPos = this.tardis.getDestination().getPos().offset(WorldHelper.rotateBlockPos(entry.getKey(), rot));

            if(level.getBlockState(worldPos).canBeReplaced()) {
                level.setBlock(worldPos, BlockRegistry.DISGUISED_BLOCK.get().defaultBlockState(), 3);
                DelayedServerTask.schedule(level.getServer(), () -> {
                    if (level.getBlockEntity(worldPos) instanceof IDisguisedBlock tile) {
                        tile.setDisguisedState(entry.getValue().rotate(rot.getRotated(Rotation.CLOCKWISE_180)));
                    }
                });
            }
        }
    }

    public Rotation rotFromFacing(){
        final Direction dir = tardis.getControlDataOrCreate(ControlRegistry.FACING.get()).get();
        return dir == Direction.EAST ? Rotation.CLOCKWISE_90 : (
                dir == Direction.SOUTH ? Rotation.CLOCKWISE_180 : (
                        dir == Direction.WEST ? Rotation.COUNTERCLOCKWISE_90 :
                                Rotation.NONE
                )
        );
    }

    @Override
    public void setPosition(Level level, BlockPos pos) {
        ChunkLoader.addTicketToExterior((ServerLevel) level, pos);

        final List<BlockPos> positions = new ArrayList<>();

        if(UpgradeRegistry.ATRIUM.get().canBeUsed(this.tardis)){
            this.tardis.getInteriorManager().getMainAtrium().ifPresent(att -> {
                att.buildShell(att.getAllConnectedBlocks());
                this.placeShell((ServerLevel) level, att.shell);
                positions.addAll(WorldHelper.rotateBlockPositions(att.shell.keySet(), rotFromFacing()));
            });
        }

        level.setBlock(pos, this.exteriorState.setValue(ExteriorBlock.FACING, this.tardis.getControlDataOrCreate(ControlRegistry.FACING.get()).get()), 3);
        DelayedServerTask.schedule(level.getServer(), 5, () -> {
            if(level.getBlockEntity(pos) instanceof ExteriorTile exterior){
                exterior.getDoorHandler().update(tardis.getInteriorManager().getDoorHandler(), level);
                exterior.setInterior(this.tardis.getLevel().dimension(), this.getType());
                exterior.setShell(addToAtriumList(positions));
                exterior.onLanded();
                tardis.getExteriorExtraData().getExteriorTexVariant().ifPresent(id -> {
                    tardis.getLevel().registryAccess().registry(JsonRegistries.TEXTURE_VARIANTS)
                            .map(reg -> reg.get(id))
                            .ifPresent(var -> {
                                exterior.setTextureVariant(var);
                            });
                });
            }
        });
    }

    public List<BlockPos> addToAtriumList(List<BlockPos> shell){
        return shell;
    }

    @Override
    public void delete(Level level, BlockPos pos) {
        //Let the tile handle deletion if possible, if not, do our best
        if(level.getBlockEntity(pos) instanceof ExteriorTile ext){
            ext.delete();
        }
        //Fallback
        else{
            level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
            level.setBlock(pos.above(), Blocks.AIR.defaultBlockState(), 3);
        }
    }

    @Override
    public void fly(Vec3 motion) {

    }

    @Override
    public void placeEntityAt(ServerLevel exteriorLevel, Entity e) {
        BlockPos pos = this.tardis.getLocation().getPos();
        //Move to actual exterior if it exists
        if(exteriorLevel.getBlockEntity(pos) instanceof ExteriorTile exterior){
            Direction dir = WorldHelper.getHorizontalFacing(exterior.getBlockState());
            TeleportEntry.add(new TeleportEntry(e, exteriorLevel, WorldHelper.centerOfBlockPos(exterior.getBlockPos().relative(dir)), WorldHelper.getDegreeFromRotation(dir)));
            return;
        }
        else{
            //Move to somewhere along path if we can't find exterior
            SpaceTimeCoord target = this.tardis.getLocation();
            TeleportEntry.add(new TeleportEntry(e, exteriorLevel, WorldHelper.centerOfBlockPos(target.getPos(), true), e.getYHeadRot()));
        }
        e.setPos(WorldHelper.centerOfBlockPos(pos));
    }

    @Override
    public TeleportEntry.LocationData getEntityTeleportTarget(ServerLevel exteriorLevel, Entity e) {
        if(exteriorLevel.getBlockEntity(tardis.getLocation().getPos()) instanceof ExteriorTile ext){
            Optional<TeleportEntry.LocationData> data = ext.getTeleportHandler().placeAtMe(e);
            if(data.isPresent())
                return data.get();
        }
        return new TeleportEntry.LocationData(WorldHelper.centerOfBlockPos(tardis.getLocation().getPos()), 0);
    }

    @Override
    public void playMoodChangedEffects(Level level, SpaceTimeCoord location, boolean isPositive) {
        final int steps = 20;
        final float deltaAngle = 360.0F / (float)steps;
        final Vec3 center = WorldHelper.centerOfBlockPos(location.getPos(), false);
        final ParticleOptions particle = isPositive ? ParticleTypes.HEART : ParticleTypes.ANGRY_VILLAGER;

        for(int i = 0; i < steps; ++i){

            level.addParticle(particle,
                        center.x + Math.toDegrees(Math.sin(deltaAngle * i)), center.y, center.z + Math.toDegrees(Math.cos(deltaAngle * i)),
                        0, 0.25, 0
                    );

        }

    }

    @Override
    public void setDoorHandler(DoorHandler other) {
        ServerLevel level = this.tardis.getLevel().getServer().getLevel(this.tardis.getLocation().getLevel());
        if(level != null){
            level.getChunkAt(tardis.getLocation().getPos());
            if(level.getBlockEntity(this.tardis.getLocation().getPos()) instanceof IDoor door){
                door.getDoorHandler().update(other, level);
                door.getDoorHandler().save();
            }
        }
    }

    @Override
    public Optional<DoorHandler> getDoorHandler() {
        ServerLevel level = this.tardis.getLevel().getServer().getLevel(this.tardis.getLocation().getLevel());
        if(level != null){
            if(level.getBlockEntity(tardis.getLocation().getPos()) instanceof ExteriorTile tile){
                return Optional.of(tile.getDoorHandler());
            }
        }
        return Optional.empty();
    }

    @Override
    public AABB getSize() {

        Optional<AtriumWorldStructure> att = tardis.getInteriorManager().getMainAtrium();
        if(att.isPresent()){
            att.get().buildShell(att.get().getAllConnectedBlocks());
            final AABB size = att.get().shell.isEmpty() ? this.size : this.size.minmax(WorldHelper.getBoundsFromBlocks(att.get().shell.keySet()));
            return size;
        }

        return this.size;
    }

    @Override
    public TeleportHandler<?> getTeleportHandler() {
        if(tardis.getLevel() instanceof ServerLevel server){
            ServerLevel destination = server.getServer().getLevel(tardis.getLocation().getLevel());
            if(destination != null){
                if(destination.getBlockEntity(tardis.getLocation().getPos()) instanceof ITeleportEntities<?> teleporter){
                    return teleporter.getTeleportHandler();
                }
            }
        }
        return null;
    }

    @Override
    public CompoundTag serializeNBT() {
        return new CompoundTag(); // This exterior type doesn't need any additional data
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
