package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.BigDoorTile;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import net.tardis.mod.sound.SoundRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class BigDoorBlock<T extends BlockEntity & IMultiblockMaster> extends AbstractMultiblockMasterBlock<T> {

    public static BooleanProperty OPEN = BooleanProperty.create("open");
    public static final DirectionalVoxelShape CLOSED_SHAPE = new DirectionalVoxelShape(Shapes.box(
            -1, 0, 0,
            2, 4, 1
    ));

    public static final DirectionalVoxelShape OPEN_SHAPE = new DirectionalVoxelShape(Shapes.join(
            Shapes.box(
                    -1, 0, 0,
                    -0.9375, 4, 1
            ),
            Shapes.box(
                    1.9375, 0, 0,
                    2, 4, 1
            ), BooleanOp.OR
    ));

    public BigDoorBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type, MultiblockData data) {
        super(pProperties, type, data);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if(!pLevel.isClientSide() && pHand == InteractionHand.MAIN_HAND){
            pLevel.setBlock(pPos, pState.cycle(OPEN), 3);
            pLevel.playSound(null, pPos, SoundRegistry.SLIDING_DOOR.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
        }
        final boolean oldDoor = pState.getValue(OPEN);
        if(pLevel.getBlockEntity(pPos) instanceof BigDoorTile door){
            door.startAnimation(!oldDoor);
        }
        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        final Direction dir = pState.getValue(BlockStateProperties.HORIZONTAL_FACING);
        return pState.getValue(OPEN) ? OPEN_SHAPE.getShapeFor(dir) : CLOSED_SHAPE.getShapeFor(dir);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING, OPEN);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    public static BigDoorBlock<BigDoorTile> create(){
        return new BigDoorBlock<BigDoorTile>(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.BIG_DOOR,
                MultiblockDatas.BIG_DOOR
        );
    }
}
