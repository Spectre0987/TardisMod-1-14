package net.tardis.mod.block;

import net.minecraftforge.common.capabilities.Capability;
import net.tardis.mod.cap.Capabilities;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class MultiChildCapRedirectBlock extends MultiblockBlock{

    private final Supplier<List<Capability<?>>> reDirectCaps;

    public MultiChildCapRedirectBlock(Supplier<List<Capability<?>>> redirCaps){
        this.reDirectCaps = redirCaps;
    }

    public <T> boolean isCapRedirected(Capability<T> cap){
        return this.reDirectCaps.get().contains(cap);
    }

}
