package net.tardis.mod.block.monitors;

import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

import java.util.HashMap;

public record MonitorData(ResourceLocation texture, ResourceLocation frameTex, int width, int height, int padLeft, int padTop, int padRight, int padBottom) {

    public static final HashMap<ResourceLocation, MonitorData> DATAS = new HashMap<>();

    public static final ResourceLocation STEAM = create("steam",
            Helper.createRL("textures/screens/monitors/steam.png"),
            Helper.createRL("textures/screens/monitors/steam_frame.png"),
            243, 188,
            23, 18, 23, 18
    );

    public static final ResourceLocation RCA = create("rca",
            Helper.createRL("textures/screens/monitors/rca.png"),
            Helper.createRL("textures/screens/monitors/rca_frame.png"),
            252, 181,
            23, 8, 21, 17
    );

    public static final ResourceLocation EYE = create("eye",
            Helper.createRL("textures/screens/monitors/eye.png"),
            Helper.createRL("textures/screens/monitors/eye_frame.png"),
            256, 181,
            23, 18, 14, 29
    );

    public static final ResourceLocation GALVANIC = create("galvanic",
                Helper.createRL("textures/screens/monitors/galvanic.png"), null,
                239, 181,
                14, 8, 15, 16

            );


    public static ResourceLocation create(ResourceLocation loc, MonitorData data){
        DATAS.put(loc, data);
        return loc;
    }

    public static ResourceLocation create(String key, ResourceLocation texture, ResourceLocation frameTex, int width, int height, int padLeft, int padTop, int padRight, int padBottom){
        return create(Helper.createRL(key), new MonitorData(texture, frameTex, width, height, padLeft, padTop, padRight, padBottom));
    }

    public static MonitorData getData(ResourceLocation loc) {
        return DATAS.get(loc);
    }
}
