package net.tardis.mod.block.monitors;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.MapItem;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.saveddata.maps.MapDecoration;
import net.minecraft.world.level.saveddata.maps.MapItemSavedData;
import net.minecraft.world.phys.BlockHitResult;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Map;
import java.util.function.Supplier;

public class MonitorBlock<T extends BaseMonitorTile> extends BaseEntityBlock {

    public final Supplier<BlockEntityType<T>> type;
    private final ResourceLocation dataId;

    public MonitorBlock(BlockBehaviour.Properties properties, ResourceLocation dataId, Supplier<BlockEntityType<T>> type) {
        super(properties.noOcclusion());
        this.type = type;
        this.dataId = dataId;
    }

    public static MonitorBlock<BaseMonitorTile> createSteam(){
        return new MonitorBlock<>(
                BlockBehaviour.Properties.of(Material.METAL)
                    .sound(SoundType.ANVIL)
                    .strength(2)
                    .noOcclusion(),
                MonitorData.STEAM,
                TileRegistry.BASIC_MONITOR
        );
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        final ItemStack held = pPlayer.getItemInHand(pHand);

        if(held.getItem() == Items.FILLED_MAP){
            if(!pLevel.isClientSide()){
                final MapItemSavedData mapData = MapItem.getSavedData(held, pLevel);
                BlockPos target = new BlockPos(mapData.centerX, 70, mapData.centerZ);
                final Iterator<MapDecoration> iterator = mapData.getDecorations().iterator();
                while(iterator.hasNext()){
                    final MapDecoration dec = iterator.next();
                    if(dec.getType() == MapDecoration.Type.PLAYER)
                        continue;
                    target = new BlockPos(mapData.centerX + dec.getX(), 70, mapData.centerZ + dec.getY());
                }

                final BlockPos finalTarget = target;
                Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                    tardis.setDestination(new SpaceTimeCoord(mapData.dimension, finalTarget));
                });
            }
            return InteractionResult.sidedSuccess(!pLevel.isClientSide());
        }

        if(pHand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(pLevel.isClientSide());

        if(!pLevel.isClientSide){
            Network.sendTo((ServerPlayer) pPlayer, new OpenGuiDataMessage(GuiDatas.MON_MAIN.create().withData(this.dataId)));
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return this.type.get().create(pPos, pState);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
       return pBlockEntityType == type.get() && pLevel.isClientSide ? (Level level, BlockPos pPos, BlockState state, T pBlockEntity) -> {
           ((BaseMonitorTile)pBlockEntity).clientTick();
       } : null;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
