package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.FabricatorTile;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class FabricatorBlock extends AbstractMultiblockMasterBlock<FabricatorTile> {


    public static final VoxelShape SHAPE = Shapes.box(0, 0, 0, 1, 3, 1);

    public FabricatorBlock(Properties pProperties, Supplier<? extends BlockEntityType<FabricatorTile>> type, MultiblockData data) {
        super(pProperties, type, data);
    }

    public static FabricatorBlock create(){
        return new FabricatorBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.FABRICATOR_MACHINE,
                MultiblockDatas.FABRICATOR
        );
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return pLevel.isClientSide() ? null : createTickerHelper(pBlockEntityType, TileRegistry.FABRICATOR_MACHINE.get(), (level, pos, state, tile) -> tile.serverTick());
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if(pLevel.getBlockEntity(pPos) instanceof FabricatorTile tile && !pLevel.isClientSide()){
            tile.setCraftingItem(pPlayer.getMainHandItem().isEmpty() ? null : pPlayer.getMainHandItem().getItem());
        }
        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }
}
