package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.world_structures.AtriumWorldStructure;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructrureType;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

import java.util.Optional;
import java.util.function.Supplier;

public class AtriumMasterBlock extends DefaultSingleBlockTardisStructureBlock<AtriumWorldStructure> implements ISonicNamable{
    public AtriumMasterBlock(Properties pProperties, Supplier<? extends TardisWorldStructrureType<AtriumWorldStructure>> factory) {
        super(pProperties, factory);
    }

    public static AtriumMasterBlock create(){
        return new AtriumMasterBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TardisWorldStructureRegistry.ATRIUM
        );
    }

    @Override
    public void setName(Level level, BlockPos pos, Optional<String> name) {
        Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
            tardis.getInteriorManager().getWorldStructure(pos, TardisWorldStructureRegistry.ATRIUM.get()).ifPresent(atrium -> {
                atrium.setName(name.isEmpty() ? null : name.get());
            });
        });
    }

    @Override
    public Optional<String> getExistingName(Level level, BlockPos pos) {
        final LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, level);
        if(tardisHolder.isPresent()){
            Optional<AtriumWorldStructure> at = tardisHolder.orElseThrow(NullPointerException::new).getInteriorManager().getWorldStructure(pos, TardisWorldStructureRegistry.ATRIUM.get());
            if(at.isPresent())
                return at.get().getName();
        }
        return Optional.empty();
    }
}
