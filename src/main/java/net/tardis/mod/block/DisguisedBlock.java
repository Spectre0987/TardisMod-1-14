package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.GlassBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.DisguisedBlockTile;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class DisguisedBlock<T extends BlockEntity & IDisguisedBlock> extends BaseTileBlock<T> {

    public DisguisedBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        super(pProperties, type);
    }

    public static <T extends BlockEntity & IDisguisedBlock> DisguisedBlock<T> create(){
        return new DisguisedBlock(
            BasicProps.Block.INVINCIBLE.get().noOcclusion(),
            TileRegistry.DISGUISED_BLOCK

        );
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public VoxelShape getOcclusionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos) {
        if(pLevel.getBlockEntity(pPos) instanceof IDisguisedBlock dis && dis.getDisguisedState() != null){
            return dis.getDisguisedState().getOcclusionShape(pLevel, pPos);
        }
        return super.getOcclusionShape(pState, pLevel, pPos);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        if(pLevel.getBlockEntity(pPos) instanceof IDisguisedBlock dis && dis.getDisguisedState() != null){
            return dis.getDisguisedState().getShape(pLevel, pPos, pContext);
        }
        return super.getShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public boolean hasDynamicShape() {
        return true;
    }

    @Override
    public VoxelShape getVisualShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return super.getVisualShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        BlockState dis = getDisguise(pLevel, pPos);
        if(dis != null)
            return dis.getCollisionShape(pLevel, pPos, pContext);
        return super.getCollisionShape(pState, pLevel, pPos, pContext);
    }



    @Override
    public boolean hidesNeighborFace(BlockGetter level, BlockPos pos, BlockState state, BlockState neighborState, Direction dir) {
        BlockState dis = getDisguise(level, pos);
        if(dis != null) {
            dis.hidesNeighborFace(level, pos, neighborState, dir);
        }
        return super.hidesNeighborFace(level, pos, state, neighborState, dir);
    }

    @Override
    public float getShadeBrightness(BlockState pState, BlockGetter pLevel, BlockPos pPos) {
        BlockState dis = getDisguise(pLevel, pPos);
        if(dis != null)
            return dis.getShadeBrightness(pLevel, pPos);
        return super.getShadeBrightness(pState, pLevel, pPos);
    }

    @Override
    public int getLightEmission(BlockState state, BlockGetter level, BlockPos pos) {
        BlockState dis = getDisguise(level, pos);
        if(dis != null) {
            return dis.getLightEmission(level, pos);
        }
        return super.getLightEmission(state, level, pos);
    }

    @Override
    public boolean supportsExternalFaceHiding(BlockState state) {
        return true;
    }

    @Nullable
    public BlockState getDisguise(BlockGetter level, BlockPos pos){
        if(level.getBlockEntity(pos) instanceof IDisguisedBlock dis && dis.getDisguisedState() != null){
            return dis.getDisguisedState();
        }
        return null;
    }
}
