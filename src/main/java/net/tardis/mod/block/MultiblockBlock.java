package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.IMultiblock;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.MultiblockChildTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class MultiblockBlock extends BaseEntityBlock {

    public MultiblockBlock() {
        super(Properties.of(Material.METAL).noOcclusion());
    }


    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return TileRegistry.MULTIBLOCK.get().create(pPos, pState);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        final Optional<BlockPos> masterPos = getMasterPos(pLevel, pPos);
        if(masterPos.isPresent()){
            final BlockState masterState = pLevel.getBlockState(masterPos.get());
            return masterState.use(
                    pLevel, pPlayer,pHand,
                    new BlockHitResult(
                            pHit.getLocation(),
                            pHit.getDirection(),
                            masterPos.get(), false
                    )
            );
        }
        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }



    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {

        final Optional<BlockPos> masterPos = getMasterPos(pLevel, pPos);
        if(masterPos.isPresent() && pLevel.getBlockEntity(masterPos.get()) instanceof IMultiblockMaster master && master.getMultiblockData() != null){
            //master.getMultiblockData().destroy(pLevel, masterPos.get(), getRotation(pLevel.getBlockState(masterPos.get())));
            pLevel.destroyBlock(masterPos.get(), true);
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {

        Optional<BlockPos> offset = getMasterPos(pLevel, pPos);
        if(offset.isPresent() && pLevel.getBlockEntity(pPos) instanceof IMultiblock<?> child && child.getMasterPos() != null){
            final BlockState masterState = pLevel.getBlockState(offset.get());
            return masterState.getBlock().getVisualShape(
                    masterState, pLevel, offset.get(), pContext
            ).move(child.getMasterPos().getX(), child.getMasterPos().getY(), child.getMasterPos().getZ());
        }

        return Shapes.empty();
    }

    @Override
    public VoxelShape getCollisionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return this.getShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public VoxelShape getVisualShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return super.getVisualShape(pState, pLevel, pPos, pContext);
    }

    public static Rotation getRotation(BlockState masterState){
        if(masterState.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
            return Helper.getRotationFromDirection(masterState.getValue(BlockStateProperties.HORIZONTAL_FACING));
        }
        return Rotation.NONE;
    }


    public static Optional<BlockPos> getMasterPos(BlockGetter level, BlockPos pos){
        if(level.getBlockEntity(pos) instanceof IMultiblock<?> multi){
            if(multi.getMasterPos() != null)
                return Optional.of(multi.getMasterPos().offset(pos));
        }
        return Optional.empty();
    }
}
