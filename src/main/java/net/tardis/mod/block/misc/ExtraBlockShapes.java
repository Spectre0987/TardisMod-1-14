package net.tardis.mod.block.misc;

import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.helpers.Helper;

public class ExtraBlockShapes {

    //public static final VoxelShape PLAQUE = Helper.blockShapePixels(0, 0, 0);

    public static VoxelShape makeComfyChairShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0, 0, 0, 0.125, 0.375, 0.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.875, 0, 0, 1, 0.375, 0.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 0, 0.875, 0.125, 0.375, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.875, 0, 0.875, 1, 0.375, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 0.375, 0, 1, 0.5, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0, 0.5, 0, 0.125, 0.71875, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.875, 0.5, 0, 1, 0.71875, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.125, 0.5, 0.875, 0.875, 1.5, 1), BooleanOp.OR);

        return shape;
    }
}
