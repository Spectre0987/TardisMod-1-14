package net.tardis.mod.block.dev;

import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.blockentities.RendererTestTile;
import net.tardis.mod.blockentities.TileRegistry;

import java.util.function.Supplier;

public class TestRendererBlock extends BaseTileBlock<RendererTestTile> {
    public TestRendererBlock() {
        super(BasicProps.Block.METAL.get().noOcclusion(), TileRegistry.RENDERER_TEST);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }
}
