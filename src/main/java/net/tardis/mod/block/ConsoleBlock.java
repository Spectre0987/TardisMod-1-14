package net.tardis.mod.block;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.entity.ControlEntity;

public class ConsoleBlock extends BaseEntityBlock {

    public static final VoxelShape SHAPE = Shapes.create(0.25, 0, 0.25, 0.75, 1, 0.75);

    private final Supplier<? extends BlockEntityType<? extends ConsoleTile>> consoleTileFactory;

    public ConsoleBlock(Supplier<? extends BlockEntityType<? extends ConsoleTile>> consoleTileFactory) {
        super(Properties.of(Material.BARRIER)
                .strength(4)
                .noOcclusion());
        this.consoleTileFactory = consoleTileFactory;

    }

    @Override
    public void animateTick(BlockState pState, Level pLevel, BlockPos pPos, RandomSource pRandom) {
        super.animateTick(pState, pLevel, pPos, pRandom);
        pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            //Makes the console randomly smoke when it's never been flown
            if(tardis.getInteriorManager().hasNeverFlown()){

                final int particlesToSpawn = 10;
                final Vec3 center = WorldHelper.centerOfBlockPos(pPos, false).add(0, 1, 0);

                for(int i = 0; i < particlesToSpawn; ++i){

                    final double angle = Math.toRadians(pLevel.random.nextDouble() * 360.0);
                    final double length = pLevel.random.nextDouble() * 2;

                    pLevel.addParticle(ParticleTypes.SMOKE,
                            center.x + Math.sin(angle) * length, center.y, center.z - Math.cos(angle) * length,
                            0, 0.01, 0
                    );
                }
            }
        });
    }

    @Override
    public void onPlace(BlockState state, Level level, BlockPos pos, BlockState newState, boolean p_60570_) {
        super.onPlace(state, level, pos, newState, p_60570_);

        if(!level.isClientSide()){
            if(level.getBlockEntity(pos) instanceof ConsoleTile console){
                this.clearPreexistingControls(console, level, pos);
                
                //Add new consoles
                Map<ControlType<?>, ControlPositionData> positionData = this.getControlPositionData(console);
                if(positionData != null){
                    this.placeControls(positionData, console, level, pos);
                }
            }
        }
    }

    public Map<ControlType<?>, ControlPositionData> getControlPositionData(ConsoleTile console){
        return ControlPositionDataRegistry.POSITION_DATAS
                .getOrDefault(ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(console.getType()), new HashMap<>());
    }

    public void clearPreexistingControls(@Nullable ConsoleTile console, Level level, BlockPos pos){
        //Remove left over control entities in case of fuckery
        for(ControlEntity control : level.getEntitiesOfClass(ControlEntity.class, new AABB(pos).inflate(2))){
            control.kill();
        }

        if(console != null)
            console.killAllControls();
    }

    public void placeControls(Map<ControlType<?>, ControlPositionData> positionData, ConsoleTile console, Level level, BlockPos pos){
        this.clearPreexistingControls(console, level, pos);
        for(Map.Entry<ControlType<?>, ControlPositionData> data : positionData.entrySet()){
            ControlEntity control = new ControlEntity(level, data.getKey(), data.getValue(), console);
            Vec3 offset = data.getValue().offset();
            control.setPos(WorldHelper.centerOfBlockPos(pos, false).add(offset.x, offset.y, offset.z));
            level.addFreshEntity(control);
            console.addControl(control);
        }
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if(!(pNewState.getBlock() instanceof ConsoleBlock) && !pLevel.isClientSide){
            //If the new block is not a console block, we're getting removed

            //If there is a pre-existing tile, tell it to kill it's controls
            if(pLevel.getBlockEntity(pPos) instanceof ConsoleTile console){
                console.killAllControls();
            }
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
    }

    @Override
    public PushReaction getPistonPushReaction(BlockState pState) {
        return PushReaction.PUSH_ONLY;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state){
        return this.consoleTileFactory.get().create(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> otherType) {
        return pLevel.isClientSide ? createTickerHelper(otherType, this.consoleTileFactory.get(), ConsoleTile::tick) : null;
    }

    @Override
    public VoxelShape getInteractionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos) {
        return SHAPE;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }
}
