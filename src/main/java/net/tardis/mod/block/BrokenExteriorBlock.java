package net.tardis.mod.block;

import com.mojang.datafixers.util.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.blockentities.BrokenExteriorTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.PlayParticleMessage;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Supplier;

public class BrokenExteriorBlock extends AbstractMultiblockMasterBlock<BrokenExteriorTile> implements IPlayParticleEffects{

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    private final Supplier<ExteriorType> exteriorSupplier;
    private MultiblockData multiblockData;

    public BrokenExteriorBlock(Supplier<ExteriorType> exterior, MultiblockData data) {
        super(Properties.of(Material.BARRIER).noOcclusion().strength(99999999999999999999999999F), TileRegistry.BROKEN_EXTERIOR, MultiblockDatas.BROKEN_EXTERIOR);
        this.exteriorSupplier = exterior;
        this.multiblockData = data;
    }

    public BrokenExteriorBlock(Supplier<ExteriorType> exterior) {
        this(exterior, MultiblockDatas.BROKEN_EXTERIOR);
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult p_60508_) {

        if(level.getBlockEntity(pos) instanceof BrokenExteriorTile tile){
            final List<Pair<Integer, Ingredient>> unlocks = tile.createUnlockItemList();
            for(Pair<Integer, Ingredient> pair : unlocks){
                final ItemStack itemUsed = player.getItemInHand(hand);
                if(pair.getSecond().test(itemUsed)){

                    if(!player.isCreative()){
                        if(itemUsed.hasCraftingRemainingItem()){
                            player.setItemInHand(hand, itemUsed.getCraftingRemainingItem());
                        }
                        else player.getItemInHand(hand).shrink(1);
                    }

                    tile.modLoyalty(player, pair.getFirst());
                    if(!level.isClientSide)
                        Network.sendNear(level.dimension(), pos, 32, new PlayParticleMessage(pos, 0));
                    return InteractionResult.sidedSuccess(level.isClientSide());
                }
            }

        }

        return InteractionResult.PASS;
    }

    @Override
    public void animateTick(BlockState pState, Level pLevel, BlockPos pPos, RandomSource pRandom) {
        super.animateTick(pState, pLevel, pPos, pRandom);

        final Vec3 pos = WorldHelper.centerOfBlockPos(pPos, true);

        pLevel.addParticle(ParticleTypes.SMOKE, pos.x, pos.y + 1, pos.z, 0, 0.3, 0);

        //Rarer paricles

        if(pLevel.random.nextFloat() > 0.1)
            return;

        for(int i = 0; i < 4; ++i){
            pLevel.addParticle(ParticleTypes.LAVA,
                    pPos.getX() + 0.5 + Math.sin(pLevel.random.nextFloat()),
                    pPos.getY() + 0.5,
                    pPos.getZ() + 0.5 + Math.cos(pLevel.random.nextFloat()),
                    0, 0, 0
            );
        }

    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        BrokenExteriorTile tile = TileRegistry.BROKEN_EXTERIOR.get().create(pPos, pState);
        tile.setMasterData(this.multiblockData);
        tile.setExterior(this.exteriorSupplier.get());
        return tile;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Override
    public void playParticles(Level level, BlockPos pos, int extraData) {

        final int amount = 30;
        final float degreeStep = 360.0F / amount;

        for(int i = 0; i < amount; ++i){
            final double rad = Math.toRadians(i * degreeStep);

            level.addParticle(ParticleTypes.HEART,
                        pos.getX() + 0.5 + Math.sin(rad),
                        pos.getY() + 0.5,
                        pos.getZ() + 0.5 + Math.cos(rad),
                    0.0, 0.1, 0.0
            );
        }

    }
}
