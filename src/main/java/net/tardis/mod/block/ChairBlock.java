package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.block.misc.ExtraBlockShapes;
import net.tardis.mod.entity.ChairEntity;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

public class ChairBlock extends Block {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final int INTERACT_DIST = 4 * 4;
    final float chairHeight;
    final DirectionalVoxelShape shape;

    public ChairBlock(Properties pProperties, float chairHeight, DirectionalVoxelShape shape) {
        super(pProperties);
        this.chairHeight = chairHeight;
        this.shape = shape;
    }

    public static ChairBlock createComfy(){
        return new ChairBlock(
                BasicProps.Block.WOOL.get(),
                0.5F,
                new DirectionalVoxelShape(ExtraBlockShapes.makeComfyChairShape())
        );
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pPlayer.getOnPos().distSqr(pPos) > INTERACT_DIST){
            return InteractionResult.sidedSuccess(pLevel.isClientSide());
        }

        if(!pLevel.isClientSide && pHand == InteractionHand.MAIN_HAND){
            ChairEntity entity = EntityRegistry.CHAIR.get().create(pLevel).setHeight(this.chairHeight);
            entity.setPos(WorldHelper.centerOfBlockPos(pPos, false));
            pLevel.addFreshEntity(entity);
            pPlayer.startRiding(entity);
            entity.setHeight(chairHeight);
        }
        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext).setValue(FACING, pContext.getHorizontalDirection().getOpposite());

    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return this.shape.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    public boolean isOccupied(LevelAccessor level, BlockPos pos){
        return !level.getEntitiesOfClass(ChairEntity.class, new AABB(pos)).isEmpty();
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
