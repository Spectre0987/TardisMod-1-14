package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.Constants;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.TemporalScoopTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.ScoopVaultMenu;

import java.util.function.Supplier;

public class ScoopVaultBlock extends BaseTileBlock<TemporalScoopTile> {
    public ScoopVaultBlock(Properties pProperties, Supplier<? extends BlockEntityType<TemporalScoopTile>> tile) {
        super(pProperties, tile);
    }

    public static ScoopVaultBlock create(){
        return new ScoopVaultBlock(
                BasicProps.Block.WOOD.get().noOcclusion(),
                TileRegistry.TEMPORAL_SCOOP
        );
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pHand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(pLevel.isClientSide);

        if(!pLevel.isClientSide()){
            LazyOptional<ITardisLevel> tardisHolder = pLevel.getCapability(Capabilities.TARDIS);
            pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                NetworkHooks.openScreen((ServerPlayer) pPlayer, new SimpleMenuProvider(
                        (id, inv, player) -> new ScoopVaultMenu(id, inv, tardis),
                        getName()
                ));
            });
            if(!tardisHolder.isPresent()){
                Helper.sendHotbarMessage(pPlayer, Constants.Translation.MUST_BE_USED_IN_TARDIS);
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }
}
