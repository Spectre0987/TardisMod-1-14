package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.MenuConstructor;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.SimpleHorizonalTileBlock;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class QuantiscopeBlock extends SimpleHorizonalTileBlock<QuantiscopeTile> {
    public QuantiscopeBlock(Properties pProperties, Supplier<? extends BlockEntityType<QuantiscopeTile>> type) {
        super(pProperties, type);
    }

    public static QuantiscopeBlock create(){
        return new QuantiscopeBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.QUANTISCOPE
        );
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, this.blockEntityType.get(), (level, pos, state, tile) -> tile.tick());
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof QuantiscopeTile tile && pPlayer instanceof ServerPlayer serverPlayer){
            tile.getCurrentSetting().getMenu().ifPresent(menu -> {
                NetworkHooks.openScreen(serverPlayer, new SimpleMenuProvider(menu, getName()), pPos);
            });
        }

        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }
}
