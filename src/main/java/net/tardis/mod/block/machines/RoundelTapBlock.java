package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.blockentities.RoundelTapTile;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class RoundelTapBlock<T extends RoundelTapTile> extends BaseTileBlock<T> {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public final Component NEEDS_ROUNDEL_TRANS = Component.translatable(Constants.Translation.makeGenericTranslation(ForgeRegistries.BLOCKS, this) + ".needs_roundel");

    public RoundelTapBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        super(pProperties, type);
    }

    public static RoundelTapBlock<RoundelTapTile> create(){
        return new RoundelTapBlock<>(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.ROUNDEL_TAP
        );
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(FACING, pContext.getClickedFace());
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return pLevel.isClientSide() ? null : createTickerHelper(pBlockEntityType, TileRegistry.ROUNDEL_TAP.get(), (level, pos, state, tile) -> tile.tick());
    }

    public static boolean isOnRoundel(BlockState tap, BlockGetter level, BlockPos pos){
        return level.getBlockState(pos.relative(tap.getValue(BlockStateProperties.FACING).getOpposite())).getBlock() instanceof RoundelBlock;
    }
}
