package net.tardis.mod.block.machines;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.ARSPanelTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.items.functions.sonic.SonicBlockInteractions;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.ARSMenu;
import net.tardis.mod.world.data.ARSRoomLevelData;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public class ARSPanelBlock extends BaseTileBlock<ARSPanelTile> implements SonicBlockInteractions.ISonicBlockAction {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(Helper.blockShapePixels(2, 0, 12, 14, 12, 16));

    public ARSPanelBlock(Properties pProperties, Supplier<? extends BlockEntityType<ARSPanelTile>> type) {
        super(pProperties, type);
    }

    public static ARSPanelBlock create(){
        return new ARSPanelBlock(BasicProps.Block.METAL.get().noOcclusion(), TileRegistry.ARS_PANEL);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(FACING));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext).setValue(FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof ARSPanelTile ars){

            if(ars.doWork(pLevel, pHit.getLocation(), pPlayer.getItemInHand(pHand))){
                return InteractionResult.sidedSuccess(pLevel.isClientSide());
            }

            if(pHand == InteractionHand.MAIN_HAND){
                if(!pLevel.isClientSide()) {

                    ARSRoomLevelData.getData((ServerLevel) pLevel).getRoomFor(pPos).ifPresent(entry -> {
                        Tardis.LOGGER.debug("Found ARS Entry " + entry.room.toString());
                    });

                    ars.displayMissingWork(pPlayer);
                    NetworkHooks.openScreen((ServerPlayer) pPlayer, new SimpleMenuProvider(
                            (id, inv, player) -> new ARSMenu(id, inv, ars),
                            Component.empty()
                    ), pPos);
                }
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public boolean useOnBlock(BlockState target, BlockPos pos, ItemStack sonic, Level level, Player player, InteractionHand hand) {
        return true;
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
