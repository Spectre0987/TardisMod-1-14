package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.RiftCollectorTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.sonic.SonicBlockInteractions;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class RiftCollectorBlock extends BaseTileBlock<RiftCollectorTile> implements SonicBlockInteractions.ISonicBlockAction {

    private Component safetyEngaged;
    private Component safetyDisengaged;

    public RiftCollectorBlock(Properties pProperties, Supplier<? extends BlockEntityType<RiftCollectorTile>> type) {
        super(pProperties, type);
    }

    public static RiftCollectorBlock create(){
        return new RiftCollectorBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.RIFT_COLLECTOR
        );
    }

    public Component getSonicTranslation(boolean on){
        final String base = Constants.Translation.makeGenericTranslation(ForgeRegistries.BLOCKS, this);
        if(on){
            if(this.safetyEngaged == null){
                this.safetyEngaged = Component.translatable(base + ".engaged");
            }
            return this.safetyEngaged;
        }

        if(this.safetyDisengaged == null){
            this.safetyDisengaged = Component.translatable(base + ".disenaged");
        }

        return this.safetyDisengaged;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof RiftCollectorTile collector && !pLevel.isClientSide()){
            final ItemStack held = pPlayer.getItemInHand(pHand);

            //If a battery or rift fuel
            if(held.getCapability(Capabilities.ARTRON_TANK_ITEM).isPresent() || ItemArtronValueReloader.ArtronValue.getArtronValue(held) > 0 || (pHand == InteractionHand.MAIN_HAND && held.isEmpty())){
                ItemStack returned = collector.setItemStack(held);
                pPlayer.setItemInHand(pHand, returned);

            }
        }
        return InteractionResult.sidedSuccess(pLevel.isClientSide);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return pLevel.isClientSide ? null : createTickerHelper(pBlockEntityType, TileRegistry.RIFT_COLLECTOR.get(), (level, pos, state, tile) -> tile.tick());
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public boolean useOnBlock(BlockState target, BlockPos pos, ItemStack sonic, Level level, Player player, InteractionHand hand) {
        if(!level.isClientSide() && level.getBlockEntity(pos) instanceof RiftCollectorTile collector){
            collector.setSafety(!collector.areSafetiesEngaged());
            player.sendSystemMessage(getSonicTranslation(collector.areSafetiesEngaged()));
        }
        return true;
    }
}
