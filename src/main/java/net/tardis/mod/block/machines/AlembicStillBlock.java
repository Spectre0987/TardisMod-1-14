package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.blockentities.crafting.AlembicStillBlockEntity;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class AlembicStillBlock<T extends AlembicStillBlockEntity> extends AlembicBlock<T>{

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(makeShape());


    public AlembicStillBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        super(pProperties, type);
    }

    public static AlembicStillBlock<AlembicStillBlockEntity> create(){
        return new AlembicStillBlock<>(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.ALEMBIC_STILL
        );
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pMovedByPiston) {
        super.onPlace(pState, pLevel, pPos, pOldState, pMovedByPiston);
        if(pState.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
            MultiblockDatas.STILL.placeBlocks(pLevel, pPos, Helper.getRotationFromDirection(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
        }
        else MultiblockDatas.STILL.placeBlocks(pLevel, pPos);
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        if(pState.getBlock() != pNewState.getBlock()){
            if(pState.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
                MultiblockDatas.STILL.destroy(pLevel, pPos, Helper.getRotationFromDirection(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
            }
            else MultiblockDatas.STILL.destroy(pLevel, pPos);
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    @Override
    public @Nullable BlockEntity newBlockEntity(BlockPos blockPos, BlockState blockState) {
        T still = this.blockEntityType.get().create(blockPos, blockState);
        still.setMasterData(MultiblockDatas.STILL);
        return still;
    }

    @Override
    public @Nullable <T1 extends BlockEntity> BlockEntityTicker<T1> getTicker(Level pLevel, BlockState pState, BlockEntityType<T1> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, blockEntityType.get(), AlembicStillBlockEntity::tick);
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0, 0, 0, 1, 1.125, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.125, 0.875, 0.125, 0.875, 2, 0.875), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.625, 1.4375, 0.34375, 0.125, 1.9375, 0.65625), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-1, 1, 0.0625, -0.125, 1.5625, 0.9375), BooleanOp.OR);

        return shape;
    }
}
