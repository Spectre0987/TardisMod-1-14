package net.tardis.mod.block.machines;

import it.unimi.dsi.fastutil.ints.Int2BooleanArrayMap;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.menu.DefaultMenuProvider;
import net.tardis.mod.menu.EngineMenu;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.subsystem.SubsystemType;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.function.Supplier;

public class EngineBlock<T extends BlockEntity> extends BaseEntityBlock {

    public final Supplier<BlockEntityType<T>> engine;


    public EngineBlock(Properties pProperties, Supplier<BlockEntityType<T>> type) {
        super(pProperties.noOcclusion());
        this.engine = type;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pHand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(pLevel.isClientSide());

        if(!pLevel.isClientSide && pHit.getDirection().getAxis().isHorizontal()){

            Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                final Int2BooleanArrayMap activeToggles = EngineMenu.buildToggleList(pLevel, pHit.getDirection());

                NetworkHooks.openScreen((ServerPlayer)pPlayer, new DefaultMenuProvider<>((id, inv) ->
                                new EngineMenu(id, inv, tardis, pHit.getDirection())),
                        buf -> {
                            buf.writeEnum(pHit.getDirection());
                            buf.writeInt(activeToggles.size());
                            for(int i : activeToggles.keySet()){
                                buf.writeInt(i);
                                buf.writeBoolean(activeToggles.get(i));
                            }
                        }
                );
            });
        }


        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return this.engine.get().create(pPos, pState);
    }
}
