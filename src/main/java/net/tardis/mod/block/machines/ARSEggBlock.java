package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public class ARSEggBlock extends Block {

    public static final VoxelShape SHAPE = makeShape();

    public ARSEggBlock(Properties pProperties) {
        super(pProperties);
    }

    public static ARSEggBlock create(){
        return new ARSEggBlock(
                BasicProps.Block.STONE.get().noOcclusion()
        );
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            if(pPlayer instanceof ServerPlayer player){
                Network.sendTo(player, new OpenGuiDataMessage(GuiDatas.ARS_EGG.create().with(tardis).addAllItems(player.getLevel())));
            }
        });
        return InteractionResult.sidedSuccess(!pLevel.isClientSide());
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0.3125, 0, 0.3125, 0.6875, 0.625, 0.6875), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.359375, 0.625, 0.359375, 0.640625, 0.6875, 0.640625), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.421875, 0.6875, 0.421875, 0.578125, 1, 0.578125), BooleanOp.OR);

        return shape;
    }
}
