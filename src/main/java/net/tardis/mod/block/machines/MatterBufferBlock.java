package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.Tags;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.SimpleHorizonalTileBlock;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.MatterBufferTile;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class MatterBufferBlock extends SimpleHorizonalTileBlock<MatterBufferTile> {
    public MatterBufferBlock(Properties pProperties, Supplier<? extends BlockEntityType<MatterBufferTile>> type) {
        super(pProperties, type);
    }

    public static MatterBufferBlock create(){
        return new MatterBufferBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.MATTER_BUFFER
        );
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, this.blockEntityType.get(), (level, pos, state, tile) -> tile.tick());
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pHand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(pLevel.isClientSide());

        final ItemStack held = pPlayer.getItemInHand(pHand);

        if(!held.isEmpty() && !held.is(Tags.Items.COBBLESTONE)){
            return InteractionResult.sidedSuccess(pLevel.isClientSide());
        }


        if(!pLevel.isClientSide() && pLevel.getBlockEntity(pPos) instanceof MatterBufferTile buffer){
            pPlayer.setItemInHand(pHand, buffer.getInventory().insertItem(0, pPlayer.getItemInHand(pHand), false));
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide);
    }
}
