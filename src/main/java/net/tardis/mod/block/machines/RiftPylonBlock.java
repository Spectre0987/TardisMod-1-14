package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.RiftPylonTile;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.registry.ParticleRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class RiftPylonBlock extends BaseTileBlock {

    public static final VoxelShape SHAPE = makeShape();

    public RiftPylonBlock(Properties pProperties, Supplier<? extends BlockEntityType<?>> type) {
        super(pProperties, type);
    }

    public static RiftPylonBlock create(){
        return new RiftPylonBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.RIFT_PYLON
        );
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return !pLevel.isClientSide() ? createTickerHelper(pBlockEntityType, TileRegistry.RIFT_PYLON.get(), (level, pos, state, tile) -> tile.tick()) : null;
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0, 0, 0, 1, 0.625, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.3125, 0.625, 0.3125, 0.6875, 1, 0.6875), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.375, 1, 0.375, 0.625, 1.65625, 0.625), BooleanOp.OR);

        return shape;
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public void animateTick(BlockState pState, Level pLevel, BlockPos pPos, RandomSource pRandom) {
        super.animateTick(pState, pLevel, pPos, pRandom);

        if(pLevel.getGameTime() % 10 != 0 || Rift.getFromChunk(pLevel.getChunkAt(pPos)).isEmpty())
            return;

        final Vec3 pos = WorldHelper.centerOfBlockPos(pPos, true).add(0, 0.5, 0);
        pLevel.addParticle(ParticleRegistry.RIFT.get(), pos.x, pos.y, pos.z, 0, 0.01, 0);

        if(pLevel.getBlockEntity(pPos) instanceof RiftPylonTile pylon){
            pylon.getCollector(true).ifPresent(col -> {
                float dist = 1F;
                Vec3 path = WorldHelper.centerOfBlockPos(col.getBlockPos(), true).subtract(WorldHelper.centerOfBlockPos(pylon.getBlockPos(), true));
                final Vec3 speed = path.normalize().scale(0.01F);
                int step = Mth.floor(path.length() / dist);

                for(int i = 0; i < step; ++i){
                    Vec3 partPos = path.normalize()
                            .scale((i / (float)step) * path.length())
                            .add(WorldHelper.centerOfBlockPos(pylon.getBlockPos().above(), true));
                    pLevel.addParticle(ParticleRegistry.RIFT.get(), partPos.x, partPos.y, partPos.z,
                            speed.x, speed.y, speed.z);
                }

            });
        }

    }
}
