package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructrureType;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructure;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

import java.util.function.Supplier;

public class DefaultSingleBlockTardisStructureBlock<T extends TardisWorldStructure> extends Block {

    final Supplier<? extends TardisWorldStructrureType<T>> factory;

    public DefaultSingleBlockTardisStructureBlock(Properties pProperties, Supplier<? extends TardisWorldStructrureType<T>> factory) {
        super(pProperties);
        this.factory = factory;
    }

    public static <T extends TardisWorldStructure> DefaultSingleBlockTardisStructureBlock<T> create(Supplier<? extends TardisWorldStructrureType<T>> factory){
        return new DefaultSingleBlockTardisStructureBlock<T>(
                BasicProps.Block.METAL.get().noOcclusion(),
                factory
        );
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
        pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            tardis.getInteriorManager().removeWorldStructure(pPos);
        });
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pMovedByPiston) {
        super.onPlace(pState, pLevel, pPos, pOldState, pMovedByPiston);
        pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            tardis.getInteriorManager().addWorldStructre(pPos, this.factory.get());
        });
    }
}
