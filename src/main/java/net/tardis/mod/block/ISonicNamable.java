package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.sonic.SonicBlockInteractions;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

import java.util.Optional;

public interface ISonicNamable extends SonicBlockInteractions.ISonicBlockAction {

    void setName(Level level, BlockPos pos, Optional<String> name);
    Optional<String> getExistingName(Level level, BlockPos pos);

    default boolean useOnBlock(BlockState target, BlockPos pos, ItemStack sonic, Level level, Player player, InteractionHand hand){
        if(!level.isClientSide()){
            Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
                Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(GuiDatas.SONIC_NAME.create().create(getExistingName(level, pos), pos)));
            });
        }
        return true;
    }

}
