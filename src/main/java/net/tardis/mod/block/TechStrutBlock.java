package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import org.jetbrains.annotations.Nullable;

public class TechStrutBlock extends Block {

    public static final EnumProperty<StrutType> STRUT_TYPE = EnumProperty.create("strut_type", StrutType.class);
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(Shapes.box(0.359375, 0, 0.25, 0.640625, 1, 1));

    public TechStrutBlock(Properties pProperties) {
        super(pProperties);
    }

    public static TechStrutBlock create(){
        return new TechStrutBlock(BasicProps.Block.METAL.get().noOcclusion());
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pIsMoving) {
        super.onPlace(pState, pLevel, pPos, pOldState, pIsMoving);

        if(pState.getBlock() == this){
            BlockState potentialState = getStateForWorld(pLevel, pState, pPos);
            if(!pState.equals(potentialState)){
                pLevel.setBlock(pPos, potentialState, 3);
            }
        }

    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext).setValue(FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    public BlockState getStateForWorld(LevelReader level, BlockState state, BlockPos pos){
        if(level.getBlockState(pos.below()).isCollisionShapeFullBlock(level, pos)){
            return state.setValue(STRUT_TYPE, StrutType.BOTTOM);
        }
        if(level.getBlockState(pos.above()).isCollisionShapeFullBlock(level, pos)){
            return state.setValue(STRUT_TYPE, StrutType.TOP);
        }
        return state.setValue(STRUT_TYPE, StrutType.MIDDLE);
    }

    @Override
    public void neighborChanged(BlockState pState, Level pLevel, BlockPos pPos, Block pBlock, BlockPos pFromPos, boolean pIsMoving) {
        super.neighborChanged(pState, pLevel, pPos, pBlock, pFromPos, pIsMoving);
        BlockState potState = getStateForWorld(pLevel, pState, pPos);
        if(!pState.equals(potState)){
            pLevel.setBlock(pPos, potState, 3);
        }
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(STRUT_TYPE, FACING);
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }


    public enum StrutType implements StringRepresentable {
        TOP,
        MIDDLE,
        BOTTOM;

        @Override
        public String getSerializedName() {
            return this.name().toLowerCase();
        }
    }

}
