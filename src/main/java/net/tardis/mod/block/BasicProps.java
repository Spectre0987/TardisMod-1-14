package net.tardis.mod.block;

import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;

import java.util.function.Supplier;

public class BasicProps {

    public class Block{

        public static final Supplier<BlockBehaviour.Properties> CRYSTAL = () -> BlockBehaviour.Properties.of(Material.AMETHYST)
                .sound(SoundType.AMETHYST)
                .strength(0.3F)
                .noOcclusion();

        public static final Supplier<BlockBehaviour.Properties> METAL = () -> BlockBehaviour.Properties.of(Material.METAL)
                .sound(SoundType.METAL)
                .strength(1.0F);

        public static final Supplier<BlockBehaviour.Properties> WOOL = () -> BlockBehaviour.Properties.of(Material.WOOL)
                .sound(SoundType.WOOL)
                .strength(0.4F);

        public static final Supplier<BlockBehaviour.Properties> GLASS = () -> BlockBehaviour.Properties.of(Material.GLASS)
                .strength(0.3F)
                .noOcclusion();

        public static final Supplier<BlockBehaviour.Properties> WOOD = () -> BlockBehaviour.Properties.of(Material.WOOD)
                .sound(SoundType.WOOD)
                .strength(0.5F, 3.0F);
        public static final Supplier<BlockBehaviour.Properties> STONE = () -> BlockBehaviour.Properties.of(Material.STONE)
                .sound(SoundType.STONE)
                .strength(0.5F);
        public static final Supplier<BlockBehaviour.Properties> INVINCIBLE = () -> BlockBehaviour.Properties.of(Material.BARRIER)
                .strength(9999999);
    }

    public class Item{

        public static final Supplier<net.minecraft.world.item.Item.Properties> BASE = () ->new net.minecraft.world.item.Item.Properties()
                .stacksTo(64);

        public static final Supplier<net.minecraft.world.item.Item.Properties> ONE = () -> new net.minecraft.world.item.Item.Properties()
                .stacksTo(1);

    }

}
