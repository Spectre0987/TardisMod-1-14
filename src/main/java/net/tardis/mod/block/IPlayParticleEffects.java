package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

public interface IPlayParticleEffects {

    void playParticles(Level level, BlockPos pos, int extraData);

}
