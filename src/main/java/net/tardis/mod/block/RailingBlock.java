package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.Half;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import org.jetbrains.annotations.Nullable;

public class RailingBlock extends SimpleHorizonalBlock {

    public static final DirectionalVoxelShape LEFT_SHAPE = new DirectionalVoxelShape(Helper.blockShapePixels(14, 0, 0, 16, 16, 16));
    public static final DirectionalVoxelShape RIGHT_SHAPE = new DirectionalVoxelShape(Helper.blockShapePixels(0, 0, 0, 2, 16, 16));


    public static final BooleanProperty IS_ANGLED = BooleanProperty.create("is_angled");
    public static final BooleanProperty RIGHT = BooleanProperty.create("right");

    public RailingBlock(Properties pProperties) {
        super(pProperties);
    }

    public static RailingBlock create(){
        return new RailingBlock(BasicProps.Block.METAL.get().noOcclusion());
    }

    @Override
    public @Nullable BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return setRightFromPlacement(stateBasedOnWorld(super.getStateForPlacement(pContext), pContext.getLevel(), pContext.getClickedPos()), pContext);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(IS_ANGLED, RIGHT);
    }

    public BlockState stateBasedOnWorld(BlockState original, BlockGetter level, BlockPos pos){
        final BlockState below = level.getBlockState(pos.below());
        if(below.is(BlockTags.STAIRS)){
            if(
                    WorldHelper.stateValueEquals(below, StairBlock.HALF, Half.BOTTOM, false) &&
                            WorldHelper.stateValueEquals(below, BlockStateProperties.HORIZONTAL_FACING, original.getValue(BlockStateProperties.HORIZONTAL_FACING).getOpposite(), false)
            ){
                return original.setValue(IS_ANGLED, true);
            }
        }
        return original.setValue(IS_ANGLED, false);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return pState.getValue(RIGHT) ?
                RIGHT_SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)) :
                LEFT_SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    @Override
    public void neighborChanged(BlockState pState, Level pLevel, BlockPos pPos, Block pNeighborBlock, BlockPos pNeighborPos, boolean pMovedByPiston) {
        super.neighborChanged(pState, pLevel, pPos, pNeighborBlock, pNeighborPos, pMovedByPiston);
        BlockState newState = stateBasedOnWorld(pState, pLevel, pPos);
        if(!newState.equals(pState))
            pLevel.setBlock(pPos, newState, 3);
    }

    public static BlockState setRightFromPlacement(BlockState state, BlockPlaceContext context){
        Vec3 vec = context.getClickLocation().subtract(WorldHelper.centerOfBlockPos(context.getClickedPos()))
                .yRot(state.getValue(BlockStateProperties.HORIZONTAL_FACING).toYRot());
        return state.setValue(RIGHT, vec.x() > 0);
    }
}
