package net.tardis.mod.advancement;

import com.google.gson.JsonObject;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.CriterionTrigger;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.advancements.critereon.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.helpers.Helper;

public class TardisTriggers {

    public static final PlayerTrigger TAKE_OFF_TARDIS = CriteriaTriggers.register(new PlayerTrigger(Helper.createRL("takeoff_tardis")));


    //MUST be called so the field is registered before datapack loading
    public static void init(){}

    public class EnterTARDISCriterion extends Criterion{

        public static class Trigger extends SimpleCriterionTrigger {

            final ResourceLocation id;

            public Trigger(ResourceLocation id, ResourceLocation parent){
                this.id = id;
            }

            @Override
            protected AbstractCriterionTriggerInstance createInstance(JsonObject json, EntityPredicate.Composite pPlayer, DeserializationContext pContext) {
                return null;
            }

            @Override
            public ResourceLocation getId() {
                return id;
            }
        }

    }
}
