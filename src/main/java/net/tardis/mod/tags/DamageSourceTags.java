package net.tardis.mod.tags;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageType;
import net.tardis.mod.helpers.Helper;

public class DamageSourceTags {


    public static TagKey<DamageType> temporalGraceBlocks = create("temporal_grace_blocks");



    public static TagKey<DamageType> create(String name){
        return TagKey.create(Registries.DAMAGE_TYPE, Helper.createRL(name));
    }

}
