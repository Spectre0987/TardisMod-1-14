package net.tardis.mod.tags;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.Helper;

public class ItemTags {

    public static final TagKey<Item> CREATIVE_TAB_ROUNDEL = TagKey.create(Registries.ITEM, Helper.createRL("roundel_tab"));
    public static final TagKey<Item> CREATIVE_TAB_COMPONENTS = TagKey.create(Registries.ITEM, Helper.createRL("component_tab"));
    public static final TagKey<Item> SHOW_CONTROL_ITEMS = create("show_tardis_control");
    public static final TagKey<Item> SONIC_PORT_ACCEPTS = create("sonic_port");
    public static final TagKey<Item> ARS_EGG_ITEMS = create("ars_egg_blocks");
    public static final TagKey<Item> ALEMBIC_BIOMASS = create("biomass");
    public static final TagKey<Item> KEYS = create("tardis_keys");
    public static final TagKey<Item> ARS_LOCKED = create("ars_locked");
    public static final TagKey<Item> TARDIS_MUSIC_DISK = create("tardis_music_disk");

    public static final TagKey<Item> COPPER_PLATES = forge("plates/copper");

    //Forge
    public static final TagKey<Item> MERCURY_BOTTLE = forge("bottles/mercury");
    public static final TagKey<Item> CINNABAR = forge("gems/cinnabar");


    public static TagKey<Item> create(String s){
        return TagKey.create(Registries.ITEM, Helper.createRL(s));
    }

    public static TagKey<Item> forge(String s){
        return TagKey.create(Registries.ITEM, new ResourceLocation("forge", s));
    }

}
