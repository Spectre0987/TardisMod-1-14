package net.tardis.mod.tags;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;
import net.tardis.mod.helpers.Helper;

public class BlockTags {

    public static final TagKey<Block> UNBREAKABLES = create("unbreakable");
    public static final TagKey<Block> TEMPORAL_DISTORTION = create("temporal_distortion");
    public static final TagKey<Block> SONIC_BLACKLIST = create("sonic_blacklist");
    public static final TagKey<Block> BROKEN_EXTERIORS = create("broken_exterior");
    public static final TagKey<Block> CONSOLES = create("consoles");
    public static final TagKey<Block> EXTERIORS = create("exteriors");
    public static final TagKey<Block> FULL_ROUNDEL = create("full_roundel");


    public static TagKey<Block> create(String name){
        return TagKey.create(Registries.BLOCK, Helper.createRL(name));
    }
}
