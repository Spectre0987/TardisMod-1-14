package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.CarExteriorEntity;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.exterior.ChameleonExterior;
import net.tardis.mod.exterior.EntityExterior;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.exterior.TileExterior;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.DoorState;

import java.util.function.Supplier;

public class ExteriorRegistry {

    public static final DeferredRegister<ExteriorType> EXTERIORS = DeferredRegister.create(Helper.createRL("exterior"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<ExteriorType>> REGISTRY = EXTERIORS.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<ExteriorType> STEAM = EXTERIORS.register("steam", () -> new ExteriorType((type, tardis) ->
            new TileExterior(type, tardis, BlockRegistry.STEAM_EXTERIOR.get().defaultBlockState()))
                .setMustBeUnlocked());
    public static final RegistryObject<ExteriorType> TT_CAPSULE = EXTERIORS.register("tt_capsule", () -> new ExteriorType((type, tardis) ->
            new TileExterior(type, tardis, BlockRegistry.TT_CAPSULE.get().defaultBlockState()), DoorState.CLOSED, DoorState.BOTH)
    );
    public static final RegistryObject<ExteriorType> POLICE_BOX = EXTERIORS.register("police_box", () -> new ExteriorType(
            (type, tardis) -> new TileExterior(type, tardis, BlockRegistry.POLICE_BOX_EXTERIOR.get().defaultBlockState())
    ).setMustBeUnlocked());

    public static final RegistryObject<ExteriorType> CHAMELEON = EXTERIORS.register("chameleon", () -> new ExteriorType(
            (type, tardis) -> new ChameleonExterior(type, tardis, BlockRegistry.CHAMELEON_EXTERIOR.get().defaultBlockState())
    ));

    public static final RegistryObject<ExteriorType> IMPALA = EXTERIORS.register("impala", () -> new ExteriorType(
            (type, tardis) -> new EntityExterior(type, tardis, level -> new CarExteriorEntity(EntityRegistry.IMPALA.get(), level))
    ).setMustBeUnlocked());
    public static final RegistryObject<ExteriorType> COFFIN = EXTERIORS.register("coffin", () -> new ExteriorType(
            (type, tardis) -> new TileExterior(type, tardis, BlockRegistry.COFFIN_EXTERIOR.get().defaultBlockState())
    ).setMustBeUnlocked());
    public static final RegistryObject<ExteriorType> OCTA = EXTERIORS.register("octa", () -> new ExteriorType(
            (type, tardis) -> new TileExterior(type, tardis, BlockRegistry.OCTA_EXTERIOR.get().defaultBlockState())
    ).setMustBeUnlocked());
    public static final RegistryObject<ExteriorType> TRUNK = EXTERIORS.register("trunk", () -> new ExteriorType(
            (type, tardis) -> new TileExterior(type, tardis, BlockRegistry.TRUNK_EXTERIOR.get().defaultBlockState())
    ).setMustBeUnlocked());
    public static final RegistryObject<ExteriorType> SPRUCE = EXTERIORS.register("spruce", () -> new ExteriorType(
            (type, tardis) -> new TileExterior(type, tardis, BlockRegistry.SPRUCE_EXTERIOR.get().defaultBlockState())
    ).setMustBeUnlocked());

}
