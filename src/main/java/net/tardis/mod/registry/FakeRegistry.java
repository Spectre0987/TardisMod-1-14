package net.tardis.mod.registry;

import net.minecraft.resources.ResourceLocation;

import javax.swing.text.html.Option;
import java.util.HashMap;
import java.util.Optional;

public class FakeRegistry<T> {

    private final HashMap<ResourceLocation, T> map = new HashMap<>();
    public final T defaultObject;

    public FakeRegistry(T defaultObject){
        this.defaultObject = defaultObject;
    }

    public void register(ResourceLocation key, T object){
        this.map.put(key, object);
    }

    public T getDefaultObject(){
        return this.defaultObject;
    }

    public T getObjectOrDefault(ResourceLocation key){
        return this.getObject(key).orElse(this.defaultObject);
    }

    public Optional<T> getObject(ResourceLocation key){
        if(this.map.containsKey(key))
            return Optional.of(this.map.get(key));
        return Optional.empty();
    }

}
