package net.tardis.mod.registry;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DataPackRegistryEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.item.misc.TardisTool;
import net.tardis.mod.loot.BlockLootTable;
import net.tardis.mod.loot.EntitiesAsLoot;
import net.tardis.mod.loot.SchematicLootTable;
import net.tardis.mod.misc.PhasedShellDisguise;
import net.tardis.mod.misc.TardisDimensionInfo;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.sonic_screwdriver.SonicPartConnectionPoint;
import net.tardis.mod.sound.SoundScheme;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class JsonRegistries {

    public static ResourceKey<Registry<SoundScheme>> SOUND_SCHEMES_REGISTRY = create("sound_scheme");
    public static ResourceKey<Registry<ARSRoom>> ARS_ROOM_REGISTRY = create("ars_room");
    public static ResourceKey<Registry<TardisDimensionInfo>> TARDIS_DIM_INFO = create("tardis_dimension_info");
    public static ResourceKey<Registry<SchematicLootTable>> SCHEMATIC_LOOT = create("loot/schematic_loot");
    public static ResourceKey<Registry<BlockLootTable>> BLOCK_AS_LOOT = create("loot/block_as_loot");
    public static ResourceKey<Registry<EntitiesAsLoot>> ENTITIES_AS_LOOT = create("loot/entities");
    public static ResourceKey<Registry<TardisTool>> TOOLS = create("tool_items");
    public static ResourceKey<Registry<PhasedShellDisguise>> POS_DISGUISE = create("disguises");
    public static ResourceKey<Registry<TextureVariant>> TEXTURE_VARIANTS = create("texture_variants");

    public static <T> ResourceKey<Registry<T>> create(String name){
        return ResourceKey.createRegistryKey(Helper.createRL(name));
    }



    @SubscribeEvent
    public static void register(DataPackRegistryEvent.NewRegistry event){

        event.dataPackRegistry(SOUND_SCHEMES_REGISTRY, SoundScheme.CODEC, SoundScheme.CODEC);
        event.dataPackRegistry(ARS_ROOM_REGISTRY, ARSRoom.CODEC, ARSRoom.CODEC);
        event.dataPackRegistry(TARDIS_DIM_INFO, TardisDimensionInfo.CODEC, TardisDimensionInfo.CODEC);
        event.dataPackRegistry(SCHEMATIC_LOOT, SchematicLootTable.CODEC, SchematicLootTable.CODEC);
        event.dataPackRegistry(BLOCK_AS_LOOT, BlockLootTable.CODEC, BlockLootTable.CODEC);
        event.dataPackRegistry(TOOLS, TardisTool.CODEC, TardisTool.CODEC);
        event.dataPackRegistry(ENTITIES_AS_LOOT, EntitiesAsLoot.CODEC, EntitiesAsLoot.CODEC);
        event.dataPackRegistry(POS_DISGUISE, PhasedShellDisguise.CODEC, PhasedShellDisguise.CODEC);
        event.dataPackRegistry(TEXTURE_VARIANTS, TextureVariant.CODEC, TextureVariant.CODEC);
    }

}
