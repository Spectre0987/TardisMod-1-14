package net.tardis.mod.registry;

import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.flight_event.AddArtronFlightEvent;
import net.tardis.mod.flight_event.AxisMissFlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.flight_event.TimeWindsEvent;
import net.tardis.mod.flight_event.space_battle.SpaceBattleDodgeFireFlightEvent;
import net.tardis.mod.flight_event.space_battle.SpaceBattleEscapeFlightEvent;
import net.tardis.mod.flight_event.space_battle.SpaceBattleSearchFlightEvent;
import net.tardis.mod.helpers.Helper;

import java.util.List;
import java.util.function.Supplier;

public class FlightEventRegistry {

    public static final DeferredRegister<FlightEventType> FLIGHT_EVENTS = DeferredRegister.create(Helper.createRL("flight_event"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<FlightEventType>> REGISTRY = FLIGHT_EVENTS.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<FlightEventType> TIME_WINDS = FLIGHT_EVENTS.register("time_winds", () -> new FlightEventType(
            1.0F,
            TimeWindsEvent::new,
            TimeWindsEvent.SHOULD_HAPPEN)
    );
    public static final RegistryObject<FlightEventType> ADD_ARTRON = FLIGHT_EVENTS.register("add_artron", () -> new FlightEventType(
            0.2F, AddArtronFlightEvent::new, AddArtronFlightEvent.SHOULD_HAPPEN)
    );
    public static final RegistryObject<FlightEventType> X_SHIFT  = FLIGHT_EVENTS.register("x_shift", () -> new FlightEventType(1.0F, (type, tardis) ->
            new AxisMissFlightEvent(type, tardis, ControlRegistry.X, Direction.Axis.X)
    ));
    public static final RegistryObject<FlightEventType> Y_SHIFT  = FLIGHT_EVENTS.register("y_shift", () -> new FlightEventType(1.0F, (type, tardis) ->
            new AxisMissFlightEvent(type, tardis, ControlRegistry.Y, Direction.Axis.Y)
    ));
    public static final RegistryObject<FlightEventType> Z_SHIFT  = FLIGHT_EVENTS.register("z_shift", () -> new FlightEventType(1.0F, (type, tardis) ->
            new AxisMissFlightEvent(type, tardis, ControlRegistry.Z, Direction.Axis.Z)
    ));


    public static FlightEventType getRandomType(ITardisLevel tardis){
        RandomSource source = tardis.getLevel().getRandom();

        float chance = source.nextFloat();
        List<FlightEventType> validTypes = REGISTRY.get().getValues().stream()
                .filter(FlightEventType::isNatural)
                .filter(type -> type.canApply(tardis))
                .filter(type -> type.chance >= chance).toList();

        return validTypes.isEmpty() ? X_SHIFT.get() : validTypes.get(source.nextInt(validTypes.size()));


    }


    //Space battle events
    public static final RegistryObject<FlightEventType> SPACE_EVENT_DODGE = FLIGHT_EVENTS.register("space_dodge", () ->
            new FlightEventType(0.0F, SpaceBattleDodgeFireFlightEvent::new).notNatural());
    public static final RegistryObject<FlightEventType> SPACE_EVENT_SEARCH = FLIGHT_EVENTS.register("space_search", () ->
            new FlightEventType(0.0F, SpaceBattleSearchFlightEvent::new).notNatural());
    public static final RegistryObject<FlightEventType> SPACE_EVENT_ESCAPE = FLIGHT_EVENTS.register("space_escape", () ->
            new FlightEventType(0.0F, SpaceBattleEscapeFlightEvent::new).notNatural());



}
