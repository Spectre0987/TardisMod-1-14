package net.tardis.mod.registry;

import com.mojang.serialization.Codec;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.emotional.traits.EmptyTrait;
import net.tardis.mod.emotional.traits.LikeBiomeTrait;
import net.tardis.mod.emotional.traits.LikeCreatureTrait;
import net.tardis.mod.emotional.traits.LikeUseItemTrait;
import net.tardis.mod.helpers.Helper;

import java.util.function.Supplier;

public class TraitRegistry {

    public static final DeferredRegister<Codec<? extends Trait>> TYPES = DeferredRegister.create(Helper.createRL("trait"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<Codec<? extends Trait>>> REGISTRY = TYPES.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<Codec<LikeCreatureTrait>> LIKE_CREATURE = TYPES.register("like_creature", () -> LikeCreatureTrait.CODEC);
    public static final RegistryObject<Codec<LikeBiomeTrait>> LIKE_BIOME = TYPES.register("like_biome", () -> LikeBiomeTrait.CODEC);
    public static final RegistryObject<Codec<LikeUseItemTrait>> LIKE_USE_ITEM = TYPES.register("like_use_item", () -> LikeUseItemTrait.CODEC);
    public static final RegistryObject<Codec<EmptyTrait>> EMPTY = TYPES.register("empty_trait", () -> EmptyTrait.CODEC);



}
