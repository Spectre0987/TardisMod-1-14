package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.vortex.*;

import java.util.function.Supplier;

public class VortexPhenomenaRegistry {

    public static final DeferredRegister<VortexPhenomenaType<?>> TYPES = DeferredRegister.create(Helper.createRL("vortex_phenomena"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<VortexPhenomenaType<?>>> REGISTRY = TYPES.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<VortexPhenomenaType<SpaceBattlePheonomena>> SPACE_BATTLE = TYPES.register("space_battle", () ->
            new VortexPhenomenaType<>(SpaceBattlePheonomena::new)
    );
    public static final RegistryObject<VortexPhenomenaType<WormholePhenomena>> WORMHOLE = TYPES.register("wormhole", () ->
                new VortexPhenomenaType<>(WormholePhenomena::new)
            );

    public static final RegistryObject<VortexPhenomenaType<AncientDebrisVortexPhenomena>> ANCIENT_DEBRIS = TYPES.register("ancient_debris", () ->
                new VortexPhenomenaType<>(AncientDebrisVortexPhenomena::new)
            );
    public static final RegistryObject<VortexPhenomenaType<AsteroidVortexPhenomena>> ASTROID = TYPES.register("astroid", () ->
                new VortexPhenomenaType<>(AsteroidVortexPhenomena::new)
            );


}
