package net.tardis.mod.registry;

import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.world_structures.AtriumWorldStructure;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructrureType;
import net.tardis.mod.misc.tardis.world_structures.WaypointTardisStructure;

import java.util.function.Supplier;

public class TardisWorldStructureRegistry {

    public static final DeferredRegister<TardisWorldStructrureType<?>> TYPES = DeferredRegister.create(Helper.createRL("world_upgrades"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<TardisWorldStructrureType<?>>> REGISTRY = TYPES.makeRegistry(RegistryBuilder::new);


    public static final RegistryObject<TardisWorldStructrureType<WaypointTardisStructure>> WAYPOINT = TYPES.register("waypoints", () -> new TardisWorldStructrureType<>(
            WaypointTardisStructure::new
    ));


    public static final RegistryObject<TardisWorldStructrureType<AtriumWorldStructure>> ATRIUM = TYPES.register("atrium", () -> new TardisWorldStructrureType<>(
            AtriumWorldStructure::new
    ));
}
