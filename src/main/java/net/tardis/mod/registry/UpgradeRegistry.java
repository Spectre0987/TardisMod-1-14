package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.sonic.EmptySonicUpgrade;
import net.tardis.mod.upgrade.tardis.EmptyTardisUpgrade;
import net.tardis.mod.upgrade.tardis.EnergySiphonTardisUpgrade;
import net.tardis.mod.upgrade.tardis.LaserMinerTardisUpgrade;
import net.tardis.mod.upgrade.tardis.NanoGeneTardisUpgrade;
import net.tardis.mod.upgrade.types.SonicUpgradeType;
import net.tardis.mod.upgrade.types.TardisUpgradeType;
import net.tardis.mod.upgrade.types.UpgradeType;

import java.util.function.Supplier;

public class UpgradeRegistry {

    public static final DeferredRegister<UpgradeType<?, ? extends Upgrade<?>>> UPGRADES = DeferredRegister.create(Helper.createRL("upgrades"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<UpgradeType<?, ? extends Upgrade<?>>>> REGISTRY = UPGRADES.makeRegistry(RegistryBuilder::new);


    public static RegistryObject<TardisUpgradeType<NanoGeneTardisUpgrade>> TARDIS_NANOGENE = UPGRADES.register("tardis/nanogene", () ->
            new TardisUpgradeType<>(stack -> stack.getItem() == ItemRegistry.UPGRADE_NANO_GENE.get(), NanoGeneTardisUpgrade::new));
    public static RegistryObject<TardisUpgradeType<LaserMinerTardisUpgrade>> TARDIS_LASER_MINER = UPGRADES.register("tardis/laser_miner", () ->
                new TardisUpgradeType<>(stack -> stack.getItem() == ItemRegistry.UPGRADE_LASER_MINER.get(), LaserMinerTardisUpgrade::new)
            );
    public static RegistryObject<TardisUpgradeType<EnergySiphonTardisUpgrade>> TARDIS_ENERGY_SYPHON_UPGRADE = UPGRADES.register("tardis/energy_syphon", () ->
                new TardisUpgradeType<>(stack -> stack.getItem() == ItemRegistry.UPGRADE_ENERGY_SYPHON.get(), EnergySiphonTardisUpgrade::new)
            );
    public static RegistryObject<TardisUpgradeType<EmptyTardisUpgrade>> TARDIS_REMOTE = UPGRADES.register("tardis/remote_circuit", () ->
        new TardisUpgradeType<>(stack -> stack.getItem() == ItemRegistry.UPGRADE_REMOTE.get(), EmptyTardisUpgrade::new)
    );

    public static RegistryObject<TardisUpgradeType<EmptyTardisUpgrade>> ATRIUM = UPGRADES.register("tardis/atrium", () ->
                new TardisUpgradeType<>(
                        stack -> stack.getItem() == ItemRegistry.UPGRADE_ATRIUM.get(),
                        EmptyTardisUpgrade::new
                )
            );


    //Sonic Upgrades
    public static RegistryObject<SonicUpgradeType<EmptySonicUpgrade>> SONIC_DEMAT_TARDIS = UPGRADES.register("sonic/demat", () ->
                new SonicUpgradeType<>(
                        stack -> stack.getItem() == ItemRegistry.UPGRADE_SONIC_DEMAT.get(),
                        EmptySonicUpgrade::new
                )
            );

}
