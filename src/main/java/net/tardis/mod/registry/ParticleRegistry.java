package net.tardis.mod.registry;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;

public class ParticleRegistry {

    public static final DeferredRegister<ParticleType<?>> TYPES = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, Tardis.MODID);

    public static final RegistryObject<SimpleParticleType> RIFT = TYPES.register("rift", () -> new SimpleParticleType(false));
}
