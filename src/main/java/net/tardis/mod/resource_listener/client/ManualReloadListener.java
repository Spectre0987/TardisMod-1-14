package net.tardis.mod.resource_listener.client;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.serialization.JsonOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.client.gui.manual.entries.ItemEntry;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;
import net.tardis.mod.helpers.Helper;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

public class ManualReloadListener extends SimplePreparableReloadListener<Map<ManualChapter, List<ManualEntry>>> {

    public static final String PATH = "tardis/manual/en_us";

    public static final List<ManualChapterLink> entries = new ArrayList<>();

    public static final List<ItemLike> itemsInManual = new ArrayList<>();

    @Override
    protected Map<ManualChapter, List<ManualEntry>> prepare(ResourceManager manager, ProfilerFiller pProfiler) {

        final Map<ManualChapter, List<ManualEntry>> entries = new HashMap<>();

        for(Map.Entry<ResourceLocation, Resource> resource : manager.listResources(PATH, Helper::isRLJson).entrySet()){

            try{
                JsonObject root = JsonParser.parseReader(resource.getValue().openAsReader()).getAsJsonObject();
                ManualEntry.EntryType.fromRL(new ResourceLocation(root.get("type").getAsString())).ifPresent(type -> {
                    ManualEntry entry = type.codec().parse(JsonOps.INSTANCE, root).getOrThrow(false, Tardis.LOGGER::warn);
                    entries.computeIfAbsent(entry.getChapter(), s -> new ArrayList<>()).add(entry);
                });

            }
            catch(IOException exception){
                Tardis.LOGGER.warn("Error parsing TARDIS Manual entry %s".formatted(resource.getKey().toString()));
                Tardis.LOGGER.warn(exception);
            }


        }


        return entries;
    }

    public static List<ManualEntry> getEntriesForChapter(ManualChapter chapter){
        return getLinkFromChapterName(chapter.name(), entries).entries();
    }

    @Override
    protected void apply(Map<ManualChapter, List<ManualEntry>> list, ResourceManager pResourceManager, ProfilerFiller pProfiler) {
        entries.clear();
        itemsInManual.clear();

        //Add root chapters
        entries.addAll(this.addAllChapters(new ArrayList<>(list.keySet()), new ArrayList<>()));
        //Add entries
        for(Map.Entry<ManualChapter, List<ManualEntry>> entry : list.entrySet()){
            Tardis.LOGGER.debug("Attempting to load chapter for " + entry.getKey().name());
            getLinkFromChapterName(entry.getKey().name(), entries).entries().addAll(entry.getValue());
        }

        //Sort
        sortLinkChapters(entries);
        for(ManualChapterLink e : entries){
            sortEntries(e);
            addToPredicate(e);
        }

    }

    public static void addToPredicate(ManualChapterLink link){
        for(ManualEntry entry : link.entries){
            if(entry instanceof ItemEntry e){
                itemsInManual.add(e.stack.getItem());
            }
        }
        for(ManualChapterLink child : link.child()){
            addToPredicate(child);
        }
    }

    public void sortLinkChapters(List<ManualChapterLink> links){
        links.sort((link1, link2) -> {
            sortLinkChapters(link1.child());
            sortLinkChapters(link2.child());
            if(link1 == link2)
                return 0;
            return link1.chapter().weight() > link2.chapter.weight() ? -1 : 1;
        });
    }

    public void sortEntries(ManualChapterLink link){
        link.entries().sort((e, e2) -> {
            if(e.getEntryType() == ManualEntry.EntryType.TITLE){
                if(e2.getEntryType() == ManualEntry.EntryType.TITLE){
                    return 0;
                }
                return -1;
            }
            return 0;
        });
        for(ManualChapterLink child : link.child()){
            sortEntries(child);
        }
    }

    public static ManualChapterLink getLinkFromChapterName(String name, List<ManualChapterLink> search){
        for(ManualChapterLink link : search){
            if(link.chapter.name().equals(name))
                return link;
            ManualChapterLink subResult = getLinkFromChapterName(name, link.child());
            if(subResult != null)
                return subResult;
        }
        return null;
    }

    public List<ManualChapterLink> addAllChapters(List<ManualChapter> chapters, List<ManualChapterLink> links){
        if(chapters.size() == 0)
            return links;
        //Add root entries
        chapters.stream().filter(c -> c.parentChapter().isEmpty()).forEach(c -> {
            links.add(ManualChapterLink.create(c));
            Tardis.LOGGER.debug("Manual: Adding chapter {}", c.name());
        });
        chapters.removeIf(c -> c.parentChapter().isEmpty());

        //Add sub entries

        for(ManualChapter chapter : chapters){
            ManualChapterLink parent = getLinkFromChapterName(chapter.parentChapter().get(), links);
            if(parent != null){
                parent.child().add(ManualChapterLink.create(chapter));
                Tardis.LOGGER.debug("Manual: Adding chapter {}", chapter.name());
            }
        }
        chapters.removeIf(c -> getLinkFromChapterName(c.name(), links) != null);
        return addAllChapters(chapters, links);
    }

    public record ManualChapterLink(ManualChapter chapter, List<ManualEntry> entries, List<ManualChapterLink> child){

        public static ManualChapterLink create(ManualChapter chapter){
            return new ManualChapterLink(chapter, new ArrayList<>(), new ArrayList<>());
        }

    }
}
