package net.tardis.mod.resource_listener.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimpleJsonResourceReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.TraitRegistry;

import java.util.HashMap;
import java.util.Map;

public class TraitListener extends SimpleJsonResourceReloadListener {

    public static final TraitListener INSTANCE = new TraitListener(new GsonBuilder().setPrettyPrinting().create());
    public static final HashMap<ResourceLocation, Trait> traits = new HashMap<>();
    public static final String PATH = "tardis/emotional/traits";

    public TraitListener(Gson pGson) {
        super(pGson, PATH);
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> objects, ResourceManager manager, ProfilerFiller pProfiler) {

        for(Map.Entry<ResourceLocation, JsonElement> ele : objects.entrySet()){
            if(ele.getValue().isJsonObject() && ele.getValue().getAsJsonObject().has("type")){
                final JsonObject obj = ele.getValue().getAsJsonObject();

                Codec<? extends Trait> type = TraitRegistry.REGISTRY.get().getValue(new ResourceLocation(obj.get("type").getAsString()));
                if(type != null){
                    traits.put(ele.getKey(), type.decode(JsonOps.INSTANCE, obj).getOrThrow(false, Tardis.LOGGER::warn).getFirst());
                }
            }
        }
        System.out.println();


    }
}
