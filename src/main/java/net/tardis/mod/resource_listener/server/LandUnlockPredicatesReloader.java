package net.tardis.mod.resource_listener.server;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.tags.TagKey;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.CodecHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ObjectOrTagCodec;

import java.io.IOException;
import java.util.*;

public class LandUnlockPredicatesReloader extends SimplePreparableReloadListener<HashMap<ResourceLocation, LandUnlockPredicatesReloader.LandUnlockPredicate>> {


    public static final LandUnlockPredicatesReloader INSTANCE = new LandUnlockPredicatesReloader();
    public static final String PATH = Tardis.MODID + "/land_unlockers";
    public final Map<ResourceLocation, LandUnlockPredicate> predicateMaps = new HashMap<>();

    @Override
    protected HashMap<ResourceLocation, LandUnlockPredicate> prepare(ResourceManager manager, ProfilerFiller pProfiler) {

        final HashMap<ResourceLocation, LandUnlockPredicate> map = new HashMap<>();

        try{

            for(Map.Entry<ResourceLocation, Resource> r : manager.listResources(PATH, Helper::isRLJson).entrySet()){

                final JsonElement ele = JsonParser.parseReader(r.getValue().openAsReader());
                LandUnlockPredicate.CODEC.decode(JsonOps.INSTANCE, ele).result().ifPresent(p -> {
                    map.put(r.getKey(), p.getFirst());
                });

            }

        }
        catch(IOException e){
            Tardis.LOGGER.warn(e);
        }

        return map;
    }

    @Override
    protected void apply(HashMap<ResourceLocation, LandUnlockPredicate> pObject, ResourceManager pResourceManager, ProfilerFiller pProfiler) {
        this.predicateMaps.clear();
        this.predicateMaps.putAll(pObject);
        Tardis.LOGGER.debug("Loaded {} TARDIS Landing Unlockers!", this.predicateMaps.size());
    }

    public record LandUnlockPredicate(ResourceKey<?> unlockedItem, Optional<List<TagKey<Biome>>> biome, Optional<List<ResourceLocation>> structure){

        public static final Codec<LandUnlockPredicate> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                        CodecHelper.GENERIC_RESOURCE_KEY_CODEC.fieldOf("unlock").forGetter(LandUnlockPredicate::unlockedItem),
                        TagKey.codec(Registries.BIOME).listOf().optionalFieldOf("biomes").forGetter(LandUnlockPredicate::biome),
                        ResourceLocation.CODEC.listOf().optionalFieldOf("structures").forGetter(LandUnlockPredicate::structure)
                ).apply(instance, LandUnlockPredicate::new)
        );

        public boolean matchesAnyStructure(Collection<Structure> struct, RegistryAccess access){
            //If no structure constraints, all structures technically match
            if(!structure().isPresent())
                return true;

            final Registry<Structure> reg = access.registryOrThrow(Registries.STRUCTURE);
            final List<ResourceLocation> structuresInKeys = struct.stream().map(reg::getKey).toList();

            return structuresInKeys.stream().anyMatch(this.structure().get()::contains);
        }

    }

}
