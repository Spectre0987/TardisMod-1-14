package net.tardis.mod.resource_listener.server;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TardisNames extends SimplePreparableReloadListener<List<List<String>>> {

    public static List<List<String>> NAMES = new ArrayList<>();
    public static final ResourceLocation NAMES_PATH = Helper.createRL("tardis_names.json");

    //For Client caching
    public static Map<ResourceKey<Level>, String> CLIENT_NAMES = new HashMap<>();

    @Override
    protected List<List<String>> prepare(ResourceManager pResourceManager, ProfilerFiller pProfiler) {

        final List<List<String>> names = new ArrayList<>();

        pResourceManager.getResource(NAMES_PATH).ifPresent(nameResource -> {
            try {
                JsonArray root = JsonParser.parseReader(new InputStreamReader(nameResource.open())).getAsJsonArray();
                for(JsonElement ele : root){
                    JsonArray nameArray = ele.getAsJsonArray();
                    List<String> nameList = new ArrayList<>();

                    for(JsonElement e : nameArray){
                        nameList.add(e.getAsString());
                    }

                    names.add(nameList);
                }
            }
            catch(Exception e){
                Tardis.LOGGER.error("Error loading TARDIS Names!");
                Tardis.LOGGER.error(e);
            }
        });

        return names;
    }

    @Override
    protected void apply(List<List<String>> pObject, ResourceManager pResourceManager, ProfilerFiller pProfiler) {
        NAMES = pObject;
    }

    public static String generateTardisName(MinecraftServer server){

        List<String> nameL = new ArrayList<>();

        //Generate name from parts
        for(List<String> names : NAMES){
            nameL.add(names.get(server.overworld().random.nextInt(names.size() - 1)));
        }

        return String.join(" ", nameL);
    }
}
