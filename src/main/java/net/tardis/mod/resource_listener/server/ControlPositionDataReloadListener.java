package net.tardis.mod.resource_listener.server;

import com.google.gson.JsonParser;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.tardis.mod.Tardis;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.registry.ControlRegistry;
import org.apache.logging.log4j.Level;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ControlPositionDataReloadListener extends SimplePreparableReloadListener<HashMap<ResourceLocation, Map<ControlType<?>, ControlPositionData>>>{
    
	protected static final String folderName = Tardis.MODID + "/controls";
	public static final ControlPositionDataReloadListener INSTANCE = new ControlPositionDataReloadListener();
	
	@Override
	protected HashMap<ResourceLocation, Map<ControlType<?>, ControlPositionData>> prepare(ResourceManager pResourceManager, ProfilerFiller pProfiler) {
		return ControlPositionDataReloadListener.load(pResourceManager);
	}

	@Override
	protected void apply(HashMap<ResourceLocation, Map<ControlType<?>, ControlPositionData>> map, ResourceManager pResourceManager, ProfilerFiller pProfiler) {
	    ControlPositionDataRegistry.POSITION_DATAS.clear();
        ControlPositionDataRegistry.POSITION_DATAS.putAll(map);
	}
	
	public static HashMap<ResourceLocation, Map<ControlType<?>, ControlPositionData>> load(ResourceManager manager){
        HashMap<ResourceLocation, Map<ControlType<?>, ControlPositionData>> list = new HashMap<>();


        Map<ResourceLocation, Resource> map = manager.listResources(folderName, file -> file.getPath().endsWith(".json"));

        for(Map.Entry<ResourceLocation, Resource> entry : map.entrySet()) {
            Tardis.LOGGER.log(Level.DEBUG, "Loading console control layout file %s".formatted(entry.getKey()));

            try {
                final ConsoleControlPosition data = ConsoleControlPosition.CODEC.decode(JsonOps.INSTANCE, JsonParser.parseReader(entry.getValue().openAsReader()))
                        .getOrThrow(false, Tardis.LOGGER::warn).getFirst();

                list.computeIfAbsent(data.console(), id -> new HashMap<>())
                        .putAll(data.data());
            } catch (IOException e) {
                Tardis.LOGGER.warn(e);
            }
        }
        return list;
    }

    public record ConsoleControlPosition(ResourceLocation console, Map<ControlType<?>, ControlPositionData> data){

        public static final Codec<ConsoleControlPosition> CODEC = RecordCodecBuilder.create(i ->
                i.group(
                        ResourceLocation.CODEC.fieldOf("console").forGetter(ConsoleControlPosition::console),
                        Codec.unboundedMap(ControlRegistry.REGISTRY.get().getCodec(), ControlPositionData.CODEC).fieldOf("controls").forGetter(ConsoleControlPosition::data)

                ).apply(i, ConsoleControlPosition::new)
        );

    }

}
