package net.tardis.mod.resource_listener.server;

import com.google.gson.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.tardis.mod.Tardis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItemArtronValueReloader extends SimplePreparableReloadListener<List<ItemArtronValueReloader.ArtronValue>> {

    public static final ItemArtronValueReloader INSTANCE = new ItemArtronValueReloader();
    public static final String FOLDER_PATH = Tardis.MODID + "/artron_fuel";

    final List<ArtronValue> values = new ArrayList<>();

    @Override
    protected List<ArtronValue> prepare(ResourceManager manager, ProfilerFiller pProfiler) {
        List<ArtronValue> values = new ArrayList<>();
        for(Map.Entry<ResourceLocation, Resource> entry : manager.listResources(FOLDER_PATH, name -> name.getPath().endsWith(".json")).entrySet()){

            try{
                try{
                    JsonObject root = JsonParser.parseReader(entry.getValue().openAsReader()).getAsJsonObject();

                    JsonArray itemArray = root.get("values").getAsJsonArray();

                    for(JsonElement ele : itemArray){
                        final ArtronValue val = ArtronValue.deserialize(ele.getAsJsonObject());
                        if(val != null){
                            values.add(val);
                        }
                    }


                }
                catch(IOException exception){

                }
            }
            catch(JsonParseException exception){
                Tardis.LOGGER.warn("Unable to parse Artron Fuel Data from " + entry.getKey().toString());
                Tardis.LOGGER.warn(exception);
            }

        }
        return values;
    }

    @Override
    protected void apply(List<ArtronValue> list, ResourceManager pResourceManager, ProfilerFiller pProfiler) {
        this.values.clear();
        this.values.addAll(list);
    }

    public record ArtronValue(Ingredient ingredient, float value){


        public boolean isValid(ItemStack stack){
            return this.ingredient().test(stack);
        }

        /**
         *
         * @param stack
         * @return 0 if not an item that can be consumed for Artron
         */
        public static float getArtronValue(ItemStack stack){
            for(ArtronValue val : INSTANCE.values){
                if(val.isValid(stack)){
                    return val.value();
                }
            }
            return 0;
        }

        public static ArtronValue deserialize(JsonObject root) throws JsonSyntaxException {
            return new ArtronValue(Ingredient.fromJson(root.get("ingredient")), root.get("artron").getAsFloat());
        }

        public JsonElement serialize(){
            JsonObject root = new JsonObject();
            root.add("ingredient", this.ingredient().toJson());
            root.add("artron", new JsonPrimitive(this.value()));
            return root;
        }

    };

}
