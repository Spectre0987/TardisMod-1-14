package net.tardis.mod.helpers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.TickTask;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.registries.IForgeRegistry;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.Trait;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Helper {

    public static final ResourceLocation createRL(String name){
        return new ResourceLocation(Tardis.MODID, name);
    }

    public static Vec3 blockPosToVec3(BlockPos pos, boolean centered) {
        double offset = centered ? 0.5 : 0;
        return new Vec3(pos.getX() + offset, pos.getY() + offset, pos.getZ() + offset);
    }

    public static <T> Optional<T> readRegistryFromString(CompoundTag tag, Registry<T> registry, String name) {
        if(tag.contains(name)){
            ResourceLocation rl = new ResourceLocation(tag.getString(name));
            return Optional.of(registry.get(rl));
        }
        return Optional.empty();
    }

    public static <V> void writeRegistryToNBT(CompoundTag tag, Registry<V> registry, V value, String name){
        ResourceLocation loc = registry.getKey(value);
        if(loc != null){
            tag.putString(name, loc.toString());
        }

    }

    public static <T> Optional<T> readRegistryFromString(CompoundTag tag, IForgeRegistry<T> registry, String name) {
        if(tag != null && tag.contains(name)){
            ResourceLocation rl = new ResourceLocation(tag.getString(name));
            return registry.containsKey(rl) ? Optional.of(registry.getValue(rl)) : Optional.empty();
        }
        return Optional.empty();
    }

    public static <V> void writeRegistryToNBT(CompoundTag tag, IForgeRegistry<V> registry, V value, String name){
        ResourceLocation loc = registry.getKey(value);
        if(loc != null){
            tag.putString(name, loc.toString());
        }

    }

    public static <T> Optional<T> readOptionalNBT(CompoundTag tag, String key, BiFunction<CompoundTag, String, T> reader) {
        if(tag.contains(key)){
            return Optional.of(reader.apply(tag, key));
        }
        return Optional.empty();
    }

    public static <T> void encodeOptional(FriendlyByteBuf buf, Optional<T> optional, BiConsumer<T, FriendlyByteBuf> encoder) {
        buf.writeByte(optional.isPresent() ? 1 : 0);
        optional.ifPresent(val -> encoder.accept(val, buf));
    }

    public static <T> Optional<T> decodeOptional(FriendlyByteBuf buf, Function<FriendlyByteBuf, T> decoder){
        boolean exists = buf.readByte() == 1;
        if(exists){
            return Optional.of(decoder.apply(buf));
        }
        return Optional.empty();
    }

    public static int randomIntWithNegative(RandomSource random, int radius) {
        return radius - random.nextInt(radius * 2);
    }

    public static <T> void encodeMultiple(FriendlyByteBuf buf, Collection<T> list, BiConsumer<T, FriendlyByteBuf> encoder) {

        buf.writeInt(list.size());
        for(T t : list){
            encoder.accept(t, buf);
        }

    }

    public static <T> Collection<T> decodeMultiple(FriendlyByteBuf buf, Function<FriendlyByteBuf, T> decoder){
        final Collection<T> col = new ArrayList<T>();
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            col.add(decoder.apply(buf));
        }
        return col;
    }

    public static BlockPos min(BlockPos first, BlockPos second) {
        return new BlockPos(
                Math.min(first.getX(), second.getX()),
                Math.min(first.getY(), second.getY()),
                Math.min(first.getZ(), second.getZ())
        );
    }

    public static BlockPos max(BlockPos first, BlockPos second) {
        return new BlockPos(
                Math.max(first.getX(), second.getX()),
                Math.max(first.getY(), second.getY()),
                Math.max(first.getZ(), second.getZ())
        );
    }

    public static BlockPos scalePos(BlockPos pos, double scale) {
        return new BlockPos(
                Mth.floor(pos.getX() * scale),
                Mth.floor(pos.getY() * scale),
                Mth.floor(pos.getZ() * scale)
        );
    }

    public static VoxelShape blockShapePixels(int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
        return Shapes.box(minX / 16.0F, minY / 16.0F, minZ / 16.0F,
                maxX / 16.0F, maxY / 16.0F, maxZ / 16.0F);
    }

    /**
     * See {@link #readResourceKey(CompoundTag, String)}
     * @param tag
     * @param key
     * @param value
     */
    public static void writeResourceKey(@Nullable CompoundTag tag, String key, @Nullable ResourceKey<?> value) {
        if(value == null || tag == null){
            if(tag != null && tag.contains(key)){
                tag.remove(key);
            }
            return;
        }
        tag.putString(key, value.registry().toString() + "|" + value.location().toString());
    }

    /**
     * Standard way to read a generic {@link ResourceKey} from nbt
     * @param tag
     * @param key
     * @return
     */
    public static Optional<ResourceKey<?>> readResourceKey(@Nullable CompoundTag tag, String key){
        if(tag == null || !tag.contains(key)){
            return Optional.empty();
        }
        String[] parts = tag.getString(key).split("|");
        if(parts.length < 2){
            return Optional.empty();
        }
        return Optional.of(ResourceKey.create(
                ResourceKey.createRegistryKey(new ResourceLocation(parts[0])),
                new ResourceLocation(parts[1])
        ));
    }

    public static BlockPos vec3ToBlockPos(Vec3 pos) {
        return new BlockPos(Mth.floor(pos.x), Mth.floor(pos.y), Mth.floor(pos.z));
    }

    /**
     * @param value
     * @return an {@link Optional} of the value, if the value is null, return {@link Optional#empty()}
     */
    public static <T> Optional<T> nullableToOptional(@Nullable T value) {
        if(value == null)
            return Optional.empty();
        return Optional.of(value);
    }

    public static boolean isRLJson(ResourceLocation rl) {
        return rl.getPath().endsWith(".json");
    }

    public static <T> void writeListNBT(CompoundTag tag, String name, Collection<T> values, BiConsumer<T, ListTag> writer) {
        final ListTag list = new ListTag();
        for(T item : values){
            writer.accept(item, list);
        }
        tag.put(name, list);
    }

    public static <T, N extends Tag> List<T> readListNBT(@Nullable CompoundTag tag, @Nullable String name, int tagType, Function<N, T> reader){
        final List<T> list = new ArrayList<>();
        if(tag == null || name == null || !tag.contains(name)){
            return list;
        }

        final ListTag listTag = tag.getList(name, tagType);

        for(int i = 0; i < listTag.size(); ++i){
            list.add(reader.apply((N)listTag.get(i)));
        }

        return list;
    }

    public static void sendHotbarMessage(Player pPlayer, Component translation) {
        if(pPlayer instanceof ServerPlayer player){
            player.displayClientMessage(translation, true);
        }
    }

    public static void doServerTask(MinecraftServer server, Runnable run) {
        server.doRunTask(new TickTask(server.getTickCount(), run));
    }

    public static Rotation getRotationFromDirection(Direction dir) {
        switch(dir){
            default: return Rotation.NONE;
            case EAST: return Rotation.CLOCKWISE_90;
            case SOUTH: return Rotation.CLOCKWISE_180;
            case WEST: return Rotation.COUNTERCLOCKWISE_90;
        }
    }

    public static String rlToFileName(String path) {
        final int indexOfSlash = path.lastIndexOf('/');
        final int indexOfDot = path.lastIndexOf('.');

        if(indexOfSlash != -1){
            path = path.substring(indexOfSlash);
        }

        if(indexOfDot != -1){
            path = path.substring(0, indexOfDot);
        }

        return path;
    }

    public static <K, V> Optional<K> getKeyFromValueHashMap(Map<K, V> map, V val) {
        for(Map.Entry<K, V> entry : map.entrySet()){
            if(entry.getValue() == val)
                return Optional.of(entry.getKey());
        }
        return Optional.empty();
    }
}
