package net.tardis.mod.helpers;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

import java.util.Optional;
import java.util.function.Predicate;

public class InventoryHelper {

    public static void addItem(Entity player, ItemStack stack){
        if(!player.level.isClientSide()){
            final ItemEntity item = new ItemEntity(player.level, player.getX(), player.getY(), player.getZ(), stack);
            item.setNoPickUpDelay();
            player.level.addFreshEntity(item);
        }
    }

    public static boolean isHolding(LivingEntity player, Predicate<ItemStack> test){
        return test.test(player.getMainHandItem()) || test.test(player.getOffhandItem());
    }

    public static Optional<InteractionHand> getInEitherHand(Player player, Predicate<ItemStack> stackTest) {
        if(stackTest.test(player.getMainHandItem())){
            return Optional.of(InteractionHand.MAIN_HAND);
        }
        if(stackTest.test(player.getOffhandItem())){
            return Optional.of(InteractionHand.OFF_HAND);
        }
        return Optional.empty();
    }
}
