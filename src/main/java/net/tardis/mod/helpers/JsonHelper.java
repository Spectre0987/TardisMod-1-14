package net.tardis.mod.helpers;

import com.google.gson.*;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistry;
import net.minecraftforge.registries.IForgeRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class JsonHelper {

    public static Item getItemFromString(String key){
        ResourceLocation loc = ResourceLocation.tryParse(key);
        Item item = ForgeRegistries.ITEMS.getValue(loc);
        if(item == null)
            throw new JsonSyntaxException("No item registered for %s!".formatted(key));
        return item;
    }

    public static <T> JsonPrimitive saveRegistry(IForgeRegistry<T> registry, T item){
        ResourceLocation rl = registry.getKey(item);
        return new JsonPrimitive(rl.toString());
    }

    public static Optional<ResourceLocation> readRegistry(JsonElement element){
        if(element != null){
            return Optional.of(new ResourceLocation(element.getAsString()));
        }
        return Optional.empty();
    }

    public static <T> Optional<T> readFromRegSafe(IForgeRegistry<T> reg, JsonElement ele){
        Optional<ResourceLocation> rl = readRegistry(ele);
        if(rl.isPresent()){
            if(reg.containsKey(rl.get())){
                return Optional.of(reg.getValue(rl.get()));
            }
        }
        return Optional.empty();
    }

    public static JsonObject writeItemStack(ItemStack stack){
        JsonObject item = new JsonObject();
        item.add("item", new JsonPrimitive(ForgeRegistries.ITEMS.getKey(stack.getItem()).toString()));

        if(stack.getCount() > 1){
            item.add("count", new JsonPrimitive(stack.getCount()));
        }

        return item;
    }

    public static ItemStack readItemStack(JsonObject itemObj) throws JsonParseException{
        if(!itemObj.has("item"))
            throw new JsonParseException("No \"item\" tag present!");

        final ResourceLocation itemId = new ResourceLocation(itemObj.get("item").getAsString());
        Item item = ForgeRegistries.ITEMS.getValue(itemId);

        if(item == null)
            throw new JsonParseException("No item registered with id %s !".formatted(itemId.toString()));

        int count = itemObj.has("count") ? itemObj.get("count").getAsInt() : 1;
        return new ItemStack(item, count);
    }

    public static FluidStack readFluidStack(JsonObject obj){
        final Fluid fluid = ForgeRegistries.FLUIDS.getValue(new ResourceLocation(obj.get("fluid").getAsString()));
        final int amount = obj.get("amount").getAsInt();
        return new FluidStack(fluid, amount);
    }

    public static JsonObject writeFluidStack(FluidStack stack){
        JsonObject obj = new JsonObject();
        obj.add("fluid", new JsonPrimitive(ForgeRegistries.FLUIDS.getKey(stack.getFluid()).toString()));
        obj.add("amount", new JsonPrimitive(stack.getAmount()));
        return obj;
    }

    public static @Nullable JsonElement getOrNull(JsonObject root, String name){
        if(!root.has(name)){
            return null;
        }
        return root.get(name);
    }

    public static JsonObject writeResourceKey(ResourceKey<?> key){
        final JsonObject obj = new JsonObject();
        obj.add("type", new JsonPrimitive(key.registry().toString()));
        obj.add("location", new JsonPrimitive(key.location().toString()));
        return obj;
    }

    public static <T> ResourceKey<T> readResourceKey(JsonObject obj){
        return ResourceKey.create(
                ResourceKey.createRegistryKey(new ResourceLocation(obj.get("type").getAsString())),
                new ResourceLocation(obj.get("location").getAsString())
        );
    }

}
