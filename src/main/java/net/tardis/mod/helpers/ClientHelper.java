package net.tardis.mod.helpers;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.world.entity.AnimationState;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

import java.util.Objects;

public class ClientHelper {

    public static AnimationState getControlState(ITardisLevel level, ControlType<?> type){
        Objects.requireNonNull(type);
        return level.getControlDataOrCreate(type).getUseAnimationState();
    }

    public static void translatePixel(PoseStack pose, double x, double y, double z){
        pose.translate(x / 16.0, y / 16.0, z / 16.0);
    }

    public static void renderPolyTube(PoseStack pose, int sides, int radius){}

}
