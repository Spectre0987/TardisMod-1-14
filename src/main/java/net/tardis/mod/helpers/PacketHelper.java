package net.tardis.mod.helpers;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class PacketHelper {


    public static <T> void encodeArray(FriendlyByteBuf buf, BiConsumer<T, FriendlyByteBuf> encoder, T... values){
        buf.writeInt(values.length);
        for(T val : values){
            encoder.accept(val, buf);
        }
    }

    public static <T> List<T> decodeArrayOrList(FriendlyByteBuf buf, Function<FriendlyByteBuf, T> decoder){
        final int size = buf.readInt();
        List<T> list = new ArrayList<>();
        for(int i = 0; i < size; ++i){
            list.add(decoder.apply(buf));
        }

        return list;
    }

    public static ResourceKey<?> decodeResourceKey(FriendlyByteBuf buf){
        final ResourceLocation reg = buf.readResourceLocation();
        final ResourceLocation loc = buf.readResourceLocation();
        return ResourceKey.create(ResourceKey.createRegistryKey(reg), loc);
    }

    public static void encodeResourceKey(ResourceKey<?> key, FriendlyByteBuf buf){
        buf.writeResourceLocation(key.registry());
        buf.writeResourceLocation(key.location());
    }

}
