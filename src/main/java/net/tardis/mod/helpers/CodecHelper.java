package net.tardis.mod.helpers;

import com.google.common.collect.Lists;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.registries.ForgeRegistries;
import org.joml.Vector3f;

import java.util.List;

public class CodecHelper {

    public static final Codec<Vector3f> VEC3F_CODEC = Codec.FLOAT.listOf().comapFlatMap(CodecHelper::readVec3f, v -> Lists.newArrayList(v.x, v.y, v.z));
    public static final Codec<ResourceKey<?>> GENERIC_RESOURCE_KEY_CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                        ResourceLocation.CODEC.fieldOf("registry").forGetter(k -> k.registry()),
                        ResourceLocation.CODEC.fieldOf("location").forGetter(k -> k.location())
                ).apply(instance, (reg, loc) -> ResourceKey.create(ResourceKey.createRegistryKey(reg), loc))
            );


    public static DataResult<Vector3f> readVec3f(List<Float> floats){

        if(floats.size() != 3)
            return DataResult.error(() -> "ERROR: vec3f must contain exactly 3 arguments");

        return DataResult.success(new Vector3f(floats.get(0), floats.get(1), floats.get(2)));

    }

    public static <T extends Enum> Codec<T> createEnumCodec(Class<T> e){
        return Codec.STRING.comapFlatMap(name -> {
            for(T val : e.getEnumConstants()){
                if(val.name().toLowerCase().equals(name.toLowerCase())){
                    return DataResult.success(val);
                }
            }
            return DataResult.error(() -> "Parsing Error: " + name + " not found for enum " + e.getName());
        }, val -> val.name().toLowerCase()).stable();
    }

}
