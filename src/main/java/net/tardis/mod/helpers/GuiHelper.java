package net.tardis.mod.helpers;

import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.Slot;
import net.tardis.mod.client.renderers.tiles.BrokenExteriorRenderer;
import net.tardis.mod.exterior.ExteriorType;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class GuiHelper {

    public static List<Slot> getPlayerSlots(Inventory inv, int startX, int startY) {
        List<Slot> slots = new ArrayList<>();
        //Add player inv
        for(int i = 0; i < inv.items.size() - 9; ++i){
            slots.add(new Slot(inv, i + 9,
                    startX + (i % 9) * 18,
                    startY + (i / 9) * 18));
        }
        //hotbar
        for(int i = 0; i < 9; ++i){
            slots.add(new Slot(inv, i, startX + (i * 18), startY + 58));
        }
        return slots;
    }

    public static void renderExteriorToGui(PoseStack stack, ExteriorType exteriorType, int x, int y){
        BrokenExteriorRenderer.getRenderFor(exteriorType).ifPresent(model -> {

            stack.pushPose();

            stack.translate(0, 0, 1000);
            RenderSystem.applyModelViewMatrix();
            stack.pushPose();
            stack.translate(x, y, -950);
            stack.mulPoseMatrix((new Matrix4f()).scaling((float)5, (float)5, (float)(-5)));
            stack.mulPose(Axis.XP.rotationDegrees(22.5F).add(Axis.YN.rotationDegrees(45F)));

            MultiBufferSource.BufferSource source = Minecraft.getInstance().renderBuffers().bufferSource();

            Lighting.setupForEntityInInventory();

            model.applyTranslations(stack);
            model.getModel().ifPresent(m -> {
                RenderType type = model.getRenderType();
                RenderSystem.runAsFancy(() -> {
                    m.renderToBuffer(stack, source.getBuffer(type), 15728880, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
                });
                source.endBatch();
            });
            Lighting.setupFor3DItems();
            stack.popPose();
            stack.popPose();
        });
    }

    public static void drawCenteredText(PoseStack pose, Font font, Component text, int x, int y, int color) {
        int start = x - font.width(text) / 2;
        font.draw(pose, text, start, y, color);
    }

    public static Component formatBlockPos(BlockPos value) {
        return Component.literal("%d, %d, %d".formatted(value.getX(), value.getY(), value.getZ()));
    }
}
