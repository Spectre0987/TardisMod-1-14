package net.tardis.mod.events;

import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ChunkHolder;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.*;
import net.minecraftforge.event.entity.EntityJoinLevelEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.ShieldBlockEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.event.level.ChunkEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.emotional.traits.LikeUseItemTrait;
import net.tardis.mod.misc.ChunkLoader;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.cap.chunks.ChunkCapability;
import net.tardis.mod.cap.chunks.IChunkCap;
import net.tardis.mod.cap.chunks.TardisChunkCap;
import net.tardis.mod.cap.entities.TardisPlayer;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.cfl.CFLTrackTardis;
import net.tardis.mod.cap.level.GeneralWorldCap;
import net.tardis.mod.cap.level.IGeneralLevel;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.level.TardisCap;
import net.tardis.mod.commands.CommandRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.misc.DelayedServerTask;
import net.tardis.mod.misc.TeleportEntry;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.*;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.LoyaltyFunctionRegistry;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.resource_listener.server.*;
import net.tardis.mod.tags.BlockTags;
import net.tardis.mod.world.data.TardisLevelData;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class CommonEvents {

    @SubscribeEvent
    public static void attachWorldCapabilies(AttachCapabilitiesEvent<Level> event){

        //For unregistered worlds, namely Immersive Engineering's Manual's Fake world
        if(event.getObject().registryAccess().registryOrThrow(Registries.DIMENSION_TYPE).getKey(event.getObject().dimensionType()) == null)
            return;

        //If this is a TARDIS
        if(event.getObject().dimensionTypeId().location().equals(DimensionTypes.TARDIS_TYPE.location()))
            event.addCapability(Constants.CapabilityKeys.TARDIS_LEVEL, new GenericProvider<>(Capabilities.TARDIS, new TardisCap(event.getObject())));
        //If not, add the general capability
        else event.addCapability(Constants.CapabilityKeys.GENERAL_WORLD, new GenericProvider<>(Capabilities.GENERAL_LEVEL, new GeneralWorldCap(event.getObject())));
    }

    @SubscribeEvent
    public static void attachEntityCaps(AttachCapabilitiesEvent<Entity> event){
        if(event.getObject() instanceof Player player){
            event.addCapability(Constants.CapabilityKeys.PLAYER, new GenericProvider<>(
                    Capabilities.PLAYER, new TardisPlayer(player)
            ));
        }
    }

    @SubscribeEvent
    public static void attachChunkCaps(AttachCapabilitiesEvent<LevelChunk> event){
        //For unregistered worlds, namely Immersive Engineering's Manual's Fake world
        if(event.getObject().getLevel().registryAccess().registryOrThrow(Registries.DIMENSION_TYPE).getKey(event.getObject().getLevel().dimensionType()) == null)
            return;

        if(!DimensionTypes.TARDIS_TYPE.equals(event.getObject().getLevel().dimensionTypeId())){
            event.addCapability(Constants.CapabilityKeys.CHUNK_CAP, new GenericProvider<>(
                    Capabilities.CHUNK, new ChunkCapability(event.getObject())
            ));
        }
        else event.addCapability(Constants.CapabilityKeys.TARDIS_CHUNK, new GenericProvider<>(Capabilities.TARDIS_CHUNK, new TardisChunkCap(event.getObject())));
    }

    @SubscribeEvent
    public static void playerLoginEvent(PlayerEvent.PlayerLoggedInEvent event){
        Iterable<ServerLevel> levels = event.getEntity().getServer().getAllLevels();

        //When the player joins the world, tell them all the names of the TARDISes
        for(ServerLevel level : levels){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                Network.sendTo((ServerPlayer) event.getEntity(), new SetTardisNameMessage(level.dimension(), tardis.getTardisName()));
            });
        }

    }

    @SubscribeEvent
    public static void onWorldTick(TickEvent.LevelTickEvent event){
        if(event.phase == TickEvent.Phase.END){
            event.level.getCapability(Capabilities.TARDIS).ifPresent(ITardisLevel::tick);
            event.level.getCapability(Capabilities.GENERAL_LEVEL).ifPresent(IGeneralLevel::tick);

            //Handle server world only stuffs
            if(event.level instanceof ServerLevel level){

                //Tick vortex phenomena
                if(level.dimension().equals(Level.OVERWORLD)){
                    TardisLevelData.get(level).tickActivePhenomena();
                }

                //Handle teleporting
                List<UUID> teleportsToRemove = new ArrayList<>();
                for(TeleportEntry entry: TeleportEntry.getEntries()){
                    if(entry.shouldRun(level)){ //If this is the level the entity is leaving from
                        if(entry.tick(level))
                            teleportsToRemove.add(entry.entityID); //Schedule removal of all completed teleports
                    }
                }
                teleportsToRemove.stream().forEach(TeleportEntry::remove);

                //tick rifts
                for(ChunkHolder c : level.getChunkSource().chunkMap.getChunks()){
                    c.getFullChunkFuture().thenAccept(e -> {
                        e.left().ifPresent(fullChunk -> {
                            if(fullChunk != null){
                                fullChunk.getCapability(Capabilities.CHUNK).ifPresent(IChunkCap::tick);
                            }
                        });
                    });
                }
            }

        }
    }

    @SubscribeEvent
    public static void onLivingTick(LivingEvent.LivingTickEvent event){
        event.getEntity().getCapability(Capabilities.PLAYER).ifPresent(cap -> cap.tick());
    }

    @SubscribeEvent
    public static void onEnterWorld(EntityJoinLevelEvent event){
        //Send TARDIS data to the client when the player enters one
        event.getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            if(event.getEntity() instanceof ServerPlayer player){
                Network.sendTo(player, new TardisLoadMessage(tardis.serializeNBT()));
                Network.sendTo(player, new UpdateDoorStateMessage(tardis.getId(), tardis.getInteriorManager().getDoorHandler()));
                for(ControlType<?> type : ControlRegistry.REGISTRY.get()){
                    Network.sendTo(player, new ControlDataMessage(tardis.getId(), tardis.getControlDataOrCreate(type)));
                }
            }

        });
    }

    @SubscribeEvent
    public static void onChangeDim(PlayerEvent.PlayerChangedDimensionEvent event){
        Capabilities.getCap(Capabilities.TARDIS, event.getEntity().level).ifPresent(tardis -> {
            MinecraftForge.EVENT_BUS.post(new TardisEvent.EnterEvent.Post(tardis, event.getEntity()));
        });
    }
    
    @SubscribeEvent
    public static void onServerResourceReload(AddReloadListenerEvent event){
        event.addListener(ControlPositionDataReloadListener.INSTANCE);
        event.addListener(TraitCompatsListener.INSTANCE);
        event.addListener(new TardisNames());
        event.addListener(ItemArtronValueReloader.INSTANCE);
        event.addListener(LandUnlockPredicatesReloader.INSTANCE);
        event.addListener(TraitListener.INSTANCE);
    }

    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent event){
        final BlockState block = event.getState();
        if(block.is(BlockTags.UNBREAKABLES)){
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onPlayerRightClick(PlayerInteractEvent.RightClickBlock event){
        if(!event.getLevel().isClientSide){
            //Handle creating the TRDS Table
            BlockState state = event.getLevel().getBlockState(event.getPos());
            ItemStack clickedWith = event.getItemStack();
            if(state.getBlock() == Blocks.SMITHING_TABLE){
                if(clickedWith.is(BlockRegistry.XION.get().asItem())){
                    event.getLevel().setBlock(event.getPos(), BlockRegistry.DRAFTING_TABLE.get().defaultBlockState(), 3);
                    clickedWith.shrink(1);
                    event.setUseBlock(Event.Result.DENY);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onTardisLanded(TardisEvent.TardisInitLandEvent event){
        for(Player player : event.getTARDIS().getLevel().getServer().getPlayerList().getPlayers()){
            for(ItemStack stack : player.getInventory().items){
                stack.getCapability(Capabilities.CFL).ifPresent(tool -> {
                    tool.getBoundTARDISKey().ifPresent(key -> {
                        if(key.equals(event.getTARDIS().getLevel().dimension())){

                            tool.getCurrentFunction().ifPresent(func -> {
                                if(func instanceof CFLTrackTardis trackFunc){
                                    trackFunc.setTrackingPos(event.getTARDIS().getLocation());
                                }
                            });

                        }
                    });
                });
            }
        }
    }

    @SubscribeEvent
    public static void onCommand(CommandEvent event){

    }

    @SubscribeEvent
    public static void onChunkGenerated(ChunkEvent.Load event){
        //Request update from the server
        if(event.getLevel().isClientSide()){
            Network.sendToServer(new LoadChunkCapabilityMessage(event.getChunk().getPos(), new CompoundTag()));
        }

        //If we can't get capabilities for this, stop
        if(!(event.getChunk() instanceof LevelChunk) || event.getChunk().getStatus() != ChunkStatus.FULL)
            return;

        final LevelChunk chunk = (LevelChunk) event.getChunk();

        //Generate outside cap data
        if(event.isNewChunk()){
            chunk.getCapability(Capabilities.CHUNK).ifPresent(cap -> {
                cap.onGenerated();
            });
        }
    }

    @SubscribeEvent
    public static void onTardisNotifEvent(TardisEvent.TardisNotificationEvent event){
        if(!event.getTARDIS().isClient()){
            for(ServerPlayer player : event.getTARDIS().getLevel().getServer().getPlayerList().getPlayers()){
                for(ItemStack item : player.inventoryMenu.getItems()){
                    LazyOptional<ICFLTool> toolHolder = item.getCapability(Capabilities.CFL);
                    if(toolHolder.isPresent()){
                        final ICFLTool tool = toolHolder.orElseThrow(NullPointerException::new);
                        if(tool.getBoundTARDISKey().isPresent() && tool.getBoundTARDISKey().get().equals(event.getTARDIS().getLevel().dimension())){
                            player.displayClientMessage(event.notif.notif().apply(event.getTARDIS()), true);
                            break;
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void onEntityHurt(LivingHurtEvent event){
        //If the hurt entity is inside a TARDIS
        event.getEntity().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            tardis.getSubsystem(SubsystemRegistry.TEMPORAL_GRACE.get()).ifPresent(grace -> {
                if(!grace.isBrokenOrOff()){
                    if(grace.isBlockedCompletely(event.getSource().type()))
                        event.setCanceled(true); //Stop the entity from taking damage
                    //Calculate damage to apply to circuit, always at least 1
                    final int circuitDamage = Mth.clamp(
                            Mth.floor(event.getAmount() * 0.1),
                            1, Integer.MAX_VALUE
                    );
                    grace.damage(circuitDamage);
                }
            });
        });
    }

    @SubscribeEvent
    public static void onCommandRegister(RegisterCommandsEvent event){
        CommandRegistry.register(event.getDispatcher());
    }

    @SubscribeEvent
    public static void onServerTick(TickEvent.ServerTickEvent event){
        for(DelayedServerTask task : new ArrayList<>(DelayedServerTask.TASKS)){
            if(event.getServer().getTickCount() >= task.timeToStart()){
                task.action().run();
            }
        }
        DelayedServerTask.clearFinished(event.getServer().getTickCount());
    }

    @SubscribeEvent
    public static void onShieldBlock(ShieldBlockEvent event){
        if(event.getEntity() instanceof Player player){
            ItemStack shield = player.getUseItem();
            if(shield.getItem() == ItemRegistry.PERSONAL_SHIELD.get()){
                shield.getCapability(ForgeCapabilities.ENERGY).ifPresent(battery -> {
                    battery.extractEnergy(1, false);
                    if(battery.getEnergyStored() <= 0)
                        player.stopUsingItem();
                });
            }
        }
    }

    @SubscribeEvent
    public static void onExitTardis(TardisEvent.ExitEvent.Pre event){
        if(event.getEntity() instanceof Player player && LoyaltyFunctionRegistry.CLOSE_DOOR.get().applies(event.getTARDIS(), player)){

            LoyaltyFunctionRegistry.CLOSE_DOOR.get().saveFromVoid(event.getTARDIS(), player, event);

        }
    }

    @SubscribeEvent
    public static void onPlayerFinishedUsing(LivingEntityUseItemEvent.Finish event){
        if(!event.getEntity().getLevel().isClientSide()){
            for(ServerLevel level : event.getEntity().getServer().getAllLevels()){
                Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
                    //If the using entity is crew of this tardis
                    if(tardis.getEmotionalHandler().getLoyalty(event.getEntity().getUUID()).isPresent()){
                        for(Trait t : tardis.getEmotionalHandler().getTraits()){
                            if(t instanceof LikeUseItemTrait trait){
                                trait.getLikedData(event.getResultStack()).ifPresent(like -> {
                                    tardis.getEmotionalHandler().modLoyalty(event.getEntity().getUUID(), like.loyalty());
                                    tardis.getEmotionalHandler().modMood(like.mood(), 0, 100);
                                });
                            }
                        }
                    }
                });
            }
        }
    }
}
