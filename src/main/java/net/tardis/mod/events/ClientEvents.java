package net.tardis.mod.events;

import net.minecraft.ChatFormatting;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.RegisterClientReloadListenersEvent;
import net.minecraftforge.client.event.RenderLevelStageEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.gui.IScreenKeyInput;
import net.tardis.mod.client.gui.ManualScreen;
import net.tardis.mod.client.renderers.level.ILevelExtraRenderer;
import net.tardis.mod.entity.IDrivable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DriveMessage;
import net.tardis.mod.resource_listener.client.ManualReloadListener;
import net.tardis.mod.sound.MovingSound;
import net.tardis.mod.sound.SoundRegistry;

import java.util.Optional;

@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {



    @SubscribeEvent
    public static void onWorldTickClient(TickEvent.ClientTickEvent event){
        //Needed to tick tardis caps on the client
        if(event.phase == TickEvent.Phase.END){
            Level level = Minecraft.getInstance().level;
            if(level != null && !Minecraft.getInstance().isPaused()){
                level.getCapability(Capabilities.TARDIS).ifPresent(ITardisLevel::tick);
            }
            ClientRegistry.tickLevelRenderers();
        }

    }

    @SubscribeEvent
    public static void onKey(InputEvent.Key event){
        final Player player = Minecraft.getInstance().player;

        //If there is no player in the world, we don't want any of this logic to be run
        if(player == null)
            return;

        //Send key presses to guis that listen for them
        if(Minecraft.getInstance().screen instanceof IScreenKeyInput input){
            if(input.onKeyPress(event.getKey(), event.getAction())){
                event.setCanceled(true);
            }
        }

        //Open the manual when the hotkey is pressed
        if(Minecraft.getInstance().screen instanceof AbstractContainerScreen<?> screen){
            if(ClientRegistry.MANUAL_KEY.consumeClick()){
                Slot s = screen.getSlotUnderMouse();
                if(s != null && ManualReloadListener.itemsInManual.contains(s.getItem().getItem())){
                    Minecraft.getInstance().setScreen(new ManualScreen(Optional.of(s.getItem().getItem())));
                }
            }
        }

        //Drive vehicles
        if(player.getVehicle() instanceof IDrivable drivable){
            KeyMapping forward = Minecraft.getInstance().options.keyUp;
            KeyMapping reverse = Minecraft.getInstance().options.keyDown;
            if(forward.isDown() || reverse.isDown()){
                float turn = Minecraft.getInstance().options.keyLeft.isDown() ? -5 :
                        Minecraft.getInstance().options.keyRight.isDown() ? 5 : 0;

                drivable.drive(turn, forward.isDown() ? drivable.getMaxSpeed() : -drivable.getMaxSpeed());
                Network.sendToServer(new DriveMessage(player.getVehicle().getId(), turn, forward.isDown() ? drivable.getMaxSpeed() : -drivable.getMaxSpeed()));
            }
            else {
                drivable.stop();
                Network.sendToServer(new DriveMessage(player.getVehicle().getId(), 0, 0));
            }
        }

    }

    @SubscribeEvent
    public static void onTardisEntered(TardisEvent.EnterEvent.Post event){
        if(event.getTARDIS().getInteriorManager().hasNeverFlown()){
            if(event.getEntity() instanceof Player player){
                Minecraft.getInstance().getSoundManager().play(new MovingSound(player, SoundRegistry.FIRST_ENTRANCE_MUSIC.get(), SoundSource.PLAYERS, player.getRandom(), 1.0F, false));
            }
        }
    }

    @SubscribeEvent
    public static void onLevelRender(RenderLevelStageEvent event){

        for(ILevelExtraRenderer renderer : ClientRegistry.getLevelExtraRenderers(event.getStage())){
            if(renderer.shouldRender(event.getLevelRenderer(), event.getCamera(), event.getFrustum())){
                renderer.render(event.getLevelRenderer(), event.getPoseStack(), event.getProjectionMatrix(), event.getCamera(),
                        event.getRenderTick(), event.getPartialTick());
            }
        }
    }

    @SubscribeEvent
    public static void onTooltip(ItemTooltipEvent event){
        if(ManualReloadListener.itemsInManual.contains(event.getItemStack().getItem())){
            event.getToolTip().add(
                    Component.literal("Press %s to open TARDIS Manual Entry".formatted(ClientRegistry.MANUAL_KEY.getKey().getDisplayName().getString()))
                            .withStyle(ChatFormatting.GRAY,
                                    ChatFormatting.UNDERLINE,
                                    ChatFormatting.ITALIC
                            )
            );
        }
    }

    @net.minecraftforge.fml.common.Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT, bus = net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus.MOD)
    public static class Mod{

        @SubscribeEvent
        public static void onReloadListener(RegisterClientReloadListenersEvent event){
            event.registerReloadListener(new ManualReloadListener());
        }

    }

}
