package net.tardis.mod.ars;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.api.events.TardisARSRoomProccessEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.ConsoleBlock;
import net.tardis.mod.helpers.CodecHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.DelayedServerTask;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.world.TardisChunkGenerator;
import net.tardis.mod.world.data.ARSRoomLevelData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ARSRoom {

    public static final Codec<ARSRoom> CODEC = RecordCodecBuilder.create(
            instance -> {
                return instance.group(
                        ResourceLocation.CODEC.fieldOf("roomLocation").forGetter(ARSRoom::getRoomLocation),
                        Type.CODEC.fieldOf("type").forGetter(ARSRoom::getType),
                        Codec.BOOL.fieldOf("canBeRemoved").forGetter(ARSRoom::canBeRemoved),
                        Codec.INT.fieldOf("y_offset").forGetter(ARSRoom::getYOffset),
                        ResourceLocation.CODEC.optionalFieldOf("parent").forGetter(ARSRoom::getParent),
                        CodecHelper.GENERIC_RESOURCE_KEY_CODEC.listOf().fieldOf("extra_unlocks").forGetter(ARSRoom::getExtraUnlocks),
                        Codec.BOOL.fieldOf("generates_naturally").forGetter(ARSRoom::generatesNaturally)
                ).apply(instance, ARSRoom::new);
            });

    public static final StructurePlaceSettings DEFAULT_SETTINGS = new StructurePlaceSettings();

    private final ResourceLocation roomLocation;
    private final boolean canBeRemoved;
    private final Type type;
    private final int yOffset;
    private final Optional<ResourceLocation> parent;
    private final List<ResourceKey<?>> extra_unlocks;
    private final boolean generatesNaturally;

    public ARSRoom(ResourceLocation roomLocation, Type type, boolean canBeRemoved, int yOffset, Optional<ResourceLocation> parent, List<ResourceKey<?>> extra_unlocks, boolean generatesNaturally) {
        this.roomLocation = roomLocation;
        this.type = type;
        this.canBeRemoved = canBeRemoved;
        this.yOffset = yOffset;
        this.parent = parent;
        this.extra_unlocks = extra_unlocks;
        this.generatesNaturally = generatesNaturally;
    }

    public ARSRoom(ResourceLocation roomLocation, Type type, boolean canBeRemoved, int yOffset, Optional<ResourceLocation> parent){
        this(roomLocation, type, canBeRemoved, yOffset, parent, new ArrayList<>(), true);
    }

    public ARSRoom(ResourceLocation roomLocation, Type type){
        this(roomLocation, type, 0);
    }

    public ARSRoom(ResourceLocation roomLocation, Type type, int yOffset){
        this(roomLocation, type, true, yOffset, Optional.empty());
    }

    public void spawnRoom(ServerLevelAccessor level, BlockPos pos, StructureTemplateManager manager, StructurePlaceSettings settings){
        final BlockPos finalPos = pos.immutable().above(this.getYOffset());
        manager.get(this.roomLocation).ifPresentOrElse(room -> {

            DelayedServerTask.schedule(level.getServer(), () -> {
                ARSRoomLevelData.getData(level.getLevel()).addRoomEntry(
                        level.getLevel().registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getKey(this),
                        finalPos,
                        room.getSize()
                );
            });

            room.placeInWorld(level, finalPos, finalPos, settings, level.getRandom(), 3);
            doTardisProcessing(level, finalPos, this, room);
        }, () -> Tardis.LOGGER.warn("Could not find structure for ars room " + level.registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getKey(this).toString()));
    }

    public static BlockPos getSpawnPos(ChunkPos pos){
        return pos.getBlockAt(0, 64, 0).north(TardisChunkGenerator.chunkSize).west(TardisChunkGenerator.chunkSize);
    }

    public static void doTardisProcessing(ServerLevelAccessor level, final BlockPos pos, ARSRoom arsRoom, StructureTemplate room){

        MinecraftForge.EVENT_BUS.post(new TardisARSRoomProccessEvent(level, pos, arsRoom, room));

        //Remove door ID from interior doors
        room.filterBlocks(pos, DEFAULT_SETTINGS, BlockRegistry.INTERIOR_DOOR.get()).forEach(info -> {
            info.nbt.remove("door_id");
        });

        room.filterBlocks(pos, DEFAULT_SETTINGS, BlockRegistry.ARS_PANEL.get()).forEach(info -> {
            info.nbt.putLong("spawn_pos", pos.asLong());
            info.nbt.putString("room", level.getLevel().registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getKey(arsRoom).toString());
        });

        ForgeRegistries.BLOCKS.getValues().stream().filter(block -> block instanceof ConsoleBlock).forEach(block -> {
            room.filterBlocks(pos, DEFAULT_SETTINGS, block).forEach(info -> {
                info.nbt.remove("control_list");
                Tardis.LOGGER.debug("Removed control info from %s at %d, %d, %d".formatted(
                        ForgeRegistries.BLOCKS.getKey(block),
                        info.pos.getX(),
                        info.pos.getY(),
                        info.pos.getZ()
                ));
            });
        });

    }

    public ResourceLocation getRoomLocation(){
        return this.roomLocation;
    }

    public boolean canBeRemoved(){
        return this.canBeRemoved;
    }

    public int getYOffset(){
        return this.yOffset;
    }

    public Type getType(){
        return this.type;
    }

    public boolean generatesNaturally(){
        return this.generatesNaturally;
    }

    public Optional<ResourceLocation> getParent(){
        return this.parent;
    }

    public List<ResourceKey<?>> getExtraUnlocks(){
        if(this.extra_unlocks == null)
            return new ArrayList<>();
        return this.extra_unlocks;
    }

    public static ResourceLocation createCorridorRL(ResourceLocation rl){
        return rl.withPrefix("corridor/");
    }

    public static ResourceLocation createConsoleRoomRl(ResourceLocation rl){
        return rl.withPrefix("console_room/");
    }

    public enum Type{
        ROOM,
        CORRIDOR;

        public static Codec<Type> CODEC = CodecHelper.createEnumCodec(Type.class);

        public boolean isRoom(){
            return this != CORRIDOR;
        }

    }

    public static class Builder{

        final ResourceLocation id;
        final Type type;
        boolean canRemove = true;
        int yOffset = 0;
        ResourceLocation parent;
        List<ResourceKey<?>> extraUnlocks = new ArrayList<>();
        boolean generateNaturally = true;

        public Builder(ResourceLocation id, Type type){
            this.id = id;
            this.type = type;
        }

        public Builder noRemove(){
            this.canRemove = false;
            return this;
        }

        public Builder withOffset(int y){
            this.yOffset = y;
            return this;
        }

        public Builder withParent(ResourceLocation parent){
            this.parent = parent;
            return this;
        }

        public Builder addUnlock(ResourceKey<?> key){
            this.extraUnlocks.add(key);
            return this;
        }

        public Builder addUnlock(ItemLike item){
            return this.addUnlock(ForgeRegistries.ITEMS.getResourceKey(item.asItem()).get());
        }

        public Builder setNoSpawn(){
            this.generateNaturally = false;
            return this;
        }

        public ARSRoom build(){
            return new ARSRoom(this.id, this.type, this.canRemove, this.yOffset, Helper.nullableToOptional(this.parent), this.extraUnlocks, this.generateNaturally);
        }
    }
}
