package net.tardis.mod.ars;

import net.minecraft.core.BlockPos;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.JsonRegistries;

import java.util.Optional;

public class RoomEntry implements INBTSerializable<CompoundTag> {

    public ResourceLocation room;
    public BlockPos start;
    public Vec3i size;
    private AABB boundingBox;

    private boolean isDirty = false;

    public RoomEntry(ResourceLocation roomRL, BlockPos start, Vec3i size){
        this.room = roomRL;
        this.start = start;
        this.size = size;
    }

    public RoomEntry(CompoundTag tag){
        this.deserializeNBT(tag);
    }

    public void setRoom(ResourceLocation room){
        this.room = room;
        this.setChanged();
    }

    public void setChanged(){
        this.isDirty = true;
    }

    public boolean isDirty(){
        return this.isDirty;
    }

    public AABB getBoundingBox(){
        if(this.boundingBox == null)
            this.boundingBox = new AABB(this.start, this.start.immutable().offset(this.size));
        return this.boundingBox;
    }

    public boolean isInside(BlockPos pos){
       // return this.getBoundingBox().contains(Helper.blockPosToVec3(pos, true));

        return pos.getX() >= start.getX() && pos.getX() <= start.getX() + size.getX() &&
                pos.getZ() >= start.getZ() && pos.getZ() <= start.getZ() + size.getZ();

    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putString("room", this.room.toString());
        tag.putLong("start", this.start.asLong());
        tag.putInt("sizeX", this.size.getX());
        tag.putInt("sizeY", this.size.getY());
        tag.putInt("sizeZ", this.size.getZ());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.room = new ResourceLocation(tag.getString("room"));
        this.start = BlockPos.of(tag.getLong("start"));
        this.size = new Vec3i(
                tag.getInt("sizeX"),
                tag.getInt("sizeY"),
                tag.getInt("sizeZ")
        );
    }

    public Optional<ARSRoom> getRoom(RegistryAccess reg) {
        return reg.registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getOptional(this.room);
    }

}
