package net.tardis.mod.datagen;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class TardisItemArtronDataProvider implements DataProvider {

    final DataGenerator generator;
    final String modid;

    /**
     * Add ons use this one
     * @param generator
     * @param modid - Mod id of your add-on
     */
    public TardisItemArtronDataProvider(DataGenerator generator, String modid){
        this.generator = generator;
        this.modid = modid;
    }

    public TardisItemArtronDataProvider(DataGenerator gen){
        this(gen, Tardis.MODID);
    }

    final List<ItemArtronValueReloader.ArtronValue> values = new ArrayList<>();

    public void registerArtronValues(){
        this.add(BlockRegistry.XION.get().asItem(), 10);
        this.add(BlockRegistry.XION_BLOCK.get().asItem(), 40);
    }

    public void add(Item item, float value){
        this.add(Ingredient.of(item), value);
    }

    public void add(TagKey<Item> tag, float value){
        add(Ingredient.of(tag), value);
    }

    public void add(Ingredient ing, float value){
        this.values.add(new ItemArtronValueReloader.ArtronValue(ing, value));
    }

    public JsonObject createJson(){
        JsonObject root = new JsonObject();

        JsonArray array = new JsonArray();
        for(ItemArtronValueReloader.ArtronValue val : this.values){
            array.add(val.serialize());
        }

        root.add("values", array);
        return root;
    }

    /**
     *
     * @param fileName just the name of the file, {name}.json
     * @return
     */
    public Path getPath(String fileName){
        return this.generator.getPackOutput().getOutputFolder(PackOutput.Target.DATA_PACK)
                .resolve(this.modid)
                .resolve(ItemArtronValueReloader.FOLDER_PATH)
                .resolve(fileName + ".json");
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output) {

        this.registerArtronValues();

        return DataProvider.saveStable(output, createJson(), getPath("tardis_values"));
    }

    @Override
    public String getName() {
        return "TARDIS Mod Item Artron Provider";
    }
}
