package net.tardis.mod.datagen;

import com.mojang.serialization.JsonOps;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.worldgen.Structures;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.StructureTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.ExteriorRegistry;
import net.tardis.mod.resource_listener.server.LandUnlockPredicatesReloader;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class LandUnlockerProvider implements DataProvider {

    final HashMap<ResourceLocation, Builder> predicates = new HashMap<>();
    final String modid;
    final DataGenerator gen;
    final CompletableFuture<HolderLookup.Provider> registries;

    public LandUnlockerProvider(DataGenerator gen, CompletableFuture<HolderLookup.Provider> registries, String modid){
        this.gen = gen;
        this.registries = registries;
        this.modid = modid;
    }

    public LandUnlockerProvider(DataGenerator gen, CompletableFuture<HolderLookup.Provider> registries){
        this(gen, registries, Tardis.MODID);
    }


    public void addUnlockers(HolderLookup.Provider reg){
        this.add("mansion", Builder.create(ExteriorRegistry.IMPALA.getKey())
                .withStructure(new ResourceLocation("mansion")));

        this.add("trunk", Builder.create(ExteriorRegistry.TRUNK.getKey())
                .withBiome(BiomeTags.IS_BEACH)
        );
        this.add("coffin", Builder.create(ExteriorRegistry.COFFIN.getKey())
                .withBiome(BiomeTags.IS_BADLANDS));
        this.add("spruce_door", Builder.create(ExteriorRegistry.SPRUCE.getKey())
                .withStructure(new ResourceLocation("village_desert"))
                .withStructure(new ResourceLocation("village_plains"))
                .withStructure(new ResourceLocation("village_savanna"))
                .withStructure(new ResourceLocation("village_snowy"))
                .withStructure(new ResourceLocation("village_taiga"))
        );
        this.add("octa", new Builder(ExteriorRegistry.OCTA.getKey())
                .withStructure(new ResourceLocation("ocean_ruin_cold"))
                .withStructure(new ResourceLocation("ocean_ruin_warm"))
        );

    }

    public void add(String name, Builder builder){
        this.predicates.put(new ResourceLocation(this.modid, name), builder);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output) {

        return this.registries.thenCompose(reg -> {
            this.addUnlockers(reg);

            CompletableFuture<?>[] tasks = new CompletableFuture[this.predicates.size()];
            int i = 0;
            for(Map.Entry<ResourceLocation, Builder> entry : this.predicates.entrySet()){
                try{
                    tasks[i] = DataProvider.saveStable(output,
                            LandUnlockPredicatesReloader.LandUnlockPredicate.CODEC.encodeStart(JsonOps.INSTANCE, entry.getValue().create()).getOrThrow(false, Tardis.LOGGER::warn),
                            getPath(entry.getKey())
                    );
                }
                catch(Exception e){
                    Tardis.LOGGER.warn("Error Processing Land Predicate {}!", entry.getKey().toString());
                    Tardis.LOGGER.warn(e);
                }
                ++i;
            }
            return CompletableFuture.allOf(tasks);
        });
    }

    public Path getPath(ResourceLocation id){
        return gen.getPackOutput().getOutputFolder()
                .resolve("data").resolve(this.modid).resolve(LandUnlockPredicatesReloader.PATH)
                .resolve(id.getPath() + ".json");
    }

    @Override
    public String getName() {
        return "TARDIS Land unlocker";
    }

    public static class Builder{

        final ResourceKey<?> unlockId;

        public List<TagKey<Biome>> biomes = new ArrayList<>();
        public List<ResourceLocation> structures = new ArrayList<>();

        private Builder(ResourceKey<?> unlockId){
            this.unlockId = unlockId;
        }

        public static Builder create(ResourceKey<?> unlockId){
            return new Builder(unlockId);
        }

        public Builder withBiome(TagKey<Biome> biome){
            this.biomes.add(biome);
            return this;
        }

        public Builder withStructure(ResourceLocation struct){
            this.structures.add(struct);
            return this;
        }


        public LandUnlockPredicatesReloader.LandUnlockPredicate create() throws Exception{
            if(this.biomes.isEmpty() && this.structures.isEmpty())
                throw new Exception("LandUnlocker must have either a biome or structure!");

            return new LandUnlockPredicatesReloader.LandUnlockPredicate(
                    this.unlockId,
                    this.biomes.isEmpty() ? Optional.empty() : Optional.of(this.biomes),
                    this.structures.isEmpty() ? Optional.empty() : Optional.of(this.structures)
            );
        }

    }
}
