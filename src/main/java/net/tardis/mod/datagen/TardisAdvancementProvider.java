package net.tardis.mod.datagen;

import net.minecraft.advancements.*;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.ForgeAdvancementProvider;
import net.tardis.mod.Tardis;
import net.tardis.mod.advancement.TardisTriggers;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class TardisAdvancementProvider extends ForgeAdvancementProvider {

    public static final List<Advancement> ADVANCEMENTS = new ArrayList<>();

    public static final ResourceLocation ROOT = Helper.createRL("root");
    public static final ResourceLocation GATHER_XION = Helper.createRL("gather_xion");
    public static final ResourceLocation CRYSTAL_DREAM = Helper.createRL("crystal_dream");
    public static final ResourceLocation FIRST_ENTRANCE = Helper.createRL("enter_tardis");
    public static final ResourceLocation FIRST_TAKEOFF = Helper.createRL("first_takeoff");


    public TardisAdvancementProvider(PackOutput p_256529_, CompletableFuture<HolderLookup.Provider> p_255722_, ExistingFileHelper fileHelper) {
        super(p_256529_, p_255722_, fileHelper, buildSubproviders());
    }

    public static List<AdvancementGenerator> buildSubproviders(){
        List<AdvancementGenerator> providers = new ArrayList<>();

        providers.add((registries, saver, existingFileHelper) -> {

            /**
            Advancement root = advancement(ROOT, BlockRegistry.STEAM_BROKEN_EXTERIOR.get(), FrameType.TASK, options -> {

            });
             **/

            Advancement gatherXion = advancement(GATHER_XION, BlockRegistry.XION.get(), FrameType.TASK, options -> {
                //options.parent(root);
                options.addCriterion("has_crystal", hasItem(BlockRegistry.XION.get()));
            });

            Advancement crystalDream = advancement(CRYSTAL_DREAM, Blocks.RED_BED, FrameType.TASK, options -> {
                options.parent(gatherXion)
                        .requirements(RequirementsStrategy.AND)
                        .addCriterion("slept", PlayerTrigger.TriggerInstance.sleptInBed())
                        .addCriterion("has_crystal", hasItem(BlockRegistry.XION.get()));
            });
            /*

            Advancement firstEnter = advancement(Helper.createRL("first_enter"), BlockRegistry.STEAM_BROKEN_EXTERIOR.get(), FrameType.TASK, options -> {
                options.parent(crystalDream)
                        .addCriterion("", new ChangeDimensionTrigger.TriggerInstance(EntityPredicate.Composite.ANY, ));
            });

             */

            Advancement firstTakeoff = advancement(FIRST_TAKEOFF, ItemRegistry.DEMAT_CIRCUIT.get(), FrameType.GOAL, options -> {
                options.parent(crystalDream)
                        .addCriterion("takeoff", new PlayerTrigger.TriggerInstance(TardisTriggers.TAKE_OFF_TARDIS.getId(), EntityPredicate.Composite.ANY));

            });

            //Seperate
            advancement(Helper.createRL("make_tea"), ItemRegistry.TEA.get(), FrameType.GOAL, options -> {
                options.addCriterion("has_tea", hasItem(ItemRegistry.TEA.get()));
            });


            for(Advancement a : ADVANCEMENTS){
                saver.accept(a);
            }

        });

        return providers;
    }

    public static InventoryChangeTrigger.TriggerInstance hasItem(ItemLike item){
        return InventoryChangeTrigger.TriggerInstance.hasItems(item);
    }

    public static Advancement advancement(ResourceLocation id, ItemLike displayItem, FrameType frameType, Consumer<Advancement.Builder> buildOptions){
        Advancement.Builder builder = Advancement.Builder.advancement();
        builder = builder.display(
                displayItem,
                translate(id, true),
                translate(id, false),
                Helper.createRL("textures/block/decoration/roundels/concrete/light_blue_concrete_full.png"),
                frameType,
                true, true, false

        );
        buildOptions.accept(builder);
        Advancement a = builder.build(id);
        ADVANCEMENTS.add(a);
        return a;
    }

    public static Component translate(ResourceLocation Id, boolean isTitle){
        return Component.translatable(
                Tardis.MODID + ".advancement." + (isTitle ? "title." : "desc.") + Id.getNamespace() + "." + Id.getPath().replace('/', '.')
        );
    }
}
