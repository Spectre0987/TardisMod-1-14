package net.tardis.mod.datagen;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;

import java.util.concurrent.CompletableFuture;

public class TardisBiomeTagProvider extends TagsProvider<Biome> {
    public TardisBiomeTagProvider(PackOutput pOutput, CompletableFuture<HolderLookup.Provider> pLookupProvider, ExistingFileHelper helper) {
        super(pOutput, Registries.BIOME, pLookupProvider, Tardis.MODID, helper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        this.tag(Tags.Biomes.IS_VOID)
                .add(ResourceKey.create(Registries.BIOME, Helper.createRL("tardis")));
    }
}
