package net.tardis.mod.datagen;

import com.mojang.serialization.JsonOps;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.loot.BlockLootTable;
import net.tardis.mod.registry.JsonRegistries;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class TardisBlockAsLootProvider implements DataProvider {

    final DataGenerator generator;
    final String modid;

    final HashMap<ResourceLocation, BlockLootTable> loot = new HashMap<>();

    public TardisBlockAsLootProvider(DataGenerator generator, String modid){
        this.generator = generator;
        this.modid = modid;
    }

    public TardisBlockAsLootProvider(DataGenerator generator){
        this(generator, Tardis.MODID);
    }

    public BlockLootTable loot(ResourceLocation loc){
        final BlockLootTable table = BlockLootTable.create();
        this.loot.put(loc, table);
        return table;
    }

    public void registerLoot(){
        this.loot(Helper.createRL("rift"))
                .addBlock(BlockRegistry.STEAM_BROKEN_EXTERIOR.get(), 100);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput pOutput) {
        registerLoot();
        final CompletableFuture<?>[] tasks = new CompletableFuture[this.loot.size()];

        int index = 0;
        for(Map.Entry<ResourceLocation, BlockLootTable> entry : this.loot.entrySet()){

            Path path = getPath(entry.getKey());
            tasks[index] = DataProvider.saveStable(pOutput, BlockLootTable.CODEC.encodeStart(JsonOps.INSTANCE, entry.getValue()).getOrThrow(false, s -> {}), path);

            index++;
        }

        return CompletableFuture.allOf(tasks);
    }

    public Path getPath(ResourceLocation key){
        return this.generator.getPackOutput().getOutputFolder()
                .resolve("data")
                .resolve(modid)
                .resolve(Constants.getPathFromJsonReg(JsonRegistries.BLOCK_AS_LOOT, key));
    }

    @Override
    public String getName() {
        return "TARDIS Blocks as Loot";
    }
}
