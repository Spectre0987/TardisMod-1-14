package net.tardis.mod.datagen;

import com.google.gson.JsonElement;
import com.mojang.serialization.JsonOps;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.client.gui.manual.entries.*;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.registry.VortexPhenomenaRegistry;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class TardisManualProvider implements DataProvider {

    final DataGenerator gen;
    final String modid;
    final String language;
    final HashMap<ResourceLocation, ManualEntry> entries = new HashMap<>();

    public static ManualChapter BROKEN_EXTERIORS = ManualChapter.create("broken_exteriors", 9999999);
    public static ManualChapter MAIN_CHAPTER = ManualChapter.create("main", 99999);
        public static ManualChapter REFULING = ManualChapter.create("refuel", 300, MAIN_CHAPTER);
        public static ManualChapter FLIGHT = ManualChapter.create("flight", 10, MAIN_CHAPTER);
            public static ManualChapter TAKEOFF = ManualChapter.create("takeoff", 100, FLIGHT);
            public static ManualChapter GENERAL_FLIGHT = ManualChapter.create("general_flight", 75, FLIGHT);
            public static ManualChapter LANDING = ManualChapter.create("landing", 25, FLIGHT);
        public static ManualChapter COMPONENTS = ManualChapter.create("components", 250, MAIN_CHAPTER);
        public static ManualChapter UPGRADES = ManualChapter.create("upgrades", 0, MAIN_CHAPTER);
    public static final ManualChapter VP_CHAPTER = ManualChapter.create("vortex_phenomena", 75);
    public static final ManualChapter TOOLS_CHAPTER = ManualChapter.create("tools", 100);
        public static ManualChapter CFL = ManualChapter.create("cfl", 999, TOOLS_CHAPTER);
            public static ManualChapter CFL_FUNCTIONS_CHAPTER = ManualChapter.create("cfl_functions", 99, CFL);
        public static final ManualChapter SONIC = ManualChapter.create("sonic", 80, TOOLS_CHAPTER);
            public static final ManualChapter SONIC_FUNCTIONS = ManualChapter.create("sonic_functions", 0, SONIC);
    public static final ManualChapter FLIGHT_EVENTS = ManualChapter.create("flight_events", 120);
    public static final ManualChapter MACHINES = ManualChapter.create("machines", 0);


    public TardisManualProvider(DataGenerator gen, String modid, String language){
        this.gen = gen;
        this.modid = modid;
        this.language = language;
    }

    public TardisManualProvider(DataGenerator gen){
        this(gen, Tardis.MODID, "en_us");
    }

    public void registerManualEntries(){

        this.title("broken/title", BROKEN_EXTERIORS, "Derelict Time Ships");
        this.text("broken/page_1", BROKEN_EXTERIORS, "   There is a possibility that a particularly rebellious TARDIS' security systems may degrade with time if they are not maintained. Such TARDISes may allow people entry if they are provided with enough items relevant to the TARDIS' interests. If such a time ship is found, using the Chronofault Locator on it will allow you to see the required items.");

        this.title("main_title", MAIN_CHAPTER, "T.T. Capsule Type 40");

        this.title("fuel_title", REFULING, "TARDIS Refueling");
        this.text("fuel/refueling", REFULING, "Refueling the TARDIS\n\nShould the timeship become disconnected from the main Eye of Harmony on Galifrey, (Like if you've fallen into another universe) it is possible to fuel the ship with rift energy, or by burning crystals made of condensed Artron, like Xion Crystals, in the Capacitor section of the TARDIS Engine.");

        this.title("flight/title", FLIGHT, "TARDIS Flight Protocols");
        this.title("flight/takeoff_title", TAKEOFF, "Initiating Vortex Flight");
        this.text("flight/takeoff", TAKEOFF, "  To initiate Vortex Flight, a timeship must have artron, and at least a working dematerialization circuit and main fluid links. After these items are checked, you must only disengage the handbrake and increase throttle.");
        this.title("flight/general_title", GENERAL_FLIGHT, "Vortex Flight");
        this.text("flight/general", GENERAL_FLIGHT, "Vortex Flight\n\nIn flight, any changes to the destination will be added to your current flight course. By throttling down, you can pause your progress in the vortex, avoiding fuel use and flight events. ");
        this.title("flight/landing", LANDING, "Exiting Vortex Flight");
        this.text("flight/landing/text", LANDING, "To exit the vortex, you must first reduce your throttle to zero, then engage the handbrake. If one were to engage the handbrake while the TARDIS was still throttled up, the ship will violently crash.");

        this.title("components/title", COMPONENTS, "TARDIS Subsystems");
        this.item("components/demat", COMPONENTS, ItemRegistry.DEMAT_CIRCUIT.get(), "Dematerialization Circuit\n\nRequired for Flight\n  This is the most critical component of the TARDIS. It handles the transition between the vortex and real-space, so operators must ensure this system is in good health.");
        this.item("components/fluid_links", COMPONENTS, ItemRegistry.FLUID_LINKS.get(), "Fluid Links\n\nRequired for Flight and refueling\n  A very important component for timeships, these are filled with mercury and serve as an interface between the time rotor and the Artron Banks");
        this.item("components/nav_com", COMPONENTS, ItemRegistry.NAV_COM.get(), "Nav-Com.\n\n  Handles TARDIS navigation. This circuit is required to change dimensions, for the ship to get it's bearings in space, and to program coordinates.");
        this.item("components/chameleon", COMPONENTS, ItemRegistry.CHAMELEON_CIRCUIT.get(), "Chameleon Cirucit\n\n  This circuit is required to change the appearance of the outer shell. It's purpose is to scan your ship's surroundings and disguise itself appropriately, but it can be programmed to keep a specific configuration if the operator wishes.");
        this.item("components/shield", COMPONENTS, ItemRegistry.SHIELD_GENERATOR.get(), "Shield Generator\n\n  This circuit will absorb incoming damage, sparing your other systems");
        this.item("components/grace", COMPONENTS, ItemRegistry.TEMPORAL_GRACE.get(), "Temporal Grace Circuit\n\n  This circuit will put the interior of your ship into a state of temporal grace, reducing damage you will take from most sources, and completely eliminate most enviromental damage");
        this.item("components/antenna", COMPONENTS, ItemRegistry.INTERSTITAL_ANTENNA.get(), "Interstitial Antenna\n\n  This circuit is required for receiving transmissions, scanning, or otherwise getting data from the world outside of the TARDIS");

        this.title("tools_title", TOOLS_CHAPTER, "Tools");
        this.item("tools/piston_hammer", TOOLS_CHAPTER, ItemRegistry.PISTON_HAMMER.get(), "Piston Hammer\n\n   Arms to weak to flatten metal on your own? Working with poor tolerances and can't get a part to fit? Percussive Maintenance is your friend. This tool can be used by right-clicking on a Temporal Recipe Design System to craft items");
        this.item("tools/welding_torch", TOOLS_CHAPTER, ItemRegistry.WELDING_TORCH.get(), "Welding Torch\n\n   Makes two pieces of metal into one piece of metal. You can use this on a Temporal Recipe Design System to craft more advanced items.");

        this.item("cfl", CFL, ItemRegistry.CHRONOFAULT_LOCATOR.get(), "The Chronofault Locator (C.F.L)\n\nAn extremely useful tool for diagnosing issues with your Capsule, " +
                "as well as finding anomalies around you. This item has several functions, most of which require it to be attuned to a time ship first");
        this.text("cfl/detector", CFL_FUNCTIONS_CHAPTER, "Distortion Detector\n\n The CFL is capable of detecting local space-time distortions. These could come in the form of time faring vessels, temporal rifts, or other such things.");
        this.text("cfl/internal_diag", CFL_FUNCTIONS_CHAPTER, "Internal Diagnostics\n\n Requires Attunement\n This function will let you see valuable info about the linked timeship, such as it's location, it's programmed destination, how much fuel the timeship currently has, and whether or not it is currently in the Time Vortex");
        this.text("cfl/subsystems", CFL_FUNCTIONS_CHAPTER, "Subsystem Readout\n\n Requires Attunement\n This mode allows you to see the health of the components from the linked timeship.");
        this.text("cfl/track_tardis", CFL_FUNCTIONS_CHAPTER, "Track Timeship\n\n Requires Attunement\n This mode allows the CFL to work as a compass, guiding you back to your timeship. A dot will appear on it's screen when you are facing the direction of the linked ship");

        this.item("sonic/desc", SONIC, ItemRegistry.SONIC.get(), "The multi-tool of multi-tools, the sonic probe, referred to by almost all as a sonic screwdriver. This tool is the definition of feature creep. What was invented by a rouge Time Lord as a simple universal screw driver has been given too many features to count, although we will attempt to on the following pages.");
        this.text("sonic/functions/block", SONIC_FUNCTIONS, "Block Interaction.\n\nThe main usage of this tool, this mode will interface with physical machines in the world. This will be able to override certain functions that would otherwise be unable to be accessed. If no specific override is present, it will allow you to use the machine long range instead.");

        this.vortexPhenomena("vp/space_battle", VP_CHAPTER, VortexPhenomenaRegistry.SPACE_BATTLE.get(), "A terrible battle locked in time, ships with working antennas and enough maneuverability can take advantage of the carnage and download schematics for use in your timeship. The standard procedure goes as follows. Use your timeship's randomizer to dodge incoming attacks, then your communicator to download any schematics you can find. If one of the participants in the battle should happen to notice you, using your timeship's throttle should be enough to escape them");
        this.vortexPhenomena("vp/asteroid", VP_CHAPTER, VortexPhenomenaRegistry.ASTROID.get(), "Naturally occurring space-rocks can sometimes make their way into the Time Vortex. If your ship is equipped with a Laser Mining Upgrade, you can collect raw materials from them, which can be retrieved via the Temporal Scoop Vault");
        this.vortexPhenomena("vp/ancient_debris", VP_CHAPTER, VortexPhenomenaRegistry.ANCIENT_DEBRIS.get(), "The site of a eons-passed great space battle, these areas can be home to a vast amount of salvage. Any Salvage your ship collects will be available via the Temporal Scoop Vault");
        this.vortexPhenomena("vp/wormhole", VP_CHAPTER, VortexPhenomenaRegistry.WORMHOLE.get(), "Complicated space-time events, wormholes link two points in the Time Vortex. Passing through one will deposit your ship at the other end, where ever that may be. Luckily, they work both ways");

        this.title("flight_event/title", FLIGHT_EVENTS, "Flight Events");
        this.text("flight_event/axis_drift", FLIGHT_EVENTS, "Axis Drift Event.\n\n Occasionally, the the navigation systems will start to drift in flight. To correct this, just activate the corresponding Axis control (X, Y, Z)");
        this.text("flight_event/time_winds", FLIGHT_EVENTS, "Time Winds.\n\n Time winds are forces within the Time Vortex that can slow your ship or knock it off course, virtually any model of timeship has enough power at full throttle to avoid this, so if you encounter them, you may simply increase throttle");
        this.text("flight_event/add_artron", FLIGHT_EVENTS, "Artron Pockets.\n\n Rarely, a timeship will encounter pockets of pure artron energy inside the Time Vortex. A quick pilot will be able to open the refueling control to soak up some of the artron as you pass through these pockets");

        this.title("upgrades/title", UPGRADES, "Retrofits (Upgrades)");
        this.upgrade("upgrades/energy_syphon", "Energy Syphon Upgrade\n\n  Drains atomic energy in a range round the TARDIS exterior, turning lava to obsidian and freezing water.");
        this.upgrade("upgrades/remote", "Remote Operations Circuit\n\n  Allows your ship to receive stattenheim signals traveling through the vortex.");
        this.upgrade("upgrades/nano_gene", "Nano-gene Carrier Upgrade\n\n  Fills your ship with nano-genes, self replicating nano bots that repair physical damage");
        this.upgrade("upgrades/laser_miner", "Laser miner circuit\n\n  Allows your ship's exoshell to emit a high powered mining laser that can be used to extract materials from physical objects in the Time Vortex, like Asteroids.");
        this.upgrade("upgrades/atrium", "TARDIS Atrium Upgrade\n\n  Allows you to build a structure in an Atrium Room. This structure will materialize and dematerialize with your time ship's Exo Shell. To activate, use a sonic on the Atrium door frame to assign a name to this structure, which will allow you to set it as active on your monitor.");

        this.title("machines/title", MACHINES, "Machines");
        this.item("machines/drafting_table", MACHINES, BlockRegistry.DRAFTING_TABLE.get().asItem(), "Temporal Recipe Design System (TRDS)\n\n  One of the simplest 'machines,' this is nothing more than a level workspace used to make complex items. Certain items made with this table will have a chance to fail, with the chance depending on the tools used");
        this.item("machines/quantiscope", MACHINES, BlockRegistry.QUANTISCOPE.get().asItem(), "Multi-Quantiscope\n\n   A very useful machine, this serves as 'tier 2' to the TRDS Table, able to create the same items cheaper and more reliably, as well as more advanced items that can't be made in the TRDS Table");
        this.item("machines/matter_buffer", MACHINES, BlockRegistry.MATTER_BUFFER.get(), "Real-Matter Buffer\n\nA machine capable of breaking down real matter from the outside world (such as cobblestone) for use in the TARDIS. Real-Matter from the buffer will be used in the creation and repair of rooms in the ARS system, or when generating items from the ARS Egg");
        this.item("machines/ars_egg", MACHINES, BlockRegistry.ARS_EGG.get(), "ARS Egg\n\nFound in the ARS Tree Room, these are the heart of the ARS System. Using these eggs, the TARDIS can create various machines and decorative blocks by using processed matter from the Real-Matter Buffer");
        this.item("machines/ars_panel", MACHINES, BlockRegistry.ARS_PANEL.get(), "ARS Panel\n\nFound in nearly every room in the TARDIS, these panels control that node of the ARS system. Using these, you can repair rooms that have become corrupted due to lack of maintenance, or to replace rooms with others.");
        this.item("machines/rift_pylon", MACHINES, BlockRegistry.RIFT_PYLON.get(), "Rift Pylon\n\nThese machines are capable of absorbing or transmitting energy from Temporal Rifts, and transmitting them to a valid collector. If there are Xion Crystals in it's vicinity, it can use rift power to grow those clusters");
        this.item("machines/rift_collector", MACHINES, BlockRegistry.RIFT_COLLECTOR.get(), "Artron Collector\n\nThis is the focal point of the Rift Pylons. They will absorb artron and send them to this collector, or take from this collector to charge a rift. By default, the collector will not push enough artron into rifts to overcharge them, or pull enough from rifts to close them. These safe guards can be disabled by using sonic on this machine.");
    }

    public void title(String id, ManualChapter chapter, String title){
        this.entries.put(new ResourceLocation(this.modid, id),
                new TitleEntry(ManualEntry.EntryType.TITLE, chapter, title)
        );
    }

    public void text(String id, ManualChapter chapter, String text){
        this.entries.put(new ResourceLocation(this.modid, id),
                new TextEntry(ManualEntry.EntryType.TEXT, chapter, text)
        );
    }

    public void item(String id, ManualChapter chapter, ItemLike item, String text){
        this.entries.put(new ResourceLocation(this.modid, id),
            new ItemEntry(ManualEntry.EntryType.ITEM, chapter, new ItemStack(item), text)
        );
    }

    public void upgrade(String id, String text){
        this.item(id, UPGRADES, ItemRegistry.UPGRADE_REMOTE.get(), text);
    }

    public void vortexPhenomena(String id, ManualChapter chapter, VortexPhenomenaType vpType, String text){
        this.entries.put(new ResourceLocation(this.modid, id),
                new VortexPhenomenonManualEntry(ManualEntry.EntryType.VP, chapter, vpType, text)
        );
    }

    @Override
    public CompletableFuture<?> run(CachedOutput pOutput) {

        this.registerManualEntries();

        final CompletableFuture<?> [] tasks = new CompletableFuture[this.entries.size()];

        int index = 0;
        for(Map.Entry<ResourceLocation, ? extends ManualEntry> e : this.entries.entrySet()){
            JsonElement ele = e.getValue().getEntryType().getCodec().encodeStart(JsonOps.INSTANCE, e.getValue()).getOrThrow(false, (error) -> {});

            tasks[index] = DataProvider.saveStable(pOutput, ele, getPath(e.getKey()));

            ++index;
        }

        return CompletableFuture.allOf(tasks);
    }

    public Path getPath(ResourceLocation loc){
        return this.gen.getPackOutput().getOutputFolder()
                .resolve("assets")
                .resolve(loc.getNamespace())
                .resolve(Tardis.MODID)
                .resolve("manual")
                .resolve(this.language)
                .resolve(loc.getPath() + ".json");
    }

    @Override
    public String getName() {
        return "TARDIS Manual Provider";
    }
}
