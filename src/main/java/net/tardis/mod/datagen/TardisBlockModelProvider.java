package net.tardis.mod.datagen;

import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TardisBlockModelProvider extends BlockModelProvider {
    public TardisBlockModelProvider(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {

    }

    public ResourceLocation key(Block block) {
        return ForgeRegistries.BLOCKS.getKey(block).withPrefix("block/");
    }
}
