package net.tardis.mod.datagen;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.item.KeyItem;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.tags.ItemTags;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

public class TardisItemTagsProvider extends ItemTagsProvider {


    public TardisItemTagsProvider(PackOutput p_275343_, CompletableFuture<HolderLookup.Provider> p_275729_, CompletableFuture<TagLookup<Block>> p_275322_, @Nullable ExistingFileHelper existingFileHelper) {
        super(p_275343_, p_275729_, p_275322_, Tardis.MODID, existingFileHelper);
    }

    @Override
    public void addTags(HolderLookup.Provider provider) {
        IntrinsicTagAppender<Item> componentTag = this.tag(ItemTags.CREATIVE_TAB_COMPONENTS);
                componentTag
                        .add(ItemRegistry.ARTRON_CAP_LEAKY.getKey())
                    .add(ItemRegistry.CIRCUIT_PLATE.getKey())
                    .add(ItemRegistry.CRYSTAL_VIALS.getKey())
                    .add(ItemRegistry.CHRONOFAULT_LOCATOR.getKey())
                    .add(ItemRegistry.EXO_CIRCUITS.getKey())
                    .add(ItemRegistry.PISTON_HAMMER.getKey())
                    .add(ItemRegistry.WELDING_TORCH.getKey())
                    .add(ItemRegistry.MERCURY_BOTTLE.getKey())
                    .add(ItemRegistry.DEMAT_CIRCUIT.getKey())
                    .add(ItemRegistry.MANUAL.getKey())
                    .add(ItemRegistry.PIRATE_KEY.getKey())
                    .add(ItemRegistry.GALLIFREYAN_KEY.getKey())
                    .add(ItemRegistry.CINNABAR.getKey())
                    .add(ItemRegistry.DEMAT_CIRCUIT.getKey())
                    .add(ItemRegistry.MANUAL.getKey())
                    .add(ItemRegistry.XION_LENS.getKey())
                    .add(ItemRegistry.DATA_CRYSTAL.getKey())
                    .add(ItemRegistry.ARTRON_CAP_BASIC.getKey())
                    .add(ItemRegistry.ARTRON_CAP_MID.getKey())
                    .add(ItemRegistry.ARTRON_CAP_HIGH.getKey())
                    .add(ItemRegistry.SONIC.getKey())
                    .add(ItemRegistry.CRYSTAL_VIALS_MERCURY.getKey())
                    .add(ItemRegistry.STATTENHEIM_REMOTE.getKey());
        add(componentTag,
                ItemRegistry.UPGRADE_BASE.get(),
                ItemRegistry.UPGRADE_NANO_GENE.get(),
                ItemRegistry.UPGRADE_LASER_MINER.get(),
                ItemRegistry.UPGRADE_ENERGY_SYPHON.get(),
                ItemRegistry.TEA.get(),
                ItemRegistry.UPGRADE_REMOTE.get(),
                ItemRegistry.BURNT_EXOCIRCUITS.get(),
                ItemRegistry.UPGRADE_ATRIUM.get(),
                ItemRegistry.FIRST_ENTER_RECORD.get(),
                ItemRegistry.UPGRADE_SONIC_DEMAT.get()
        );
        add(componentTag, item -> item instanceof SubsystemItem);
        add(componentTag, BlockRegistry.DRAFTING_TABLE);
        add(componentTag, BlockRegistry.ENGINE_STEAM);
        add(componentTag, BlockRegistry.G8_CONSOLE);
        add(componentTag, BlockRegistry.NEUVEAU_CONSOLE);
        add(componentTag, BlockRegistry.STEAM_BROKEN_EXTERIOR);
        add(componentTag, BlockRegistry.STEAM_MONITOR);
        add(componentTag, BlockRegistry.STEAM_CONSOLE);
        add(componentTag, BlockRegistry.INTERIOR_DOOR);
        add(componentTag, BlockRegistry.ALEMBIC);
        add(componentTag, BlockRegistry.DATA_MATRIX);
        add(componentTag, BlockRegistry.ARS_PANEL);
        add(componentTag, BlockRegistry.GALVANIC_CONSOLE);
        add(componentTag, BlockRegistry.XION_BLOCK);
        add(componentTag, BlockRegistry.XION);
        add(componentTag, BlockRegistry.ARS_PANEL);
        add(componentTag, BlockRegistry.EYE_MONITOR);
        add(componentTag, BlockRegistry.RCA_MONITOR);
        add(componentTag, BlockRegistry.ARS_EGG);
        add(componentTag, BlockRegistry.ENGINE_ROOF);
        add(componentTag,
                BlockRegistry.MATTER_BUFFER,
                BlockRegistry.SCOOP_VAULT,
                BlockRegistry.RIFT_COLLECTOR,
                BlockRegistry.RIFT_PYLON,
                BlockRegistry.QUANTISCOPE,
                BlockRegistry.NEMO_CONSOLE,
                BlockRegistry.WAYPOINT_BANKS,
                BlockRegistry.ATRIUM_PLATFORM_BLOCK,
                BlockRegistry.ATRIUM_MASTER,
                BlockRegistry.ITEM_PLAQUE,
                BlockRegistry.BIG_DOOR,
                BlockRegistry.ROUNDEL_TAP,
                BlockRegistry.VARIABLE_MONITOR,
                BlockRegistry.ALEMBIC_STILL,
                BlockRegistry.FABRICATOR_MACHINE,
                BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR,
                BlockRegistry.LANDING_PAD,
                BlockRegistry.RAILING_STEAM,
                BlockRegistry.RAILING_ALABASTER,
                BlockRegistry.RAILING_TUNGSTEN

        );


        this.tag(ItemTags.MERCURY_BOTTLE)
                .add(ItemRegistry.MERCURY_BOTTLE.getKey())
                .add(ItemRegistry.CRYSTAL_VIALS_MERCURY.getKey());

        this.tag(ItemTags.SHOW_CONTROL_ITEMS)
                .add(ItemRegistry.CHRONOFAULT_LOCATOR.get(), ItemRegistry.MANUAL.get());

        this.tag(Tags.Items.RAW_MATERIALS)
                .add(ItemRegistry.CINNABAR.get())
                .add(BlockRegistry.XION.get().asItem());

        this.tag(ItemTags.TARDIS_MUSIC_DISK)
                .add(ItemRegistry.FIRST_ENTER_RECORD.get());

        this.tag(ItemTags.COPPER_PLATES)
                .add(ItemRegistry.PLATE_COPPER.get());

        this.tag(net.minecraft.tags.ItemTags.MUSIC_DISCS)
                .addTag(ItemTags.TARDIS_MUSIC_DISK);

        this.tag(ItemTags.CINNABAR)
                .add(ItemRegistry.CINNABAR.get());


        IntrinsicTagAppender<Item> sonic = this.tag(ItemTags.SONIC_PORT_ACCEPTS)
                        .add(
                                ItemRegistry.SONIC.get(),
                                ItemRegistry.DATA_CRYSTAL.get()
                        );
        this.add(sonic, i -> i instanceof KeyItem);


        this.tag(ItemTags.ALEMBIC_BIOMASS)
                .addTags(Tags.Items.CROPS)
                .addTags(Tags.Items.SEEDS)
                .addTags(Tags.Items.MUSHROOMS)
                .addTags(net.minecraft.tags.ItemTags.SAPLINGS);

        IntrinsicTagAppender<Item> arsEgg = this.tag(ItemTags.ARS_EGG_ITEMS);
        add(arsEgg,
                BlockRegistry.RCA_MONITOR,
                BlockRegistry.ALABASTER_TILES_BIG,
                BlockRegistry.ALABASTER,
                BlockRegistry.ALABASTER_SCREEN,
                BlockRegistry.ALABASTER_BOOKSHELF,
                BlockRegistry.ALABASTER_RUNNER_LIGHTS,
                BlockRegistry.ALABASTER_PIPES,
                BlockRegistry.ALABASTER_PILLAR_WALL,
                BlockRegistry.ALABASTER_PILLAR,
                BlockRegistry.ALABASTER_WALL,
                BlockRegistry.ALABASTER_SLAB,
                BlockRegistry.ALABASTER_STAIRS,
                BlockRegistry.ALABASTER_TILES_SMALL,
                BlockRegistry.ALABASTER_TILES_LARGE,
                BlockRegistry.ENGINE_ROOF,
                BlockRegistry.DEDICATION_PLAQUE,
                BlockRegistry.ALABASTER_GRATE,
                BlockRegistry.TUNGSTEN_GRATE,
                BlockRegistry.ITEM_PLAQUE,
                BlockRegistry.BIG_DOOR,
                BlockRegistry.STEAM_CONSOLE,
                BlockRegistry.NEMO_CONSOLE,
                BlockRegistry.NEUVEAU_CONSOLE,
                BlockRegistry.TUNGSTEN_PLATE_SMALL,
                BlockRegistry.TUNGSTEN_SMALL_PLATE_SLAB,
                BlockRegistry.VARIABLE_MONITOR,
                BlockRegistry.CORAL_BLANK,
                BlockRegistry.CORAL_SLAB,
                BlockRegistry.CORAL_STAIRS,
                BlockRegistry.CORAL_WALL,
                BlockRegistry.METAL_GRATE_WALL,
                BlockRegistry.RAILING_TUNGSTEN,
                BlockRegistry.RAILING_ALABASTER,
                BlockRegistry.RAILING_STEAM

        );
        add(arsEgg, item -> item instanceof BlockItem b && b.getBlock() instanceof RoundelBlock);

        this.add(this.tag(ItemTags.ARS_LOCKED),
                    BlockRegistry.STEAM_CONSOLE.get().asItem(),
                    BlockRegistry.NEMO_CONSOLE.get().asItem(),
                    BlockRegistry.NEUVEAU_CONSOLE.get().asItem()
                );

        IntrinsicTagAppender<Item> roundelTag = this.tag(ItemTags.CREATIVE_TAB_ROUNDEL);
        //Add All Roundels automatically
        for(Item item : ForgeRegistries.ITEMS.getValues()){
            if(item instanceof BlockItem i){
                if(i.getBlock() instanceof RoundelBlock){
                    roundelTag.add(item);
                }
            }
        }
        add(roundelTag, BlockRegistry.FOAM_PIPE);
        add(roundelTag, BlockRegistry.METAL_GRATE);
        add(roundelTag, BlockRegistry.METAL_GRATE_SLAB);
        add(roundelTag, BlockRegistry.METAL_GRATE_SOLID);
        add(roundelTag, BlockRegistry.TUNGSTEN);
        add(roundelTag, BlockRegistry.TUNGSTEN_PLATE_SLAB);
        add(roundelTag, BlockRegistry.TUNGSTEN_PLATE);
        add(roundelTag, BlockRegistry.TUNGSTEN_PATTERN);
        add(roundelTag, BlockRegistry.TUNGSTEN_PIPES);
        add(roundelTag, BlockRegistry.TUNGSTEN_SCREEN);
        add(roundelTag, BlockRegistry.TUNGSTEN_RUNNING_LIGHTS);
        add(roundelTag, BlockRegistry.WIRE_HANG);
        add(roundelTag,
                BlockRegistry.ALABASTER,
                BlockRegistry.ALABASTER_TILES_LARGE,
                BlockRegistry.ALABASTER_TILES_SMALL,
                BlockRegistry.ALABASTER_PILLAR,
                BlockRegistry.BLINKING_LIGHTS,
                BlockRegistry.STEAM_GRATE,
                BlockRegistry.HOLO_LADDER,
                BlockRegistry.METAL_GRATE_TRAPDOOR,
                BlockRegistry.TUNGSTEN_STAIRS_SMALL_PLATES,
                BlockRegistry.TUNGSTEN_STAIRS,
                BlockRegistry.ALABASTER_STAIRS,
                BlockRegistry.ALABASTER_SLAB,
                BlockRegistry.METAL_GRATE_TRAPDOOR,
                BlockRegistry.COMFY_CHAIR_GREEN,
                BlockRegistry.COMFY_CHAIR_RED,
                BlockRegistry.ALABASTER_WALL,
                BlockRegistry.ALABASTER_PILLAR_WALL,
                BlockRegistry.ALABASTER_PIPES,
                BlockRegistry.ALABASTER_TILES_BIG,
                BlockRegistry.ALABASTER_BOOKSHELF,
                BlockRegistry.ALABASTER_SCREEN,
                BlockRegistry.ALABASTER_RUNNER_LIGHTS,
                BlockRegistry.EBONY_BOOKSHELF,
                BlockRegistry.STEEL_HEX_OFFSET,
                BlockRegistry.STEEL_HEX,
                BlockRegistry.MOON_BASE_GLASS,
                BlockRegistry.DEDICATION_PLAQUE,
                BlockRegistry.TUNGSTEN_GRATE,
                BlockRegistry.ALABASTER_GRATE,
                BlockRegistry.TUNGSTEN_PLATE_SMALL,
                BlockRegistry.TUNGSTEN_SMALL_PLATE_SLAB,
                BlockRegistry.CORAL_STAIRS,
                BlockRegistry.CORAL_SLAB,
                BlockRegistry.CORAL_BLANK,
                BlockRegistry.CORAL_WALL,
                BlockRegistry.METAL_GRATE_WALL
        );

        this.tag(ItemTags.KEYS)
                .add(ItemRegistry.PIRATE_KEY.get())
                .add(ItemRegistry.GALLIFREYAN_KEY.get());

    }

    //Some tag functions require the base block type, and will fail if you use a subclass instead
    public Block toBlock(RegistryObject<? extends Block> block){
        return block.get();
    }

    public void add(TagAppender<Item> appender, Predicate<Item> pred){
        ForgeRegistries.ITEMS.getValues().stream().filter(pred).forEach(item -> {
            ForgeRegistries.ITEMS.getResourceKey(item).ifPresent(appender::add);
        });
    }

    public void add(TagAppender<Item> tag, RegistryObject<? extends Block>... block){
        for(RegistryObject<?> b : block){
            tag.add(ResourceKey.create(Registries.ITEM, b.getId()));
        }
    }

    public void add(TagAppender<Item> tag, Item... items){
        for(Item item : items){
            ForgeRegistries.ITEMS.getResourceKey(item).ifPresent(key -> {
                tag.add(key);
            });
        }
    }
}
