package net.tardis.mod.datagen;

import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSet;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

public class TardisLootProvider extends LootTableProvider {

    public TardisLootProvider(PackOutput p_254123_) {
        super(p_254123_, Set.of(), createEntires());
    }

    public static List<SubProviderEntry> createEntires(){
        List<SubProviderEntry> list = new ArrayList<>();

        list.add(new SubProviderEntry(() -> new Blocks(Set.of()), LootContextParamSets.BLOCK));
        list.add(new SubProviderEntry(GenericLootTableProvider::new, LootContextParamSets.EMPTY));

        return list;
    }

    public static class Blocks extends BlockLootSubProvider{

        public Blocks(Set<Item> explosionResist) {
            super(explosionResist, FeatureFlags.REGISTRY.allFlags());
        }

        @Override
        protected void generate() {
            this.dropSelf(BlockRegistry.ALEMBIC.get());
            this.dropSelf(BlockRegistry.XION_BLOCK.get());
            this.dropSelf(BlockRegistry.XION.get());
            this.dropSelf(BlockRegistry.ATRIUM_PLATFORM_BLOCK.get());
            this.dropSelf(BlockRegistry.DRAFTING_TABLE.get());
            this.dropSelf(BlockRegistry.ENGINE_STEAM.get());
            this.dropSelf(BlockRegistry.G8_CONSOLE.get());
            this.dropSelf(BlockRegistry.INTERIOR_DOOR.get());
            this.dropSelf(BlockRegistry.NEUVEAU_CONSOLE.get());
            this.dropSelf(BlockRegistry.STEAM_CONSOLE.get());
            this.dropSelf(BlockRegistry.STEAM_MONITOR.get());
            this.dropSelf(BlockRegistry.METAL_GRATE.get());
            this.dropSelf(BlockRegistry.METAL_GRATE_SLAB.get());
            this.dropSelf(BlockRegistry.METAL_GRATE_SOLID.get());
            this.dropSelf(BlockRegistry.FOAM_PIPE.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PLATE.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_SCREEN.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_RUNNING_LIGHTS.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PATTERN.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PATTERN.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PLATE_SLAB.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PIPES.get());
            this.dropSelf(BlockRegistry.TUNGSTEN.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PLATE_SMALL.get());
            this.dropSelf(BlockRegistry.WIRE_HANG.get());
            this.dropSelf(BlockRegistry.ARS_PANEL.get());
            this.dropSelf(BlockRegistry.DATA_MATRIX.get());
            this.dropSelf(BlockRegistry.ALABASTER_PILLAR.get());
            this.dropSelf(BlockRegistry.ALABASTER.get());
            this.dropSelf(BlockRegistry.ALABASTER_TILES_SMALL.get());
            this.dropSelf(BlockRegistry.ALABASTER_TILES_LARGE.get());
            this.dropSelf(BlockRegistry.GALVANIC_CONSOLE.get());
            this.dropSelf(BlockRegistry.TECH_STRUT.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_STAIRS.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_STAIRS_SMALL_PLATES.get());
            this.dropSelf(BlockRegistry.EYE_MONITOR.get());
            this.dropSelf(BlockRegistry.RCA_MONITOR.get());
            this.dropSelf(BlockRegistry.ALABASTER_STAIRS.get());
            this.dropSelf(BlockRegistry.BLINKING_LIGHTS.get());
            this.dropSelf(BlockRegistry.HOLO_LADDER.get());
            this.dropSelf(BlockRegistry.STEAM_GRATE.get());
            this.dropSelf(BlockRegistry.ALABASTER_WALL.get());
            this.dropSelf(BlockRegistry.ALABASTER_SLAB.get());
            this.dropSelf(BlockRegistry.METAL_GRATE_TRAPDOOR.get());
            this.dropSelf(BlockRegistry.COMFY_CHAIR_RED.get());
            this.dropSelf(BlockRegistry.COMFY_CHAIR_GREEN.get());
            this.dropSelf(BlockRegistry.ALABASTER_PILLAR_WALL.get());
            this.dropSelf(BlockRegistry.ALABASTER_PIPES.get());
            this.dropSelf(BlockRegistry.ALABASTER_SCREEN.get());
            this.dropSelf(BlockRegistry.ALABASTER_TILES_BIG.get());
            this.dropSelf(BlockRegistry.ALABASTER_RUNNER_LIGHTS.get());
            this.dropSelf(BlockRegistry.ALABASTER_BOOKSHELF.get());
            this.dropSelf(BlockRegistry.EBONY_BOOKSHELF.get());
            this.dropSelf(BlockRegistry.MOON_BASE_GLASS.get());
            this.dropSelf(BlockRegistry.STEEL_HEX_OFFSET.get());
            this.dropSelf(BlockRegistry.STEEL_HEX.get());
            this.dropSelf(BlockRegistry.DEDICATION_PLAQUE.get());
            this.dropSelf(BlockRegistry.QUANTISCOPE.get());
            this.dropSelf(BlockRegistry.RIFT_PYLON.get());
            this.dropSelf(BlockRegistry.RIFT_COLLECTOR.get());
            this.dropSelf(BlockRegistry.MATTER_BUFFER.get());
            this.dropSelf(BlockRegistry.SCOOP_VAULT.get());
            this.dropSelf(BlockRegistry.NEMO_CONSOLE.get());
            this.dropSelf(BlockRegistry.ENGINE_ROOF.get());
            this.dropSelf(BlockRegistry.WAYPOINT_BANKS.get());
            this.dropSelf(BlockRegistry.ITEM_PLAQUE.get());
            this.dropSelf(BlockRegistry.ALABASTER_GRATE.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_GRATE.get());
            this.dropSelf(BlockRegistry.BIG_DOOR.get());
            this.dropSelf(BlockRegistry.ROUNDEL_TAP.get());
            this.dropSelf(BlockRegistry.LANDING_PAD.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_SMALL_PLATE_SLAB.get());
            this.dropSelf(BlockRegistry.TUNGSTEN_PLATE_SMALL.get());
            this.dropSelf(BlockRegistry.VARIABLE_MONITOR.get());
            this.dropSelf(BlockRegistry.ALEMBIC_STILL.get());
            this.dropSelf(BlockRegistry.CORAL_BLANK.get());
            this.dropSelf(BlockRegistry.CORAL_STAIRS.get());
            this.dropSelf(BlockRegistry.CORAL_SLAB.get());
            this.dropSelf(BlockRegistry.CORAL_WALL.get());
            this.dropSelf(BlockRegistry.METAL_GRATE_WALL.get());
            this.dropSelf(BlockRegistry.FABRICATOR_MACHINE.get());
            this.dropSelf(BlockRegistry.RAILING_STEAM.get());
            this.dropSelf(BlockRegistry.RAILING_ALABASTER.get());
            this.dropSelf(BlockRegistry.RAILING_TUNGSTEN.get());

            this.dropEmpty(
                    BlockRegistry.CORRIDOR_STRUCTURE_SAVER.get(),
                    BlockRegistry.EXTERIOR_COLLISION.get(),
                    BlockRegistry.MULTIBLOCK.get(),
                    BlockRegistry.STEAM_EXTERIOR.get(),
                    BlockRegistry.STEAM_BROKEN_EXTERIOR.get(),
                    BlockRegistry.MERCURY_FLUID.get(),
                    BlockRegistry.DISGUISED_BLOCK.get(),
                    BlockRegistry.ATRIUM_PLATFORM_BLOCK.get(),
                    BlockRegistry.ATRIUM_MASTER.get(),
                    BlockRegistry.RENDERER_TEST.get(),
                    BlockRegistry.POLICE_BOX_EXTERIOR.get(),
                    BlockRegistry.ARS_EGG.get(),
                    BlockRegistry.BIOMASS_FLUID.get(),
                    BlockRegistry.TT_CAPSULE.get(),
                    BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR.get(),
                    BlockRegistry.DISGUISED_BLOCK.get(),
                    BlockRegistry.CHAMELEON_EXTERIOR.get(),
                    BlockRegistry.CHAMELEON_AUXILLARY.get(),
                    BlockRegistry.MULTIBLOCK_REDIR_FLUID.get(),
                    BlockRegistry.TELEPORT_TUBE.get(),
                    BlockRegistry.COFFIN_EXTERIOR.get(),
                    BlockRegistry.OCTA_EXTERIOR.get(),
                    BlockRegistry.TRUNK_EXTERIOR.get(),
                    BlockRegistry.SPRUCE_EXTERIOR.get()
            );

            for(RegistryObject<Block> block : BlockRegistry.BLOCKS.getEntries()){
                if(block.get() instanceof RoundelBlock){
                    this.dropSelf(block.get());
                }
            }
        }

        public void dropEmpty(Block... blocks){
            for(Block block : blocks){
                this.add(block, LootTable.lootTable().setParamSet(LootContextParamSet.builder().optional(LootContextParams.EXPLOSION_RADIUS).build()));
            }
        }

        public void dropSelf(Block... blocks){
            for(Block block : blocks){
                super.dropSelf(block);
            }
        }

        @Override
        protected Iterable<Block> getKnownBlocks() {
            return BlockRegistry.BLOCKS.getEntries().stream().flatMap(RegistryObject::stream)::iterator;
        }
    }

    public static class GenericLootTableProvider implements LootTableSubProvider{

        @Override
        public void generate(BiConsumer<ResourceLocation, LootTable.Builder> builder) {
            builder.accept(Helper.createRL("vortex_phenomena/ancient_debris"), LootTable.lootTable()
                            .withPool(
                                //Junk Loot pool
                                LootPool.lootPool()
                                        .setRolls(ConstantValue.exactly(4))
                                        .name("junk")
                                        .add(LootItem.lootTableItem(Items.BONE).setWeight(30))
                                        .add(LootItem.lootTableItem(net.minecraft.world.level.block.Blocks.LIGHTNING_ROD).setWeight(10))
                                        .add(LootItem.lootTableItem(net.minecraft.world.level.block.Blocks.SOUL_SAND).setWeight(20))
                                        .add(LootItem.lootTableItem(net.minecraft.world.level.block.Blocks.HOPPER).setWeight(15))
                                        .add(LootItem.lootTableItem(ItemRegistry.XION_LENS.get()).setWeight(5))
                                        .add(LootItem.lootTableItem(ItemRegistry.CIRCUIT_PLATE.get()).setWeight(10))
                                        .add(LootItem.lootTableItem(Items.REDSTONE).setWeight(25))
                                        .add(LootItem.lootTableItem(Items.GUNPOWDER).setWeight(20))
                                        .add(LootItem.lootTableItem(Items.AMETHYST_SHARD).setWeight(10))
                                        .add(LootItem.lootTableItem(Items.LEVER).setWeight(30))
                                        .add(TagEntry.expandTag(ItemTags.BUTTONS).setWeight(40))
                                        .add(LootItem.lootTableItem(Items.REPEATER).setWeight(10))
                                        .add(LootItem.lootTableItem(Items.OBSERVER).setWeight(10))
                                        .add(LootItem.lootTableItem(Items.COMPARATOR).setWeight(10))
                            ).withPool(LootPool.lootPool()
                                    .setRolls(ConstantValue.exactly(2))
                                    .name("treasure")
                                    .add(LootItem.lootTableItem(Items.NETHERITE_SCRAP).setWeight(2))
                                    .add(LootItem.lootTableItem(Items.COPPER_INGOT).setWeight(5))
                                    .add(LootItem.lootTableItem(Items.IRON_INGOT).setWeight(10))
                                    .add(LootItem.lootTableItem(Items.ENDER_PEARL).setWeight(5))
                                    .add(LootItem.lootTableItem(ItemRegistry.EXO_CIRCUITS.get()).setWeight(10))
                                    .add(TagEntry.expandTag(net.tardis.mod.tags.ItemTags.TARDIS_MUSIC_DISK))
                            )

                    );
            builder.accept(Helper.createRL("vortex_phenomena/astroid"), LootTable.lootTable()
                    .withPool(LootPool.lootPool()
                                .setRolls(ConstantValue.exactly(7))
                                .name("normal")
                                .add(TagEntry.expandTag(Tags.Items.RAW_MATERIALS))
                    )
            );
        }
    }
}
