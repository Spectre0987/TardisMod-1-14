package net.tardis.mod.datagen;

import net.minecraft.core.Direction;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.PackType;
import net.minecraft.util.Mth;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.Half;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraft.world.level.block.state.properties.WallSide;
import net.minecraftforge.client.model.generators.*;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.block.*;
import net.tardis.mod.block.machines.AlembicBlock;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.BlockRegistry;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TardisBlockStateProvider extends BlockStateProvider {

    public final ExistingFileHelper fileHelper;
    public final String modid;
    private final BlockModelProvider blockModels;

    public TardisBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper, String modid) {
        super(gen.getPackOutput(), modid, exFileHelper);
        this.fileHelper = exFileHelper;
        this.modid = modid;
        this.blockModels = new TardisBlockModelProvider(gen.getPackOutput(), exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
        this.horizontalBlock(BlockRegistry.DRAFTING_TABLE.get(), createExistingFile(BlockRegistry.DRAFTING_TABLE));
        this.horizontalBlock(BlockRegistry.STEAM_MONITOR.get(), state ->
                state.getValue(BlockStateProperties.HANGING) ?
                        createExistingFile("block/machines/monitor/steampunk_monitor_hanging") :
                        createExistingFile("block/machines/monitor/steampunk_monitor")
                );
        this.randomModelBlock(BlockRegistry.XION.get(),
                createExistingFile("block/resources/xion/xion_crystal_1"),
                createExistingFile("block/resources/xion/xion_crystal_2"),
                createExistingFile("block/resources/xion/xion_crystal_3"),
                createExistingFile("block/resources/xion/xion_crystal_4"),
                createExistingFile("block/resources/xion/xion_crystal_5"),
                createExistingFile("block/resources/xion/xion_crystal_6")
        );
        this.simpleBlockWithItem(BlockRegistry.XION_BLOCK.get());
        this.horizontalBlock(BlockRegistry.ALEMBIC.get(), state -> createExistingFile(state.getValue(AlembicBlock.ON) ? "block/machines/alembic_on" : "block/machines/alembic_off"));
        this.horizontalBlockWithItem(BlockRegistry.FOAM_PIPE.get(), state -> cubeAll(BlockRegistry.FOAM_PIPE.get()), 90);
        this.simpleBlockWithItem(BlockRegistry.METAL_GRATE.get(), cubeAll(BlockRegistry.METAL_GRATE.getId()).renderType("translucent"));
        this.simpleBlockWithItem(BlockRegistry.METAL_GRATE_SOLID.get());
        this.transparentSlab(BlockRegistry.METAL_GRATE_SLAB.get(),
                Helper.createRL("block/decoration/metal_grate_slab_side"),
                Helper.createRL("block/decoration/metal_grate"), true);
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN.get());
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_PLATE.get());
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_PIPES.get());
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_PATTERN.get());
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_RUNNING_LIGHTS.get(), cubeAll(Helper.createRL("decoration/tungsten/tungsten_blue_runner_lights")));
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_PLATE_SMALL.get());
        this.transparentSlab(BlockRegistry.TUNGSTEN_PLATE_SLAB.get(),
                blockTexture(BlockRegistry.TUNGSTEN_PLATE_SLAB.get()),
                blockTexture(BlockRegistry.TUNGSTEN_PLATE_SLAB.get()), false);
        this.simpleBlockWithItem(BlockRegistry.WIRE_HANG.get(), this.models()
                .cross(createBlockRL(BlockRegistry.WIRE_HANG).getPath(),
                        createBlockRL(BlockRegistry.WIRE_HANG)
                ).renderType("translucent"));
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_SCREEN.get());
        generateFluid(BlockRegistry.MERCURY_FLUID.get(), Helper.createRL("block/mercury_still"));
        generateFluid(BlockRegistry.BIOMASS_FLUID.get(), Helper.createRL("block/mercury_still"));
        this.simpleBlockWithItem(BlockRegistry.DATA_MATRIX.get(), createExistingFile("block/resources/data_matrix"));
        simpleBlockWithItem(BlockRegistry.ALABASTER.get(),
                this.cubeAll(BlockRegistry.ALABASTER.getId(), Helper.createRL("block/decoration/alabaster/alabaster")));
        simpleBlockWithItem(BlockRegistry.ALABASTER_TILES_LARGE.get(),
                this.cubeAll(BlockRegistry.ALABASTER_TILES_LARGE.getId(), Helper.createRL("block/decoration/alabaster/alabaster_largetiles")));
        simpleBlockWithItem(BlockRegistry.ALABASTER_TILES_SMALL.get(), this.cubeAll(BlockRegistry.ALABASTER_TILES_SMALL.getId(),
                Helper.createRL("block/decoration/alabaster/alabaster_smalltiles")));
        this.simpleBlockWithItem(BlockRegistry.ALABASTER_PILLAR.get(), models().cubeColumn(createBlockRL(BlockRegistry.ALABASTER_PILLAR).getPath(),
                    Helper.createRL("block/decoration/alabaster/alabaster_pillar"),
                    Helper.createRL("block/decoration/alabaster/alabaster_pillar_top")
                ));
        this.horizontalBlock(BlockRegistry.ARS_PANEL.get(), createExistingFile("block/machines/ars_panel"));
        this.horizontalBlock(BlockRegistry.TECH_STRUT.get(), state -> {
            if(state.getValue(TechStrutBlock.STRUT_TYPE) == TechStrutBlock.StrutType.BOTTOM){
                return createExistingFile("block/decoration/tech_strut_floor");
            }
            if(state.getValue(TechStrutBlock.STRUT_TYPE) == TechStrutBlock.StrutType.TOP){
                return createExistingFile("block/decoration/tech_strut_upper");
            }
            return createExistingFile("block/decoration/tech_strut_wall");
        });
        this.stairsBlock(BlockRegistry.TUNGSTEN_STAIRS.get(), Helper.createRL("block/decoration/tungsten/tungsten_plate"));
        this.stairsBlock(BlockRegistry.TUNGSTEN_STAIRS_SMALL_PLATES.get(), Helper.createRL("block/decoration/tungsten/tungsten_plate_small"));
        this.horizontalBlock(BlockRegistry.RCA_MONITOR.get(), new ModelFile.ExistingModelFile(Helper.createRL("block/machines/monitor/rca_monitor"), this.blockModels.existingFileHelper));
        this.horizontalBlock(BlockRegistry.EYE_MONITOR.get(), new ModelFile.ExistingModelFile(Helper.createRL("block/machines/monitor/eye_monitor"), this.blockModels.existingFileHelper));
        this.simpleBlockWithItem(BlockRegistry.BLINKING_LIGHTS.get(), this.cubeAll(BlockRegistry.BLINKING_LIGHTS.getId(), Helper.createRL("block/decoration/blinking_lights")));
        this.stairsBlock(BlockRegistry.ALABASTER_STAIRS.get(), Helper.createRL("block/decoration/alabaster/alabaster"));
        this.horizontalBlock(BlockRegistry.HOLO_LADDER.get(), new ModelFile.ExistingModelFile(Helper.createRL("block/decoration/holo_ladders"), this.fileHelper));
        this.horizontalBlock(BlockRegistry.STEAM_GRATE.get(), new ModelFile.ExistingModelFile(Helper.createRL("block/decoration/steam_grate"), this.fileHelper));
        this.wallBlock(BlockRegistry.ALABASTER_WALL.get(), Helper.createRL("block/decoration/alabaster/alabaster"));
        this.trapdoorBlock(BlockRegistry.METAL_GRATE_TRAPDOOR.get(), Helper.createRL("block/decoration/metal_grate"), false, "cutout");
        this.transparentSlab(BlockRegistry.ALABASTER_SLAB.get(),
                Helper.createRL("block/decoration/alabaster/alabaster"),
                Helper.createRL("block/decoration/alabaster/alabaster"),false);
        this.horizontalBlock(BlockRegistry.COMFY_CHAIR_RED.get(),
                new ModelFile.ExistingModelFile(Helper.createRL("block/chairs/comfy_chair"), this.fileHelper));
        this.horizontalBlock(BlockRegistry.COMFY_CHAIR_GREEN.get(), models()
                .getBuilder("block/chairs/" + toRl(BlockRegistry.COMFY_CHAIR_GREEN.get()))
                .parent(new ModelFile.ExistingModelFile(Helper.createRL("block/chairs/comfy_chair"), this.fileHelper))
                .texture("color", Helper.createRL("block/decoration/chair/comfy/green"))
        );
        this.wallBlock(BlockRegistry.ALABASTER_PILLAR_WALL.get(), Helper.createRL("block/decoration/alabaster/alabaster_pillar"));
        this.horizontalBlockWithItem(BlockRegistry.ALABASTER_TILES_BIG.get(), state -> this.cubeAll(BlockRegistry.ALABASTER_TILES_BIG.getId(), Helper.createRL("block/decoration/alabaster/alabaster_big_tiles")), 90);
        this.generateTopBottomWithItem(BlockRegistry.ALABASTER_BOOKSHELF.get(),
                Helper.createRL("block/decoration/alabaster/alabaster_bookshelf"),
                Helper.createRL("block/decoration/alabaster/alabaster")
        );
        this.generateTopBottomWithItem(BlockRegistry.ALABASTER_SCREEN.get(),
                Helper.createRL("block/decoration/alabaster/alabaster_screen"),
                Helper.createRL("block/decoration/alabaster/alabaster")
        );
        this.generateTopBottomWithItem(BlockRegistry.ALABASTER_RUNNER_LIGHTS.get(),
                Helper.createRL("block/decoration/alabaster/alabaster_blue_runner_lights"),
                Helper.createRL("block/decoration/alabaster/alabaster")
        );
        this.generateTopBottomWithItem(BlockRegistry.ALABASTER_PIPES.get(),
                Helper.createRL("block/decoration/alabaster/alabaster_pipes"),
                Helper.createRL("block/decoration/alabaster/alabaster")
        );
        this.simpleBlockWithItem(BlockRegistry.STEEL_HEX_OFFSET.get(),
                Helper.createRL("block/decoration/steel_hexwall_offsetopen"), "cutout");
        this.simpleBlockWithItem(BlockRegistry.STEEL_HEX.get(),
                Helper.createRL("block/decoration/steel_hexwall_open"), "cutout");
        this.generateTopBottomWithItem(BlockRegistry.EBONY_BOOKSHELF.get(),
                Helper.createRL("block/decoration/ebony_bookshelf"),
                Helper.createRL("block/decoration/ebony_bookshelf_top")
        );
        this.simpleBlockWithItem(BlockRegistry.MOON_BASE_GLASS.get(), Helper.createRL("block/decoration/moon_base_glass"), "translucent");
        this.simpleBlockWithItem(BlockRegistry.ARS_EGG.get(), getExistingModel("block/machines/ars_egg"));
        this.horizontalBlockWithItem(BlockRegistry.DEDICATION_PLAQUE.get(), state -> getExistingModel("block/decoration/plaque"));
        this.horizontalBlockWithItem(BlockRegistry.QUANTISCOPE.get(), state -> getExistingModel("block/machines/quantiscope"));
        this.simpleBlockWithItem(BlockRegistry.RIFT_PYLON.get(), getExistingModel("block/machines/artron_pylon"));
        this.simpleBlockWithItem(BlockRegistry.RIFT_COLLECTOR.get(), getExistingModel("block/machines/artron_collector"));
        this.horizontalBlockWithItem(BlockRegistry.MATTER_BUFFER.get(), state -> getExistingModel("block/machines/neutronic_spectrometer"));
        this.simpleBlockWithItem(BlockRegistry.SCOOP_VAULT.get(), getExistingModel("block/machines/temporal_scoop_vault"));
        this.horizontalBlockWithItem(BlockRegistry.WAYPOINT_BANKS.get(), state -> state.getValue(WaypointBlock.IS_TOP) ?
                new TardisItemModelProvider.BuiltinItemModel() : getExistingModel("block/machines/waypoint_bank"));
        this.generateTopBottomWithItem(BlockRegistry.ATRIUM_PLATFORM_BLOCK.get(), Helper.createRL("block/machines/atriumfloor_side"), Helper.createRL("block/machines/atriumfloor_top"));
        this.horizontalBlockWithItem(BlockRegistry.ITEM_PLAQUE.get(), state -> getExistingModel("block/decoration/tardis_trophy"));
        this.simpleBlockWithItem(BlockRegistry.ATRIUM_MASTER.get(), getExistingModel("block/machines/atrium_arch"));
        this.simpleBlockWithItem(BlockRegistry.ALABASTER_GRATE.get(), cubeAll(Helper.createRL("decoration/alabaster/alabaster_grate")).renderType("translucent"));
        this.simpleBlockWithItem(BlockRegistry.TUNGSTEN_GRATE.get(), cubeAll(Helper.createRL("decoration/tungsten/tungsten_grate")).renderType("translucent"));
        this.allFacingBlockWithItem(BlockRegistry.ROUNDEL_TAP.get(), state -> getExistingModel("block/machines/roundel_tap"));
        this.simpleBlockWithItem(BlockRegistry.LANDING_PAD.get(), models().cubeBottomTop(
                "landing_pad",
                Helper.createRL("block/machines/airlock_bottom"),
                Helper.createRL("block/machines/tech_floor_dark_plate"),
                Helper.createRL("block/machines/landing_pad")
        ));
        this.transparentSlab(BlockRegistry.TUNGSTEN_SMALL_PLATE_SLAB.get(),
                Helper.createRL("block/decoration/tungsten/tungsten_plate_small"),
                Helper.createRL("block/decoration/tungsten/tungsten_plate_small"), false
        );
        this.horizontalBlockWithItem(BlockRegistry.VARIABLE_MONITOR.get(), state -> getExistingModel("block/machines/monitor/dynamic"));
        this.simpleBlockWithItem(BlockRegistry.CORAL_BLANK.get(), cubeAll(Helper.createRL("decoration/tardis_coral_block")));
        this.stairsBlock(BlockRegistry.CORAL_STAIRS.get(), Helper.createRL("block/decoration/tardis_coral_block"));
        this.transparentSlab(BlockRegistry.CORAL_SLAB.get(),
                Helper.createRL("block/decoration/tardis_coral_block"),
                Helper.createRL("block/decoration/tardis_coral_block"), false);
        this.horizontalBlockWithItem(BlockRegistry.ALEMBIC_STILL.get(), state -> getExistingModel("block/machines/alembic_still"));
        this.wallBlock(BlockRegistry.CORAL_WALL.get(), Helper.createRL("block/decoration/tardis_coral_block"));
        this.wallBlock(BlockRegistry.METAL_GRATE_WALL.get(), Helper.createRL("block/decoration/metal_grate_03"), false);
        this.generateRailing(BlockRegistry.RAILING_STEAM.get(), Helper.createRL("block/decoration/railings/railing_steam"));
        this.generateRailing(BlockRegistry.RAILING_TUNGSTEN.get(), Helper.createRL("block/decoration/railings/railing_tungsten"));
        this.generateRailing(BlockRegistry.RAILING_ALABASTER.get(), Helper.createRL("block/decoration/railings/railing_alabaster"));


        this.generateEmpty(BlockRegistry.MULTIBLOCK_REDIR_FLUID);
        this.generateEmpty(BlockRegistry.G8_CONSOLE);
        this.generateEmpty(BlockRegistry.STEAM_CONSOLE);
        this.generateEmpty(BlockRegistry.STEAM_EXTERIOR);
        this.generateEmpty(BlockRegistry.MULTIBLOCK);
        this.generateEmpty(BlockRegistry.TT_CAPSULE);
        this.generateEmpty(BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR);
        this.generateEmpty(BlockRegistry.CORRIDOR_STRUCTURE_SAVER);
        this.generateEmpty(BlockRegistry.NEUVEAU_CONSOLE);
        this.generateEmpty(BlockRegistry.INTERIOR_DOOR);
        this.generateEmpty(BlockRegistry.STEAM_BROKEN_EXTERIOR);
        this.generateEmpty(BlockRegistry.EXTERIOR_COLLISION);
        this.generateEmpty(BlockRegistry.ENGINE_STEAM);
        this.generateEmpty(BlockRegistry.ENGINE_ROOF, true);
        this.generateEmpty(BlockRegistry.POLICE_BOX_EXTERIOR);
        this.generateEmpty(BlockRegistry.GALVANIC_CONSOLE);
        this.generateEmpty(BlockRegistry.NEMO_CONSOLE);
        this.generateEmpty(BlockRegistry.CHAMELEON_AUXILLARY);
        this.generateEmpty(BlockRegistry.CHAMELEON_EXTERIOR);
        this.generateEmpty(BlockRegistry.DISGUISED_BLOCK);
        this.generateEmpty(BlockRegistry.BIG_DOOR);
        //this.generateEmpty(BlockRegistry.MULTIBLOCK_REDIR_FLUID);
        this.generateEmpty(BlockRegistry.RENDERER_TEST);
        this.generateEmpty(BlockRegistry.FABRICATOR_MACHINE);
        this.generateEmpty(BlockRegistry.TELEPORT_TUBE);
        this.generateEmpty(BlockRegistry.COFFIN_EXTERIOR);
        this.generateEmpty(BlockRegistry.TRUNK_EXTERIOR);
        this.generateEmpty(BlockRegistry.SPRUCE_EXTERIOR);

        ForgeRegistries.BLOCKS.getKeys().stream().filter(rl -> {
            if(!rl.getNamespace().equals(this.modid))
                return false;
            Block block = ForgeRegistries.BLOCKS.getValue(rl);
            if(!(block instanceof RoundelBlock))
                return false;
            if(this.fileHelper.exists(rl, PackType.CLIENT_RESOURCES))
                return false;
            return true;
        }).forEach(rl -> {

            final Block roundel = ForgeRegistries.BLOCKS.getValue(rl);
            simpleBlock(roundel, cubeAll(rl));

        });
    }

    public void generateRailing(RailingBlock block, ResourceLocation texture){
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block).withPrefix("block/");

        final BlockModelBuilder flat = this.models().getBuilder(id.getPath())
                .parent(getExistingModel("block/decoration/stair_rail"))
                .texture("text", texture.toString());

        final BlockModelBuilder angle = this.models().getBuilder(id.getPath() + "_angled")
                .parent(getExistingModel("block/decoration/stair_rail_angled"))
                .texture("text", texture.toString());

        final BlockModelBuilder flat_right = this.models().getBuilder(id.getPath() + "_right")
                .parent(getExistingModel("block/decoration/stair_rail_right"))
                .texture("text", texture.toString());

        final BlockModelBuilder angle_right = this.models().getBuilder(id.getPath() + "_angled_right")
                .parent(getExistingModel("block/decoration/stair_rail_angled_right"))
                .texture("text", texture.toString());

        this.horizontalBlockWithItem(block, state -> {

            final boolean is_angled = state.getValue(RailingBlock.IS_ANGLED);

            if(state.getValue(RailingBlock.RIGHT)) {
                return is_angled ? angle_right : flat_right;
            }
            return is_angled ? angle : flat;
        });
    }

    private void allFacingBlockWithItem(Block block, Function<BlockState, ModelFile> modelFunc) {

        BiFunction<ConfiguredModel.Builder<?>, BlockState, ConfiguredModel.Builder> rotation = (builder, blockState) -> {
            if(blockState.hasProperty(BlockStateProperties.FACING)){
                final Direction facing = blockState.getValue(BlockStateProperties.FACING);
                if(facing.getAxis().isHorizontal()) {
                    builder = builder.rotationY(Mth.floor(facing.toYRot() - 180))
                            .rotationX(90);
                }
                else builder = builder.rotationX(Mth.floor(
                        facing == Direction.UP ? 0 : 180
                ));
            }
            return builder;
        };

        this.getVariantBuilder(block)
                .forAllStates(state -> rotation.apply(ConfiguredModel.builder()
                                .modelFile(modelFunc.apply(state)), state)
                        .build());
        this.itemModels().getBuilder("item/" + ForgeRegistries.ITEMS.getKey(block.asItem()).getPath())
                .parent(modelFunc.apply(block.defaultBlockState()));

    }

    public void horizontalBlockWithItem(Block block, Function<BlockState, ModelFile> existingModel, int angleOffset) {
        final ResourceLocation key = ForgeRegistries.BLOCKS.getKey(block);
        horizontalBlock(block, existingModel, angleOffset);

        this.itemModels().getBuilder("item/" + key.getPath())
                .parent(existingModel.apply(block.defaultBlockState()));
    }

    public void horizontalBlockWithItem(Block block, Function<BlockState, ModelFile> existingModel){
        this.horizontalBlockWithItem(block, existingModel, 180);
    }

    public ModelFile.ExistingModelFile getExistingModel(String path){
        return new ModelFile.ExistingModelFile(new ResourceLocation(this.modid, path), this.fileHelper);
    }

    @Override
    public void simpleBlock(Block block) {
        this.simpleBlock(block, cubeAll(block));
    }

    @Override
    public void stairsBlock(StairBlock block, ResourceLocation texture) {
        this.stairsBlock(block, "block/" + toRl(block), texture);
    }

    public void simpleBlockWithItem(Block block, ResourceLocation texture, String renderType){
        BlockModelBuilder model = this.cubeAll(ForgeRegistries.BLOCKS.getKey(block), texture)
                .renderType(renderType);
        simpleBlockWithItem(block, model);
    }

    public void simpleBlockWithItem(Block block, ResourceLocation tex){
        simpleBlockWithItem(block, tex, "solid");
    }

    public void simpleBlockWithItem(Block block, ModelFile model) {
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block);
        simpleBlock(block, model);
        itemModels().getBuilder(ModelProvider.ITEM_FOLDER + "/" + id.getPath())
                .parent(model);
    }

    public void generateFluid(LiquidBlock block, ResourceLocation texture){
        final String path = toRl(block);
        this.simpleBlock(block, this.models().getBuilder(path).texture("particle", texture));
    }

    public void simpleBlockWithItem(Block block){
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block);
        this.simpleBlock(block);
        this.itemModels()
                .getBuilder(ModelProvider.ITEM_FOLDER + "/" + id.getPath())
                .parent(new ModelFile.ExistingModelFile(
                        id.withPrefix(ModelProvider.BLOCK_FOLDER + "/"),
                        this.fileHelper));
    }

    public String toRl(Block block){
        return ForgeRegistries.BLOCKS.getKey(block).getPath();
    }

    public void transparentSlab(SlabBlock block, ResourceLocation sides, ResourceLocation topBottom, boolean isTransparent){
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block);
        final ResourceLocation idWithPath = id.withPrefix(ModelProvider.BLOCK_FOLDER + "/");
        final String renderType = isTransparent ? "translucent" : "solid";

        ModelBuilder<?> topModel = this.models().slabTop(idWithPath.getPath() + "_top", sides, topBottom, topBottom)
                .renderType(renderType);
        ModelFile bottomModel = this.models().slab(idWithPath.getPath(), sides, topBottom, topBottom)
                .renderType(renderType);
        ModelFile doubleModel = cubeAll(id.withSuffix("_double"), topBottom).renderType(renderType);

        Function<BlockState, ModelFile> modelFunc = state -> {
            switch(state.getValue(SlabBlock.TYPE)){
                case TOP: return topModel;
                case BOTTOM: return bottomModel;
                default: return doubleModel;
            }
        };
        //Build State
        this.getVariantBuilder(block)
                .forAllStates(state -> ConfiguredModel.builder()
                        .modelFile(modelFunc.apply(state))
                        .build()
                );
        //Build Item Model
        this.itemModels().getBuilder(ModelProvider.ITEM_FOLDER + "/" + id.getPath())
                .parent(bottomModel);
    }

    public void generateTopBottomWithItem(Block block, ResourceLocation sides, ResourceLocation top){
        final ResourceLocation key = ForgeRegistries.BLOCKS.getKey(block);
        BlockModelBuilder model = models().cubeColumn(ModelProvider.BLOCK_FOLDER + "/" + key.getPath(),
                sides, top);

        this.simpleBlock(block, model);
        this.generateBlockItem(block, model);

    }

    public void generateBlockItem(Block block, ModelFile modelFile){
        final ResourceLocation key = ForgeRegistries.BLOCKS.getKey(block);
        this.itemModels().getBuilder(ModelProvider.ITEM_FOLDER + "/" + key.getPath())
                .parent(modelFile);
    }

    public void trapdoorBlock(TrapDoorBlock block, ResourceLocation texture, boolean orientable, String renderType) {

        final ResourceLocation key = ForgeRegistries.BLOCKS.getKey(block);

        final BlockModelBuilder topModel = models().trapdoorTop(ModelProvider.BLOCK_FOLDER + "/" + key.getPath() + "_top", texture)
                .renderType(renderType);
        final BlockModelBuilder bottomModel = models().trapdoorBottom(ModelProvider.BLOCK_FOLDER + "/" + key.getPath() + "_bottom", texture)
                .renderType(renderType);
        final BlockModelBuilder openModel = models().trapdoorOpen(ModelProvider.BLOCK_FOLDER + "/" + key.getPath() + "_open", texture)
                .renderType(renderType);

        Function<BlockState, BlockModelBuilder> modelFunc = state -> {
            if(state.getValue(TrapDoorBlock.OPEN))
                return openModel;
            if(state.getValue(TrapDoorBlock.HALF) == Half.BOTTOM)
                return bottomModel;
            return topModel;
        };

        //generate state
        this.getVariantBuilder(block).forAllStates(state -> ConfiguredModel.builder()
                .rotationY(Mth.floor(state.getValue(TrapDoorBlock.FACING).toYRot() - 180))
                .modelFile(modelFunc.apply(state))
                .build()
        );

        //Generate Item
        this.itemModels().getBuilder(ModelProvider.ITEM_FOLDER + "/" + key.getPath()).parent(bottomModel);

    }

    @Override
    public void wallBlock(WallBlock block, ResourceLocation texture) {
        this.wallBlock(block, texture, true);
    }

    public void wallBlock(WallBlock block, ResourceLocation texture, boolean solid) {
        String path = ModelProvider.BLOCK_FOLDER + "/" + toRl(block);

        BlockModelBuilder postModel = this.models().wallPost(path + "_post", texture).renderType(solid ? "solid" : "cutout");
        BlockModelBuilder sideModel = this.models().wallSide(path + "_side", texture).renderType(solid ? "solid" : "cutout");
        BlockModelBuilder sideTallModel = this.models().wallSideTall(path + "_tall", texture).renderType(solid ? "solid" : "cutout");

        MultiPartBlockStateBuilder builder = this.getMultipartBuilder(block)
                .part()
                .modelFile(postModel).uvLock(true).addModel()
                .condition(WallBlock.UP, true)
                .end();

        for(Map.Entry<Direction, Property<WallSide>> entry : WALL_PROPS.entrySet()){
            if(entry.getKey().getAxis().isHorizontal()){
                builder = wallSide(builder, sideModel, entry.getKey(), WallSide.LOW);
                builder = wallSide(builder, sideTallModel, entry.getKey(), WallSide.TALL);
            }
        }

        this.itemModels().getBuilder(ModelProvider.ITEM_FOLDER + "/" + toRl(block))
                .parent(itemModels().wallInventory(path, texture));

    }

    public MultiPartBlockStateBuilder wallSide(MultiPartBlockStateBuilder builder, ModelFile model, Direction dir, WallSide height){
        return builder.part()
                .modelFile(model)
                .uvLock(true)
                .rotationY((int) dir.toYRot() - 180 % 360)
                .addModel()
                    .condition(WALL_PROPS.get(dir), height)
                    .end();
    }

    @Override
    public BlockModelProvider models() {
        return this.blockModels;
    }

    public BlockModelBuilder cubeAll(ResourceLocation rl, ResourceLocation tex){
        final ResourceLocation fullPath = rl.withPrefix(ModelProvider.BLOCK_FOLDER + "/");
        BlockModelBuilder builder = models().cubeAll(fullPath.getPath(), tex);
        return builder;
    }

    public BlockModelBuilder cubeAll(ResourceLocation rl){
        return cubeAll(rl,
                rl.withPrefix("block/"));
    }



    public ModelFile cubeAll(Block block){
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block);
        return cubeAll(id);
    }

    public ResourceLocation createBlockRL(RegistryObject<? extends Block> block){
        ResourceLocation loc = block.getId();
        return new ResourceLocation(loc.getNamespace(), "block/" + loc.getPath());
    }

    public ModelFile createExistingFile(RegistryObject<? extends Block> reg){
        return new ModelFile.ExistingModelFile(createBlockRL(reg), this.fileHelper);
    }

    public ModelFile createExistingFile(String path){
        return new ModelFile.ExistingModelFile(Helper.createRL(path), this.fileHelper);
    }

    public void generateEmpty(RegistryObject<? extends Block> reg, boolean withItem){
        this.simpleBlock(reg.get(), new TardisItemModelProvider.BuiltinModel());
        if(withItem){
            this.itemModels().getBuilder(reg.getId().getPath()).parent(new TardisItemModelProvider.BuiltinModel());
        }
    }

    public void generateEmpty(RegistryObject<? extends Block> reg){
        this.generateEmpty(reg, false);
    }

    public String path(Block block){
        final ResourceLocation id = ForgeRegistries.BLOCKS.getKey(block);
        return ModelProvider.BLOCK_FOLDER + "/" + id.getPath();
    }

    public void randomModelBlock(Block block, ModelFile... files){

        ConfiguredModel[] configuredModels = new ConfiguredModel[files.length];
        for(int i = 0; i < files.length; ++i){
            configuredModels[i] = new ConfiguredModel(files[i]);
        }

        getVariantBuilder(block)
                .partialState()
                .addModels(configuredModels);
    }
}
