package net.tardis.mod.datagen;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.damagesource.DamageType;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.Tardis;
import net.tardis.mod.tags.DamageSourceTags;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

public class TardisDamageTypeTagsProvider extends TagsProvider<DamageType> {

    public TardisDamageTypeTagsProvider(PackOutput pOutput, CompletableFuture<HolderLookup.Provider> pLookupProvider, @Nullable ExistingFileHelper existingFileHelper) {
        super(pOutput, Registries.DAMAGE_TYPE, pLookupProvider, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider tags) {
        this.tag(DamageSourceTags.temporalGraceBlocks)
                .addTags(DamageTypeTags.IS_DROWNING)
                .addTags(DamageTypeTags.IS_FIRE)
                .addTags(DamageTypeTags.IS_FALL);
    }
}
