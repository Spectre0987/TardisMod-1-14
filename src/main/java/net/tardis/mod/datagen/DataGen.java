package net.tardis.mod.datagen;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.datafixers.util.Pair;
import net.minecraft.core.*;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.DatapackBuiltinEntriesProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.item.misc.TardisTool;
import net.tardis.mod.loot.SchematicLootTable;
import net.tardis.mod.misc.PhasedShellDisguise;
import net.tardis.mod.misc.TardisDimensionInfo;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.registry.ExteriorRegistry;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.sound.SoundScheme;
import net.tardis.mod.tags.BiomeTags;
import net.tardis.mod.world.TardisBiomeModifier;
import net.tardis.mod.world.WorldRegistry;
import net.tardis.mod.world.features.BrokenTardisFeatureConfig;
import net.tardis.mod.world.features.configs.XionFeatureConfig;
import net.tardis.mod.world.placements.OnBlockPlacement;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    public static final List<RegistrySet> DATAPACK_SET = new ArrayList<>();

    @SubscribeEvent
    public static void registerDataGen(GatherDataEvent event){
        event.getGenerator().addProvider(event.includeServer(), new TardisExportAllBlockIds(event.getGenerator().getPackOutput(), event.getLookupProvider()));
        event.getGenerator().addProvider(event.includeServer(), new TardisAdvancementProvider(event.getGenerator().getPackOutput(), event.getLookupProvider(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeClient(), new TardisLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(event.includeClient(), new TardisBlockStateProvider(event.getGenerator(), event.getExistingFileHelper(), Tardis.MODID));
        event.getGenerator().addProvider(event.includeClient(), new TardisItemModelProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeServer(), new TardisRecipeProvider(event.getGenerator()));
        event.getGenerator().addProvider(event.includeServer(), new TardisLootModiferProvider(event.getGenerator().getPackOutput()));
        event.getGenerator().addProvider(event.includeServer(), new TextureVariantProvider(event.getGenerator()));
        final TardisBlockTagsProvider blocktagsProvider = new TardisBlockTagsProvider(event.getGenerator().getPackOutput(),
                event.getLookupProvider(),
                event.getExistingFileHelper());
        event.getGenerator().addProvider(event.includeServer(), blocktagsProvider);
        event.getGenerator().addProvider(event.includeServer(), new TardisItemTagsProvider(
                event.getGenerator().getPackOutput(),
                event.getLookupProvider(),
                blocktagsProvider.contentsGetter(),
                event.getExistingFileHelper()
        ));
        event.getGenerator().addProvider(event.includeServer(), new TardisSoundProvider(event.getGenerator().getPackOutput(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeServer(), new TardisLootProvider(event.getGenerator().getPackOutput()));
        event.getGenerator().addProvider(event.includeClient(), new AnimatedBakedTextureProvider(event.getGenerator(), Tardis.MODID, event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeServer(), new TardisItemArtronDataProvider(event.getGenerator()));
        event.getGenerator().addProvider(event.includeServer(), new TardisBiomeTagProvider(event.getGenerator().getPackOutput(), event.getLookupProvider(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeClient(), new TardisManualProvider(event.getGenerator()));
        event.getGenerator().addProvider(event.includeServer(), new TardisBlockAsLootProvider(event.getGenerator()));
        event.getGenerator().addProvider(event.includeServer(), new TardisDamageTypeTagsProvider(event.getGenerator().getPackOutput(), event.getLookupProvider(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(event.includeServer(), new LandUnlockerProvider(event.getGenerator(), event.getLookupProvider()));
        event.getGenerator().addProvider(event.includeServer(), new TardisTraitProvider(event.getGenerator()));

        final JsonReg<ARSRoom> roomReg = json(JsonRegistries.ARS_ROOM_REGISTRY)
                .add("rooms/tardis_grid_steam", rl -> new ARSRoom(rl, ARSRoom.Type.ROOM, true, -15,
                            Optional.empty(), new ArrayList<>(), false
                        ))
                .add("rooms/tardis_grid_steam_abandoned", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withParent(Helper.createRL("rooms/tardis_grid_steam"))
                        .addUnlock(BlockRegistry.STEAM_CONSOLE.get())
                        .addUnlock(ExteriorRegistry.STEAM.getKey())
                        .withOffset(-15)
                        .build())
                .add("rooms/tardis_grid_alabaster", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withOffset(-7)
                        .setNoSpawn()
                        .build())
                .add("rooms/tardis_grid_alabaster_abandoned", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withParent(Helper.createRL("rooms/tardis_grid_alabaster"))
                        .addUnlock(BlockRegistry.G8_CONSOLE.get())
                        .withOffset(-7)
                        .build())
                .add("rooms/tardis_grid_nautilus", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withOffset(-17)
                        .setNoSpawn()
                        .build())
                .add("rooms/tardis_grid_nautilus_abandoned", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withParent(Helper.createRL("rooms/tardis_grid_nautilus"))
                        .addUnlock(BlockRegistry.NEMO_CONSOLE.get())
                        .withOffset(-17)
                        .build())
                .add("rooms/tardis_grid_arstree", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .setNoSpawn()
                        .withOffset(-17)
                        .build())
                .add("rooms/tardis_grid_arstree_abandoned", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withParent(Helper.createRL("rooms/tardis_grid_arstree"))
                        .withOffset(-17)
                        .build())
                .add("rooms/tardis_grid_nouveau", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withOffset(-5)
                        .setNoSpawn()
                        .build())
                .add("rooms/tardis_grid_nouveau_abandoned", rl -> new ARSRoom.Builder(rl, ARSRoom.Type.ROOM)
                        .withParent(Helper.createRL("rooms/tardis_grid_nouveau"))
                        .withOffset(-5)
                        .addUnlock(BlockRegistry.NEUVEAU_CONSOLE.get())
                        .build());
        addRoomWithRepair(roomReg, Helper.createRL("rooms/tardis_grid_archive"), ARSRoom.Type.ROOM, build ->
                build.withOffset(-8)
        );
        addRoomWithRepair(roomReg, Helper.createRL("rooms/tardis_grid_arbor"), ARSRoom.Type.ROOM, build ->
                build.withOffset(-14)
        );
        addRoomWithRepair(roomReg, Helper.createRL("rooms/tardis_grid_atrium"), ARSRoom.Type.ROOM, build ->
                build.withOffset(-11)
        );
        addRoomWithRepair(roomReg, Helper.createRL("rooms/tardis_grid_eye"), ARSRoom.Type.ROOM, build ->
                build.withOffset(-17)
        );

        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_01"), ARSRoom.Type.CORRIDOR, build -> {});
        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_02"), ARSRoom.Type.CORRIDOR, build ->
                build.withOffset(-1)
        );
        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_03"), ARSRoom.Type.CORRIDOR, build ->
                build.withOffset(-15)
        );
        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_04"), ARSRoom.Type.CORRIDOR, build ->
                build.withOffset(-8)
        );
        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_05"), ARSRoom.Type.CORRIDOR, build ->
                build.withOffset(0)
        );
        addRoomWithRepair(roomReg, Helper.createRL("corridor/tardis_grid_06"), ARSRoom.Type.CORRIDOR, build -> {});
        addRoomWithRepair(roomReg, Helper.createRL("rooms/tardis_grid_fab_01"), ARSRoom.Type.ROOM, builder -> {
            builder.withOffset(-17);
        });


        json(JsonRegistries.TARDIS_DIM_INFO)
                .add("tardis",  new TardisDimensionInfo(
                        DimensionTypes.TARDIS_TYPE,
                        "Tardis Interior",
                        false,
                        true,
                        true,
                        200
                ))
                .add("nether", new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE,
                                new ResourceLocation("the_nether")),
                        "The Nether",
                        true,
                        true,
                        false,
                        200
                ))
                .add("the_end", new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("the_end")),
                        "The End",
                        true,
                        false,
                        true,
                        200
                ))
                .add("overworld", new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("overworld")),
                        "Earth",
                        true,
                        false,
                        false,
                        200
                ))
                .add(new ResourceLocation("ad_astra", "glacio"), new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("ad_astra", "glacio")),
                        "Glacio",
                        true,
                        false,
                        false,
                        200
                ))
                .add(new ResourceLocation("ad_astra", "mars"), new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("ad_astra", "mars")),
                        "Mars",
                        true,
                        false,
                        false,
                        200
                ))
                .add(new ResourceLocation("ad_astra", "mercury"), new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("ad_astra", "mercury")),
                        "Mercury",
                        true,
                        false,
                        false,
                        200
                ))
                .add(new ResourceLocation("ad_astra", "moon"), new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("ad_astra", "moon")),
                        "The Moon",
                        true,
                        false,
                        false,
                        200
                ))
                .add(new ResourceLocation("ad_astra", "venus"), new TardisDimensionInfo(
                        ResourceKey.create(Registries.DIMENSION_TYPE, new ResourceLocation("ad_astra", "venus")),
                        "Venus",
                        true,
                        false,
                        false,
                        200
                ));

        json(JsonRegistries.SOUND_SCHEMES_REGISTRY)
                .add("default", new SoundScheme(
                        SoundRegistry.DEFAULT_TARDIS_TAKEOFF.get(), 366,
                        SoundRegistry.DEFAULT_TARDIS_LOOP.get(), 40,
                        SoundRegistry.DEFAULT_TARDIS_LAND.get(), 224
                ))
                .add("master", new SoundScheme(
                        SoundRegistry.MASTER_TARDIS_TAKEOFF.get(), 360,
                        SoundRegistry.DEFAULT_TARDIS_LOOP.get(), 40,
                        SoundRegistry.MASTER_TARDIS_LAND.get(), 220
                ))
                .add("tv", new SoundScheme(
                        SoundRegistry.TV_TARDIS_TAKEOFF.get(), 220,
                        SoundRegistry.DEFAULT_TARDIS_LOOP.get(), 40,
                        SoundRegistry.TV_TARDIS_LAND.get(), 220
                ))
                .add("junk", new SoundScheme(
                        SoundRegistry.JUNK_TARDIS_TAKEOFF.get(), 354,
                        SoundRegistry.DEFAULT_TARDIS_LOOP.get(), 40,
                        SoundRegistry.JUNK_TARDIS_LAND.get(), 234

                ));

        DATAPACK_SET.add(set -> {

            set.add(ForgeRegistries.Keys.BIOME_MODIFIERS, boot -> {
                addBootstrapEntry(boot, ForgeRegistries.Keys.BIOME_MODIFIERS, Helper.createRL("tardis"), () -> new TardisBiomeModifier(
                        HolderSet.direct(
                                lookup(boot, Registries.PLACED_FEATURE, Helper.createRL("broken_tardis")),
                                lookup(boot, Registries.PLACED_FEATURE, Helper.createRL("xion_patch"))
                        )
                ));
            });

            set.add(Registries.PLACED_FEATURE, boot -> {
                addBootstrapEntry(boot, Registries.PLACED_FEATURE, Helper.createRL("broken_tardis"), () -> new PlacedFeature(
                        Holder.direct(new ConfiguredFeature<>(
                                WorldRegistry.BROKEN_TARDIS_FEATURE.get(),
                                new BrokenTardisFeatureConfig(2, 3, 50,
                                        BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR.get().defaultBlockState(),
                                        Lists.newArrayList(BiomeTags.TARDIS_GEN_BLACKLIST))
                        )),
                        List.of(
                                HeightRangePlacement.uniform(VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(100)),
                                new OnBlockPlacement(5)
                        )));

                addBootstrapEntry(boot, Registries.PLACED_FEATURE, Helper.createRL("xion_patch"), () -> new PlacedFeature(
                        Holder.direct(new ConfiguredFeature<>(
                                WorldRegistry.XION_PATCH_FEATURE.get(),
                                new XionFeatureConfig(30, 8, 6, 8, true, Lists.newArrayList(BiomeTags.TARDIS_GEN_BLACKLIST))
                        )),
                        List.of(
                                HeightRangePlacement.triangle(VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(64))
                        )
                ));
            });
        });

        json(JsonRegistries.SCHEMATIC_LOOT)
                .add("space_battle", new SchematicLootTable()
                        .addEntry(ExteriorRegistry.POLICE_BOX.getKey(), 100)
                        .addEntry(ItemRegistry.UPGRADE_REMOTE.getKey(), 20)
                        .addEntry(ItemRegistry.STATTENHEIM_REMOTE.getKey(), 20)
                );

        json(JsonRegistries.TOOLS)
                .add("welding_torch", new TardisTool(
                        ToolType.WELDING, ItemRegistry.WELDING_TORCH.get(), 0.2F, ParticleTypes.FLAME,
                        SoundRegistry.WELDING.getHolder(), 151)
                ).add("piston_hammer", new TardisTool(
                        ToolType.HAMMER, ItemRegistry.PISTON_HAMMER.get(), 0F, ParticleTypes.SMOKE,
                        Optional.empty(), 0
                ))
                .add("sonic_weld", new TardisTool(
                        ToolType.WELDING, ItemRegistry.SONIC.get(), 0.1F, ParticleTypes.FLAME,
                        SoundRegistry.SONIC_INTERACT.getHolder(), 80
                ))
                .add("sonic_sonic", new TardisTool(
                        ToolType.SONIC, ItemRegistry.SONIC.get(), 0.0F, ParticleTypes.FLAME,
                        SoundRegistry.SONIC_INTERACT.getHolder(), 80
                ));

        json(JsonRegistries.POS_DISGUISE)
                        .add("cactus", new PhasedShellDisguise.Builder()
                                .addBiomeTag(Tags.Biomes.IS_DESERT)
                                .add(BlockPos.ZERO, Blocks.CACTUS)
                                .add(BlockPos.ZERO.above(), Blocks.CACTUS)
                                .build()
                        );

        createDatapackRegistry(event);
    }

    public static <T> Holder<T> lookup(BootstapContext<?> context, ResourceKey<? extends Registry<T>> regKey, ResourceLocation key){
        return context.lookup(regKey).getOrThrow(ResourceKey.create(regKey, key));
    }

    public static void addRoomWithRepair(JsonReg<ARSRoom> reg, ResourceLocation rl, ARSRoom.Type type, Consumer<ARSRoom.Builder> build){
        final ResourceLocation parentLoc = new ResourceLocation(rl.getNamespace(), rl.getPath() + "_abandoned");

        final ARSRoom.Builder builder = new ARSRoom.Builder(rl, type)
                .setNoSpawn();
        final ARSRoom.Builder parentBuilder = new ARSRoom.Builder(parentLoc, type)
                .withParent(rl);

        build.accept(builder);
        build.accept(parentBuilder);

        reg.add(parentLoc, parentBuilder.build());
        reg.add(rl, builder.build());

    }

    public static void createDatapackRegistry(GatherDataEvent event){

        final RegistrySetBuilder builder = new RegistrySetBuilder();
        for(RegistrySet b : DATAPACK_SET){
            b.build(builder);
        }

        event.getGenerator().addProvider(true, new DatapackBuiltinEntriesProvider(
                event.getGenerator().getPackOutput(),
                event.getLookupProvider(),
                builder,
                Set.of(Tardis.MODID, "ad_astra")
        ));
    }

    public static <T> void addBootstrapEntry(BootstapContext<T> boot, ResourceKey<? extends Registry<T>> reg, ResourceLocation name, Supplier<T> value){
        boot.register(ResourceKey.create(reg, name), value.get());
    }

    public static <T> void addBootstrapEntry(BootstapContext<T> boot, ResourceKey<? extends Registry<T>> reg, Pair<ResourceLocation, Supplier<T>>... values){
        for(Pair<ResourceLocation, Supplier<T>> pair : values){
            addBootstrapEntry(boot, reg, pair.getFirst(), pair.getSecond());
        }
    }

    public static <T> JsonReg<T> json(ResourceKey<? extends Registry<T>> regKey){
        JsonReg<T> reg = new JsonReg<>(regKey, new HashMap<>());
        DATAPACK_SET.add(reg::build);
        return reg;
    }

    public record JsonReg<T>(ResourceKey<? extends Registry<T>> reg, Map<ResourceLocation, T> values){

        public JsonReg<T> add(ResourceLocation key, T value){
            this.values().put(key, value);
            return this;
        }

        public JsonReg<T> add(String key, T value){
            return this.add(Helper.createRL(key), value);
        }

        public JsonReg<T> add(String key, Function<ResourceLocation, T> factory){
            ResourceLocation rl = Helper.createRL(key);
            return add(rl, factory.apply(rl));
        }

        public void build(RegistrySetBuilder builder){
            builder.add(reg, set -> {
                for(Map.Entry<ResourceLocation, T> value : values().entrySet()){
                    set.register(ResourceKey.create(reg(), value.getKey()), value.getValue());
                }
            });
        }

    }

    public <T> Path getPathForRegistry(Path basePath, ResourceKey<Registry<T>> regKey, ResourceLocation loc){
        return basePath.resolve("data")
                .resolve(loc.getNamespace())
                .resolve(regKey.location().getNamespace())
                .resolve(regKey.location().getPath())
                .resolve(loc.getPath() + ".json");
    }

    public interface RegistrySet{
        void build(RegistrySetBuilder builder);
    }

}
