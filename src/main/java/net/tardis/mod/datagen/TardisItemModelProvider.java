package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.ModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;

public class TardisItemModelProvider extends ItemModelProvider{

    public TardisItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator.getPackOutput(), Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {

        this.generateSpecialBlockItem(BlockRegistry.G8_CONSOLE.getId());
        this.generateSpecialBlockItem(BlockRegistry.STEAM_CONSOLE.getId());
        this.generateSpecialBlockItem(BlockRegistry.NEUVEAU_CONSOLE.getId());
        this.generateSpecialBlockItem(BlockRegistry.ENGINE_STEAM.getId());
        this.generateSpecialBlockItem(ItemRegistry.SONIC.getId());
        this.generateSpecialBlockItem(BlockRegistry.GALVANIC_CONSOLE.getId());
        this.generateSpecialBlockItem(BlockRegistry.NEMO_CONSOLE.getId());
        this.generateSpecialBlockItem(BlockRegistry.FABRICATOR_MACHINE.getId());
        this.generateSpecialBlockItem(BlockRegistry.TELEPORT_TUBE.getId());

        this.generateBlockItem(BlockRegistry.STEAM_MONITOR, Helper.createRL("block/machines/monitor/steampunk_monitor"));
        this.generateBlockItem(BlockRegistry.EYE_MONITOR, Helper.createRL("block/machines/monitor/eye_monitor"));
        this.generateBlockItem(BlockRegistry.RCA_MONITOR, Helper.createRL("block/machines/monitor/rca_monitor"));
        this.generateBlockItem(BlockRegistry.ALEMBIC, Helper.createRL("block/machines/alembic_off"));
        this.generateBlockItem(BlockRegistry.TECH_STRUT, Helper.createRL("block/decoration/tech_strut_floor"));
        this.generateBlockItem(BlockRegistry.ARS_PANEL, Helper.createRL("block/machines/ars_panel"));
        this.generateBlockItem(BlockRegistry.TUNGSTEN_STAIRS, Helper.createRL("block/decoration/tungsten/stair_stairs"));
        this.generateBlockItem(BlockRegistry.TUNGSTEN_STAIRS_SMALL_PLATES, Helper.createRL("block/decoration/tungsten/stair_small_plates_stairs"));
        this.generateBlockItem(BlockRegistry.CORAL_STAIRS, Helper.createRL("block/decoration/coral_stairs_stairs"));
        this.generateBlockItem(BlockRegistry.ALABASTER_STAIRS, Helper.createRL("block/decoration/alabaster/stairs_stairs"));
        this.generateBlockItem(BlockRegistry.HOLO_LADDER, Helper.createRL("block/decoration/holo_ladders"));
        this.generateBlockItem(BlockRegistry.STEAM_GRATE, Helper.createRL("block/decoration/steam_grate"));
        this.generateBlockItem(BlockRegistry.COMFY_CHAIR_RED, Helper.createRL("block/chairs/comfy_chair"));
        this.generateBlockItem(BlockRegistry.COMFY_CHAIR_GREEN, Helper.createRL("block/chairs/comfy_chair_green"));


        this.generateDefaultItem(BlockRegistry.XION.getId(), Helper.createRL("item/resources/xion_crystal"));
        this.generateDefaultItem(ItemRegistry.EXO_CIRCUITS.getId(), Helper.createRL("item/resources/circuits"));
        this.generateBlockItem(BlockRegistry.XION_BLOCK, Helper.createRL("block/xion_block"));
        this.generateDefaultItem(ItemRegistry.CIRCUIT_PLATE.getId(), Helper.createRL("item/resources/circuit_paste"));
        this.generateDefaultItem(ItemRegistry.MERCURY_BOTTLE.getId(), Helper.createRL("item/resources/mercury_bottle"));
        this.generateDefaultItem(ItemRegistry.MANUAL.getId(), Helper.createRL("item/manual"));
        this.generateDefaultItem(BlockRegistry.STEAM_BROKEN_EXTERIOR.getId(), Helper.createRL("item/broken_exterior"));
        this.generateDefaultItem(ItemRegistry.CINNABAR.getId(), Helper.createRL("item/resources/cinnabar_chunk"));
        this.generateDefaultItem(ItemRegistry.XION_LENS.getId(), Helper.createRL("item/resources/xion_lens"));
        this.generateDefaultItem(ItemRegistry.CRYSTAL_VIALS.getId(), Helper.createRL("item/resources/vial/empty"));
        this.generateDefaultItem(ItemRegistry.CRYSTAL_VIALS_MERCURY.getId(), Helper.createRL("item/resources/vial/mercury"));
        this.generateDefaultItem(ItemRegistry.DATA_CRYSTAL.getId(), Helper.createRL("item/resources/data_crystal"));
        this.generateDefaultItem(BlockRegistry.INTERIOR_DOOR.getId(), Helper.createRL("item/interior_door"));
        this.generateDefaultItem(ItemRegistry.TAPE_MEASURE.getId(), Helper.createRL("item/tools/tape_measure"));
        this.generateDefaultItem(ItemRegistry.TEA.getId(), Helper.createRL("item/tea"));
        this.generateUpgrade(ItemRegistry.UPGRADE_NANO_GENE.get());
        this.generateUpgrade(ItemRegistry.UPGRADE_LASER_MINER.get());
        this.generateUpgrade(ItemRegistry.UPGRADE_ENERGY_SYPHON.get());
        this.generateDefaultItem(ItemRegistry.UPGRADE_BASE.getId(), Helper.createRL("item/components/upgrade_circuit_blank"));
        this.generateUpgrade(ItemRegistry.UPGRADE_REMOTE.get());
        this.generateDefaultItem(ItemRegistry.BURNT_EXOCIRCUITS.getId(), Helper.createRL("item/resources/circuits_burnt"));
        this.generateDefaultItem(ItemRegistry.PHASED_OPTIC_SHELL_GENERATOR.getId(), Helper.createRL("item/tools/plasmic"));
        this.generateUpgrade(ItemRegistry.UPGRADE_ATRIUM.get());
        this.generateDefaultItem(BlockRegistry.BIG_DOOR.getId(), Helper.createRL("item/decorations/slidingdoor_alabaster_item"));
        this.generateDefaultItem(ItemRegistry.FIRST_ENTER_RECORD.getId(), Helper.createRL("item/music_disc_firstentrance"));
        this.generateDefaultItem(ItemRegistry.PLATE_COPPER.getId(), Helper.createRL("item/resources/copper_plate"));
        //this.generateDefaultItem(BlockRegistry.FABRICATOR_MACHINE.getId(), Helper.createRL("item/crappy_industrial_fab_sprite"));
        this.generateDefaultItem(BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR.getId(), Helper.createRL("item/tt_derelict"));
        this.generateSonicUpgrade(ItemRegistry.UPGRADE_SONIC_DEMAT.get());

        ForgeRegistries.BLOCKS.getKeys().stream().filter(rl -> {
            if(!rl.getNamespace().equals(this.modid))
                return false;
            if(!(ForgeRegistries.BLOCKS.getValue(rl) instanceof RoundelBlock)){
                return false;
            }
            return true;
        }).forEach(rl -> {
            generateBlockItem(rl.withPrefix("item/"), rl);
        });

    }

    public ItemModelBuilder generateDefaultItem(ResourceLocation key, ResourceLocation... textures){

        String path = key.getPath();
        if(!path.startsWith(ITEM_FOLDER)){
            path = ITEM_FOLDER + "/" + path;
        }

        ItemModelBuilder builder = this.getBuilder(path)
                .parent(new BuiltinItemModel());

        for(int i = 0; i < textures.length; ++i){
            builder = builder.texture("layer" + i, textures[i]);
        }

        this.generatedModels.put(key, builder);
        return builder;
    }

    public void generateSonicUpgrade(Item item){
        this.generateDefaultItem(ForgeRegistries.ITEMS.getKey(item),
                Helper.createRL("item/components/upgrade_sonic"));
    }

    public void generateBlockItem(ResourceLocation block, ResourceLocation model){
        this.generatedModels.put(block, this.getBuilder(block.getPath())
                .parent(this.getExistingFile(model.withPrefix(ModelProvider.BLOCK_FOLDER + "/"))));
    }
    public <T extends Block> void generateBlockItem(RegistryObject<T> block, ResourceLocation model){
        this.generatedModels.put(block.getId(), this.getBuilder(ModelProvider.ITEM_FOLDER + "/" + block.getId().getPath())
                .parent(this.getExistingFile(model)));
    }

    public void generateSpecialBlockItem(ResourceLocation reg){
        this.generatedModels.put(reg, this.getBuilder("item/" + reg.getPath())
                .parent(new BuiltinModel()));
    }

    public void generateUpgrade(Item item){
        final ResourceLocation key = ForgeRegistries.ITEMS.getKey(item);
        this.generateDefaultItem(key, Helper.createRL("item/components/upgrade_circuit"));
    }

    public static class BuiltinModel extends ModelFile{

        protected BuiltinModel() {
            super(new ResourceLocation("builtin/entity"));
        }

        @Override
        protected boolean exists() {
            return true;
        }
    }

    public static class BuiltinItemModel extends ModelFile{

        public BuiltinItemModel() {
            super(new ResourceLocation("item/generated"));
        }

        @Override
        protected boolean exists() {
            return true;
        }
    }

}
