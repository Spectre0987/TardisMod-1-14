package net.tardis.mod.datagen;

import net.minecraft.data.PackOutput;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.GlobalLootModifierProvider;
import net.minecraftforge.common.loot.LootTableIdCondition;
import net.tardis.mod.Tardis;
import net.tardis.mod.loot.TardisLootModifier;
import net.tardis.mod.registry.ItemRegistry;

public class TardisLootModiferProvider extends GlobalLootModifierProvider {

    public TardisLootModiferProvider(PackOutput output) {
        super(output, Tardis.MODID);
    }

    @Override
    protected void start() {
        this.add("redstone", TardisLootModifier.Builder.create(LootTableIdCondition.builder(Blocks.REDSTONE_ORE.getLootTable()).build())
                        .loot(0.25F, new ItemStack(ItemRegistry.CINNABAR.get()))
                .build()
            );
        this.add("redstone_deepslate", TardisLootModifier.Builder.create(LootTableIdCondition.builder(Blocks.DEEPSLATE_REDSTONE_ORE.getLootTable()).build())
                .loot(0.25F, new ItemStack(ItemRegistry.CINNABAR.get()))
                .build()
        );
    }
}
