package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.*;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.Tags;
import net.minecraftforge.fluids.FluidStack;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.fluids.FluidRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.*;

import java.util.function.Consumer;

public class TardisRecipeProvider extends RecipeProvider {
    public TardisRecipeProvider(DataGenerator pGenerator) {
        super(pGenerator.getPackOutput());
    }

    @Override
    protected void buildRecipes(Consumer<FinishedRecipe> consumer) {

        ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ItemRegistry.PISTON_HAMMER.get())
                .pattern(" pi")
                .pattern(" sp")
                .pattern("s  ")
                .define('p', Ingredient.of(Blocks.PISTON))
                .define('i', Ingredient.of(Tags.Items.INGOTS_IRON))
                .define('s', Ingredient.of(Items.STICK))
        .unlockedBy("has", has(Blocks.PISTON))
        .save(consumer);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ItemRegistry.CRYSTAL_VIALS.get(), 12)
                .unlockedBy("has", has(BlockRegistry.XION.get()))
                .pattern("c c")
                .pattern("c c")
                .pattern(" c ")
                .define('c', BlockRegistry.XION.get())
        .save(consumer);

        ShapedRecipeBuilder.shaped(RecipeCategory.BREWING, BlockRegistry.ALEMBIC.get())
                .unlockedBy("has", has(ItemRegistry.CRYSTAL_VIALS.get()))
                .pattern("vc")
                .pattern("fp")
                .pattern("ww")
                .define('v', ItemRegistry.CRYSTAL_VIALS.get())
                .define('c', Tags.Items.INGOTS_COPPER)
                .define('f', Blocks.FURNACE)
                .define('p', Items.FLOWER_POT)
                .define('w', ItemTags.WOODEN_SLABS)
                .save(consumer);

        ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ItemRegistry.MANUAL.get())
                .unlockedBy("has", has(BlockRegistry.XION.get()))
                .requires(BlockRegistry.XION.get())
                .requires(Items.BOOK)
                .save(consumer);

        SingleItemRecipeBuilder.stonecutting(Ingredient.of(BlockRegistry.XION.get()), RecipeCategory.MISC, ItemRegistry.XION_LENS.get())
                .unlockedBy("has", has(BlockRegistry.XION.get()))
                .save(consumer);

        //Alembic

        AlembicRecipe.Builder.create(Helper.createRL("mercury"))
                .unlockedBy("has", has(Items.WATER_BUCKET))
                .ingredient(ItemRegistry.CINNABAR.get())
                .withRequiredFluid(new FluidStack(Fluids.WATER, 500))
                .withResult(new FluidStack(FluidRegistry.MERCURY.get(), 250))
                .save(consumer, RecipeRegistry.ALEMBIC_SERIALIZER.get());


        AlembicRecipe.Builder.create(Helper.createRL("biomass"))
                .unlockedBy("has", has(Items.SPRUCE_SAPLING))
                .withRequiredFluid(new FluidStack(Fluids.WATER, 100))
                .withResult(new FluidStack(FluidRegistry.BIOMASS.get(), 100))
                .ingredient(net.tardis.mod.tags.ItemTags.ALEMBIC_BIOMASS)
                .save(consumer, RecipeRegistry.ALEMBIC_SERIALIZER.get());

        //Alembic Bottling
        AlembicBottlingRecipe.Builder.create(Helper.createRL("mercury"), ItemRegistry.MERCURY_BOTTLE.get())
                        .unlockedBy("has", has(Items.GLASS_BOTTLE))
                        .fluid(new FluidStack(FluidRegistry.MERCURY.get(), 500))
                        .save(consumer);
        AlembicBottlingRecipe.Builder.create(Helper.createRL("mercury_vial"), ItemRegistry.CRYSTAL_VIALS_MERCURY.get())
                .unlockedBy("has", has(ItemRegistry.CRYSTAL_VIALS.get()))
                .container(ItemRegistry.CRYSTAL_VIALS.get())
                .fluid(new FluidStack(FluidRegistry.MERCURY.get(), 500))
                        .save(consumer);

        AlembicBottlingRecipe.Builder.create(Helper.createRL("exo_circuit"), ItemRegistry.CIRCUIT_PLATE.get())
                .unlockedBy("has", has(net.tardis.mod.tags.ItemTags.COPPER_PLATES))
                .fluid(new FluidStack(FluidRegistry.BIOMASS.get(), 200))
                .container(net.tardis.mod.tags.ItemTags.COPPER_PLATES)
                        .save(consumer);
        AlembicBottlingRecipe.Builder.create(Helper.createRL("tea"), ItemRegistry.TEA.get())
                .unlockedBy("has", has(Items.GLASS_BOTTLE))
                .container(Items.GLASS_BOTTLE)
                .fluid(new FluidStack(FluidRegistry.BIOMASS.get(), 500))
                .save(consumer);

        //Drafting Table
        DraftingTableRecipe.Builder.create(Helper.createRL("welding_torch"), new ItemStack(ItemRegistry.WELDING_TORCH.get()))
                .unlockedBy("has", has(ItemRegistry.CRYSTAL_VIALS.get()))
                .pattern("lii")
                .pattern("bxr")
                .pattern("i  ")
                .map('l', Items.SOUL_LANTERN)
                .map('x', ItemRegistry.XION_LENS.get())
                .map('b', Tags.Items.RODS_BLAZE)
                .map('i', Tags.Items.INGOTS_IRON)
                .map('r', Items.LIGHTNING_ROD)
                .toolType(ToolType.HAMMER, 30)
                .save(consumer, RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get());

        DraftingTableRecipe.Builder.create(Helper.createRL("xion_block"), new ItemStack(BlockRegistry.XION_BLOCK.get()))
                .unlockedBy("has", has(BlockRegistry.XION_BLOCK.get()))
                .map('x', BlockRegistry.XION.get())
                .pattern("xx ")
                .pattern("xx ")
                .pattern("   ")
                .toolType(ToolType.WELDING, 20)
                .save(consumer, RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get());

        DraftingTableRecipe.Builder.create(Helper.createRL("exo_circuits"), new ItemStack(ItemRegistry.EXO_CIRCUITS.get()))
                .unlockedBy("has", has(ItemRegistry.CIRCUIT_PLATE.get()))
                .pattern("ccg")
                .pattern("c g")
                .pattern("ccg")
                .map('c', ItemRegistry.CIRCUIT_PLATE.get())
                .map('g', Tags.Items.NUGGETS_GOLD)
                .slag(new ItemStack(ItemRegistry.BURNT_EXOCIRCUITS.get()))
                .toolType(ToolType.WELDING, 20)
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("diagnostic_tool"), new ItemStack(ItemRegistry.CHRONOFAULT_LOCATOR.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("ccc")
                .pattern("idi")
                .pattern("fi ")
                .map('d', BlockRegistry.DATA_MATRIX.get())
                .map('f', ItemRegistry.FLUID_LINKS.get())
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('i', Tags.Items.INGOTS_IRON)
                .toolType(ToolType.WELDING, 20)
                .save(consumer, RecipeRegistry.DRAFTING_TABLE_SERIALIZER.get());

        DraftingTableRecipe.Builder.create(Helper.createRL("components/demat"), new ItemStack(ItemRegistry.DEMAT_CIRCUIT.get()))
                .pattern("lil")
                .pattern("cpc")
                .pattern("lil")
                .map('l', ItemRegistry.XION_LENS.get())
                .map('i', Items.IRON_INGOT)
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('p', Items.ENDER_PEARL)
                .unlockedBy("has", has(Items.ENDER_PEARL))
                .toolType(ToolType.WELDING, 30)
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/fluid_links"), new ItemStack(ItemRegistry.FLUID_LINKS.get()))
                .unlockedBy("has", has(net.tardis.mod.tags.ItemTags.MERCURY_BOTTLE))
                .pattern("gmg")
                .pattern("gmg")
                .pattern("ici")
                .map('g', Tags.Items.GLASS)
                .map('m', net.tardis.mod.tags.ItemTags.MERCURY_BOTTLE)
                .map('i', Tags.Items.INGOTS_IRON)
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .toolType(ToolType.WELDING, 20)
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("data_matrix"), new ItemStack(BlockRegistry.DATA_MATRIX.get()))
                .unlockedBy("has", has(BlockRegistry.XION.get()))
                .pattern("crc")
                .pattern("rdr")
                .pattern("crc")
                .map('c', ItemRegistry.DATA_CRYSTAL.get())
                .map('r', Tags.Items.DUSTS_REDSTONE)
                .map('d', Tags.Items.GEMS_DIAMOND)
                .toolType(ToolType.WELDING, 20)
                .slag(
                        new ItemStack(Items.DIAMOND),
                        new ItemStack(ItemRegistry.DATA_CRYSTAL.get()),
                        new ItemStack(ItemRegistry.DATA_CRYSTAL.get()),
                        new ItemStack(ItemRegistry.DATA_CRYSTAL.get())
                )
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("data_crystal"), new ItemStack(ItemRegistry.DATA_CRYSTAL.get()))
                .pattern("ggc")
                .pattern("xx ")
                .pattern("xx ")
                .map('g', Tags.Items.NUGGETS_GOLD)
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('x', BlockRegistry.XION.get())
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(Items.GOLD_NUGGET))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/nav_com"), new ItemStack(ItemRegistry.NAV_COM.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("lrl")
                .pattern("eae")
                .pattern("lrl")
                .map('l', ItemRegistry.XION_LENS.get())
                .map('r', Items.LIGHTNING_ROD)
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .map('a', ItemRegistry.INTERSTITAL_ANTENNA.get())
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(ItemRegistry.INTERSTITAL_ANTENNA.get()))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/antenna"), new ItemStack(ItemRegistry.INTERSTITAL_ANTENNA.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("iri")
                .pattern("eee")
                .pattern("iri")
                .map('i', Tags.Items.INGOTS_IRON)
                .map('r', Items.LIGHTNING_ROD)
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(ItemRegistry.EXO_CIRCUITS.get()), new ItemStack(ItemRegistry.EXO_CIRCUITS.get()))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/stabilizer"), new ItemStack(ItemRegistry.STABILIZER.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("olo")
                .pattern("lel")
                .pattern("olo")
                .map('o', Tags.Items.OBSIDIAN)
                .map('l', ItemRegistry.XION_LENS.get())
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(Blocks.OBSIDIAN), new ItemStack(ItemRegistry.EXO_CIRCUITS.get()))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/chameleon_circuit"), new ItemStack(ItemRegistry.CHAMELEON_CIRCUIT.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("ger")
                .pattern("ede")
                .pattern("beb")
                .map('g', Tags.Items.DYES_GREEN)
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .map('r', Tags.Items.DYES_RED)
                .map('d', BlockRegistry.DATA_MATRIX.get())
                .map('b', Tags.Items.DYES_BLUE)
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(ItemRegistry.EXO_CIRCUITS.get()), new ItemStack(BlockRegistry.DATA_MATRIX.get()))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("components/temporal_grace"), new ItemStack(ItemRegistry.TEMPORAL_GRACE.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .pattern("dld")
                .pattern("ete")
                .pattern("dld")
                .map('d', BlockRegistry.DATA_MATRIX.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .map('t', Items.GHAST_TEAR)
                .toolType(ToolType.WELDING, 20)
                .slag(new ItemStack(BlockRegistry.DATA_MATRIX.get()), new ItemStack(Items.GHAST_TEAR))
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("capacitors/basic"), new ItemStack(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .unlockedBy("has", has(BlockRegistry.XION_BLOCK.get()))
                .pattern("ili")
                .pattern("exe")
                .pattern("xlx")
                .map('i', Tags.Items.INGOTS_IRON)
                .map('l', ItemRegistry.XION_LENS.get())
                .map('e', ItemRegistry.EXO_CIRCUITS.get())
                .map('x', BlockRegistry.XION_BLOCK.get())
                .toolType(ToolType.HAMMER, 20)
                .save(consumer);


        DraftingTableRecipe.Builder.create(Helper.createRL("artron_pylon"), new ItemStack(BlockRegistry.RIFT_PYLON.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .pattern(" a ")
                .pattern("cpc")
                .pattern("imi")
                .map('a', ItemRegistry.XION_LENS.get())
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('p', Items.LIGHTNING_ROD)
                .map('i', Tags.Items.INGOTS_IRON)
                .map('m', BlockRegistry.DATA_MATRIX.get())
                .toolType(ToolType.WELDING, 20)
                .save(consumer);

        DraftingTableRecipe.Builder.create(Helper.createRL("artron_collector"), new ItemStack(BlockRegistry.RIFT_COLLECTOR.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .pattern("ili")
                .pattern("xix")
                .pattern("cac")
                .map('i', Tags.Items.INGOTS_IRON)
                .map('l', ItemRegistry.XION_LENS.get())
                .map('x', BlockRegistry.XION.get())
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('a', ItemRegistry.ARTRON_CAP_BASIC.get())
                .toolType(ToolType.WELDING, 20)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("quantiscope"), new ItemStack(BlockRegistry.QUANTISCOPE.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .toolType(ToolType.WELDING, 20)
                .pattern("iti")
                .pattern("ili")
                .pattern("cdc")
                .map('i', Tags.Items.INGOTS_IRON)
                .map('t', ItemRegistry.WELDING_TORCH.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('d', BlockRegistry.DATA_MATRIX.get())
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("temporal_scoop"), new ItemStack(BlockRegistry.SCOOP_VAULT.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .pattern("cdc")
                .pattern("cbc")
                .pattern("cmc")
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('d', ItemRegistry.DEMAT_CIRCUIT.get())
                .map('b', Tags.Items.BARRELS)
                .map('m', BlockRegistry.DATA_MATRIX.get())
                .toolType(ToolType.WELDING, 40)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("machines/matter_buffer"), new ItemStack(BlockRegistry.MATTER_BUFFER.get()))
                .unlockedBy("has", has(ItemRegistry.DATA_CRYSTAL.get()))
                .toolType(ToolType.WELDING, 40)
                .pattern(" a ")
                .pattern("clc")
                .pattern("fbf")
                .map('a', ItemRegistry.INTERSTITAL_ANTENNA.get())
                .map('c', ItemRegistry.DATA_CRYSTAL.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .map('f', ItemRegistry.FLUID_LINKS.get())
                .map('b', Tags.Items.BARRELS)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("components/shield"), new ItemStack(ItemRegistry.SHIELD_GENERATOR.get()))
                .unlockedBy("has", has(Tags.Items.OBSIDIAN))
                .pattern("oco")
                .pattern("cdc")
                .pattern("oco")
                .map('o', Tags.Items.OBSIDIAN)
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('d', BlockRegistry.DATA_MATRIX.get())
                .toolType(ToolType.HAMMER, 30)
                .toolType(ToolType.HAMMER, 40)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("repair_circuit"), new ItemStack(ItemRegistry.EXO_CIRCUITS.get()))
                .unlockedBy("has", has(ItemRegistry.BURNT_EXOCIRCUITS.get()))
                .pattern("c  ")
                .pattern("   ")
                .pattern("   ")
                .map('c', ItemRegistry.BURNT_EXOCIRCUITS.get())
                .toolType(ToolType.SONIC, 40)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("waypoint_banks"), new ItemStack(BlockRegistry.WAYPOINT_BANKS.get()))
                .unlockedBy("has", has(ItemRegistry.DATA_CRYSTAL.get()))
                .pattern("ccc")
                .pattern("imi")
                .pattern("ili")
                .map('c', ItemRegistry.DATA_CRYSTAL.get())
                .map('i', Tags.Items.INGOTS_IRON)
                .map('m', BlockRegistry.DATA_MATRIX.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .toolType(ToolType.WELDING, 60)
                .slag(new ItemStack(ItemRegistry.DATA_CRYSTAL.get(), 2), new ItemStack(BlockRegistry.DATA_MATRIX.get()))
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("tools/personal_shield"), new ItemStack(ItemRegistry.PERSONAL_SHIELD.get()))
                .unlockedBy("has", has(ItemRegistry.SHIELD_GENERATOR.get()))
                .pattern("cac")
                .pattern("lsl")
                .pattern("cic")
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('a', ItemRegistry.ARTRON_CAP_BASIC.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .map('s', ItemRegistry.SHIELD_GENERATOR.get())
                .map('i', Items.SHIELD)
                .slag(
                        new ItemStack(ItemRegistry.ARTRON_CAP_BASIC.get()),
                        new ItemStack(ItemRegistry.SHIELD_GENERATOR.get())
                )
                .toolType(ToolType.WELDING, 60)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("tardis_trophy"), new ItemStack(BlockRegistry.ITEM_PLAQUE.get(), 4))
                .unlockedBy("has", has(Items.DARK_OAK_SIGN))
                .pattern(" s ")
                .pattern("sgs")
                .pattern(" s ")
                .map('s', Items.DARK_OAK_SIGN)
                .map('g', Tags.Items.INGOTS_GOLD)
                .toolType(ToolType.WELDING, 60)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("keys/main"), new ItemStack(ItemRegistry.GALLIFREYAN_KEY.get()))
                .unlockedBy("has", has(Tags.Items.INGOTS_IRON))
                .pattern("iii")
                .pattern("cic")
                .pattern("lil")
                .map('i', Tags.Items.INGOTS_IRON)
                .map('c', ItemRegistry.EXO_CIRCUITS.get())
                .map('l', ItemRegistry.XION_LENS.get())
                .toolType(ToolType.HAMMER, 40)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("machines/alembic_still"), new ItemStack(BlockRegistry.ALEMBIC_STILL.get()))
                .unlockedBy("has", has(Items.GLASS_BOTTLE))
                .pattern("ggl")
                .pattern("gma")
                .pattern("c  ")
                .map('g', Tags.Items.GLASS)
                .map('l', ItemRegistry.FLUID_LINKS.get())
                .map('m', BlockRegistry.DATA_MATRIX.get())
                .map('a', BlockRegistry.ALEMBIC.get())
                .map('c', Blocks.CAULDRON)
                .toolType(ToolType.HAMMER, 60)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("machines/fabricator"), new ItemStack(BlockRegistry.FABRICATOR_MACHINE.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .pattern("lsl")
                .pattern("ili")
                .pattern("cdc")
                .map('l', ItemRegistry.XION_LENS.get())
                .map('s', BlockRegistry.QUANTISCOPE.get())
                .map('i', Tags.Items.INGOTS_IRON)
                .map('c', ItemRegistry.ARTRON_CAP_BASIC.get())
                .map('d', BlockRegistry.DRAFTING_TABLE.get())
                .toolType(ToolType.WELDING, 60)
                .save(consumer);
        DraftingTableRecipe.Builder.create(Helper.createRL("resources/copper_plate"), new ItemStack(ItemRegistry.PLATE_COPPER.get()))
                .unlockedBy("has", has(Tags.Items.INGOTS_COPPER))
                .toolType(ToolType.HAMMER, 40)
                .pattern("   ")
                .pattern(" c ")
                .pattern("   ")
                .map('c', Tags.Items.INGOTS_COPPER)
                .save(consumer);

        //Quantiscope
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("upgrades/mining_laser"), new ItemStack(ItemRegistry.UPGRADE_LASER_MINER.get()))
                .unlockedBy("has", has(ItemRegistry.UPGRADE_BASE.get()))
                .withMain(ItemRegistry.UPGRADE_BASE.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Items.DIAMOND_PICKAXE)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("upgrades/energy_syphon"), new ItemStack(ItemRegistry.UPGRADE_ENERGY_SYPHON.get()))
                .unlockedBy("has", has(ItemRegistry.UPGRADE_BASE.get()))
                .withMain(ItemRegistry.UPGRADE_BASE.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Blocks.ICE)
                .with(ItemRegistry.ARTRON_CAP_MID.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("upgrades/blank"), new ItemStack(ItemRegistry.UPGRADE_BASE.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .withMain(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.GEMS_AMETHYST)
                .with(Tags.Items.INGOTS_COPPER)
                .with(Tags.Items.NUGGETS_GOLD)
                .with(Tags.Items.GEMS_QUARTZ)
                .with(Tags.Items.INGOTS_COPPER)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("temporal_scoop"), new ItemStack(BlockRegistry.SCOOP_VAULT.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(Tags.Items.BARRELS)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.DEMAT_CIRCUIT.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("tools/sonic"), new ItemStack(ItemRegistry.SONIC.get()))
                .unlockedBy("has", has(Tags.Items.GEMS_DIAMOND))
                .withMain(Tags.Items.GEMS_DIAMOND)
                .with(ItemRegistry.ARTRON_CAP_BASIC.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("tools/stattenheim_remote"), new ItemStack(ItemRegistry.STATTENHEIM_REMOTE.get()))
                .unlockedBy("has", has(Items.NETHERITE_SCRAP))
                .withMain(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Items.NETHERITE_SCRAP)
                .with(ItemRegistry.INTERSTITAL_ANTENNA.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(Items.NETHERITE_SCRAP)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("upgrades/remote_circuit"), new ItemStack(ItemRegistry.UPGRADE_REMOTE.get()))
                .unlockedBy("has", has(ItemRegistry.UPGRADE_BASE.get()))
                .withMain(ItemRegistry.UPGRADE_BASE.get())
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(ItemRegistry.INTERSTITAL_ANTENNA.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(ItemRegistry.XION_LENS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/shield"), new ItemStack(ItemRegistry.SHIELD_GENERATOR.get()))
                .unlockedBy("has", has(Tags.Items.OBSIDIAN))
                .withMain(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.OBSIDIAN)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.OBSIDIAN)
                .save(consumer);


        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("exocircuits"), new ItemStack(ItemRegistry.EXO_CIRCUITS.get()))
                .unlockedBy("has", has(ItemRegistry.CIRCUIT_PLATE.get()))
                .withMain(ItemRegistry.CIRCUIT_PLATE.get())
                .with(Items.GOLD_NUGGET)
                .with(Items.GOLD_NUGGET)
                .with(ItemRegistry.CIRCUIT_PLATE.get())
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("tools/chronofault_locator"), new ItemStack(ItemRegistry.CHRONOFAULT_LOCATOR.get()))
                .unlockedBy("has", has(ItemRegistry.DATA_CRYSTAL.get()))
                .withMain(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("component/demat_circuit"), new ItemStack(ItemRegistry.DEMAT_CIRCUIT.get()))
                .unlockedBy("has", has(Tags.Items.ENDER_PEARLS))
                .withMain(Tags.Items.ENDER_PEARLS)
                .with(Tags.Items.INGOTS_IRON)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("component/fluid_link"), new ItemStack(ItemRegistry.FLUID_LINKS.get()))
                .unlockedBy("has", has(net.tardis.mod.tags.ItemTags.MERCURY_BOTTLE))
                .withMain(net.tardis.mod.tags.ItemTags.MERCURY_BOTTLE)
                .with(Tags.Items.GLASS)
                .with(Tags.Items.GLASS)
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("data_matrix"), new ItemStack(BlockRegistry.DATA_MATRIX.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(Tags.Items.GEMS_DIAMOND)
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(Tags.Items.DUSTS_REDSTONE)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("data_crystal"), new ItemStack(ItemRegistry.DATA_CRYSTAL.get()))
                .unlockedBy("has", has(ItemRegistry.EXO_CIRCUITS.get()))
                .withMain(ItemRegistry.EXO_CIRCUITS.get())
                .with(BlockRegistry.XION.get())
                .with(BlockRegistry.XION.get())
                .with(Tags.Items.NUGGETS_GOLD)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/nav_com"), new ItemStack(ItemRegistry.NAV_COM.get()))
                .unlockedBy("has", has(ItemRegistry.INTERSTITAL_ANTENNA.get()))
                .withMain(ItemRegistry.INTERSTITAL_ANTENNA.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Items.LIGHTNING_ROD)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/antenna"), new ItemStack(ItemRegistry.INTERSTITAL_ANTENNA.get()))
                .unlockedBy("has", has(Items.LIGHTNING_ROD))
                .withMain(Items.LIGHTNING_ROD)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/chameleon_circuit"), new ItemStack(ItemRegistry.CHAMELEON_CIRCUIT.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(BlockRegistry.DATA_MATRIX.get())
                .with(Tags.Items.DYES_RED)
                .with(Tags.Items.DYES_GREEN)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.DYES_BLUE)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/temporal_grace"), new ItemStack(ItemRegistry.TEMPORAL_GRACE.get()))
                .unlockedBy("has", has(Items.GHAST_TEAR))
                .withMain(Items.GHAST_TEAR)
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("capacitors/low"), new ItemStack(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .unlockedBy("has", has(BlockRegistry.XION_BLOCK.get()))
                .withMain(BlockRegistry.XION_BLOCK.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("capacitors/mid"), new ItemStack(ItemRegistry.ARTRON_CAP_MID.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .withMain(ItemRegistry.ARTRON_CAP_BASIC.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(Tags.Items.RODS_BLAZE)
                .with(BlockRegistry.XION.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("capacitors/high"), new ItemStack(ItemRegistry.ARTRON_CAP_HIGH.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_MID.get()))
                .withMain(ItemRegistry.ARTRON_CAP_MID.get())
                .with(Tags.Items.GEMS_AMETHYST)
                .with(BlockRegistry.XION_BLOCK.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/artron_pylon"), new ItemStack(BlockRegistry.RIFT_PYLON.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(Items.LIGHTNING_ROD)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Items.LIGHTNING_ROD)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(BlockRegistry.DATA_MATRIX.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/artron_collector"), new ItemStack(BlockRegistry.RIFT_COLLECTOR.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .withMain(ItemRegistry.ARTRON_CAP_BASIC.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(BlockRegistry.XION.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/matter_buffer"), new ItemStack(BlockRegistry.MATTER_BUFFER.get()))
                .unlockedBy("has", has(ItemRegistry.DATA_CRYSTAL.get()))
                .withMain(Tags.Items.BARRELS)
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(ItemRegistry.INTERSTITAL_ANTENNA.get())
                .with(ItemRegistry.XION_LENS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/waypoint_banks"), new ItemStack(BlockRegistry.WAYPOINT_BANKS.get()))
                .unlockedBy("has", has(ItemRegistry.DATA_CRYSTAL.get()))
                .withMain(BlockRegistry.DATA_MATRIX.get())
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(ItemRegistry.XION_LENS.get())
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("tools/personal_shield"), new ItemStack(ItemRegistry.PERSONAL_SHIELD.get()))
                .unlockedBy("has", has(ItemRegistry.SHIELD_GENERATOR.get()))
                .withMain(Items.SHIELD)
                .with(ItemRegistry.XION_LENS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.SHIELD_GENERATOR.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("tardis_trophy"), new ItemStack(BlockRegistry.ITEM_PLAQUE.get()))
                .unlockedBy("has", has(Items.DARK_OAK_SIGN))
                .withMain(Items.DARK_OAK_SIGN)
                .with(Tags.Items.NUGGETS_GOLD)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("keys/main"), new ItemStack(ItemRegistry.GALLIFREYAN_KEY.get()))
                .unlockedBy("has", has(Tags.Items.INGOTS_IRON))
                .withMain(Tags.Items.INGOTS_IRON)
                .with(ItemRegistry.XION_LENS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("keys/pirate"), new ItemStack(ItemRegistry.PIRATE_KEY.get()))
                .unlockedBy("has", has(net.tardis.mod.tags.ItemTags.KEYS))
                .withMain(net.tardis.mod.tags.ItemTags.KEYS)
                .with(ItemTags.COALS)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("big_door"), new ItemStack(BlockRegistry.BIG_DOOR.get()))
                .unlockedBy("has", has(ItemTags.DOORS))
                .withMain(ItemTags.DOORS)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);

        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("components/stabilizer"), new ItemStack(ItemRegistry.STABILIZER.get()))
                .unlockedBy("has", has(Tags.Items.OBSIDIAN))
                .withMain(ItemRegistry.EXO_CIRCUITS.get())
                .with(Tags.Items.OBSIDIAN)
                .with(ItemRegistry.XION_LENS.get())
                .with(Tags.Items.OBSIDIAN)
                .with(ItemRegistry.XION_LENS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("roundel_tap"), new ItemStack(BlockRegistry.ROUNDEL_TAP.get(), 2))
                .unlockedBy("has", has(ItemRegistry.DEMAT_CIRCUIT.get()))
                .withMain(ItemRegistry.DEMAT_CIRCUIT.get())
                .with(Blocks.LIGHTNING_ROD)
                .with(ItemRegistry.XION_LENS.get())
                .with(Tags.Items.BARRELS)
                .with(Blocks.LIGHTNING_ROD)
                .with(ItemRegistry.XION_LENS.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("upgrades/atrium"), new ItemStack(ItemRegistry.UPGRADE_ATRIUM.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(ItemRegistry.UPGRADE_BASE.get())
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Items.PISTON)
                .with(ItemRegistry.EXO_CIRCUITS.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/still"), new ItemStack(BlockRegistry.ALEMBIC_STILL.get()))
                .unlockedBy("has", has(BlockRegistry.DATA_MATRIX.get()))
                .withMain(BlockRegistry.ALEMBIC.get())
                .with(ItemRegistry.FLUID_LINKS.get())
                .with(BlockRegistry.DATA_MATRIX.get())
                .with(Tags.Items.GLASS)
                .with(Tags.Items.GLASS)
                .with(Tags.Items.GLASS)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/fabricator"), new ItemStack(BlockRegistry.FABRICATOR_MACHINE.get()))
                .unlockedBy("has", has(ItemRegistry.ARTRON_CAP_BASIC.get()))
                .withMain(BlockRegistry.QUANTISCOPE.get())
                .with(BlockRegistry.DRAFTING_TABLE.get())
                .with(ItemRegistry.ARTRON_CAP_BASIC.get())
                .with(ItemRegistry.XION_LENS.get())
                .with(Tags.Items.INGOTS_IRON)
                .with(Tags.Items.INGOTS_IRON)
                .save(consumer);
        QuantiscopeCraftingRecipe.Builder.create(Helper.createRL("machines/hypertube"), new ItemStack(ItemRegistry.TUBE_ITEM.get(), 2))
                .unlockedBy("has", has(Items.PHANTOM_MEMBRANE))
                .withMain(Items.PHANTOM_MEMBRANE)
                .with(Tags.Items.GLASS)
                .with(ItemRegistry.DATA_CRYSTAL.get())
                .with(Blocks.DISPENSER)
                .with(Tags.Items.GLASS)
                .with(ItemRegistry.ARTRON_CAP_BASIC.get())
                .save(consumer);

    }
}
