package net.tardis.mod.datagen;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.tardis.mod.helpers.Helper;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class AnimatedBakedTextureProvider implements DataProvider {

    public final ExistingFileHelper fileHelper;
    public final DataGenerator generator;
    public final String modid;
    public Map<ResourceLocation, AnimatedTextureBuilder> builders = new HashMap<>();

    public AnimatedBakedTextureProvider(DataGenerator generator, String modid, ExistingFileHelper existingFileHelper){
        this.generator = generator;
        this.modid = modid;
        this.fileHelper = existingFileHelper;

    }

    public void registerTextures(){
        this.tex("block/decoration/alabaster/alabaster_blue_runner_lights")
                .frame(20)
                .interpolate();
        this.tex("block/decoration/alabaster/alabaster_screen")
                .frame(20)
                .interpolate();
        this.tex("block/decoration/alabaster/alabaster_pipes")
                .frame(20)
                .interpolate();

    }

    public AnimatedTextureBuilder tex(ResourceLocation texture){
        AnimatedTextureBuilder tex = new AnimatedTextureBuilder();
        this.builders.put(texture, tex);
        return tex;
    }

    public AnimatedTextureBuilder tex(String tex){
        return tex(new ResourceLocation(this.modid, tex));
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache) {
        this.registerTextures();

        CompletableFuture<?>[] tasks = new CompletableFuture[this.builders.size()];

        int index = 0;
        for(ResourceLocation key : this.builders.keySet()){
            final AnimatedTextureBuilder builder = this.builders.get(key);
            tasks[index] = DataProvider.saveStable(cache, builder.toJson(), getPath(key));

            ++index;
        }

        return CompletableFuture.allOf(tasks);
    }

    public Path getPath(ResourceLocation loc){
        String path = loc.getPath();
        if(!path.endsWith(".png")){
            path += ".png";
        }
        return this.generator.getPackOutput().getOutputFolder()
                .resolve("assets")
                .resolve(this.modid)
                .resolve("textures")
                .resolve(path + ".mcmeta");
    }

    @Override
    public String getName() {
        return "Animated Baked Texture Provider";
    }

    public static class AnimatedTextureBuilder{

        int frameTime = -1;
        boolean interpolate = false;

        public AnimatedTextureBuilder(){

        }

        public AnimatedTextureBuilder frame(int time){
            this.frameTime = time;
            return this;
        }

        public AnimatedTextureBuilder interpolate(){
            this.interpolate = true;
            return this;
        }

        public JsonObject toJson(){
            JsonObject root = new JsonObject();

            JsonObject animObj = new JsonObject();
            if(frameTime > 0)
                animObj.add("frametime", new JsonPrimitive(this.frameTime));
            if(interpolate)
                animObj.add("interpolate", new JsonPrimitive(true));
            root.add("animation", animObj);
            return root;
        }

    }
}
