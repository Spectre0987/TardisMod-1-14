package net.tardis.mod.datagen;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.concurrent.CompletableFuture;

public class TardisExportAllBlockIds implements DataProvider {

    protected final CompletableFuture<HolderLookup.Provider> completable;
    protected final PackOutput output;

    public TardisExportAllBlockIds(PackOutput output, CompletableFuture<HolderLookup.Provider> completable){
        this.output = output;
        this.completable = completable;
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache) {
        return this.completable.thenCompose(reg -> {

            JsonObject root = new JsonObject();
            JsonArray validIds = new JsonArray();
            for(ResourceLocation loc : ForgeRegistries.BLOCKS.getKeys()){
                validIds.add(loc.toString());
            }
            root.add("valid_ids", validIds);

            return DataProvider.saveStable(cache, root, this.output.getOutputFolder().resolve("masterlist.json"));
        });
    }

    @Override
    public String getName() {
        return "TARDIS Block Key Exporter";
    }
}
