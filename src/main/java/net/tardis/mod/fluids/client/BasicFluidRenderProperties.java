package net.tardis.mod.fluids.client;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.tardis.mod.fluids.BasicFluidType;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class BasicFluidRenderProperties implements IClientFluidTypeExtensions {

    final Supplier<BasicFluidType> type;

    public BasicFluidRenderProperties(Supplier<BasicFluidType> type){
        this.type = type;
    }

    @Override
    public @Nullable ResourceLocation getOverlayTexture() {
        return this.type.get().still;
    }

    @Override
    public ResourceLocation getStillTexture() {
        return this.type.get().still;
    }

    @Override
    public ResourceLocation getFlowingTexture() {
        return this.type.get().flowing;
    }

    @Override
    public int getTintColor() {
        return this.type.get().tint;
    }
}
