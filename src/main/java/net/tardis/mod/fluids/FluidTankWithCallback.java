package net.tardis.mod.fluids;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.templates.FluidTank;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class FluidTankWithCallback extends FluidTank {

    public Consumer<FluidTankWithCallback> changedCallback = (t) -> {};

    public FluidTankWithCallback withCallback(Consumer<FluidTankWithCallback> callback){
        this.changedCallback = callback;
        return this;
    }

    public FluidTankWithCallback(int capacity) {
        super(capacity);
    }

    public FluidTankWithCallback(int capacity, Predicate<FluidStack> validator) {
        super(capacity, validator);
    }

    @Override
    protected void onContentsChanged() {
        super.onContentsChanged();
        this.changedCallback.accept(this);
    }
}
