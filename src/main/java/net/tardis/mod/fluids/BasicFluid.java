package net.tardis.mod.fluids;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.fluids.FluidType;

import java.util.function.Supplier;

public class BasicFluid extends FlowingFluid {

    final Supplier<? extends FluidType> type;
    final Supplier<? extends LiquidBlock> block;

    public BasicFluid(Supplier<? extends FluidType> type, Supplier<? extends LiquidBlock> block){
        this.type = type;
        this.block = block;
    }

    @Override
    public Fluid getFlowing() {
        return FluidRegistry.MERCURY.get();
    }

    @Override
    public Fluid getSource() {
        return FluidRegistry.MERCURY.get();
    }

    @Override
    public Vec3 getFlow(BlockGetter pBlockReader, BlockPos pPos, FluidState pFluidState) {
        return Vec3.ZERO;
    }

    @Override
    protected boolean canConvertToSource(Level p_256009_) {
        return false;
    }

    @Override
    protected void beforeDestroyingBlock(LevelAccessor pLevel, BlockPos pPos, BlockState pState) {

    }

    @Override
    public float getHeight(FluidState pState, BlockGetter pLevel, BlockPos pPos) {
        return 1F;
    }

    @Override
    public float getOwnHeight(FluidState pState) {
        return 1.0F;
    }

    @Override
    protected int getSlopeFindDistance(LevelReader pLevel) {
        return 0;
    }

    @Override
    protected int getDropOff(LevelReader pLevel) {
        return 0;
    }

    @Override
    public Item getBucket() {
        return Items.BUCKET;
    }

    @Override
    protected boolean canBeReplacedWith(FluidState pState, BlockGetter pLevel, BlockPos pPos, Fluid pFluid, Direction pDirection) {
        return false;
    }

    @Override
    public int getTickDelay(LevelReader pLevel) {
        return 0;
    }

    @Override
    protected float getExplosionResistance() {
        return 0;
    }

    @Override
    protected BlockState createLegacyBlock(FluidState pState) {
        return this.block.get().defaultBlockState();
    }

    @Override
    public boolean isSource(FluidState pState) {
        return true;
    }

    @Override
    public int getAmount(FluidState pState) {
        return 0;
    }

    @Override
    public FluidType getFluidType() {
        return this.type.get();
    }

    @Override
    protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> pBuilder) {
        super.createFluidStateDefinition(pBuilder);
        pBuilder.add(LEVEL);
    }
}
