package net.tardis.mod.fluids;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.fluids.FluidType;
import net.tardis.mod.fluids.client.BasicFluidRenderProperties;

import java.util.function.Consumer;

public class BasicFluidType extends FluidType {
    public final int tint;
    public final ResourceLocation still;
    public final ResourceLocation flowing;

    public BasicFluidType(Properties properties, int tint, ResourceLocation still, ResourceLocation flowing) {
        super(properties);
        this.tint = tint;
        this.still = still;
        this.flowing = flowing;
    }

    public BasicFluidType(Properties prop, int tint, ResourceLocation both){
        this(prop, tint, both, both);
    }

    @Override
    public void initializeClient(Consumer<IClientFluidTypeExtensions> consumer) {
        super.initializeClient(consumer);
        consumer.accept(new BasicFluidRenderProperties(() -> this));
    }
}
