package net.tardis.mod.fluids;

import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;

public class FluidRegistry {

    public static final DeferredRegister<FluidType> FLUID_TYPES = DeferredRegister.create(ForgeRegistries.Keys.FLUID_TYPES, Tardis.MODID);
    public static final DeferredRegister<Fluid> FLUIDS = DeferredRegister.create(ForgeRegistries.FLUIDS, Tardis.MODID);

    //Fluid Types
    public static final RegistryObject<BasicFluidType> MERCURY_TYPE = FLUID_TYPES.register("mercury", () -> new BasicFluidType(FluidType.Properties.create()
            .canConvertToSource(false)
            .canDrown(true),
            0xFFFFFFFF,
            Helper.createRL("block/mercury_still")
    ));

    public static final RegistryObject<BasicFluidType> BIOMASS_TYPE = FLUID_TYPES.register("biomass", () -> new BasicFluidType(FluidType.Properties.create()
            .canDrown(false)
            .canConvertToSource(false), 0xFF005500, Helper.createRL("block/mercury_still")
    ));

    //Fluids
    public static final RegistryObject<BasicFluid> MERCURY = FLUIDS.register("mercury", () -> new BasicFluid(MERCURY_TYPE, BlockRegistry.MERCURY_FLUID));
    public static final RegistryObject<BasicFluid> BIOMASS = FLUIDS.register("biomass", () -> new BasicFluid(BIOMASS_TYPE, BlockRegistry.BIOMASS_FLUID));

    public static void register(IEventBus bus){
        FLUID_TYPES.register(bus);
        FLUIDS.register(bus);
    }

}
