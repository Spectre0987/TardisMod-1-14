package net.tardis.mod.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.SharedSuggestionProvider;
import net.minecraft.commands.arguments.DimensionArgument;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class CommandRegistry {

    public static final SuggestionProvider<CommandSourceStack> GET_TARDISES = (context, builder) -> {
        List<ResourceLocation> loc = new ArrayList<>();
        for(ServerLevel level : context.getSource().getLevel().getServer().getAllLevels()){
            Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> loc.add(level.dimension().location()));
        }
        return SharedSuggestionProvider.suggestResource(loc, builder);
    };

    public static final Supplier<RequiredArgumentBuilder<CommandSourceStack, ResourceLocation>> TARDIS_ARG = () -> Commands.argument("tardis", DimensionArgument.dimension()).suggests(GET_TARDISES);


    private static final List<Supplier<ITardisSubCommand>> SUB_COMMANDS = new ArrayList<>();

    public static void register(Supplier<ITardisSubCommand> command){
        SUB_COMMANDS.add(command);
    }

    public static void registerSubCommands(){
        register(EmotionalCommands::new);
        register(UnlockCommand::new);
        register(AttuneItemCommand::new);
        register(FuelCommand::new);
        register(InteriorCommand::new);
        register(RiftCommand::new);
        register(VortexPhenomenaCommand::new);
        register(DebugCommand::new);
    }

    public static void register(CommandDispatcher<CommandSourceStack> dispatch){
        final LiteralArgumentBuilder<CommandSourceStack> root = Commands.literal("tardis");
        registerSubCommands();
        for(Supplier<ITardisSubCommand> command : SUB_COMMANDS){
            dispatch.register(command.get().build(Commands.literal("tardis").requires(s -> s.hasPermission(2))));
        }
    }

    public static Component createCommandFeedback(String feedback){
        return Component.translatable(Tardis.MODID + ".commands.feedback.");
    }


    public static LazyOptional<ITardisLevel> getTardis(CommandContext<CommandSourceStack> context){
        final ResourceKey<Level> tardisKey = ResourceKey.create(Registries.DIMENSION, context.getArgument("tardis", ResourceLocation.class));
        return Capabilities.getCap(Capabilities.TARDIS, context.getSource().getServer().getLevel(tardisKey));
    }

}
