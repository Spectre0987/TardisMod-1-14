package net.tardis.mod.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.IAttunable;

public class AttuneItemCommand implements ITardisSubCommand {

    public static final String ATTUNED_ITEM_TRANS = Tardis.MODID + ".commands.feedback.attune.success";

    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(
                Commands.literal("attune").then(
                        CommandRegistry.TARDIS_ARG.get().executes(AttuneItemCommand::attune)
                )
        );
    }

    public static int attune(CommandContext<CommandSourceStack> context){

        ServerPlayer player = context.getSource().getPlayer();
        LazyOptional<ITardisLevel> tardisHolder = CommandRegistry.getTardis(context);
        final ItemStack held = player.getMainHandItem();
        if(tardisHolder.isPresent()){
            if(held.getItem() instanceof IAttunable at){
                player.setItemInHand(InteractionHand.MAIN_HAND, at.onAttuned(tardisHolder.orElseThrow(NullPointerException::new), held));
                player.displayClientMessage(Component.translatable(ATTUNED_ITEM_TRANS, held.getDisplayName().getString()), true);
                return 1;
            }
        }

        return 0;
    }
}
