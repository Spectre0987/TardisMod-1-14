package net.tardis.mod.commands;

import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.misc.tardis.TardisEngine;

public class DebugCommand implements ITardisSubCommand{
    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(Commands.literal("debug")
                .then(CommandRegistry.TARDIS_ARG.get()
                .then(Commands.literal("setup").executes(DebugCommand::setup))
                        .then(Commands.literal("light_level").then(Commands.argument("light_level", FloatArgumentType.floatArg(0.0F, 1.0F))
                                        .executes(DebugCommand::setLight)))
                )
        );
    }

    public static int setup(CommandContext<CommandSourceStack> stack){
        LazyOptional<ITardisLevel> tardisHolder = CommandRegistry.getTardis(stack);
        if(tardisHolder.isPresent()){
            final ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
            final ItemStackHandler capInv = tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.CAPACITORS);
            final ItemStackHandler engInv = tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.SUBSYSTEMS);

            for(int i = 0; i < capInv.getSlots(); ++i){
                capInv.setStackInSlot(i, new ItemStack(ItemRegistry.ARTRON_CAP_HIGH.get()));
            }
            tardis.getFuelHandler().getMaxArtron();//Update max artron vals
            tardis.getFuelHandler().fillArtron(Float.MAX_VALUE, false);

            engInv.setStackInSlot(0, new ItemStack(ItemRegistry.DEMAT_CIRCUIT.get()));
            engInv.setStackInSlot(1, new ItemStack(ItemRegistry.NAV_COM.get()));
            engInv.setStackInSlot(2, new ItemStack(ItemRegistry.CHAMELEON_CIRCUIT.get()));
            engInv.setStackInSlot(3, new ItemStack(ItemRegistry.TEMPORAL_GRACE.get()));

            engInv.setStackInSlot(4, new ItemStack(ItemRegistry.FLUID_LINKS.get()));
            engInv.setStackInSlot(5, new ItemStack(ItemRegistry.STABILIZER.get()));
            engInv.setStackInSlot(6, new ItemStack(ItemRegistry.INTERSTITAL_ANTENNA.get()));
            engInv.setStackInSlot(7, new ItemStack(ItemRegistry.SHIELD_GENERATOR.get()));


        }
        return -1;
    }

    public static int setLight(CommandContext<CommandSourceStack> s){
        float amount = FloatArgumentType.getFloat(s, "light_level");
        CommandRegistry.getTardis(s).ifPresent(tardis -> {
            tardis.getInteriorManager().setLightLevel(amount);
        });
        return 0;
    }

}
