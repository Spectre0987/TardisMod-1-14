package net.tardis.mod.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.SharedSuggestionProvider;
import net.minecraft.commands.arguments.ResourceLocationArgument;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.misc.tardis.vortex.VortexPheonomena;
import net.tardis.mod.registry.VortexPhenomenaRegistry;
import net.tardis.mod.world.data.TardisLevelData;

import java.util.Optional;

public class VortexPhenomenaCommand implements ITardisSubCommand {

    public static final SuggestionProvider<CommandSourceStack> VORTEXS = (context, builder) -> {
        return SharedSuggestionProvider.suggestResource(VortexPhenomenaRegistry.REGISTRY.get().getKeys(), builder);
    };

    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(
                Commands.literal("vp").then(Commands.argument("pos", BlockPosArgument.blockPos()).then(
                        Commands.literal("create")
                                .then(
                                        Commands.argument("vortex", ResourceLocationArgument.id()
                                            ).suggests(VORTEXS).executes(VortexPhenomenaCommand::createPhenomena)
                                ))
                        .then(Commands.literal("remove"))
                )
        );
    }

    public static int createPhenomena(CommandContext<CommandSourceStack> stack){
        final VortexPhenomenaType<?> type = VortexPhenomenaRegistry.REGISTRY.get().getValue(ResourceLocationArgument.getId(stack, "vortex"));

        final ChunkPos cPos = new ChunkPos(BlockPosArgument.getBlockPos(stack, "pos"));
        final VortexPheonomena vp = type.create(cPos);
        vp.onGenerated(stack.getSource().getServer().getLevel(Level.OVERWORLD));

        TardisLevelData.get(stack.getSource().getLevel()).addPhenomena(cPos, Optional.of(vp));
        return 0;
    }

}
