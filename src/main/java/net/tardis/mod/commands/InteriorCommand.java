package net.tardis.mod.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.TeleportEntry;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.misc.tardis.InteriorDoorData;

public class InteriorCommand implements ITardisSubCommand{
    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(
                Commands.literal("interior")
                        .then(CommandRegistry.TARDIS_ARG.get().executes(InteriorCommand::teleportSelf)
                                .then(Commands.argument("entity", EntityArgument.entity()).executes(InteriorCommand::teleportOther)))
        );
    }

    private static int teleportSelf(CommandContext<CommandSourceStack> context) {
        LazyOptional<ITardisLevel> tardisHolder = CommandRegistry.getTardis(context);
        if(tardisHolder.isPresent()){
            ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
            tardis.getInteriorManager().getDoorHandler().setDoorState(tardis.getInteriorManager().getDoorHandler().validDoorStates, DoorState.CLOSED);
            final InteriorDoorData data = tardis.getInteriorManager().getMainInteriorDoor();
            final ServerPlayer player = context.getSource().getPlayer();

            TeleportEntry.add(new TeleportEntry(
                    player,
                    (ServerLevel) tardis.getLevel(), data.placeAtMe(player, tardis), player.getYHeadRot()
            ));
            return 1;
        }
        return 0;
    }

    public static int teleportOther(CommandContext<CommandSourceStack> context) throws CommandSyntaxException {
        Entity e = EntityArgument.getEntity(context, "entity");
        LazyOptional<ITardisLevel> tardisHolder = CommandRegistry.getTardis(context);
        if(tardisHolder.isPresent()){
            ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
            tardis.getInteriorManager().getDoorHandler().setDoorState(tardis.getInteriorManager().getDoorHandler().validDoorStates, DoorState.CLOSED);
            final InteriorDoorData data = tardis.getInteriorManager().getMainInteriorDoor();

            TeleportEntry.add(new TeleportEntry(
                    e,
                    (ServerLevel) tardis.getLevel(), data.placeAtMe(e, tardis), e.getYHeadRot()
            ));
            return 1;
        }
        return 0;
    }
}
