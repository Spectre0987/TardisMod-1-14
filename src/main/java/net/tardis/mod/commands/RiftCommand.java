package net.tardis.mod.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.ChunkPos;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.rifts.Rift;

import java.util.Optional;

public class RiftCommand implements ITardisSubCommand{
    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(Commands.literal("rift")
                .then(Commands.argument("pos", BlockPosArgument.blockPos())
                        .then(Commands.literal("create")
                                .executes(RiftCommand::createRift))
                                .then(Commands.literal("status").executes(RiftCommand::getStatus))
                        ));
    }

    public static int createRift(CommandContext<CommandSourceStack> context){
        BlockPos riftLocation = BlockPosArgument.getBlockPos(context, "pos");
        final ChunkPos riftChunkPos = new ChunkPos(riftLocation);
        Capabilities.getCap(Capabilities.CHUNK, context.getSource().getLevel().getChunk(riftChunkPos.x, riftChunkPos.z)).ifPresent(chunk -> {
            chunk.setRift(new Rift(chunk.getChunk(), riftLocation.subtract(riftChunkPos.getWorldPosition())));
            Tardis.LOGGER.debug("created rift");
        });
        return 1;
    }

    public static int getStatus(CommandContext<CommandSourceStack> context){
        Optional<Rift> riftHolder = Rift.getFromChunk(context.getSource().getLevel().getChunkAt(BlockPosArgument.getBlockPos(context, "pos")));
        if(riftHolder.isEmpty()){
            //TODO: INform player it's empty
            return 0;
        }
        context.getSource().getPlayer().sendSystemMessage(Component.literal(riftHolder.get().getStoredArtron() + " artron in rift"), false);

        return 1;
    }
}
