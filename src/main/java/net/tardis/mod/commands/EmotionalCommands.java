package net.tardis.mod.commands;

import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.DimensionArgument;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.TraitRegistry;
import net.tardis.mod.resource_listener.server.TraitListener;

import java.util.Map;
import java.util.UUID;

public class EmotionalCommands implements ITardisSubCommand {


    public static final String LOYALTY_SET_TRANS = Tardis.MODID + ".commands.emotional.loyalty.set";

    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(
                Commands.literal("emotional")
                        .then(Commands.argument("tardis", DimensionArgument.dimension()).suggests(CommandRegistry.GET_TARDISES)
                        .then(
                                Commands.literal("list")
                                        .then(Commands.literal("loyalty").executes(EmotionalCommands::listLoyalty))
                                        .then(Commands.literal("traits").executes(EmotionalCommands::listTraits))
                        )
                        .then(Commands.literal("set")
                                .then(Commands.literal("loyalty")
                                        .then(Commands.argument("player", EntityArgument.player())
                                                .then(Commands.argument("amount", IntegerArgumentType.integer(-100, 100))
                                                .executes(EmotionalCommands::setLoyalty))))
                                //.then(Commands.literal("traits").executes(EmotionalCommands::setTraits))
                        )
                )
        );
    }

    public static int listLoyalty(CommandContext<CommandSourceStack> context) throws CommandSyntaxException{

        ServerLevel tardis = DimensionArgument.getDimension(context, "tardis");
        if(tardis != null){
            MutableComponent feedback = Component.literal("Loyalties\n");
            tardis.getCapability(Capabilities.TARDIS).ifPresent(t -> {
                for(Map.Entry<UUID, Integer> entry : t.getEmotionalHandler().getLoyalties().entrySet()){
                    ServerPlayer player = tardis.getServer().getPlayerList().getPlayer(entry.getKey());
                    feedback.append((player != null ? player.getDisplayName() : Component.literal(entry.getKey().toString())))
                            .append(" : ")
                            .append(Component.literal(entry.getValue() + "\n"));
                }
            });

            context.getSource().getPlayer().displayClientMessage(feedback, false);
            return 1;
        }

        return 0;
    }

    public static int listTraits(CommandContext<CommandSourceStack> context) throws CommandSyntaxException{
        ServerLevel level = DimensionArgument.getDimension(context, "tardis");
        if(level != null){

            Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
                final MutableComponent feedback = Component.literal("Traits:\n");
                for(Trait trait : tardis.getEmotionalHandler().getTraits()){
                    Helper.getKeyFromValueHashMap(TraitListener.traits, trait).ifPresent(key -> {
                        feedback.append(Component.literal(key.toString()).append("\n"));
                    });
                }
                context.getSource().getPlayer().sendSystemMessage(feedback);
            });

            return 1;
        }
        return 0;
    }

    public static int setLoyalty(CommandContext<CommandSourceStack> context){
        try {
            final ServerPlayer player = EntityArgument.getPlayer(context, "player");
            final int amount = IntegerArgumentType.getInteger(context, "amount");

            LazyOptional<ITardisLevel> tardisHolder = CommandRegistry.getTardis(context);
            if(tardisHolder.isPresent()){
                ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
                tardis.getEmotionalHandler().modLoyalty(player.getUUID(), amount);
                player.sendSystemMessage(Component.translatable(LOYALTY_SET_TRANS, player.getDisplayName().getString(), tardis.getEmotionalHandler().getLoyalty(player.getUUID())));
                return 0;
            }
            else return -1;


        }
        catch(CommandSyntaxException e){
            return -1;
        }
    }
}
