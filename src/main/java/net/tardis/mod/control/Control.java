package net.tardis.mod.control;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.sound.SoundRegistry;

/**
 * @author Lilith
 * The basic template of a Tardis Control
 * Extend this for custom controls
 *
 */
public abstract class Control<D extends ControlData<?>> extends TypeHolder<ControlType<D>> {

    public static final Component MISSING_REQUIRED_SYSTEM = Component.translatable("tardis.feedback.control.missing_system");

    public Control(ControlType<D> type){
        super(type);
    }

    public abstract InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level);
    public InteractionResult onPunch(Player player, ITardisLevel level){
        return onUse(player, InteractionHand.MAIN_HAND, level);
    }

    public D getData(ITardisLevel tardis){
        return tardis.getControlDataOrCreate(this.getType());
    }

    public static String makeTransKey(String name){
        return "message.control." + Tardis.MODID + "." + name;
    }

    public SoundEvent getDefaultSuccessSound(D controlData){
        return SoundRegistry.AXIS_CONTROL_SOUND.get();
    }

    public SoundEvent getDefaultFailSound(D controlData){
        return SoundEvents.DISPENSER_FAIL;
    }

}
