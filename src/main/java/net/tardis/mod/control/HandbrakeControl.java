package net.tardis.mod.control;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.misc.landing.TardisLandingContext;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.sound.SoundRegistry;

public class HandbrakeControl extends Control<ControlDataBool>{

    public HandbrakeControl(ControlType<ControlDataBool> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(hand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(level.isClient());

        ControlData<Boolean> data = level.getControlDataOrCreate(this.getType());
        data.set(!data.get());

        //If we just turned the handbrake on in flight while throttled up
        if(!level.isClient() && level.isInVortex() && data.get()){
            if(level.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get() > 0)
                level.crash(TardisLandingContext.basic(level));
            else level.initLanding(false);
        }
        else ThrottleControl.checkAndTakeoff(level);
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataBool data) {
        return data.get() ? SoundRegistry.HANDBRAKE_ON.get() : SoundRegistry.HANDBRAKE_OFF.get();
    }
}
