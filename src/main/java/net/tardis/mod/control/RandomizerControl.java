package net.tardis.mod.control;

import com.mojang.math.Axis;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataNone;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.sound.SoundRegistry;

public class RandomizerControl extends Control<ControlDataNone>{

    public RandomizerControl(ControlType<ControlDataNone> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(!level.getLevel().isClientSide) {
            final int scale = IncrementControl.getActualAmount(level);
            level.setDestination(level.getDestination().randomize(level.getLevel().random, scale == 0 ? 10 : scale));
            player.sendSystemMessage(Component.translatable(AxisControl.NOTIF_TRANS,
                    level.getDestination().getPos().getX(),
                    level.getDestination().getPos().getY(),
                    level.getDestination().getPos().getZ()
            ));
        }
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataNone data) {
        return SoundRegistry.RANDOMIZER_CONTROL.get();
    }
}
