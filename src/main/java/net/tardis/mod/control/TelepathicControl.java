package net.tardis.mod.control;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public class TelepathicControl extends Control{

    public TelepathicControl(ControlType type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(!level.isClient()){
            Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(GuiDatas.TELEPATHIC.create()));
        }
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
