package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataFloat extends ControlData<Float>{

    public ControlDataFloat(ControlType type, ITardisLevel tardis) {
        super(type, tardis, 0F);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeFloat(this.get());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readFloat());
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putFloat("value", this.get());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.set(tag.getFloat("value"));
    }
}
