package net.tardis.mod.control.datas;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.Control;
import net.tardis.mod.control.ControlType;

public class DoorControl extends Control<ControlDataNone> {
    public DoorControl(ControlType<ControlDataNone> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        level.getInteriorManager().getDoorHandler().onInteract(player, player.getItemInHand(hand), hand);
        return InteractionResult.sidedSuccess(level.isClient());
    }
}
