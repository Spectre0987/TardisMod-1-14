package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataItemStack extends ControlData<ItemStack>{
    public ControlDataItemStack(ControlType type, ITardisLevel tardis) {
        super(type, tardis, ItemStack.EMPTY);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeItemStack(this.get(), false);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readItem());
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.put("item", this.get().serializeNBT());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.set(ItemStack.of(nbt.getCompound("item")));
    }
}
