package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

public class ControlDataResourceKey extends ControlData<ResourceLocation>{


    public ControlDataResourceKey(ControlType type, ITardisLevel tardis) {
        super(type, tardis, new ResourceLocation("empty"));
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeResourceLocation(this.get());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(buf.readResourceLocation());
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putString("loc", this.get().toString());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.set(new ResourceLocation(nbt.getString("loc")));
    }
}
