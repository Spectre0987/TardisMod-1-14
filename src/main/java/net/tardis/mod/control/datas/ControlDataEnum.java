package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;

import java.util.function.Function;

public class ControlDataEnum<T extends Enum> extends ControlData<T>{

    public final Function<Integer, T> decoder;

    public ControlDataEnum(ControlType type, ITardisLevel tardis, Function<Integer, T> decoder, T defaultValue) {
        super(type, tardis, defaultValue);
        this.decoder = decoder;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.get().ordinal());
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.set(this.decoder.apply(buf.readInt()));
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("enum", this.get().ordinal());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.set(this.decoder.apply(tag.getInt("enum")));
    }
}
