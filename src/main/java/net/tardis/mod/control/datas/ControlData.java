package net.tardis.mod.control.datas;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ControlDataMessage;

import javax.annotation.Nullable;

/** Generic wrapper to apply data from a control to the Tardis Capability*/

public abstract class ControlData<T> implements INBTSerializable<CompoundTag> {

    public AnimationState usedState = new AnimationState();
    public long animationStartTime = Long.MAX_VALUE;

    private final ControlType type;
    private final ITardisLevel tardis;
    private T value;
    private T previous;

    public ControlData(ControlType type, @Nullable ITardisLevel tardis, T defaultValue){
        this.type = type;
        this.tardis = tardis;
        this.value = this.previous = defaultValue;
    }

    public ControlData(ControlType type, T defaultValue){
        this(type, null, defaultValue);
    }

    public ControlType getType(){
        return this.type;
    }

    public void set(T value){
        this.previous = this.value;
        this.value = value;
        if(previous != value)
            this.update();
    }

    public void update(){
        if(this.tardis != null && !this.tardis.getLevel().isClientSide()){
            ResourceKey<Level> levelKey = this.tardis.getLevel().dimension();
            Network.sendPacketToDimension(levelKey, new ControlDataMessage(levelKey, this));
        }
    }

    public T get(){
        return this.value;
    }

    public T getPrevious(){
        return this.previous;
    }

    public AnimationState getUseAnimationState(){
        return this.usedState;
    }

    public void playAnimation(int time, boolean force){
        if(time > this.animationStartTime + 20 || force){
            this.usedState.start(time);
            this.animationStartTime = time;
            this.update();
        }
    }
    public void playAnimation(int time){
        this.playAnimation(time, false);
    }

    public float getAnimationPercent(float time, int animationLength){
        this.usedState.updateTime(time, 1.0F);
        float secondsElapsed = this.usedState.getAccumulatedTime() / 1000.0F;
        return Mth.clamp(secondsElapsed / (animationLength / 20.0F), 0, 1);
    }

    public void copyTo(ControlData<T> data){
        data.set(this.get());
    }

    public abstract void encode(FriendlyByteBuf buf);
    public abstract void decode(FriendlyByteBuf buf);

}
