package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;

import java.util.function.Supplier;

public abstract class RequiresSubsystemControl<D extends ControlData<?>> extends Control<D>{

    final Supplier<? extends SubsystemType<?>> requiredSystemType;

    public RequiresSubsystemControl(ControlType<D> type, Supplier<? extends SubsystemType<?>> requiredSystemType) {
        super(type);
        this.requiredSystemType = requiredSystemType;
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(this.requiredSystemType.get().canBeUsed(level)){
            final Subsystem s = level.getSubsystem(this.requiredSystemType.get()).get();
            return useWithSubsystem(player, hand, s, level);
        }

        if(hand == InteractionHand.MAIN_HAND && !level.isClient())
            player.sendSystemMessage(Control.MISSING_REQUIRED_SYSTEM);
        return InteractionResult.FAIL;
    }

    public abstract InteractionResult useWithSubsystem(Player player, InteractionHand hand, Subsystem system, ITardisLevel level);
}
