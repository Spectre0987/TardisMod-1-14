package net.tardis.mod.control;

import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataEnum;

public class FacingControl extends Control<ControlDataEnum<Direction>>{

    public FacingControl(ControlType<ControlDataEnum<Direction>> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        ControlData<Direction> data = level.getControlDataOrCreate(this.getType());
        data.set(data.get().getClockWise());
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }
}
