package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataEnum;
import net.tardis.mod.misc.enums.LandingType;
import org.apache.logging.log4j.Level;

public class LandControl extends Control<ControlDataEnum<LandingType>>{

    public LandControl(ControlType<ControlDataEnum<LandingType>> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(hand == InteractionHand.OFF_HAND)
            return InteractionResult.PASS;

        if(!level.isClient()){
            ControlData<LandingType> landData = level.getControlDataOrCreate(this.getType());
            landData.set(landData.get() == LandingType.UP ? LandingType.DOWN : LandingType.UP);
        }
        return InteractionResult.sidedSuccess(level.isClient());
    }
}
