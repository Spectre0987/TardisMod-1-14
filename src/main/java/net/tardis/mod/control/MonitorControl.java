package net.tardis.mod.control;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.control.datas.ControlDataNone;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

import java.util.Optional;

public class MonitorControl extends Control<ControlDataNone>{


    public MonitorControl(ControlType<ControlDataNone> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(!level.isClient()){
            getMonData().ifPresent(rl -> {
                Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(GuiDatas.MON_MAIN.create().withData(rl)));
            });
        }
        return InteractionResult.sidedSuccess(level.isClient());
    }

    public Optional<ResourceLocation> getMonData(){
        //TODO: Make this console-dependant
        return Optional.of(MonitorData.GALVANIC);
    }
}
