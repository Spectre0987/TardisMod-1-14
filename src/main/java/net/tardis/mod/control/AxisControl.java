package net.tardis.mod.control;

import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataNone;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.subsystem.Subsystem;

public class AxisControl extends Control<ControlDataNone> {

    public static final String NOTIF_TRANS = "control." + Tardis.MODID + ".axis.report";

    public final Direction.Axis axis;

    public AxisControl(ControlType<ControlDataNone> type, Direction.Axis axis) {
        super(type);
        this.axis = axis;
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(!SubsystemRegistry.NAV_COM.get().canBeUsed(level)){
            return InteractionResult.PASS;
        }

        SpaceTimeCoord coord = level.getDestination();

        level.setDestination(coord.getLevel(), coord.getPos().relative(this.axis, IncrementControl.getActualAmount(level) * (player.isShiftKeyDown() ? -1 : 1)));
        player.displayClientMessage(Component.translatable(NOTIF_TRANS, level.getDestination().getPos().getX(), level.getDestination().getPos().getY(), level.getDestination().getPos().getZ()), true);
        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataNone data) {
        return SoundRegistry.AXIS_CONTROL_SOUND.get();
    }
}
