package net.tardis.mod.control;

import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataResourceKey;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TardisDimensionInfo;

import java.util.ArrayList;
import java.util.List;

public class DimensionControl extends Control<ControlDataResourceKey>{

    public static String TRANS = makeTransKey("dimension");

    public DimensionControl(ControlType<ControlDataResourceKey> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(!level.isClient()){
            List<Level> levels = getPossibleDimensions(level.getLevel().getServer(), level);

            int index = getIndexFrom(levels, ResourceKey.create(Registries.DIMENSION, (ResourceLocation) level.getControlDataOrCreate(this.getType()).get()));
            //change index and make sure it's in bounds
            index += player.isShiftKeyDown() ? -1 : 1;
            if(index < 0){
                index = levels.size() - 1;
            }
            else if(index >= levels.size()){
                index = 0;
            }

            final Level targetLevel = levels.get(index);
            final ResourceKey<Level> targetDim = targetLevel.dimension();

            level.setDestination(new SpaceTimeCoord(targetDim, level.getDestination().getPos()));
            level.getControlDataOrCreate(this.getType()).set(targetDim.location());
            player.sendSystemMessage(Component.translatable(TRANS, getName(level.getLevel().registryAccess(), targetLevel.dimensionTypeId())));

        }

        return InteractionResult.sidedSuccess(level.isClient());
    }

    public static int getIndexFrom(List<Level> levels, ResourceKey<Level> key){
        for(int i = 0; i < levels.size(); ++i){
            if(key.equals(levels.get(i).dimension())){
                return i;
            }
        }
        return 0;
    }

    public static List<Level> getPossibleDimensions(MinecraftServer server, ITardisLevel tardis){
        List<Level> levels = new ArrayList<>();
        for(Level level : server.getAllLevels()){

            TardisEvent.TardisTravelDimensionCheck event = new TardisEvent.TardisTravelDimensionCheck(tardis, level.dimension());
            final boolean canceled = MinecraftForge.EVENT_BUS.post(event);
            //Allow events to cancel travel to this dimension
            if(canceled)
                continue;
            //Allow events to force this dimension to be travelable
            if(event.isForceAllowed()){
                levels.add(level);
                continue;
            }

            boolean canTravel = true;
            for(TardisDimensionInfo info : server.registryAccess().registryOrThrow(JsonRegistries.TARDIS_DIM_INFO)){
                if(info.AppliesTo(level.dimensionTypeId())){
                    if(!info.canTravelTo()){
                        canTravel = false;
                    }
                    else if(info.canTravelTo() && info.mustBeUnlockedFirst()){ //If we can travel here but it needs to be unlocked first
                        if(!tardis.getUnlockHandler().isUnlocked(level.dimensionTypeId())){
                            canTravel = false;
                        }
                    }
                }
            }

            if(canTravel){
                levels.add(level);
            }

        }
        return levels;
    }

    public static String getName(RegistryAccess access, ResourceKey<DimensionType> key){
        for(TardisDimensionInfo info : access.registryOrThrow(JsonRegistries.TARDIS_DIM_INFO)){
            if(info.AppliesTo(key)){
                return info.customName();
            }
        }
        String name = key.location().getPath();
        if(name.contains("/")){
            name = name.substring(name.lastIndexOf('/'));
        }
        return name.replace('_', ' ');
    }
}
