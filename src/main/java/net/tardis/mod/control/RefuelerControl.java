package net.tardis.mod.control;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.config.Config;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.registry.SubsystemRegistry;
import org.apache.logging.log4j.Level;

import java.util.Optional;

public class RefuelerControl extends RequiresSubsystemControl<ControlDataBool>{

    public static final Component NO_CAP = Component.translatable(makeTransKey(ControlRegistry.REFUELER.getId().getPath()) + ".no_cap");
    public static final Component NO_RIFT = Component.translatable(makeTransKey(ControlRegistry.REFUELER.getId().getPath()) + ".no_rift");

    public RefuelerControl(ControlType<ControlDataBool> type) {
        super(type, SubsystemRegistry.FLUID_LINK_TYPE);
    }

    @Override
    public InteractionResult useWithSubsystem(Player player, InteractionHand hand, Subsystem system, ITardisLevel level) {
        if(!level.getLevel().isClientSide){
            ControlData<Boolean> fuel = level.getControlDataOrCreate(ControlRegistry.REFUELER.get());

            //If attempting to turn on fueling
            if(!fuel.get()){
                if(level.getFuelHandler().getMaxArtron() <= 0) {
                    player.sendSystemMessage(NO_CAP);
                }
                else if(!Config.Server.CAN_REFUEL_OUTIDE_RIFTS.get() && !level.getExteriorExtraData().IsInRift()){
                    player.sendSystemMessage(NO_RIFT);
                }
                else fuel.set(true);
            }
            else fuel.set(false);
        }
        return InteractionResult.sidedSuccess(level.isClient());
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataBool controlData) {
        return controlData.get() ? SoundRegistry.REFUEL_ON.get() : SoundRegistry.REFUEL_OFF.get();
    }
}
