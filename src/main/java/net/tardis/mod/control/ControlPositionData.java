package net.tardis.mod.control;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.CodecHelper;
import net.tardis.mod.helpers.JsonHelper;
import net.tardis.mod.registry.ControlRegistry;

import java.util.Optional;

/**
/**
 * @author Lilith
 * A data structure class to handle a Control's data such as position and size
 *
 */
public record ControlPositionData(Vec3 offset, Vec3 size, Optional<Holder<SoundEvent>> failSound, Optional<Holder<SoundEvent>> successSound) {


    public static final Codec<ControlPositionData> CODEC = RecordCodecBuilder.create(i ->
            i.group(
                    Vec3.CODEC.fieldOf("offset").forGetter(ControlPositionData::offset),
                    Vec3.CODEC.fieldOf("size").forGetter(ControlPositionData::size),
                    SoundEvent.CODEC.optionalFieldOf("fail_sound").forGetter(ControlPositionData::failSound),
                    SoundEvent.CODEC.optionalFieldOf("success_sound").forGetter(ControlPositionData::successSound)
            ).apply(i, ControlPositionData::new)
    );
}
