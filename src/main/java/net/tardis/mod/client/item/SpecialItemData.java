package net.tardis.mod.client.item;

import ca.weblite.objc.Client;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.renderers.SpecialItemRenderer;

public class SpecialItemData implements net.minecraftforge.client.extensions.common.IClientItemExtensions {


    @Override
    public BlockEntityWithoutLevelRenderer getCustomRenderer() {

        return ClientRegistry.ITEM_RENDERER.isPresent() ? ClientRegistry.ITEM_RENDERER.get() : IClientItemExtensions.super.getCustomRenderer();
    }
}
