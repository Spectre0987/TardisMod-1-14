package net.tardis.mod.client;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.serialization.JsonOps;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.ItemOverrides;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.*;
import net.minecraft.core.Direction;
import net.minecraft.core.RegistryAccess;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.RandomSource;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ModelEvent;
import net.minecraftforge.client.event.RegisterClientReloadListenersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.registry.SonicPartRegistry;
import net.tardis.mod.sonic_screwdriver.SonicPartConnectionPoint;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;
import org.jetbrains.annotations.Nullable;

import java.io.InputStreamReader;
import java.util.*;


@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class SonicPartModelLoader extends SimplePreparableReloadListener<Map<ResourceLocation, SonicPartConnectionPoint>> {

    public static SonicPartModelLoader INSTANCE = new SonicPartModelLoader();

    public final Map<ResourceLocation, SonicPartConnectionPoint> points = new HashMap<>();

    @SubscribeEvent
    public static void onReload(RegisterClientReloadListenersEvent event){
        event.registerReloadListener(INSTANCE);
    }

    @Override
    protected Map<ResourceLocation, SonicPartConnectionPoint> prepare(ResourceManager manager, ProfilerFiller pProfiler) {
        this.points.clear();

        Map<ResourceLocation, Resource> resources = manager.listResources("sonic_parts", rl -> rl.getPath().endsWith(".json"));
        for(Map.Entry<ResourceLocation, Resource> entry : resources.entrySet()){
            try{

                SonicPartConnectionPoint part = SonicPartConnectionPoint.CODEC.decode(JsonOps.INSTANCE, JsonParser.parseReader(new InputStreamReader(entry.getValue().open())))
                        .getOrThrow(false, s -> {}).getFirst();

                points.put(new ResourceLocation(
                        entry.getKey().getNamespace(),
                        entry.getKey().getPath().replace(".json", "")
                ), part);


            }
            catch(Exception e){
                Tardis.LOGGER.warn("Error parsing sonic part %s".formatted(entry.getKey().toString()));
                Tardis.LOGGER.warn(e);
            }
        }

        return points;
    }

    public static Optional<SonicPartConnectionPoint> pointFromReg(SonicPartRegistry.SonicPartType part){
        ResourceLocation key = SonicPartRegistry.REGISTRY.get().getKey(part).withPrefix("sonic_parts/");
        return INSTANCE.points.containsKey(key) ? Optional.of(INSTANCE.points.get(key)) : Optional.empty();
    }

    @Override
    protected void apply(Map<ResourceLocation, SonicPartConnectionPoint> list, ResourceManager pResourceManager, ProfilerFiller pProfiler) {}

    @SubscribeEvent
    public static void load(ModelEvent.RegisterAdditional event){

        for(SonicPartConnectionPoint point : INSTANCE.points.values()){
            event.register(point.model());
        }


    }
}
