package net.tardis.mod.client;

import com.google.common.collect.ImmutableMap;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.RenderStateShard;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.model.AtlasSet;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import org.apache.http.cookie.SM;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import java.nio.FloatBuffer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class TardisRenderTypes extends RenderType {

    public static final Function<ResourceLocation, RenderType> GUI_FLUID = Util.memoize(tex -> create("tardis_fluid",
            DefaultVertexFormat.POSITION_COLOR_TEX,
            VertexFormat.Mode.QUADS, 256,
            false,
            false,
            RenderType.CompositeState.builder()
                    .setTextureState(new TextureStateShard(tex, false, false))
                    .setShaderState(RenderStateShard.POSITION_COLOR_TEX_SHADER)
                    .createCompositeState(true)
            ));
    public static ShaderInstance SNOW_SHADER;
    public static ShaderInstance VORTEX_SHADER;
    public static ShaderInstance RIFT_SHADER;
    public static ShaderInstance FORCEFIELD_SHADER;
    public static ShaderInstance DEMAT_BLOCK_SHADER;

    public static VertexFormat SNOW_FORMAT = new VertexFormat(ImmutableMap.<String, VertexFormatElement>builder()
            .put("Position", DefaultVertexFormat.ELEMENT_POSITION)
            .put("Color", DefaultVertexFormat.ELEMENT_COLOR)
            .put("UV0", DefaultVertexFormat.ELEMENT_UV)
            .put("UV2", DefaultVertexFormat.ELEMENT_UV2)
            .put("Normal", DefaultVertexFormat.ELEMENT_NORMAL)
            .build());

    public static VertexFormat VORTEX_FORMAT = new VertexFormat(ImmutableMap.<String, VertexFormatElement>builder()
            .put("Position", DefaultVertexFormat.ELEMENT_POSITION)
            .put("UV0", DefaultVertexFormat.ELEMENT_UV)
            .put("Normal", DefaultVertexFormat.ELEMENT_NORMAL)
            .build());

    public static BiFunction<ResourceLocation, Vector4f, RenderType> SNOW_TYPE = Util.memoize((tex, snowColor) -> create("snow",SNOW_FORMAT, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE,
            false, false,
            RenderType.CompositeState.builder()
                    .setShaderState(new ShaderStateShardWith(() -> SNOW_SHADER, instance -> {
                        instance.getUniform("SnowColor").set(snowColor);
                    }))
                    .setTextureState(new TextureStateShard(tex, false, false))
                    .setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
                    //.setLayeringState(RenderType.VIEW_OFFSET_Z_LAYERING)
                    .setCullState(NO_CULL)
                    .setLightmapState(LIGHTMAP)
                    .createCompositeState(false)
        ));

    public static Function<ResourceLocation, RenderType> RIFT = Util.memoize(tex -> create("rift",
                DefaultVertexFormat.POSITION_TEX, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE, false, false,
                RenderType.CompositeState.builder()
                        .setShaderState(new ShaderStateShard(() -> RIFT_SHADER))
                        .setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
                        .setTextureState(new MultiTextureStateShard.Builder()
                                .add(tex, false, false)
                                .add(Helper.createRL("textures/level/vortex.png"), false, false)
                                .build())
                        .setCullState(NO_CULL)
                        .setLightmapState(NO_LIGHTMAP)
                        .createCompositeState(false)
            ));

    public static Function<Float, RenderType> FORCEFIELD = ticks -> create("force_field", DefaultVertexFormat.POSITION, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE, false, true,
            RenderType.CompositeState.builder()
                    .setShaderState(new ShaderStateShardWith(() -> FORCEFIELD_SHADER, shader -> RenderSystem.setShaderGameTime(Minecraft.getInstance().level.getGameTime(), ticks)))
                    .setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
                    .setCullState(CullStateShard.NO_CULL)
                    .setLightmapState(NO_LIGHTMAP)
                    .createCompositeState(false)

    );


    public static final RenderType VORTEX = create("vortex", VORTEX_FORMAT, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE,
            false, false,
            RenderType.CompositeState.builder()
                    .setShaderState(new ShaderStateShard(() -> VORTEX_SHADER))
                    .setDepthTestState(new DepthTestStateShard("test", GL11.GL_LEQUAL))
                    .setWriteMaskState(new WriteMaskStateShard(true, false))
                    .setLayeringState(RenderType.VIEW_OFFSET_Z_LAYERING)
                    .setTransparencyState(TRANSLUCENT_TRANSPARENCY)
                    .setLightmapState(NO_LIGHTMAP)
                    .setTextureState(MultiTextureStateShard.builder()
                            .add(Helper.createRL("textures/level/vortex.png"), false, false)
                            .add(Helper.createRL("textures/level/vortex_dust.png"), false, false)
                            .build())
                    .createCompositeState(false)

    );

    public static final Function<Float, RenderType> DEMAT_BLOCK_TYPE = alpha -> create("demat_blocks", DefaultVertexFormat.BLOCK, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE,
                true, true,
                RenderType.CompositeState.builder()
                        .setShaderState(new ShaderStateShardWith(() -> DEMAT_BLOCK_SHADER, shader -> {
                            shader.getUniform("DematAlpha").set(alpha);
                        }))
                        .setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
                        .setLightmapState(LIGHTMAP)
                        .setTextureState(BLOCK_SHEET_MIPPED)
                        //.setDepthTestState()
                        //.setOutputState(RenderStateShard.TRANSLUCENT_TARGET)
                        //.setWriteMaskState(COLOR_WRITE)
                        //.setLayeringState(VIEW_OFFSET_Z_LAYERING)
                        //.setOutputState(RenderStateShard.TRANSLUCENT_TARGET)
                        .createCompositeState(true)
            );

    public static final RenderType COLOR = create("color", DefaultVertexFormat.POSITION_COLOR, VertexFormat.Mode.QUADS, SMALL_BUFFER_SIZE,
            false, false,
            RenderType.CompositeState.builder()
                    .setShaderState(new ShaderStateShard(GameRenderer::getPositionColorShader))
                    .setCullState(NO_CULL)
                    .createCompositeState(false)
            );


    public static final ParticleRenderType RIFT_PARTICLE = new ParticleRenderType() {
        @Override
        public void begin(BufferBuilder bb, TextureManager pTextureManager) {
            RenderSystem.disableBlend();
            RenderSystem.depthMask(true);
            RenderSystem.setShader(() -> RIFT_SHADER);
            RenderSystem.setShaderTexture(0, new ResourceLocation("textures/particle/critical_hit.png"));
            RenderSystem.setShaderTexture(1, Helper.createRL("textures/level/vortex.png"));
            bb.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        }

        @Override
        public void end(Tesselator tess) {
            tess.end();
        }
    };



    public TardisRenderTypes(String pName, VertexFormat pFormat, VertexFormat.Mode pMode, int pBufferSize, boolean pAffectsCrumbling, boolean pSortOnUpload, Runnable pSetupState, Runnable pClearState) {
        super(pName, pFormat, pMode, pBufferSize, pAffectsCrumbling, pSortOnUpload, pSetupState, pClearState);
    }

    @OnlyIn(Dist.CLIENT)
    public static class ShaderStateShardWith extends ShaderStateShard{

        public ShaderStateShardWith(Supplier<ShaderInstance> instance, Consumer<ShaderInstance> instanceConsumer){
            super(instance);
            this.setupState = () -> {
                RenderSystem.setShader(instance);
                if(RenderSystem.getShader() != null){
                    instanceConsumer.accept(RenderSystem.getShader());
                }
            };
        }

    }

}
