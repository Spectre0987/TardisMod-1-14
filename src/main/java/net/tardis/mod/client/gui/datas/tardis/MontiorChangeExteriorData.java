package net.tardis.mod.client.gui.datas.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ExteriorRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MontiorChangeExteriorData extends GuiData {

    public Optional<ExteriorType> currentType = Optional.empty();
    public List<ExteriorType> unlockedTypes = new ArrayList<>();

    public MontiorChangeExteriorData(int id) {
        super(id);
    }

    public MontiorChangeExteriorData fromTardis(ITardisLevel tardis){
        this.currentType = Optional.of(tardis.getExterior().getType());

        //TODO: Add Unlock system
        for(ExteriorType type : ExteriorRegistry.REGISTRY.get()){
            ResourceKey<ExteriorType> typeLoc = ResourceKey.create(ExteriorRegistry.REGISTRY.get().getRegistryKey(), ExteriorRegistry.REGISTRY.get().getKey(type));
            if(!type.mustBeUnlocked() || tardis.getUnlockHandler().isUnlocked(typeLoc)) {
                this.unlockedTypes.add(type);
            }
        }
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        Helper.encodeOptional(buf, this.currentType, (type, b) -> b.writeRegistryId(ExteriorRegistry.REGISTRY.get(), type));
        Helper.encodeMultiple(buf, this.unlockedTypes, (t, b) -> b.writeRegistryId(ExteriorRegistry.REGISTRY.get(), t));
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.unlockedTypes.clear();
        this.currentType = Helper.decodeOptional(buf, b -> b.readRegistryId());
        this.unlockedTypes.addAll(Helper.decodeMultiple(buf, b -> b.readRegistryId()));
    }
}
