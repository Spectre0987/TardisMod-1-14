package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.misc.tardis.montior.MonitorFunction;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MonitorActionMessage;
import net.tardis.mod.registry.MonitorFunctionRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class MonitorScreen extends Screen {

    public static final Component NEXT_BUTTON = Component.translatable("monitor.button.tardis.next"),
        PREV_BUTTON = Component.translatable("monitor.button.tardis.prev");

    final List<String> submenu = new ArrayList<>();
    final MonitorData data;
    int left, top, right, bottom;

    String currentMenu = "";
    Screen previousMenu = null;

    int usedHeight = 0;

    public MonitorScreen(MonitorData data, String menu) {
        super(Component.empty());
        this.data = data;
        this.currentMenu = menu;
    }

    public MonitorScreen(MonitorData data, String menu, Screen previousMenu) {
        this(data, menu);
        this.previousMenu = previousMenu;
    }

    public MonitorScreen(MonitorData data){
        this(data, "");
    }

    public static void openSubScreen(Function<MonitorData, MonitorScreen> subscreen) {
        if(Minecraft.getInstance().screen instanceof MonitorScreen parent){
            Minecraft.getInstance().setScreen(subscreen.apply(parent.data));
        }
    }

    public <T extends MonitorScreen> MonitorScreen setPreviousScreen(T old){
        this.previousMenu = old;
        return this;
    }

    @Override
    protected void init() {
        super.init();
        this.usedHeight = 0;

        final int centerX = this.width / 2 - this.data.width() / 2,
                centerY = this.height / 2 - this.data.height() / 2;
        this.left = centerX + data.padLeft();
        this.top = centerY + data.padTop();
        this.right = centerX + this.data.width() - this.data.padRight();
        this.bottom = centerY + this.data.height() - this.data.padBottom();

        this.children().clear();

        this.setup();
    }

    public void openMenu(String menu){
        Minecraft.getInstance().setScreen(new MonitorScreen(this.data, menu, this));
    }

    public <T extends MonitorScreen> T openMenu(Function<MonitorData, T> factory){
        final T screen = factory.apply(this.data);
        screen.setPreviousScreen(this);
        Minecraft.getInstance().setScreen(screen);
        return screen;
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {

        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);

        RenderSystem.setShaderTexture(0, this.data.texture());
        this.blit(pPoseStack, this.width / 2 - this.data.width() / 2, this.height / 2 - this.data.height() / 2, 0, 0, this.data.width(), this.data.height());
        this.renderExtra(pPoseStack, pMouseX, pMouseY, pPartialTick);

        for(Renderable render : this.renderables){
            render.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        }

        //Render Frame
        if(this.data.frameTex() != null){
            RenderSystem.setShaderTexture(0, this.data.frameTex());
            this.blit(pPoseStack, this.width / 2 - this.data.width() / 2, this.height / 2 - this.data.height() / 2, 0, 0, this.data.width(), this.data.height());
        }

    }

    /**
     * Renders submenu-specific stuff
     * @param pose
     * @param mouseX
     * @param mouseY
     * @param partial
     */
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial){}

    /**
     * like {@link Screen#init()} for submenu-specific stuff
     */
    public void setup(){
        Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

            this.usedHeight = 4;
            this.createSubmenuList();
            final int x = this.left + 4;
            //Folders
            for(String folder : submenu){
                this.addNewFolder(folder, x);
            }

            //Protocols
            for(MonitorFunction function : MonitorFunctionRegistry.REGISTRY.get().getValues()){
                if(getFolderPath(function).equals(this.currentMenu) && function.shouldDisplay(tardis)){
                    this.addProtocol(function, tardis, x);
                }
            }
        });
    }

    public String getFolderPath(MonitorFunction function){
        String path = MonitorFunctionRegistry.REGISTRY.get().getKey(function).getPath();
        if(path.contains("/")){ //This is a sub folder of something
            return path.substring(0, path.lastIndexOf("/"));
        }
        return "";
    }

    public void createSubmenuList(){
        this.submenu.clear();
        for(MonitorFunction function : MonitorFunctionRegistry.REGISTRY.get()){
            String path = getFolderPath(function);

            if(!path.startsWith(this.currentMenu) || path.equals(this.currentMenu)){
                continue; // if this is not a child of the current submenu
            }

            //only get the next step
            if(path.lastIndexOf('/') > this.currentMenu.length() + 1){
                String nextStep = path.substring(0, path.indexOf('/', this.currentMenu.length() + 1));
                if(!this.submenu.contains(nextStep))
                    this.submenu.add(nextStep);
            }
            else if(!this.submenu.contains(path))
                this.submenu.add(path);

        }
    }

    public void addNewFolder(String folder, int x){
        this.addTextOption(x, createFolderTrans(folder), but -> {
            this.openMenu(folder);
        });
    }

    public void addProtocol(MonitorFunction function, ITardisLevel tardis, int x){
        this.addTextOption(x, function.getText(tardis), but -> {
            boolean result = function.doClientAction(tardis, Minecraft.getInstance().player);
            Network.sendToServer(new MonitorActionMessage(Minecraft.getInstance().player.getId(), function));
            if(result)
                Minecraft.getInstance().setScreen(null);
        });
    }

    public TextOption addTextOption(int x, Component text, Button.OnPress pressAction){
        final TextOption option = this.addRenderableWidget(new TextOption(x, top + usedHeight, this.font, text, pressAction));
        usedHeight += this.font.lineHeight + 2;
        return option;
    }

    public TextOption addTextOption(Component text, Button.OnPress pressAction){
        return this.addTextOption(this.left, text, pressAction);
    }

    public void addNextPrevButtons(Button.OnPress nextAction, Button.OnPress prevAction){
        this.addRenderableWidget(new TextOption(this.left, this.bottom - this.font.lineHeight, this.font, PREV_BUTTON, prevAction));
        this.addRenderableWidget(new TextOption(this.right - this.font.width(PREV_BUTTON), this.bottom - this.font.lineHeight, this.font, NEXT_BUTTON, nextAction));

    }

    public static Component createFolderTrans(String path){
        return Component.translatable("monitor.folders." + Tardis.MODID + "." + path.replace('/', '.'));
    }
}
