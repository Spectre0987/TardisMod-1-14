package net.tardis.mod.client.gui.dev;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.JsonOps;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.item.dev.PhasedOpticShellItem;
import net.tardis.mod.misc.PhasedShellDisguise;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.POSItemClearMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public class PhasedOpticShellSaverGui extends Screen {


    public static final String path = "tardis/pos/";

    final InteractionHand hand;
    final BlockPos start;
    final BlockPos end;
    final BlockPos exteriorPos;
    EditBox fileName;

    public PhasedOpticShellSaverGui(InteractionHand hand, BlockPos exteriorPos, BlockPos start, BlockPos end) {
        super(Component.empty());
        this.hand = hand;
        this.exteriorPos = exteriorPos;
        this.start = start;
        this.end = end;
    }

    @Override
    protected void init() {
        super.init();

        this.fileName = this.addRenderableWidget(new EditBox(this.font, 10, 10, 400, this.font.lineHeight + 2, Component.empty()));
        this.addRenderableWidget(Button.builder(Component.literal("Save"), but -> this.save())
                        .size(40, 20)
                        .pos(410 - 40, this.font.lineHeight + 22)
                .build());

        this.addRenderableWidget(Button.builder(Component.literal("Clear"), but -> {
                    Network.sendToServer(new POSItemClearMessage(this.hand));
        })
                        .pos(10, this.font.lineHeight + 22)
                        .size(40, 20)
        .build());

    }

    public void save(){

        final PhasedShellDisguise.Builder builder = new PhasedShellDisguise.Builder()
                .addBiomeTag(BiomeTags.IS_OVERWORLD);

        BlockPos.betweenClosed(Helper.min(this.start, this.end), Helper.max(this.start, this.end)).forEach(p -> {
            final BlockState state = Minecraft.getInstance().level.getBlockState(p);
            if(!state.isAir()){
                builder.add(p.subtract(this.exteriorPos), state);
            }
        });

        PhasedShellDisguise.CODEC.encodeStart(JsonOps.INSTANCE, builder.build()).result().ifPresent(json -> {
            Path filePath = Minecraft.getInstance().gameDirectory.toPath().resolve(path);
            filePath.toFile().mkdirs();

            try {
                final Gson g = new GsonBuilder().setPrettyPrinting().create();
                final JsonWriter write = g.newJsonWriter(new FileWriter(filePath.resolve(this.fileName.getValue() + ".json").toFile()));
                g.toJson(json, write);
                write.close();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        });

        Minecraft.getInstance().setScreen(null);
    }


    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        this.renderBackground(pPoseStack);
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        GuiHelper.drawCenteredText(pPoseStack, this.font, Component.literal("Are you sure this should be the exterior location?"), this.width / 2, this.height - 10, 0xFFFFFF);
    }
}
