package net.tardis.mod.client.gui.containers.quantiscope;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.tardis.mod.client.gui.widgets.ToggleButton;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.quantiscope.SonicUpgradeQuantiscopeMenu;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SonicUpgradeQuantiscopeMessage;

public class SonicUpgradeQuantiscopeMenuScreen extends BaseQuantiscopeMenuScreen<SonicUpgradeQuantiscopeMenu>{

    public static final ResourceLocation TEX = Helper.createRL("textures/screens/containers/quantiscope/sonic_upgrade_iron.png");

    public SonicUpgradeQuantiscopeMenuScreen(SonicUpgradeQuantiscopeMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
    }

    @Override
    protected void init() {
        this.imageWidth = 176;
        this.imageHeight = 166;
        this.titleLabelY = -8;
        super.init();
        this.addRenderableWidget(new ToggleButton(this.leftPos + 64, this.topPos + 47, 236, 88, 8, b -> {
            b.setState(!b.getState());
            Network.sendToServer(new SonicUpgradeQuantiscopeMessage(this.getMenu().setting.getParent().getBlockPos(), b.getState()));
        }));
    }

    @Override
    public ResourceLocation getTexture() {
        return TEX;
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pPoseStack);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, getTexture());
        blit(pPoseStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
    }
}
