package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SetDematAnimMessage;
import net.tardis.mod.registry.DematAnimationRegistry;

public class MonitorDematAnimScreen extends MonitorScreen{
    public MonitorDematAnimScreen(MonitorData data) {
        super(data);
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {



    }

    @Override
    public void setup() {
        for(DematAnimationRegistry.DematAnimationType type : DematAnimationRegistry.REGISTRY.get()){
            this.addTextOption(left, DematAnimationRegistry.makeTranslation(type), but -> {
                Network.sendToServer(new SetDematAnimMessage(type));
                Minecraft.getInstance().setScreen(null);
            });
        }
    }
}
