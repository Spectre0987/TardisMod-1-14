package net.tardis.mod.client.gui.datas;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.PacketHelper;
import net.tardis.mod.misc.TextureVariant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SonicTexVarGuiData extends GuiData{

    public final List<ResourceLocation> variants = new ArrayList<>();
    public BlockPos pos;
    public BlockState state;

    public SonicTexVarGuiData(int id) {
        super(id);
    }

    public SonicTexVarGuiData withVariants(BlockPos pos, BlockState state, List<ResourceLocation> variants){
        this.variants.addAll(variants);
        this.pos = pos;
        this.state = state;
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeBlockPos(this.pos);
        buf.writeRegistryId(ForgeRegistries.BLOCKS, this.state.getBlock());
        Helper.encodeMultiple(buf, this.variants, (tex, b) -> buf.writeResourceLocation(tex));
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.pos = buf.readBlockPos();
        this.state = ((Block)buf.readRegistryId()).defaultBlockState();
        this.variants.addAll(Helper.decodeMultiple(buf, FriendlyByteBuf::readResourceLocation));
    }
}
