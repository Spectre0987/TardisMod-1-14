package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.registry.JsonRegistries;

import java.util.ArrayList;
import java.util.List;

public class SonicARSGuiData extends GuiData{

    public List<ResourceLocation> unlockedRooms = new ArrayList<>();

    public SonicARSGuiData(int id) {
        super(id);
    }

    public SonicARSGuiData fromTardis(ITardisLevel tardis){
        tardis.getUnlockHandler().unlocked.stream().filter(key -> key.registry().equals(JsonRegistries.ARS_ROOM_REGISTRY.location())).forEach(key -> unlockedRooms.add(key.location()));
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.unlockedRooms.size());
        for(ResourceLocation rl : this.unlockedRooms){
            buf.writeResourceLocation(rl);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.unlockedRooms.add(buf.readResourceLocation());
        }
    }
}
