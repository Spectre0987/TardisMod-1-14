package net.tardis.mod.client.gui.sonic;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.client.gui.datas.SonicARSGuiData;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeARSRoomMessage;
import net.tardis.mod.registry.JsonRegistries;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SonicARSScreen extends Screen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/sonic/ars_sonic_gui.png");

    final List<ResourceLocation> arsRooms = new ArrayList<>();
    int roomIndex = 0;

    int guiLeft;
    int guiTop;

    @Nullable
    TextOption ARSButton;

    public SonicARSScreen(SonicARSGuiData data) {
        super(Component.empty());
        arsRooms.addAll(data.unlockedRooms);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        this.renderBackground(pPoseStack);
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
    }

    @Override
    public void renderBackground(PoseStack pose) {
        super.renderBackground(pose);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pose, (width - 159) / 2, (height - 158) / 2, 0, 0, 159, 158);

    }

    public void modIndex(int mod){
        if(this.roomIndex + mod >= this.arsRooms.size())
            this.roomIndex = 0;
        else if(roomIndex + mod < 0)
            this.roomIndex = this.arsRooms.size() - 1;
        else this.roomIndex += mod;

        if(ARSButton != null){
            this.removeWidget(ARSButton);
        }
        this.getSelectedRoom().ifPresent(room -> {
            this.addRenderableWidget(this.ARSButton = new TextOption(
                    guiLeft + 38, guiTop + 71, this.font,
                        Component.translatable(Constants.Translation.makeGenericTranslation(JsonRegistries.ARS_ROOM_REGISTRY, room)),
                        but -> {
                            Network.sendToServer(new ChangeARSRoomMessage(room));
                        }
                    ));
        });

    }

    public Optional<ResourceLocation> getSelectedRoom(){
        if(this.roomIndex >= 0 && this.roomIndex < this.arsRooms.size()){
            return Optional.of(this.arsRooms.get(this.roomIndex));
        }
        return Optional.empty();
    }

    @Override
    protected void init() {
        super.init();
        this.guiLeft = (this.width - 159) / 2;
        this.guiTop = (this.height - 158) / 2;

        //Next
        this.addRenderableWidget(new ImageButton(
                this.guiLeft + 147, this.guiTop + 115,
                11, 22,
                243, 3,
                TEXTURE,
                but -> modIndex(1)
        ));
        //Prev
        this.addRenderableWidget(new ImageButton(
                this.guiLeft + 4, this.guiTop + 115,
                11, 22,
                229, 3,
                TEXTURE,
                but -> modIndex(-1)
        ));
        this.modIndex(0);

    }
}
