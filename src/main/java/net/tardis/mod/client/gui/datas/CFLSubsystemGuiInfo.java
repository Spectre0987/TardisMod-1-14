package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.diagnostic_tool.CFLSubsytemInfoScreen;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CFLSubsystemGuiInfo extends DiagnosticMainScreenData {

    public List<SubsystemInfo> infos = new ArrayList<>();

    public CFLSubsystemGuiInfo(int id) {
        super(id);
    }

    public CFLSubsystemGuiInfo fromTardis(ITardisLevel tardis){
        this.tardis = Optional.of(tardis.getId());
        infos.addAll(SubsystemInfo.createList(tardis));
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        super.encode(buf);
        buf.writeInt(this.infos.size());
        for(SubsystemInfo info : this.infos){
            info.encode(buf);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        super.decode(buf);
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.infos.add(SubsystemInfo.decode(buf));
        }
    }

    public record SubsystemInfo(SubsystemType type, float health){

        public void encode(FriendlyByteBuf buf){
            buf.writeRegistryId(SubsystemRegistry.TYPE_REGISTRY.get(), this.type());
            buf.writeFloat(this.health());
        }

        public static SubsystemInfo decode(FriendlyByteBuf buf){
            return new SubsystemInfo(buf.readRegistryId(), buf.readFloat());
        }

        public static Optional<SubsystemInfo> create(ITardisLevel tardis, SubsystemType type){
            Optional<Subsystem> sys = tardis.getSubsystem(type);
            if(sys.isEmpty() || (sys.get().isBrokenOrOff() && !type.isRequiredForFlight))
                return Optional.empty();
            return Optional.of(new SubsystemInfo(type, sys.get().getSubsytemHealth()));
        }

        public static List<SubsystemInfo> createList(ITardisLevel level){
            List<SubsystemInfo> infos = new ArrayList<>();
            for(SubsystemType type : SubsystemRegistry.TYPE_REGISTRY.get()){
                create(level, type).ifPresent(info -> {
                    infos.add(info);
                });
            }
            return infos;
        }

    }
}
