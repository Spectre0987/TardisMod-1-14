package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;

public class TextOption extends Button {

    final Font font;

    public TextOption(int pX, int pY, Font font, Component pMessage, OnPress pOnPress) {
        super(pX, pY, font.width(pMessage.getString()), font.lineHeight, pMessage, pOnPress, (comp) -> MutableComponent.create(pMessage.getContents()));
        this.font = font;
    }

    @Override
    public void renderWidget(PoseStack pPoseStack, int p_275505_, int p_275674_, float p_275696_) {
        this.font.draw(pPoseStack, "> " + this.getMessage().getString(), this.getX(), this.getY(), 0x000000);
    }
}
