package net.tardis.mod.client.gui;

public interface IScreenKeyInput {

    /**
     *
     * @param key
     * @param action
     * @return - True if we should cancel this input event
     */
    boolean onKeyPress(int key, int action);

}
