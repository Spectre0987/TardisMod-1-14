package net.tardis.mod.client.gui.containers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.ItemDisplayContext;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.DraftingTableMenu;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.DraftingTableRecipe;

import java.util.Map;

public class DraftingTableScreen extends AbstractContainerScreen<DraftingTableMenu> {

    public static final ResourceLocation TEX = Helper.createRL("textures/screens/containers/trds_table.png");

    public DraftingTableScreen(DraftingTableMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
        this.imageWidth = 176;
        this.imageHeight = 166;
    }

    @Override
    protected void init() {
        this.titleLabelY = -8;
        super.init();
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pPoseStack);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEX);
        blit(pPoseStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        this.renderTooltip(pPoseStack, pMouseX, pMouseY);

        DraftingTableRecipe recipe = this.menu.table.getRecipe();
        if(this.menu.table.isRecipeValid()){
            int x = this.leftPos + 110;
            int y = this.topPos + 54;

            for(Map.Entry<ToolType, Integer> entry : recipe.tools.entrySet()){
                Minecraft.getInstance().getItemRenderer().renderGuiItem(pPoseStack, entry.getKey().getDisplay(), x, y);
                x += 18;
            }

        }

    }
}
