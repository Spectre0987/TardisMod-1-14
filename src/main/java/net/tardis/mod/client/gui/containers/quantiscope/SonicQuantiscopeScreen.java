package net.tardis.mod.client.gui.containers.quantiscope;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.client.SonicPartModelLoader;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.menu.quantiscope.SonicQuantiscopeMenu;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SonicChangeModelMessage;
import net.tardis.mod.registry.SonicPartRegistry;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class SonicQuantiscopeScreen extends BaseQuantiscopeMenuScreen<SonicQuantiscopeMenu> {

    public static final ResourceLocation TEX = Helper.createRL("textures/screens/containers/quantiscope/sonic_iron.png");
    public final EnumMap<SonicPartSlot, List<SonicPartRegistry.SonicPartType>> partMap = new EnumMap<>(SonicPartSlot.class);

    public SonicQuantiscopeScreen(SonicQuantiscopeMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);

        for(SonicPartRegistry.SonicPartType type : SonicPartRegistry.REGISTRY.get()){
            partMap.computeIfAbsent(type.getSlot(), t -> new ArrayList<>()).add(type);
        }
    }

    @Override
    protected void init() {
        this.imageWidth = 176;
        this.imageHeight = 166;
        this.titleLabelY = -8;
        super.init();

        for(SonicPartSlot slot : SonicPartSlot.values()){
            int xOffset = slot == SonicPartSlot.HANDLE ? 103 : 28 + (slot.ordinal() * 38);
            //Forward
            this.addRenderableWidget(new ImageButton(
                    this.leftPos + xOffset, this.topPos + 18, 6, 6, 178, 1, 7, getTexture(), but ->
                            this.setNext(slot, true)));
            //Backwards
            this.addRenderableWidget(new ImageButton(
                    this.leftPos + xOffset, this.topPos + 42, 6, 6, 178, 1, 7, getTexture(), but ->
                    this.setNext(slot, false)));
        }

    }

    @Override
    public ResourceLocation getTexture() {
        return TEX;
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pPoseStack);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEX);
        blit(pPoseStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);

        this.getMenu().setting.getInventory().ifPresent(inv -> {
            inv.getStackInSlot(0).getCapability(Capabilities.SONIC).ifPresent(sonic -> {
                this.renderSonicSlot(pPoseStack, sonic);
            });
        });

    }

    public void renderSonicSlot(PoseStack pose, ISonicCapability sonic){
        pose.pushPose();
        pose.translate(this.leftPos + 18, this.topPos + 22, 100);

        final BufferBuilder bb = Tesselator.getInstance().getBuilder();
        MultiBufferSource source = MultiBufferSource.immediate(bb);
        final VertexConsumer partConsumer = source.getBuffer(RenderType.cutout());
        RenderType.cutout().setupRenderState();
        int index = 0;
        for(SonicPartSlot slot : SonicPartSlot.values()){
            pose.pushPose();
            pose.translate(index * 40, 0, 0);
            pose.scale(20, 20, 20);
            pose.mulPose(Axis.XP.rotationDegrees(113));
            pose.mulPose(Axis.YP.rotationDegrees(90));
            pose.mulPose(Axis.XP.rotationDegrees(22));
            renderSonicPart(pose, slot, sonic, partConsumer);
            pose.popPose();
            ++index;
        }

        BufferUploader.drawWithShader(bb.end());

        pose.popPose();
        RenderType.cutout().clearRenderState();
    }

    public void renderSonicPart(PoseStack pose, SonicPartSlot slot, ISonicCapability sonic, VertexConsumer source){

        SonicPartModelLoader.pointFromReg(sonic.getSonicPart(slot)).ifPresent(point -> {
            final BakedModel model = Minecraft.getInstance().getModelManager().getModel(point.model());
            Minecraft.getInstance().getItemRenderer().renderModelLists(model, new ItemStack(ItemRegistry.SONIC.get()), LightTexture.FULL_BLOCK, OverlayTexture.NO_OVERLAY, pose, source);
        });
    }

    public int currentPartIndex(SonicPartRegistry.SonicPartType type){
        for(int i = 0; i < partMap.get(type.getSlot()).size(); ++i){
            if(type == partMap.get(type.getSlot()).get(i)){
                return i;
            }
        }
        return 0;
    }

    public void setNext(SonicPartSlot slot, boolean forward){
        this.getMenu().setting.getInventory().get().getStackInSlot(0).getCapability(Capabilities.SONIC).ifPresent(sonic -> {
            List<SonicPartRegistry.SonicPartType> validTypes = this.partMap.get(slot);
            int index = currentPartIndex(sonic.getSonicPart(slot));
            index += forward ? 1 : -1;
            if(index >= validTypes.size()){
                index = 0;
            }
            else if(index < 0){
                index = validTypes.size() - 1;
            }
            sonic.setSonicPart(slot, validTypes.get(index));
            Network.sendToServer(new SonicChangeModelMessage(this.menu.setting.getParent().getBlockPos(), sonic.getSonicPart(slot)));
        });
    }

    public int getModelSize(BakedModel model){
        return 0;
    }

}
