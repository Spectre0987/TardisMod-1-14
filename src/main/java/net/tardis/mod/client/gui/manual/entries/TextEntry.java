package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.client.gui.Font;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.FormattedText;
import net.minecraft.network.chat.Style;
import net.minecraft.util.Mth;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public class TextEntry extends ManualEntry {

    public static final Codec<TextEntry> TEXT_CODEC = RecordCodecBuilder.create(instance ->
        instance.group(
                EntryType.CODEC.fieldOf("type").forGetter(e -> EntryType.TEXT),
                ManualChapter.CODEC.fieldOf("chapter").forGetter(ManualEntry::getChapter),
                Codec.STRING.fieldOf("text").forGetter(e -> e.text)
        ).apply(instance, TextEntry::new)
    );

    final String text;
    final Style style = Style.EMPTY.withFont(Helper.createRL("manual"));

    public TextEntry(EntryType<?> type, ManualChapter chapter, String text) {
        super(type, chapter);
        this.text = text;
    }

    @Override
    public void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY){
        //font.drawWordWrap(pose, Component.literal(this.text), x, y, pageWidth, 0x000000);
        int usedHeight = nonTextSpace();
        for(FormattedText t : font.getSplitter().splitLines(Component.literal(text).withStyle(style), pageWidth, style)){
            font.draw(pose, Component.literal(t.getString()).withStyle(style), x, y + usedHeight, 0x000000);
            if(usedHeight + font.lineHeight >= pageHeight){
                return;
            }
            usedHeight += font.lineHeight;
        }
    }

    @Override
    public int getHeightUsed(Font font, int width){
        return Mth.ceil(font.wordWrapHeight(Component.literal(this.text).withStyle(style), width)) + nonTextSpace();
    }

    @Override
    public @Nullable ManualEntry split(Font font, int pageWidth, int pageHeight) {
        if(getHeightUsed(font, pageWidth) > pageHeight){ //If this is true, this one entry can never fit on a page, so split it out
            List<FormattedText> lines = font.getSplitter().splitLines(Component.literal(this.text).withStyle(style), pageWidth, this.style);
            //How many lines can fit on this page
            int blankSpaceLines = Mth.ceil(nonTextSpace() / (float)font.lineHeight);
            int amountOfLines = Mth.floor(pageHeight / (float)font.lineHeight) - blankSpaceLines;
            if(amountOfLines >= lines.size() - 1)
                return null;

            //Create new Entry with the rest
            return new TextEntry(EntryType.TEXT, this.chapter, joinAll(lines.subList(amountOfLines + 1, lines.size())));

        }
        return null;
    }

    public static String joinAll(Collection<FormattedText> lines){
        String s = "";
        for(FormattedText text : lines){
            s += text.getString() + "\n";
        }
        return s;
    }

    @Override
    public boolean isFullPage(){
        return false;
    }
}
