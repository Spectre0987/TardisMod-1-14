package net.tardis.mod.client.gui.datas;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.registry.TardisWorldStructureRegistry;
import oshi.util.tuples.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MonitorWaypointData extends MainMonitorData {

    public final List<WaypointDTO> waypoints = new ArrayList<>();

    public MonitorWaypointData(int id) {
        super(id);
    }

    public MonitorWaypointData fromTardis(ITardisLevel tardis){
        tardis.getInteriorManager().getAllOfType(TardisWorldStructureRegistry.WAYPOINT.get()).forEach(struc -> {
            for(Map.Entry<String, SpaceTimeCoord> entry : struc.getWaypoints().entrySet()){
                waypoints.add(new WaypointDTO(
                        entry.getKey(), entry.getValue(), struc.getPosition()
                ));
            }
        });
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        //super.encode(buf);
        buf.writeInt(this.waypoints.size());
        for(WaypointDTO dto : this.waypoints){
            dto.encode(buf);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        //super.decode(buf);
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.waypoints.add(WaypointDTO.decode(buf));
        }
    }

    public record WaypointDTO(String name, SpaceTimeCoord coord, BlockPos bankPosition){

        public void encode(FriendlyByteBuf buf){
            buf.writeInt(name().length());
            buf.writeUtf(name(), name().length());
            coord().encode(buf);
            buf.writeBlockPos(bankPosition());
        }

        public static WaypointDTO decode(FriendlyByteBuf buf){
            final int nameSize = buf.readInt();
            return new WaypointDTO(buf.readUtf(nameSize), SpaceTimeCoord.decode(buf), buf.readBlockPos());
        }

    }
}
