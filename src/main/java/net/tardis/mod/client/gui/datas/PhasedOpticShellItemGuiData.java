package net.tardis.mod.client.gui.datas;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.item.dev.PhasedOpticShellItem;

import java.util.Optional;

public class PhasedOpticShellItemGuiData extends GuiData{

    public InteractionHand hand;
    public BlockPos start;
    public BlockPos end;
    public BlockPos exterior;

    public PhasedOpticShellItemGuiData(int id) {
        super(id);
    }

    public PhasedOpticShellItemGuiData fromItem(InteractionHand hand, BlockPos start, BlockPos end, BlockPos exterior){
        this.hand = hand;
        this.start = start;
        this.end = end;
        this.exterior = exterior;
        return this;
    }

    public static Optional<PhasedOpticShellItemGuiData> createFromItem(InteractionHand hand, ItemStack stack, BlockPos exterior){
        Optional<BlockPos> startPos = PhasedOpticShellItem.getPosition(stack, true);
        Optional<BlockPos> endPos = PhasedOpticShellItem.getPosition(stack, false);
        if(startPos.isEmpty() || endPos.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(GuiDatas.POS_ITEM.create().fromItem(hand, startPos.get(), endPos.get(), exterior));
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeEnum(this.hand);
        buf.writeBlockPos(this.start);
        buf.writeBlockPos(this.end);
        buf.writeBlockPos(this.exterior);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.hand = buf.readEnum(InteractionHand.class);
        this.start = buf.readBlockPos();
        this.end = buf.readBlockPos();
        this.exterior = buf.readBlockPos();
    }
}
