package net.tardis.mod.client.gui.minigame.misc;

public class GameGrid<T> {

    public final int x, y, width, height, pieceWidth, pieceHeight;

    public GameGrid(int x, int y, int width, int height, int pieceWidth, int pieceHeight){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.pieceWidth = pieceWidth;
        this.pieceHeight = pieceHeight;
    }

    public boolean areCoordsIn(int mouseX, int mouseY){
        //Check width
        if(mouseX > this.x && mouseX < this.x + this.width){
            if(mouseY > this.y && mouseY < this.y + this.height)
                return true;
        }
        return false;
    }

    public void addPiece(int x, int y, T piece){}

    public T getPiece(int x, int y){
        return null; //TODO
    }

    public int getStepsX(){
        return (int)Math.floor(this.pieceWidth / this.width);
    }

    public int getStepsY(){
        return (int)Math.floor(this.pieceHeight / this.height);
    }

}
