package net.tardis.mod.client.gui.icons;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.resources.ResourceLocation;

public abstract class GuiIconRenderer {

    final Screen parentScreen;
    final ResourceLocation texture;

    public GuiIconRenderer(Screen screen, ResourceLocation texture){
        this.parentScreen = screen;
        this.texture = texture;
    }

    public Screen getParentScreen(){
        return this.parentScreen;
    }

    public abstract void render(PoseStack pose, int mouseX, int mouseY);

    public ResourceLocation getTexture(){
        return this.texture;
    }

}
