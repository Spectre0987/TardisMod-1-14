package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.helpers.Helper;

public class SonicItemButton extends Button {


    final ItemStack displayItem;
    final ResourceLocation texture;
    final int u = 143, v = 112, texWidth = 34, texHeight = 37;

    public SonicItemButton(int pX, int pY, ItemStack displayItem, ResourceLocation background, Button.OnPress pressAction) {
        super(pX, pY, 20, 20, Component.empty(), pressAction, (but) -> Component.empty());
        this.displayItem = displayItem;
        this.texture = background;
    }

    @Override
    public void renderWidget(PoseStack pose, int p_275505_, int p_275674_, float p_275696_) {

        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, this.texture);

        blit(pose, getX() + 8 - texWidth / 2, getY() + 8 - texHeight / 2, u, v, texWidth, texHeight);

        Minecraft.getInstance().getItemRenderer().renderGuiItem(pose, this.displayItem, this.getX(), this.getY());
    }
}
