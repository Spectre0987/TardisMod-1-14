package net.tardis.mod.client.gui.datas;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import net.tardis.mod.client.gui.datas.tardis.MonitorAtriumGuiData;
import net.tardis.mod.client.gui.datas.tardis.MonitorSoundData;
import net.tardis.mod.client.gui.datas.tardis.MontiorChangeExteriorData;
import net.tardis.mod.client.gui.datas.tardis.TardisFlightCourseData;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class GuiDatas {

    private static int CURRENT_ID = 0;
    private static Int2ObjectArrayMap<GuiDataRegistryObject<?>> DATAS = new Int2ObjectArrayMap<>();

    public static final GuiDataRegistryObject<DiagnosticMainScreenData> DIAGNOSTIC_MAIN = register(DiagnosticMainScreenData::new);
    public static final GuiDataRegistryObject<BrokenInfoGuiData> DIAG_BROKEN_INFO = register(BrokenInfoGuiData::new);
    public static final GuiDataRegistryObject<DiagTardisInfoData> DIAG_TARDIS_INFO = register(DiagTardisInfoData::new);
    public static final GuiDataRegistryObject<MontiorChangeExteriorData> MON_CHANGE_EXT = register(MontiorChangeExteriorData::new);
    public static final GuiDataRegistryObject<MonitorDematGuiData> MON_CHANGE_DEMAT_ANIM = register(MonitorDematGuiData::new);
    public static final GuiDataRegistryObject<MonitorSoundData> MON_SOUND_SCHEME = register(MonitorSoundData::new);
    public static final GuiDataRegistryObject<MainMonitorData> MON_MAIN = register(MainMonitorData::new);
    public static final GuiDataRegistryObject<MonitorWaypointData> MON_WAYPOINTS = register(MonitorWaypointData::new);
    public static final GuiDataRegistryObject<WaypointEditGuiData> MON_WAYPOINT_EDIT = register(WaypointEditGuiData::new);
    public static final GuiDataRegistryObject<ManualGuiData> MANUAL = register(ManualGuiData::new);
    public static final GuiDataRegistryObject<StructureBlockData> STRUCTURE = register(StructureBlockData::new);
    public static final GuiDataRegistryObject<ManualGuiData> TARDIS_MANUAL = register(ManualGuiData::new);
    public static final GuiDataRegistryObject<TardisFlightCourseData> MON_FLIGHT_COURSE = register(TardisFlightCourseData::new);
    public static final GuiDataRegistryObject<EmptyGuiData> TELEPATHIC = register(EmptyGuiData::new);
    public static final GuiDataRegistryObject<SonicGuiData> SONIC = register(SonicGuiData::new);
    public static final GuiDataRegistryObject<ARSEggGuiData> ARS_EGG = register(ARSEggGuiData::new);
    public static final GuiDataRegistryObject<CFLSubsystemGuiInfo> CFL_SUBSYSTEM_INFO = register(CFLSubsystemGuiInfo::new);

    public static final GuiDataRegistryObject<SonicARSGuiData> SONIC_ARS = register(SonicARSGuiData::new);

    public static final GuiDataRegistryObject<MonitorAtriumGuiData> MON_ATRIUM = register(MonitorAtriumGuiData::new);


    public static final GuiDataRegistryObject<PhasedOpticShellItemGuiData> POS_ITEM = register(PhasedOpticShellItemGuiData::new);
    public static final GuiDataRegistryObject<SonicNamingGuiData> SONIC_NAME = register(SonicNamingGuiData::new);
    public static final GuiDataRegistryObject<SonicTexVarGuiData> SONIC_TEX_VAR = register(SonicTexVarGuiData::new);



    public static <T extends GuiData> void ifMatches(GuiDataRegistryObject<T> reg, GuiData data, Consumer<T> action){
        if(data.id == reg.getId()){
            data.cast(reg).ifPresent(action);
        }
    }

    public static <T extends GuiData> GuiDataRegistryObject<T> register(Function<Integer, T> factory){
        final GuiDataRegistryObject<T> regObj = new GuiDataRegistryObject<>(id(), factory);
        DATAS.put(regObj.getId(), regObj);
        return regObj;
    }

    public static int id(){
        int cur = CURRENT_ID;
        CURRENT_ID++;
        return cur;
    }

    public static Optional<GuiData> createById(int id){
        GuiDataRegistryObject<?> reg = DATAS.get(id);
        return reg == null ? Optional.empty() : Optional.of(reg.create());
    }

    public static class GuiDataRegistryObject<T extends GuiData>{
        private final int id;
        private final Function<Integer, T> factory;

        public GuiDataRegistryObject(int id, Function<Integer, T> factory){
            this.id = id;
            this.factory = factory;
        }

        public final int getId(){
            return this.id;
        }

        public T create(){
            return this.factory.apply(this.id);
        }

    }

}
