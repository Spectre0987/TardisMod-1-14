package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.datas.tardis.MonitorAtriumGuiData;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SetMainAtriumMessage;

import java.util.HashMap;
import java.util.Map;

public class MonitorAtriumScreen extends MonitorScreen{

    public static final Component SELECT_TRANS = Component.translatable("screen." + Tardis.MODID + ".monitor.atrium.select");
    public final HashMap<String, BlockPos> atriums = new HashMap<>();

    public MonitorAtriumScreen(MonitorData data, MonitorAtriumGuiData monData) {
        super(data);
        this.atriums.putAll(monData.atriums);
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {
        GuiHelper.drawCenteredText(pose, font, SELECT_TRANS, width / 2, this.top, 0x000000);
    }

    @Override
    public void setup() {
        this.usedHeight = 20;
        for(Map.Entry<String, BlockPos> entry : this.atriums.entrySet()){
            this.addTextOption(Component.literal(entry.getKey()).append(" [").append(GuiHelper.formatBlockPos(entry.getValue())).append("]"), but -> {
                Network.sendToServer(new SetMainAtriumMessage(entry.getValue()));
            });
        }

    }
}
