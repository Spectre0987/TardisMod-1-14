package net.tardis.mod.client.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.PageHandler;
import net.tardis.mod.client.gui.manual.Page;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicMessage;

import java.util.Locale;

public class TelepathicScreen extends Screen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/telepathic_panel.png");
    public PageHandler<ResourceLocation> biomes;
    private int index = 0;

    public TelepathicScreen() {
        super(Component.empty());
    }

    @Override
    protected void init() {
        super.init();
        this.clearWidgets();

        if(biomes == null){
            this.biomes = new PageHandler<>(minecraft.level.registryAccess().registryOrThrow(Registries.BIOME).keySet(), 10);
        }

        this.addRenderableWidget(new Button.Builder(Component.literal(">"), but -> modIndex(1))
                .pos(width - 50 - 20, height - 70)
                .size(20, 20)
                .build());
        this.addRenderableWidget(new Button.Builder(Component.literal("<"), but -> modIndex(-1))
                .pos(50, height - 70)
                .size(20, 20)
                .build());
        this.addBiomeOptions();

    }

    public void addBiomeOptions(){

        final int centerX = width / 2, centerY = height / 2;
        int lineOffset = 0;
        for(ResourceLocation biomeKey : this.biomes.getItemsOnPage(this.index)){
            this.addRenderableWidget(new TextOption(centerX - 50, centerY - 50 + lineOffset, this.font, formatOption(biomeKey), buf -> {
                Network.sendToServer(new TelepathicMessage(biomeKey));
                Minecraft.getInstance().setScreen(null);
            }));
            lineOffset += font.lineHeight;
        }
    }

    public Component formatOption(ResourceLocation key){
        String text = key.getPath();
        //Remove path
        if(text.contains("/")){
            text = text.substring(text.lastIndexOf('/'));
        }
        text = text.replace('_', ' ');

        if(!key.getNamespace().equals("minecraft")){
            text = text + " [%s]".formatted(key.getNamespace());
        }

        return Component.literal(text);
    }

    public void modIndex(int mod){
        int newIndex = this.index + mod;

        if(newIndex >= biomes.getPages()){
            this.index = 0;
        }
        else this.index = newIndex;
        init();
    }

    @Override
    public void renderBackground(PoseStack pose) {
        super.renderBackground(pose);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pose, (width - 207) / 2, (height - 169) / 2, 0, 0, 207, 169);

    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        this.renderBackground(pPoseStack);
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
    }
}
