package net.tardis.mod.client.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.BookEditScreen;
import net.minecraft.client.gui.screens.inventory.PageButton;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.PageHandler;
import net.tardis.mod.client.gui.datas.ARSEggGuiData;
import net.tardis.mod.client.gui.widgets.ItemButton;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ARSSpawnItemMessage;
import net.tardis.mod.tags.ItemTags;

import java.util.ArrayList;
import java.util.List;

public class ARSEggScreen extends Screen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/ars_egg.png");
    public PageHandler<Item> pageHandler;
    public int page = 0;
    final int numItemRing = 20,
            numItemRing2 = 15,
            numItemRing3 = 10;

    public ARSEggScreen(ARSEggGuiData data) {
        super(Component.empty());
        this.pageHandler = new PageHandler<>(data.items,
                numItemRing + numItemRing2 + numItemRing3);
    }

    @Override
    protected void init() {
        this.setupButtons(0);
    }

    public void setupButtons(int pageIndex){
        this.clearWidgets();
        this.page = pageIndex;

        this.addRing(0, numItemRing, 80);
        this.addRing(numItemRing, numItemRing + numItemRing2, 60);
        this.addRing(numItemRing + numItemRing2, this.pageHandler.getItemsOnPage(this.page).size(), 40);

        final int widthPad = 70;

        this.addRenderableWidget(new PageButton(this.width / 2 + widthPad, this.height - 30, true, but -> modIndex(1), true));
        this.addRenderableWidget(new PageButton(this.width / 2 - widthPad, this.height - 30, false, but -> modIndex(-1), true));

    }

    public void modIndex(int mod){
        if(this.page + mod < 0){
            this.page = this.pageHandler.getPages() - 1;
        }
        else if(this.page + mod >= this.pageHandler.getPages())
            this.page = 0;
        else this.page += mod;
        this.setupButtons(this.page);
    }

    public void addRing(int fromIndex, int toIndex, int dist){

        //If the start index is greater than items on the page, cancel processing as this ring is empty anyway
        if(fromIndex >= this.pageHandler.getItemsOnPage(this.page).size())
            return;

        final List<Item> items = this.pageHandler.getItemsOnPage(this.page).subList(fromIndex, Math.min(toIndex, this.pageHandler.getItemsOnPage(this.page).size() - 1));

        final float rotationPer = 360.0F / (float)items.size();

        for(int i = 0; i < items.size(); ++i){
            final int x = (int)(this.width / 2 - 8 + Math.sin(Math.toRadians(rotationPer * i)) * dist),
            y = (int)(this.height / 2 - 8 + Math.cos(Math.toRadians(rotationPer * i)) * dist);

            final Item item = items.get(i);

            this.addRenderableWidget(new ItemButton(x, y, new ItemStack(item), but -> {
                Network.sendToServer(new ARSSpawnItemMessage(new ItemStack(item,
                        Minecraft.getInstance().player.isShiftKeyDown() ? item.getMaxStackSize() : 1
                )));
            }));
        }
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {

        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);

        blit(pPoseStack, (width - 256) / 2, (height - 256) / 2, 0, 0, 256, 256);

        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
    }
}
