package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.client.gui.manual.Page;
import net.tardis.mod.client.gui.manual.entries.FixedTOCPage;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;
import net.tardis.mod.resource_listener.client.ManualReloadListener;

import java.util.ArrayList;
import java.util.List;

public class MonitorManualScreen extends MonitorScreen{

    public Page page;
    public List<ManualEntry> entries = new ArrayList<>();
    public int index;

    public MonitorManualScreen(MonitorData data) {
        super(data);
    }


    @Override
    public void setup() {
        this.page = new Page(this.font, this.left, this.top, this.data.width() - data.padRight() - data.padLeft(), this.data.height() - this.data.padBottom() - data.padTop());

        this.entries.clear();
        this.entries.add(new FixedTOCPage(ManualReloadListener.entries));
        for(ManualReloadListener.ManualChapterLink link : ManualReloadListener.entries){
            addManualChapter(link);
        }

        this.addNextPrevButtons(but -> this.index++, but -> this.index--);
    }

    @Override
    public boolean mouseClicked(double pMouseX, double pMouseY, int pButton) {
        /*
        for(int i = 0; i < getPages().size(); ++i){
            final Page page = getPages().get(i);
            if(page.hasClickedOn((int)pMouseX, (int)pMouseY)){
                int entryIndex = this.index + i;
                if(entryIndex < this.entries.size()){
                    if(this.entries.get(entryIndex) instanceof FixedTOCPage toc){
                        ManualChapter selected_chapter = toc.onClickedIn(this, (int) pMouseX, (int)pMouseY);
                        if(selected_chapter != null){
                            this.moveToManualPage(e -> e.getChapter() != null && e.getChapter().name().equals(selected_chapter.name()));
                        }
                    }
                }
                return true;
            }
        }

         */
        return super.mouseClicked(pMouseX, pMouseY, pButton);
    }

    public void addManualChapter(ManualReloadListener.ManualChapterLink link){
        for(ManualEntry entry : link.entries()){
            this.addEntry(entry);
        }
        for(ManualReloadListener.ManualChapterLink child : link.child()){
            addManualChapter(child);
        }
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {
        super.renderExtra(pose, mouseX, mouseY, partial);
        page.renderEntries(pose, this.index, this.entries);
    }

    public void addEntry(ManualEntry entry){
        ManualEntry e = entry;
        while(e != null){
            this.entries.add(e);
            e = e.split(this.font, this.data.width() - this.data.padLeft() * 2, this.data.height() - this.data.padTop() * 2);
        }
    }
}
