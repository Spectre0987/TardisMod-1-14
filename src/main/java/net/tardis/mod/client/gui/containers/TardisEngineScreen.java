package net.tardis.mod.client.gui.containers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.elements.ToolTipArea;
import net.tardis.mod.client.gui.widgets.SegmentProgressBarWidget;
import net.tardis.mod.client.gui.widgets.ToggleButton;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.menu.EngineMenu;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.IAttunable;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ToggleEngineSlotMessage;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;
import org.jline.reader.Widget;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class TardisEngineScreen extends AbstractContainerScreen<EngineMenu> {

    public static final ResourceLocation SYSTEMS = Helper.createRL("textures/screens/containers/engine/subsystem.png");
    public static final ResourceLocation ATTUNEMENT = Helper.createRL("textures/screens/containers/engine/attunement.png");
    public static final ResourceLocation CAPACITOR = Helper.createRL("textures/screens/containers/engine/capacitor.png");
    public static final ResourceLocation UPGRADE = Helper.createRL("textures/screens/containers/engine/upgrade.png");

    public static final String SUBSYSTEM_FILTER_TRANS = "menu." + Tardis.MODID + ".engine.tooltip.subsystem_filter";

    public TardisEngineScreen(EngineMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, Component.empty());
    }

    @Override
    protected void init() {
        super.init();
        final TardisEngine.EngineSide side = TardisEngine.EngineSide.getForDirection(this.menu.side).get();
        //final TardisEngine engine = Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).

        if(side == TardisEngine.EngineSide.SUBSYSTEMS || side == TardisEngine.EngineSide.UPGRADES){

            List<Slot> engineSlots = menu.slots.stream().filter(s -> !(s.container instanceof Inventory)).toList();

            for(Slot slot : engineSlots){

                //Add toggles to each engine slot
                final int yOffset = slot.index + 1 > engineSlots.size() / 2 ?
                        18 + 1 : //If the bottom row
                        -ToggleButton.HEIGHT - 3; //If the top row

                ToggleButton button = this.addWidget(new ToggleButton(this.leftPos + slot.x - 1, this.topPos + slot.y + yOffset, 177, 2, but -> {
                    boolean newState = !but.getState();
                    but.setState(newState);
                    Minecraft.getInstance().player.playSound(newState ? SoundRegistry.SUBSYSTEM_ON.get() : SoundRegistry.SUBSYSTEM_OFF.get());
                    Network.sendToServer(new ToggleEngineSlotMessage(side, slot.index, but.getState()));
                }));
                button.setState(menu.isSlotActive(slot.index));

                //If Subsytems, Add tooltips letting you know what components go where
                if(side == TardisEngine.EngineSide.SUBSYSTEMS){
                    if(slot.index >= EngineMenu.TYPE_ORDER.length){
                        break; // If there are no more locked slots
                    }

                    final SubsystemType sysType = EngineMenu.TYPE_ORDER[slot.index];

                    this.addRenderableOnly(new ToolTipArea(this.leftPos + slot.x, this.topPos + slot.y, 18, 18, () -> Tooltip.create(Component.translatable(
                            SUBSYSTEM_FILTER_TRANS, Component.translatable(sysType.getTranslationKey())))));
                }
            }
        }

        //Set toggle states
        if(side == TardisEngine.EngineSide.SUBSYSTEMS){
            this.minecraft.level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                List<ToggleButton> buttons = this.children().stream().filter(c -> c instanceof ToggleButton).map(c -> (ToggleButton)c).toList();
                for(int i = 0; i < buttons.size(); ++i){
                    final ToggleButton butt = buttons.get(i);
                    if(i < EngineMenu.TYPE_ORDER.length){
                        tardis.getSubsystem(EngineMenu.TYPE_ORDER[i]).ifPresent(s -> {
                            butt.setState(s.isActivated());
                        });
                    }
                }
            });
        }

        if(this.menu.side == TardisEngine.EngineSide.CHARGING.getSide()){
            Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

                final ItemStack item = tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.CHARGING)
                        .getStackInSlot(TardisEngine.ATTUNEMENT_SLOT);

                final int maxTicks = item.getItem() instanceof IAttunable a ? a.getTicksToAttune() : 0;

                this.addRenderableOnly(new SegmentProgressBarWidget(this.getGuiLeft() + 111, this.getGuiTop() + 19, 32, 32)
                        .setProgressCallBack(() -> maxTicks == 0 ? 0 : menu.getAttunementTicks() / (float)maxTicks)
                        .addSegment(11, 0, 188, 1, 10, 5)
                        .addSegment(22, 0, 199, 1, 10, 10)
                        .addSegment(27, 11, 204, 12, 5, 10)
                        .addSegment(22, 22, 199, 23, 10, 10)
                        .addSegment(11, 27, 188, 28, 10, 5)
                        .addSegment(0, 22, 177, 23, 10, 10)
                        .addSegment(0, 11, 177, 12, 5, 10)
                        .addSegment(0, 0, 177, 1, 10, 10)
                );
            });
        }

    }



    @Override
    protected void renderLabels(PoseStack pPoseStack, int pMouseX, int pMouseY) {
        super.renderLabels(pPoseStack, pMouseX, pMouseY);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);

        RenderSystem.setShaderTexture(0, getTextureForSide(this.menu.getSide()));
        for(GuiEventListener listener : this.children()){
            if(listener instanceof Renderable render){
                render.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
            }
        }
        this.renderTooltip(pPoseStack, pMouseX, pMouseY);
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pPoseStack);

        RenderSystem.setShaderTexture(0, getTextureForSide(this.menu.getSide()));
        this.blit(pPoseStack, this.width / 2 - 176 / 2, this.height / 2 - 166 / 2, 0, 0, 176, 166);

        //Render Attunement
        if(this.menu.side == TardisEngine.EngineSide.CHARGING.getSide()){

        }


    }



    public boolean hasToggles(TardisEngine.EngineSide side){
        return side == TardisEngine.EngineSide.SUBSYSTEMS ||
                side == TardisEngine.EngineSide.UPGRADES;
    }

    public ResourceLocation getTextureForSide(@Nullable Direction dir){
        switch(dir){
            default: return SYSTEMS;
            case EAST: return UPGRADE;
            case SOUTH: return CAPACITOR;
            case WEST: return ATTUNEMENT;
        }
    }
}
