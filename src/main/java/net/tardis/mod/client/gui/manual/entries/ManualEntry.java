package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import net.minecraft.client.gui.Font;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.helpers.Helper;

import java.util.HashMap;
import java.util.Optional;

public abstract class ManualEntry {

    final EntryType<? extends ManualEntry> type;
    final ManualChapter chapter;

    public ManualEntry(EntryType<? extends ManualEntry> type, ManualChapter chapter){
        this.type = type;
        this.chapter = chapter;
    }

    public EntryType<? extends ManualEntry> getEntryType(){
        return this.type;
    }

    //Extra space at the top that no text should be rendered on
    public int nonTextSpace(){
        return 0;
    }

    /**
     *
     * @return true if this always takes a full page,
     * false if it can share the page with other entries
     */
    public abstract boolean isFullPage();
    public abstract void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY);
    public abstract int getHeightUsed(Font font, int width);

    public ManualEntry split(Font font, int pageWidth, int pageHeight){
        return null;
    }

    public ManualChapter getChapter(){
        return chapter;
    }

    public record EntryType<T extends ManualEntry>(ResourceLocation id, Codec<T> codec){


        public static final Codec<EntryType<?>> CODEC = ResourceLocation.CODEC.comapFlatMap(EntryType::fromCodec, EntryType::id);


        public static final HashMap<ResourceLocation, EntryType<?>> entries = new HashMap<>();


        public static final EntryType<TextEntry> TEXT = create(Helper.createRL("text"), TextEntry.TEXT_CODEC);
        public static final EntryType<ItemEntry> ITEM = create(Helper.createRL("item"), ItemEntry.CODEC);
        public static final EntryType<TitleEntry> TITLE = create(Helper.createRL("title"), TitleEntry.CODEC);
        public static final EntryType<VortexPhenomenonManualEntry> VP = create(Helper.createRL("vortex_phenomena"), VortexPhenomenonManualEntry.CODEC);



        public static void init(){}

        public <T extends ManualEntry> Codec<T> getCodec(){
            return (Codec<T>)codec();
        }

        public static Optional<EntryType<?>> fromRL(ResourceLocation type) {
            if (entries.containsKey(type)) {
                return Optional.of(entries.get(type));
            }
            return Optional.empty();
        }

        public static DataResult<EntryType<?>> fromCodec(ResourceLocation loc){
            for(ResourceLocation rl : entries.keySet()){
                if(rl.equals(loc)){
                    return DataResult.success(entries.get(loc));
                }
            }
            return DataResult.error(() -> "No valid manual entry type for %s".formatted(loc.toString()));
        }

        public static <T extends ManualEntry> EntryType<T> create(ResourceLocation rl, final Codec<T> codec){
            final EntryType<T> entry = new ManualEntry.EntryType<>(rl, codec);
            entries.put(rl, entry);
            return entry;
        }

    }
}
