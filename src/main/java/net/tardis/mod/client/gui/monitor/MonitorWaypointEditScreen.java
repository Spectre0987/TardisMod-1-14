package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Constants;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.datas.MonitorWaypointData;
import net.tardis.mod.client.gui.datas.WaypointEditGuiData;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.helpers.DimensionNameHelper;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DeleteWaypointMessage;
import net.tardis.mod.network.packets.LoadWaypointMessage;

public class MonitorWaypointEditScreen extends MonitorScreen{

    final MonitorWaypointData.WaypointDTO editingPoint;

    public MonitorWaypointEditScreen(MonitorData data, MonitorWaypointData.WaypointDTO gui) {
        super(data);
        this.editingPoint = gui;
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {
        GuiHelper.drawCenteredText(pose, this.font, Component.literal(this.editingPoint.name()), width / 2, this.top, 0x000000);
        GuiHelper.drawCenteredText(pose, this.font, Component.literal("%d, %d, %d".formatted(
                this.editingPoint.coord().getPos().getX(),
                this.editingPoint.coord().getPos().getY(),
                this.editingPoint.coord().getPos().getZ()
        )), width / 2, this.top + font.lineHeight + 5, 0x000000);
        GuiHelper.drawCenteredText(pose, this.font, Component.literal(DimensionNameHelper.getFriendlyName(Minecraft.getInstance().level, this.editingPoint.coord().getLevel())), width / 2, top + (font.lineHeight * 2) + 5, 0xFFFFFF);
        super.renderExtra(pose, mouseX, mouseY, partial);
    }

    @Override
    public void setup() {

        final Component delete = Component.literal("Delete");

        this.addRenderableWidget(new TextOption(left, bottom - font.lineHeight, this.font, Component.literal("Load"), b -> {
            Network.sendToServer(new LoadWaypointMessage(this.editingPoint.bankPosition(), this.editingPoint.name()));
            Minecraft.getInstance().setScreen(null);
        }));

        this.addRenderableWidget(new TextOption(right - this.font.width(delete), bottom - font.lineHeight, this.font, delete, b -> {
            Network.sendToServer(new DeleteWaypointMessage(this.editingPoint.bankPosition(), this.editingPoint.name()));
            Minecraft.getInstance().setScreen(null);
        }));
    }
}
