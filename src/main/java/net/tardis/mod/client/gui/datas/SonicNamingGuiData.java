package net.tardis.mod.client.gui.datas;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class SonicNamingGuiData extends GuiData{

    public BlockPos pos;
    public Optional<String> name = Optional.empty();


    public SonicNamingGuiData(int id) {
        super(id);
    }

    public SonicNamingGuiData create(Optional<String> name, BlockPos pos){
        this.name = name;
        this.pos = pos;
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeBlockPos(this.pos);
        Helper.encodeOptional(buf, this.name, (val, b) -> b.writeUtf(val));
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.pos = buf.readBlockPos();
        this.name = Helper.<String>decodeOptional(buf, b -> b.readUtf());
    }
}
