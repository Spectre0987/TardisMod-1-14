package net.tardis.mod.client.gui.containers.quantiscope;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Inventory;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.quantiscope.CraftingQuantiscopeMenu;

public class CraftingQuantiscopeScreen extends BaseQuantiscopeMenuScreen<CraftingQuantiscopeMenu> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/containers/quantiscope/weld_iron.png");

    public CraftingQuantiscopeScreen(CraftingQuantiscopeMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
    }

    @Override
    protected void renderBg(PoseStack pose, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(pose);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pose, width / 2 - this.imageWidth / 2, this.height / 2 - this.imageHeight / 2, 0, 0, imageWidth, imageHeight);

        float progress = this.menu.setting.getCraftingTicksLeft() == 0 ? 0 : (200 - this.menu.setting.getCraftingTicksLeft()) / 200.0F;
        blit(pose, this.leftPos + 86, this.topPos + 31, 179, 1, Mth.floor(21 * progress), 14);

    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        this.renderTooltip(pPoseStack, pMouseX, pMouseY);
    }

    @Override
    protected void init() {
        this.imageWidth = 176;
        this.imageHeight = 166;
        this.titleLabelY = -8;
        super.init();
    }

    @Override
    public ResourceLocation getTexture() {
        return TEXTURE;
    }
}
