package net.tardis.mod.client.gui.sonic;

import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.BufferUploader;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.tardis.mod.client.gui.datas.SonicTexVarGuiData;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.client.renderers.FakeRenderLevel;
import net.tardis.mod.misc.ITextureVariantBlockEntity;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateTextureVariantMessage;
import net.tardis.mod.registry.JsonRegistries;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SonicTextureVariantScreen extends BasicSonicScreen {

    final FakeRenderLevel fakeLevel = new FakeRenderLevel();
    public final BlockPos editingPos;
    public List<ResourceLocation> textureVariants = new ArrayList<>();
    int textureIndex = 0;

    public SonicTextureVariantScreen(SonicTexVarGuiData data) {
        super(Component.empty());
        this.textureVariants.addAll(data.variants);
        this.editingPos = data.pos;
        this.fakeLevel.setBlock(BlockPos.ZERO, data.state, 3);
        this.renderInnerSquare = false;
    }

    @Override
    protected void init() {
        super.init();

        this.addNextPrev(but -> modIndex(-1), but -> modIndex(1));
        this.addRenderableWidget(new ImageButton(
                (this.width - 20) / 2, (this.height + 125) / 2,
                20, 19, 207, 0, TEXTURE, but -> {
                    if(!this.textureVariants.isEmpty() && this.textureIndex < this.textureVariants.size()){
                        Network.sendToServer(new UpdateTextureVariantMessage(this.editingPos, Optional.of(this.textureVariants.get(this.textureIndex))));
                    }
                }
        ));
    }

    public void modIndex(int mod){

        if(this.textureVariants.isEmpty())
            return;

        int proposedIndex = this.textureIndex + mod;
        if(proposedIndex >= this.textureVariants.size())
            this.textureIndex = 0;
        else if(proposedIndex < 0)
            this.textureIndex = this.textureVariants.size() - 1;
        else this.textureIndex = proposedIndex;
        if(this.fakeLevel.getBlockEntity(BlockPos.ZERO) instanceof ITextureVariantBlockEntity e){
            e.setTextureVariant(Minecraft.getInstance().level.registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).get(this.textureVariants.get(this.textureIndex)));
        }
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partialTick) {

        BlockEntity renderTile = fakeLevel.getBlockEntity(BlockPos.ZERO);
        if(renderTile != null){

            pose.pushPose();

            pose.translate((width - 25) / 2, (height + 30) / 2, 30);
            pose.scale(20, 20, 20);
            pose.mulPose(Axis.XP.rotationDegrees(140));
            pose.mulPose(Axis.YP.rotationDegrees(45));

            final BufferBuilder bb = Tesselator.getInstance().getBuilder();
            MultiBufferSource.BufferSource source = MultiBufferSource.immediate(bb);

            BlockEntityRenderer renderer = Minecraft.getInstance().getBlockEntityRenderDispatcher().getRenderer(renderTile);
            if(renderer != null)
                renderer.render(renderTile, partialTick, pose, source, LightTexture.FULL_BLOCK, OverlayTexture.NO_OVERLAY);
            pose.popPose();

            source.endBatch();
            if(bb.building())
                BufferUploader.drawWithShader(bb.endOrDiscardIfEmpty());

        }

    }

    @Override
    public void tick() {
        super.tick();
    }
}
