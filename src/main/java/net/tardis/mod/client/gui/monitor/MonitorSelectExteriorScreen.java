package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Constants;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeExteriorMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class MonitorSelectExteriorScreen extends MonitorScreen{


    public static final Component SELECT_TRANS = Component.translatable(Constants.Translation.makeGuiTitleTranslation("monitor.exterior.select"));

    Optional<ExteriorType> currentType = Optional.empty();
    List<ExteriorType> unlockedTypes = new ArrayList<>();
    int index = 0;

    public MonitorSelectExteriorScreen(MonitorData data) {
        super(data);
    }

    public MonitorSelectExteriorScreen setData(Optional<ExteriorType> currentType, Collection<ExteriorType> unlockedTypes){
        this.currentType = currentType;
        this.unlockedTypes.clear();
        this.unlockedTypes.addAll(unlockedTypes);

        this.currentType.ifPresent(type -> {
            for(int i = 0; i < this.unlockedTypes.size(); ++i){
                if(this.unlockedTypes.get(i) == type){
                    this.index = i;
                    break;
                }
            }
        });

        return this;
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);

        this.currentType.ifPresent(type-> {

            this.font.draw(pPoseStack, type.getTranslation(), this.width / 2 - this.font.width(type.getTranslation()) / 2, this.top, 0x000000);

            GuiHelper.renderExteriorToGui(pPoseStack, type, this.width / 2, this.height / 2);
        });

    }

    public void modIndex(int mod){

        if(this.index + mod >= this.unlockedTypes.size()){
            this.index = 0;
        }
        else if(this.index + mod < 0){
            this.index = this.unlockedTypes.size() - 1;
        }
        else this.index += mod;

        this.currentType = Optional.of(this.unlockedTypes.get(this.index));

    }

    @Override
    public void setup() {
        this.addNextPrevButtons(b -> modIndex(1), b -> modIndex(-1));

        int selectSize = this.font.width(SELECT_TRANS);
        this.addRenderableWidget(new TextOption(
                this.left + ((this.right - this.left) / 2) - (selectSize / 2),
                this.bottom - this.font.lineHeight,
                this.font, SELECT_TRANS, but -> {
                    this.currentType.ifPresent(type -> {
                        Network.sendToServer(new ChangeExteriorMessage(type));
                    });
        }
        ));
    }
}
