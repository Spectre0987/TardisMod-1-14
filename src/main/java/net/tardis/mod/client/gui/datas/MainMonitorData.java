package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;

public class MainMonitorData extends GuiData{

    public ResourceLocation loc;

    public MainMonitorData(int id) {
        super(id);
    }

    public MainMonitorData withData(ResourceLocation loc){
        this.loc = loc;
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeResourceLocation(this.loc);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.loc = buf.readResourceLocation();
    }
}
