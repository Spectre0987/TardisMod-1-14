package net.tardis.mod.client.gui.datas.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.MainMonitorData;
import net.tardis.mod.registry.JsonRegistries;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MonitorSoundData extends MainMonitorData {

    public final List<ResourceLocation> soundsSchemes = new ArrayList<>();

    public MonitorSoundData(int id) {
        super(id);
    }

    public MonitorSoundData fromServer(MinecraftServer server){
        final Set<ResourceLocation> sound = server.registryAccess().registryOrThrow(JsonRegistries.SOUND_SCHEMES_REGISTRY).keySet();
        this.soundsSchemes.addAll(sound);
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.soundsSchemes.size());
        for(ResourceLocation loc : this.soundsSchemes){
            buf.writeResourceLocation(loc);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.soundsSchemes.add(buf.readResourceLocation());
        }
    }
}
