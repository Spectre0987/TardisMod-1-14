package net.tardis.mod.client.gui.manual;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;

import java.util.List;

public record Page(Font font, int x, int y, int width, int height){

    public boolean hasClickedOn(int mouseX, int mouseY){
        if(mouseX > x && mouseX < x + width){
            if(mouseY > y && mouseY < y + height){
                return true;
            }
        }
        return false;
    }

    public int renderEntries(PoseStack pose, int startingIndex, List<ManualEntry> allEntries){

        if(startingIndex >= allEntries.size() || startingIndex < 0)
            return startingIndex;

        int usedSpace = 0;
        for(int i = startingIndex; i < allEntries.size(); ++i){
            ManualEntry e = allEntries.get(i);

            if(startingIndex != i && e.isFullPage()){ //If we already have text on this page, but the next entry should be on it's own page
                return i;
            }

            if(e.isFullPage()){
                e.render(pose, this.font, this.x, this.y, this.width, this.height, 0, 0);
                return i + 1;
            }

            if(usedSpace != 0 && usedSpace + e.getHeightUsed(font, width) > this.height){
                return i;
            }

            e.render(pose, this.font, this.x, this.y + usedSpace, this.width, this.height - usedSpace, 0, 0);

            usedSpace += e.getHeightUsed(this.font, this.width) + this.font.lineHeight;

        }

        return startingIndex;
    }


}
