package net.tardis.mod.client.gui.monitor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SaveWaypointMessage;

public class MonitorWaypointSaveScreen extends MonitorScreen{

    public static final Component SAVE = Component.translatable(Tardis.MODID + ".monitor.screen.waypoint.save");
    public static final Component EXIT = Component.translatable(Tardis.MODID + ".monitor.screen.waypoint.exit");

    private EditBox waypointName;

    public MonitorWaypointSaveScreen(MonitorData data) {
        super(data);
    }

    @Override
    public void setup() {

        final int editSize = Mth.floor(this.data.width() * 0.75F);
        this.addRenderableWidget(this.waypointName = new EditBox(
                font,
                this.left + ((this.right - this.left) - editSize) / 2,
                this.top + data.padTop(),
                editSize, font.lineHeight,
                Component.empty()
        ));

        this.addRenderableWidget(new TextOption(this.right - this.font.width(SAVE) - this.data.padRight(), this.bottom - font.lineHeight, this.font, SAVE, b -> {
            Network.sendToServer(new SaveWaypointMessage(this.waypointName.getValue(), SaveWaypointMessage.Action.SAVE));
            Minecraft.getInstance().setScreen(null);
        }));
    }
}
