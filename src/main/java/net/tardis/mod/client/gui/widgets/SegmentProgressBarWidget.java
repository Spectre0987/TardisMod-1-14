package net.tardis.mod.client.gui.widgets;


import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.network.chat.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class SegmentProgressBarWidget extends AbstractWidget {

    public Supplier<Float> progressCallback = () -> 0.0F;
    public List<Segment> segments = new ArrayList<>();
    public int ticks = 0;

    public SegmentProgressBarWidget(int pX, int pY, int pWidth, int pHeight) {
        super(pX, pY, pWidth, pHeight, Component.empty());
    }

    public SegmentProgressBarWidget setProgressCallBack(Supplier<Float> callBack){
        this.progressCallback = callBack;
        return this;
    }

    public SegmentProgressBarWidget addSegment(Segment seg){
        this.segments.add(seg);
        return this;
    }

    public SegmentProgressBarWidget addSegment(int x, int y, int u, int v, int width, int height){
        return this.addSegment(new Segment(x, y, u, v, width, height));
    }

    @Override
    public void renderWidget(PoseStack pose, int p_268034_, int p_268009_, float p_268085_) {

        final float percent = this.progressCallback.get();
        final int segmentIndex = getCurrentSegment();

        if(percent <= 0)
            return;

        for(int i = 0; i < getCurrentSegment(); ++i){
            final Segment s = this.segments.get(i);
            blit(pose,this.getX() + s.x,
                    this.getY() + s.y,
                    s.u, s.v, s.width, s.height
            );
        }

        if(segmentIndex < this.segments.size()){
            if(System.currentTimeMillis() % 2000 < 1000){
                Segment s = this.segments.get(segmentIndex);
                blit(pose,this.getX() + s.x,
                        this.getY() + s.y,
                        s.u, s.v, s.width, s.height
                );
            }
        }

    }

    public int getCurrentSegment(){
        float currPercent = 0;
        final float percentPerStep = 1 / (float)this.segments.size();

        int index = 0;

        for(Segment s : this.segments){

            currPercent += percentPerStep;

            if(this.progressCallback.get() <= currPercent){
                return index;
            }
            ++index;
        }
        return index;
    }

    public void tick(){
        ++this.ticks;
    }


    @Override
    protected void updateWidgetNarration(NarrationElementOutput p_259858_) {

    }

    public record Segment(int x, int y, int u, int v, int width, int height){}
}
