package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.screens.Screen;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.resource_listener.client.ManualReloadListener;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FixedTOCPage extends ManualEntry {

    public final List<ChapterThing> hitBoxed = new ArrayList<>();
    final List<ManualReloadListener.ManualChapterLink> chapterLinks = new ArrayList<>();
    int chaptersAdded = 0;

    public FixedTOCPage(Collection<ManualReloadListener.ManualChapterLink> links) {
        super(null, null);
        this.chapterLinks.addAll(links);
    }

    @Override
    public boolean isFullPage() {
        return true;
    }

    @Override
    public void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY) {
        this.hitBoxed.clear();
        int yOffset = 0;
        for(ManualReloadListener.ManualChapterLink link : this.chapterLinks){
            if(yOffset + this.getTotalHeightFromLink(link, font) > pageHeight)
                return;
            yOffset += addChapter(pose, link, font, x, y + yOffset, 0, pageHeight);
            ++chaptersAdded;
        }
        System.out.println();
    }

    public int getTotalHeightFromLink(ManualReloadListener.ManualChapterLink link, Font font){
        int usedHeight = font.lineHeight;
        for(ManualReloadListener.ManualChapterLink child : link.child()){
            usedHeight += getTotalHeightFromLink(child, font);
        }
        return usedHeight;
    }

    public int addChapter(PoseStack pose, ManualReloadListener.ManualChapterLink link, Font font, int x, int y, int usedHeight, int pageHeight){

        font.draw(pose, link.chapter().makeTranslation(), x, y, 0x000000);
        this.hitBoxed.add(ChapterThing.create(link.chapter(), font, x, y));
        usedHeight += font.lineHeight;
        for(ManualReloadListener.ManualChapterLink child : link.child()){
            usedHeight += addChapter(pose, child, font, x + 7, y + usedHeight, 0, pageHeight);
        }
        return usedHeight;
    }

    @Override
    public ManualEntry split(Font font, int pageWidth, int pageHeight) {

        int usedHeight = 0;
        int linksUsed = 0;
        for(ManualReloadListener.ManualChapterLink link : this.chapterLinks){
            if(usedHeight + getTotalHeightFromLink(link, font) > pageHeight){
                return new FixedTOCPage(this.chapterLinks.subList(linksUsed, chapterLinks.size()));
            }
            usedHeight += getTotalHeightFromLink(link, font);
            ++linksUsed;
        }

        return null;
    }

    public @Nullable ManualChapter onClickedIn(Screen screen, int mouseX, int mouseY){
        for(ChapterThing thing : this.hitBoxed){
            if(mouseX > thing.x() && mouseX < thing.x() + thing.width()){
                if(mouseY > thing.y() && mouseY < thing.y() + thing.height()){
                    return thing.chapter();
                }
            }
        }
        return null;
    }

    @Override
    public int getHeightUsed(Font font, int width) {
        return 0;
    }

    public record ChapterThing(ManualChapter chapter, int x, int y, int width, int height){

        public static ChapterThing create(ManualChapter chapter, Font font, int x, int y){
            return new ChapterThing(chapter, x, y, font.width(chapter.makeTranslation()), font.lineHeight);
        }

    }
}
