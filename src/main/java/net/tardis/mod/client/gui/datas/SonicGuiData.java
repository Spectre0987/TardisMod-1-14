package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SonicGuiData extends GuiData{

    public InteractionHand hand;
    public final List<ItemFunctionType<ISonicCapability>> types = new ArrayList<>();

    public SonicGuiData(int id) {
        super(id);
    }

    public SonicGuiData withHand(InteractionHand hand, Collection<? extends ItemFunctionType<ISonicCapability>> activeTypes){
        this.hand = hand;
        types.addAll(activeTypes);
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeEnum(this.hand);
        buf.writeInt(this.types.size());
        for(ItemFunctionType<ISonicCapability> type : this.types){
            buf.writeRegistryId(ItemFunctionRegistry.REGISTRY.get(), type);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.hand = buf.readEnum(InteractionHand.class);
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            final ItemFunctionType<?> func = buf.readRegistryId();
            if(func instanceof SonicFunctionType st)
                this.types.add(st);
        }
    }
}
