package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.network.chat.Component;

import java.util.ArrayList;
import java.util.List;

public class TextGroup implements Renderable {

    private final int x, y, height, color;
    private final Font font;
    private boolean isCentered = false;

    public List<Component> text = new ArrayList<>();

    public TextGroup(int x, int y, int height, int color, Font font){
        this.x = x;
        this.y = y;
        this.height = height;
        this.font = font;
        this.color = color;
    }

    public TextGroup addText(Component comp){
        this.text.add(comp);
        return this;
    }

    public TextGroup centered(){
        this.isCentered = true;
        return this;
    }

    public int getWidth(){
        int width = 0;

        for(Component comp : this.text){
            if(this.font.width(comp) > width){
                width = font.width(comp);
            }
        }

        return width;
    }

    @Override
    public void render(PoseStack pose, int mouseX, int mouseY, float partialTick) {

        int xOffset = this.isCentered ? -this.getWidth() / 2 : 0;
        int curY = this.y;
        for(Component comp : this.text){
            this.font.draw(pose, comp, this.x + xOffset, curY, this.color);
            curY += this.font.lineHeight;
        }
    }
}
