package net.tardis.mod.client.gui.diagnostic_tool;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

public class DiagnosticToolBaseScreen extends Screen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/diagnostic.png");
    public static final int U = 255, V = 182;

    public DiagnosticToolBaseScreen() {
        super(Component.translatable("gui.diagnostic_tool.title"));
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderBackground(matrixStack);
        RenderSystem.setShaderTexture(0, TEXTURE);

        blit(matrixStack, this.width / 2 - U / 2, this.height / 2 - V / 2, 0, 0, U, V);

        for(Renderable child : this.renderables){
            child.render(matrixStack, mouseX, mouseY, partialTicks);
        }

        this.renderAxilliaryData(matrixStack, mouseX, mouseY, partialTicks);
    }

    public void renderAxilliaryData(PoseStack pose, int mouseX, int mouseY, float partialTicks){}
}
