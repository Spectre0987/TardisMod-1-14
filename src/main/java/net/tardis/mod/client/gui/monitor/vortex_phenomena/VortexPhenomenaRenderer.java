package net.tardis.mod.client.gui.monitor.vortex_phenomena;

import com.mojang.blaze3d.vertex.PoseStack;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;

public interface VortexPhenomenaRenderer {

    boolean isValid(VortexPhenomenaType<?> type);

    /**
     *
     * @param stack
     * @param x
     * @param y
     * @param radius - Currently unused
     */
    void render(PoseStack stack, VortexPhenomenaType<?> type, int x, int y, int radius);

    void setupRenderer(PoseStack stack);
    void endRenderer(PoseStack stack);

}
