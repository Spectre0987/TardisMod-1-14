package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class ItemButton extends Button {

    public final ItemStack display;

    public ItemButton(int pX, int pY, ItemStack display, OnPress pOnPress) {
        super(pX, pY, 18, 18, Component.empty(), pOnPress, but -> Component.empty());
        this.display = display;
    }

    @Override
    public void renderWidget(PoseStack pose, int p_275505_, int p_275674_, float p_275696_) {
        Minecraft.getInstance().getItemRenderer()
                .renderGuiItem(pose, this.display, this.getX(), this.getY());
    }
}
