package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.client.gui.manual.ManualChapter;

public class ItemEntry extends TextEntry{

    public static final Codec<ItemEntry> CODEC = RecordCodecBuilder.create(instance ->
        instance.group(
                EntryType.CODEC.fieldOf("type").forGetter(e -> EntryType.ITEM),
                ManualChapter.CODEC.fieldOf("chapter").forGetter(ItemEntry::getChapter),
                ItemStack.CODEC.fieldOf("item").forGetter(e -> e.stack),
                Codec.STRING.fieldOf("description").forGetter(e -> e.text)
        ).apply(instance, ItemEntry::new)
    );
    public final ItemStack stack;

    public ItemEntry(EntryType<?> type, ManualChapter chapter, ItemStack stack, String desc) {
        super(type, chapter, desc);
        this.stack = stack;
    }

    @Override
    public boolean isFullPage() {
        return true;
    }

    @Override
    public void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY) {
        Minecraft.getInstance().getItemRenderer().renderGuiItem(pose, this.stack, x + pageWidth / 2 - 18, y + 4);
        super.render(pose, font, x, y, pageWidth, pageHeight, mouseX, mouseY);
    }

    @Override
    public int nonTextSpace() {
        return 25;
    }

    @Override
    public int getHeightUsed(Font font, int width) {
        return super.getHeightUsed(font, width);
    }
}
