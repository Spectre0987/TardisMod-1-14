package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.network.chat.Component;

import java.util.concurrent.Callable;

public class ToggleButton extends AbstractButton {

    public static final int WIDTH = 18, HEIGHT = 7;
    public static int U = 0,
            V = 0;
    private boolean isOn = false;
    private IButtonAction action;
    private int vOffset;

    public ToggleButton(int pX, int pY, int u, int v, int vOffset, IButtonAction run) {
        super(pX, pY, WIDTH, HEIGHT, Component.empty());
        this.action = run;
        this.U = u;
        this.V = v;
        this.visible = true;
        this.vOffset = vOffset;
    }

    public ToggleButton(int x, int y, int u, int v, IButtonAction run){
        this(x, y, u, v, HEIGHT, run);
    }

    @Override
    public void onPress() {
        this.action.onPress(this);
    }

    @Override
    protected void updateWidgetNarration(NarrationElementOutput p_259858_) {

    }

    @Override
    public void renderWidget(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        //super.renderButton(pPoseStack, pMouseX, pMouseY, pPartialTick);
        this.blit(pPoseStack, this.getX(), this.getY(), U, V + (this.getState() ? vOffset : 0), WIDTH, HEIGHT * (this.getState() ? 2 : 1));
    }

    public ToggleButton setState(boolean state){
        this.isOn = state;
        return this;
    }

    public boolean getState(){
        return this.isOn;
    }

    public static interface IButtonAction{
        void onPress(ToggleButton button);
    }

}
