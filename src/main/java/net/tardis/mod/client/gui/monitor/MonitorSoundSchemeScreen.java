package net.tardis.mod.client.gui.monitor;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SoundSchemeMessage;
import net.tardis.mod.registry.JsonRegistries;

public class MonitorSoundSchemeScreen extends MonitorScreen{


    public MonitorSoundSchemeScreen(MonitorData data) {
        super(data);
    }

    @Override
    public void setup() {

        final int x = (this.width / 2 - this.data.width() / 2) + data.padLeft();

        for(ResourceLocation loc : Minecraft.getInstance().level.registryAccess().registryOrThrow(JsonRegistries.SOUND_SCHEMES_REGISTRY).keySet()){
            this.addTextOption(x, Component.translatable(Constants.Translation.makeGenericTranslation(JsonRegistries.SOUND_SCHEMES_REGISTRY, loc)), b -> {
                Network.sendToServer(new SoundSchemeMessage(loc));
                Minecraft.getInstance().setScreen(null);
            });
        }

    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {
        super.renderExtra(pose, mouseX, mouseY, partial);

    }
}
