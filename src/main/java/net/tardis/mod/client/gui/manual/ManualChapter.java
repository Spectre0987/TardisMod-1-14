package net.tardis.mod.client.gui.manual;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public record ManualChapter(String name, int weight, Optional<String> parentChapter) {


    public static final Codec<ManualChapter> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    Codec.STRING.fieldOf("name").forGetter(ManualChapter::name),
                    Codec.INT.fieldOf("weight").forGetter(ManualChapter::weight),
                    Codec.STRING.optionalFieldOf("parent_chapter").forGetter(ManualChapter::parentChapter)
            ).apply(instance, ManualChapter::new)
    );

    public Component makeTranslation(){
        return makeTranslation(this.name());
    }

    public static Component makeTranslation(String chapterName){
        return Component.translatable(Tardis.MODID + ".manual_chapter." + chapterName);
    }

    public static ManualChapter create(String title, int weight, @Nullable String parent){
        return new ManualChapter(title, weight, Helper.nullableToOptional(parent));
    }

    public static ManualChapter create(String title, int weight, ManualChapter parent){
        return new ManualChapter(title, weight, Optional.of(parent.name()));
    }

    public static ManualChapter create(String title, int weight){
        return create(title, weight, (String)null);
    }

}
