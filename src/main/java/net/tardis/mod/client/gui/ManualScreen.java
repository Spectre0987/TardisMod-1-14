package net.tardis.mod.client.gui;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.font.FontManager;
import net.minecraft.client.gui.font.FontSet;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraftforge.client.ClientForgeMod;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.gui.datas.ManualGuiData;
import net.tardis.mod.client.gui.manual.IManualScreen;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.client.gui.manual.Page;
import net.tardis.mod.client.gui.manual.entries.FixedTOCPage;
import net.tardis.mod.client.gui.manual.entries.ItemEntry;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;
import net.tardis.mod.client.renderers.tiles.MonitorRenderer;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.resource_listener.client.ManualReloadListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class ManualScreen extends Screen implements IManualScreen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/manual.png");
    public static final int WIDTH = 254, HEIGHT = 163;

    public final List<ManualEntry> entries = new ArrayList<>();

    public Page first, second;

    public int index = 0;
    final Optional<Item> selectedItem;

    public ManualScreen(ManualGuiData data) {
        this(data.item);
    }

    public ManualScreen(){
        this(Optional.empty());
    }

    public ManualScreen(Optional<Item> item){
        super(Component.empty());
        this.selectedItem = item;
    }

    @Override
    protected void init() {
        super.init();
        this.first = new Page(this.font, (width - WIDTH) / 2 + 18, (height - HEIGHT) / 2 + 15, 110, 120);
        this.second = new Page(this.font, (width - WIDTH) / 2 + 134, (height - HEIGHT) / 2 + 19, 110, 120);

        this.addRenderableWidget(new ImageButton(width / 2 + 96, (height - HEIGHT) / 2 + HEIGHT + 4, 18, 10, 5, 200, TEXTURE, (but) -> {
            this.index += 2;
        }));
        this.addRenderableWidget(new ImageButton(width / 2 - 115, (height - HEIGHT) / 2 + HEIGHT + 4, 18, 10, 28, 200, TEXTURE, (but) -> {
            this.index -= 2;
        }));
        loadEntires();
    }

    public void loadEntires(){
        this.entries.clear();
        this.addTOC();
        for(ManualReloadListener.ManualChapterLink link : ManualReloadListener.entries){
            addManualChapter(link);
        }
        selectedItem.ifPresent(item -> {
            moveToManualPage(e -> e instanceof ItemEntry i && i.stack.getItem() == item);
        });
    }

    public void moveToManualPage(Predicate<ManualEntry> entry){
        for(int i = 0; i < this.entries.size(); ++i){
            if(entry.test(entries.get(i))){
                this.index = i % this.getNumberOfPages() != 0 ? Math.max(i - 1, 0) : i;
            }
        }
    }

    public void addManualChapter(ManualReloadListener.ManualChapterLink link){
        for(ManualEntry entry : link.entries()){
            this.addEntry(entry);
        }
        for(ManualReloadListener.ManualChapterLink child : link.child()){
            addManualChapter(child);
        }
    }

    public void addTOC(){
        ManualEntry entry = new FixedTOCPage(ManualReloadListener.entries);
        this.entries.add(entry);
        ManualEntry leftOver = entry.split(this.font, 110, 130);
        while(leftOver != null){
            this.entries.add(leftOver);
            leftOver = leftOver.split(this.font, 110, 130);
        }
    }

    public void addEntry(final ManualEntry entry){

        int width = Integer.MAX_VALUE,
                height = Integer.MAX_VALUE;
        for(Page page : getPages()){
            if(page.width() < width){
                width = page.width();
            }
            if(page.height() < height){
                height = page.height();
            }
        }

        this.entries.add(entry);
        Tardis.LOGGER.debug("Adding manual entry to gui {} : {}", entry.getChapter(), entry.getEntryType());
        ManualEntry leftOver = entry.split(this.font, width, height);
        if(leftOver != null)
            this.addEntry(leftOver);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pPoseStack, width / 2 - WIDTH / 2, this.height / 2 - HEIGHT / 2, 0, 0, WIDTH, HEIGHT);
        for(Renderable render : this.renderables){
            render.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        }

        int leftOver = first.renderEntries(pPoseStack, this.index, this.entries);
        second.renderEntries(pPoseStack, leftOver, this.entries);

    }

    @Override
    public boolean mouseClicked(double pMouseX, double pMouseY, int pButton) {
        for(int i = 0; i < getPages().size(); ++i){
            final Page page = getPages().get(i);
            if(page.hasClickedOn((int)pMouseX, (int)pMouseY)){
                int entryIndex = this.index + i;
                if(entryIndex < this.entries.size()){
                    if(this.entries.get(entryIndex) instanceof FixedTOCPage toc){
                        ManualChapter selected_chapter = toc.onClickedIn(this, (int) pMouseX, (int)pMouseY);
                        if(selected_chapter != null){
                            this.moveToManualPage(e -> e.getChapter() != null && e.getChapter().name().equals(selected_chapter.name()));
                        }
                    }
                }
                return true;
            }
        }
        return super.mouseClicked(pMouseX, pMouseY, pButton);
    }

    @Override
    public int getNumberOfPages() {
        return 2;
    }

    @Override
    public List<Page> getPages() {
        return Lists.newArrayList(this.first, this.second);
    }
}
