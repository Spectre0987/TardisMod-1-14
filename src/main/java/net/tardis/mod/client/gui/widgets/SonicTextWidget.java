package net.tardis.mod.client.gui.widgets;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;

public class SonicTextWidget extends EditBox {

    public int u = 164, v = 3;
    public final ResourceLocation texture;

    public SonicTextWidget(Font pFont, ResourceLocation texture, int pX, int pY, Component pMessage) {
        super(pFont, pX, pY, 91, 26, pMessage);
        this.setBordered(false);
        this.texture = texture;
    }

    @Override
    public void renderWidget(PoseStack pose, int p_94161_, int p_94162_, float p_94163_) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, this.texture);
        int middle = (this.height - 8) / 2;
        blit(pose, this.getX(), this.getY(), this.u, this.v, this.getWidth(), this.getHeight());
        final int oldY = this.getY();
        this.setY(oldY + middle);
        this.setX(this.getX() + 10);
        super.renderWidget(pose, p_94161_, p_94162_, p_94163_);
        this.setY(oldY);
        this.setX(this.getX() - 10);

    }


}
