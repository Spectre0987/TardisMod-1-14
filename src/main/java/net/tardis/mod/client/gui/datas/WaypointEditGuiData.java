package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;

public class WaypointEditGuiData extends GuiData{

    public MonitorWaypointData.WaypointDTO dto;

    public WaypointEditGuiData(int id) {
        super(id);
    }

    public WaypointEditGuiData fromWaypoint(MonitorWaypointData.WaypointDTO dto){
        this.dto = dto;
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        this.dto.encode(buf);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.dto = MonitorWaypointData.WaypointDTO.decode(buf);
    }
}
