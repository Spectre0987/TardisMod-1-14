package net.tardis.mod.client.gui.containers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.tardis.mod.client.elements.ToolTipArea;
import net.tardis.mod.client.renderers.FluidRenderer;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.AlembicMenu;

public class AlembicMenuScreen extends AbstractContainerScreen<AlembicMenu> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/containers/alembic.png");

    public AlembicMenuScreen(AlembicMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
        this.imageWidth = 176;
        this.imageHeight = 166;
        this.titleLabelY += 30;
    }

    @Override
    protected void init() {
        super.init();

        this.addRenderableOnly(new ToolTipArea(this.leftPos + 46, this.topPos + 10, 10, 53, () -> {
            Component craftingTankTooltip = this.menu.getAlembic().getCraftingTank().getFluid().getDisplayName();
            craftingTankTooltip.getSiblings().add(Component.literal("\n%dmb".formatted(this.menu.getAlembic().getCraftingTank().getFluidAmount())).withStyle(ChatFormatting.GRAY));
            return Tooltip.create(craftingTankTooltip);
        }));
        this.addRenderableOnly(new ToolTipArea(this.leftPos + 118, this.topPos + 9, 12, 55, () -> {
            Component resultTankTooltip = this.menu.getAlembic().getResultTank().getFluid().getDisplayName();
            resultTankTooltip.getSiblings().add(Component.literal("\n%dmb".formatted(this.menu.getAlembic().getResultTank().getFluidAmount())).withStyle(ChatFormatting.GRAY));
            return Tooltip.create(resultTankTooltip);
        }));

    }

    @Override
    protected void renderBg(PoseStack poseStack, float partialTicks, int mouseX, int mouseY) {
        this.renderBackground(poseStack);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(poseStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);


        //Render Fluid Tanks
        FluidTank craftingTank = this.menu.getAlembic().getCraftingTank();
        FluidTank resultTank = this.menu.getAlembic().getResultTank();
        FluidRenderer.renderIntoGui(craftingTank.getFluid(), this.leftPos + 46, this.topPos + 10, 10, 53, craftingTank.getFluidAmount() / (float)craftingTank.getCapacity());
        FluidRenderer.renderIntoGui(resultTank.getFluid(), leftPos + 119, topPos + 10, 10, 53, resultTank.getFluidAmount() / (float)resultTank.getCapacity());

        //Render Tank overlays
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        //Draw edges of the gui tank texture
        blit(poseStack, leftPos + 45, topPos + 9, 193, 18, 11, 54);
        blit(poseStack, leftPos + 118, topPos + 9, 180, 18, 12, 55);

        this.renderFuelPercent(poseStack, this.menu.getAlembic().getFuelPercentage());
        this.renderProgressPercent(poseStack, this.menu.getAlembic().getProgress());

    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        renderTooltip(pPoseStack, pMouseX, pMouseY);
    }

    public void renderFuelPercent(PoseStack pose, float percent){
        int pixels = (int)Math.floor(percent * 13);
        blit(pose, this.leftPos + 63, this.topPos + 30 + 13 - pixels, 203, 1 + 13 - pixels, 13, pixels);
    }

    public void renderProgressPercent(PoseStack pose, float percent){
        blit(pose, this.leftPos + 90, this.topPos + 31, 178, 1, (int)Math.floor(21 * percent), 14);
    }
}
