package net.tardis.mod.client.gui.monitor;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import com.mojang.datafixers.util.Pair;
import net.minecraft.client.Minecraft;
import net.minecraft.client.Options;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.tardis.mod.Constants;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.VortexPheomenaClientHander;
import net.tardis.mod.client.gui.IScreenKeyInput;
import net.tardis.mod.client.gui.datas.tardis.TardisFlightCourseData;
import net.tardis.mod.client.gui.monitor.vortex_phenomena.VortexPhenomenaRenderer;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.FlightCourse;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.registry.VortexPhenomenaRegistry;
import net.tardis.mod.world.data.TardisLevelData;
import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFW;

import java.util.*;

public class MonitorFlightCourseScreen extends MonitorScreen implements IScreenKeyInput {

    public static final int COLOR = 0xFF8e00aa;
    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/flight_course.png");
    public static final ResourceLocation CLOSE_STAR_TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/flight_course_close_star.png");
    public static final ResourceLocation DIST_STAR_TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/flight_course_dist_star.png");
    public static final ResourceLocation ICONS_TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/vortex_phenomena_icon_sheet.png");
    public static final List<VortexPhenomenaRenderer> RENDERERS = new ArrayList<>();

    public static final int LINE_WIDTH = 3;
    final Optional<FlightCourse> course;
    float distanceTardisTraveled = 0;
    public HashMap<ChunkPos, TardisLevelData.VortexPhenomenaDTO> phenomena = new HashMap<>();
    final Map<VortexPhenomenaRenderer, List<ChunkPos>> vpPerRenderer = new HashMap<>();
    final SpaceTimeCoord currentLocation;

    int offsetX, offsetY;
    float scale = 1.0F;

    public MonitorFlightCourseScreen(MonitorData data, TardisFlightCourseData guiData) {
        super(data);
        this.course = guiData.course;
        this.distanceTardisTraveled = guiData.distTardisTraveled;
        VortexPheomenaClientHander.vortexPMap.putAll(guiData.vortexPhenomenaNear);
        this.phenomena.putAll(VortexPheomenaClientHander.vortexPMap);
        this.currentLocation = guiData.currentLocation;
    }

    @Override
    public void setup() {
        this.setupVortexPhenomenaRenderers();

        final BlockPos startPos = currentLocation.getPos();
        this.offsetX = startPos.getX();
        this.offsetY = startPos.getZ();
    }

    @Override
    public void renderExtra(PoseStack pose, int mouseX, int mouseY, float partial) {

        enableScissor((this.width - this.data.width()) / 2, (this.height - this.data.height()) / 2,
                (this.width - this.data.width()) / 2 + this.data.width(), this.bottom
                );
        pose.pushPose();
        renderFullScreen(pose, TEXTURE);
        //renderFullScreen(pose, DIST_STAR_TEXTURE);

        this.course.ifPresent(course -> {

            BufferBuilder bufferUpload = Tesselator.getInstance().getBuilder();
            RenderSystem.enableBlend();

            RenderSystem.setShader(GameRenderer::getPositionColorShader);


            bufferUpload.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);

            for(FlightCourse.FlightLeg leg : course.getLegs()){
                renderLeg(leg, pose, bufferUpload);
            }

            BufferUploader.drawWithShader(bufferUpload.end());

            RenderSystem.disableBlend();

            RenderSystem.setShader(GameRenderer::getPositionTexShader);


        });

        //Render TARDIS Icon
        Pair<Integer, Integer> tardisIconCoords = worldToScreenCoord(this.currentLocation.getPos());
        renderTardisIcon(pose, tardisIconCoords.getFirst(), tardisIconCoords.getSecond());


        renderFullScreen(pose, DIST_STAR_TEXTURE);

        //Render Vortex Phenomena
        for(Map.Entry<VortexPhenomenaRenderer, List<ChunkPos>> entry : vpPerRenderer.entrySet()){
            pose.pushPose();
            entry.getKey().setupRenderer(pose);

            for(ChunkPos pos : entry.getValue()){
                VortexPhenomenaType<?> type = this.phenomena.get(pos).type();
                Pair<Integer, Integer> coord = worldToScreenCoord(pos.getMiddleBlockPosition(0));
                entry.getKey().render(pose, type, coord.getFirst(), coord.getSecond(), (int)(this.phenomena.get(pos).radius() * scale));
            }

            entry.getKey().endRenderer(pose);
            pose.popPose();
        }

        //Tooltips

        this.course.ifPresent(c -> {
            for(FlightCourse.FlightLeg leg : c.getLegs()){
                if(isMouseOver(leg.end.getPos(), 5, mouseX, mouseY)){
                    renderTooltip(pose, Component.literal("End Pos %d, %d".formatted(leg.end.getPos().getX(), leg.end.getPos().getZ())), mouseX, mouseY);
                }
            }
        });
        for(ChunkPos pos : this.phenomena.keySet()){
            final BlockPos bp = pos.getMiddleBlockPosition(0);
            if(isMouseOver(bp, 5, mouseX, mouseY)){

                final TardisLevelData.VortexPhenomenaDTO vp = this.phenomena.get(pos);

                List<Component> toolTips = new ArrayList<>();
                toolTips.add(Component.translatable(Constants.Translation.makeGenericTranslation(VortexPhenomenaRegistry.REGISTRY.get(), vp.type())));
                toolTips.add(Component.literal("%d, %d".formatted(bp.getX(), bp.getZ())));
                toolTips.addAll(vp.extraText());
                renderTooltip(pose, toolTips, Optional.empty(), mouseX, mouseY);
            }
        }
        pose.popPose();
        disableScissor();
    }

    public void setupVortexPhenomenaRenderers(){

        this.vpPerRenderer.clear();

        for(VortexPhenomenaRenderer renderer : RENDERERS){
            for(Map.Entry<ChunkPos, TardisLevelData.VortexPhenomenaDTO> entry : this.phenomena.entrySet()){
                if(renderer.isValid(entry.getValue().type())){
                    vpPerRenderer.computeIfAbsent(renderer, r -> Lists.newArrayList())
                            .add(entry.getKey());
                }
            }
        }
    }

    public boolean isMouseOver(BlockPos pos, int radius, int mouseX, int mouseY){
        final Pair<Integer, Integer> screenPos = worldToScreenCoord(pos);

        if(mouseX > screenPos.getFirst() - radius && mouseX < screenPos.getFirst() + radius){
            if(mouseY > screenPos.getSecond() - radius && mouseY < screenPos.getSecond() + radius){
                return true;
            }
        }
        return false;

    }

    public Pair<Integer, Integer> worldToScreenCoord(BlockPos pos){
        return new Pair<>(
                width / 2 + (int)((pos.getX() - offsetX) * scale),
                height / 2 + (int)((pos.getZ() - offsetY) * scale)
        );
    }

    public void renderFullScreen(PoseStack pose, ResourceLocation texture){
        float aspectRatio = this.data.height() / (float)data.width();
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, texture);
        blit(pose, this.left - this.data.padLeft(), this.top - this.data.padTop(),
                this.data.width(), this.data.height(),
                0,0,
                512, Mth.floor(512 * aspectRatio),
                256, 256);
    }

    public void renderTardisIcon(PoseStack pose, int x, int y){
        itemRenderer.renderGuiItem(pose, new ItemStack(BlockRegistry.STEAM_BROKEN_EXTERIOR.get()), x - 8, y - 8);
    }

    public boolean isOnScreen(int x, int y){
        return (x > this.left && x < this.right) && (y > this.top && y < this.bottom);
    }

    public boolean isOnScreen(BlockPos pos){
        final Pair<Integer, Integer> screenPos = worldToScreenCoord(pos);
        return isOnScreen(screenPos.getFirst(), screenPos.getSecond());
    }

    public void renderLeg(FlightCourse.FlightLeg leg, PoseStack pose, BufferBuilder vertex){

        BlockPos start = leg.start.getPos();//Helper.min(leg.start.getPos(), leg.end.getPos());
        BlockPos end = leg.end.getPos();//Helper.max(leg.start.getPos(), leg.end.getPos());

        Pair<Integer, Integer> startPos = worldToScreenCoord(start);
        Pair<Integer, Integer> endPos = worldToScreenCoord(end);


        //Render Leg

        pose.pushPose();
        final Matrix4f poseMatrix = pose.last().pose();
        vertex.vertex(poseMatrix, startPos.getFirst(), startPos.getSecond(), 0).color(COLOR).endVertex();
        vertex.vertex(poseMatrix, startPos.getFirst() - LINE_WIDTH, startPos.getSecond() + LINE_WIDTH, 0).color(COLOR).endVertex();
        vertex.vertex(poseMatrix, endPos.getFirst() - LINE_WIDTH, endPos.getSecond() + LINE_WIDTH, 0).color(COLOR).endVertex();
        vertex.vertex(poseMatrix, endPos.getFirst(), endPos.getSecond(), 0).color(COLOR).endVertex();

        pose.popPose();

    }

    @Override
    public boolean onKeyPress(int key, int action) {

        final Options options = Minecraft.getInstance().options;

        final int upCode = options.keyUp.getKey().getValue();
        final int downCode = options.keyDown.getKey().getValue();
        final int leftCode = options.keyLeft.getKey().getValue();
        final int rightCode = options.keyRight.getKey().getValue();
        final int zoomIn = GLFW.GLFW_KEY_E;
        final int zoonOut = GLFW.GLFW_KEY_F;

        int amountToMove = (int)((1.0 - scale) * 10);
        if(amountToMove <= 0){
            amountToMove = 10;
        }

        if(key == upCode){
            this.offsetY -= amountToMove;
        }
        else if(key == downCode){
            this.offsetY += amountToMove;
        }
        if(key == leftCode){
            this.offsetX -= amountToMove;
        }
        else if(key == rightCode){
            this.offsetX += amountToMove;
        }
        if(key == zoomIn){
            this.scale = Mth.clamp(scale + 0.0025F, 1.0F, 100.0F);
        }
        else if(key == zoonOut){
            this.scale = Mth.clamp(scale - 0.0025F, 0.1F, 1000.0F);
        }

        return false;
    }

    public static void registerVortexPhenomenaRenderer(VortexPhenomenaRenderer renderer){
        RENDERERS.add(renderer);
    }
}
