package net.tardis.mod.client.gui.datas;

import net.tardis.mod.misc.IEncodeable;

import java.util.Optional;

public abstract class GuiData implements IEncodeable {

    public final int id;

    public GuiData(int id){
        this.id = id;
    }

    public <T extends GuiData>Optional<T> cast(Class<T> clazz){
        if(clazz.isInstance(this)){
            return Optional.of((T)this);
        }
        return Optional.empty();
    }

    public <T extends GuiData> Optional<T> cast(GuiDatas.GuiDataRegistryObject<T> data){
        return Optional.of((T)this);
    }

}
