package net.tardis.mod.client.gui.datas.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.misc.tardis.world_structures.AtriumWorldStructure;

import java.util.HashMap;
import java.util.Map;

public class MonitorAtriumGuiData extends GuiData {

    public final HashMap<String, BlockPos> atriums = new HashMap<>();

    public MonitorAtriumGuiData(int id) {
        super(id);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.atriums.size());
        for(Map.Entry<String, BlockPos> entry : this.atriums.entrySet()){
            buf.writeUtf(entry.getKey());
            buf.writeBlockPos(entry.getValue());
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.atriums.put(buf.readUtf(), buf.readBlockPos());
        }
    }

    public MonitorAtriumGuiData withTardis(ITardisLevel tardis) {
        tardis.getInteriorManager().getAllWorldStructures().stream()
                .filter(s -> s instanceof AtriumWorldStructure at && at.getName().isPresent())
                .map(s -> (AtriumWorldStructure)s)
                .forEach(s -> {
                    this.atriums.put(s.getName().get(), s.getPosition());
                });
        return this;
    }
}
