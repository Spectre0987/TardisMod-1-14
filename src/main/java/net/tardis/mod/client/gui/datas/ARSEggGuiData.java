package net.tardis.mod.client.gui.datas;

import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.tags.ItemTags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ARSEggGuiData extends GuiData{

    public int matterBuffer = 0;
    public final List<Item> items = new ArrayList<>();

    public ARSEggGuiData(int id) {
        super(id);
    }

    public ARSEggGuiData with(ITardisLevel level){
        this.matterBuffer = level.getInteriorManager().getMatterBuffer();
        return this;
    }

    public ARSEggGuiData withItems(List<Item> items){
        this.items.addAll(items);
        return this;
    }

    public ARSEggGuiData addAllItems(ServerLevel tardis){
        ForgeRegistries.ITEMS.tags().getTag(ItemTags.ARS_EGG_ITEMS).stream().filter(item -> showItem(tardis, item)).forEach(items::add);
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.matterBuffer);
        buf.writeInt(this.items.size());
        for(Item item : this.items){
            buf.writeRegistryId(ForgeRegistries.ITEMS, item);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.matterBuffer = buf.readInt();
        final int size = buf.readInt();
        for(int i = 0; i < size; ++i){
            this.items.add(buf.readRegistryIdSafe(Item.class));
        }
    }

    public static boolean showItem(Level tardis, Item item){
        if(ForgeRegistries.ITEMS.tags().getTag(ItemTags.ARS_LOCKED).contains(item)){
            LazyOptional<ITardisLevel> currentTardis = Capabilities.getCap(Capabilities.TARDIS, tardis);
            final ResourceKey<Item> id = ForgeRegistries.ITEMS.getResourceKey(item).get();
            if(currentTardis.isPresent() && currentTardis.orElseThrow(NullPointerException::new).getUnlockHandler().isUnlocked(id)){
                return true;
            }
            return false;
        }
        return true;
    }
}
