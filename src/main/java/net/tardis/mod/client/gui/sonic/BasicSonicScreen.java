package net.tardis.mod.client.gui.sonic;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

public abstract class BasicSonicScreen extends Screen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/sonic/generic_sonic_gui.png");

    int guiLeft;
    int guiTop;
    boolean renderInnerSquare = true;
    boolean renderInnerGrid = true;

    public BasicSonicScreen(Component pTitle) {
        super(pTitle);
    }


    @Override
    protected void init() {
        super.init();
        this.guiLeft = (width - 152) / 2;
        this.guiTop = (height - 151) / 2;

    }

    @Override
    public void renderBackground(PoseStack pose) {
        super.renderBackground(pose);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pose, this.guiLeft, this.guiTop, 5, 3, 152, 151);

        if(this.renderInnerSquare)
            blit(pose, this.guiLeft + 31, this.guiTop + 23, 161, 73, 89, 105);

        if(renderInnerGrid)
            blit(pose, this.guiLeft + 30, this.guiTop + 30, 36, 158, 93, 94);
    }

    @Override
    public void render(PoseStack pose, int pMouseX, int pMouseY, float pPartialTick) {
        this.renderBackground(pose);
        super.render(pose, pMouseX, pMouseY, pPartialTick);
        this.renderExtra(pose, pMouseX, pMouseY, pPartialTick);
    }


    public void addNextPrev(Button.OnPress prev, Button.OnPress next){
        this.addRenderableWidget(new ImageButton(
                this.guiLeft + 145, this.guiTop + 90,
                11, 22,
                245, 0,
                TEXTURE,
                next
        ));
        //Prev
        this.addRenderableWidget(new ImageButton(
                this.guiLeft - 5, this.guiTop + 90,
                11, 22,
                230, 0,
                TEXTURE,
                prev
        ));
    }


    public abstract void renderExtra(PoseStack pose, int mouseX, int mouseY, float partialTick);

}
