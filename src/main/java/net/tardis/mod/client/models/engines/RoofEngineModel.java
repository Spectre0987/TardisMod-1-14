package net.tardis.mod.client.models.engines;// Made with Blockbench 4.10.3
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.world.entity.Entity;
import net.tardis.mod.helpers.Helper;

public class RoofEngineModel extends EntityModel<Entity> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("engine/roof"), "main");
	private final ModelPart glow;
	private final ModelPart roof;
	private final ModelPart basiccubes;
	private final ModelPart compartmentoutline;
	private final ModelPart rotorstand;
	private final ModelPart door1_rotate_y;
	private final ModelPart door2_rotate_y;
	private final ModelPart door3_rotate_y;
	private final ModelPart door4_rotate_y;

	public RoofEngineModel(ModelPart root) {
		this.glow = root.getChild("glow");
		this.roof = root.getChild("roof");
		this.basiccubes = root.getChild("basiccubes");
		this.compartmentoutline = root.getChild("compartmentoutline");
		this.rotorstand = root.getChild("rotorstand");
		this.door1_rotate_y = root.getChild("door1_rotate_y");
		this.door2_rotate_y = root.getChild("door2_rotate_y");
		this.door3_rotate_y = root.getChild("door3_rotate_y");
		this.door4_rotate_y = root.getChild("door4_rotate_y");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow = partdefinition.addOrReplaceChild("glow", CubeListBuilder.create().texOffs(0, 62).addBox(-6.0F, -32.0F, -6.0F, 12.0F, 7.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 56.0F, 0.0F));

		PartDefinition rotorglow1 = glow.addOrReplaceChild("rotorglow1", CubeListBuilder.create().texOffs(12, 22).addBox(-0.5F, -46.5F, -5.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(48, 22).addBox(-0.5F, -46.5F, 4.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 26.0F, 0.0F));

		PartDefinition rotorglow2 = glow.addOrReplaceChild("rotorglow2", CubeListBuilder.create().texOffs(12, 22).addBox(-0.5F, -46.5F, -5.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(48, 22).addBox(-0.5F, -46.5F, 4.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 26.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition rotorglow3 = glow.addOrReplaceChild("rotorglow3", CubeListBuilder.create().texOffs(12, 22).addBox(-0.5F, -46.5F, -5.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(48, 22).addBox(-0.5F, -46.5F, 4.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 26.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition roof = partdefinition.addOrReplaceChild("roof", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -64.0F, -8.0F, 16.0F, 6.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 72.0F, 0.0F));

		PartDefinition basiccubes = partdefinition.addOrReplaceChild("basiccubes", CubeListBuilder.create(), PartPose.offset(0.0F, 82.0F, 0.0F));

		PartDefinition raidators = basiccubes.addOrReplaceChild("raidators", CubeListBuilder.create().texOffs(48, 0).addBox(-7.0F, -57.0F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(48, 0).addBox(-7.0F, -55.25F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(48, 0).addBox(-7.0F, -53.5F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition compartmentoutline = partdefinition.addOrReplaceChild("compartmentoutline", CubeListBuilder.create().texOffs(48, 0).addBox(-6.5F, -31.0F, -6.5F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 22).addBox(-6.5F, -31.0F, 3.5F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(42, 44).addBox(3.5F, -31.0F, -6.5F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 44).addBox(3.5F, -31.0F, 3.5F, 3.0F, 8.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(58, 37).addBox(-7.0F, -23.0F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(39, 79).addBox(-4.5F, -32.0F, -4.5F, 9.0F, 10.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(65, 65).addBox(-6.5F, -32.0F, -6.5F, 13.0F, 1.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 46.0F, 0.0F));

		PartDefinition cross = compartmentoutline.addOrReplaceChild("cross", CubeListBuilder.create().texOffs(36, 44).addBox(-0.5F, -20.8F, -10.0F, 1.0F, 10.0F, 20.0F, new CubeDeformation(0.0F))
		.texOffs(75, 79).addBox(-10.0F, -20.8F, -0.5F, 20.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -11.25F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition rotorstand = partdefinition.addOrReplaceChild("rotorstand", CubeListBuilder.create(), PartPose.offset(0.0F, 82.0F, 0.0F));

		PartDefinition rotorstand1 = rotorstand.addOrReplaceChild("rotorstand1", CubeListBuilder.create().texOffs(8, 9).addBox(-1.0F, -50.75F, -5.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 9).addBox(-1.0F, -50.75F, 3.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 11).addBox(-0.5F, -47.5F, -5.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F))
		.texOffs(9, 44).addBox(-0.5F, -47.5F, 4.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offset(0.0F, -1.0F, 0.0F));

		PartDefinition rotorstand2 = rotorstand.addOrReplaceChild("rotorstand2", CubeListBuilder.create().texOffs(8, 9).addBox(-1.0F, -50.75F, -5.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 9).addBox(-1.0F, -50.75F, 3.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 11).addBox(-0.5F, -47.5F, -5.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F))
		.texOffs(9, 44).addBox(-0.5F, -47.5F, 4.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(0.0F, -1.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition rotorstand3 = rotorstand.addOrReplaceChild("rotorstand3", CubeListBuilder.create().texOffs(8, 9).addBox(-1.0F, -50.75F, -5.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 9).addBox(-1.0F, -50.75F, 3.5F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(48, 11).addBox(-0.5F, -47.5F, -5.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F))
		.texOffs(9, 44).addBox(-0.5F, -47.5F, 4.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.25F)), PartPose.offsetAndRotation(0.0F, -1.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition door1_rotate_y = partdefinition.addOrReplaceChild("door1_rotate_y", CubeListBuilder.create().texOffs(7, 105).addBox(0.0F, -20.0F, -0.5F, 7.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.5F, 35.0F, -6.25F));

		PartDefinition door2_rotate_y = partdefinition.addOrReplaceChild("door2_rotate_y", CubeListBuilder.create().texOffs(25, 99).addBox(-0.5F, -20.0F, 0.0F, 1.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(6.25F, 35.0F, -3.5F));

		PartDefinition door3_rotate_y = partdefinition.addOrReplaceChild("door3_rotate_y", CubeListBuilder.create().texOffs(43, 105).addBox(-7.0F, -20.0F, -0.5F, 7.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(3.5F, 35.0F, 6.25F));

		PartDefinition door4_rotate_y = partdefinition.addOrReplaceChild("door4_rotate_y", CubeListBuilder.create().texOffs(61, 99).addBox(-0.5F, -20.0F, -7.0F, 1.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.25F, 35.0F, 3.5F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glow.render(poseStack, vertexConsumer, LightTexture.FULL_BRIGHT, packedOverlay, red, green, blue, alpha);
		roof.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		basiccubes.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		compartmentoutline.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rotorstand.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door1_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door2_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door3_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door4_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}