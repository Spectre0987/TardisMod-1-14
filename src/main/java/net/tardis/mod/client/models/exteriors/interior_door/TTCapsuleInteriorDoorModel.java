package net.tardis.mod.client.models.exteriors.interior_door;// Made with Blockbench 4.12.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class TTCapsuleInteriorDoorModel extends BasicTileHierarchicalModel<InteriorDoorTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/interior/tt"), "main");
	private final ModelPart tt_capsule_interior_door;
	private final ModelPart frame;
	private final ModelPart walls_angled;
	private final ModelPart door_frame_west;
	private final ModelPart door_frame_east;
	private final ModelPart walls_outer;
	private final ModelPart door_rotate_y;
	private final ModelPart bone19;
	private final ModelPart bone7;
	private final ModelPart boti;

	public TTCapsuleInteriorDoorModel(ModelPart root) {
		super(root);
		this.tt_capsule_interior_door = root.getChild("tt_capsule_interior_door");
		this.frame = this.tt_capsule_interior_door.getChild("frame");
		this.walls_angled = this.frame.getChild("walls_angled");
		this.door_frame_west = this.walls_angled.getChild("door_frame_west");
		this.door_frame_east = this.walls_angled.getChild("door_frame_east");
		this.walls_outer = this.frame.getChild("walls_outer");
		this.door_rotate_y = this.tt_capsule_interior_door.getChild("door_rotate_y");
		this.bone19 = this.door_rotate_y.getChild("bone19");
		this.bone7 = this.door_rotate_y.getChild("bone7");
		this.boti = this.tt_capsule_interior_door.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition tt_capsule_interior_door = partdefinition.addOrReplaceChild("tt_capsule_interior_door", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, -0.5F, 0.0F, 3.1416F, 0.0F));

		PartDefinition frame = tt_capsule_interior_door.addOrReplaceChild("frame", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition walls_angled = frame.addOrReplaceChild("walls_angled", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 0.0F));

		PartDefinition door_frame_west = walls_angled.addOrReplaceChild("door_frame_west", CubeListBuilder.create().texOffs(38, 10).addBox(-4.3332F, -37.815F, -13.8624F, 7.0F, 38.0F, 4.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.2994F, 0.0F, 1.7094F, 0.0F, -1.0472F, 0.0F));

		PartDefinition door_frame_east = walls_angled.addOrReplaceChild("door_frame_east", CubeListBuilder.create().texOffs(87, 7).addBox(-2.8648F, -37.815F, -14.2054F, 7.0F, 38.0F, 4.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.2994F, 0.0F, 1.7094F, 0.0F, 1.0472F, 0.0F));

		PartDefinition walls_outer = frame.addOrReplaceChild("walls_outer", CubeListBuilder.create().texOffs(44, 40).addBox(-18.75F, -1.975F, -13.925F, 28.0F, 2.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(44, 25).addBox(-18.75F, -41.975F, -13.925F, 28.0F, 2.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(66, 2).addBox(-18.75F, -39.975F, -13.925F, 4.0F, 38.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(78, 3).addBox(5.25F, -39.975F, -13.925F, 4.0F, 38.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(4.2526F, 0.06F, 5.0871F));

		PartDefinition door_rotate_y = tt_capsule_interior_door.addOrReplaceChild("door_rotate_y", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 4.0F));

		PartDefinition bone19 = door_rotate_y.addOrReplaceChild("bone19", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.2994F, -2.0F, -2.2906F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r1 = bone19.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(59, 11).addBox(-3.875F, -37.8F, -11.6184F, 4.0F, 38.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(0.4386F, -0.105F, 1.4865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone7 = door_rotate_y.addOrReplaceChild("bone7", CubeListBuilder.create().texOffs(71, 11).addBox(-4.198F, -37.915F, -9.7473F, 9.0F, 38.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.2994F, -2.0F, -2.2906F));

		PartDefinition cube_r2 = bone7.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(93, 11).addBox(-2.5F, -38.0F, -14.0F, 4.0F, 38.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.0366F, 0.095F, 3.6134F, 0.0F, -0.5236F, 0.0F));

		PartDefinition boti = tt_capsule_interior_door.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(1, 2).addBox(-15.75F, -16.0F, -5.25F, 10.0F, 14.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(1, 2).addBox(-15.75F, -26.0F, -5.25F, 10.0F, 10.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(1, 2).addBox(-5.75F, -26.0F, -5.25F, 10.0F, 10.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(1, 2).addBox(-15.75F, -40.0F, -5.25F, 10.0F, 14.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(1, 2).addBox(-5.75F, -40.0F, -5.25F, 10.0F, 14.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(1, 2).addBox(-5.75F, -16.0F, -5.25F, 10.0F, 14.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(5.0F, 0.0F, -3.25F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		tt_capsule_interior_door.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(InteriorDoorTile tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen())
			this.door_rotate_y.offsetRotation(AnimationHelper.degrees(60, AnimationHelper.Axis.Y));
	}
}