package net.tardis.mod.client.models.consoles;

import net.minecraft.client.model.geom.ModelPart;
import net.tardis.mod.control.ControlType;

import java.util.Optional;

public interface IAdditionalConsoleRenderData {

    Optional<String> getPartForControl(ControlType<?> type);

}
