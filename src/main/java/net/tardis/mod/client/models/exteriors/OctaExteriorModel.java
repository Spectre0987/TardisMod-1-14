package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class OctaExteriorModel<T extends ExteriorTile> extends BasicTileHierarchicalModel<T> implements IExteriorModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/octa"), "main");
	private final ModelPart glow_bands;
	private final ModelPart glow_lamp;
	private final ModelPart cap;
	private final ModelPart top1;
	private final ModelPart top2;
	private final ModelPart top3;
	private final ModelPart top4;
	private final ModelPart walls;
	private final ModelPart doors;
	private final ModelPart door_left;
	private final ModelPart door_right;
	private final ModelPart base;
	private final ModelPart boti;

	public OctaExteriorModel(ModelPart root) {
		super(root, RenderType::entityTranslucent);
		this.glow_bands = root.getChild("glow_bands");
		this.glow_lamp = root.getChild("glow_lamp");
		this.cap = root.getChild("cap");
		this.top1 = this.cap.getChild("top1");
		this.top2 = this.cap.getChild("top2");
		this.top3 = this.cap.getChild("top3");
		this.top4 = this.cap.getChild("top4");
		this.walls = root.getChild("walls");
		this.doors = root.getChild("doors");
		this.door_left = this.doors.getChild("door_left");
		this.door_right = this.doors.getChild("door_right");
		this.base = root.getChild("base");
		this.boti = root.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow_bands = partdefinition.addOrReplaceChild("glow_bands", CubeListBuilder.create(), PartPose.offset(-0.5F, -18.75F, -1.875F));

		PartDefinition upper = glow_bands.addOrReplaceChild("upper", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r1 = upper.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-1.5645F, 0.0F, 1.0622F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r2 = upper.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-1.9196F, 0.0F, 2.9196F, 0.0F, 1.5708F, 0.0F));

		PartDefinition cube_r3 = upper.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-0.8574F, 0.0F, 4.4841F, 0.0F, 2.3562F, 0.0F));

		PartDefinition cube_r4 = upper.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(1.0F, 0.0F, 4.8392F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r5 = upper.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(2.5645F, 0.0F, 3.777F, 0.0F, -2.3562F, 0.0F));

		PartDefinition cube_r6 = upper.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(2.9196F, 0.0F, 1.9196F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r7 = upper.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(1.8574F, 0.0F, 0.3551F, 0.0F, -0.7854F, 0.0F));

		PartDefinition lower = glow_bands.addOrReplaceChild("lower", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offset(0.0F, 4.5F, 0.0F));

		PartDefinition cube_r8 = lower.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-1.5645F, 0.0F, 1.0622F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r9 = lower.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-1.9196F, 0.0F, 2.9196F, 0.0F, 1.5708F, 0.0F));

		PartDefinition cube_r10 = lower.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(-0.8574F, 0.0F, 4.4841F, 0.0F, 2.3562F, 0.0F));

		PartDefinition cube_r11 = lower.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(1.0F, 0.0F, 4.8392F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r12 = lower.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(2.5645F, 0.0F, 3.777F, 0.0F, -2.3562F, 0.0F));

		PartDefinition cube_r13 = lower.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(2.9196F, 0.0F, 1.9196F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r14 = lower.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(94, 12).mirror().addBox(-5.0F, -3.99F, -11.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).mirror(false), PartPose.offsetAndRotation(1.8574F, 0.0F, 0.3551F, 0.0F, -0.7854F, 0.0F));

		PartDefinition glow_lamp = partdefinition.addOrReplaceChild("glow_lamp", CubeListBuilder.create().texOffs(94, 1).addBox(-1.0F, 1.5F, -5.0F, 3.0F, 7.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.5F, -29.7188F, 4.0F));

		PartDefinition cap = partdefinition.addOrReplaceChild("cap", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition top1 = cap.addOrReplaceChild("top1", CubeListBuilder.create(), PartPose.offset(0.0F, -41.75F, -0.5F));

		PartDefinition east3 = top1.addOrReplaceChild("east3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r15 = east3.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(30, 43).addBox(-6.0F, -4.0F, -14.0F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.4853F, 0.01F, 0.4853F, 0.0F, 1.5708F, 0.0F));

		PartDefinition se3 = top1.addOrReplaceChild("se3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r16 = se3.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(63, 43).addBox(-6.0F, -4.01F, -14.0F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.8284F, 0.01F, 0.3431F, 0.0F, 1.5708F, 0.0F));

		PartDefinition west3 = top1.addOrReplaceChild("west3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r17 = west3.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(30, 43).addBox(-6.3431F, -3.99F, -14.1421F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.3431F, 0.0F, 0.8284F, 0.0F, -1.5708F, 0.0F));

		PartDefinition sw3 = top1.addOrReplaceChild("sw3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r18 = sw3.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(63, 43).addBox(-6.0F, -4.01F, -14.0F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.8284F, 0.01F, 0.3431F, 0.0F, -1.5708F, 0.0F));

		PartDefinition south3 = top1.addOrReplaceChild("south3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r19 = south3.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(30, 43).addBox(-6.3431F, -3.99F, -14.1421F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.3431F, 0.0F, 0.8284F, 0.0F, 3.1416F, 0.0F));

		PartDefinition north4 = top1.addOrReplaceChild("north4", CubeListBuilder.create().texOffs(30, 43).addBox(-6.0F, -4.0F, -14.0F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition nw3 = top1.addOrReplaceChild("nw3", CubeListBuilder.create().texOffs(62, 42).addBox(-5.6569F, -4.0F, -14.1421F, 12.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition ne3 = top1.addOrReplaceChild("ne3", CubeListBuilder.create().texOffs(63, 43).addBox(-6.3431F, -4.0F, -14.1421F, 12.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition top2 = cap.addOrReplaceChild("top2", CubeListBuilder.create(), PartPose.offset(-0.5F, -43.0F, 0.25F));

		PartDefinition east4 = top2.addOrReplaceChild("east4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r20 = east4.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(1, 42).mirror().addBox(-5.3431F, -3.98F, -12.7279F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(-0.0261F, -0.02F, 0.445F, 0.0F, 1.5708F, 0.0F));

		PartDefinition se4 = top2.addOrReplaceChild("se4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r21 = se4.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(1, 42).addBox(-4.9497F, -3.97F, -13.435F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.3237F, -0.02F, 1.1246F, 0.0F, 1.5708F, 0.0F));

		PartDefinition west4 = top2.addOrReplaceChild("west4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r22 = west4.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(1, 42).mirror().addBox(-4.9497F, -4.0F, -13.435F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.01F)).mirror(false), PartPose.offsetAndRotation(0.3431F, 0.0F, -0.2721F, 0.0F, -1.5708F, 0.0F));

		PartDefinition sw4 = top2.addOrReplaceChild("sw4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r23 = sw4.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(1, 42).addBox(-4.9497F, -3.97F, -13.435F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.4175F, -0.02F, -0.7171F, 0.0F, -1.5708F, 0.0F));

		PartDefinition south4 = top2.addOrReplaceChild("south4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r24 = south4.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(1, 42).mirror().addBox(-5.3431F, -3.98F, -12.7279F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.681F, -0.02F, 0.8384F, 0.0F, 3.1416F, 0.0F));

		PartDefinition north5 = top2.addOrReplaceChild("north5", CubeListBuilder.create().texOffs(1, 42).mirror().addBox(-5.0F, -4.0F, -13.0F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition nw4 = top2.addOrReplaceChild("nw4", CubeListBuilder.create().texOffs(1, 42).addBox(-4.9497F, -3.99F, -13.435F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition ne4 = top2.addOrReplaceChild("ne4", CubeListBuilder.create().texOffs(1, 42).addBox(-5.3431F, -3.99F, -12.7279F, 11.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition top3 = cap.addOrReplaceChild("top3", CubeListBuilder.create(), PartPose.offset(-0.5F, -47.0F, 2.5F));

		PartDefinition east5 = top3.addOrReplaceChild("east5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r25 = east5.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(94, 29).addBox(-4.0F, -0.98F, -13.0F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-20.364F, -0.01F, -2.636F, 0.0F, -1.5708F, 0.0F));

		PartDefinition se5 = top3.addOrReplaceChild("se5", CubeListBuilder.create().texOffs(94, 29).addBox(-2.636F, -1.0F, 6.7071F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition west5 = top3.addOrReplaceChild("west5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r26 = west5.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(94, 29).addBox(-4.0F, -0.98F, -13.0F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.636F, -0.01F, -2.636F, 0.0F, -1.5708F, 0.0F));

		PartDefinition sw5 = top3.addOrReplaceChild("sw5", CubeListBuilder.create().texOffs(94, 29).addBox(-5.6569F, -1.0F, 6.0F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition south5 = top3.addOrReplaceChild("south5", CubeListBuilder.create().texOffs(94, 29).addBox(-4.0F, -0.99F, 5.7279F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition north6 = top3.addOrReplaceChild("north6", CubeListBuilder.create().texOffs(94, 29).addBox(-4.0F, -0.99F, -13.0F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition nw5 = top3.addOrReplaceChild("nw5", CubeListBuilder.create().texOffs(94, 29).addBox(-5.6569F, -1.0F, -12.7279F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition ne5 = top3.addOrReplaceChild("ne5", CubeListBuilder.create().texOffs(94, 29).addBox(-2.636F, -1.0F, -12.0208F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition top4 = cap.addOrReplaceChild("top4", CubeListBuilder.create(), PartPose.offset(-0.5F, -47.7188F, 4.0F));

		PartDefinition east6 = top4.addOrReplaceChild("east6", CubeListBuilder.create().texOffs(92, 36).addBox(-7.9497F, -1.0313F, -7.0503F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition se6 = top4.addOrReplaceChild("se6", CubeListBuilder.create().texOffs(92, 36).addBox(-5.5858F, -1.0112F, -5.6569F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition west6 = top4.addOrReplaceChild("west6", CubeListBuilder.create().texOffs(92, 36).addBox(1.9497F, -1.0313F, -7.0503F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sw6 = top4.addOrReplaceChild("sw6", CubeListBuilder.create().texOffs(92, 37).addBox(-0.7071F, -1.0112F, -6.364F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition south6 = top4.addOrReplaceChild("south6", CubeListBuilder.create().texOffs(92, 36).addBox(-3.0F, -1.0213F, -2.1005F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition north7 = top4.addOrReplaceChild("north7", CubeListBuilder.create().texOffs(92, 36).addBox(-3.0F, -1.0213F, -12.0F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition nw6 = top4.addOrReplaceChild("nw6", CubeListBuilder.create().texOffs(92, 36).addBox(-5.6569F, -1.0112F, -11.3137F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition ne6 = top4.addOrReplaceChild("ne6", CubeListBuilder.create().texOffs(92, 36).addBox(-0.636F, -1.0112F, -10.6066F, 7.0F, 3.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition lamp_frame = cap.addOrReplaceChild("lamp_frame", CubeListBuilder.create().texOffs(51, 80).addBox(-2.0F, 3.5F, -6.0F, 4.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(51, 80).addBox(-2.0F, 1.75F, -6.0F, 4.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(51, 80).addBox(-3.025F, 3.3125F, -7.075F, 6.0F, 2.0F, 6.0F, new CubeDeformation(-0.75F))
		.texOffs(51, 80).addBox(-1.25F, 0.0625F, -6.55F, 1.0F, 5.0F, 5.0F, new CubeDeformation(-0.75F))
		.texOffs(51, 80).addBox(0.25F, 0.0625F, -6.55F, 1.0F, 5.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, -53.2188F, 4.5F));

		PartDefinition cube_r27 = lamp_frame.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(51, 80).addBox(0.25F, -2.525F, -3.25F, 1.0F, 5.0F, 5.0F, new CubeDeformation(-0.75F))
		.texOffs(51, 80).addBox(-1.25F, -2.525F, -3.25F, 1.0F, 5.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.75F, 2.5625F, -4.05F, 0.0F, -1.5708F, 0.0F));

		PartDefinition walls = partdefinition.addOrReplaceChild("walls", CubeListBuilder.create(), PartPose.offset(0.0F, 22.0F, 0.0F));

		PartDefinition east = walls.addOrReplaceChild("east", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r28 = east.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(12, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition se = walls.addOrReplaceChild("se", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r29 = se.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(45, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition west = walls.addOrReplaceChild("west", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r30 = west.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(1, 1).mirror().addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition sw = walls.addOrReplaceChild("sw", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r31 = sw.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(23, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition south = walls.addOrReplaceChild("south", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r32 = south.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(34, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition doors = partdefinition.addOrReplaceChild("doors", CubeListBuilder.create(), PartPose.offset(0.0F, 22.0F, 0.0F));

		PartDefinition door_left = doors.addOrReplaceChild("door_left", CubeListBuilder.create(), PartPose.offset(-12.0625F, 0.0F, -4.9375F));

		PartDefinition north = door_left.addOrReplaceChild("north", CubeListBuilder.create().texOffs(80, 1).addBox(-5.0F, -39.0F, -12.0F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(-1.8125F, -20.75F, -12.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(-1.8125F, -20.75F, -11.25F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(12.0625F, 0.0F, 4.9375F));

		PartDefinition ne = door_left.addOrReplaceChild("ne", CubeListBuilder.create().texOffs(1, 1).addBox(-5.0503F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(1, 1).addBox(-5.0503F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(12.0625F, 0.0F, 4.9375F, 0.0F, 0.7854F, 0.0F));

		PartDefinition door_right = doors.addOrReplaceChild("door_right", CubeListBuilder.create(), PartPose.offset(12.0F, 0.0F, -4.9063F));

		PartDefinition nw = door_right.addOrReplaceChild("nw", CubeListBuilder.create().texOffs(12, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-12.0F, 0.0F, 4.9063F, 0.0F, -0.7854F, 0.0F));

		PartDefinition north2 = door_right.addOrReplaceChild("north2", CubeListBuilder.create().texOffs(68, 1).addBox(0.0F, -39.0F, -12.0F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(0.7625F, -20.75F, -12.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(0.7625F, -20.75F, -11.25F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-12.0F, 0.0F, 4.9063F));

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, -0.5F));

		PartDefinition east2 = base.addOrReplaceChild("east2", CubeListBuilder.create().texOffs(46, 60).addBox(-14.4853F, -2.0F, -5.5147F, 14.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition se2 = base.addOrReplaceChild("se2", CubeListBuilder.create().texOffs(46, 59).addBox(-14.8284F, -1.99F, -5.6569F, 14.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition west2 = base.addOrReplaceChild("west2", CubeListBuilder.create().texOffs(46, 60).addBox(0.4853F, -2.0F, -5.5147F, 14.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sw2 = base.addOrReplaceChild("sw2", CubeListBuilder.create().texOffs(46, 60).addBox(0.8284F, -2.02F, -5.6569F, 14.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition south2 = base.addOrReplaceChild("south2", CubeListBuilder.create().texOffs(46, 59).addBox(-6.0F, -2.01F, 0.9706F, 12.0F, 2.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition north3 = base.addOrReplaceChild("north3", CubeListBuilder.create().texOffs(45, 59).addBox(-6.0F, -2.01F, -14.0F, 12.0F, 2.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(89, 53).addBox(-1.0F, -2.125F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition nw2 = base.addOrReplaceChild("nw2", CubeListBuilder.create().texOffs(46, 59).addBox(-5.6569F, -1.99F, -14.1421F, 12.0F, 2.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition ne2 = base.addOrReplaceChild("ne2", CubeListBuilder.create().texOffs(46, 57).addBox(-6.3431F, -2.02F, -14.1421F, 12.0F, 2.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 53).addBox(-10.9289F, -46.0F, -4.9289F, 22.0F, 44.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glow_bands.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		glow_lamp.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		cap.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		walls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		doors.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		base.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen()){
			this.door_left.offsetRotation(AnimationHelper.degrees(45, AnimationHelper.Axis.Y));
			this.door_right.offsetRotation(AnimationHelper.degrees(-45, AnimationHelper.Axis.Y));
		}
	}

	@Override
	public void animateDemat(T exterior, float age) {

	}

	@Override
	public void animateRemat(T exterior, float age) {

	}

	@Override
	public void animateSolid(T exterior, float age) {

	}
}