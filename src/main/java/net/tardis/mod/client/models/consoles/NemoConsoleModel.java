package net.tardis.mod.client.models.consoles;// Made with Blockbench 4.10.4
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.consoles.NemoConsoleAnimations;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ControlRegistry;

import java.util.Optional;

public class NemoConsoleModel<T extends ConsoleTile> extends BasicTileHierarchicalModel<T> implements IAdditionalConsoleRenderData {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("consoles/nemo"), "main");
	private final ModelPart glow_baseoffset_1;
	private final ModelPart glow_baseoffset_2;
	private final ModelPart glow_baseoffset_3;
	private final ModelPart console;
	private final ModelPart controls;
	private final ModelPart bb_main;
	private final ModelPart time_rotor_slide_y;

	public NemoConsoleModel(ModelPart root) {
		super(root);
		this.glow_baseoffset_1 = root.getChild("glow_baseoffset_1");
		this.glow_baseoffset_2 = root.getChild("glow_baseoffset_2");
		this.glow_baseoffset_3 = root.getChild("glow_baseoffset_3");
		this.console = root.getChild("console");
		this.controls = root.getChild("controls");
		this.bb_main = root.getChild("bb_main");
		this.time_rotor_slide_y = root.getChild("time_rotor_slide_y");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glow_baseoffset_1 = partdefinition.addOrReplaceChild("glow_baseoffset_1", CubeListBuilder.create().texOffs(170, 1).addBox(-2.5F, -3.5F, -4.375F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(170, 1).addBox(-2.5F, -3.5F, 3.2853F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 16.0F, 0.125F));

		PartDefinition glow_baseoffset_2 = partdefinition.addOrReplaceChild("glow_baseoffset_2", CubeListBuilder.create().texOffs(170, 1).addBox(-2.5389F, -3.5F, -4.3526F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(170, 1).addBox(-2.5389F, -3.5F, 3.3077F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 16.0F, 0.125F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_baseoffset_3 = partdefinition.addOrReplaceChild("glow_baseoffset_3", CubeListBuilder.create().texOffs(170, 1).addBox(-2.5389F, -3.5F, -4.3077F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(170, 1).addBox(-2.5389F, -3.5F, 3.3526F, 5.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 16.0F, 0.125F, 0.0F, -2.0944F, 0.0F));

		PartDefinition console = partdefinition.addOrReplaceChild("console", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition bottom_plate = console.addOrReplaceChild("bottom_plate", CubeListBuilder.create().texOffs(11, 95).addBox(-5.0F, -0.9F, -2.0F, 10.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(21, 96).addBox(-4.0F, -0.9F, -4.0F, 8.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(26, 102).addBox(-4.0F, -0.9F, 2.0F, 8.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition stations = console.addOrReplaceChild("stations", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_1 = stations.addOrReplaceChild("station_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition edge_1 = station_1.addOrReplaceChild("edge_1", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_1 = edge_1.addOrReplaceChild("cooling_blades_1", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_1 = edge_1.addOrReplaceChild("plasma_coil_1", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_1 = edge_1.addOrReplaceChild("belly_1", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts = belly_1.addOrReplaceChild("bolts", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b = belly_1.addOrReplaceChild("bolts_b", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c = belly_1.addOrReplaceChild("bolts_c", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_1 = station_1.addOrReplaceChild("plane_1", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_1 = station_1.addOrReplaceChild("rib_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_1 = rib_1.addOrReplaceChild("rib_tilt_1", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_1 = rib_tilt_1.addOrReplaceChild("rib_deco_1", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_2 = rib_1.addOrReplaceChild("lowerrib_tilt_2", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_1 = station_1.addOrReplaceChild("base_fin_1", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_1 = station_1.addOrReplaceChild("clawfoot_1", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_1 = clawfoot_1.addOrReplaceChild("leg_1", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_1 = clawfoot_1.addOrReplaceChild("ball_1", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_2 = stations.addOrReplaceChild("station_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition edge_2 = station_2.addOrReplaceChild("edge_2", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_2 = edge_2.addOrReplaceChild("cooling_blades_2", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_2 = edge_2.addOrReplaceChild("plasma_coil_2", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_2 = edge_2.addOrReplaceChild("belly_2", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts2 = belly_2.addOrReplaceChild("bolts2", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b2 = belly_2.addOrReplaceChild("bolts_b2", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c2 = belly_2.addOrReplaceChild("bolts_c2", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_2 = station_2.addOrReplaceChild("plane_2", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_2 = station_2.addOrReplaceChild("rib_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_2 = rib_2.addOrReplaceChild("rib_tilt_2", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_2 = rib_tilt_2.addOrReplaceChild("rib_deco_2", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_3 = rib_2.addOrReplaceChild("lowerrib_tilt_3", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_2 = station_2.addOrReplaceChild("base_fin_2", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_2 = station_2.addOrReplaceChild("clawfoot_2", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_2 = clawfoot_2.addOrReplaceChild("leg_2", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_2 = clawfoot_2.addOrReplaceChild("ball_2", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_3 = stations.addOrReplaceChild("station_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition edge_3 = station_3.addOrReplaceChild("edge_3", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_3 = edge_3.addOrReplaceChild("cooling_blades_3", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_3 = edge_3.addOrReplaceChild("plasma_coil_3", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_3 = edge_3.addOrReplaceChild("belly_3", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts3 = belly_3.addOrReplaceChild("bolts3", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b3 = belly_3.addOrReplaceChild("bolts_b3", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c3 = belly_3.addOrReplaceChild("bolts_c3", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_3 = station_3.addOrReplaceChild("plane_3", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_3 = station_3.addOrReplaceChild("rib_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_3 = rib_3.addOrReplaceChild("rib_tilt_3", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_3 = rib_tilt_3.addOrReplaceChild("rib_deco_3", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_4 = rib_3.addOrReplaceChild("lowerrib_tilt_4", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_3 = station_3.addOrReplaceChild("base_fin_3", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_3 = station_3.addOrReplaceChild("clawfoot_3", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_3 = clawfoot_3.addOrReplaceChild("leg_3", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_3 = clawfoot_3.addOrReplaceChild("ball_3", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_4 = stations.addOrReplaceChild("station_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition edge_4 = station_4.addOrReplaceChild("edge_4", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_4 = edge_4.addOrReplaceChild("cooling_blades_4", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_4 = edge_4.addOrReplaceChild("plasma_coil_4", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_4 = edge_4.addOrReplaceChild("belly_4", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts4 = belly_4.addOrReplaceChild("bolts4", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b4 = belly_4.addOrReplaceChild("bolts_b4", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c4 = belly_4.addOrReplaceChild("bolts_c4", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_4 = station_4.addOrReplaceChild("plane_4", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_4 = station_4.addOrReplaceChild("rib_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_4 = rib_4.addOrReplaceChild("rib_tilt_4", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_4 = rib_tilt_4.addOrReplaceChild("rib_deco_4", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_5 = rib_4.addOrReplaceChild("lowerrib_tilt_5", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_4 = station_4.addOrReplaceChild("base_fin_4", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_4 = station_4.addOrReplaceChild("clawfoot_4", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_4 = clawfoot_4.addOrReplaceChild("leg_4", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_4 = clawfoot_4.addOrReplaceChild("ball_4", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_5 = stations.addOrReplaceChild("station_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition edge_5 = station_5.addOrReplaceChild("edge_5", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_5 = edge_5.addOrReplaceChild("cooling_blades_5", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_5 = edge_5.addOrReplaceChild("plasma_coil_5", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_5 = edge_5.addOrReplaceChild("belly_5", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts5 = belly_5.addOrReplaceChild("bolts5", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b5 = belly_5.addOrReplaceChild("bolts_b5", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c5 = belly_5.addOrReplaceChild("bolts_c5", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_5 = station_5.addOrReplaceChild("plane_5", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_5 = station_5.addOrReplaceChild("rib_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_5 = rib_5.addOrReplaceChild("rib_tilt_5", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_5 = rib_tilt_5.addOrReplaceChild("rib_deco_5", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_6 = rib_5.addOrReplaceChild("lowerrib_tilt_6", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_5 = station_5.addOrReplaceChild("base_fin_5", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_5 = station_5.addOrReplaceChild("clawfoot_5", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_5 = clawfoot_5.addOrReplaceChild("leg_5", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_5 = clawfoot_5.addOrReplaceChild("ball_5", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_6 = stations.addOrReplaceChild("station_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition edge_6 = station_6.addOrReplaceChild("edge_6", CubeListBuilder.create().texOffs(13, 1).addBox(-8.75F, -15.0F, -15.175F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(59, 96).addBox(-2.5F, -19.75F, -5.25F, 5.0F, 4.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(40, 95).addBox(-2.5F, -17.75F, -3.75F, 5.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(41, 102).addBox(-3.0F, -22.75F, -5.75F, 6.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(38, 93).addBox(-2.75F, -20.5F, -5.75F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(0.875F, -23.75F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 95).addBox(-1.0F, -23.35F, -5.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(51, 95).addBox(-2.8926F, -23.75F, -4.9898F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.425F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(23, 133).addBox(-2.5F, -29.025F, -4.175F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(193, 71).addBox(-2.5F, -29.5F, -4.3F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_6 = edge_6.addOrReplaceChild("cooling_blades_6", CubeListBuilder.create().texOffs(41, 90).addBox(-2.725F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(30, 85).addBox(-0.5F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(46, 84).addBox(1.775F, -11.25F, -5.5F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plasma_coil_6 = edge_6.addOrReplaceChild("plasma_coil_6", CubeListBuilder.create().texOffs(146, 12).addBox(-2.5F, -20.75F, -4.75F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_6 = edge_6.addOrReplaceChild("belly_6", CubeListBuilder.create().texOffs(46, 103).addBox(-3.5F, -4.75F, -6.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 88).addBox(-3.75F, -15.25F, -6.0F, 7.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(19, 126).addBox(-3.5F, -0.75F, -7.0F, 7.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(22, 118).addBox(-3.5F, -1.5F, -6.75F, 7.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 110).addBox(-3.75F, -3.0F, -6.5F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 82).addBox(-3.75F, -15.25F, -6.5F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition bolts6 = belly_6.addOrReplaceChild("bolts6", CubeListBuilder.create().texOffs(142, 88).addBox(-3.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-1.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(-0.5F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(0.75F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(142, 88).addBox(2.0F, -4.5F, -6.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_b6 = belly_6.addOrReplaceChild("bolts_b6", CubeListBuilder.create().texOffs(135, 90).addBox(-3.25F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-2.0F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(-0.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(0.5F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(135, 90).addBox(1.75F, -11.75F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bolts_c6 = belly_6.addOrReplaceChild("bolts_c6", CubeListBuilder.create().texOffs(140, 88).addBox(-2.75F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(-1.5F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(0.0F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(140, 88).addBox(1.25F, -22.5F, -6.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_6 = station_6.addOrReplaceChild("plane_6", CubeListBuilder.create().texOffs(13, 42).addBox(-0.3F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(13, 42).addBox(-7.8F, -20.0F, -4.5F, 8.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(56, 58).addBox(-7.05F, -20.0F, -3.0F, 14.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(28, 61).addBox(-0.8F, -20.0F, -1.5F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 42).addBox(-6.3F, -20.0F, -1.5F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 28).addBox(-5.55F, -20.0F, 0.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-0.3F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 42).addBox(-4.8F, -20.0F, 1.5F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(12, 41).addBox(-4.05F, -20.0F, 3.0F, 8.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F))
		.texOffs(126, 83).addBox(-4.75F, -19.25F, -4.25F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(127, 82).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_6 = station_6.addOrReplaceChild("rib_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_6 = rib_6.addOrReplaceChild("rib_tilt_6", CubeListBuilder.create().texOffs(20, 76).addBox(-0.5F, -20.925F, -9.875F, 1.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(20, 75).addBox(-0.5F, -19.5F, -9.75F, 1.0F, 2.0F, 13.0F, new CubeDeformation(-0.2F))
		.texOffs(9, 82).addBox(-0.7F, -19.5F, -3.0F, 1.0F, 7.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_6 = rib_tilt_6.addOrReplaceChild("rib_deco_6", CubeListBuilder.create().texOffs(31, 101).addBox(-1.0F, -21.125F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(9, 83).addBox(-1.0F, -21.275F, 0.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(8, 82).addBox(-1.0F, -21.35F, 1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lowerrib_tilt_7 = rib_6.addOrReplaceChild("lowerrib_tilt_7", CubeListBuilder.create().texOffs(205, 68).addBox(-0.7F, -18.5F, -4.75F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(19, 96).addBox(-0.675F, -17.75F, -2.0F, 1.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(25, 102).addBox(-0.65F, -5.7F, -10.25F, 1.0F, 1.0F, 5.0F, new CubeDeformation(-0.25F))
		.texOffs(22, 127).addBox(-0.65F, -5.025F, -8.325F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.02F))
		.texOffs(235, 106).addBox(-0.7F, -7.609F, -12.497F, 1.0F, 1.0F, 6.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition base_fin_6 = station_6.addOrReplaceChild("base_fin_6", CubeListBuilder.create().texOffs(7, 85).addBox(-0.75F, -12.05F, -5.75F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 78).addBox(-0.525F, -24.175F, -7.0F, 1.0F, 6.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(25, 88).addBox(-0.5F, -29.55F, -4.75F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(22, 111).addBox(-0.6F, -3.05F, -8.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(16, 90).addBox(-0.75F, -5.55F, -7.25F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(176, 117).addBox(-0.2F, -11.115F, -8.0136F, 0.0F, 10.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition clawfoot_6 = station_6.addOrReplaceChild("clawfoot_6", CubeListBuilder.create().texOffs(231, 8).addBox(-0.7F, -10.05F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -3.9F, -9.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(231, 8).addBox(-0.7F, -7.15F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(192, 80).addBox(-0.7F, -7.14F, -9.2636F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.28F))
		.texOffs(178, 66).addBox(-0.2F, -10.115F, -9.7636F, 0.0F, 7.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_6 = clawfoot_6.addOrReplaceChild("leg_6", CubeListBuilder.create().texOffs(14, 118).addBox(-0.65F, -1.05F, -14.5F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_6 = clawfoot_6.addOrReplaceChild("ball_6", CubeListBuilder.create().texOffs(29, 102).addBox(-1.1F, -1.75F, -15.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -13.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -12.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(25, 124).addBox(-1.1F, -1.675F, -11.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition controls = partdefinition.addOrReplaceChild("controls", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition side1 = controls.addOrReplaceChild("side1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dummy_barometer = side1.addOrReplaceChild("dummy_barometer", CubeListBuilder.create().texOffs(180, 151).addBox(-2.0F, -22.5F, -8.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(180, 151).addBox(-2.0F, -22.4F, -8.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(180, 151).addBox(-2.0F, -20.15F, -8.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(180, 151).addBox(-2.0F, -20.25F, -8.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offset(0.2F, 0.0F, 0.25F));

		PartDefinition glow_barometer = dummy_barometer.addOrReplaceChild("glow_barometer", CubeListBuilder.create().texOffs(24, 153).addBox(-1.5F, -22.5F, -8.0F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(23, 163).addBox(0.975F, -21.125F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(23, 163).addBox(0.975F, -21.325F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(23, 163).addBox(0.975F, -21.525F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(32, 160).addBox(0.975F, -21.725F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(32, 160).addBox(0.775F, -21.725F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(32, 160).addBox(0.575F, -21.725F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(32, 160).addBox(0.375F, -21.725F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(23, 158).addBox(0.18F, -22.48F, -7.98F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(24, 157).addBox(-1.5F, -22.825F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(26, 156).addBox(-1.5F, -23.6F, -8.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(22, 157).addBox(0.2F, -20.75F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.01F))
		.texOffs(15, 166).addBox(0.175F, -22.725F, -8.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(206, 216).addBox(0.2F, -19.75F, -8.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(203, 201).addBox(0.2F, -20.5F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(204, 211).addBox(0.975F, -20.725F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(204, 211).addBox(0.975F, -20.925F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(205, 206).addBox(-0.025F, -20.525F, -8.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_tilt_3 = side1.addOrReplaceChild("station_tilt_3", CubeListBuilder.create().texOffs(232, 18).addBox(-1.675F, -4.3F, -20.4F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(232, 18).addBox(-0.175F, -4.3F, -20.4F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(201, 181).addBox(-1.95F, -4.3F, -20.225F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(198, 176).addBox(-5.5F, 1.25F, -20.275F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(198, 176).addBox(2.35F, 0.375F, -20.275F, 3.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(198, 176).addBox(-5.5F, 0.75F, -20.275F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(204, 162).addBox(-5.025F, 2.775F, -20.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(69, 152).addBox(-1.975F, -1.25F, -20.1F, 4.0F, 6.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition spacer = station_tilt_3.addOrReplaceChild("spacer", CubeListBuilder.create(), PartPose.offset(-5.5F, -1.25F, -0.75F));

		PartDefinition handbreak = station_tilt_3.addOrReplaceChild("handbreak", CubeListBuilder.create().texOffs(127, 164).addBox(-2.05F, -0.9281F, -3.9969F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(79, 87).addBox(-0.9F, -0.4031F, -3.2469F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(200, 97).addBox(-1.05F, -0.9281F, -3.5469F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(200, 97).addBox(-1.05F, -0.9281F, -1.9469F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(233, 15).addBox(-1.275F, -0.9281F, -0.8219F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(81, 87).addBox(-1.05F, -0.9281F, -3.2969F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(1.75F, 1.7F, -20.175F, 1.4835F, 0.0F, 0.0F));

		PartDefinition bone2 = handbreak.addOrReplaceChild("bone2", CubeListBuilder.create().texOffs(78, 87).addBox(-1.4756F, -0.425F, -0.4957F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.46F))
		.texOffs(78, 87).addBox(-1.4756F, -0.5F, -0.4957F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.46F))
		.texOffs(78, 87).addBox(-1.4756F, -0.575F, -0.4957F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.46F)), PartPose.offsetAndRotation(-0.4089F, 0.0969F, -2.7418F, 0.0F, 0.1745F, 0.0F));

		PartDefinition throttle = station_tilt_3.addOrReplaceChild("throttle", CubeListBuilder.create().texOffs(127, 164).addBox(-1.6F, -1.0031F, -4.2219F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(93, 222).addBox(0.075F, -0.5031F, -3.7219F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 97).addBox(-1.525F, -1.0031F, -3.7719F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(200, 97).addBox(-1.525F, -1.0031F, -2.1719F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(225, 13).addBox(-1.4F, -1.0031F, -1.0469F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(81, 87).addBox(-1.525F, -1.0031F, -3.3969F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.125F, 1.775F, -19.95F, 1.3526F, 0.0F, 0.0F));

		PartDefinition levermount = station_tilt_3.addOrReplaceChild("levermount", CubeListBuilder.create().texOffs(151, 155).addBox(2.9F, 2.0469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(2.9F, 2.5469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(2.9F, 3.0469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(0.95F, 2.0469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(0.95F, 3.0469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(0.95F, 2.5469F, -21.2219F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F)), PartPose.offset(-1.875F, -1.25F, -0.75F));

		PartDefinition levermount_wheel = levermount.addOrReplaceChild("levermount_wheel", CubeListBuilder.create().texOffs(82, 148).addBox(2.175F, 2.0076F, -21.0076F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 148).addBox(0.675F, 2.0076F, -21.0076F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(17, 161).addBox(2.375F, 2.0F, -20.75F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(209, 83).addBox(3.35F, 2.0F, -20.2F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(209, 83).addBox(-0.55F, 2.0F, -20.2F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(17, 161).addBox(0.475F, 2.0F, -20.75F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition wheelmount = levermount_wheel.addOrReplaceChild("wheelmount", CubeListBuilder.create().texOffs(32, 155).addBox(-0.65F, 0.1768F, -1.2374F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(32, 155).addBox(-2.5F, 0.1768F, -1.2374F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(19, 146).addBox(-0.65F, -1.2374F, 0.1768F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(19, 146).addBox(-2.5F, -1.2374F, 0.1768F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(85, 165).addBox(-0.825F, 0.0F, -1.425F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 165).addBox(-2.325F, 0.0F, -1.425F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(74, 155).addBox(-0.825F, -1.4142F, -0.0108F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(74, 155).addBox(-2.325F, -1.4142F, -0.0108F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 3.0F, -20.0F, 0.7854F, 0.0F, 0.0F));

		PartDefinition wheelmarkertilt = wheelmount.addOrReplaceChild("wheelmarkertilt", CubeListBuilder.create().texOffs(151, 155).addBox(3.65F, 2.4F, -21.25F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 2.4F, -21.8F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 2.4F, -20.75F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 2.4F, -20.25F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 2.4F, -19.75F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 3.85F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 4.35F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 4.85F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.65F, 5.35F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F)), PartPose.offset(-5.675F, -4.1031F, 21.5281F));

		PartDefinition wheelmarkertilt2 = wheelmount.addOrReplaceChild("wheelmarkertilt2", CubeListBuilder.create().texOffs(151, 155).addBox(3.525F, 2.4F, -21.25F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 2.4F, -21.8F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 2.4F, -20.75F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 2.4F, -20.25F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 2.4F, -19.75F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 3.85F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 4.35F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 4.85F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(151, 155).addBox(3.525F, 5.35F, -23.225F, 0.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F)), PartPose.offset(-3.65F, -4.1031F, 21.5281F));

		PartDefinition spacer_wheel1 = levermount.addOrReplaceChild("spacer_wheel1", CubeListBuilder.create().texOffs(220, 11).addBox(2.5F, 2.25F, -20.8964F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(221, 12).addBox(2.5F, 1.75F, -20.8964F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offset(-1.075F, 0.0F, 0.0F));

		PartDefinition spacer_tilt1 = spacer_wheel1.addOrReplaceChild("spacer_tilt1", CubeListBuilder.create().texOffs(220, 11).addBox(-0.5F, 0.0F, -1.4142F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(220, 11).addBox(-0.5F, -1.4142F, 0.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(3.0F, 3.0F, -20.0F, 0.7854F, 0.0F, 0.0F));

		PartDefinition refueler = station_tilt_3.addOrReplaceChild("refueler", CubeListBuilder.create().texOffs(134, 161).addBox(-5.475F, 1.9F, -20.625F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.3F, 1.3F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.5F, 1.3F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.5F, 1.5F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.5F, 1.7F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-3.45F, 1.7F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-3.45F, 1.5F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-3.45F, 1.3F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.3F, 1.1F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 161).addBox(-5.3F, 0.9F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(136, 162).addBox(-5.0F, 0.8F, -20.0F, 2.0F, 1.0F, 0.0F, new CubeDeformation(-0.4F))
		.texOffs(135, 160).addBox(-5.2F, 0.8F, -20.725F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(135, 160).addBox(-3.8F, 0.8F, -20.725F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(135, 160).addBox(-3.65F, 1.275F, -20.65F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(135, 160).addBox(-3.65F, 1.075F, -20.65F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(135, 160).addBox(-3.65F, 0.875F, -20.65F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition refuel_needle_rotate_z = refueler.addOrReplaceChild("refuel_needle_rotate_z", CubeListBuilder.create().texOffs(105, 230).addBox(-0.5F, -0.5F, -0.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(105, 230).addBox(-0.5F, -0.7F, -0.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(105, 230).addBox(-0.5F, -0.9F, -0.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(105, 230).addBox(-0.5F, -1.1F, -0.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-3.975F, 2.3F, -19.8844F, 0.0F, 0.0F, -1.5708F));

		PartDefinition glow_refuel = refueler.addOrReplaceChild("glow_refuel", CubeListBuilder.create().texOffs(97, 233).addBox(-5.15F, 2.75F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(97, 233).addBox(-3.9F, 2.75F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(97, 233).addBox(-4.525F, 2.75F, -20.625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(16, 164).addBox(-5.0F, 1.125F, -20.425F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(15, 163).addBox(-4.75F, 1.625F, -20.425F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(17, 164).addBox(-5.25F, 1.625F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dummy_dial_c1 = station_tilt_3.addOrReplaceChild("dummy_dial_c1", CubeListBuilder.create().texOffs(181, 151).addBox(0.6125F, 0.975F, -0.35F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(227, 11).addBox(0.6375F, 1.0F, -0.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(134, 152).addBox(0.6375F, 1.0F, -0.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-2.5125F, -4.275F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_c1 = dummy_dial_c1.addOrReplaceChild("glow_c1", CubeListBuilder.create().texOffs(188, 13).addBox(-2.125F, -3.25F, -20.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition dummy_dial_c2 = station_tilt_3.addOrReplaceChild("dummy_dial_c2", CubeListBuilder.create().texOffs(181, 151).addBox(0.6125F, 0.975F, -0.35F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(227, 11).addBox(0.6375F, 1.0F, -0.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(134, 152).addBox(0.6375F, 1.0F, -0.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(3.0875F, -4.275F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_c2 = dummy_dial_c2.addOrReplaceChild("glow_c2", CubeListBuilder.create().texOffs(185, 13).addBox(-2.125F, -3.25F, -20.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition dummy_button_C1 = station_tilt_3.addOrReplaceChild("dummy_button_C1", CubeListBuilder.create().texOffs(196, 87).addBox(3.6F, 0.6F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(4.225F, 0.6F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(4.225F, 1.225F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(3.6F, 1.225F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(4.225F, 1.85F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(3.6F, 1.85F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(4.225F, 2.475F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(3.6F, 2.475F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(4.225F, 3.075F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(196, 87).addBox(3.6F, 3.075F, -20.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(89, 152).addBox(2.575F, 0.925F, -20.425F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(89, 152).addBox(2.975F, 0.925F, -20.425F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition side2 = controls.addOrReplaceChild("side2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition station_tilt_2 = side2.addOrReplaceChild("station_tilt_2", CubeListBuilder.create().texOffs(200, 180).addBox(-1.0F, 0.525F, -19.9F, 2.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(202, 181).addBox(-4.75F, 0.5F, -19.95F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(229, 12).addBox(-4.75F, 0.425F, -20.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(201, 183).addBox(1.75F, 0.5F, -20.0F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition facing_dial = station_tilt_2.addOrReplaceChild("facing_dial", CubeListBuilder.create().texOffs(73, 157).addBox(-1.575F, -1.55F, -0.25F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(182, 152).addBox(-0.55F, -0.6F, -0.525F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(213, 92).addBox(0.575F, -1.425F, -0.575F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(213, 92).addBox(-1.725F, -1.725F, -0.575F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(211, 92).addBox(-1.725F, 0.575F, -0.575F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(211, 92).addBox(-1.425F, -1.725F, -0.575F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-3.25F, 2.0F, -20.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition faceing_needle_rotate_z = facing_dial.addOrReplaceChild("faceing_needle_rotate_z", CubeListBuilder.create().texOffs(144, 155).addBox(-1.356F, -0.4927F, -0.4313F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.05F, -0.1F, -0.1812F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_dial = facing_dial.addOrReplaceChild("glow_dial", CubeListBuilder.create().texOffs(21, 159).addBox(-4.325F, 0.925F, -20.175F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(3.25F, -2.0F, 20.0F));

		PartDefinition landing_type = station_tilt_2.addOrReplaceChild("landing_type", CubeListBuilder.create().texOffs(133, 152).addBox(2.87F, 1.525F, -20.775F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(133, 152).addBox(1.45F, 1.525F, -20.775F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 152).addBox(2.66F, 1.515F, -20.765F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.39F))
		.texOffs(133, 152).addBox(2.87F, 0.325F, -20.775F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(134, 152).addBox(2.66F, 0.335F, -20.765F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.39F))
		.texOffs(133, 152).addBox(1.45F, 0.325F, -20.775F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.15F, 0.0F, 0.175F));

		PartDefinition rotate_bar_x = landing_type.addOrReplaceChild("rotate_bar_x", CubeListBuilder.create(), PartPose.offset(3.125F, 1.625F, 3.4F));

		PartDefinition cube_r1 = rotate_bar_x.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(138, 81).addBox(3.15F, 1.45F, -21.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.41F))
		.texOffs(138, 81).addBox(3.33F, 1.45F, -21.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.41F))
		.texOffs(138, 81).addBox(2.205F, 1.45F, -21.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.41F))
		.texOffs(138, 81).addBox(2.025F, 1.45F, -21.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.41F))
		.texOffs(82, 175).addBox(2.175F, 1.45F, -21.275F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-3.125F, -1.625F, -3.4F, -0.0305F, 0.0F, 0.0F));

		PartDefinition cube_r2 = rotate_bar_x.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(83, 175).addBox(3.175F, 1.45F, -22.775F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-2.825F, -1.5792F, -1.9007F, -0.0305F, 0.0F, 0.0F));

		PartDefinition cube_r3 = rotate_bar_x.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(83, 175).addBox(3.175F, 1.45F, -21.775F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-4.425F, -1.6097F, -2.9002F, -0.0305F, 0.0F, 0.0F));

		PartDefinition slider_plate = landing_type.addOrReplaceChild("slider_plate", CubeListBuilder.create().texOffs(238, 103).addBox(1.675F, 0.6F, -20.95F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(238, 103).addBox(1.675F, 2.3F, -20.95F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(234, 110).addBox(1.725F, 0.95F, -20.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(230, 112).addBox(3.625F, 0.95F, -20.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(237, 115).addBox(2.475F, 0.95F, -20.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(237, 115).addBox(2.875F, 0.95F, -20.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition up = landing_type.addOrReplaceChild("up", CubeListBuilder.create().texOffs(203, 77).addBox(3.1F, -5.9F, -0.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.25F, 2.0F, -20.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition down = landing_type.addOrReplaceChild("down", CubeListBuilder.create().texOffs(205, 95).addBox(4.75F, -4.25F, -0.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.25F, 2.0F, -20.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition dummy_button_b1 = station_tilt_2.addOrReplaceChild("dummy_button_b1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_b1 = dummy_button_b1.addOrReplaceChild("glow_b1", CubeListBuilder.create().texOffs(188, 12).addBox(0.125F, 2.55F, -20.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-1.125F, 2.55F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-0.5F, 2.55F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(0.125F, 1.825F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-1.125F, 1.825F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-0.5F, 1.825F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(0.125F, 1.125F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-1.125F, 1.125F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-0.5F, 1.125F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(0.125F, 0.425F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-1.125F, 0.425F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(188, 12).addBox(-0.5F, 0.425F, -20.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition randomizer = station_tilt_2.addOrReplaceChild("randomizer", CubeListBuilder.create().texOffs(133, 155).addBox(-1.65F, -3.25F, -19.6F, 3.0F, 2.0F, 1.0F, new CubeDeformation(0.3F))
		.texOffs(203, 180).addBox(1.2F, -3.7F, -20.45F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(203, 180).addBox(-2.6F, -3.7F, -20.45F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(203, 181).addBox(-2.6F, -4.1F, -20.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(203, 181).addBox(-2.6F, -1.3F, -20.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(203, 181).addBox(1.2F, -4.1F, -20.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(203, 181).addBox(1.2F, -1.3F, -20.45F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(201, 180).addBox(-2.2F, -1.3F, -20.45F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(201, 180).addBox(-2.2F, -4.1F, -20.45F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(205, 80).addBox(-0.925F, -1.275F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(205, 80).addBox(-0.425F, -1.275F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(205, 80).addBox(-0.425F, -4.1F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(205, 80).addBox(-0.925F, -4.1F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.2F, 0.0F, 0.0F));

		PartDefinition rotate_y = randomizer.addOrReplaceChild("rotate_y", CubeListBuilder.create().texOffs(9, 221).addBox(0.175F, -1.05F, 0.175F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(11, 216).addBox(0.275F, -1.925F, -1.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(0.3F, -2.325F, -0.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(0.275F, -1.925F, 0.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(-0.725F, -1.925F, 0.325F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(-1.075F, -1.575F, 0.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(-1.425F, -1.0F, 0.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(11, 216).addBox(-1.425F, -1.75F, -0.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(11, 216).addBox(-1.425F, -1.925F, -0.575F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(11, 216).addBox(-1.225F, -2.075F, -1.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.19F))
		.texOffs(24, 218).addBox(-0.3F, -1.625F, -1.15F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(24, 218).addBox(-0.6F, -1.525F, -1.125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(11, 216).addBox(0.225F, -1.025F, -1.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(-1.275F, -0.725F, -1.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(11, 216).addBox(-1.025F, -0.7F, -1.05F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(29, 161).addBox(-0.55F, -2.075F, -0.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(29, 161).addBox(-0.55F, -0.9F, -0.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(29, 161).addBox(-1.0F, -2.35F, -1.025F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(29, 161).addBox(0.025F, -2.325F, -0.475F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(32, 223).addBox(-0.3F, -1.175F, -1.325F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(29, 161).addBox(0.2F, -2.5F, -1.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(29, 161).addBox(-0.975F, -2.325F, 0.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(16, 220).addBox(1.05F, -1.45F, 2.95F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(206, 83).addBox(0.125F, -2.6F, 3.875F, 0.0F, 3.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.125F, -1.2F, -19.825F));

		PartDefinition cube_r4 = rotate_y.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(233, 99).addBox(-0.5F, -1.5F, -1.5F, 1.0F, 3.0F, 3.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.025F, -1.0F, -0.05F, 0.0F, 1.5708F, 0.0F));

		PartDefinition glow_globe = rotate_y.addOrReplaceChild("glow_globe", CubeListBuilder.create().texOffs(182, 230).addBox(-1.125F, -3.675F, -21.275F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.125F, 1.675F, 20.275F));

		PartDefinition pipes_2 = station_tilt_2.addOrReplaceChild("pipes_2", CubeListBuilder.create().texOffs(239, 103).addBox(0.5F, 3.125F, -19.9375F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(-4.725F, 3.45F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(-4.525F, 3.45F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(-4.225F, 3.175F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(-4.225F, 2.975F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(239, 103).addBox(-0.9F, 1.725F, -19.9375F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(0.3F, 1.925F, -19.9375F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(231, 18).addBox(0.3F, 1.725F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(231, 18).addBox(0.3F, 3.125F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(231, 18).addBox(-4.975F, 3.45F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(231, 18).addBox(-4.225F, 3.45F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(231, 18).addBox(1.7F, 3.125F, -19.9375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(240, 103).addBox(1.7F, 3.325F, -19.9375F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(240, 103).addBox(-4.975F, 3.65F, -19.9375F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(2.25F, -4.0F, -0.4375F));

		PartDefinition side3 = controls.addOrReplaceChild("side3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition station_offset_1 = side3.addOrReplaceChild("station_offset_1", CubeListBuilder.create().texOffs(68, 152).addBox(-1.125F, -8.25F, -20.25F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -3.5F, 3.1F, -0.2618F, 0.0F, 0.0F));

		PartDefinition ships_wheel = station_offset_1.addOrReplaceChild("ships_wheel", CubeListBuilder.create().texOffs(231, 100).addBox(-1.0F, -1.725F, 0.475F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.7F))
		.texOffs(227, 26).addBox(-0.5F, -1.225F, 0.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.7F))
		.texOffs(86, 173).addBox(-0.5F, -1.125F, 1.05F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.1F, -7.4747F, -21.2435F, -0.1309F, 0.0F, 0.0F));

		PartDefinition xyz_increment_rotate_z = ships_wheel.addOrReplaceChild("xyz_increment_rotate_z", CubeListBuilder.create().texOffs(172, 34).addBox(-0.975F, -0.975F, -0.4F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.7F)), PartPose.offset(-0.025F, -0.75F, 0.375F));

		PartDefinition spokes = xyz_increment_rotate_z.addOrReplaceChild("spokes", CubeListBuilder.create(), PartPose.offset(0.125F, 0.1646F, 0.675F));

		PartDefinition A1 = spokes.addOrReplaceChild("A1", CubeListBuilder.create().texOffs(181, 152).addBox(-1.075F, -9.55F, -22.15F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(181, 152).addBox(-1.075F, -6.4287F, -22.15F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(182, 152).addBox(0.9857F, -8.4893F, -22.15F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(182, 152).addBox(-2.1357F, -8.4893F, -22.15F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(202, 180).addBox(-0.2643F, -7.9393F, -22.15F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(203, 180).addBox(1.1857F, -7.9393F, -22.15F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 180).addBox(-1.7643F, -7.9143F, -22.15F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(203, 180).addBox(-2.3393F, -7.9143F, -22.15F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(205, 176).addBox(-0.575F, -7.625F, -22.15F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(205, 176).addBox(-0.575F, -6.225F, -22.15F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 177).addBox(-0.575F, -9.375F, -22.15F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 177).addBox(-0.575F, -9.75F, -22.15F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offset(-0.05F, 7.275F, 21.075F));

		PartDefinition innerring_a1 = A1.addOrReplaceChild("innerring_a1", CubeListBuilder.create().texOffs(209, 182).addBox(-1.475F, 0.625F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(205, 181).addBox(-0.475F, 0.625F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(208, 182).addBox(-1.475F, -1.8F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(212, 183).addBox(-0.475F, -1.8F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(201, 180).addBox(0.75F, -0.3F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(211, 181).addBox(0.75F, -1.75F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(208, 182).addBox(-1.7F, -1.75F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(210, 182).addBox(-1.7F, -0.3F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(-0.1F, -7.4F, -21.9F));

		PartDefinition handle = A1.addOrReplaceChild("handle", CubeListBuilder.create().texOffs(229, 98).addBox(-3.275F, -0.55F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-2.975F, -0.55F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-2.725F, -0.55F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(1.3F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(1.525F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(1.825F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.7107F, 1.9393F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.7107F, 1.3893F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(-0.7107F, 1.6393F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.7107F, -2.8857F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.7107F, -3.1857F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.7107F, -2.6357F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.1357F, -7.3643F, -21.95F));

		PartDefinition A2 = spokes.addOrReplaceChild("A2", CubeListBuilder.create().texOffs(181, 152).addBox(-0.8952F, -1.9029F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.251F))
		.texOffs(181, 152).addBox(-0.8952F, 1.2185F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.251F))
		.texOffs(182, 152).addBox(1.1654F, -0.8422F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.251F))
		.texOffs(182, 152).addBox(-1.9559F, -0.8422F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.251F))
		.texOffs(202, 177).addBox(-0.3375F, -1.725F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.36F))
		.texOffs(202, 177).addBox(-0.3375F, -2.1F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 180).addBox(-1.7268F, -0.2643F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.36F))
		.texOffs(203, 180).addBox(-2.1518F, -0.2643F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(205, 176).addBox(-0.3625F, 0.025F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.36F))
		.texOffs(205, 176).addBox(-0.3625F, 1.425F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 180).addBox(0.0232F, -0.3143F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.36F))
		.texOffs(203, 180).addBox(1.3732F, -0.3143F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-0.0875F, -0.4F, -0.575F, 0.0F, 0.0F, 0.7854F));

		PartDefinition innerring_a2 = A2.addOrReplaceChild("innerring_a2", CubeListBuilder.create().texOffs(207, 181).addBox(-0.15F, 0.625F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(209, 182).addBox(-1.45F, 0.625F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(209, 182).addBox(-1.475F, -1.825F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(203, 181).addBox(-0.075F, -1.825F, -0.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(206, 180).addBox(0.875F, -0.4F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(202, 179).addBox(0.875F, -1.8F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(211, 181).addBox(-1.55F, -1.75F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(208, 181).addBox(-0.75F, -1.05F, -0.25F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.401F))
		.texOffs(209, 182).addBox(-1.55F, -0.3F, -0.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.401F)), PartPose.offset(-0.0625F, 0.275F, -0.25F));

		PartDefinition handle2 = A2.addOrReplaceChild("handle2", CubeListBuilder.create().texOffs(229, 98).addBox(-3.125F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-2.825F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-2.575F, -0.575F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(1.45F, -0.625F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(1.7F, -0.625F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(2.0F, -0.625F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.5357F, 1.9143F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.5357F, 1.3643F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(229, 98).addBox(-0.5357F, 1.6143F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.5107F, -2.9107F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.5107F, -3.2107F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(229, 98).addBox(-0.5107F, -2.6607F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.1732F, 0.3107F, -0.3F));

		PartDefinition station_tilt_1 = side3.addOrReplaceChild("station_tilt_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition coorddial_x = station_tilt_1.addOrReplaceChild("coorddial_x", CubeListBuilder.create().texOffs(206, 182).addBox(0.7625F, -0.4F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(217, 24).addBox(0.7625F, -0.4F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(136, 156).addBox(0.736F, -0.4265F, -0.765F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(-2.7625F, -4.275F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_cord_x = coorddial_x.addOrReplaceChild("glow_cord_x", CubeListBuilder.create().texOffs(185, 11).addBox(-1.5265F, -4.2015F, -20.1649F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition coorddial_y = station_tilt_1.addOrReplaceChild("coorddial_y", CubeListBuilder.create().texOffs(206, 182).addBox(0.7625F, -0.4F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(217, 24).addBox(0.7625F, -0.4F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(136, 156).addBox(0.736F, -0.4265F, -0.765F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(-0.818F, -5.1589F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_cord_y = coorddial_y.addOrReplaceChild("glow_cord_y", CubeListBuilder.create().texOffs(185, 11).addBox(-1.5265F, -4.2015F, -20.1649F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition coorddial_z = station_tilt_1.addOrReplaceChild("coorddial_z", CubeListBuilder.create().texOffs(206, 182).addBox(0.7625F, -0.4F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(217, 24).addBox(0.7625F, -0.4F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(136, 156).addBox(0.736F, -0.4265F, -0.765F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(1.1266F, -4.275F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_cord_z = coorddial_z.addOrReplaceChild("glow_cord_z", CubeListBuilder.create().texOffs(185, 11).addBox(-1.5265F, -4.2015F, -20.1649F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition fancy_nameplate = station_tilt_1.addOrReplaceChild("fancy_nameplate", CubeListBuilder.create().texOffs(196, 180).addBox(-0.925F, -1.225F, -19.875F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(233, 126).addBox(-0.925F, -1.225F, -20.025F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(234, 126).addBox(-1.525F, -1.225F, -20.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(234, 126).addBox(0.675F, -1.225F, -20.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(197, 180).addBox(-1.725F, -1.225F, -20.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(197, 180).addBox(0.875F, -1.225F, -20.075F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, -0.35F, 0.0F));

		PartDefinition cube_r5 = fancy_nameplate.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(221, 23).addBox(1.05F, -2.05F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(221, 23).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.025F, -0.725F, -19.875F, 0.0F, 0.0F, 0.7854F));

		PartDefinition dummy_button_a1 = station_tilt_1.addOrReplaceChild("dummy_button_a1", CubeListBuilder.create().texOffs(207, 182).addBox(-6.27F, -0.49F, -8.35F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(133, 153).addBox(-6.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-4.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-5.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(1.845F, 0.265F, -11.525F));

		PartDefinition dummy_button_a2 = station_tilt_1.addOrReplaceChild("dummy_button_a2", CubeListBuilder.create().texOffs(207, 182).addBox(-6.27F, -0.49F, -8.35F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(133, 153).addBox(-6.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-4.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-5.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(4.845F, 0.265F, -11.525F));

		PartDefinition dummy_button_a3 = station_tilt_1.addOrReplaceChild("dummy_button_a3", CubeListBuilder.create().texOffs(207, 182).addBox(-6.27F, -0.49F, -8.35F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(133, 153).addBox(-6.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-4.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(133, 153).addBox(-5.27F, -0.49F, -8.725F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(7.845F, 0.265F, -11.525F));

		PartDefinition dummy_toggle_a1 = station_tilt_1.addOrReplaceChild("dummy_toggle_a1", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-2.955F, 1.465F, -11.775F));

		PartDefinition switch_x_a1 = dummy_toggle_a1.addOrReplaceChild("switch_x_a1", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt = switch_x_a1.addOrReplaceChild("tilt", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt2 = switch_x_a1.addOrReplaceChild("tilt2", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a2 = station_tilt_1.addOrReplaceChild("dummy_toggle_a2", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-1.555F, 1.465F, -11.775F));

		PartDefinition switch_x_a2 = dummy_toggle_a2.addOrReplaceChild("switch_x_a2", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt3 = switch_x_a2.addOrReplaceChild("tilt3", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt4 = switch_x_a2.addOrReplaceChild("tilt4", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a3 = station_tilt_1.addOrReplaceChild("dummy_toggle_a3", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-2.955F, 3.09F, -11.775F));

		PartDefinition switch_x_a3 = dummy_toggle_a3.addOrReplaceChild("switch_x_a3", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt5 = switch_x_a3.addOrReplaceChild("tilt5", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt6 = switch_x_a3.addOrReplaceChild("tilt6", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a4 = station_tilt_1.addOrReplaceChild("dummy_toggle_a4", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-1.555F, 3.09F, -11.775F));

		PartDefinition switch_x_a4 = dummy_toggle_a4.addOrReplaceChild("switch_x_a4", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt7 = switch_x_a4.addOrReplaceChild("tilt7", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt8 = switch_x_a4.addOrReplaceChild("tilt8", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a5 = station_tilt_1.addOrReplaceChild("dummy_toggle_a5", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(5.195F, 1.465F, -11.775F));

		PartDefinition switch_x_a5 = dummy_toggle_a5.addOrReplaceChild("switch_x_a5", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt9 = switch_x_a5.addOrReplaceChild("tilt9", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt10 = switch_x_a5.addOrReplaceChild("tilt10", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a6 = station_tilt_1.addOrReplaceChild("dummy_toggle_a6", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(6.595F, 1.465F, -11.775F));

		PartDefinition switch_x_a6 = dummy_toggle_a6.addOrReplaceChild("switch_x_a6", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt11 = switch_x_a6.addOrReplaceChild("tilt11", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt12 = switch_x_a6.addOrReplaceChild("tilt12", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a7 = station_tilt_1.addOrReplaceChild("dummy_toggle_a7", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(5.195F, 3.065F, -11.775F));

		PartDefinition switch_x_a7 = dummy_toggle_a7.addOrReplaceChild("switch_x_a7", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt13 = switch_x_a7.addOrReplaceChild("tilt13", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt14 = switch_x_a7.addOrReplaceChild("tilt14", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_toggle_a8 = station_tilt_1.addOrReplaceChild("dummy_toggle_a8", CubeListBuilder.create().texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.745F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-2.345F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(209, 181).addBox(-1.945F, -0.69F, -8.425F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(6.62F, 3.065F, -11.775F));

		PartDefinition switch_x_a8 = dummy_toggle_a8.addOrReplaceChild("switch_x_a8", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.77F, 0.3922F, -7.9825F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt15 = switch_x_a8.addOrReplaceChild("tilt15", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(207, 86).addBox(-0.55F, -0.808F, -0.4714F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition tilt16 = switch_x_a8.addOrReplaceChild("tilt16", CubeListBuilder.create().texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F))
		.texOffs(207, 86).addBox(-0.55F, -0.4118F, -0.4674F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.21F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3491F, 0.0F, 0.0F));

		PartDefinition side4 = controls.addOrReplaceChild("side4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition dimention_select = side4.addOrReplaceChild("dimention_select", CubeListBuilder.create(), PartPose.offset(-0.025F, -13.5F, 14.825F));

		PartDefinition dialframe = dimention_select.addOrReplaceChild("dialframe", CubeListBuilder.create().texOffs(202, 79).addBox(-0.625F, -1.15F, -1.025F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(14, 155).addBox(-1.15F, -1.725F, -0.95F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.85F, -1.725F, -0.925F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-0.45F, -1.725F, -0.925F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(12, 155).addBox(-1.65F, -0.525F, -0.925F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(12, 155).addBox(-1.75F, -1.925F, -0.925F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(12, 155).addBox(-1.675F, -0.325F, -0.925F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(12, 155).addBox(-1.675F, -0.125F, -0.925F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(13, 155).addBox(-1.175F, 0.075F, -0.925F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-0.5F, -2.125F, -0.925F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.7F, -2.125F, -0.925F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.125F, -2.325F, -0.925F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.325F, -2.325F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.375F, 0.075F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(0.075F, -2.325F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(0.025F, 0.075F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(0.55F, -0.525F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(0.45F, -1.925F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(-1.85F, -0.525F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(14, 155).addBox(0.525F, -0.325F, -0.925F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(201, 79).addBox(-0.95F, 0.55F, -1.025F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(201, 79).addBox(-0.35F, 0.55F, -1.025F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.05F, -7.275F, -21.075F));

		PartDefinition bone4 = dialframe.addOrReplaceChild("bone4", CubeListBuilder.create().texOffs(202, 79).addBox(-2.0899F, -8.9101F, -22.075F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(202, 79).addBox(0.8899F, -8.9101F, -22.075F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(202, 79).addBox(-1.1F, -6.9201F, -22.075F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(202, 79).addBox(-1.1F, -9.9F, -22.075F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-0.05F, 7.275F, 21.075F));

		PartDefinition dialframe2 = dimention_select.addOrReplaceChild("dialframe2", CubeListBuilder.create().texOffs(202, 79).addBox(-0.657F, -2.5451F, -1.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.301F))
		.texOffs(202, 79).addBox(-0.657F, 0.4348F, -1.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.301F))
		.texOffs(202, 79).addBox(1.3329F, -1.5551F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.301F))
		.texOffs(202, 79).addBox(-1.647F, -1.5551F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.301F)), PartPose.offsetAndRotation(0.05F, -7.275F, -21.075F, 0.0F, 0.0F, -0.7854F));

		PartDefinition needle_rotate_z = dimention_select.addOrReplaceChild("needle_rotate_z", CubeListBuilder.create().texOffs(142, 160).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(-0.075F, -7.925F, -21.6F));

		PartDefinition station_tilt_4 = side4.addOrReplaceChild("station_tilt_4", CubeListBuilder.create().texOffs(208, 184).addBox(-1.125F, -4.325F, -20.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(199, 175).addBox(1.4F, 0.25F, -20.375F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(198, 164).addBox(1.4F, 0.25F, -20.625F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(70, 156).addBox(-5.1F, 1.025F, -20.175F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(72, 156).addBox(-5.6F, 1.025F, -20.175F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition telepathics = station_tilt_4.addOrReplaceChild("telepathics", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_selector = telepathics.addOrReplaceChild("glow_selector", CubeListBuilder.create().texOffs(183, 205).addBox(1.9F, 0.75F, -20.825F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(187, 204).addBox(1.9F, 0.75F, -21.275F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(175, 207).addBox(1.9F, 3.0F, -20.75F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(177, 206).addBox(1.9F, 0.5F, -20.75F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(181, 210).addBox(1.65F, 0.75F, -20.75F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(179, 206).addBox(4.15F, 0.75F, -20.75F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dummy_button_d1 = station_tilt_4.addOrReplaceChild("dummy_button_d1", CubeListBuilder.create().texOffs(15, 162).addBox(-6.2325F, 1.085F, -8.4125F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(16, 162).addBox(-6.9325F, 1.085F, -8.4125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(146, 160).addBox(-6.9075F, 1.085F, -8.7125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-5.4075F, 1.085F, -8.8125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-6.1575F, 1.085F, -8.6125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(1.72F, 0.165F, -11.775F));

		PartDefinition dummy_button_d2 = station_tilt_4.addOrReplaceChild("dummy_button_d2", CubeListBuilder.create().texOffs(15, 162).addBox(-6.2325F, 1.085F, -8.4125F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(16, 162).addBox(-6.9325F, 1.085F, -8.4125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(146, 160).addBox(-6.9075F, 1.085F, -8.7125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-5.4075F, 1.085F, -8.6125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-6.1575F, 1.085F, -8.7125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(1.72F, 0.94F, -11.775F));

		PartDefinition dummy_button_d3 = station_tilt_4.addOrReplaceChild("dummy_button_d3", CubeListBuilder.create().texOffs(15, 162).addBox(-6.2325F, 1.085F, -8.4125F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(16, 162).addBox(-6.9325F, 1.085F, -8.4125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.15F))
		.texOffs(146, 160).addBox(-6.9075F, 1.085F, -8.7125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-5.4075F, 1.085F, -8.6125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(146, 160).addBox(-6.1575F, 1.085F, -8.7125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F)), PartPose.offset(1.72F, 1.715F, -11.775F));

		PartDefinition sonic_port = station_tilt_4.addOrReplaceChild("sonic_port", CubeListBuilder.create().texOffs(134, 161).addBox(-1.125F, -1.075F, -20.275F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(182, 152).addBox(0.075F, -1.075F, -20.35F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(182, 152).addBox(-1.325F, -1.075F, -20.35F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 155).addBox(-0.025F, -1.375F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 171).addBox(-0.025F, 0.225F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 171).addBox(-1.225F, 0.225F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 155).addBox(-1.225F, -1.375F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(207, 173).addBox(-0.925F, 0.425F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(207, 173).addBox(-0.625F, 0.425F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(218, 9).addBox(-0.05F, -0.575F, -20.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(218, 9).addBox(-2.225F, -0.575F, -20.475F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.225F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.425F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.625F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.825F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-0.05F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.15F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.35F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.55F, -0.9F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-0.05F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.15F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.35F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(0.55F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.225F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.425F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.625F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(219, 9).addBox(-1.825F, -0.25F, -20.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(207, 173).addBox(-0.325F, 0.425F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 171).addBox(-0.925F, 0.225F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 171).addBox(-0.625F, 0.225F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(202, 171).addBox(-0.325F, 0.225F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(201, 147).addBox(-0.325F, -1.55F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(201, 147).addBox(-0.925F, -1.55F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(201, 147).addBox(-0.625F, -1.55F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 155).addBox(-0.925F, -1.375F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 155).addBox(-0.625F, -1.375F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(200, 155).addBox(-0.325F, -1.375F, -20.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offset(0.125F, -0.55F, 0.0F));

		PartDefinition sonic_connection = sonic_port.addOrReplaceChild("sonic_connection", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.25F, 0.141F, -20.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition gauge_left = station_tilt_4.addOrReplaceChild("gauge_left", CubeListBuilder.create().texOffs(206, 182).addBox(0.7625F, -0.4F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(217, 24).addBox(0.7625F, -0.4F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(136, 156).addBox(0.7537F, -0.4088F, -0.7716F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(-2.7625F, -3.9214F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_cord_x2 = gauge_left.addOrReplaceChild("glow_cord_x2", CubeListBuilder.create().texOffs(185, 11).addBox(-1.5088F, -4.1838F, -20.1716F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition gauge_right = station_tilt_4.addOrReplaceChild("gauge_right", CubeListBuilder.create().texOffs(206, 182).addBox(0.7625F, -0.4F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(217, 24).addBox(0.7625F, -0.4F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(136, 156).addBox(0.7537F, -0.4088F, -0.7966F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(1.1266F, -3.9214F, -19.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glow_cord_x3 = gauge_right.addOrReplaceChild("glow_cord_x3", CubeListBuilder.create().texOffs(185, 11).addBox(-1.5088F, -4.1838F, -20.1716F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(2.7625F, 4.275F, 19.75F));

		PartDefinition pipes = station_tilt_4.addOrReplaceChild("pipes", CubeListBuilder.create().texOffs(184, 117).addBox(-0.5F, -0.025F, -20.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(184, 117).addBox(2.65F, -0.125F, -20.25F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(182, 117).addBox(-2.8F, 1.3F, -20.25F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(182, 117).addBox(0.35F, -0.45F, -20.25F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(239, 13).addBox(-0.5F, 1.3F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(239, 13).addBox(2.65F, -0.45F, -20.275F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition side5 = controls.addOrReplaceChild("side5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition monitor = side5.addOrReplaceChild("monitor", CubeListBuilder.create().texOffs(186, 84).addBox(-1.6F, -17.825F, -13.75F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(186, 84).addBox(-2.075F, -18.275F, -14.275F, 4.0F, 1.0F, 4.0F, new CubeDeformation(-0.45F))
		.texOffs(186, 84).addBox(-2.075F, -18.375F, -14.275F, 4.0F, 1.0F, 4.0F, new CubeDeformation(-0.45F)), PartPose.offset(0.025F, 0.0F, 0.0F));

		PartDefinition station_tilt_5 = side5.addOrReplaceChild("station_tilt_5", CubeListBuilder.create().texOffs(182, 152).addBox(-2.075F, 0.7F, -19.8875F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition pipes_5 = station_tilt_5.addOrReplaceChild("pipes_5", CubeListBuilder.create().texOffs(208, 92).addBox(-5.175F, 2.925F, -19.9125F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(206, 92).addBox(-4.875F, 4.925F, -19.9125F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(206, 92).addBox(-7.475F, 6.1F, -19.9125F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(206, 92).addBox(-0.225F, 4.075F, -19.9125F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(219, 3).addBox(-5.175F, 2.575F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(219, 3).addBox(-0.4F, 2.575F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(219, 3).addBox(-0.4F, 4.075F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(219, 3).addBox(2.1F, 4.075F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(208, 92).addBox(-0.4F, 2.925F, -19.9125F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(219, 3).addBox(-2.8F, -0.225F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(208, 92).addBox(-2.8F, -1.375F, -19.9125F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(208, 92).addBox(2.1F, 4.125F, -19.9125F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(219, 3).addBox(-5.175F, 6.075F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(219, 3).addBox(-5.175F, 4.925F, -19.9125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(2.25F, -4.0F, -0.4375F));

		PartDefinition glow_monitor = station_tilt_5.addOrReplaceChild("glow_monitor", CubeListBuilder.create().texOffs(195, 235).addBox(-1.575F, 0.3089F, -22.7503F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition monitor_hood = station_tilt_5.addOrReplaceChild("monitor_hood", CubeListBuilder.create().texOffs(191, 79).addBox(-1.575F, -0.4411F, 0.8497F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offset(0.0F, 0.625F, -23.375F));

		PartDefinition bone = monitor_hood.addOrReplaceChild("bone", CubeListBuilder.create().texOffs(195, 102).addBox(-1.6225F, 0.1534F, -0.6618F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(195, 102).addBox(-0.5225F, 0.1534F, -0.6618F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(196, 103).addBox(-0.5225F, 0.1534F, -0.7618F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(196, 103).addBox(-0.5225F, 0.1534F, 0.4382F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(196, 103).addBox(-1.6225F, 0.1534F, 0.4382F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(196, 103).addBox(-1.6225F, 0.1534F, -0.7618F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.6109F, 0.0F, 0.0F));

		PartDefinition bone3 = monitor_hood.addOrReplaceChild("bone3", CubeListBuilder.create().texOffs(199, 94).addBox(-1.7225F, 0.1534F, -0.4618F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(199, 94).addBox(0.5775F, 0.1534F, -0.4618F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(-1.7225F, 0.1534F, -0.5618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(-1.7225F, 0.1534F, -0.6618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(-1.7225F, 0.1534F, -0.7618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(0.5775F, 0.1534F, -0.5618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(0.5775F, 0.1534F, -0.6618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(200, 95).addBox(0.5775F, 0.1534F, -0.7618F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.45F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.6109F, 0.0F, 0.0F));

		PartDefinition dummy_plate = station_tilt_5.addOrReplaceChild("dummy_plate", CubeListBuilder.create().texOffs(30, 82).addBox(-3.0375F, -3.825F, -20.25F, 6.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dummy_bell_e1 = dummy_plate.addOrReplaceChild("dummy_bell_e1", CubeListBuilder.create().texOffs(229, 13).addBox(-1.8325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(229, 13).addBox(-2.4325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(195, 153).addBox(-2.1325F, 0.06F, -7.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(180, 41).addBox(-2.1325F, 0.06F, -8.35F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(178, 57).addBox(-2.1325F, 0.06F, -8.525F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.18F))
		.texOffs(182, 120).addBox(-2.1325F, 0.06F, -8.825F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offset(-0.405F, -3.385F, -12.2125F));

		PartDefinition dummy_bell_e2 = dummy_plate.addOrReplaceChild("dummy_bell_e2", CubeListBuilder.create().texOffs(229, 13).addBox(-2.4075F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(229, 13).addBox(-1.8075F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(195, 153).addBox(-2.1325F, 0.06F, -7.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(180, 41).addBox(-2.1325F, 0.06F, -8.35F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(178, 57).addBox(-2.1325F, 0.06F, -8.525F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.18F))
		.texOffs(182, 120).addBox(-2.1325F, 0.06F, -8.825F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.895F, -3.385F, -12.2125F));

		PartDefinition dummy_bell_e3 = dummy_plate.addOrReplaceChild("dummy_bell_e3", CubeListBuilder.create().texOffs(229, 13).addBox(-1.8325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(229, 13).addBox(-2.4325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(195, 153).addBox(-2.1325F, 0.06F, -7.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(180, 41).addBox(-2.1325F, 0.06F, -8.35F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(178, 57).addBox(-2.1325F, 0.06F, -8.525F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.18F))
		.texOffs(182, 120).addBox(-2.1325F, 0.06F, -8.825F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offset(2.245F, -3.385F, -12.2125F));

		PartDefinition dummy_bell_e4 = dummy_plate.addOrReplaceChild("dummy_bell_e4", CubeListBuilder.create().texOffs(229, 13).addBox(-1.8325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(229, 13).addBox(-2.4325F, 1.135F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(195, 153).addBox(-2.1325F, 0.06F, -7.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(180, 41).addBox(-2.1325F, 0.06F, -8.35F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(178, 57).addBox(-2.1325F, 0.06F, -8.525F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.18F))
		.texOffs(182, 120).addBox(-2.1325F, 0.06F, -8.825F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offset(3.595F, -3.385F, -12.2125F));

		PartDefinition fast_return = station_tilt_5.addOrReplaceChild("fast_return", CubeListBuilder.create().texOffs(181, 152).addBox(-5.5F, 1.6875F, -20.1875F, 3.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(29, 155).addBox(-4.975F, 2.2375F, -20.2375F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.65F, 0.0F, 0.0F));

		PartDefinition fast_return_button_rotate_x = fast_return.addOrReplaceChild("fast_return_button_rotate_x", CubeListBuilder.create().texOffs(95, 230).addBox(-4.9218F, -17.0641F, 0.4257F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.0532F, 19.3391F, -21.2757F, 0.0175F, 0.0F, 0.0F));

		PartDefinition stablizers = station_tilt_5.addOrReplaceChild("stablizers", CubeListBuilder.create().texOffs(181, 152).addBox(-5.5F, 1.6875F, -20.1875F, 3.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(29, 155).addBox(-4.975F, 2.2375F, -20.2375F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(8.7F, 0.0F, 0.0F));

		PartDefinition stablizers_button_rotate_x = stablizers.addOrReplaceChild("stablizers_button_rotate_x", CubeListBuilder.create().texOffs(19, 206).addBox(-4.9218F, -17.0641F, 0.4257F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-0.0532F, 19.3391F, -21.2757F, 0.0175F, 0.0F, 0.0F));

		PartDefinition side6 = controls.addOrReplaceChild("side6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition station_tilt_6 = side6.addOrReplaceChild("station_tilt_6", CubeListBuilder.create().texOffs(182, 152).addBox(-2.575F, -3.35F, -20.175F, 5.0F, 2.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(188, 181).addBox(-3.05F, -3.825F, -20.25F, 6.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0472F, 0.0F, 0.0F));

		PartDefinition door = station_tilt_6.addOrReplaceChild("door", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition door_plate = door.addOrReplaceChild("door_plate", CubeListBuilder.create().texOffs(132, 159).addBox(-1.25F, 0.6125F, -20.3125F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(81, 121).addBox(-0.775F, 0.225F, -20.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 160).addBox(-0.375F, 2.3625F, -20.1875F, 0.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 160).addBox(0.625F, 2.3625F, -20.1875F, 0.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 160).addBox(0.625F, 0.1125F, -20.1875F, 0.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 160).addBox(-0.375F, 0.1125F, -20.1875F, 0.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(82, 160).addBox(-0.35F, 1.85F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.175F, 1.45F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.075F, 2.05F, -20.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-1.475F, 2.05F, -20.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-1.425F, -0.35F, -20.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.05F, -0.35F, -20.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.175F, 1.05F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.55F, -0.55F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.95F, -0.55F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(140, 166).addBox(-0.75F, 1.85F, -20.175F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-1.325F, 1.05F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(81, 160).addBox(-1.25F, 0.65F, -20.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(81, 160).addBox(-1.25F, 0.25F, -20.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(81, 160).addBox(-1.25F, -0.15F, -20.25F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-1.325F, 1.45F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-1.15F, 1.85F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.975F, 2.25F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 160).addBox(-0.575F, 2.25F, -20.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition door_knob_rotate_z = door.addOrReplaceChild("door_knob_rotate_z", CubeListBuilder.create().texOffs(202, 93).addBox(-0.775F, -0.55F, 0.1167F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(23, 167).addBox(-0.785F, -0.54F, -0.6983F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.26F))
		.texOffs(20, 161).addBox(-0.775F, -0.525F, -0.4333F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(0.0F, 0.75F, -20.9167F));

		PartDefinition dummy_button_f1 = station_tilt_6.addOrReplaceChild("dummy_button_f1", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(141, 154).addBox(-5.545F, -0.165F, -8.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(137, 159).addBox(-4.62F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(1.845F, 0.115F, -11.775F));

		PartDefinition glow_f1 = dummy_button_f1.addOrReplaceChild("glow_f1", CubeListBuilder.create().texOffs(96, 236).addBox(-9.125F, 0.45F, -20.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition dummy_button_f2 = station_tilt_6.addOrReplaceChild("dummy_button_f2", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(146, 166).addBox(-6.47F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(137, 159).addBox(-4.62F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(1.845F, 1.44F, -11.775F));

		PartDefinition glow_f2 = dummy_button_f2.addOrReplaceChild("glow_f2", CubeListBuilder.create().texOffs(190, 19).addBox(-8.2F, 0.45F, -20.1625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition dummy_button_f3 = station_tilt_6.addOrReplaceChild("dummy_button_f3", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(141, 154).addBox(-5.545F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(139, 161).addBox(-6.47F, -0.165F, -8.3125F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(137, 159).addBox(-4.62F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(1.845F, 2.715F, -11.775F));

		PartDefinition glow_f3 = dummy_button_f3.addOrReplaceChild("glow_f3", CubeListBuilder.create(), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition dummy_button_f4 = station_tilt_6.addOrReplaceChild("dummy_button_f4", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(141, 154).addBox(-5.545F, -0.165F, -8.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(139, 161).addBox(-6.47F, -0.165F, -8.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(8.095F, 0.115F, -11.775F));

		PartDefinition glow_f4 = dummy_button_f4.addOrReplaceChild("glow_f4", CubeListBuilder.create().texOffs(190, 15).addBox(-7.275F, 0.45F, -20.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition dummy_button_f5 = station_tilt_6.addOrReplaceChild("dummy_button_f5", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(141, 154).addBox(-5.545F, -0.165F, -8.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(139, 161).addBox(-6.47F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(137, 159).addBox(-4.62F, -0.165F, -8.3625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(8.095F, 1.415F, -11.775F));

		PartDefinition glow_f5 = dummy_button_f5.addOrReplaceChild("glow_f5", CubeListBuilder.create(), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition dummy_button_f6 = station_tilt_6.addOrReplaceChild("dummy_button_f6", CubeListBuilder.create().texOffs(183, 152).addBox(-6.92F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.72F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.52F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(184, 152).addBox(-5.32F, -0.665F, -8.45F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(183, 152).addBox(-5.12F, -0.665F, -8.45F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(139, 161).addBox(-6.47F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(137, 159).addBox(-4.62F, -0.165F, -8.5875F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(8.095F, 2.69F, -11.775F));

		PartDefinition glow_f6 = dummy_button_f6.addOrReplaceChild("glow_f6", CubeListBuilder.create().texOffs(189, 16).addBox(-8.2F, 0.45F, -20.1625F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offset(2.655F, -0.615F, 11.775F));

		PartDefinition coms = side6.addOrReplaceChild("coms", CubeListBuilder.create().texOffs(182, 152).addBox(-2.175F, -23.85F, -8.45F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(82, 140).addBox(-2.7F, -24.35F, -9.0F, 1.0F, 8.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 140).addBox(1.425F, -24.35F, -9.0F, 1.0F, 8.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition bell_rotate_x = coms.addOrReplaceChild("bell_rotate_x", CubeListBuilder.create().texOffs(197, 89).addBox(-1.5F, 3.75F, -1.475F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.1F))
		.texOffs(122, 155).addBox(-1.525F, 4.025F, -1.525F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(193, 99).addBox(-1.0F, 1.4F, -0.975F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(207, 100).addBox(-0.45F, 0.65F, -0.425F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offset(-0.275F, -24.0F, -8.0F));

		PartDefinition time_rotor_slide_y = partdefinition.addOrReplaceChild("time_rotor_slide_y", CubeListBuilder.create(), PartPose.offset(-0.1F, 1.5F, 0.0F));

		PartDefinition glow_timerotor_slide_y = time_rotor_slide_y.addOrReplaceChild("glow_timerotor_slide_y", CubeListBuilder.create().texOffs(172, 10).addBox(-1.5625F, -25.65F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(172, 10).addBox(-1.5625F, -28.25F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, 22.25F, 0.0F));

		PartDefinition rotor_plate = time_rotor_slide_y.addOrReplaceChild("rotor_plate", CubeListBuilder.create().texOffs(24, 109).addBox(-3.8125F, -22.9F, -2.0F, 8.0F, 1.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(79, 124).addBox(-1.5625F, -24.9F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(76, 123).addBox(-1.5625F, -27.4F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(26, 112).addBox(-3.2125F, -22.9F, -3.8F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(15, 121).addBox(-3.2125F, -22.9F, 1.8F, 7.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F)), PartPose.offset(0.0F, 22.25F, 0.0F));

		PartDefinition rotor_post_1 = time_rotor_slide_y.addOrReplaceChild("rotor_post_1", CubeListBuilder.create().texOffs(198, 57).addBox(-0.5F, -5.0F, 2.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(198, 57).addBox(-0.5F, -5.0F, -3.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.4F, 0.0F));

		PartDefinition rotor_post_2 = time_rotor_slide_y.addOrReplaceChild("rotor_post_2", CubeListBuilder.create().texOffs(198, 57).addBox(-0.5F, -5.0F, 2.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(198, 57).addBox(-0.5F, -5.0F, -3.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -5.4F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition rotor_post_3 = time_rotor_slide_y.addOrReplaceChild("rotor_post_3", CubeListBuilder.create().texOffs(198, 57).addBox(-0.5F, -5.0F, 2.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(198, 57).addBox(-0.5F, -5.0F, -3.25F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -5.4F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glow_baseoffset_1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		glow_baseoffset_2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		glow_baseoffset_3.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		console.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		controls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		time_rotor_slide_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.rotorAnimationState, NemoConsoleAnimations.TIME_ROTOR, ageInTicks);
		Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level).ifPresent(tardis -> {
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.COMMUNICATOR.get()).getUseAnimationState(), NemoConsoleAnimations.Bell, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.RANDOMIZER.get()).getUseAnimationState(), NemoConsoleAnimations.RANDOMIZER, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.INCREMENT.get()).getUseAnimationState(), NemoConsoleAnimations.INCREMENT, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.DOOR.get()).getUseAnimationState(), NemoConsoleAnimations.DOOR, ageInTicks);
			NemoConsoleAnimations.animateOthers(tardis, this, ageInTicks);
		});
	}

	@Override
	public Optional<String> getPartForControl(ControlType<?> type) {
		if(type == ControlRegistry.SONIC_PORT.get())
			return Optional.of("controls/side4/station_tilt_4/sonic_port/sonic_connection");
		return Optional.empty();
	}


}


