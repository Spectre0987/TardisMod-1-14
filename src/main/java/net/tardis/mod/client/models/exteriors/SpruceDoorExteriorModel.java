package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class SpruceDoorExteriorModel<T extends ExteriorTile> extends BasicTileHierarchicalModel<T> implements IExteriorModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/spruce"), "main");
	private final ModelPart frame;
	private final ModelPart door_rotate_y;
	private final ModelPart boti;

	public SpruceDoorExteriorModel(ModelPart root) {
		super(root, RenderType::entityTranslucent);
		this.frame = root.getChild("frame");
		this.door_rotate_y = root.getChild("door_rotate_y");
		this.boti = root.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition frame = partdefinition.addOrReplaceChild("frame", CubeListBuilder.create().texOffs(38, 21).addBox(8.01F, -16.635F, -0.99F, 1.0F, 16.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(38, 21).addBox(8.01F, -32.635F, -0.99F, 1.0F, 16.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(38, 21).addBox(-8.99F, -16.635F, -0.99F, 1.0F, 16.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(38, 21).addBox(-8.99F, -32.635F, -0.99F, 1.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.5F, -7.0F));

		PartDefinition cube_r1 = frame.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(38, 0).addBox(32.425F, 8.0F, -1.0F, 1.0F, 18.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.0F, -0.125F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition cube_r2 = frame.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(38, 0).addBox(-0.5F, -9.0F, -1.0F, 1.0F, 18.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.0F, -0.125F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition door_rotate_y = partdefinition.addOrReplaceChild("door_rotate_y", CubeListBuilder.create().texOffs(0, 19).addBox(0.0F, 0.0F, -1.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(0.0F, -16.0F, -1.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 8.0F, -7.0F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 38).addBox(-8.0F, 0.0F, 0.0F, 16.0F, 16.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(0, 38).addBox(-8.0F, -16.0F, 0.0F, 16.0F, 16.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 8.0F, -7.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		frame.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen())
			this.door_rotate_y.offsetRotation(AnimationHelper.degrees(90, AnimationHelper.Axis.Y));
	}

	@Override
	public void animateDemat(T exterior, float age) {

	}

	@Override
	public void animateRemat(T exterior, float age) {

	}

	@Override
	public void animateSolid(T exterior, float age) {

	}
}