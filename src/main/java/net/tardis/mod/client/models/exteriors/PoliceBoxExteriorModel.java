package net.tardis.mod.client.models.exteriors;// Made with Blockbench 4.10.3
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.animations.exterior.PoliceBoxExteriorAnimation;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class PoliceBoxExteriorModel<T extends ExteriorTile> extends BasicTileHierarchicalModel<T> implements IExteriorModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/police_box"), "main");

	private final ModelPart base;
	private final ModelPart pillars;
	private final ModelPart sides;
	private final ModelPart door1;
	private final ModelPart door2;
	private final ModelPart signs;
	private final ModelPart roof;
	private final ModelPart lamp;

	public PoliceBoxExteriorModel(ModelPart root) {
		super(root, RenderType::entityTranslucent);
		this.base = root.getChild("base");
		this.pillars = root.getChild("pillars");
		this.sides = root.getChild("sides");
		this.door1 = root.getChild("door1");
		this.door2 = root.getChild("door2");
		this.signs = root.getChild("signs");
		this.roof = root.getChild("roof");
		this.lamp = root.getChild("lamp");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(0, 0).addBox(-13.0F, -18.0F, -13.0F, 26.0F, 2.0F, 26.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 40.0F, 0.0F));

		PartDefinition pillars = partdefinition.addOrReplaceChild("pillars", CubeListBuilder.create().texOffs(50, 112).addBox(16.0F, -35.0F, -20.0F, 4.0F, 41.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(34, 112).addBox(-4.0F, -35.0F, -20.0F, 4.0F, 41.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(98, 76).addBox(-4.0F, -35.0F, 0.0F, 4.0F, 41.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(76, 107).addBox(16.0F, -35.0F, 0.0F, 4.0F, 41.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition sides = partdefinition.addOrReplaceChild("sides", CubeListBuilder.create().texOffs(0, 75).addBox(-2.0F, -48.0F, -16.0F, 1.0F, 32.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(98, 43).addBox(0.0F, -48.0F, 1.0F, 16.0F, 32.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(64, 59).addBox(17.0F, -48.0F, -16.0F, 1.0F, 32.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 38.0F, 8.0F));

		PartDefinition door1 = partdefinition.addOrReplaceChild("door1", CubeListBuilder.create().texOffs(114, 109).addBox(0.0F, -16.0F, -0.5F, 8.0F, 32.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 6.0F, -9.5F));

		PartDefinition door2 = partdefinition.addOrReplaceChild("door2", CubeListBuilder.create().texOffs(114, 76).addBox(-8.0F, -16.0F, -0.5F, 8.0F, 32.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 6.0F, -9.5F));

		PartDefinition signs = partdefinition.addOrReplaceChild("signs", CubeListBuilder.create().texOffs(102, 0).addBox(-1.0F, -32.0F, -21.0F, 18.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(18, 75).addBox(-1.0F, -32.0F, 2.0F, 18.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(78, 0).addBox(-5.0F, -32.0F, -17.0F, 3.0F, 5.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition cube_r1 = signs.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(78, 0).addBox(-1.5F, -2.5F, -9.0F, 3.0F, 5.0F, 18.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(19.5F, -29.5F, -8.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition roof = partdefinition.addOrReplaceChild("roof", CubeListBuilder.create().texOffs(0, 28).addBox(-3.0F, -27.0F, -19.0F, 22.0F, 1.0F, 22.0F, new CubeDeformation(0.0F))
		.texOffs(0, 51).addBox(-2.0F, -36.0F, -18.0F, 20.0F, 4.0F, 20.0F, new CubeDeformation(0.0F))
		.texOffs(66, 28).addBox(1.0F, -37.0F, -15.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition lamp = partdefinition.addOrReplaceChild("lamp", CubeListBuilder.create().texOffs(0, 0).addBox(7.0F, -40.0F, -9.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition cube_r2 = lamp.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(0, 7).addBox(-2.75F, -3.0F, 0.5F, 4.5F, 2.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.0F, -36.0F, -8.75F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r3 = lamp.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(0, 2).addBox(0.5F, -3.0F, -1.75F, 0.0F, 2.0F, 4.5F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.25F, -36.0F, -8.0F, 0.0F, 0.7854F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		base.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		pillars.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		sides.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		signs.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		roof.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		lamp.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		root().getAllParts().forEach(ModelPart::resetPose);
		PoliceBoxExteriorAnimation.animateDoors(tile.getDoorHandler().getDoorState(), this.door1, this.door2);
	}

	@Override
	public void animateDemat(T exterior, float age) {

	}

	@Override
	public void animateRemat(T exterior, float age) {

	}

	@Override
	public void animateSolid(T exterior, float age) {

	}
}