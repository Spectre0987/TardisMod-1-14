package net.tardis.mod.client.models.exteriors;

import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.models.IAnimatableTileModel;

public interface IExteriorModel<T extends ExteriorTile> extends IAnimatableTileModel<T> {

    void animateDemat(T exterior, float age);
    void animateRemat(T exterior, float age);
    void animateSolid(T exterior, float age);

}
