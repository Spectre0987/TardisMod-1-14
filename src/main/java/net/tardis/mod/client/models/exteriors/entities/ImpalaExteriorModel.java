package net.tardis.mod.client.models.exteriors.entities;// Made with Blockbench 4.11.1
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.client.animations.exterior.ImpalaExteriorAnimation;
import net.tardis.mod.entity.CarExteriorEntity;
import net.tardis.mod.helpers.Helper;

public class ImpalaExteriorModel<T extends CarExteriorEntity> extends HierarchicalModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/impala"), "main");
	private final ModelPart base;
	private final ModelPart rightDoor;
	private final ModelPart leftDoor;
	private final ModelPart trunk;
	private final ModelPart wheels;
	private final ModelPart frontWheels;
	private final ModelPart leftWheel;
	private final ModelPart rightWheel;
	private final ModelPart backWheels;
	private final ModelPart leftBackWheel;
	private final ModelPart rightBackWheel;
	private final ModelPart interior;
	private final ModelPart chairs;
	private final ModelPart wheel;
	private final ModelPart shifter;

	public final ModelPart root;

	public ImpalaExteriorModel(ModelPart root) {
		super(RenderType::entityTranslucent);
		this.root = root;
		this.base = root.getChild("base");
		this.rightDoor = this.base.getChild("rightDoor");
		this.leftDoor = this.base.getChild("leftDoor");
		this.trunk = this.base.getChild("trunk");
		this.wheels = this.base.getChild("wheels");
		this.frontWheels = this.wheels.getChild("frontWheels");
		this.leftWheel = this.frontWheels.getChild("leftWheel");
		this.rightWheel = this.frontWheels.getChild("rightWheel");
		this.backWheels = this.wheels.getChild("backWheels");
		this.leftBackWheel = this.backWheels.getChild("leftBackWheel");
		this.rightBackWheel = this.backWheels.getChild("rightBackWheel");
		this.interior = root.getChild("interior");
		this.chairs = this.interior.getChild("chairs");
		this.wheel = this.interior.getChild("wheel");
		this.shifter = this.interior.getChild("shifter");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(280, 169).addBox(10.65F, -10.0F, -22.9F, 9.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(290, 232).addBox(11.15F, -13.6F, -22.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(150, 291).addBox(15.15F, -13.6F, -22.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(166, 291).addBox(-13.85F, -13.6F, -22.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(158, 291).addBox(-17.85F, -13.6F, -22.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(286, 142).addBox(-19.35F, -10.0F, -22.9F, 9.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(224, 148).addBox(-18.35F, -14.0F, -22.1F, 37.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(280, 64).addBox(-8.85F, -11.1564F, -23.8273F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(68, 294).addBox(8.15F, -11.1564F, -23.8273F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(288, 111).addBox(10.65F, -15.0F, -22.9F, 9.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(286, 145).addBox(-19.35F, -15.0F, -22.9F, 9.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(90, 277).addBox(10.65F, -16.5F, -22.3F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(112, 277).addBox(-19.35F, -16.5F, -22.3F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 75).addBox(-19.35F, -17.2071F, -21.5929F, 39.0F, 1.0F, 30.0F, new CubeDeformation(0.0F))
		.texOffs(0, 106).addBox(-10.35F, -17.6071F, -19.5929F, 21.0F, 1.0F, 28.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-14.9F, -5.9F, -21.0F, 30.0F, 1.0F, 74.0F, new CubeDeformation(0.0F))
		.texOffs(98, 106).addBox(15.1F, -5.9F, -1.9F, 1.0F, 1.0F, 41.0F, new CubeDeformation(0.0F))
		.texOffs(0, 135).addBox(-15.9F, -5.9F, -1.9F, 1.0F, 1.0F, 41.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition cube_r1 = base.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(188, 288).addBox(3.5F, -0.5F, -3.3F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.01F))
		.texOffs(292, 16).addBox(3.5F, -0.5F, 4.4195F, 2.0F, 1.0F, 2.2805F, new CubeDeformation(0.01F))
		.texOffs(294, 196).addBox(3.5F, -0.5F, 3.7097F, 2.0F, 1.0F, 0.6897F, new CubeDeformation(0.01F))
		.texOffs(138, 229).addBox(0.5F, -0.5F, 17.6195F, 5.0F, 1.0F, 0.8805F, new CubeDeformation(0.01F))
		.texOffs(202, 268).addBox(4.5397F, -0.5F, 6.5995F, 0.9603F, 1.0F, 11.0F, new CubeDeformation(0.01F))
		.texOffs(178, 258).addBox(4.5F, 0.1F, -3.3F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.3197F, -15.7713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r2 = base.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(80, 194).addBox(0.17F, -9.5F, 0.8105F, 2.33F, 1.011F, 0.1895F, new CubeDeformation(0.01F))
		.texOffs(161, 275).addBox(-2.5F, -8.6F, 0.8105F, 6.0F, 15.0F, 0.1895F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -12.379F, 57.8005F, -1.5708F, 0.1745F, -1.5708F));

		PartDefinition cube_r3 = base.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(180, 99).addBox(-2.5F, -9.6F, 0.8105F, 1.25F, 0.891F, 0.1895F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-19.15F, -12.399F, 57.8005F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r4 = base.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(150, 275).addBox(-3.5F, -8.689F, 0.8105F, 6.0F, 15.089F, 0.1895F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-19.1684F, -12.399F, 57.8005F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r5 = base.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(226, 273).addBox(0.0F, -2.0F, -3.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5081F, -15.6071F, -18.529F, 0.0F, -0.1745F, 0.0F));

		PartDefinition cube_r6 = base.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(208, 0).addBox(-0.5F, -0.5F, -7.5F, 1.0F, 1.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(10.65F, -16.9F, -12.0929F, 0.0F, 0.0F, -0.7854F));

		PartDefinition cube_r7 = base.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(70, 200).addBox(-0.5F, -0.5F, -7.5F, 1.0F, 1.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.35F, -16.9F, -12.0929F, 0.0F, 0.0F, -0.7854F));

		PartDefinition cube_r8 = base.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(178, 266).addBox(-4.5F, -1.0F, -1.0F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(128, 227).addBox(25.5F, -1.0F, -1.0F, 9.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-14.85F, -15.7929F, -21.5929F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r9 = base.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(294, 198).addBox(8.0F, -1.5782F, -0.1363F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(294, 200).addBox(-16.0F, -1.5782F, -0.1363F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.65F, -10.1782F, 66.8637F, -0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r10 = base.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(292, 253).addBox(8.0F, -0.8647F, -0.4234F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(292, 258).addBox(-16.0F, -0.8647F, -0.4234F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.65F, -10.1782F, 66.8637F, -0.48F, 0.0F, 0.0F));

		PartDefinition cube_r11 = base.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(292, 248).addBox(-9.5F, -2.0F, -0.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(220, 292).addBox(-26.5F, -2.0F, -0.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(17.65F, -7.5F, -22.6F, 0.3927F, 0.0F, 0.0F));

		PartDefinition cube_r12 = base.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(226, 268).addBox(-2.5F, -2.0F, -0.5F, 5.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.15F, -7.5F, -21.7F, 0.1309F, 0.0F, 0.0F));

		PartDefinition cube_r13 = base.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(182, 137).addBox(-20.5F, -2.0F, -0.5F, 40.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.65F, -7.5F, -21.5F, 0.3927F, 0.0F, 0.0F));

		PartDefinition cube_r14 = base.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(18, 281).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(19.15F, -12.0F, -22.3F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r15 = base.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(292, 242).addBox(-1.0F, -3.0F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.01F))
		.texOffs(292, 25).addBox(-1.0F, 2.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.14F, -12.0F, -21.8816F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r16 = base.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(214, 292).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.85F, -16.03F, -21.8816F, -1.5708F, -0.7854F, 1.5708F));

		PartDefinition cube_r17 = base.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(292, 4).addBox(-1.0F, -4.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0101F)), PartPose.offsetAndRotation(-18.85F, -12.02F, -22.3F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r18 = base.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(176, 75).addBox(-1.0F, -29.0F, 0.0F, 1.0F, 30.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.84F, -16.03F, -20.5716F, -1.5708F, 0.0F, 2.3562F));

		PartDefinition cube_r19 = base.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(150, 200).addBox(-1.0F, -21.0F, -1.0F, 1.0F, 30.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.14F, -16.01F, -12.5716F, -1.5708F, 0.0F, 0.7854F));

		PartDefinition cube_r20 = base.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(72, 277).addBox(-1.0F, -5.0F, -1.0F, 7.0F, 6.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.86F, -11.4909F, -20.1866F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r21 = base.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(264, 53).addBox(-4.7774F, -2.4543F, -1.0F, 13.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.3706F, -21.3405F, 14.514F, 1.5708F, 0.7411F, 1.8762F));

		PartDefinition cube_r22 = base.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(224, 188).addBox(-8.2226F, -2.4543F, -1.0F, 13.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(17.6706F, -21.3405F, 14.514F, 1.5708F, -0.7411F, -1.8762F));

		PartDefinition cube_r23 = base.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(290, 196).addBox(0.2F, -4.5F, -0.5F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(16.6384F, -20.209F, 45.1447F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r24 = base.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(264, 236).addBox(-0.1F, 0.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(16.6384F, -20.209F, 45.1447F, -1.5708F, 0.6545F, 1.5708F));

		PartDefinition cube_r25 = base.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(80, 187).addBox(-0.1F, 0.5F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-16.3616F, -20.209F, 45.1447F, -1.5708F, 0.6545F, 1.5708F));

		PartDefinition cube_r26 = base.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(0, 2).addBox(0.7F, -4.3F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -20.309F, 45.1447F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r27 = base.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -3.0F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -22.1135F, 41.3301F, -1.5708F, 0.6545F, 1.5708F));

		PartDefinition cube_r28 = base.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(0, 0).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 7.6693F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -23.6207F, 38.0532F, -1.5708F, 0.2618F, 1.5708F));

		PartDefinition cube_r29 = base.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -25.1303F, 31.3589F, -1.5708F, 0.1309F, 1.5708F));

		PartDefinition cube_r30 = base.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -25.5891F, 27.1698F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r31 = base.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(0, 2).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 10.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(17.3384F, -25.7726F, 22.9674F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r32 = base.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(0, 2).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 10.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -25.7726F, 22.9674F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r33 = base.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -25.5891F, 27.1698F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r34 = base.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -25.1303F, 31.3589F, -1.5708F, 0.1309F, 1.5708F));

		PartDefinition cube_r35 = base.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(0, 0).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 7.6693F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -23.6207F, 38.0532F, -1.5708F, 0.2618F, 1.5708F));

		PartDefinition cube_r36 = base.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(0, 3).addBox(-0.5F, -3.0F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -22.1135F, 41.3301F, -1.5708F, 0.6545F, 1.5708F));

		PartDefinition cube_r37 = base.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(0, 2).addBox(0.7F, -4.3F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-17.0616F, -20.309F, 45.1447F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r38 = base.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(248, 289).addBox(0.2F, -4.5F, -0.5F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-16.3616F, -20.209F, 45.1447F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r39 = base.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(204, 288).addBox(0.0F, -8.0F, -35.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-50.8616F, -22.8418F, 42.785F, -1.5708F, 0.6981F, 1.5708F));

		PartDefinition cube_r40 = base.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(262, 288).addBox(0.0F, -8.0F, -35.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-50.8616F, -25.3623F, 34.662F, -1.5708F, 0.2618F, 1.5708F));

		PartDefinition cube_r41 = base.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(244, 289).addBox(-0.7F, -3.0F, -0.5F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-16.3616F, -24.7499F, 35.0156F, -1.5708F, 0.1309F, 1.5708F));

		PartDefinition cube_r42 = base.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(180, 85).addBox(-0.3916F, 1.0F, -0.5F, 0.4416F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-16.3616F, -24.7499F, 35.0156F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r43 = base.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(54, 287).addBox(0.0F, -8.0F, -35.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F))
		.texOffs(0, 177).addBox(-1.0F, -8.0F, -35.0F, 1.0F, 9.0F, 34.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.8616F, -22.8418F, 42.785F, -1.5708F, 0.6981F, 1.5708F));

		PartDefinition cube_r44 = base.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(180, 75).addBox(-0.3916F, 1.0F, -0.5F, 0.4416F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(16.6384F, -24.7499F, 35.0156F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r45 = base.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(58, 287).addBox(-0.7F, -3.0F, -0.5F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(16.6384F, -24.7499F, 35.0156F, -1.5708F, 0.1309F, 1.5708F));

		PartDefinition cube_r46 = base.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(80, 177).addBox(0.0F, -8.0F, -35.0F, 1.0F, 9.0F, 1.0F, new CubeDeformation(0.01F))
		.texOffs(154, 148).addBox(-1.0F, -8.0F, -35.0F, 1.0F, 9.0F, 34.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.8616F, -25.3623F, 34.662F, -1.5708F, 0.2618F, 1.5708F));

		PartDefinition cube_r47 = base.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(84, 148).addBox(-1.0F, -17.0F, -35.0F, 1.0F, 18.0F, 34.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.8616F, -25.5893F, 16.9378F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r48 = base.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(122, 289).addBox(-4.0F, -5.0F, -1.0F, 5.0F, 5.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -16.4284F, -20.8516F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r49 = base.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(176, 231).addBox(-3.0F, -30.0F, -1.0F, 9.0F, 25.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -11.4284F, -20.8516F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r50 = base.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(108, 289).addBox(-1.0F, -5.0F, -1.0F, 5.0F, 5.0F, 2.0F, new CubeDeformation(0.01F))
		.texOffs(276, 104).addBox(-1.0F, 0.0F, -1.0F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.85F, -16.4484F, -20.8716F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r51 = base.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(220, 245).addBox(-6.0F, -62.0F, -1.0F, 9.0F, 21.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-19.1684F, -11.4284F, -9.8716F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r52 = base.addOrReplaceChild("cube_r52", CubeListBuilder.create().texOffs(138, 75).addBox(0.3F, -75.0F, -1.0F, 1.0F, 13.0F, 18.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-0.832F, 5.0628F, -8.6772F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r53 = base.addOrReplaceChild("cube_r53", CubeListBuilder.create().texOffs(56, 229).addBox(0.3F, -75.0F, -1.0F, 1.0F, 13.0F, 18.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(17.132F, 5.0918F, -8.7281F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r54 = base.addOrReplaceChild("cube_r54", CubeListBuilder.create().texOffs(236, 75).addBox(-4.048F, 0.2529F, 21.5523F, 4.048F, 0.6471F, 14.4477F, new CubeDeformation(0.01F))
		.texOffs(286, 134).addBox(-1.748F, 0.2529F, 14.5761F, 1.748F, 0.6471F, 6.9561F, new CubeDeformation(0.01F))
		.texOffs(138, 231).addBox(-4.048F, 0.2529F, 0.0F, 4.048F, 0.6471F, 14.5561F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.132F, -6.6571F, 65.3661F, -1.5708F, 0.5236F, 1.5708F));

		PartDefinition cube_r55 = base.addOrReplaceChild("cube_r55", CubeListBuilder.create().texOffs(290, 50).addBox(-0.5F, -0.5F, -1.5F, 4.65F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.6684F, -16.0622F, 66.7198F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r56 = base.addOrReplaceChild("cube_r56", CubeListBuilder.create().texOffs(282, 292).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.6684F, -19.4501F, 65.1567F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r57 = base.addOrReplaceChild("cube_r57", CubeListBuilder.create().texOffs(288, 292).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.6684F, -19.8379F, 64.1119F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r58 = base.addOrReplaceChild("cube_r58", CubeListBuilder.create().texOffs(294, 52).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.6684F, -19.4266F, 62.7543F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r59 = base.addOrReplaceChild("cube_r59", CubeListBuilder.create().texOffs(294, 54).addBox(-6.7F, -78.0F, -3.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-22.1684F, -12.0193F, -12.8116F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r60 = base.addOrReplaceChild("cube_r60", CubeListBuilder.create().texOffs(44, 287).addBox(4.7F, -74.0F, 0.0F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.1684F, -12.0193F, -9.8316F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r61 = base.addOrReplaceChild("cube_r61", CubeListBuilder.create().texOffs(234, 289).addBox(-0.5F, 0.5F, -2.0F, 2.0F, 4.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-19.1684F, -18.1035F, 53.4697F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r62 = base.addOrReplaceChild("cube_r62", CubeListBuilder.create().texOffs(88, 289).addBox(-0.5F, -4.5F, -3.0F, 2.0F, 9.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.1684F, -18.4947F, 44.5113F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r63 = base.addOrReplaceChild("cube_r63", CubeListBuilder.create().texOffs(98, 289).addBox(-0.5F, -4.5F, -3.0F, 2.0F, 9.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-20.1684F, -17.7193F, 35.6484F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r64 = base.addOrReplaceChild("cube_r64", CubeListBuilder.create().texOffs(292, 267).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -19.4357F, 62.7342F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r65 = base.addOrReplaceChild("cube_r65", CubeListBuilder.create().texOffs(292, 269).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -19.847F, 64.0919F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r66 = base.addOrReplaceChild("cube_r66", CubeListBuilder.create().texOffs(292, 271).addBox(-0.5F, -3.5F, -1.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -19.4592F, 65.1366F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r67 = base.addOrReplaceChild("cube_r67", CubeListBuilder.create().texOffs(70, 195).addBox(-0.5F, -0.5F, 37.2F, 4.8997F, 2.0F, 1.9497F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.9814F, -15.7713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r68 = base.addOrReplaceChild("cube_r68", CubeListBuilder.create().texOffs(284, 71).addBox(-0.5F, -0.5F, -1.5F, 4.65F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -16.0713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r69 = base.addOrReplaceChild("cube_r69", CubeListBuilder.create().texOffs(282, 289).addBox(-0.5F, -0.5F, -1.5F, 4.65F, 1.0F, 1.9497F, new CubeDeformation(0.01F))
		.texOffs(290, 38).addBox(3.5F, -0.5F, 12.2795F, 2.0F, 1.0F, 3.0897F, new CubeDeformation(0.01F))
		.texOffs(178, 268).addBox(4.47F, -0.5F, 1.2795F, 1.03F, 1.0F, 10.98F, new CubeDeformation(0.01F))
		.texOffs(56, 227).addBox(-0.5F, -0.5F, 0.4697F, 6.0F, 1.0F, 0.7897F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -15.7713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r70 = base.addOrReplaceChild("cube_r70", CubeListBuilder.create().texOffs(290, 277).addBox(-3.9F, -1.0F, 4.85F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.29F))
		.texOffs(292, 156).addBox(-3.9F, -1.0F, 7.95F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.29F))
		.texOffs(126, 287).addBox(-3.9F, -1.0F, 31.95F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.29F))
		.texOffs(40, 284).addBox(-3.9F, -1.0F, 28.85F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.29F))
		.texOffs(22, 270).addBox(-4.5F, -0.9F, 25.4F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.01F))
		.texOffs(270, 236).addBox(-1.5F, -0.9F, 26.4F, 1.0F, 1.0F, 10.0F, new CubeDeformation(0.01F))
		.texOffs(270, 247).addBox(-4.5F, -0.9F, 26.4F, 1.0F, 1.0F, 10.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.75F, -15.7713F, 65.3997F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r71 = base.addOrReplaceChild("cube_r71", CubeListBuilder.create().texOffs(290, 172).addBox(1.5F, -0.6F, 8.4F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(290, 188).addBox(1.5F, -0.6F, 5.3F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(290, 192).addBox(1.5F, -0.6F, 2.2F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(62, 290).addBox(1.5F, -0.6F, 32.4F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(290, 46).addBox(1.5F, -0.6F, 29.3F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(290, 42).addBox(1.5F, -0.6F, 26.2F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.01F))
		.texOffs(0, 270).addBox(3.5F, -0.9F, 26.3F, 1.0F, 1.0F, 10.0F, new CubeDeformation(0.01F))
		.texOffs(264, 137).addBox(0.5F, -0.9F, 26.3F, 1.0F, 1.0F, 10.0F, new CubeDeformation(0.01F))
		.texOffs(290, 230).addBox(0.5F, -0.9F, 1.3F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.01F))
		.texOffs(280, 172).addBox(0.5F, -0.9F, 35.3F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.01F))
		.texOffs(254, 188).addBox(0.5F, -0.9F, 25.3F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.95F, -15.7713F, 65.3997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r72 = base.addOrReplaceChild("cube_r72", CubeListBuilder.create().texOffs(242, 245).addBox(2.5F, -0.9F, 12.5F, 1.0F, 1.0F, 13.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -15.7713F, 65.3997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r73 = base.addOrReplaceChild("cube_r73", CubeListBuilder.create().texOffs(124, 265).addBox(-0.5F, -0.5F, 1.2795F, 0.95F, 1.0F, 5.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.35F, -15.7713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r74 = base.addOrReplaceChild("cube_r74", CubeListBuilder.create().texOffs(56, 220).addBox(-0.5F, -0.5F, 12.5995F, 1.0197F, 1.0F, 6.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-0.3803F, -15.7713F, 66.6997F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r75 = base.addOrReplaceChild("cube_r75", CubeListBuilder.create().texOffs(62, 272).addBox(-0.9143F, -6.0106F, 12.95F, 1.0F, 14.0F, 4.0F, new CubeDeformation(0.01F))
		.texOffs(70, 177).addBox(-0.9143F, -6.0106F, -17.4F, 1.0F, 14.0F, 4.4384F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.03F, -17.9284F, 56.1484F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r76 = base.addOrReplaceChild("cube_r76", CubeListBuilder.create().texOffs(264, 243).addBox(-0.5F, -3.5F, 18.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-0.6684F, -19.1357F, 62.7342F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r77 = base.addOrReplaceChild("cube_r77", CubeListBuilder.create().texOffs(292, 263).addBox(-0.5F, -3.5F, 18.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-0.6684F, -19.547F, 64.0919F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r78 = base.addOrReplaceChild("cube_r78", CubeListBuilder.create().texOffs(292, 265).addBox(-0.5F, -3.5F, 18.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-0.6684F, -19.1592F, 65.1366F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r79 = base.addOrReplaceChild("cube_r79", CubeListBuilder.create().texOffs(174, 280).addBox(-0.5F, -3.5F, 11.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(31.5316F, -19.1357F, 62.7342F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r80 = base.addOrReplaceChild("cube_r80", CubeListBuilder.create().texOffs(192, 280).addBox(-0.5F, -3.5F, 11.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(31.5316F, -19.547F, 64.0919F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r81 = base.addOrReplaceChild("cube_r81", CubeListBuilder.create().texOffs(0, 281).addBox(-0.5F, -3.5F, 11.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(31.5316F, -19.1592F, 65.1366F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r82 = base.addOrReplaceChild("cube_r82", CubeListBuilder.create().texOffs(280, 161).addBox(-0.5F, -3.5F, 13.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.5316F, -19.1592F, 65.1366F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r83 = base.addOrReplaceChild("cube_r83", CubeListBuilder.create().texOffs(280, 56).addBox(-0.5F, -3.5F, 13.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.5316F, -19.547F, 64.0919F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r84 = base.addOrReplaceChild("cube_r84", CubeListBuilder.create().texOffs(266, 279).addBox(-0.5F, -3.5F, 13.5F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.5316F, -19.1357F, 62.7342F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r85 = base.addOrReplaceChild("cube_r85", CubeListBuilder.create().texOffs(292, 273).addBox(-6.7F, -78.0F, -2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.45F, -12.0284F, -12.8316F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r86 = base.addOrReplaceChild("cube_r86", CubeListBuilder.create().texOffs(292, 275).addBox(-6.7F, -78.0F, 36.6184F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(17.45F, -11.7284F, -12.8316F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r87 = base.addOrReplaceChild("cube_r87", CubeListBuilder.create().texOffs(108, 281).addBox(-6.7F, -78.0F, 20.0F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(40.05F, -11.7284F, -12.8316F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r88 = base.addOrReplaceChild("cube_r88", CubeListBuilder.create().texOffs(90, 281).addBox(-6.7F, -78.0F, 22.0F, 2.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(9.05F, -11.7284F, -12.8316F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r89 = base.addOrReplaceChild("cube_r89", CubeListBuilder.create().texOffs(16, 247).addBox(-6.7F, -77.0F, 20.0F, 1.0F, 16.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(40.03F, -11.7284F, -12.8516F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r90 = base.addOrReplaceChild("cube_r90", CubeListBuilder.create().texOffs(0, 247).addBox(-6.7F, -77.0F, 22.0F, 1.0F, 16.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(9.03F, -11.7284F, -12.8516F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r91 = base.addOrReplaceChild("cube_r91", CubeListBuilder.create().texOffs(198, 231).addBox(-5.7F, -74.0F, 0.0F, 4.0F, 12.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-19.1684F, -11.7284F, -9.8516F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r92 = base.addOrReplaceChild("cube_r92", CubeListBuilder.create().texOffs(290, 222).addBox(-1.2026F, -1.0158F, -1.0F, 2.4051F, 2.0317F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.0758F, -9.3403F, 65.3685F, -1.4206F, -1.0052F, 0.97F));

		PartDefinition cube_r93 = base.addOrReplaceChild("cube_r93", CubeListBuilder.create().texOffs(290, 210).addBox(-2.5F, -15.5F, -1.0F, 2.5749F, 2.0317F, 2.0F, new CubeDeformation(0.01F))
		.texOffs(252, 273).addBox(-2.5F, -13.4483F, -1.0F, 5.0F, 12.9483F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.1051F, -6.9783F, 51.6484F, -1.4896F, -0.1546F, 1.0845F));

		PartDefinition cube_r94 = base.addOrReplaceChild("cube_r94", CubeListBuilder.create().texOffs(290, 226).addBox(-1.2026F, -1.0158F, -1.0F, 2.4051F, 2.0317F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.6689F, -8.4433F, 64.7823F, -1.4164F, -1.0225F, 0.965F));

		PartDefinition cube_r95 = base.addOrReplaceChild("cube_r95", CubeListBuilder.create().texOffs(34, 286).addBox(4.7F, -74.0F, 0.0F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(17.45F, -12.0284F, -9.8516F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r96 = base.addOrReplaceChild("cube_r96", CubeListBuilder.create().texOffs(240, 276).addBox(0.7F, -74.0F, 0.0F, 5.0F, 12.0F, 1.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -11.7284F, -9.8516F, -1.5708F, 0.0F, -1.5708F));

		PartDefinition cube_r97 = base.addOrReplaceChild("cube_r97", CubeListBuilder.create().texOffs(290, 218).addBox(-1.2026F, -1.0158F, -1.0F, 2.4051F, 2.0317F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.3758F, -9.3203F, 65.3685F, -1.4206F, 1.0052F, -0.97F));

		PartDefinition cube_r98 = base.addOrReplaceChild("cube_r98", CubeListBuilder.create().texOffs(290, 214).addBox(-1.2026F, -1.0158F, -1.0F, 2.4051F, 2.0317F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(17.9689F, -8.4233F, 64.7823F, -1.4164F, 1.0225F, -0.965F));

		PartDefinition cube_r99 = base.addOrReplaceChild("cube_r99", CubeListBuilder.create().texOffs(290, 206).addBox(-0.0749F, -15.5F, -1.0F, 2.5749F, 2.0317F, 2.0F, new CubeDeformation(0.01F))
		.texOffs(48, 272).addBox(-2.5F, -13.4483F, -1.0F, 5.0F, 12.9483F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.4051F, -6.9583F, 51.6484F, -1.4896F, 0.1546F, -1.0845F));

		PartDefinition cube_r100 = base.addOrReplaceChild("cube_r100", CubeListBuilder.create().texOffs(128, 200).addBox(-6.0F, -30.0F, -1.0F, 9.0F, 25.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.85F, -11.4484F, -20.8716F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r101 = base.addOrReplaceChild("cube_r101", CubeListBuilder.create().texOffs(288, 114).addBox(-2.5F, -35.5F, -1.0F, 5.0F, 8.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.1051F, -6.6783F, 3.6284F, -1.5708F, 0.0F, 1.0908F));

		PartDefinition cube_r102 = base.addOrReplaceChild("cube_r102", CubeListBuilder.create().texOffs(84, 135).addBox(-2.5F, -5.5F, -1.0F, 5.0F, 11.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.8051F, -6.6783F, 3.6284F, -1.5708F, 0.0F, 1.0908F));

		PartDefinition cube_r103 = base.addOrReplaceChild("cube_r103", CubeListBuilder.create().texOffs(22, 278).addBox(-1.0F, -5.0F, -1.0F, 7.0F, 6.0F, 2.0F, new CubeDeformation(0.03F)), PartPose.offsetAndRotation(19.14F, -11.5209F, -20.1366F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r104 = base.addOrReplaceChild("cube_r104", CubeListBuilder.create().texOffs(226, 276).addBox(-2.5F, -5.5F, -1.0F, 5.0F, 11.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.1051F, -6.6583F, 3.6284F, -1.5708F, 0.0F, 2.0508F));

		PartDefinition cube_r105 = base.addOrReplaceChild("cube_r105", CubeListBuilder.create().texOffs(175, 288).addBox(-2.5F, -35.5F, -1.0F, 5.0F, 8.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.4051F, -6.6583F, 3.6284F, -1.5708F, 0.0F, 2.0508F));

		PartDefinition cube_r106 = base.addOrReplaceChild("cube_r106", CubeListBuilder.create().texOffs(224, 289).addBox(-0.5F, 0.5F, -1.0F, 2.0F, 4.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -18.1126F, 53.4497F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r107 = base.addOrReplaceChild("cube_r107", CubeListBuilder.create().texOffs(252, 288).addBox(-0.5F, -4.5F, -1.0F, 2.0F, 9.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -18.5038F, 44.4913F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r108 = base.addOrReplaceChild("cube_r108", CubeListBuilder.create().texOffs(46, 247).addBox(-0.5F, -4.5F, -1.0F, 2.0F, 9.0F, 3.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -17.7284F, 35.6284F, -1.5708F, -0.1745F, 1.5708F));

		PartDefinition cube_r109 = base.addOrReplaceChild("cube_r109", CubeListBuilder.create().texOffs(198, 245).addBox(-6.0F, -62.0F, -1.0F, 9.0F, 21.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.45F, -11.4284F, -9.8716F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r110 = base.addOrReplaceChild("cube_r110", CubeListBuilder.create().texOffs(276, 101).addBox(-1.0F, 0.0F, -1.0F, 10.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -16.4284F, -20.8716F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r111 = base.addOrReplaceChild("cube_r111", CubeListBuilder.create().texOffs(208, 292).addBox(-1.0F, -1.0F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -16.01F, -21.8816F, -1.5708F, -0.7854F, 1.5708F));

		PartDefinition cube_r112 = base.addOrReplaceChild("cube_r112", CubeListBuilder.create().texOffs(292, 0).addBox(-1.0F, -4.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0101F))
		.texOffs(292, 22).addBox(-1.0F, 2.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -12.0F, -22.3F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r113 = base.addOrReplaceChild("cube_r113", CubeListBuilder.create().texOffs(40, 278).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(19.15F, -9.2485F, -22.2085F, 1.5708F, -0.8727F, -1.5708F));

		PartDefinition cube_r114 = base.addOrReplaceChild("cube_r114", CubeListBuilder.create().texOffs(174, 276).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 4.0F, 0.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.85F, -13.0F, -21.3F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r115 = base.addOrReplaceChild("cube_r115", CubeListBuilder.create().texOffs(238, 268).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-18.85F, -12.0F, -22.3F, 0.0F, 0.7854F, 0.0F));

		PartDefinition cube_r116 = base.addOrReplaceChild("cube_r116", CubeListBuilder.create().texOffs(292, 245).addBox(-1.0F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.84F, -14.5F, -21.8816F, -3.1416F, -0.7854F, 3.1416F));

		PartDefinition cube_r117 = base.addOrReplaceChild("cube_r117", CubeListBuilder.create().texOffs(292, 153).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-17.84F, -9.5F, -21.8816F, -3.1416F, -0.7854F, 3.1416F));

		PartDefinition cube_r118 = base.addOrReplaceChild("cube_r118", CubeListBuilder.create().texOffs(126, 281).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.85F, -9.2485F, -22.2085F, 1.5708F, -0.8727F, -1.5708F));

		PartDefinition cube_r119 = base.addOrReplaceChild("cube_r119", CubeListBuilder.create().texOffs(180, 95).addBox(-1.0F, -3.5F, 1.0F, 1.0F, 4.0F, 0.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.5471F, -10.51F, -21.29F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r120 = base.addOrReplaceChild("cube_r120", CubeListBuilder.create().texOffs(292, 19).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-18.85F, -9.5F, -22.3F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r121 = base.addOrReplaceChild("cube_r121", CubeListBuilder.create().texOffs(272, 75).addBox(0.0F, -2.0F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4009F, -15.6071F, -18.5884F, 0.0F, 0.1745F, 0.0F));

		PartDefinition cube_r122 = base.addOrReplaceChild("cube_r122", CubeListBuilder.create().texOffs(266, 277).addBox(-5.5F, -1.0F, -1.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.1581F, -16.1929F, -20.5283F, -0.7854F, 0.1745F, 0.0F));

		PartDefinition cube_r123 = base.addOrReplaceChild("cube_r123", CubeListBuilder.create().texOffs(276, 107).addBox(-5.5F, -0.5F, -0.5F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.4292F, -16.9F, -20.5283F, -0.7854F, -0.1745F, 0.0F));

		PartDefinition cube_r124 = base.addOrReplaceChild("cube_r124", CubeListBuilder.create().texOffs(22, 272).addBox(0.0F, -2.0F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4009F, -15.2071F, -20.5884F, 0.0F, 0.1745F, 0.0F));

		PartDefinition cube_r125 = base.addOrReplaceChild("cube_r125", CubeListBuilder.create().texOffs(150, 273).addBox(0.0F, -2.0F, -3.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5081F, -15.2071F, -20.529F, 0.0F, -0.1745F, 0.0F));

		PartDefinition cube_r126 = base.addOrReplaceChild("cube_r126", CubeListBuilder.create().texOffs(276, 109).addBox(-5.5F, -1.0F, -1.0F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.1581F, -15.7929F, -22.5283F, -0.7854F, 0.1745F, 0.0F));

		PartDefinition cube_r127 = base.addOrReplaceChild("cube_r127", CubeListBuilder.create().texOffs(236, 112).addBox(0.0F, -3.0F, -1.0F, 11.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.5236F, -13.5F, -21.2848F, 0.0F, 0.1745F, 0.0F));

		PartDefinition cube_r128 = base.addOrReplaceChild("cube_r128", CubeListBuilder.create().texOffs(274, 73).addBox(-5.5F, -0.5F, -0.5F, 11.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.4292F, -16.5F, -22.5283F, -0.7854F, -0.1745F, 0.0F));

		PartDefinition cube_r129 = base.addOrReplaceChild("cube_r129", CubeListBuilder.create().texOffs(266, 24).addBox(0.0F, -3.0F, -3.0F, 11.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.3854F, -13.5F, -21.2253F, 0.0F, -0.1745F, 0.0F));

		PartDefinition cube_r130 = base.addOrReplaceChild("cube_r130", CubeListBuilder.create().texOffs(272, 84).addBox(0.0F, -2.0F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(272, 81).addBox(0.0F, 3.0F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.5236F, -13.0F, -21.8848F, 0.0F, 0.1745F, 0.0F));

		PartDefinition cube_r131 = base.addOrReplaceChild("cube_r131", CubeListBuilder.create().texOffs(272, 87).addBox(0.0F, -2.0F, -3.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(272, 78).addBox(0.0F, 3.0F, -3.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.3854F, -13.0F, -21.8253F, 0.0F, -0.1745F, 0.0F));

		PartDefinition cube_r132 = base.addOrReplaceChild("cube_r132", CubeListBuilder.create().texOffs(266, 271).addBox(0.0F, -4.0F, -1.0F, 11.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.4856F, -10.0F, -21.0899F, 0.0F, 0.1745F, 0.0F));

		PartDefinition cube_r133 = base.addOrReplaceChild("cube_r133", CubeListBuilder.create().texOffs(124, 271).addBox(0.0F, -4.0F, -1.0F, 11.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -10.0F, -23.0F, 0.0F, -0.1745F, 0.0F));

		PartDefinition rightDoor = base.addOrReplaceChild("rightDoor", CubeListBuilder.create(), PartPose.offset(-19.8375F, -15.3725F, 9.124F));

		PartDefinition cube_r134 = rightDoor.addOrReplaceChild("cube_r134", CubeListBuilder.create().texOffs(292, 12).addBox(-3.4F, -3.3F, -0.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.4125F, -1.3559F, 2.5044F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r135 = rightDoor.addOrReplaceChild("cube_r135", CubeListBuilder.create().texOffs(290, 33).addBox(-0.5F, -0.3F, -3.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(-0.4125F, -1.3559F, 2.5044F, -1.6581F, 0.0F, -0.829F));

		PartDefinition cube_r136 = rightDoor.addOrReplaceChild("cube_r136", CubeListBuilder.create().texOffs(276, 90).addBox(-6.7348F, 1.7939F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(2.4669F, -5.968F, 5.39F, 1.5708F, 0.5124F, 1.8762F));

		PartDefinition cube_r137 = rightDoor.addOrReplaceChild("cube_r137", CubeListBuilder.create().texOffs(94, 241).addBox(-6.0F, -41.0F, -1.0F, 9.0F, 22.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.9875F, 3.9241F, -18.9956F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r138 = rightDoor.addOrReplaceChild("cube_r138", CubeListBuilder.create().texOffs(32, 247).addBox(-2.5F, -27.5F, -1.0F, 5.0F, 22.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(2.0324F, 8.6942F, -5.4956F, -1.5708F, 0.0F, 1.0908F));

		PartDefinition leftDoor = base.addOrReplaceChild("leftDoor", CubeListBuilder.create(), PartPose.offset(20.2F, -13.4284F, 9.1284F));

		PartDefinition cube_r139 = leftDoor.addOrReplaceChild("cube_r139", CubeListBuilder.create().texOffs(22, 275).addBox(-4.2652F, 1.7939F, -1.0F, 11.0F, 1.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-2.5294F, -7.9122F, 5.3856F, 1.5708F, -0.5124F, -1.8762F));

		PartDefinition cube_r140 = leftDoor.addOrReplaceChild("cube_r140", CubeListBuilder.create().texOffs(292, 8).addBox(0.4F, -3.3F, -0.4F, 3.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(0.05F, -3.3F, 2.5F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r141 = leftDoor.addOrReplaceChild("cube_r141", CubeListBuilder.create().texOffs(290, 28).addBox(-0.5F, -0.3F, -3.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.39F)), PartPose.offsetAndRotation(0.05F, -3.3F, 2.5F, -1.6581F, 0.0F, 0.829F));

		PartDefinition cube_r142 = leftDoor.addOrReplaceChild("cube_r142", CubeListBuilder.create().texOffs(116, 241).addBox(-6.0F, -41.0F, -1.0F, 9.0F, 22.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-1.05F, 2.0F, -19.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r143 = leftDoor.addOrReplaceChild("cube_r143", CubeListBuilder.create().texOffs(138, 247).addBox(-2.5F, -16.5F, -1.0F, 5.0F, 22.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-2.0949F, 6.77F, 5.5F, -1.5708F, 0.0F, 2.0508F));

		PartDefinition trunk = base.addOrReplaceChild("trunk", CubeListBuilder.create(), PartPose.offsetAndRotation(4.6197F, -18.8713F, 49.1997F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r144 = trunk.addOrReplaceChild("cube_r144", CubeListBuilder.create().texOffs(266, 287).addBox(-0.5F, -0.5F, 10.5995F, 1.0197F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(0.0F, 3.1F, 17.5F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r145 = trunk.addOrReplaceChild("cube_r145", CubeListBuilder.create().texOffs(276, 93).addBox(-0.5F, -0.5F, -0.1F, 3.0F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-4.1F, 3.1F, 17.5F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r146 = trunk.addOrReplaceChild("cube_r146", CubeListBuilder.create().texOffs(0, 289).addBox(-0.5F, -0.5F, 0.2795F, 0.95F, 1.0F, 7.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(8.7303F, 3.1F, 17.5F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r147 = trunk.addOrReplaceChild("cube_r147", CubeListBuilder.create().texOffs(284, 64).addBox(-0.5F, -0.5F, 12.4795F, 3.0F, 1.0F, 6.2205F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(14.5303F, 3.1F, 17.5F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r148 = trunk.addOrReplaceChild("cube_r148", CubeListBuilder.create().texOffs(0, 220).addBox(-0.5F, -3.5F, 10.5F, 2.0F, 1.0F, 26.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.9119F, -0.2879F, 15.9369F, -1.5708F, 1.0908F, 1.5708F));

		PartDefinition cube_r149 = trunk.addOrReplaceChild("cube_r149", CubeListBuilder.create().texOffs(208, 218).addBox(-0.5F, -3.5F, 10.5F, 2.0F, 1.0F, 26.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.9119F, -0.6757F, 14.8921F, -1.5708F, 0.8727F, 1.5708F));

		PartDefinition cube_r150 = trunk.addOrReplaceChild("cube_r150", CubeListBuilder.create().texOffs(208, 191).addBox(-0.5F, -3.5F, 10.5F, 2.0F, 1.0F, 26.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(18.9119F, -0.2645F, 13.5345F, -1.5708F, 0.48F, 1.5708F));

		PartDefinition cube_r151 = trunk.addOrReplaceChild("cube_r151", CubeListBuilder.create().texOffs(208, 29).addBox(-6.7F, -78.0F, 4.0F, 2.0F, 1.0F, 26.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(12.4303F, 7.1429F, -62.0314F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r152 = trunk.addOrReplaceChild("cube_r152", CubeListBuilder.create().texOffs(154, 191).addBox(-0.9143F, -6.0106F, -12.8833F, 1.0F, 14.0F, 25.8833F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(-4.5197F, 0.9429F, 6.9486F, -1.5708F, 0.0873F, 1.5708F));

		PartDefinition cube_r153 = trunk.addOrReplaceChild("cube_r153", CubeListBuilder.create().texOffs(182, 75).addBox(-6.7F, -77.0F, 4.0F, 1.0F, 16.0F, 26.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(12.4103F, 7.1429F, -62.0514F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition wheels = base.addOrReplaceChild("wheels", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition frontWheels = wheels.addOrReplaceChild("frontWheels", CubeListBuilder.create().texOffs(208, 73).addBox(-16.0F, -0.5F, -0.5F, 32.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.05F, -5.5F, -8.5F, 3.1416F, 0.0F, 0.0F));

		PartDefinition leftWheel = frontWheels.addOrReplaceChild("leftWheel", CubeListBuilder.create().texOffs(264, 212).addBox(-1.9F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(72, 285).addBox(-1.9F, -2.5F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(98, 265).addBox(-1.9F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(17.5F, 0.0F, 0.0F));

		PartDefinition cube_r154 = leftWheel.addOrReplaceChild("cube_r154", CubeListBuilder.create().texOffs(72, 265).addBox(-3.5F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(264, 224).addBox(-3.5F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.6F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition rightWheel = frontWheels.addOrReplaceChild("rightWheel", CubeListBuilder.create().texOffs(258, 125).addBox(-1.9F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(284, 124).addBox(-1.9F, -2.5F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(152, 261).addBox(-1.9F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-17.3F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r155 = rightWheel.addOrReplaceChild("cube_r155", CubeListBuilder.create().texOffs(262, 112).addBox(-3.5F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(46, 260).addBox(-3.5F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.6F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition backWheels = wheels.addOrReplaceChild("backWheels", CubeListBuilder.create().texOffs(224, 174).addBox(-15.75F, -6.0F, 45.0F, 32.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.2F, 0.0F, 0.0F));

		PartDefinition leftBackWheel = backWheels.addOrReplaceChild("leftBackWheel", CubeListBuilder.create().texOffs(266, 0).addBox(-1.9F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(18, 286).addBox(-1.9F, -2.5F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(266, 259).addBox(-1.9F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(17.75F, -5.5F, 45.5F));

		PartDefinition cube_r156 = leftBackWheel.addOrReplaceChild("cube_r156", CubeListBuilder.create().texOffs(268, 176).addBox(-3.5F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(266, 12).addBox(-3.5F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.6F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition rightBackWheel = backWheels.addOrReplaceChild("rightBackWheel", CubeListBuilder.create().texOffs(264, 29).addBox(-2.1F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(284, 279).addBox(-1.1F, -2.5F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(264, 41).addBox(-2.1F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(-16.85F, -5.5F, 45.5F));

		PartDefinition cube_r157 = rightBackWheel.addOrReplaceChild("cube_r157", CubeListBuilder.create().texOffs(264, 200).addBox(-3.5F, -5.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(264, 188).addBox(-3.5F, 2.5F, -4.5F, 4.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.4F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition interior = partdefinition.addOrReplaceChild("interior", CubeListBuilder.create().texOffs(199, 142).addBox(-2.35F, -17.2071F, -9.5929F, 22.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(182, 117).addBox(-16.35F, -6.2071F, -15.5929F, 33.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(43, 181).addBox(-1.85F, -10.2071F, -14.5929F, 4.0F, 5.0F, 19.0F, new CubeDeformation(0.0F))
		.texOffs(198, 119).addBox(-1.85F, -9.2071F, 4.4071F, 4.0F, 4.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(137, 280).addBox(-16.35F, -12.2071F, -14.5929F, 1.0F, 6.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(210, 280).addBox(15.65F, -12.2071F, -14.5929F, 1.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(224, 153).addBox(-16.35F, -13.2071F, -15.5929F, 33.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(222, 67).addBox(-2.35F, -13.2071F, -14.5929F, 19.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(222, 67).addBox(-2.35F, -17.2071F, -14.5929F, 1.0F, 4.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(225, 68).addBox(-19.35F, -13.3103F, -15.0146F, 17.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 17.0F));

		PartDefinition cube_r158 = interior.addOrReplaceChild("cube_r158", CubeListBuilder.create().texOffs(182, 142).addBox(-8.5F, -2.5F, -0.5F, 17.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.85F, -14.7587F, -10.3038F, 2.6616F, 0.0F, 0.0F));

		PartDefinition chairs = interior.addOrReplaceChild("chairs", CubeListBuilder.create().texOffs(182, 125).addBox(-13.5F, -8.0F, 10.5F, 27.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(236, 90).addBox(3.0F, -7.0F, -5.0F, 10.0F, 1.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(224, 176).addBox(2.5F, -8.0F, -5.5F, 11.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(236, 101).addBox(-13.0F, -7.0F, -5.0F, 10.0F, 1.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(94, 229).addBox(-13.5F, -8.0F, -5.5F, 11.0F, 1.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(208, 56).addBox(-13.0F, -7.0F, 11.0F, 26.0F, 1.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r159 = chairs.addOrReplaceChild("cube_r159", CubeListBuilder.create().texOffs(224, 161).addBox(-21.5F, -4.0F, -0.5F, 27.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.0F, -15.0F, 22.4F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r160 = chairs.addOrReplaceChild("cube_r160", CubeListBuilder.create().texOffs(242, 259).addBox(-5.5F, -5.0F, -0.5F, 11.0F, 13.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(152, 247).addBox(10.5F, -5.0F, -0.5F, 11.0F, 13.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-8.0F, -15.0F, 6.4F, -0.1745F, 0.0F, 0.0F));

		PartDefinition wheel = interior.addOrReplaceChild("wheel", CubeListBuilder.create(), PartPose.offset(8.0F, -10.7013F, -10.5466F));

		PartDefinition cube_r161 = wheel.addOrReplaceChild("cube_r161", CubeListBuilder.create().texOffs(292, 240).addBox(-3.5F, -0.5F, 2.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.6716F, -3.7858F, 0.6244F, 0.4971F, 0.445F, -0.6707F));

		PartDefinition cube_r162 = wheel.addOrReplaceChild("cube_r162", CubeListBuilder.create().texOffs(292, 238).addBox(-2.5F, -0.5F, 2.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.7929F, -2.1029F, 1.9157F, 0.4971F, -0.445F, 0.6707F));

		PartDefinition cube_r163 = wheel.addOrReplaceChild("cube_r163", CubeListBuilder.create().texOffs(292, 236).addBox(-3.5F, -0.5F, 2.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.5F, -1.8705F, 2.094F, -0.4971F, 0.445F, -2.4709F));

		PartDefinition cube_r164 = wheel.addOrReplaceChild("cube_r164", CubeListBuilder.create().texOffs(292, 158).addBox(-2.5F, -0.5F, 2.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.7929F, -1.3095F, 2.5245F, 0.4971F, 0.445F, -0.6707F));

		PartDefinition cube_r165 = wheel.addOrReplaceChild("cube_r165", CubeListBuilder.create().texOffs(294, 186).addBox(-4.5F, -0.5F, 2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.8787F, -0.0233F, 3.5115F, 0.6545F, 0.0F, 0.0F));

		PartDefinition cube_r166 = wheel.addOrReplaceChild("cube_r166", CubeListBuilder.create().texOffs(294, 184).addBox(-4.5F, -0.5F, 2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.1213F, -0.0233F, 3.5115F, 0.6545F, 0.0F, 0.0F));

		PartDefinition cube_r167 = wheel.addOrReplaceChild("cube_r167", CubeListBuilder.create().texOffs(294, 182).addBox(-4.5F, -0.5F, 2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.8787F, -3.3892F, 0.9287F, 0.6545F, 0.0F, 0.0F));

		PartDefinition cube_r168 = wheel.addOrReplaceChild("cube_r168", CubeListBuilder.create().texOffs(294, 180).addBox(-4.5F, -0.5F, 2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.1213F, -3.3892F, 0.9287F, 0.6545F, 0.0F, 0.0F));

		PartDefinition cube_r169 = wheel.addOrReplaceChild("cube_r169", CubeListBuilder.create().texOffs(294, 204).addBox(-0.5F, -0.5F, 2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(294, 202).addBox(5.5F, -0.5F, 2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(128, 281).addBox(2.5F, -0.5F, -11.0F, 1.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, -1.7062F, 2.2201F, 0.6545F, 0.0F, 0.0F));

		PartDefinition cube_r170 = wheel.addOrReplaceChild("cube_r170", CubeListBuilder.create().texOffs(294, 178).addBox(-0.0611F, -0.5F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -3.3559F, 4.1054F, 0.0F, -0.6545F, 1.5708F));

		PartDefinition cube_r171 = wheel.addOrReplaceChild("cube_r171", CubeListBuilder.create().texOffs(294, 176).addBox(0.2139F, -0.6139F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -3.3559F, 4.1054F, 0.4971F, 0.445F, -0.6707F));

		PartDefinition cube_r172 = wheel.addOrReplaceChild("cube_r172", CubeListBuilder.create().texOffs(62, 294).addBox(0.2139F, -0.3861F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -3.3559F, 4.1054F, -0.4971F, 0.445F, -2.4709F));

		PartDefinition shifter = interior.addOrReplaceChild("shifter", CubeListBuilder.create(), PartPose.offset(0.15F, -9.9514F, -4.9047F));

		PartDefinition cube_r173 = shifter.addOrReplaceChild("cube_r173", CubeListBuilder.create().texOffs(185, 131).addBox(-1.0F, -2.5F, -0.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(186, 130).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.6981F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 309, 309);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		base.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		interior.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart root() {
		return this.root;
	}

	@Override
	public void setupAnim(T pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		ImpalaExteriorAnimation.animateConditional(pEntity, this);
	}
}