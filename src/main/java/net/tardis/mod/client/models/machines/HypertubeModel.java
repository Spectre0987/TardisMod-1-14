package net.tardis.mod.client.models.machines;// Made with Blockbench 4.12.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.world.entity.Entity;
import net.tardis.mod.helpers.Helper;

public class HypertubeModel extends EntityModel<Entity> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("machines/hypertube"), "main");
	private final ModelPart glass;
	private final ModelPart glass_bend_c;
	private final ModelPart glass_bend_b;
	private final ModelPart glass_bend_a;
	private final ModelPart glass_mid;
	private final ModelPart glass_lower;
	private final ModelPart ribs;
	private final ModelPart cap_rib_1;
	private final ModelPart cap_rib_2;
	private final ModelPart rib_top;
	private final ModelPart rib_mid;
	private final ModelPart rib_base;
	private final ModelPart backbrace;
	private final ModelPart back;
	private final ModelPart backbrace_top;

	public HypertubeModel(ModelPart root) {
		super(RenderType::entityTranslucent);
		this.glass = root.getChild("glass");
		this.glass_bend_c = this.glass.getChild("glass_bend_c");
		this.glass_bend_b = this.glass.getChild("glass_bend_b");
		this.glass_bend_a = this.glass.getChild("glass_bend_a");
		this.glass_mid = this.glass.getChild("glass_mid");
		this.glass_lower = this.glass.getChild("glass_lower");
		this.ribs = root.getChild("ribs");
		this.cap_rib_1 = this.ribs.getChild("cap_rib_1");
		this.cap_rib_2 = this.ribs.getChild("cap_rib_2");
		this.rib_top = this.ribs.getChild("rib_top");
		this.rib_mid = this.ribs.getChild("rib_mid");
		this.rib_base = this.ribs.getChild("rib_base");
		this.backbrace = root.getChild("backbrace");
		this.back = this.backbrace.getChild("back");
		this.backbrace_top = this.backbrace.getChild("backbrace_top");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition glass = partdefinition.addOrReplaceChild("glass", CubeListBuilder.create(), PartPose.offset(0.0F, 22.8284F, -3.0F));

		PartDefinition glass_bend_c = glass.addOrReplaceChild("glass_bend_c", CubeListBuilder.create(), PartPose.offsetAndRotation(8.0F, -26.8284F, 18.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition octagon_r1 = glass_bend_c.addOrReplaceChild("octagon_r1", CubeListBuilder.create().texOffs(80, 2).addBox(-6.0F, -1.0F, -6.9706F, 8.0F, 2.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-12.8396F, -19.9312F, 7.9972F, 0.0F, 0.0F, -0.3927F));

		PartDefinition octagon_r2 = glass_bend_c.addOrReplaceChild("octagon_r2", CubeListBuilder.create().texOffs(80, 2).addBox(-4.0F, -2.0F, 17.0F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-12.3021F, -16.0152F, -1.1458F, 0.0F, 0.0F, -0.3927F));

		PartDefinition octagon_r3 = glass_bend_c.addOrReplaceChild("octagon_r3", CubeListBuilder.create().texOffs(80, 2).addBox(-5.75F, 1.0F, 17.0F, 4.0F, 6.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-10.4558F, -16.7842F, -1.1458F, 0.0F, 0.0F, -0.3927F));

		PartDefinition octagon_r4 = glass_bend_c.addOrReplaceChild("octagon_r4", CubeListBuilder.create().texOffs(80, 2).addBox(-2.75F, 1.0F, -1.0F, 4.0F, 6.0F, 2.0F, new CubeDeformation(-0.002F))
		.texOffs(80, 2).addBox(-4.0F, -2.0F, -1.0F, 7.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-13.2223F, -15.634F, -0.801F, 0.0F, 0.0F, -0.3927F));

		PartDefinition octagon_r5 = glass_bend_c.addOrReplaceChild("octagon_r5", CubeListBuilder.create().texOffs(80, 2).addBox(-4.0F, -2.0F, -1.0F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-14.2611F, -18.1317F, 15.7337F, 0.7854F, 0.0F, -0.3927F));

		PartDefinition octagon_r6 = glass_bend_c.addOrReplaceChild("octagon_r6", CubeListBuilder.create().texOffs(80, 2).addBox(-12.0F, -4.9706F, -11.4F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-3.2484F, -12.4599F, 5.5729F, -0.7854F, 0.0F, -0.3927F));

		PartDefinition glass_bend_b = glass.addOrReplaceChild("glass_bend_b", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.1274F, -39.3047F, 0.2766F, 0.0F, 1.5708F, 0.0F));

		PartDefinition octagon_r7 = glass_bend_b.addOrReplaceChild("octagon_r7", CubeListBuilder.create().texOffs(80, 2).addBox(-2.5F, -2.5F, -1.0F, 5.0F, 5.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.2259F, 0.3273F, 8.8276F, 0.0F, 0.0F, 0.7854F));

		PartDefinition octagon_r8 = glass_bend_b.addOrReplaceChild("octagon_r8", CubeListBuilder.create().texOffs(80, 2).addBox(-3.5F, -2.0F, -1.0F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(2.0405F, -1.9391F, -7.7071F, -0.7854F, 0.0F, 0.7854F));

		PartDefinition octagon_r9 = glass_bend_b.addOrReplaceChild("octagon_r9", CubeListBuilder.create().texOffs(80, 2).addBox(-2.5F, -2.5F, -1.0F, 5.0F, 5.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.2259F, 0.3273F, -8.8276F, 0.0F, 0.0F, 0.7854F));

		PartDefinition octagon_r10 = glass_bend_b.addOrReplaceChild("octagon_r10", CubeListBuilder.create().texOffs(80, 2).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-3.0543F, 3.1557F, -8.8276F, 0.0F, 0.0F, 0.7854F));

		PartDefinition octagon_r11 = glass_bend_b.addOrReplaceChild("octagon_r11", CubeListBuilder.create().texOffs(80, 2).addBox(-3.5F, -1.0F, -7.0F, 7.0F, 2.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.83F, -2.7314F, 0.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition octagon_r12 = glass_bend_b.addOrReplaceChild("octagon_r12", CubeListBuilder.create().texOffs(80, 2).addBox(-3.5F, -2.0F, -1.0F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(2.0405F, -1.9391F, 7.7071F, 0.7854F, 0.0F, 0.7854F));

		PartDefinition octagon_r13 = glass_bend_b.addOrReplaceChild("octagon_r13", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-3.405F, 2.7993F, 8.8276F, 0.0F, 0.0F, 0.7854F));

		PartDefinition glass_bend_a = glass.addOrReplaceChild("glass_bend_a", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0889F, -34.4807F, -0.969F, 1.5708F, 0.0F, -1.5708F));

		PartDefinition octagon_r14 = glass_bend_a.addOrReplaceChild("octagon_r14", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -1.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-2.082F, 5.6245F, -8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition octagon_r15 = glass_bend_a.addOrReplaceChild("octagon_r15", CubeListBuilder.create().texOffs(80, 2).addBox(-2.5F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.6932F, 2.3732F, -8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition octagon_r16 = glass_bend_a.addOrReplaceChild("octagon_r16", CubeListBuilder.create().texOffs(80, 2).addBox(-3.25F, -2.0F, -1.0F, 6.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.3421F, -1.4905F, -8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition octagon_r17 = glass_bend_a.addOrReplaceChild("octagon_r17", CubeListBuilder.create().texOffs(80, 2).addBox(-3.5F, -2.0F, -1.0F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(1.0422F, -4.1034F, -7.7071F, -0.7854F, 0.0F, 0.2618F));

		PartDefinition octagon_r18 = glass_bend_a.addOrReplaceChild("octagon_r18", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -4.5F, -7.0F, 2.0F, 8.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.8468F, -5.3137F, 0.0F, 0.0F, 0.0F, 1.8326F));

		PartDefinition octagon_r19 = glass_bend_a.addOrReplaceChild("octagon_r19", CubeListBuilder.create().texOffs(80, 2).addBox(-3.5F, -2.0F, -1.0F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(1.0422F, -4.1034F, 7.7071F, 0.7854F, 0.0F, 0.2618F));

		PartDefinition octagon_r20 = glass_bend_a.addOrReplaceChild("octagon_r20", CubeListBuilder.create().texOffs(80, 2).addBox(-2.5F, -2.0F, -1.0F, 6.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.1421F, -1.4905F, 8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition octagon_r21 = glass_bend_a.addOrReplaceChild("octagon_r21", CubeListBuilder.create().texOffs(80, 2).addBox(-1.75F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.6922F, 2.3693F, 8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition octagon_r22 = glass_bend_a.addOrReplaceChild("octagon_r22", CubeListBuilder.create().texOffs(80, 2).addBox(-2.0F, -1.5F, -1.0F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.6311F, 6.005F, 8.8276F, 0.0F, 0.0F, 0.2618F));

		PartDefinition glass_mid = glass.addOrReplaceChild("glass_mid", CubeListBuilder.create().texOffs(80, 2).addBox(-8.0013F, -5.0F, 7.799F, 16.0F, 10.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(80, 2).addBox(-8.0013F, -5.0F, -9.799F, 16.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0008F, -22.8291F, -0.0813F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition octagon_r23 = glass_mid.addOrReplaceChild("octagon_r23", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -4.9706F, -12.0F, 16.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-6.9993F, 4.1716F, -2.0028F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r24 = glass_mid.addOrReplaceChild("octagon_r24", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, 4.0F, -4.0294F, 16.0F, 2.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-6.9993F, 3.6066F, -2.7099F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r25 = glass_mid.addOrReplaceChild("octagon_r25", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -12.0F, -4.9706F, 16.0F, 2.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-6.9993F, 4.1716F, 2.0028F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r26 = glass_mid.addOrReplaceChild("octagon_r26", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -6.0294F, -1.0F, 16.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-6.9993F, 8.5563F, 4.8313F, -0.7854F, 0.0F, 0.0F));

		PartDefinition glass_lower = glass.addOrReplaceChild("glass_lower", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -5.0F, -9.799F, 16.0F, 10.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(80, 2).addBox(-1.0F, -5.0F, 7.799F, 16.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, 0.0F, -1.5708F));

		PartDefinition octagon_r27 = glass_lower.addOrReplaceChild("octagon_r27", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -12.0F, -4.9706F, 16.0F, 2.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.002F, 4.1716F, 2.0028F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r28 = glass_lower.addOrReplaceChild("octagon_r28", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, 4.0F, -4.0294F, 16.0F, 2.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.002F, 3.6066F, -2.7099F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r29 = glass_lower.addOrReplaceChild("octagon_r29", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -6.0294F, -1.0F, 16.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.002F, 8.5563F, 4.8313F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r30 = glass_lower.addOrReplaceChild("octagon_r30", CubeListBuilder.create().texOffs(80, 2).addBox(-1.0F, -4.9706F, -12.0F, 16.0F, 4.0F, 2.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.002F, 4.1716F, -2.0028F, -0.7854F, 0.0F, 0.0F));

		PartDefinition ribs = partdefinition.addOrReplaceChild("ribs", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 6.8284F, -3.0F, 1.5708F, 0.0F, -1.5708F));

		PartDefinition cap_rib_1 = ribs.addOrReplaceChild("cap_rib_1", CubeListBuilder.create(), PartPose.offset(21.4822F, -1.1391F, -9.5845F));

		PartDefinition octagon_r31 = cap_rib_1.addOrReplaceChild("octagon_r31", CubeListBuilder.create().texOffs(44, 89).addBox(-1.0F, -1.0F, 2.0F, 2.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1893F, -0.3279F, -1.7071F, 0.7854F, 0.0F, 0.5236F));

		PartDefinition octagon_r32 = cap_rib_1.addOrReplaceChild("octagon_r32", CubeListBuilder.create().texOffs(5, 69).addBox(-1.0F, -1.0F, -9.5F, 2.0F, 3.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.5236F));

		PartDefinition octagon_r33 = cap_rib_1.addOrReplaceChild("octagon_r33", CubeListBuilder.create().texOffs(5, 69).addBox(-1.0F, -1.0F, -7.0F, 2.0F, 4.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.5178F, -4.3609F, 9.5355F, 0.0F, 0.0F, 0.5236F));

		PartDefinition octagon_r34 = cap_rib_1.addOrReplaceChild("octagon_r34", CubeListBuilder.create().texOffs(17, 105).addBox(-1.0F, -1.0F, -2.5F, 2.0F, 3.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.7803F, -3.0836F, 17.5962F, -0.7854F, 0.0F, 0.5236F));

		PartDefinition octagon_r35 = cap_rib_1.addOrReplaceChild("octagon_r35", CubeListBuilder.create().texOffs(5, 69).addBox(-1.0F, 17.0F, -9.5F, 2.0F, 3.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0711F, 1.5708F, 0.0F, 0.5236F));

		PartDefinition cap_rib_2 = ribs.addOrReplaceChild("cap_rib_2", CubeListBuilder.create(), PartPose.offset(22.3353F, -2.3124F, -9.5845F));

		PartDefinition octagon_r36 = cap_rib_2.addOrReplaceChild("octagon_r36", CubeListBuilder.create().texOffs(5, 69).addBox(4.0F, -1.0F, -9.5F, 2.0F, 3.0F, 12.0F, new CubeDeformation(0.002F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 1.0472F));

		PartDefinition octagon_r37 = cap_rib_2.addOrReplaceChild("octagon_r37", CubeListBuilder.create().texOffs(5, 69).addBox(5.0F, -2.0F, 3.0F, 2.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.3968F, -0.3483F, -1.7071F, 0.7854F, 0.0F, 1.0472F));

		PartDefinition octagon_r38 = cap_rib_2.addOrReplaceChild("octagon_r38", CubeListBuilder.create().texOffs(5, 69).addBox(-1.0F, 1.0F, -7.0F, 2.0F, 4.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.593F, 0.8124F, 9.5355F, 0.0F, 0.0F, 1.0472F));

		PartDefinition octagon_r39 = cap_rib_2.addOrReplaceChild("octagon_r39", CubeListBuilder.create().texOffs(5, 69).addBox(-1.0F, -1.0F, -2.5F, 2.0F, 3.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.5836F, 2.5498F, 17.5962F, -0.7854F, 0.0F, 1.0472F));

		PartDefinition octagon_r40 = cap_rib_2.addOrReplaceChild("octagon_r40", CubeListBuilder.create().texOffs(5, 69).addBox(4.0F, 20.0F, -9.5F, 2.0F, 3.0F, 12.0F, new CubeDeformation(0.002F)), PartPose.offsetAndRotation(0.0F, 0.0F, -2.9289F, 1.5708F, 0.0F, 1.0472F));

		PartDefinition rib_top = ribs.addOrReplaceChild("rib_top", CubeListBuilder.create().texOffs(5, 69).addBox(-17.002F, -16.9497F, 5.6111F, 2.0F, 12.0F, 5.0F, new CubeDeformation(0.001F))
		.texOffs(5, 69).addBox(-17.002F, -16.9497F, -11.4543F, 2.0F, 12.0F, 5.0F, new CubeDeformation(0.001F)), PartPose.offset(31.752F, 11.5497F, 0.3661F));

		PartDefinition octagon_r41 = rib_top.addOrReplaceChild("octagon_r41", CubeListBuilder.create().texOffs(5, 69).addBox(-5.25F, -2.0F, -8.0F, 5.0F, 2.0F, 15.0F, new CubeDeformation(0.002F)), PartPose.offsetAndRotation(-15.002F, -20.7325F, 0.0784F, 0.0F, 0.0F, -1.5708F));

		PartDefinition octagon_r42 = rib_top.addOrReplaceChild("octagon_r42", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, 3.0294F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.001F, -3.5553F, -0.7196F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r43 = rib_top.addOrReplaceChild("octagon_r43", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, -19.9706F, 2.0F, 5.0F, 6.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.004F, 0.0F, -0.1642F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r44 = rib_top.addOrReplaceChild("octagon_r44", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -20.0F, -5.9706F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.001F, 10.5868F, -18.55F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r45 = rib_top.addOrReplaceChild("octagon_r45", CubeListBuilder.create().texOffs(44, 72).addBox(-17.0F, -14.0F, -5.9706F, 2.0F, 6.0F, 5.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-0.004F, -6.364F, 1.4008F, -0.7854F, 0.0F, 0.0F));

		PartDefinition rib_mid = ribs.addOrReplaceChild("rib_mid", CubeListBuilder.create().texOffs(5, 69).addBox(-17.002F, -16.6997F, 6.8861F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.001F))
		.texOffs(5, 69).addBox(-17.002F, -16.9497F, -10.8401F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.001F)), PartPose.offset(15.002F, 10.9497F, 0.4911F));

		PartDefinition octagon_r46 = rib_mid.addOrReplaceChild("octagon_r46", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, -19.9706F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.45F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r47 = rib_mid.addOrReplaceChild("octagon_r47", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -20.0F, -5.9706F, 2.0F, 4.0F, 5.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.0F, 10.5858F, -17.9348F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r48 = rib_mid.addOrReplaceChild("octagon_r48", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -14.0F, -5.9706F, 2.0F, 4.0F, 5.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.001F, -6.115F, 0.6768F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r49 = rib_mid.addOrReplaceChild("octagon_r49", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, 8.0294F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(0.001F, -6.8409F, -4.9801F, -0.7854F, 0.0F, 0.0F));

		PartDefinition rib_base = ribs.addOrReplaceChild("rib_base", CubeListBuilder.create().texOffs(5, 69).addBox(-17.002F, -16.9497F, 6.8861F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(5, 69).addBox(-17.002F, -16.9497F, -10.9401F, 2.0F, 12.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.148F, 10.9497F, 0.4911F));

		PartDefinition octagon_r50 = rib_base.addOrReplaceChild("octagon_r50", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, -19.9706F, 2.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.35F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r51 = rib_base.addOrReplaceChild("octagon_r51", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -20.0F, -5.9706F, 2.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 10.5868F, -18.0344F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r52 = rib_base.addOrReplaceChild("octagon_r52", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -14.0F, -5.9706F, 2.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -6.364F, 0.6758F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r53 = rib_base.addOrReplaceChild("octagon_r53", CubeListBuilder.create().texOffs(5, 69).addBox(-17.0F, -9.0F, 8.0294F, 2.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -7.0909F, -4.9815F, -0.7854F, 0.0F, 0.0F));

		PartDefinition backbrace = partdefinition.addOrReplaceChild("backbrace", CubeListBuilder.create(), PartPose.offset(0.0F, 26.6569F, -3.0F));

		PartDefinition back = backbrace.addOrReplaceChild("back", CubeListBuilder.create().texOffs(0, 22).addBox(-9.5F, -15.4569F, 14.25F, 16.0F, 16.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-9.5F, -31.4569F, 14.25F, 16.0F, 16.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(1.4206F, -3.2F, -8.0F));

		PartDefinition backbrace_top = backbrace.addOrReplaceChild("backbrace_top", CubeListBuilder.create().texOffs(57, 25).addBox(-2.2459F, -5.5104F, 7.8358F, 5.0F, 14.0F, 3.0F, new CubeDeformation(-0.002F))
		.texOffs(28, 29).addBox(-2.2459F, -8.3388F, -7.9927F, 5.0F, 3.0F, 16.0F, new CubeDeformation(-0.002F))
		.texOffs(6, 50).addBox(-2.093F, -5.39F, -7.0148F, 3.0F, 1.0F, 14.0F, new CubeDeformation(-0.001F))
		.texOffs(2, 45).addBox(-2.2459F, -5.5104F, -10.8211F, 5.0F, 14.0F, 3.0F, new CubeDeformation(-0.002F))
		.texOffs(43, 0).addBox(-2.093F, -5.8115F, -8.0183F, 3.0F, 13.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 0).addBox(-2.093F, -5.8115F, 6.9817F, 3.0F, 13.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0884F, -45.085F, 8.4943F, 0.0F, -1.5708F, 0.0F));

		PartDefinition octagon_r54 = backbrace_top.addOrReplaceChild("octagon_r54", CubeListBuilder.create().texOffs(43, 15).addBox(-2.5F, -2.0F, -1.0F, 5.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.2541F, -6.2175F, -8.6998F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r55 = backbrace_top.addOrReplaceChild("octagon_r55", CubeListBuilder.create().texOffs(28, 50).addBox(-3.0F, 4.0F, -4.0294F, 5.0F, 4.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.7541F, 5.682F, -2.315F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r56 = backbrace_top.addOrReplaceChild("octagon_r56", CubeListBuilder.create().texOffs(54, 50).addBox(-4.0F, -6.0294F, -3.0F, 5.0F, 7.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.7541F, 12.046F, 5.8652F, -0.7854F, 0.0F, 0.0F));

		PartDefinition octagon_r57 = backbrace_top.addOrReplaceChild("octagon_r57", CubeListBuilder.create().texOffs(60, 14).addBox(-4.0F, -12.0F, -4.9706F, 5.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.7541F, 3.6612F, 3.0368F, -0.7854F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		glass.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		ribs.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		backbrace.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}