package net.tardis.mod.client.models.consoles;// Made with Blockbench 4.9.4
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.consoles.NouveauConsoleAnimations;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class NouveauConsoleModel<T extends ConsoleTile> extends BasicTileHierarchicalModel<T> implements IAdditionalConsoleRenderData {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("neuveau_console"), "main");
	private final ModelPart stations;
	private final ModelPart rotor;
	private final ModelPart Controls;

	public NouveauConsoleModel(ModelPart root) {
		super(root);
		this.stations = root.getChild("stations");
		this.rotor = root.getChild("rotor");
		this.Controls = root.getChild("Controls");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition stations = partdefinition.addOrReplaceChild("stations", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition collum = stations.addOrReplaceChild("collum", CubeListBuilder.create(), PartPose.offset(-9.49F, -25.6255F, 0.3029F));

		PartDefinition collum_1 = collum.addOrReplaceChild("collum_1", CubeListBuilder.create().texOffs(203, 141).addBox(-10.49F, -26.6255F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(203, 141).addBox(-10.49F, -28.3755F, -0.6971F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(203, 141).addBox(-11.765F, -31.9005F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(203, 141).addBox(-10.9257F, -35.1279F, -0.6721F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offset(9.49F, 25.6255F, -0.3029F));

		PartDefinition cube_r1 = collum_1.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(203, 141).addBox(-2.9F, -3.4F, -1.075F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-9.8924F, -29.4886F, 0.4029F, 0.0F, 0.0F, 0.576F));

		PartDefinition cube_r2 = collum_1.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-9.49F, -29.3755F, 0.3029F, 0.0F, 0.0F, -0.576F));

		PartDefinition collum_2 = collum.addOrReplaceChild("collum_2", CubeListBuilder.create().texOffs(203, 141).addBox(-10.49F, -26.6255F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(203, 141).addBox(-10.49F, -28.3755F, -0.6971F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(203, 141).addBox(-11.765F, -31.9005F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(203, 141).addBox(-10.9257F, -35.1279F, -0.6721F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(9.09F, 25.4255F, 0.0721F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r3 = collum_2.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(203, 141).addBox(-2.9F, -3.4F, -1.075F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-9.8924F, -29.4886F, 0.4029F, 0.0F, 0.0F, 0.576F));

		PartDefinition cube_r4 = collum_2.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-9.49F, -29.3755F, 0.3029F, 0.0F, 0.0F, -0.576F));

		PartDefinition collum_3 = collum.addOrReplaceChild("collum_3", CubeListBuilder.create().texOffs(203, 141).addBox(-10.49F, -26.6255F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(203, 141).addBox(-10.49F, -28.3755F, -0.6971F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.25F))
		.texOffs(203, 141).addBox(-11.765F, -31.9005F, -0.6971F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(203, 141).addBox(-10.9257F, -35.1279F, -0.6721F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(8.59F, 25.4255F, -0.7029F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r5 = collum_3.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(203, 141).addBox(-2.9F, -3.4F, -1.075F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-9.8924F, -29.4886F, 0.4029F, 0.0F, 0.0F, 0.576F));

		PartDefinition cube_r6 = collum_3.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-9.49F, -29.3755F, 0.3029F, 0.0F, 0.0F, -0.576F));

		PartDefinition station_1 = stations.addOrReplaceChild("station_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition edge_1 = station_1.addOrReplaceChild("edge_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_1 = edge_1.addOrReplaceChild("plane_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.225F, 0.0F, 0.1F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r7 = plane_1.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(123, 245).addBox(-8.5F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(220, 160).addBox(-12.5F, -1.924F, -1.3682F, 8.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-9.5F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-13.0F, -3.424F, -1.3682F, 5.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 245).addBox(-14.0F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.9876F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r8 = plane_1.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(130, 244).addBox(-12.25F, -6.7686F, -2.0035F, 16.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.2376F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r9 = plane_1.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(123, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 236).addBox(-23.5F, -6.5315F, -1.6131F, 10.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.7376F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r10 = plane_1.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(123, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 234).addBox(-20.75F, -4.2938F, -2.9668F, 11.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.9876F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r11 = plane_1.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(140, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(140, 201).addBox(-16.1F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.3876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r12 = plane_1.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(140, 200).addBox(-9.6F, 2.8815F, -1.3322F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(140, 200).addBox(-4.1F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.0376F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r13 = plane_1.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(140, 203).addBox(-11.6F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(140, 203).addBox(-18.1F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r14 = plane_1.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(140, 206).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r15 = plane_1.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(140, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r16 = plane_1.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(140, 209).addBox(-13.5F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r17 = plane_1.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(140, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r18 = plane_1.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(57, 246).addBox(-8.1F, 3.328F, -3.0864F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-8.1F, 3.328F, -4.0864F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-8.1F, 3.828F, -5.0864F, 11.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition floorstrut_1 = edge_1.addOrReplaceChild("floorstrut_1", CubeListBuilder.create().texOffs(193, 163).addBox(-6.0F, -1.25F, -3.25F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(193, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.1421F, -2.1332F, -8.3739F, 0.5236F, 0.0F, 0.0F));

		PartDefinition ribs = station_1.addOrReplaceChild("ribs", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a = ribs.addOrReplaceChild("rib_a", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_bolts_1 = rib_a.addOrReplaceChild("rib_bolts_1", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -3.1047F, -7.9765F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.2838F, -12.7947F, -13.5043F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r19 = rib_bolts_1.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.0F, 0.175F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r20 = rib_bolts_1.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -2.5F, -0.175F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -7.3615F, -2.8692F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r21 = rib_bolts_1.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.55F, 0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r22 = rib_bolts_1.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r23 = rib_bolts_1.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.375F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r24 = rib_bolts_1.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition front_trim_1 = rib_a.addOrReplaceChild("front_trim_1", CubeListBuilder.create().texOffs(207, 178).addBox(-1.8908F, -17.1271F, -21.0677F, 13.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(207, 178).addBox(-11.2908F, -17.1271F, -21.0677F, 11.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition curves_1 = rib_a.addOrReplaceChild("curves_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, -0.5236F, 0.0F));

		PartDefinition cube_r25 = curves_1.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -2.6918F, -4.0438F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(230, 160).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -10.5381F, -1.286F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r26 = curves_1.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -12.6693F, -3.4712F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r27 = curves_1.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -14.845F, -6.0775F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r28 = curves_1.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -17.3422F, -8.3775F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r29 = curves_1.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -19.2004F, -9.2939F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r30 = curves_1.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(230, 160).addBox(-1.85F, -1.622F, -5.1864F, 4.0F, 3.0F, 6.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-0.66F, -21.9268F, 3.5539F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r31 = curves_1.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -22.2124F, 3.8253F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r32 = curves_1.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -22.2104F, 0.9231F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r33 = curves_1.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -22.8938F, -3.455F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r34 = curves_1.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -22.8938F, -5.7378F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r35 = curves_1.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -22.1997F, -7.7825F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r36 = curves_1.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -21.0743F, -9.0886F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r37 = curves_1.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -7.7878F, -0.5374F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r38 = curves_1.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(230, 160).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.73F))
		.texOffs(230, 160).addBox(-2.0F, -0.2059F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -4.4191F, -2.6795F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r39 = curves_1.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(230, 156).addBox(-0.7884F, -4.325F, -0.9675F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-1.2116F, -8.2172F, -9.7384F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r40 = curves_1.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(230, 156).addBox(-1.5F, -2.5F, -2.0F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -6.2175F, -9.14F, -0.4363F, 0.0F, 0.0F));

		PartDefinition station_2 = stations.addOrReplaceChild("station_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.275F, 0.0F, 0.35F, 0.0F, -1.0472F, 0.0F));

		PartDefinition edge_2 = station_2.addOrReplaceChild("edge_2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorstrut_2 = edge_2.addOrReplaceChild("floorstrut_2", CubeListBuilder.create().texOffs(200, 163).addBox(-6.0F, -1.25F, -3.25F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(200, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.6171F, -2.1332F, -8.2489F, 0.5236F, 0.0F, 0.0F));

		PartDefinition plane_2 = edge_2.addOrReplaceChild("plane_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.05F, 0.0F, 0.1F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r41 = plane_2.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(123, 245).addBox(-8.5F, -6.924F, -1.3682F, 6.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(212, 157).addBox(-12.25F, -1.924F, -1.3682F, 9.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-9.25F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-13.75F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 245).addBox(-15.0F, -6.924F, -1.3682F, 8.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.7376F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r42 = plane_2.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(130, 244).addBox(-13.25F, -6.7686F, -2.0035F, 17.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.2376F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r43 = plane_2.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(143, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(157, 236).addBox(-24.5F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.7376F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r44 = plane_2.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(123, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 234).addBox(-21.75F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.9876F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r45 = plane_2.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(122, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(119, 201).addBox(-16.1F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.3876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r46 = plane_2.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(123, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.5376F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r47 = plane_2.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(123, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.0376F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r48 = plane_2.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(122, 203).addBox(-12.1F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 203).addBox(-18.6F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r49 = plane_2.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(130, 204).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r50 = plane_2.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(123, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r51 = plane_2.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(122, 209).addBox(-13.5F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r52 = plane_2.addOrReplaceChild("cube_r52", CubeListBuilder.create().texOffs(123, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r53 = plane_2.addOrReplaceChild("cube_r53", CubeListBuilder.create().texOffs(57, 246).addBox(-9.1F, 3.328F, -3.0864F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-9.1F, 3.328F, -4.0864F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-9.1F, 3.828F, -5.0864F, 12.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition ribs2 = station_2.addOrReplaceChild("ribs2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a2 = ribs2.addOrReplaceChild("rib_a2", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition curves_2 = rib_a2.addOrReplaceChild("curves_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.2F, 0.0F, -0.134F, 0.4363F, -0.5236F, 0.0F));

		PartDefinition cube_r54 = curves_2.addOrReplaceChild("cube_r54", CubeListBuilder.create().texOffs(230, 150).addBox(-1.5F, 3.0958F, 1.3679F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -4.3276F, -8.1936F, -2.618F, 0.0F, 0.0F));

		PartDefinition cube_r55 = curves_2.addOrReplaceChild("cube_r55", CubeListBuilder.create().texOffs(230, 175).addBox(-2.0F, 1.2139F, -6.3302F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -6.2092F, -8.4522F, -1.1345F, 0.0F, 0.0F));

		PartDefinition cube_r56 = curves_2.addOrReplaceChild("cube_r56", CubeListBuilder.create().texOffs(230, 150).addBox(-1.0F, 0.9952F, -1.7181F, 3.0F, 6.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.0F, -3.9507F, -7.2763F, -1.9635F, 0.0F, 0.0F));

		PartDefinition cube_r57 = curves_2.addOrReplaceChild("cube_r57", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -2.6668F, -4.0438F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(230, 155).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -10.5381F, -1.286F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r58 = curves_2.addOrReplaceChild("cube_r58", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -12.6693F, -3.4712F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r59 = curves_2.addOrReplaceChild("cube_r59", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -14.845F, -6.0775F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r60 = curves_2.addOrReplaceChild("cube_r60", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -17.3422F, -8.3775F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r61 = curves_2.addOrReplaceChild("cube_r61", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -19.2004F, -9.2939F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r62 = curves_2.addOrReplaceChild("cube_r62", CubeListBuilder.create().texOffs(230, 150).addBox(-2.0F, -5.2139F, -3.8302F, 4.0F, 7.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(230, 150).addBox(-2.0F, -5.2139F, -4.3302F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -23.6438F, 5.1431F, 0.2618F, 0.0F, 0.0F));

		PartDefinition cube_r63 = curves_2.addOrReplaceChild("cube_r63", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -22.4624F, 3.8253F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r64 = curves_2.addOrReplaceChild("cube_r64", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -22.5104F, 0.9231F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r65 = curves_2.addOrReplaceChild("cube_r65", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -3.455F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r66 = curves_2.addOrReplaceChild("cube_r66", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -5.7378F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r67 = curves_2.addOrReplaceChild("cube_r67", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -22.4997F, -7.7825F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r68 = curves_2.addOrReplaceChild("cube_r68", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -21.0743F, -9.0886F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r69 = curves_2.addOrReplaceChild("cube_r69", CubeListBuilder.create().texOffs(230, 150).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -7.7878F, -0.5374F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r70 = curves_2.addOrReplaceChild("cube_r70", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 6.0F, 4.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -4.4191F, -2.6795F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r71 = curves_2.addOrReplaceChild("cube_r71", CubeListBuilder.create().texOffs(230, 150).addBox(-2.0F, 1.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -3.7294F, -5.1584F, -1.5708F, 0.0F, 0.0F));

		PartDefinition front_trim_2 = rib_a2.addOrReplaceChild("front_trim_2", CubeListBuilder.create().texOffs(200, 150).addBox(-1.3908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(200, 150).addBox(-11.7908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_bolts_2 = ribs2.addOrReplaceChild("rib_bolts_2", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -3.1047F, -7.6265F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.3088F, -12.7947F, -13.8543F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r72 = rib_bolts_2.addOrReplaceChild("cube_r72", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.0F, 0.175F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r73 = rib_bolts_2.addOrReplaceChild("cube_r73", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -2.5F, -0.325F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -7.3615F, -2.8692F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r74 = rib_bolts_2.addOrReplaceChild("cube_r74", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.55F, -0.025F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r75 = rib_bolts_2.addOrReplaceChild("cube_r75", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -1.25F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 11.8283F, 3.4996F, -1.1083F, 0.0F, 0.0F));

		PartDefinition cube_r76 = rib_bolts_2.addOrReplaceChild("cube_r76", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 7.1266F, 5.6802F, -0.2356F, 0.0F, 0.0F));

		PartDefinition cube_r77 = rib_bolts_2.addOrReplaceChild("cube_r77", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r78 = rib_bolts_2.addOrReplaceChild("cube_r78", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.375F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r79 = rib_bolts_2.addOrReplaceChild("cube_r79", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition station_3 = stations.addOrReplaceChild("station_3", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.725F, 0.0F, 0.1F, 0.0F, -2.0944F, 0.0F));

		PartDefinition edge_3 = station_3.addOrReplaceChild("edge_3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorstrut_3 = edge_3.addOrReplaceChild("floorstrut_3", CubeListBuilder.create().texOffs(198, 163).addBox(-6.0F, -1.25F, -3.25F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(200, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.0921F, -2.1332F, -8.2989F, 0.5236F, 0.0F, 0.0F));

		PartDefinition plane_3 = edge_3.addOrReplaceChild("plane_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.35F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r80 = plane_3.addOrReplaceChild("cube_r80", CubeListBuilder.create().texOffs(128, 245).addBox(-8.5F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 166).addBox(-13.5F, -1.924F, -1.3682F, 10.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 192).addBox(-9.5F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 192).addBox(-14.0F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(120, 245).addBox(-15.0F, -6.924F, -1.3682F, 8.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.9876F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r81 = plane_3.addOrReplaceChild("cube_r81", CubeListBuilder.create().texOffs(130, 244).addBox(-13.25F, -6.7686F, -2.0035F, 17.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.2376F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r82 = plane_3.addOrReplaceChild("cube_r82", CubeListBuilder.create().texOffs(124, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 236).addBox(-24.5F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.7376F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r83 = plane_3.addOrReplaceChild("cube_r83", CubeListBuilder.create().texOffs(122, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 234).addBox(-21.75F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.9876F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r84 = plane_3.addOrReplaceChild("cube_r84", CubeListBuilder.create().texOffs(130, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.4876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r85 = plane_3.addOrReplaceChild("cube_r85", CubeListBuilder.create().texOffs(123, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r86 = plane_3.addOrReplaceChild("cube_r86", CubeListBuilder.create().texOffs(130, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 200).addBox(-4.35F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.7876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r87 = plane_3.addOrReplaceChild("cube_r87", CubeListBuilder.create().texOffs(130, 203).addBox(-12.1F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(130, 203).addBox(-18.6F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r88 = plane_3.addOrReplaceChild("cube_r88", CubeListBuilder.create().texOffs(130, 206).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r89 = plane_3.addOrReplaceChild("cube_r89", CubeListBuilder.create().texOffs(130, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r90 = plane_3.addOrReplaceChild("cube_r90", CubeListBuilder.create().texOffs(130, 209).addBox(-13.5F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r91 = plane_3.addOrReplaceChild("cube_r91", CubeListBuilder.create().texOffs(130, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r92 = plane_3.addOrReplaceChild("cube_r92", CubeListBuilder.create().texOffs(57, 246).addBox(-9.1F, 3.328F, -3.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-9.1F, 3.328F, -4.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-9.1F, 3.828F, -5.0864F, 12.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition ribs3 = station_3.addOrReplaceChild("ribs3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a3 = ribs3.addOrReplaceChild("rib_a3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition front_trim_3 = rib_a3.addOrReplaceChild("front_trim_3", CubeListBuilder.create().texOffs(192, 150).addBox(-1.1408F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(192, 150).addBox(-11.5408F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_bolts_3 = ribs3.addOrReplaceChild("rib_bolts_3", CubeListBuilder.create().texOffs(163, 173).addBox(-0.55F, -3.1047F, -7.7265F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.5088F, -12.7947F, -13.6043F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r93 = rib_bolts_3.addOrReplaceChild("cube_r93", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.0F, 0.3F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r94 = rib_bolts_3.addOrReplaceChild("cube_r94", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -2.5F, -0.3F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -7.3615F, -2.8692F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r95 = rib_bolts_3.addOrReplaceChild("cube_r95", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.55F, 0.1F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r96 = rib_bolts_3.addOrReplaceChild("cube_r96", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r97 = rib_bolts_3.addOrReplaceChild("cube_r97", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.375F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r98 = rib_bolts_3.addOrReplaceChild("cube_r98", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition curves_3 = station_3.addOrReplaceChild("curves_3", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.275F, 0.0F, 0.15F, 0.4363F, -2.618F, 0.0F));

		PartDefinition cube_r99 = curves_3.addOrReplaceChild("cube_r99", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -2.7168F, -4.0438F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(203, 141).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -10.5381F, -1.286F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r100 = curves_3.addOrReplaceChild("cube_r100", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -12.6693F, -3.4712F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r101 = curves_3.addOrReplaceChild("cube_r101", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -14.845F, -6.0775F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r102 = curves_3.addOrReplaceChild("cube_r102", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -17.3422F, -8.3775F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r103 = curves_3.addOrReplaceChild("cube_r103", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -19.2004F, -9.2939F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r104 = curves_3.addOrReplaceChild("cube_r104", CubeListBuilder.create().texOffs(203, 141).addBox(-2.15F, -1.372F, -4.9364F, 4.0F, 3.0F, 6.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-0.34F, -22.2535F, 3.6892F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r105 = curves_3.addOrReplaceChild("cube_r105", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -22.2124F, 3.8253F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r106 = curves_3.addOrReplaceChild("cube_r106", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -22.2354F, 0.9231F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r107 = curves_3.addOrReplaceChild("cube_r107", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -22.9188F, -3.455F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r108 = curves_3.addOrReplaceChild("cube_r108", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -22.9188F, -5.7378F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r109 = curves_3.addOrReplaceChild("cube_r109", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -22.2247F, -7.7825F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r110 = curves_3.addOrReplaceChild("cube_r110", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -21.0743F, -9.0886F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r111 = curves_3.addOrReplaceChild("cube_r111", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -7.7878F, -0.5374F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r112 = curves_3.addOrReplaceChild("cube_r112", CubeListBuilder.create().texOffs(203, 141).addBox(-2.0F, -0.2059F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F))
		.texOffs(203, 141).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -4.4191F, -2.6795F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r113 = curves_3.addOrReplaceChild("cube_r113", CubeListBuilder.create().texOffs(203, 156).addBox(-0.7884F, -4.325F, -0.9675F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-1.2116F, -8.2172F, -9.7384F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r114 = curves_3.addOrReplaceChild("cube_r114", CubeListBuilder.create().texOffs(203, 156).addBox(-1.5F, -2.5F, -2.0F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -6.2175F, -9.14F, -0.4363F, 0.0F, 0.0F));

		PartDefinition station_4 = stations.addOrReplaceChild("station_4", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.725F, 0.0F, -0.4F, 0.0F, 3.1416F, 0.0F));

		PartDefinition edge_4 = station_4.addOrReplaceChild("edge_4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_4 = edge_4.addOrReplaceChild("plane_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.25F, 0.0F, 0.1F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r115 = plane_4.addOrReplaceChild("cube_r115", CubeListBuilder.create().texOffs(123, 245).addBox(-8.5F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(195, 140).addBox(-13.5F, -1.924F, -1.3682F, 10.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-9.25F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-13.75F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 245).addBox(-15.0F, -6.924F, -1.3682F, 8.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.7376F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r116 = plane_4.addOrReplaceChild("cube_r116", CubeListBuilder.create().texOffs(130, 244).addBox(-13.25F, -6.7686F, -2.0035F, 17.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.2376F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r117 = plane_4.addOrReplaceChild("cube_r117", CubeListBuilder.create().texOffs(123, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 236).addBox(-24.5F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.7376F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r118 = plane_4.addOrReplaceChild("cube_r118", CubeListBuilder.create().texOffs(123, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 234).addBox(-21.75F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.9876F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r119 = plane_4.addOrReplaceChild("cube_r119", CubeListBuilder.create().texOffs(123, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.4876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r120 = plane_4.addOrReplaceChild("cube_r120", CubeListBuilder.create().texOffs(123, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r121 = plane_4.addOrReplaceChild("cube_r121", CubeListBuilder.create().texOffs(123, 200).addBox(-9.85F, 2.8815F, -1.3322F, 7.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 200).addBox(-4.35F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.7876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r122 = plane_4.addOrReplaceChild("cube_r122", CubeListBuilder.create().texOffs(122, 203).addBox(-12.1F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 203).addBox(-18.6F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r123 = plane_4.addOrReplaceChild("cube_r123", CubeListBuilder.create().texOffs(123, 206).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r124 = plane_4.addOrReplaceChild("cube_r124", CubeListBuilder.create().texOffs(123, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r125 = plane_4.addOrReplaceChild("cube_r125", CubeListBuilder.create().texOffs(123, 209).addBox(-13.5F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r126 = plane_4.addOrReplaceChild("cube_r126", CubeListBuilder.create().texOffs(123, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r127 = plane_4.addOrReplaceChild("cube_r127", CubeListBuilder.create().texOffs(57, 246).addBox(-9.1F, 3.328F, -3.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-9.1F, 3.328F, -4.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-9.1F, 3.828F, -5.0864F, 12.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition ribs4 = station_4.addOrReplaceChild("ribs4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a4 = ribs4.addOrReplaceChild("rib_a4", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition front_trim_4 = rib_a4.addOrReplaceChild("front_trim_4", CubeListBuilder.create().texOffs(195, 150).addBox(-1.3908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(195, 150).addBox(-11.7908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition curves_4 = rib_a4.addOrReplaceChild("curves_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, -0.5236F, 0.0F));

		PartDefinition cube_r128 = curves_4.addOrReplaceChild("cube_r128", CubeListBuilder.create().texOffs(230, 132).addBox(-1.5F, 3.0958F, 1.3679F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -4.3276F, -8.1936F, -2.618F, 0.0F, 0.0F));

		PartDefinition cube_r129 = curves_4.addOrReplaceChild("cube_r129", CubeListBuilder.create().texOffs(230, 181).addBox(-2.0F, 1.2139F, -6.3302F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -6.3092F, -8.4522F, -1.1345F, 0.0F, 0.0F));

		PartDefinition cube_r130 = curves_4.addOrReplaceChild("cube_r130", CubeListBuilder.create().texOffs(230, 132).addBox(-1.0F, 0.9952F, -1.7181F, 3.0F, 6.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.0F, -3.9507F, -7.2763F, -1.9635F, 0.0F, 0.0F));

		PartDefinition cube_r131 = curves_4.addOrReplaceChild("cube_r131", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -2.6418F, -4.5438F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(230, 157).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -10.5381F, -1.286F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r132 = curves_4.addOrReplaceChild("cube_r132", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -12.6693F, -3.4712F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r133 = curves_4.addOrReplaceChild("cube_r133", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -14.845F, -6.0775F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r134 = curves_4.addOrReplaceChild("cube_r134", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -17.3422F, -8.3775F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r135 = curves_4.addOrReplaceChild("cube_r135", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -19.2004F, -9.2939F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r136 = curves_4.addOrReplaceChild("cube_r136", CubeListBuilder.create().texOffs(230, 132).addBox(-2.0F, -5.2139F, -3.8302F, 4.0F, 7.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(230, 132).addBox(-2.0F, -5.2139F, -4.3302F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -23.6438F, 5.1431F, 0.2618F, 0.0F, 0.0F));

		PartDefinition cube_r137 = curves_4.addOrReplaceChild("cube_r137", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -22.4624F, 3.8253F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r138 = curves_4.addOrReplaceChild("cube_r138", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -22.5104F, 0.9231F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r139 = curves_4.addOrReplaceChild("cube_r139", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -3.455F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r140 = curves_4.addOrReplaceChild("cube_r140", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -5.7378F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r141 = curves_4.addOrReplaceChild("cube_r141", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -22.4997F, -7.7825F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r142 = curves_4.addOrReplaceChild("cube_r142", CubeListBuilder.create().texOffs(230, 155).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -21.0743F, -9.0886F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r143 = curves_4.addOrReplaceChild("cube_r143", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -7.7878F, -0.5374F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r144 = curves_4.addOrReplaceChild("cube_r144", CubeListBuilder.create().texOffs(230, 157).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 6.0F, 4.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -4.4191F, -2.6795F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r145 = curves_4.addOrReplaceChild("cube_r145", CubeListBuilder.create().texOffs(230, 132).addBox(-2.0F, 1.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -3.7294F, -5.1584F, -1.5708F, 0.0F, 0.0F));

		PartDefinition floorstrut_4 = rib_a4.addOrReplaceChild("floorstrut_4", CubeListBuilder.create().texOffs(196, 163).addBox(-6.0F, -1.25F, -3.25F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(196, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.4171F, -2.1332F, -8.5239F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_bolts_4 = ribs4.addOrReplaceChild("rib_bolts_4", CubeListBuilder.create().texOffs(163, 173).addBox(-0.55F, -3.0797F, -7.5765F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.5088F, -12.7947F, -13.8543F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r146 = rib_bolts_4.addOrReplaceChild("cube_r146", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.0F, 0.275F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r147 = rib_bolts_4.addOrReplaceChild("cube_r147", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -2.5F, -0.4F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -7.3615F, -2.8692F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r148 = rib_bolts_4.addOrReplaceChild("cube_r148", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.55F, 0.05F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.55F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r149 = rib_bolts_4.addOrReplaceChild("cube_r149", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -1.5F, -0.775F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 11.8283F, 3.4996F, -1.1083F, 0.0F, 0.0F));

		PartDefinition cube_r150 = rib_bolts_4.addOrReplaceChild("cube_r150", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.725F, 0.325F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 7.1266F, 5.6802F, -0.2356F, 0.0F, 0.0F));

		PartDefinition cube_r151 = rib_bolts_4.addOrReplaceChild("cube_r151", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r152 = rib_bolts_4.addOrReplaceChild("cube_r152", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.325F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r153 = rib_bolts_4.addOrReplaceChild("cube_r153", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.025F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition station_5 = stations.addOrReplaceChild("station_5", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.475F, 0.0F, -0.9F, 0.0F, 2.0944F, 0.0F));

		PartDefinition edge_5 = station_5.addOrReplaceChild("edge_5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition plane_5 = edge_5.addOrReplaceChild("plane_5", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.75F, 0.0F, 0.1F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r154 = plane_5.addOrReplaceChild("cube_r154", CubeListBuilder.create().texOffs(123, 245).addBox(-8.5F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.2376F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r155 = plane_5.addOrReplaceChild("cube_r155", CubeListBuilder.create().texOffs(213, 170).addBox(-7.0F, -1.924F, -1.3682F, 10.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-3.0F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-7.5F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 245).addBox(-8.25F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(1.4876F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r156 = plane_5.addOrReplaceChild("cube_r156", CubeListBuilder.create().texOffs(130, 244).addBox(-13.25F, -6.7686F, -2.0035F, 17.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.4876F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r157 = plane_5.addOrReplaceChild("cube_r157", CubeListBuilder.create().texOffs(123, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 236).addBox(-24.5F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.9876F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r158 = plane_5.addOrReplaceChild("cube_r158", CubeListBuilder.create().texOffs(123, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 234).addBox(-21.75F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.2376F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r159 = plane_5.addOrReplaceChild("cube_r159", CubeListBuilder.create().texOffs(127, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.4876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r160 = plane_5.addOrReplaceChild("cube_r160", CubeListBuilder.create().texOffs(127, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r161 = plane_5.addOrReplaceChild("cube_r161", CubeListBuilder.create().texOffs(127, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.2876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r162 = plane_5.addOrReplaceChild("cube_r162", CubeListBuilder.create().texOffs(127, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.7876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r163 = plane_5.addOrReplaceChild("cube_r163", CubeListBuilder.create().texOffs(126, 203).addBox(-12.1F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(127, 203).addBox(-18.6F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r164 = plane_5.addOrReplaceChild("cube_r164", CubeListBuilder.create().texOffs(127, 206).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r165 = plane_5.addOrReplaceChild("cube_r165", CubeListBuilder.create().texOffs(127, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r166 = plane_5.addOrReplaceChild("cube_r166", CubeListBuilder.create().texOffs(127, 209).addBox(-13.5F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r167 = plane_5.addOrReplaceChild("cube_r167", CubeListBuilder.create().texOffs(127, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r168 = plane_5.addOrReplaceChild("cube_r168", CubeListBuilder.create().texOffs(57, 246).addBox(-9.1F, 3.328F, -3.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-9.1F, 3.328F, -4.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-9.1F, 3.828F, -5.0864F, 12.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition ribs5 = station_5.addOrReplaceChild("ribs5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a5 = ribs5.addOrReplaceChild("rib_a5", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition front_trim_5 = rib_a5.addOrReplaceChild("front_trim_5", CubeListBuilder.create().texOffs(209, 150).addBox(-1.8908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(209, 150).addBox(-12.2908F, -17.1271F, -21.0677F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition floorstrut_5 = rib_a5.addOrReplaceChild("floorstrut_5", CubeListBuilder.create().texOffs(196, 163).addBox(-5.5F, -1.25F, -3.175F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(210, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.1671F, -2.1332F, -8.2739F, 0.5236F, 0.0F, 0.0F));

		PartDefinition rib_bolts_5 = ribs5.addOrReplaceChild("rib_bolts_5", CubeListBuilder.create().texOffs(163, 173).addBox(-0.6F, -3.1047F, -7.8265F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.8347F, -12.7947F, -14.0038F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r169 = rib_bolts_5.addOrReplaceChild("cube_r169", CubeListBuilder.create().texOffs(163, 173).addBox(-0.1F, 0.0F, 0.175F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r170 = rib_bolts_5.addOrReplaceChild("cube_r170", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.1F, -6.9646F, -0.7326F, -1.2785F, 0.0F, 0.0F));

		PartDefinition cube_r171 = rib_bolts_5.addOrReplaceChild("cube_r171", CubeListBuilder.create().texOffs(163, 173).addBox(-0.15F, 0.55F, 0.075F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r172 = rib_bolts_5.addOrReplaceChild("cube_r172", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.1F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r173 = rib_bolts_5.addOrReplaceChild("cube_r173", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.375F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.6F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r174 = rib_bolts_5.addOrReplaceChild("cube_r174", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.6F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition curves_5 = station_5.addOrReplaceChild("curves_5", CubeListBuilder.create(), PartPose.offsetAndRotation(-15.2554F, -11.75F, -0.35F, 0.4363F, 1.5708F, 0.0F));

		PartDefinition cube_r175 = curves_5.addOrReplaceChild("cube_r175", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -2.7168F, -4.5438F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(230, 151).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(0.0F, 6.2307F, 6.8719F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r176 = curves_5.addOrReplaceChild("cube_r176", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 4.0995F, 4.6867F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r177 = curves_5.addOrReplaceChild("cube_r177", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(0.0F, 1.9238F, 2.0804F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r178 = curves_5.addOrReplaceChild("cube_r178", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(0.0F, -0.5734F, -0.2196F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r179 = curves_5.addOrReplaceChild("cube_r179", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(0.0F, -2.4316F, -1.136F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r180 = curves_5.addOrReplaceChild("cube_r180", CubeListBuilder.create().texOffs(230, 151).addBox(0.625F, -1.622F, -5.1864F, 4.0F, 3.0F, 5.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-2.64F, -5.158F, 11.7118F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r181 = curves_5.addOrReplaceChild("cube_r181", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(0.0F, -5.4436F, 11.9832F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r182 = curves_5.addOrReplaceChild("cube_r182", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -5.5415F, 9.081F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r183 = curves_5.addOrReplaceChild("cube_r183", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(0.0F, -6.2249F, 4.7029F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r184 = curves_5.addOrReplaceChild("cube_r184", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(0.0F, -6.2249F, 2.4201F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r185 = curves_5.addOrReplaceChild("cube_r185", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(0.0F, -5.5308F, 0.3754F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r186 = curves_5.addOrReplaceChild("cube_r186", CubeListBuilder.create().texOffs(230, 150).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -4.3055F, -0.9307F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r187 = curves_5.addOrReplaceChild("cube_r187", CubeListBuilder.create().texOffs(230, 151).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(0.0F, 8.981F, 7.6205F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r188 = curves_5.addOrReplaceChild("cube_r188", CubeListBuilder.create().texOffs(230, 131).addBox(-2.0F, -0.2059F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F))
		.texOffs(230, 151).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(0.0F, 12.3498F, 5.4784F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r189 = curves_5.addOrReplaceChild("cube_r189", CubeListBuilder.create().texOffs(230, 156).addBox(-0.7884F, -4.325F, -0.9675F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.7116F, 8.5516F, -1.5805F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r190 = curves_5.addOrReplaceChild("cube_r190", CubeListBuilder.create().texOffs(230, 156).addBox(-1.5F, -2.5F, -2.0F, 3.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 10.5513F, -0.9821F, -0.4363F, 0.0F, 0.0F));

		PartDefinition station_6 = stations.addOrReplaceChild("station_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.025F, 0.0F, -0.65F, 0.0F, 1.0472F, 0.0F));

		PartDefinition edge_6 = station_6.addOrReplaceChild("edge_6", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ribs6 = station_6.addOrReplaceChild("ribs6", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_a6 = ribs6.addOrReplaceChild("rib_a6", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition front_trim_6 = rib_a6.addOrReplaceChild("front_trim_6", CubeListBuilder.create().texOffs(190, 150).addBox(-0.8908F, -17.1271F, -20.4427F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(190, 150).addBox(-12.2908F, -17.1271F, -20.4427F, 13.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition curves_6 = rib_a6.addOrReplaceChild("curves_6", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.0286F, 0.0F, 0.4335F, 0.4363F, -0.5236F, 0.0F));

		PartDefinition cube_r191 = curves_6.addOrReplaceChild("cube_r191", CubeListBuilder.create().texOffs(230, 134).addBox(-1.5F, 3.0958F, 1.3679F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -4.3276F, -8.1936F, -2.618F, 0.0F, 0.0F));

		PartDefinition cube_r192 = curves_6.addOrReplaceChild("cube_r192", CubeListBuilder.create().texOffs(230, 181).addBox(-2.0F, 1.2139F, -6.3302F, 4.0F, 4.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -6.2092F, -8.4522F, -1.1345F, 0.0F, 0.0F));

		PartDefinition cube_r193 = curves_6.addOrReplaceChild("cube_r193", CubeListBuilder.create().texOffs(230, 134).addBox(-1.0F, 0.9952F, -1.7181F, 3.0F, 6.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.0F, -3.9507F, -7.2763F, -1.9635F, 0.0F, 0.0F));

		PartDefinition cube_r194 = curves_6.addOrReplaceChild("cube_r194", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -2.6418F, -4.2938F, 4.0F, 3.0F, 7.0F, new CubeDeformation(-0.8F))
		.texOffs(230, 156).addBox(-2.0F, -5.9668F, -4.5438F, 4.0F, 5.0F, 5.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -10.5381F, -1.286F, 0.48F, 0.0F, 0.0F));

		PartDefinition cube_r195 = curves_6.addOrReplaceChild("cube_r195", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -6.924F, -2.3682F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -12.6693F, -3.4712F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r196 = curves_6.addOrReplaceChild("cube_r196", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -6.7686F, -3.0035F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -14.845F, -6.0775F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r197 = curves_6.addOrReplaceChild("cube_r197", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -6.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -17.3422F, -8.3775F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r198 = curves_6.addOrReplaceChild("cube_r198", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -5.0438F, -5.4668F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -19.2004F, -9.2939F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r199 = curves_6.addOrReplaceChild("cube_r199", CubeListBuilder.create().texOffs(230, 134).addBox(-2.0F, -5.2139F, -3.8302F, 4.0F, 7.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(230, 134).addBox(-2.0F, -5.2139F, -4.3302F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -23.6438F, 5.1431F, 0.2618F, 0.0F, 0.0F));

		PartDefinition cube_r200 = curves_6.addOrReplaceChild("cube_r200", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, 0.378F, -5.1864F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(-0.5F, -22.4624F, 3.8253F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r201 = curves_6.addOrReplaceChild("cube_r201", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, 1.8815F, -2.5822F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -22.5104F, 0.9231F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r202 = curves_6.addOrReplaceChild("cube_r202", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, 0.7686F, -3.0035F, 4.0F, 6.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -3.455F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r203 = curves_6.addOrReplaceChild("cube_r203", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, 2.217F, -4.1865F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -23.1938F, -5.7378F, -1.4399F, 0.0F, 0.0F));

		PartDefinition cube_r204 = curves_6.addOrReplaceChild("cube_r204", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, 0.8679F, -5.5958F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.72F)), PartPose.offsetAndRotation(-0.5F, -22.4997F, -7.7825F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r205 = curves_6.addOrReplaceChild("cube_r205", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -2.0F, -6.5F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -21.0743F, -9.0886F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r206 = curves_6.addOrReplaceChild("cube_r206", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -3.2941F, -6.3296F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.74F)), PartPose.offsetAndRotation(-0.5F, -7.7878F, -0.5374F, -0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r207 = curves_6.addOrReplaceChild("cube_r207", CubeListBuilder.create().texOffs(230, 156).addBox(-2.0F, -2.7059F, -6.3296F, 4.0F, 6.0F, 4.0F, new CubeDeformation(-0.73F)), PartPose.offsetAndRotation(-0.5F, -4.4191F, -2.6795F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r208 = curves_6.addOrReplaceChild("cube_r208", CubeListBuilder.create().texOffs(230, 134).addBox(-2.0F, 1.5315F, -3.6131F, 4.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, -3.7294F, -5.1584F, -1.5708F, 0.0F, 0.0F));

		PartDefinition floorstrut_6 = ribs6.addOrReplaceChild("floorstrut_6", CubeListBuilder.create().texOffs(196, 163).addBox(-6.0F, -1.25F, -3.25F, 12.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(196, 163).addBox(-5.0F, -1.75F, -0.75F, 10.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.1671F, -2.1332F, -7.4989F, 0.5236F, 0.0F, 0.0F));

		PartDefinition plane_6 = ribs6.addOrReplaceChild("plane_6", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.5F, 0.0F, 0.675F, 0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r209 = plane_6.addOrReplaceChild("cube_r209", CubeListBuilder.create().texOffs(123, 245).addBox(-8.5F, -6.924F, -1.3682F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(215, 174).addBox(-13.0F, -1.924F, -1.3682F, 10.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-9.25F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 192).addBox(-13.75F, -3.424F, -1.3682F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 245).addBox(-15.0F, -6.924F, -1.3682F, 8.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(7.7376F, -10.7447F, -1.519F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r210 = plane_6.addOrReplaceChild("cube_r210", CubeListBuilder.create().texOffs(130, 244).addBox(-13.25F, -6.7686F, -2.0035F, 17.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(4.2376F, -13.0022F, -4.2273F, 0.829F, 0.0F, 0.0F));

		PartDefinition cube_r211 = plane_6.addOrReplaceChild("cube_r211", CubeListBuilder.create().texOffs(123, 236).addBox(-15.0F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(123, 236).addBox(-24.5F, -6.5315F, -1.6131F, 11.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(13.7376F, -14.9511F, -7.3839F, 0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r212 = plane_6.addOrReplaceChild("cube_r212", CubeListBuilder.create().texOffs(123, 234).addBox(-11.25F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 234).addBox(-21.75F, -4.2938F, -2.9668F, 12.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.9876F, -18.286F, -9.3712F, 0.2182F, 0.0F, 0.0F));

		PartDefinition cube_r213 = plane_6.addOrReplaceChild("cube_r213", CubeListBuilder.create().texOffs(123, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.4876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r214 = plane_6.addOrReplaceChild("cube_r214", CubeListBuilder.create().texOffs(123, 201).addBox(-10.6F, 0.828F, -4.0864F, 7.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -22.5723F, 4.5169F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r215 = plane_6.addOrReplaceChild("cube_r215", CubeListBuilder.create().texOffs(123, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.2876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r216 = plane_6.addOrReplaceChild("cube_r216", CubeListBuilder.create().texOffs(123, 200).addBox(-10.85F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(9.7876F, -22.2014F, 3.579F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r217 = plane_6.addOrReplaceChild("cube_r217", CubeListBuilder.create().texOffs(122, 203).addBox(-12.1F, 2.8815F, -1.3322F, 10.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(122, 203).addBox(-18.6F, 2.8815F, -1.3322F, 8.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(10.0376F, -22.7425F, 1.1383F, -1.789F, 0.0F, 0.0F));

		PartDefinition cube_r218 = plane_6.addOrReplaceChild("cube_r218", CubeListBuilder.create().texOffs(123, 206).addBox(-13.5F, 1.2686F, -1.7535F, 19.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(3.9876F, -23.3795F, -2.8515F, -1.7017F, 0.0F, 0.0F));

		PartDefinition cube_r219 = plane_6.addOrReplaceChild("cube_r219", CubeListBuilder.create().texOffs(123, 206).addBox(-12.5F, -1.0F, -1.0F, 20.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.5669F, -6.4409F, -1.501F, 0.0F, 0.0F));

		PartDefinition cube_r220 = plane_6.addOrReplaceChild("cube_r220", CubeListBuilder.create().texOffs(123, 209).addBox(-13.25F, -2.0F, -1.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -24.1741F, -8.8529F, -1.3177F, 0.0F, 0.0F));

		PartDefinition cube_r221 = plane_6.addOrReplaceChild("cube_r221", CubeListBuilder.create().texOffs(123, 227).addBox(-14.0F, -1.6F, -1.15F, 22.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.4876F, -23.3091F, -10.6277F, -1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r222 = plane_6.addOrReplaceChild("cube_r222", CubeListBuilder.create().texOffs(57, 246).addBox(-9.1F, 3.328F, -3.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(57, 246).addBox(-9.1F, 3.328F, -4.0864F, 12.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(208, 165).addBox(-9.1F, 3.828F, -5.0864F, 12.0F, 2.0F, 5.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.6329F, -23.0394F, 7.8524F, -1.1781F, 0.0F, 0.0F));

		PartDefinition rib_bolts_6 = ribs6.addOrReplaceChild("rib_bolts_6", CubeListBuilder.create().texOffs(163, 173).addBox(-0.7F, -3.1047F, -7.9765F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(7.2588F, -12.7947F, -12.8543F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r223 = rib_bolts_6.addOrReplaceChild("cube_r223", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.0F, -0.075F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.7F, -10.3761F, 4.1446F, -0.7854F, 0.0F, 0.0F));

		PartDefinition cube_r224 = rib_bolts_6.addOrReplaceChild("cube_r224", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -2.5F, -0.475F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.7F, -7.3615F, -2.8692F, -1.3526F, 0.0F, 0.0F));

		PartDefinition cube_r225 = rib_bolts_6.addOrReplaceChild("cube_r225", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 0.55F, -0.175F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.7F, -6.8575F, -4.5916F, -0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r226 = rib_bolts_6.addOrReplaceChild("cube_r226", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.75F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 11.8283F, 3.4996F, -1.1083F, 0.0F, 0.0F));

		PartDefinition cube_r227 = rib_bolts_6.addOrReplaceChild("cube_r227", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 1.0F, -0.25F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.625F, 7.1266F, 5.6802F, -0.2356F, 0.0F, 0.0F));

		PartDefinition cube_r228 = rib_bolts_6.addOrReplaceChild("cube_r228", CubeListBuilder.create().texOffs(163, 173).addBox(-0.5F, -0.5F, -0.75F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.125F, 5.4722F, 5.9551F, 0.4189F, 0.0F, 0.0F));

		PartDefinition cube_r229 = rib_bolts_6.addOrReplaceChild("cube_r229", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, 2.25F, -0.375F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.625F, 2.5356F, -1.746F, 1.3352F, 0.0F, 0.0F));

		PartDefinition cube_r230 = rib_bolts_6.addOrReplaceChild("cube_r230", CubeListBuilder.create().texOffs(163, 173).addBox(0.0F, -0.5F, -0.125F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.625F, 1.7056F, -4.1175F, 1.1345F, 0.0F, 0.0F));

		PartDefinition rotor = partdefinition.addOrReplaceChild("rotor", CubeListBuilder.create(), PartPose.offset(0.0F, 13.0F, 0.0F));

		PartDefinition pump = rotor.addOrReplaceChild("pump", CubeListBuilder.create().texOffs(139, 201).addBox(-9.0F, -7.75F, 0.0F, 9.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(128, 242).addBox(-9.0F, -7.75F, -9.0F, 9.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(133, 242).addBox(0.0F, -7.75F, -9.0F, 9.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(148, 200).addBox(0.0F, -7.75F, 0.0F, 9.0F, 1.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_core = pump.addOrReplaceChild("glow_core", CubeListBuilder.create().texOffs(216, 210).addBox(-1.0F, -15.75F, -1.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(0.5F, -15.0F, -1.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-2.0F, -12.75F, -2.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-0.5F, -11.5F, 0.25F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-2.25F, -10.25F, -3.25F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-4.25F, -10.75F, -2.75F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(1.75F, -10.75F, -2.0F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-3.25F, -10.75F, 1.75F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-3.75F, -9.0F, -3.0F, 6.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(2.25F, -9.0F, -2.0F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(216, 210).addBox(-4.75F, -9.0F, -2.0F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.5F, 0.0F, -0.25F));

		PartDefinition cube_r231 = glow_core.addOrReplaceChild("cube_r231", CubeListBuilder.create().texOffs(216, 210).addBox(3.0F, -9.0F, -2.75F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition cube_r232 = glow_core.addOrReplaceChild("cube_r232", CubeListBuilder.create().texOffs(216, 210).addBox(3.0F, -9.0F, -1.25F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition sides = rotor.addOrReplaceChild("sides", CubeListBuilder.create().texOffs(139, 201).addBox(-9.0F, -9.75F, 9.0F, 18.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(139, 201).addBox(-9.0F, -9.75F, -10.0F, 18.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.55F, 0.0F));

		PartDefinition cube_r233 = sides.addOrReplaceChild("cube_r233", CubeListBuilder.create().texOffs(69, 146).addBox(3.65F, -2.15F, -1.6F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(67, 147).addBox(-3.35F, -2.15F, 3.15F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(67, 145).addBox(-3.75F, -2.65F, -6.25F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(92, 138).addBox(-2.5F, -0.65F, -2.5F, 5.0F, 3.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(192, 165).addBox(-9.0F, -1.15F, -9.0F, 18.0F, 1.0F, 18.0F, new CubeDeformation(0.0F))
		.texOffs(139, 201).addBox(-9.0F, -9.75F, -10.0F, 18.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition cube_r234 = sides.addOrReplaceChild("cube_r234", CubeListBuilder.create().texOffs(139, 201).addBox(-9.0F, -9.75F, -10.0F, 18.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_bits = sides.addOrReplaceChild("glow_bits", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r235 = glow_bits.addOrReplaceChild("cube_r235", CubeListBuilder.create().texOffs(206, 218).addBox(-3.3F, -0.65F, -5.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(206, 218).addBox(4.2F, -0.65F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(206, 218).addBox(-2.8F, -0.65F, 3.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(206, 218).addBox(-1.5F, 1.35F, -1.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition Controls = partdefinition.addOrReplaceChild("Controls", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition north = Controls.addOrReplaceChild("north", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition throttle = north.addOrReplaceChild("throttle", CubeListBuilder.create(), PartPose.offset(-2.55F, 1.35F, -1.75F));

		PartDefinition throttle_plate = throttle.addOrReplaceChild("throttle_plate", CubeListBuilder.create(), PartPose.offset(0.7376F, -20.2878F, -15.871F));

		PartDefinition cube_r236 = throttle_plate.addOrReplaceChild("cube_r236", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -0.5F, -1.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, 0.8424F, -0.9682F, -0.5629F, 0.0F, 0.0F));

		PartDefinition cube_r237 = throttle_plate.addOrReplaceChild("cube_r237", CubeListBuilder.create().texOffs(104, 143).addBox(-1.025F, -2.7136F, -0.8986F, 2.0F, 3.75F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.3913F, 0.3335F, -1.0996F, 0.0F, 0.0F));

		PartDefinition cube_r238 = throttle_plate.addOrReplaceChild("cube_r238", CubeListBuilder.create().texOffs(104, 142).addBox(-0.975F, -1.5F, -1.0F, 2.0F, 3.5F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.7F, 0.1333F, -0.213F, -0.8945F, 0.0F, 0.0F));

		PartDefinition cube_r239 = throttle_plate.addOrReplaceChild("cube_r239", CubeListBuilder.create().texOffs(64, 171).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 3.5F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, 0.0F, 0.0F, -0.9119F, 0.0F, 0.0F));

		PartDefinition cube_r240 = throttle_plate.addOrReplaceChild("cube_r240", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -2.0F, -1.25F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.9005F, 1.3936F, -1.4617F, 0.0F, 0.0F));

		PartDefinition cube_r241 = throttle_plate.addOrReplaceChild("cube_r241", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -3.175F, -1.0F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.1173F, 0.1538F, -1.069F, 0.0F, 0.0F));

		PartDefinition throttle_ring = throttle.addOrReplaceChild("throttle_ring", CubeListBuilder.create(), PartPose.offset(0.3926F, -19.7518F, -16.4664F));

		PartDefinition cube_r242 = throttle_ring.addOrReplaceChild("cube_r242", CubeListBuilder.create().texOffs(73, 137).addBox(-0.975F, -1.5F, -1.9F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(1.375F, -0.075F, 0.375F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r243 = throttle_ring.addOrReplaceChild("cube_r243", CubeListBuilder.create().texOffs(73, 137).addBox(-0.325F, -3.425F, -1.7F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.73F, -0.098F, -0.2382F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r244 = throttle_ring.addOrReplaceChild("cube_r244", CubeListBuilder.create().texOffs(73, 137).addBox(-0.95F, -3.5F, -1.575F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(1.35F, 0.5288F, 1.6437F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r245 = throttle_ring.addOrReplaceChild("cube_r245", CubeListBuilder.create().texOffs(73, 137).addBox(-0.975F, -1.5F, -1.9F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(0.7F, -0.075F, 0.375F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r246 = throttle_ring.addOrReplaceChild("cube_r246", CubeListBuilder.create().texOffs(73, 137).addBox(-0.325F, -3.425F, -1.7F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.055F, -0.098F, -0.2382F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r247 = throttle_ring.addOrReplaceChild("cube_r247", CubeListBuilder.create().texOffs(73, 137).addBox(-0.95F, -3.5F, -1.575F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(0.675F, 0.5288F, 1.6437F, -0.2269F, 0.0F, 0.0F));

		PartDefinition throttle_rotate_x = throttle.addOrReplaceChild("throttle_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(1.4226F, -20.3425F, -15.5303F, 1.3963F, 0.0F, 0.0F));

		PartDefinition cube_r248 = throttle_rotate_x.addOrReplaceChild("cube_r248", CubeListBuilder.create().texOffs(40, 178).addBox(-2.225F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(40, 178).addBox(-1.175F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(40, 178).addBox(-0.125F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(73, 174).addBox(-2.65F, -0.9F, -0.8625F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(73, 174).addBox(-1.4F, -0.9F, -0.8625F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-1.485F, -2.3383F, -1.2712F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r249 = throttle_rotate_x.addOrReplaceChild("cube_r249", CubeListBuilder.create().texOffs(68, 141).addBox(-0.35F, -0.75F, -1.8625F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.61F, -1.6228F, -0.7651F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r250 = throttle_rotate_x.addOrReplaceChild("cube_r250", CubeListBuilder.create().texOffs(68, 141).addBox(-0.35F, -1.0F, -1.8625F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.61F, -1.5963F, -0.8075F, -1.0123F, 0.0F, 0.0F));

		PartDefinition handbreak = north.addOrReplaceChild("handbreak", CubeListBuilder.create(), PartPose.offset(-0.9F, 1.35F, -1.75F));

		PartDefinition handbreak_plate = handbreak.addOrReplaceChild("handbreak_plate", CubeListBuilder.create(), PartPose.offset(0.7376F, -20.2878F, -15.871F));

		PartDefinition cube_r251 = handbreak_plate.addOrReplaceChild("cube_r251", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -0.5F, -1.2F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, 0.8424F, -0.9682F, -0.5629F, 0.0F, 0.0F));

		PartDefinition cube_r252 = handbreak_plate.addOrReplaceChild("cube_r252", CubeListBuilder.create().texOffs(104, 143).addBox(-1.025F, -2.7136F, -0.8986F, 2.0F, 3.75F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.3913F, 0.3335F, -1.0996F, 0.0F, 0.0F));

		PartDefinition cube_r253 = handbreak_plate.addOrReplaceChild("cube_r253", CubeListBuilder.create().texOffs(104, 142).addBox(-0.975F, -1.5F, -1.0F, 2.0F, 3.5F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.7F, 0.1333F, -0.213F, -0.8945F, 0.0F, 0.0F));

		PartDefinition cube_r254 = handbreak_plate.addOrReplaceChild("cube_r254", CubeListBuilder.create().texOffs(64, 171).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 3.5F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, 0.0F, 0.0F, -0.9119F, 0.0F, 0.0F));

		PartDefinition cube_r255 = handbreak_plate.addOrReplaceChild("cube_r255", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -2.0F, -1.25F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.9005F, 1.3936F, -1.4617F, 0.0F, 0.0F));

		PartDefinition cube_r256 = handbreak_plate.addOrReplaceChild("cube_r256", CubeListBuilder.create().texOffs(65, 172).addBox(-1.5F, -3.175F, -1.0F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.75F, -0.1173F, 0.1538F, -1.069F, 0.0F, 0.0F));

		PartDefinition handbreak_ring = handbreak.addOrReplaceChild("handbreak_ring", CubeListBuilder.create(), PartPose.offset(0.3926F, -19.7518F, -16.4664F));

		PartDefinition cube_r257 = handbreak_ring.addOrReplaceChild("cube_r257", CubeListBuilder.create().texOffs(67, 139).addBox(-0.975F, -1.5F, -1.9F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(1.375F, -0.075F, 0.375F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r258 = handbreak_ring.addOrReplaceChild("cube_r258", CubeListBuilder.create().texOffs(67, 139).addBox(-0.325F, -3.425F, -1.7F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.73F, -0.098F, -0.2382F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r259 = handbreak_ring.addOrReplaceChild("cube_r259", CubeListBuilder.create().texOffs(67, 139).addBox(-0.95F, -3.5F, -1.575F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(1.35F, 0.5288F, 1.6437F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r260 = handbreak_ring.addOrReplaceChild("cube_r260", CubeListBuilder.create().texOffs(67, 139).addBox(-0.975F, -1.5F, -1.9F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(0.7F, -0.075F, 0.375F, -0.2269F, 0.0F, 0.0F));

		PartDefinition cube_r261 = handbreak_ring.addOrReplaceChild("cube_r261", CubeListBuilder.create().texOffs(67, 139).addBox(-0.325F, -3.425F, -1.7F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.055F, -0.098F, -0.2382F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r262 = handbreak_ring.addOrReplaceChild("cube_r262", CubeListBuilder.create().texOffs(67, 139).addBox(-0.95F, -3.5F, -1.575F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(0.675F, 0.5288F, 1.6437F, -0.2269F, 0.0F, 0.0F));

		PartDefinition handbreak_rotate_x = handbreak.addOrReplaceChild("handbreak_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(1.4476F, -20.3425F, -15.5303F, 1.3963F, 0.0F, 0.0F));

		PartDefinition cube_r263 = handbreak_rotate_x.addOrReplaceChild("cube_r263", CubeListBuilder.create().texOffs(33, 166).addBox(3.875F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(33, 166).addBox(4.925F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(33, 166).addBox(2.825F, -0.9F, -0.8875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(73, 174).addBox(4.375F, -0.9F, -0.8625F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(73, 174).addBox(2.1F, -0.9F, -0.8625F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-3.16F, -2.3383F, -1.2712F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r264 = handbreak_rotate_x.addOrReplaceChild("cube_r264", CubeListBuilder.create().texOffs(62, 137).addBox(-0.35F, -0.75F, -1.8625F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.635F, -1.6228F, -0.7651F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r265 = handbreak_rotate_x.addOrReplaceChild("cube_r265", CubeListBuilder.create().texOffs(62, 137).addBox(-0.35F, -1.0F, -1.8625F, 2.0F, 2.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.635F, -1.5963F, -0.8075F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dummy_a1 = north.addOrReplaceChild("dummy_a1", CubeListBuilder.create(), PartPose.offset(0.25F, -1.75F, 5.0F));

		PartDefinition cube_r266 = dummy_a1.addOrReplaceChild("cube_r266", CubeListBuilder.create().texOffs(101, 142).addBox(-0.375F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(33, 199).addBox(-0.375F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-0.2374F, -19.9844F, -16.9655F, -0.4014F, 0.0F, 0.0F));

		PartDefinition dummy_b1 = north.addOrReplaceChild("dummy_b1", CubeListBuilder.create(), PartPose.offset(-3.95F, 0.375F, -0.175F));

		PartDefinition button_pad_a1 = dummy_b1.addOrReplaceChild("button_pad_a1", CubeListBuilder.create(), PartPose.offset(-1.3374F, -19.9844F, -16.9655F));

		PartDefinition cube_r267 = button_pad_a1.addOrReplaceChild("cube_r267", CubeListBuilder.create().texOffs(28, 209).addBox(-1.75F, -2.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -2.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -1.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -1.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -1.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -1.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -0.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -0.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 233).addBox(-1.75F, -0.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 233).addBox(-1.0F, -0.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-1.875F, -3.0F, -0.5F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition button_pad_d1 = dummy_b1.addOrReplaceChild("button_pad_d1", CubeListBuilder.create(), PartPose.offset(9.4126F, -19.9844F, -16.9655F));

		PartDefinition cube_r268 = button_pad_d1.addOrReplaceChild("cube_r268", CubeListBuilder.create().texOffs(28, 209).addBox(-1.75F, -2.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -2.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -2.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -1.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -1.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -1.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -1.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.0F, -0.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(28, 209).addBox(-1.75F, -0.75F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 233).addBox(-1.75F, -0.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 233).addBox(-1.0F, -0.25F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-1.875F, -3.0F, -0.5F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dummy_c1 = north.addOrReplaceChild("dummy_c1", CubeListBuilder.create(), PartPose.offset(-2.725F, 0.25F, -0.25F));

		PartDefinition dial_pad_a1 = dummy_c1.addOrReplaceChild("dial_pad_a1", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9844F, -16.9655F));

		PartDefinition cube_r269 = dial_pad_a1.addOrReplaceChild("cube_r269", CubeListBuilder.create().texOffs(65, 172).addBox(-1.875F, -3.0F, -0.35F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_a1 = dial_pad_a1.addOrReplaceChild("dial_a1", CubeListBuilder.create(), PartPose.offset(-0.3679F, -0.3896F, 1.2279F));

		PartDefinition cube_r270 = dial_a1.addOrReplaceChild("cube_r270", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r271 = dial_a1.addOrReplaceChild("cube_r271", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -1.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b1 = dial_pad_a1.addOrReplaceChild("dial_b1", CubeListBuilder.create(), PartPose.offset(-0.3679F, 0.3604F, -0.2721F));

		PartDefinition cube_r272 = dial_b1.addOrReplaceChild("cube_r272", CubeListBuilder.create().texOffs(61, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r273 = dial_b1.addOrReplaceChild("cube_r273", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dummy_c2 = north.addOrReplaceChild("dummy_c2", CubeListBuilder.create(), PartPose.offset(3.375F, 0.25F, -0.25F));

		PartDefinition dial_pad_a2 = dummy_c2.addOrReplaceChild("dial_pad_a2", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9844F, -16.9655F));

		PartDefinition cube_r274 = dial_pad_a2.addOrReplaceChild("cube_r274", CubeListBuilder.create().texOffs(65, 172).addBox(-1.875F, -3.0F, -0.35F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_a2 = dial_pad_a2.addOrReplaceChild("dial_a2", CubeListBuilder.create(), PartPose.offset(-0.3679F, -0.3896F, 1.2279F));

		PartDefinition cube_r275 = dial_a2.addOrReplaceChild("cube_r275", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r276 = dial_a2.addOrReplaceChild("cube_r276", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -1.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b2 = dial_pad_a2.addOrReplaceChild("dial_b2", CubeListBuilder.create(), PartPose.offset(-0.3679F, 0.3604F, -0.2721F));

		PartDefinition cube_r277 = dial_b2.addOrReplaceChild("cube_r277", CubeListBuilder.create().texOffs(61, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r278 = dial_b2.addOrReplaceChild("cube_r278", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.55F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition deco_lamps_1 = north.addOrReplaceChild("deco_lamps_1", CubeListBuilder.create(), PartPose.offset(-1.5F, -1.75F, 5.0F));

		PartDefinition lamp_a1 = deco_lamps_1.addOrReplaceChild("lamp_a1", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.5332F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r279 = lamp_a1.addOrReplaceChild("cube_r279", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r280 = lamp_a1.addOrReplaceChild("cube_r280", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_a1 = lamp_a1.addOrReplaceChild("glow_a1", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r281 = glow_a1.addOrReplaceChild("cube_r281", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition lamp_b1 = deco_lamps_1.addOrReplaceChild("lamp_b1", CubeListBuilder.create(), PartPose.offsetAndRotation(5.2918F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r282 = lamp_b1.addOrReplaceChild("cube_r282", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r283 = lamp_b1.addOrReplaceChild("cube_r283", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_b1 = lamp_b1.addOrReplaceChild("glow_b1", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r284 = glow_b1.addOrReplaceChild("cube_r284", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition north_west = Controls.addOrReplaceChild("north_west", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition dummy_c3 = north_west.addOrReplaceChild("dummy_c3", CubeListBuilder.create(), PartPose.offset(3.375F, 0.925F, -1.1F));

		PartDefinition dial_pad_a3 = dummy_c3.addOrReplaceChild("dial_pad_a3", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9844F, -16.9655F));

		PartDefinition cube_r285 = dial_pad_a3.addOrReplaceChild("cube_r285", CubeListBuilder.create().texOffs(65, 172).addBox(-1.875F, -1.0F, -0.15F, 6.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.9076F, 0.0F, 0.0F));

		PartDefinition dial_a3 = dial_pad_a3.addOrReplaceChild("dial_a3", CubeListBuilder.create(), PartPose.offset(-0.3679F, 0.8104F, -0.0721F));

		PartDefinition cube_r286 = dial_a3.addOrReplaceChild("cube_r286", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r287 = dial_a3.addOrReplaceChild("cube_r287", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -1.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b3 = dial_pad_a3.addOrReplaceChild("dial_b3", CubeListBuilder.create(), PartPose.offset(1.1321F, 0.6854F, -0.1721F));

		PartDefinition cube_r288 = dial_b3.addOrReplaceChild("cube_r288", CubeListBuilder.create().texOffs(61, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r289 = dial_b3.addOrReplaceChild("cube_r289", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.425F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b4 = dial_pad_a3.addOrReplaceChild("dial_b4", CubeListBuilder.create(), PartPose.offset(2.5321F, 0.6854F, -0.1721F));

		PartDefinition cube_r290 = dial_b4.addOrReplaceChild("cube_r290", CubeListBuilder.create().texOffs(61, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r291 = dial_b4.addOrReplaceChild("cube_r291", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.425F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dummy_b2 = north_west.addOrReplaceChild("dummy_b2", CubeListBuilder.create(), PartPose.offset(-3.95F, 0.375F, -0.175F));

		PartDefinition button_pad_a2 = dummy_b2.addOrReplaceChild("button_pad_a2", CubeListBuilder.create(), PartPose.offset(-1.3374F, -19.9844F, -16.9655F));

		PartDefinition cube_r292 = button_pad_a2.addOrReplaceChild("cube_r292", CubeListBuilder.create().texOffs(37, 170).addBox(-1.1F, 0.825F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(37, 170).addBox(-1.6F, 0.825F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(37, 170).addBox(-2.1F, 0.825F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(37, 170).addBox(-2.6F, 0.825F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 236).addBox(-1.1F, 0.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 236).addBox(-1.6F, 0.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 236).addBox(-2.1F, 0.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 236).addBox(-2.6F, 0.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 208).addBox(-1.1F, -0.2F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 208).addBox(-1.6F, -0.2F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 208).addBox(-2.1F, -0.2F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(33, 208).addBox(-2.6F, -0.2F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(74, 205).addBox(-2.6F, -0.75F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(74, 205).addBox(-2.1F, -0.75F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(74, 205).addBox(-1.6F, -0.75F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(74, 205).addBox(-1.1F, -0.75F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-2.875F, -1.0F, -0.425F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition dummy_a2 = north_west.addOrReplaceChild("dummy_a2", CubeListBuilder.create(), PartPose.offset(0.25F, -1.75F, 5.0F));

		PartDefinition readout_a1 = dummy_a2.addOrReplaceChild("readout_a1", CubeListBuilder.create(), PartPose.offset(-3.7374F, -19.2344F, -20.4655F));

		PartDefinition cube_r293 = readout_a1.addOrReplaceChild("cube_r293", CubeListBuilder.create().texOffs(101, 142).addBox(-9.125F, -3.25F, 0.275F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(6.0F, 0.0F, 0.0F, -1.2305F, 0.0F, 0.0F));

		PartDefinition cube_r294 = readout_a1.addOrReplaceChild("cube_r294", CubeListBuilder.create().texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition readout_b1 = dummy_a2.addOrReplaceChild("readout_b1", CubeListBuilder.create(), PartPose.offset(2.2626F, -19.2344F, -20.4655F));

		PartDefinition cube_r295 = readout_b1.addOrReplaceChild("cube_r295", CubeListBuilder.create().texOffs(101, 142).addBox(-0.375F, -3.25F, 0.275F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.2305F, 0.0F, 0.0F));

		PartDefinition cube_r296 = readout_b1.addOrReplaceChild("cube_r296", CubeListBuilder.create().texOffs(101, 142).addBox(-0.375F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(33, 199).addBox(-0.375F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition sonic_port = north_west.addOrReplaceChild("sonic_port", CubeListBuilder.create(), PartPose.offset(0.0F, 0.025F, 0.0F));

		PartDefinition hex_port = sonic_port.addOrReplaceChild("hex_port", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.5124F, -19.022F, -16.346F, -2.5482F, 0.0F, 0.0F));

		PartDefinition cube_r297 = hex_port.addOrReplaceChild("cube_r297", CubeListBuilder.create().texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.4539F, -0.0125F, 0.2621F, -1.5708F, 1.0472F, 0.0F));

		PartDefinition cube_r298 = hex_port.addOrReplaceChild("cube_r298", CubeListBuilder.create().texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.4539F, 0.0125F, -0.2621F, -1.5708F, 2.0944F, 0.0F));

		PartDefinition cube_r299 = hex_port.addOrReplaceChild("cube_r299", CubeListBuilder.create().texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.0F, -0.0125F, -0.5242F, -1.5708F, 3.1416F, 0.0F));

		PartDefinition cube_r300 = hex_port.addOrReplaceChild("cube_r300", CubeListBuilder.create().texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-0.4539F, 0.0125F, -0.2621F, -1.5708F, -2.0944F, 0.0F));

		PartDefinition cube_r301 = hex_port.addOrReplaceChild("cube_r301", CubeListBuilder.create().texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-0.4539F, -0.0125F, 0.2621F, -1.5708F, -1.0472F, 0.0F));

		PartDefinition cube_r302 = hex_port.addOrReplaceChild("cube_r302", CubeListBuilder.create().texOffs(94, 142).addBox(-2.05F, -0.975F, -1.7F, 4.0F, 3.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(65, 172).addBox(-1.5F, -1.225F, -1.675F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(65, 172).addBox(-1.5F, 0.3F, -1.675F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(66, 173).addBox(-1.5F, 0.5F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.0F, 0.0125F, 0.5242F, -1.5708F, 0.0F, 0.0F));

		PartDefinition sonic_trim = hex_port.addOrReplaceChild("sonic_trim", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0125F, 0.5242F));

		PartDefinition cube_r303 = sonic_trim.addOrReplaceChild("cube_r303", CubeListBuilder.create().texOffs(62, 176).addBox(0.075F, -1.025F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(0.075F, 0.0F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(0.25F, -0.675F, -1.625F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(-2.075F, -1.025F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(-2.075F, 0.0F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(-2.275F, -0.675F, -1.625F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r304 = sonic_trim.addOrReplaceChild("cube_r304", CubeListBuilder.create().texOffs(62, 176).addBox(0.65F, -0.675F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(-2.675F, -0.675F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.025F, -0.2F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r305 = sonic_trim.addOrReplaceChild("cube_r305", CubeListBuilder.create().texOffs(62, 176).addBox(0.25F, -0.275F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(62, 176).addBox(-2.275F, -0.275F, -1.65F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.025F, 0.0F, -1.5708F, 0.0F, 0.0F));

		PartDefinition dim_select = north_west.addOrReplaceChild("dim_select", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clock_dim = dim_select.addOrReplaceChild("clock_dim", CubeListBuilder.create(), PartPose.offset(0.0126F, -21.5748F, -11.0677F));

		PartDefinition hand_rotate_z = clock_dim.addOrReplaceChild("hand_rotate_z", CubeListBuilder.create(), PartPose.offset(-0.5022F, -2.5654F, -1.961F));

		PartDefinition cube_r306 = hand_rotate_z.addOrReplaceChild("cube_r306", CubeListBuilder.create().texOffs(37, 235).addBox(-0.9834F, -0.3F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(-0.017F, -0.385F, 0.71F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r307 = hand_rotate_z.addOrReplaceChild("cube_r307", CubeListBuilder.create().texOffs(37, 235).addBox(-0.9434F, -0.325F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.93F)), PartPose.offsetAndRotation(-0.057F, -0.545F, 0.665F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r308 = hand_rotate_z.addOrReplaceChild("cube_r308", CubeListBuilder.create().texOffs(37, 235).addBox(-0.9234F, -0.35F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.94F)), PartPose.offsetAndRotation(-0.077F, -0.675F, 0.63F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r309 = hand_rotate_z.addOrReplaceChild("cube_r309", CubeListBuilder.create().texOffs(37, 235).addBox(-1.0F, -0.275F, -0.575F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0013F, -0.62F, 0.745F, -1.5708F, 0.0F, 0.0F));

		PartDefinition clock_face = clock_dim.addOrReplaceChild("clock_face", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r310 = clock_face.addOrReplaceChild("cube_r310", CubeListBuilder.create().texOffs(103, 145).addBox(-1.0F, -0.225F, -0.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-0.5F, -2.9739F, -1.2121F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r311 = clock_face.addOrReplaceChild("cube_r311", CubeListBuilder.create().texOffs(34, 199).addBox(-1.05F, -0.25F, -3.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.95F, 0.5235F, -0.0938F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r312 = clock_face.addOrReplaceChild("cube_r312", CubeListBuilder.create().texOffs(34, 199).addBox(-0.6F, -0.25F, -3.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.05F, -0.1265F, -0.0938F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r313 = clock_face.addOrReplaceChild("cube_r313", CubeListBuilder.create().texOffs(34, 199).addBox(-1.2F, -0.25F, -5.0F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.8F, 0.5735F, -0.0938F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r314 = clock_face.addOrReplaceChild("cube_r314", CubeListBuilder.create().texOffs(34, 199).addBox(-1.5F, -0.25F, -4.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.5F, 0.4235F, 0.0562F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r315 = clock_face.addOrReplaceChild("cube_r315", CubeListBuilder.create().texOffs(34, 199).addBox(-2.35F, -0.25F, -3.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -0.1265F, -0.0938F, -1.5708F, 0.0F, 0.0F));

		PartDefinition clock_frame = clock_dim.addOrReplaceChild("clock_frame", CubeListBuilder.create(), PartPose.offset(-0.5F, -1.0765F, 0.0562F));

		PartDefinition cube_r316 = clock_frame.addOrReplaceChild("cube_r316", CubeListBuilder.create().texOffs(216, 157).addBox(-1.5F, -1.5F, -5.0F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(-2.0F, -1.5F, -4.5F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(-2.0F, -1.5F, -4.0F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(0.7F, 1.225F, -0.975F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(0.7F, -1.65F, -1.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(-2.7F, -1.65F, -1.2F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(-2.7F, 1.225F, -0.975F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(216, 157).addBox(-1.5F, -0.75F, -2.5F, 3.0F, 3.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 1.5F, 0.0F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r317 = clock_frame.addOrReplaceChild("cube_r317", CubeListBuilder.create().texOffs(216, 157).addBox(-2.0F, -1.5F, -5.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.05F, 1.5375F, -0.06F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r318 = clock_frame.addOrReplaceChild("cube_r318", CubeListBuilder.create().texOffs(216, 157).addBox(-1.025F, -0.25F, -1.375F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 1.5F, 0.0F, -1.5708F, 0.0188F, -0.567F));

		PartDefinition cube_r319 = clock_frame.addOrReplaceChild("cube_r319", CubeListBuilder.create().texOffs(216, 157).addBox(-2.0F, -0.25F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 1.5F, 0.0F, -1.5708F, -0.0175F, 0.5233F));

		PartDefinition cube_r320 = clock_frame.addOrReplaceChild("cube_r320", CubeListBuilder.create().texOffs(216, 157).addBox(0.0F, -1.5F, -5.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.05F, 1.5375F, -0.06F, -1.5708F, 0.0F, 0.0F));

		PartDefinition dim_base = dim_select.addOrReplaceChild("dim_base", CubeListBuilder.create(), PartPose.offset(-0.4874F, -21.1513F, -11.0116F));

		PartDefinition cube_r321 = dim_base.addOrReplaceChild("cube_r321", CubeListBuilder.create().texOffs(65, 172).addBox(-2.5F, -2.0F, -1.0F, 5.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.4923F, 0.0F, 0.0F));

		PartDefinition figure_1 = dim_base.addOrReplaceChild("figure_1", CubeListBuilder.create(), PartPose.offset(-0.175F, 0.0F, 0.0F));

		PartDefinition cube_r322 = figure_1.addOrReplaceChild("cube_r322", CubeListBuilder.create().texOffs(65, 172).addBox(0.325F, 0.3F, -2.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.4923F, 0.0F, 0.0F));

		PartDefinition cube_r323 = figure_1.addOrReplaceChild("cube_r323", CubeListBuilder.create().texOffs(65, 172).addBox(-1.325F, -1.875F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-1.325F, -1.875F, -1.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-0.775F, -1.875F, -2.125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-0.775F, -1.875F, -1.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-0.775F, -1.875F, -1.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-1.325F, -1.875F, -1.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(1.125F, -0.9162F, -2.3792F, -1.7626F, -0.0356F, 0.47F));

		PartDefinition cube_r324 = figure_1.addOrReplaceChild("cube_r324", CubeListBuilder.create().texOffs(65, 172).addBox(-0.1F, 0.3F, -2.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(0.0F, 0.3F, -2.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.225F, 0.0F, 0.0F, -1.4923F, 0.0F, 0.0F));

		PartDefinition cube_r325 = figure_1.addOrReplaceChild("cube_r325", CubeListBuilder.create().texOffs(65, 172).addBox(-0.75F, -1.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.86F))
		.texOffs(65, 172).addBox(-0.475F, -1.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.86F))
		.texOffs(65, 172).addBox(-0.5F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.88F)), PartPose.offsetAndRotation(0.84F, -1.194F, -1.398F, -2.0988F, 0.0F, 0.0F));

		PartDefinition cube_r326 = figure_1.addOrReplaceChild("cube_r326", CubeListBuilder.create().texOffs(65, 172).addBox(-0.925F, -1.2F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.88F)), PartPose.offsetAndRotation(1.115F, -0.1142F, -1.6804F, -1.8719F, 0.0F, 0.0F));

		PartDefinition cube_r327 = figure_1.addOrReplaceChild("cube_r327", CubeListBuilder.create().texOffs(65, 172).addBox(-1.225F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.88F)), PartPose.offsetAndRotation(1.365F, -0.1142F, -1.6804F, -2.0734F, 0.169F, 0.2969F));

		PartDefinition cube_r328 = figure_1.addOrReplaceChild("cube_r328", CubeListBuilder.create().texOffs(65, 172).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.88F)), PartPose.offsetAndRotation(1.1F, -1.194F, -1.398F, -2.0988F, 0.0F, 0.0F));

		PartDefinition figure_2 = dim_base.addOrReplaceChild("figure_2", CubeListBuilder.create(), PartPose.offset(0.1998F, 0.0F, 0.0F));

		PartDefinition cube_r329 = figure_2.addOrReplaceChild("cube_r329", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-2.325F, 0.3F, -2.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.4923F, 0.0F, 0.0F));

		PartDefinition cube_r330 = figure_2.addOrReplaceChild("cube_r330", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-0.675F, -1.875F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-0.675F, -1.875F, -1.2F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-1.225F, -1.875F, -2.125F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-1.225F, -1.875F, -1.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-1.225F, -1.875F, -1.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-0.675F, -1.875F, -1.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false), PartPose.offsetAndRotation(-1.125F, -0.9162F, -2.3792F, -1.7626F, 0.0356F, -0.47F));

		PartDefinition cube_r331 = figure_2.addOrReplaceChild("cube_r331", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-1.9F, 0.3F, -2.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-2.0F, 0.3F, -2.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)).mirror(false), PartPose.offsetAndRotation(-0.225F, 0.0F, 0.0F, -1.4923F, 0.0F, 0.0F));

		PartDefinition cube_r332 = figure_2.addOrReplaceChild("cube_r332", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-1.25F, -1.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.86F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-1.525F, -1.3F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.86F)).mirror(false)
		.texOffs(65, 172).mirror().addBox(-1.5F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.88F)).mirror(false), PartPose.offsetAndRotation(-0.84F, -1.194F, -1.398F, -2.0988F, 0.0F, 0.0F));

		PartDefinition cube_r333 = figure_2.addOrReplaceChild("cube_r333", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-1.075F, -1.2F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.88F)).mirror(false), PartPose.offsetAndRotation(-1.115F, -0.1142F, -1.6804F, -1.8719F, 0.0F, 0.0F));

		PartDefinition cube_r334 = figure_2.addOrReplaceChild("cube_r334", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-0.775F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.88F)).mirror(false), PartPose.offsetAndRotation(-1.365F, -0.1142F, -1.6804F, -2.0734F, -0.169F, -0.2969F));

		PartDefinition cube_r335 = figure_2.addOrReplaceChild("cube_r335", CubeListBuilder.create().texOffs(65, 172).mirror().addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.88F)).mirror(false), PartPose.offsetAndRotation(-1.1F, -1.194F, -1.398F, -2.0988F, 0.0F, 0.0F));

		PartDefinition south_west = Controls.addOrReplaceChild("south_west", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition telepathics = south_west.addOrReplaceChild("telepathics", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_bit = telepathics.addOrReplaceChild("glow_bit", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9844F, -16.9655F));

		PartDefinition cube_r336 = glow_bit.addOrReplaceChild("cube_r336", CubeListBuilder.create().texOffs(64, 203).addBox(-2.475F, -4.2F, -0.275F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 203).addBox(-1.975F, -3.8F, -0.275F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 203).addBox(-1.975F, -6.1F, -0.275F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 203).addBox(-2.475F, -5.7F, -0.275F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.5F, 0.3922F, -0.3445F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r337 = glow_bit.addOrReplaceChild("cube_r337", CubeListBuilder.create().texOffs(99, 175).addBox(-2.025F, -2.025F, -1.15F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.7068F, -0.9899F, 3.4241F, -0.8617F, -0.62F, -0.4624F));

		PartDefinition cube_r338 = glow_bit.addOrReplaceChild("cube_r338", CubeListBuilder.create().texOffs(64, 203).addBox(-3.5F, -4.95F, -0.275F, 7.0F, 3.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r339 = glow_bit.addOrReplaceChild("cube_r339", CubeListBuilder.create().texOffs(124, 173).addBox(0.925F, -1.65F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(124, 173).addBox(0.05F, -1.65F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(124, 173).addBox(-0.875F, -1.65F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(124, 173).addBox(-1.725F, -1.65F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(124, 173).addBox(-2.575F, -1.65F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.15F, -0.7366F, 0.8958F, -1.0123F, 0.0F, 0.0F));

		PartDefinition frame = telepathics.addOrReplaceChild("frame", CubeListBuilder.create(), PartPose.offset(-0.3874F, -20.721F, -16.0697F));

		PartDefinition cube_r340 = frame.addOrReplaceChild("cube_r340", CubeListBuilder.create().texOffs(65, 172).addBox(1.275F, -3.65F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(1.275F, -5.55F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-3.325F, -5.55F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-3.325F, -3.65F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.2F, 0.7737F, -1.0495F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r341 = frame.addOrReplaceChild("cube_r341", CubeListBuilder.create().texOffs(65, 172).addBox(-2.825F, -2.45F, 0.0F, 6.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r342 = frame.addOrReplaceChild("cube_r342", CubeListBuilder.create().texOffs(201, 178).addBox(-2.825F, -6.2F, 0.4F, 6.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.825F, -6.45F, 0.2F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.825F, -6.45F, 0.0F, 6.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, 0.6889F, -1.1025F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r343 = frame.addOrReplaceChild("cube_r343", CubeListBuilder.create().texOffs(65, 172).addBox(-3.5F, -4.95F, 0.0F, 7.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.15F, 0.7366F, -0.8958F, -1.0123F, 0.0F, 0.0F));

		PartDefinition fast_return = south_west.addOrReplaceChild("fast_return", CubeListBuilder.create(), PartPose.offset(-3.5F, 0.5F, 0.0F));

		PartDefinition cube_r344 = fast_return.addOrReplaceChild("cube_r344", CubeListBuilder.create().texOffs(58, 143).addBox(-3.0F, -1.0F, -0.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.65F))
		.texOffs(65, 172).addBox(-3.0F, -1.0F, -0.125F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(-0.2374F, -19.9844F, -16.9655F, -1.0123F, 0.0F, 0.0F));

		PartDefinition insert = fast_return.addOrReplaceChild("insert", CubeListBuilder.create(), PartPose.offset(3.5F, -0.5F, 0.0F));

		PartDefinition cube_r345 = insert.addOrReplaceChild("cube_r345", CubeListBuilder.create().texOffs(136, 244).addBox(-3.0F, -1.0F, -1.15F, 4.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(30, 237).addBox(-3.0F, -1.0F, -1.4F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-3.7374F, -19.4844F, -16.9655F, -1.0123F, 0.0F, 0.0F));

		PartDefinition stablizers = south_west.addOrReplaceChild("stablizers", CubeListBuilder.create(), PartPose.offset(4.5126F, -18.9014F, -16.6012F));

		PartDefinition cube_r346 = stablizers.addOrReplaceChild("cube_r346", CubeListBuilder.create().texOffs(58, 143).addBox(-3.0F, -1.0F, -0.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.65F))
		.texOffs(65, 172).addBox(-3.0F, -1.0F, -0.125F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(1.0F, -0.583F, -0.3643F, -1.0123F, 0.0F, 0.0F));

		PartDefinition insert2 = stablizers.addOrReplaceChild("insert2", CubeListBuilder.create(), PartPose.offset(4.7374F, 18.9014F, 16.6012F));

		PartDefinition cube_r347 = insert2.addOrReplaceChild("cube_r347", CubeListBuilder.create().texOffs(136, 244).addBox(-3.0F, -1.0F, -1.15F, 4.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(67, 238).addBox(-3.0F, -1.0F, -1.4F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-3.7374F, -19.4844F, -16.9655F, -1.0123F, 0.0F, 0.0F));

		PartDefinition deco_lamps_2 = south_west.addOrReplaceChild("deco_lamps_2", CubeListBuilder.create(), PartPose.offset(-1.5F, -1.75F, 5.0F));

		PartDefinition lamp_a2 = deco_lamps_2.addOrReplaceChild("lamp_a2", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.8832F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r348 = lamp_a2.addOrReplaceChild("cube_r348", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r349 = lamp_a2.addOrReplaceChild("cube_r349", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_a2 = lamp_a2.addOrReplaceChild("glow_a2", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r350 = glow_a2.addOrReplaceChild("cube_r350", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition lamp_b2 = deco_lamps_2.addOrReplaceChild("lamp_b2", CubeListBuilder.create(), PartPose.offsetAndRotation(5.8418F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r351 = lamp_b2.addOrReplaceChild("cube_r351", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r352 = lamp_b2.addOrReplaceChild("cube_r352", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_b2 = lamp_b2.addOrReplaceChild("glow_b2", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r353 = glow_b2.addOrReplaceChild("cube_r353", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition dummy_a3 = south_west.addOrReplaceChild("dummy_a3", CubeListBuilder.create(), PartPose.offset(0.25F, -1.75F, 5.0F));

		PartDefinition readout_a3 = dummy_a3.addOrReplaceChild("readout_a3", CubeListBuilder.create(), PartPose.offset(-0.9874F, -20.9844F, -15.7155F));

		PartDefinition cube_r354 = readout_a3.addOrReplaceChild("cube_r354", CubeListBuilder.create().texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition readout_a2 = dummy_a3.addOrReplaceChild("readout_a2", CubeListBuilder.create(), PartPose.offset(3.5126F, -20.9844F, -15.7155F));

		PartDefinition cube_r355 = readout_a2.addOrReplaceChild("cube_r355", CubeListBuilder.create().texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition readout_a4 = dummy_a3.addOrReplaceChild("readout_a4", CubeListBuilder.create(), PartPose.offset(1.2626F, -17.7344F, -22.7155F));

		PartDefinition cube_r356 = readout_a4.addOrReplaceChild("cube_r356", CubeListBuilder.create().texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition south = Controls.addOrReplaceChild("south", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition door = south.addOrReplaceChild("door", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition door_base = door.addOrReplaceChild("door_base", CubeListBuilder.create(), PartPose.offset(5.0126F, -19.5604F, -16.7005F));

		PartDefinition cube_r357 = door_base.addOrReplaceChild("cube_r357", CubeListBuilder.create().texOffs(63, 147).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, -0.212F, -0.1325F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r358 = door_base.addOrReplaceChild("cube_r358", CubeListBuilder.create().texOffs(197, 159).addBox(-2.0F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r359 = door_base.addOrReplaceChild("cube_r359", CubeListBuilder.create().texOffs(65, 172).addBox(-2.0F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.212F, 0.1325F, -1.0123F, 0.0F, 0.0F));

		PartDefinition handle_rotate = door.addOrReplaceChild("handle_rotate", CubeListBuilder.create(), PartPose.offsetAndRotation(5.0F, -19.0F, -16.25F, -0.9164F, 0.0024F, -0.0471F));

		PartDefinition cube_r360 = handle_rotate.addOrReplaceChild("cube_r360", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 1.675F, -1.15F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0126F, -1.4695F, -2.5661F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r361 = handle_rotate.addOrReplaceChild("cube_r361", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 1.45F, -1.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.84F)), PartPose.offsetAndRotation(0.0126F, -1.5172F, -2.3011F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r362 = handle_rotate.addOrReplaceChild("cube_r362", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 1.375F, -1.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.82F)), PartPose.offsetAndRotation(0.0126F, -1.7106F, -2.0387F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r363 = handle_rotate.addOrReplaceChild("cube_r363", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 1.275F, -1.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0126F, -1.9067F, -1.8664F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r364 = handle_rotate.addOrReplaceChild("cube_r364", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 1.125F, -1.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0126F, -2.1717F, -1.4424F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r365 = handle_rotate.addOrReplaceChild("cube_r365", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 0.575F, -2.6F, 2.0F, 2.0F, 4.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0126F, -1.5144F, -1.0317F, -0.1396F, 0.0F, 0.0F));

		PartDefinition cube_r366 = handle_rotate.addOrReplaceChild("cube_r366", CubeListBuilder.create().texOffs(197, 159).addBox(-1.0F, 0.575F, -1.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.0126F, -1.5356F, -1.045F, -0.1396F, 0.0F, 0.0F));

		PartDefinition coms = south.addOrReplaceChild("coms", CubeListBuilder.create(), PartPose.offset(-3.7374F, -19.3087F, -15.8784F));

		PartDefinition cube_r367 = coms.addOrReplaceChild("cube_r367", CubeListBuilder.create().texOffs(64, 172).addBox(-1.0F, -2.8906F, -1.9768F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(103, 238).addBox(-1.0F, -2.5406F, -1.7518F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(64, 172).addBox(-1.0F, -0.0406F, -1.4768F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(201, 145).addBox(-1.5F, -3.0F, -2.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(201, 145).addBox(-1.5F, -2.5F, -1.5F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r368 = coms.addOrReplaceChild("cube_r368", CubeListBuilder.create().texOffs(99, 238).addBox(-1.15F, -1.4F, 0.75F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(164, 178).addBox(-1.4F, -1.375F, 0.775F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(99, 238).addBox(-1.4F, -1.4F, 0.75F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.5F, -3.1002F, 3.0743F, -1.3439F, 0.0F, 0.0F));

		PartDefinition taptap_rotate_x = coms.addOrReplaceChild("taptap_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.0333F, -1.3737F, 0.7545F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r369 = taptap_rotate_x.addOrReplaceChild("cube_r369", CubeListBuilder.create().texOffs(201, 145).addBox(-1.5F, -0.2949F, -2.7721F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(97, 234).addBox(-1.725F, -2.6949F, -2.7721F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(95, 238).addBox(-1.25F, -2.6949F, -2.7721F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.5333F, 2.012F, -0.7034F, -1.0123F, 0.0F, 0.0F));

		PartDefinition monitor = south.addOrReplaceChild("monitor", CubeListBuilder.create(), PartPose.offset(0.25F, -1.75F, 5.0F));

		PartDefinition base = monitor.addOrReplaceChild("base", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9844F, -16.9655F));

		PartDefinition cube_r370 = base.addOrReplaceChild("cube_r370", CubeListBuilder.create().texOffs(59, 178).addBox(0.55F, -3.0F, 1.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(59, 178).addBox(-2.7F, -3.0F, 1.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.375F, -4.2742F, -0.2083F, -1.6755F, 0.0F, 0.0F));

		PartDefinition cube_r371 = base.addOrReplaceChild("cube_r371", CubeListBuilder.create().texOffs(59, 178).addBox(2.275F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.78F))
		.texOffs(59, 178).addBox(-0.975F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.78F)), PartPose.offsetAndRotation(-1.355F, -1.798F, 1.0562F, -0.6283F, 0.0F, 0.0F));

		PartDefinition cube_r372 = base.addOrReplaceChild("cube_r372", CubeListBuilder.create().texOffs(99, 143).addBox(-1.3F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(99, 143).addBox(0.075F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(99, 143).addBox(-2.625F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(99, 143).addBox(1.375F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(197, 179).addBox(-3.125F, -3.0F, -0.5F, 7.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition monitor_box = monitor.addOrReplaceChild("monitor_box", CubeListBuilder.create(), PartPose.offset(0.1376F, -24.2586F, -17.1738F));

		PartDefinition cube_r373 = monitor_box.addOrReplaceChild("cube_r373", CubeListBuilder.create().texOffs(90, 143).addBox(-2.5F, -2.825F, -1.9F, 5.0F, 3.0F, 4.0F, new CubeDeformation(-0.5F))
		.texOffs(197, 179).addBox(-3.5F, -2.325F, -2.9F, 7.0F, 5.0F, 6.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.6755F, 0.0F, 0.0F));

		PartDefinition cube_r374 = monitor_box.addOrReplaceChild("cube_r374", CubeListBuilder.create().texOffs(101, 137).addBox(-0.7F, -0.825F, -1.925F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 137).addBox(-0.7F, -0.825F, -1.425F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 137).addBox(-0.7F, -0.875F, -1.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(197, 179).addBox(-1.125F, -1.5F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(2.5F, 0.0812F, -0.1845F, -1.6755F, -0.2618F, 0.0F));

		PartDefinition cube_r375 = monitor_box.addOrReplaceChild("cube_r375", CubeListBuilder.create().texOffs(101, 137).addBox(-6.125F, 0.425F, -0.825F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(101, 137).addBox(-6.125F, 0.475F, -1.25F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 137).addBox(-6.125F, 0.475F, -1.75F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(2.5F, 0.0812F, -0.1845F, -1.6755F, 0.2618F, 0.0F));

		PartDefinition cube_r376 = monitor_box.addOrReplaceChild("cube_r376", CubeListBuilder.create().texOffs(197, 179).addBox(-0.85F, -1.5F, -1.5F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(-2.5F, 0.0812F, -0.1845F, -1.6755F, 0.2618F, 0.0F));

		PartDefinition screen = monitor.addOrReplaceChild("screen", CubeListBuilder.create(), PartPose.offset(0.1376F, -24.2586F, -17.1738F));

		PartDefinition cube_r377 = screen.addOrReplaceChild("cube_r377", CubeListBuilder.create().texOffs(148, 195).addBox(-2.5F, 1.275F, -2.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.6755F, 0.0F, 0.0F));

		PartDefinition screen_text = screen.addOrReplaceChild("screen_text", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r378 = screen_text.addOrReplaceChild("cube_r378", CubeListBuilder.create().texOffs(2, 247).addBox(-2.5F, 1.3F, -2.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(-0.25F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.6755F, 0.0F, 0.0F));

		PartDefinition dummy_c4 = south.addOrReplaceChild("dummy_c4", CubeListBuilder.create(), PartPose.offset(-0.375F, -1.525F, 2.2F));

		PartDefinition dial_pad_a4 = dummy_c4.addOrReplaceChild("dial_pad_a4", CubeListBuilder.create(), PartPose.offset(-0.2374F, -19.9094F, -16.9655F));

		PartDefinition cube_r379 = dial_pad_a4.addOrReplaceChild("cube_r379", CubeListBuilder.create().texOffs(31, 202).addBox(-1.6F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 234).addBox(-1.1F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(31, 202).addBox(-0.15F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 234).addBox(0.35F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(31, 202).addBox(1.25F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(36, 234).addBox(1.75F, 0.6F, -0.4F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(65, 172).addBox(-1.875F, -1.0F, -0.15F, 6.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.9076F, 0.0F, 0.0F));

		PartDefinition dial_a4 = dial_pad_a4.addOrReplaceChild("dial_a4", CubeListBuilder.create(), PartPose.offset(-0.3679F, 0.8104F, -0.0721F));

		PartDefinition cube_r380 = dial_a4.addOrReplaceChild("cube_r380", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r381 = dial_a4.addOrReplaceChild("cube_r381", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -1.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b5 = dial_pad_a4.addOrReplaceChild("dial_b5", CubeListBuilder.create(), PartPose.offset(1.1321F, 0.6854F, -0.1721F));

		PartDefinition cube_r382 = dial_b5.addOrReplaceChild("cube_r382", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r383 = dial_b5.addOrReplaceChild("cube_r383", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.425F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dial_b6 = dial_pad_a4.addOrReplaceChild("dial_b6", CubeListBuilder.create(), PartPose.offset(2.5321F, 0.6854F, -0.1721F));

		PartDefinition cube_r384 = dial_b6.addOrReplaceChild("cube_r384", CubeListBuilder.create().texOffs(62, 146).addBox(-1.375F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -0.8471F, -0.6431F, -0.4873F));

		PartDefinition cube_r385 = dial_b6.addOrReplaceChild("cube_r385", CubeListBuilder.create().texOffs(70, 149).addBox(-1.375F, -0.8F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(68, 148).addBox(-1.375F, -0.8F, -0.425F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.3679F, -0.3104F, 0.0721F, -1.0123F, 0.0F, 0.0F));

		PartDefinition dummy_b4 = south.addOrReplaceChild("dummy_b4", CubeListBuilder.create(), PartPose.offset(1.3376F, -19.6094F, -17.1405F));

		PartDefinition button_pad_a3 = dummy_b4.addOrReplaceChild("button_pad_a3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r386 = button_pad_a3.addOrReplaceChild("cube_r386", CubeListBuilder.create().texOffs(92, 176).addBox(-1.35F, 0.5F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(34, 205).addBox(-2.35F, 0.5F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(92, 176).addBox(-2.35F, -0.5F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(34, 205).addBox(-1.35F, -0.5F, -0.675F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(65, 172).addBox(-2.875F, -1.0F, -0.425F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition south_east = Controls.addOrReplaceChild("south_east", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition inc_select = south_east.addOrReplaceChild("inc_select", CubeListBuilder.create(), PartPose.offset(0.0F, -1.0F, 4.0F));

		PartDefinition inc_knob_rotate_x = inc_select.addOrReplaceChild("inc_knob_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.2624F, -13.719F, -17.7642F, 0.3927F, 0.0F, 0.0F));

		PartDefinition cube_r387 = inc_knob_rotate_x.addOrReplaceChild("cube_r387", CubeListBuilder.create().texOffs(199, 149).addBox(-1.0F, -1.0407F, -1.7566F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(199, 149).addBox(-1.0F, -1.0407F, -1.5066F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(199, 149).addBox(-1.0F, -1.0407F, -2.0066F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(199, 149).addBox(-1.0F, -1.0407F, -2.2566F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.025F, -5.8311F, -0.7523F, -1.4573F, 0.0F, 0.0F));

		PartDefinition cube_r388 = inc_knob_rotate_x.addOrReplaceChild("cube_r388", CubeListBuilder.create().texOffs(68, 144).addBox(-1.0F, -0.9998F, -8.1529F, 2.0F, 2.0F, 9.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.025F, 0.1689F, -0.0023F, -1.4573F, 0.0F, 0.0F));

		PartDefinition inc_track = inc_select.addOrReplaceChild("inc_track", CubeListBuilder.create(), PartPose.offset(-0.2374F, -18.6501F, -19.9703F));

		PartDefinition cube_r389 = inc_track.addOrReplaceChild("cube_r389", CubeListBuilder.create().texOffs(97, 145).addBox(-1.475F, 0.0F, -1.075F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-0.04F, -0.0449F, 0.0755F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r390 = inc_track.addOrReplaceChild("cube_r390", CubeListBuilder.create().texOffs(97, 145).addBox(-1.5F, -1.5F, -1.075F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(0.0F, 0.015F, 0.0354F, -1.1781F, 0.0F, 0.0F));

		PartDefinition cube_r391 = inc_track.addOrReplaceChild("cube_r391", CubeListBuilder.create().texOffs(98, 138).addBox(-1.5F, -1.0F, -1.05F, 3.0F, 5.0F, 2.0F, new CubeDeformation(-0.76F)), PartPose.offsetAndRotation(0.0F, -0.9633F, 3.8263F, -1.357F, 0.0F, 0.0F));

		PartDefinition cube_r392 = inc_track.addOrReplaceChild("cube_r392", CubeListBuilder.create().texOffs(70, 167).addBox(-1.75F, 0.25F, -1.5F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(70, 167).addBox(0.25F, 0.25F, -1.5F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(70, 167).addBox(-0.25F, 0.25F, -1.5F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(70, 167).addBox(-1.25F, 0.25F, -1.5F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.25F, -0.7341F, 4.4857F, -1.357F, 0.0F, 0.0F));

		PartDefinition cube_r393 = inc_track.addOrReplaceChild("cube_r393", CubeListBuilder.create().texOffs(65, 172).addBox(-0.5F, -1.5F, -0.5F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.5F, -1.5F, -0.5F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.0F, -2.0F, -0.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -1.3343F, 3.0048F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r394 = inc_track.addOrReplaceChild("cube_r394", CubeListBuilder.create().texOffs(68, 169).addBox(-0.225F, -0.5F, -1.325F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(68, 169).addBox(-0.725F, -1.5F, -1.325F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(68, 169).addBox(0.275F, -1.5F, -1.325F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.775F, 0.3061F, -0.5439F, -1.1781F, 0.0F, 0.0F));

		PartDefinition x = south_east.addOrReplaceChild("x", CubeListBuilder.create(), PartPose.offset(-4.7374F, -17.3833F, -19.1737F));

		PartDefinition cube_r395 = x.addOrReplaceChild("cube_r395", CubeListBuilder.create().texOffs(164, 175).addBox(-1.5F, -1.5F, -1.8F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(161, 167).addBox(-2.55F, -0.6F, -1.2F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(96, 140).addBox(-2.0F, -2.0F, -1.0F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(32, 146).addBox(-2.0F, -2.0F, -1.5F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0391F, -0.0921F, -0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r396 = x.addOrReplaceChild("cube_r396", CubeListBuilder.create().texOffs(159, 171).addBox(-2.55F, -1.4F, -1.05F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0404F, -0.2887F, -0.4014F, 0.0F, 0.0F));

		PartDefinition x_rotate_z = x.addOrReplaceChild("x_rotate_z", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0472F, -0.286F, -1.778F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r397 = x_rotate_z.addOrReplaceChild("cube_r397", CubeListBuilder.create().texOffs(67, 171).addBox(-2.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, -2.3327F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.302F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(0.198F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, 0.2173F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(64, 143).addBox(-1.027F, -1.8827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -1.4827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.177F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.577F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.877F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.477F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.5827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.1827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(91, 146).addBox(-1.027F, 0.2173F, -2.389F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(98, 141).addBox(-1.027F, -1.0327F, -0.964F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0298F, -0.0286F, 0.0222F, -1.0123F, 0.0F, 0.0F));

		PartDefinition y = south_east.addOrReplaceChild("y", CubeListBuilder.create(), PartPose.offset(-0.3874F, -17.3833F, -19.1737F));

		PartDefinition cube_r398 = y.addOrReplaceChild("cube_r398", CubeListBuilder.create().texOffs(41, 176).addBox(-1.5F, -1.5F, -1.8F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(31, 170).addBox(-2.55F, -0.6F, -1.2F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(32, 146).addBox(-2.0F, -2.0F, -1.5F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0391F, -0.0921F, -0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r399 = y.addOrReplaceChild("cube_r399", CubeListBuilder.create().texOffs(33, 171).addBox(-2.55F, -1.4F, -1.05F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0404F, -0.2887F, -0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r400 = y.addOrReplaceChild("cube_r400", CubeListBuilder.create().texOffs(96, 140).addBox(2.35F, -2.0F, -1.0F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-4.35F, -0.0391F, -0.0921F, -0.4014F, 0.0F, 0.0F));

		PartDefinition y_rotate_z = y.addOrReplaceChild("y_rotate_z", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0472F, -0.286F, -1.778F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r401 = y_rotate_z.addOrReplaceChild("cube_r401", CubeListBuilder.create().texOffs(67, 171).addBox(-2.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, -2.3327F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.302F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(0.198F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, 0.2173F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(64, 143).addBox(-1.027F, -1.8827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -1.4827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.177F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.577F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.877F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.477F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.5827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.1827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(91, 146).addBox(-1.027F, 0.2173F, -2.389F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(98, 141).addBox(-1.027F, -1.0327F, -0.964F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0298F, -0.0286F, 0.0222F, -1.0123F, 0.0F, 0.0F));

		PartDefinition z = south_east.addOrReplaceChild("z", CubeListBuilder.create(), PartPose.offset(4.0626F, -17.3833F, -19.1737F));

		PartDefinition cube_r402 = z.addOrReplaceChild("cube_r402", CubeListBuilder.create().texOffs(99, 174).addBox(-1.5F, -1.5F, -1.8F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(95, 173).addBox(-2.55F, -0.6F, -1.2F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(32, 146).addBox(-2.0F, -2.0F, -1.5F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0391F, -0.0921F, -0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r403 = z.addOrReplaceChild("cube_r403", CubeListBuilder.create().texOffs(92, 175).addBox(-2.55F, -1.4F, -1.05F, 5.0F, 2.0F, 4.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0404F, -0.2887F, -0.4014F, 0.0F, 0.0F));

		PartDefinition cube_r404 = z.addOrReplaceChild("cube_r404", CubeListBuilder.create().texOffs(96, 140).addBox(6.775F, -2.0F, -1.0F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-8.8F, -0.0391F, -0.0921F, -0.4014F, 0.0F, 0.0F));

		PartDefinition z_rotate_z = z.addOrReplaceChild("z_rotate_z", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0472F, -0.286F, -1.778F, 1.0036F, 0.0F, 0.0F));

		PartDefinition cube_r405 = z_rotate_z.addOrReplaceChild("cube_r405", CubeListBuilder.create().texOffs(67, 171).addBox(-2.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -0.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-0.027F, -2.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, -2.3327F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-2.302F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(0.198F, -1.5327F, -0.964F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 171).addBox(-1.527F, 0.2173F, -0.964F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(64, 143).addBox(-1.027F, -1.8827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -1.4827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.177F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-0.577F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.877F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.477F, -1.0327F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.5827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(64, 143).addBox(-1.027F, -0.1827F, -0.964F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(91, 146).addBox(-1.027F, 0.2173F, -2.389F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(98, 141).addBox(-1.027F, -1.0327F, -0.964F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0298F, -0.0286F, 0.0222F, -1.0123F, 0.0F, 0.0F));

		PartDefinition deco_lamps_3 = south_east.addOrReplaceChild("deco_lamps_3", CubeListBuilder.create(), PartPose.offset(-1.5F, -1.75F, 5.0F));

		PartDefinition lamp_a3 = deco_lamps_3.addOrReplaceChild("lamp_a3", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.3582F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r406 = lamp_a3.addOrReplaceChild("cube_r406", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r407 = lamp_a3.addOrReplaceChild("cube_r407", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_a3 = lamp_a3.addOrReplaceChild("glow_a3", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r408 = glow_a3.addOrReplaceChild("cube_r408", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition lamp_b3 = deco_lamps_3.addOrReplaceChild("lamp_b3", CubeListBuilder.create(), PartPose.offsetAndRotation(5.3918F, -19.1927F, -17.9863F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r409 = lamp_b3.addOrReplaceChild("cube_r409", CubeListBuilder.create().texOffs(99, 146).addBox(-3.625F, -0.6F, -2.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-3.1F, -0.725F, -2.225F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F))
		.texOffs(100, 237).addBox(-3.1F, -0.725F, -2.85F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-3.625F, -0.25F, -2.5F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, 0.0F, 0.0F));

		PartDefinition cube_r410 = lamp_b3.addOrReplaceChild("cube_r410", CubeListBuilder.create().texOffs(100, 237).addBox(-2.025F, -0.725F, 0.325F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(100, 236).addBox(-2.025F, -0.725F, 0.95F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(2.1208F, -0.7917F, 1.0208F, 0.0F, -1.5708F, 0.0F));

		PartDefinition glow_b3 = lamp_b3.addOrReplaceChild("glow_b3", CubeListBuilder.create(), PartPose.offset(2.1208F, -0.8417F, 1.0208F));

		PartDefinition cube_r411 = glow_b3.addOrReplaceChild("cube_r411", CubeListBuilder.create().texOffs(103, 171).addBox(-3.125F, -2.5F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -0.875F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(103, 171).addBox(-3.125F, -2.25F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.65F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition dummy_a4 = south_east.addOrReplaceChild("dummy_a4", CubeListBuilder.create(), PartPose.offset(0.65F, -0.85F, 2.5F));

		PartDefinition readout_a5 = dummy_a4.addOrReplaceChild("readout_a5", CubeListBuilder.create(), PartPose.offset(-4.7374F, -19.2344F, -20.4655F));

		PartDefinition cube_r412 = readout_a5.addOrReplaceChild("cube_r412", CubeListBuilder.create().texOffs(101, 142).addBox(-9.125F, -3.25F, 0.275F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(6.0F, 0.0F, 0.0F, -1.2305F, 0.0F, 0.0F));

		PartDefinition cube_r413 = readout_a5.addOrReplaceChild("cube_r413", CubeListBuilder.create().texOffs(33, 199).addBox(-3.125F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 142).addBox(-3.125F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition readout_b2 = dummy_a4.addOrReplaceChild("readout_b2", CubeListBuilder.create(), PartPose.offset(-2.0374F, -21.5094F, -13.7155F));

		PartDefinition cube_r414 = readout_b2.addOrReplaceChild("cube_r414", CubeListBuilder.create().texOffs(101, 142).addBox(-2.375F, -3.25F, 0.275F, 7.0F, 4.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.2305F, 0.0F, 0.0F));

		PartDefinition cube_r415 = readout_b2.addOrReplaceChild("cube_r415", CubeListBuilder.create().texOffs(101, 142).addBox(-2.375F, -1.75F, 0.0F, 7.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(33, 199).addBox(-2.375F, -1.75F, -0.225F, 7.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition readout_c3 = dummy_a4.addOrReplaceChild("readout_c3", CubeListBuilder.create(), PartPose.offset(3.3626F, -19.2344F, -20.4655F));

		PartDefinition cube_r416 = readout_c3.addOrReplaceChild("cube_r416", CubeListBuilder.create().texOffs(101, 142).addBox(-0.375F, -3.25F, 0.275F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.2305F, 0.0F, 0.0F));

		PartDefinition cube_r417 = readout_c3.addOrReplaceChild("cube_r417", CubeListBuilder.create().texOffs(101, 142).addBox(-0.375F, -1.75F, 0.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(33, 199).addBox(-0.375F, -1.75F, -0.225F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4014F, 0.0F, 0.0F));

		PartDefinition north_east = Controls.addOrReplaceChild("north_east", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition facing = north_east.addOrReplaceChild("facing", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition facing_base = facing.addOrReplaceChild("facing_base", CubeListBuilder.create(), PartPose.offset(-0.2374F, -21.9844F, -11.9655F));

		PartDefinition cube_r418 = facing_base.addOrReplaceChild("cube_r418", CubeListBuilder.create().texOffs(165, 175).addBox(-0.5F, -0.25F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(165, 175).addBox(-0.5F, -1.75F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(165, 175).addBox(-3.5F, -1.75F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(165, 175).addBox(-3.5F, -0.25F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(165, 175).addBox(0.5F, -1.0F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(165, 175).addBox(-4.25F, -1.0F, -0.25F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r419 = facing_base.addOrReplaceChild("cube_r419", CubeListBuilder.create().texOffs(38, 202).addBox(-2.075F, -1.875F, -1.1125F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.075F, -1.875F, -0.9125F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.3498F, 0.2186F, -0.855F, -0.6591F, -0.4893F));

		PartDefinition posts = facing.addOrReplaceChild("posts", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.205F, -22.4169F, -12.4662F, 0.5237F, -0.0189F, -0.0109F));

		PartDefinition cube_r420 = posts.addOrReplaceChild("cube_r420", CubeListBuilder.create().texOffs(161, 169).addBox(-1.0F, -0.95F, -0.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.4748F, 0.0F, 0.0F));

		PartDefinition cube_r421 = posts.addOrReplaceChild("cube_r421", CubeListBuilder.create().texOffs(161, 169).addBox(-1.0F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.003F, 0.1937F, -1.1071F, -1.4232F, -0.7799F, -0.1042F));

		PartDefinition cube_r422 = posts.addOrReplaceChild("cube_r422", CubeListBuilder.create().texOffs(65, 172).addBox(-2.45F, -0.975F, -1.125F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.0324F, 0.6F, 0.5581F, 0.0F, -1.4661F, -1.5708F));

		PartDefinition landing = north_east.addOrReplaceChild("landing", CubeListBuilder.create(), PartPose.offset(-5.65F, 0.15F, -0.125F));

		PartDefinition cube_r423 = landing.addOrReplaceChild("cube_r423", CubeListBuilder.create().texOffs(101, 233).addBox(-0.4F, -0.025F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(101, 233).addBox(0.4F, -0.025F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(102, 235).addBox(0.4F, -1.925F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(102, 235).addBox(-0.4F, -1.925F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(66, 138).addBox(-0.4F, -1.0F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(66, 138).addBox(0.4F, -1.0F, -0.575F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F))
		.texOffs(158, 173).addBox(-0.5F, -2.0F, -0.25F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-0.2374F, -19.9844F, -16.9655F, -1.0123F, 0.0F, 0.0F));

		PartDefinition knife_rotate_z = landing.addOrReplaceChild("knife_rotate_z", CubeListBuilder.create(), PartPose.offsetAndRotation(0.7376F, -19.5718F, -16.7804F, 0.3927F, 0.0F, 0.0F));

		PartDefinition cube_r424 = knife_rotate_z.addOrReplaceChild("cube_r424", CubeListBuilder.create().texOffs(32, 168).addBox(0.0F, -3.575F, -0.7F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(67, 143).addBox(0.0F, -3.1F, -0.7F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-1.0F, -0.2418F, -0.1055F, -1.2741F, 0.0F, 0.0F));

		PartDefinition randomizer = north_east.addOrReplaceChild("randomizer", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition book = randomizer.addOrReplaceChild("book", CubeListBuilder.create(), PartPose.offset(0.075F, 0.0F, 0.0F));

		PartDefinition cover = book.addOrReplaceChild("cover", CubeListBuilder.create(), PartPose.offset(4.925F, -19.4055F, -16.9506F));

		PartDefinition cube_r425 = cover.addOrReplaceChild("cube_r425", CubeListBuilder.create().texOffs(207, 167).addBox(-0.925F, -1.975F, -1.425F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(0.0385F, 0.1689F, 0.2037F, -1.0124F, 0.0092F, -0.0148F));

		PartDefinition page_1 = book.addOrReplaceChild("page_1", CubeListBuilder.create(), PartPose.offset(4.925F, -19.5485F, -16.723F));

		PartDefinition cube_r426 = page_1.addOrReplaceChild("cube_r426", CubeListBuilder.create().texOffs(42, 204).addBox(-0.9217F, -1.8028F, -1.1494F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(0.0329F, 0.025F, 0.0F, -1.0124F, 0.0092F, -0.0148F));

		PartDefinition page_2 = book.addOrReplaceChild("page_2", CubeListBuilder.create(), PartPose.offset(4.925F, -19.5235F, -16.723F));

		PartDefinition cube_r427 = page_2.addOrReplaceChild("cube_r427", CubeListBuilder.create().texOffs(42, 204).addBox(-0.925F, -1.975F, -1.375F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(0.0408F, 0.3165F, -0.0053F, -1.0124F, 0.0092F, -0.0148F));

		PartDefinition spine = book.addOrReplaceChild("spine", CubeListBuilder.create(), PartPose.offset(4.925F, -19.3319F, -16.914F));

		PartDefinition cube_r428 = spine.addOrReplaceChild("cube_r428", CubeListBuilder.create().texOffs(206, 166).addBox(3.85F, -2.0F, -0.775F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-4.8431F, -0.3582F, -0.1457F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r429 = spine.addOrReplaceChild("cube_r429", CubeListBuilder.create().texOffs(206, 166).addBox(3.925F, -2.0F, -0.4F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-4.9181F, -0.5914F, -0.2915F, -1.0123F, 0.0F, 0.0F));

		PartDefinition book_back = book.addOrReplaceChild("book_back", CubeListBuilder.create(), PartPose.offset(5.5363F, -19.3154F, -16.8255F));

		PartDefinition cube_r430 = book_back.addOrReplaceChild("cube_r430", CubeListBuilder.create().texOffs(41, 203).addBox(-0.925F, -2.0F, -1.15F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(-0.5737F, -0.0352F, -0.022F, -1.0124F, 0.0092F, -0.0148F));

		PartDefinition cube_r431 = book_back.addOrReplaceChild("cube_r431", CubeListBuilder.create().texOffs(41, 204).addBox(-0.925F, -1.975F, -1.375F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.98F)), PartPose.offsetAndRotation(-0.5712F, 0.0745F, 0.076F, -1.0124F, 0.0092F, -0.0148F));

		PartDefinition cube_r432 = book_back.addOrReplaceChild("cube_r432", CubeListBuilder.create().texOffs(207, 167).addBox(4.025F, -2.0F, -0.4F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-5.5294F, -0.5232F, -0.3269F, -1.0123F, 0.0F, 0.0F));

		PartDefinition lecturn = randomizer.addOrReplaceChild("lecturn", CubeListBuilder.create(), PartPose.offset(-0.2931F, -19.9635F, -16.9525F));

		PartDefinition cube_r433 = lecturn.addOrReplaceChild("cube_r433", CubeListBuilder.create().texOffs(68, 166).addBox(-0.925F, -0.9F, -3.275F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(68, 166).addBox(-0.925F, -0.95F, -3.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.87F)), PartPose.offsetAndRotation(5.38F, -2.4097F, 3.22F, -0.0611F, 0.0F, 0.0F));

		PartDefinition cube_r434 = lecturn.addOrReplaceChild("cube_r434", CubeListBuilder.create().texOffs(68, 166).addBox(-0.95F, -0.2F, -1.5F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(5.4F, -2.328F, 3.2504F, -1.021F, 0.0F, 0.0F));

		PartDefinition cube_r435 = lecturn.addOrReplaceChild("cube_r435", CubeListBuilder.create().texOffs(68, 166).addBox(-0.6F, -1.325F, -1.225F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(5.05F, -1.2823F, 2.3776F, -2.0246F, 0.0F, 0.0F));

		PartDefinition cube_r436 = lecturn.addOrReplaceChild("cube_r436", CubeListBuilder.create().texOffs(68, 166).addBox(-1.5F, -2.0F, -1.75F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.95F))
		.texOffs(163, 167).addBox(-2.0F, -1.5F, -1.5F, 4.0F, 3.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(5.45F, 0.5001F, 1.6239F, -1.2392F, 0.0F, 0.0F));

		PartDefinition cube_r437 = lecturn.addOrReplaceChild("cube_r437", CubeListBuilder.create().texOffs(159, 170).addBox(3.45F, -1.65F, -0.325F, 4.0F, 4.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.0123F, 0.0F, 0.0F));

		PartDefinition cube_r438 = lecturn.addOrReplaceChild("cube_r438", CubeListBuilder.create().texOffs(166, 173).addBox(3.45F, 0.35F, -0.575F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(0.0F, 0.1272F, 0.0795F, -1.0123F, 0.0F, 0.0F));

		PartDefinition glow_booklight = lecturn.addOrReplaceChild("glow_booklight", CubeListBuilder.create(), PartPose.offset(5.38F, -2.4097F, 3.22F));

		PartDefinition cube_r439 = glow_booklight.addOrReplaceChild("cube_r439", CubeListBuilder.create().texOffs(36, 172).addBox(-1.925F, -0.7F, -3.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(33, 207).addBox(-1.925F, -0.35F, -3.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.0611F, 0.0F, 0.0F));

		PartDefinition dummy_b3 = north_east.addOrReplaceChild("dummy_b3", CubeListBuilder.create(), PartPose.offset(0.7376F, -19.6094F, -17.1405F));

		PartDefinition slider_pad_a1 = dummy_b3.addOrReplaceChild("slider_pad_a1", CubeListBuilder.create(), PartPose.offset(-1.7F, 0.0F, 0.0F));

		PartDefinition cube_r440 = slider_pad_a1.addOrReplaceChild("cube_r440", CubeListBuilder.create().texOffs(65, 172).addBox(-1.35F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.375F, 0.2274F, 1.3154F, -1.7061F, 0.0F, 0.0F));

		PartDefinition cube_r441 = slider_pad_a1.addOrReplaceChild("cube_r441", CubeListBuilder.create().texOffs(65, 172).addBox(-3.675F, 1.5F, -0.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.975F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r442 = slider_pad_a1.addOrReplaceChild("cube_r442", CubeListBuilder.create().texOffs(65, 172).addBox(-1.55F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(95, 145).addBox(-2.575F, -2.0F, -0.375F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.225F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.875F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition slider_a = slider_pad_a1.addOrReplaceChild("slider_a", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r443 = slider_a.addOrReplaceChild("cube_r443", CubeListBuilder.create().texOffs(37, 240).addBox(-2.725F, 1.0F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r444 = slider_a.addOrReplaceChild("cube_r444", CubeListBuilder.create().texOffs(37, 240).addBox(-2.375F, 1.0F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(-0.01F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition slider_a2 = slider_pad_a1.addOrReplaceChild("slider_a2", CubeListBuilder.create(), PartPose.offset(0.7F, 0.0F, 0.0F));

		PartDefinition cube_r445 = slider_a2.addOrReplaceChild("cube_r445", CubeListBuilder.create().texOffs(37, 240).addBox(-2.725F, -1.0F, -0.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r446 = slider_a2.addOrReplaceChild("cube_r446", CubeListBuilder.create().texOffs(37, 240).addBox(-2.375F, -1.0F, -0.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(-0.01F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition slider_pad_a2 = dummy_b3.addOrReplaceChild("slider_pad_a2", CubeListBuilder.create(), PartPose.offset(2.55F, 0.0F, 0.0F));

		PartDefinition cube_r447 = slider_pad_a2.addOrReplaceChild("cube_r447", CubeListBuilder.create().texOffs(65, 172).addBox(-1.35F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(-1.375F, 0.2274F, 1.3154F, -1.7061F, 0.0F, 0.0F));

		PartDefinition cube_r448 = slider_pad_a2.addOrReplaceChild("cube_r448", CubeListBuilder.create().texOffs(65, 172).addBox(-3.675F, 1.5F, -0.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.975F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r449 = slider_pad_a2.addOrReplaceChild("cube_r449", CubeListBuilder.create().texOffs(65, 172).addBox(-1.55F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(95, 145).addBox(-2.575F, -2.0F, -0.375F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.225F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-2.875F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition slider_a3 = slider_pad_a2.addOrReplaceChild("slider_a3", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r450 = slider_a3.addOrReplaceChild("cube_r450", CubeListBuilder.create().texOffs(37, 240).addBox(-2.725F, -1.5F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r451 = slider_a3.addOrReplaceChild("cube_r451", CubeListBuilder.create().texOffs(37, 240).addBox(-2.375F, -1.5F, -0.75F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(-0.01F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition slider_a4 = slider_pad_a2.addOrReplaceChild("slider_a4", CubeListBuilder.create(), PartPose.offset(0.7F, 0.0F, 0.0F));

		PartDefinition cube_r452 = slider_a4.addOrReplaceChild("cube_r452", CubeListBuilder.create().texOffs(37, 240).addBox(-2.725F, 0.25F, -0.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r453 = slider_a4.addOrReplaceChild("cube_r453", CubeListBuilder.create().texOffs(37, 240).addBox(-2.375F, 0.25F, -0.725F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.83F)), PartPose.offsetAndRotation(-0.01F, 0.0F, 0.0F, -0.877F, 0.0F, 0.0F));

		PartDefinition gear_spinner = dummy_b3.addOrReplaceChild("gear_spinner", CubeListBuilder.create(), PartPose.offset(0.425F, 0.0F, 0.0F));

		PartDefinition gear_rotate_x = gear_spinner.addOrReplaceChild("gear_rotate_x", CubeListBuilder.create(), PartPose.offset(-1.225F, 0.5425F, -0.2285F));

		PartDefinition cube_r454 = gear_rotate_x.addOrReplaceChild("cube_r454", CubeListBuilder.create().texOffs(98, 236).addBox(-2.225F, -0.95F, -3.05F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(1.225F, 0.8575F, 1.4285F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r455 = gear_rotate_x.addOrReplaceChild("cube_r455", CubeListBuilder.create().texOffs(101, 234).addBox(-1.0F, -1.0F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(101, 234).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0017F, -0.0015F, -1.6624F, 0.0F, 0.0F));

		PartDefinition cube_r456 = gear_rotate_x.addOrReplaceChild("cube_r456", CubeListBuilder.create().texOffs(101, 234).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(101, 234).addBox(-1.0F, -1.0F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0017F, -0.0015F, 0.6938F, 0.0F, 0.0F));

		PartDefinition cube_r457 = gear_rotate_x.addOrReplaceChild("cube_r457", CubeListBuilder.create().texOffs(101, 234).addBox(-1.0F, -1.0F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(101, 234).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0017F, -0.0015F, -0.0916F, 0.0F, 0.0F));

		PartDefinition cube_r458 = gear_rotate_x.addOrReplaceChild("cube_r458", CubeListBuilder.create().texOffs(101, 234).addBox(-1.0F, -1.0F, -0.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(101, 234).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0017F, -0.0015F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r459 = gear_rotate_x.addOrReplaceChild("cube_r459", CubeListBuilder.create().texOffs(96, 233).addBox(-1.0F, -1.0F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F))
		.texOffs(96, 236).addBox(-1.0F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, -0.0017F, -0.0015F, -1.5708F, -0.6938F, 1.5708F));

		PartDefinition gear_base = gear_spinner.addOrReplaceChild("gear_base", CubeListBuilder.create(), PartPose.offset(-1.375F, 0.2274F, 1.3154F));

		PartDefinition cube_r460 = gear_base.addOrReplaceChild("cube_r460", CubeListBuilder.create().texOffs(65, 172).addBox(-1.35F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.7061F, 0.0F, 0.0F));

		PartDefinition cube_r461 = gear_base.addOrReplaceChild("cube_r461", CubeListBuilder.create().texOffs(65, 172).addBox(-3.675F, 1.5F, -0.425F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(2.35F, -0.2274F, -1.3154F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r462 = gear_base.addOrReplaceChild("cube_r462", CubeListBuilder.create().texOffs(98, 145).addBox(-2.575F, -2.0F, -0.375F, 3.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-1.65F, -0.625F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-1.65F, -0.325F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.8F, -0.625F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-1.65F, -0.475F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F))
		.texOffs(65, 172).addBox(-2.8F, -0.475F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F))
		.texOffs(65, 172).addBox(-2.8F, -0.325F, -1.075F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.875F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F))
		.texOffs(65, 172).addBox(-1.55F, -2.0F, -0.425F, 2.0F, 5.0F, 3.0F, new CubeDeformation(-0.75F)), PartPose.offsetAndRotation(1.375F, -0.2274F, -1.3154F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r463 = gear_base.addOrReplaceChild("cube_r463", CubeListBuilder.create().texOffs(65, 172).addBox(-1.65F, -0.875F, -1.075F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.8F, -0.875F, -1.075F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(1.375F, -0.1955F, -1.3539F, -0.877F, 0.0F, 0.0F));

		PartDefinition cube_r464 = gear_base.addOrReplaceChild("cube_r464", CubeListBuilder.create().texOffs(65, 172).addBox(-1.65F, 0.125F, -1.075F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(65, 172).addBox(-2.8F, 0.125F, -1.075F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(1.375F, -0.3873F, -1.1232F, -0.877F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}
	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		stations.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rotor.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		Controls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.rotorAnimationState, NouveauConsoleAnimations.MODEL_ROTOR, ageInTicks);

		if(Minecraft.getInstance().level != null){
			Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
				ControlData<Boolean> stabilizerData = tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get());
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.RANDOMIZER.get()).getUseAnimationState(), NouveauConsoleAnimations.MODEL_RANDOMIZER, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get()).getUseAnimationState(), stabilizerData.get() ? NouveauConsoleAnimations.MODEL_ACTIVATE_STABLIZERS : NouveauConsoleAnimations.MODEL_DEACTIVATE_STABLIZERS, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.COMMUNICATOR.get()).getUseAnimationState(), NouveauConsoleAnimations.COMMUNICATOR, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.DIMENSIONS.get()).getUseAnimationState(), NouveauConsoleAnimations.DIMENIONS, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.X.get()).getUseAnimationState(), NouveauConsoleAnimations.X, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.Y.get()).getUseAnimationState(), NouveauConsoleAnimations.Y, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.Z.get()).getUseAnimationState(), NouveauConsoleAnimations.Z, ageInTicks);
				this.animate(tardis.getControlDataOrCreate(ControlRegistry.DOOR.get()).getUseAnimationState(), NouveauConsoleAnimations.MODEL_DOOR_CONTROL, ageInTicks);
				NouveauConsoleAnimations.animateConditional(tardis, this, ageInTicks);
			});
		}
	}

	@Override
	public Optional<String> getPartForControl(ControlType<?> type) {
		if(type == ControlRegistry.SONIC_PORT.get()){
			return Optional.of("Controls/north_west/sonic_port/hex_port");
		}
		return Optional.empty();
	}
}