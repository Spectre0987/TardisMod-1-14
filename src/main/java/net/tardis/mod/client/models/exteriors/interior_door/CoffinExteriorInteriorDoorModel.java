package net.tardis.mod.client.models.exteriors.interior_door;


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class CoffinExteriorInteriorDoorModel extends BasicTileHierarchicalModel<InteriorDoorTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/interior/coffin"), "main");
	private final ModelPart coffin;
	private final ModelPart door_rotate_y;
	private final ModelPart frame;
	private final ModelPart boti;

	public CoffinExteriorInteriorDoorModel(ModelPart root) {
		super(root);
		this.coffin = root.getChild("coffin");
		this.door_rotate_y = this.coffin.getChild("door_rotate_y");
		this.frame = this.coffin.getChild("frame");
		this.boti = this.coffin.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition coffin = partdefinition.addOrReplaceChild("coffin", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 2.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition door_rotate_y = coffin.addOrReplaceChild("door_rotate_y", CubeListBuilder.create().texOffs(68, 90).addBox(5.0135F, -12.6143F, -2.0F, 12.0F, 36.0F, 2.0F, new CubeDeformation(0.002F))
		.texOffs(0, 73).addBox(0.7635F, -2.1143F, -3.0F, 21.0F, 4.0F, 2.0F, new CubeDeformation(0.002F))
		.texOffs(0, 66).addBox(1.9635F, 11.1357F, -3.0F, 18.0F, 4.0F, 2.0F, new CubeDeformation(0.002F))
		.texOffs(92, 81).addBox(2.9368F, 23.1953F, -2.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.002F)), PartPose.offset(-11.0288F, -24.5751F, -3.5711F));

		PartDefinition cube_r1 = door_rotate_y.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(96, 100).addBox(-1.0F, -21.0F, 1.0F, 6.0F, 26.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.2499F, 19.8575F, -3.0F, 0.0F, 0.0F, -0.1309F));

		PartDefinition cube_r2 = door_rotate_y.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(96, 85).addBox(-1.0F, -12.0F, 1.0F, 6.0F, 13.0F, 2.0F, new CubeDeformation(-0.001F)), PartPose.offsetAndRotation(0.8463F, -1.316F, -3.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition cube_r3 = door_rotate_y.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(112, 85).addBox(-5.0F, -12.0F, 1.0F, 6.0F, 13.0F, 2.0F, new CubeDeformation(-0.001F)), PartPose.offsetAndRotation(21.1786F, -1.316F, -3.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition cube_r4 = door_rotate_y.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(112, 100).addBox(-5.0F, -21.0F, 1.0F, 6.0F, 26.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(18.775F, 19.8575F, -3.0F, 0.0F, 0.0F, 0.1309F));

		PartDefinition frame = coffin.addOrReplaceChild("frame", CubeListBuilder.create().texOffs(85, 54).addBox(3.5865F, -32.2218F, -8.0F, 12.0F, 2.0F, 9.0F, new CubeDeformation(0.002F))
		.texOffs(67, 21).addBox(1.6612F, 3.3378F, -8.0F, 16.0F, 2.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(-9.5855F, -4.8176F, 3.0F));

		PartDefinition cube_r5 = frame.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(61, 32).addBox(-1.0F, -21.0F, -8.0F, 2.0F, 26.0F, 9.0F, new CubeDeformation(0.02F)), PartPose.offsetAndRotation(1.825F, 0.25F, 0.0F, 0.0F, 0.0F, -0.1309F));

		PartDefinition cube_r6 = frame.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(113, 69).addBox(-1.0F, -13.0F, -4.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.1309F));

		PartDefinition cube_r7 = frame.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(83, 32).addBox(-1.0F, -13.0F, -8.0F, 2.0F, 13.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.0012F, -20.0172F, 0.0F, 0.0F, 0.0F, 0.4363F));

		PartDefinition cube_r8 = frame.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(105, 32).addBox(-1.0F, -13.0F, -8.0F, 2.0F, 13.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(20.1743F, -20.0172F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition cube_r9 = frame.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(120, 69).addBox(-1.0F, -13.0F, -4.0F, 2.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(18.171F, 0.0F, 0.0F, 0.0F, 0.0F, 0.1309F));

		PartDefinition cube_r10 = frame.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(39, 32).addBox(-1.0F, -21.0F, -8.0F, 2.0F, 26.0F, 9.0F, new CubeDeformation(0.002F)), PartPose.offsetAndRotation(17.3481F, 0.25F, 0.0F, 0.0F, 0.0F, 0.1309F));

		PartDefinition boti = coffin.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(12, 91).addBox(-5.9865F, -18.1143F, 0.0F, 12.0F, 36.0F, 1.0F, new CubeDeformation(0.02F)), PartPose.offset(-0.0124F, -17.6751F, -5.5F));

		PartDefinition cube_r11 = boti.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(38, 102).addBox(0.0F, -21.0F, 2.0F, 5.0F, 25.0F, 1.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(-7.7509F, 13.8573F, -2.002F, 0.0F, 0.0F, -0.1309F));

		PartDefinition cube_r12 = boti.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(0, 114).addBox(0.0F, -12.0F, 2.0F, 5.0F, 13.0F, 1.0F, new CubeDeformation(0.001F)), PartPose.offsetAndRotation(-10.1532F, -7.3166F, -2.001F, 0.0F, 0.0F, 0.4363F));

		PartDefinition cube_r13 = boti.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(0, 100).addBox(-5.0F, -12.0F, 2.0F, 5.0F, 13.0F, 1.0F, new CubeDeformation(0.001F)), PartPose.offsetAndRotation(10.1801F, -7.3166F, -2.001F, 0.0F, 0.0F, -0.4363F));

		PartDefinition cube_r14 = boti.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(50, 102).addBox(-5.0F, -21.0F, 2.0F, 5.0F, 25.0F, 1.0F, new CubeDeformation(-0.002F)), PartPose.offsetAndRotation(7.7779F, 13.8573F, -2.002F, 0.0F, 0.0F, 0.1309F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		coffin.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(InteriorDoorTile tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen()){
			this.door_rotate_y.offsetRotation(AnimationHelper.degrees(90, AnimationHelper.Axis.Y));
		}
	}
}