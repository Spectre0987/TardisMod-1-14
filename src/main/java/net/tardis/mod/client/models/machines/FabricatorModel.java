package net.tardis.mod.client.models.machines;
// Made with Blockbench 4.11.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.tardis.mod.blockentities.machines.FabricatorTile;
import net.tardis.mod.client.animations.machines.FabricatorAnimations;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class FabricatorModel extends BasicTileHierarchicalModel<FabricatorTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("machines/fab"), "main");
	private final ModelPart fab;
	private final ModelPart fab_arm_0;
	private final ModelPart shoulder_0;
	private final ModelPart upperarm_0;
	private final ModelPart elbow_0;
	private final ModelPart forearm_0;
	private final ModelPart wrist_0;
	private final ModelPart hand_0;
	private final ModelPart glow_laser_0;
	private final ModelPart fab_arm_1;
	private final ModelPart shoulder_1;
	private final ModelPart upperarm_1;
	private final ModelPart elbow_1;
	private final ModelPart forearm_1;
	private final ModelPart wrist_1;
	private final ModelPart hand_1;
	private final ModelPart glow_laser_1;
	private final ModelPart fab_arm_2;
	private final ModelPart shoulder_2;
	private final ModelPart upperarm_2;
	private final ModelPart elbow_2;
	private final ModelPart forearm_2;
	private final ModelPart wrist_2;
	private final ModelPart hand_2;
	private final ModelPart glow_laser_2;
	private final ModelPart overhead;
	private final ModelPart ring_0;
	private final ModelPart ring_1;
	private final ModelPart base;
	private final ModelPart platform;
	private final ModelPart item;
	private final ModelPart controlpanel;

	public FabricatorModel(ModelPart root) {
		super(root);
		this.fab = root.getChild("fab");
		this.fab_arm_0 = this.fab.getChild("fab_arm_0");
		this.shoulder_0 = this.fab_arm_0.getChild("shoulder_0");
		this.upperarm_0 = this.shoulder_0.getChild("upperarm_0");
		this.elbow_0 = this.upperarm_0.getChild("elbow_0");
		this.forearm_0 = this.elbow_0.getChild("forearm_0");
		this.wrist_0 = this.forearm_0.getChild("wrist_0");
		this.hand_0 = this.wrist_0.getChild("hand_0");
		this.glow_laser_0 = this.hand_0.getChild("glow_laser_0");
		this.fab_arm_1 = this.fab.getChild("fab_arm_1");
		this.shoulder_1 = this.fab_arm_1.getChild("shoulder_1");
		this.upperarm_1 = this.shoulder_1.getChild("upperarm_1");
		this.elbow_1 = this.upperarm_1.getChild("elbow_1");
		this.forearm_1 = this.elbow_1.getChild("forearm_1");
		this.wrist_1 = this.forearm_1.getChild("wrist_1");
		this.hand_1 = this.wrist_1.getChild("hand_1");
		this.glow_laser_1 = this.hand_1.getChild("glow_laser_1");
		this.fab_arm_2 = this.fab.getChild("fab_arm_2");
		this.shoulder_2 = this.fab_arm_2.getChild("shoulder_2");
		this.upperarm_2 = this.shoulder_2.getChild("upperarm_2");
		this.elbow_2 = this.upperarm_2.getChild("elbow_2");
		this.forearm_2 = this.elbow_2.getChild("forearm_2");
		this.wrist_2 = this.forearm_2.getChild("wrist_2");
		this.hand_2 = this.wrist_2.getChild("hand_2");
		this.glow_laser_2 = this.hand_2.getChild("glow_laser_2");
		this.overhead = this.fab.getChild("overhead");
		this.ring_0 = this.overhead.getChild("ring_0");
		this.ring_1 = this.overhead.getChild("ring_1");
		this.base = root.getChild("base");
		this.platform = this.base.getChild("platform");
		this.item = this.base.getChild("item");
		this.controlpanel = this.base.getChild("controlpanel");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition fab = partdefinition.addOrReplaceChild("fab", CubeListBuilder.create(), PartPose.offset(0.0F, -23.0F, 0.0F));

		PartDefinition fab_arm_0 = fab.addOrReplaceChild("fab_arm_0", CubeListBuilder.create().texOffs(64, 21).addBox(-14.5F, -1.5F, -1.75F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.103F, 2.1116F, 0.25F, 0.0F, -0.5236F, 0.0F));

		PartDefinition shoulder_0 = fab_arm_0.addOrReplaceChild("shoulder_0", CubeListBuilder.create().texOffs(0, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-14.0F, 0.0F, -0.25F, 0.0F, 0.0F, -1.2654F));

		PartDefinition upperarm_0 = shoulder_0.addOrReplaceChild("upperarm_0", CubeListBuilder.create().texOffs(20, 70).addBox(-9.5F, -1.25F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.25F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition elbow_0 = upperarm_0.addOrReplaceChild("elbow_0", CubeListBuilder.create(), PartPose.offsetAndRotation(-10.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.48F));

		PartDefinition cube_r1 = elbow_0.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(44, 70).addBox(-22.75F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(20.5F, 0.25F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition forearm_0 = elbow_0.addOrReplaceChild("forearm_0", CubeListBuilder.create().texOffs(20, 76).addBox(-28.0F, -1.75F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(20.5F, 0.25F, 0.0F));

		PartDefinition wrist_0 = forearm_0.addOrReplaceChild("wrist_0", CubeListBuilder.create().texOffs(64, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-27.5F, 0.0F, 0.0F, 0.0F, 0.0F, -0.6545F));

		PartDefinition hand_0 = wrist_0.addOrReplaceChild("hand_0", CubeListBuilder.create().texOffs(64, 27).addBox(-5.5F, -0.75F, -1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_laser_0 = hand_0.addOrReplaceChild("glow_laser_0", CubeListBuilder.create().texOffs(62, 50).addBox(-11.25F, -0.5F, -0.5F, 11.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-3.25F, 0.25F, 0.0F));

		PartDefinition fab_arm_1 = fab.addOrReplaceChild("fab_arm_1", CubeListBuilder.create().texOffs(64, 21).addBox(-14.5F, -1.5F, -1.75F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.103F, 2.1116F, 0.25F, 0.0F, 1.5708F, 0.0F));

		PartDefinition shoulder_1 = fab_arm_1.addOrReplaceChild("shoulder_1", CubeListBuilder.create().texOffs(0, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-14.0F, 0.0F, -0.25F, 0.0F, 0.0F, -1.2654F));

		PartDefinition upperarm_1 = shoulder_1.addOrReplaceChild("upperarm_1", CubeListBuilder.create().texOffs(20, 70).addBox(-9.5F, -1.25F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.25F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition elbow_1 = upperarm_1.addOrReplaceChild("elbow_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-10.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.48F));

		PartDefinition cube_r2 = elbow_1.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(44, 70).addBox(-22.75F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(20.5F, 0.25F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition forearm_1 = elbow_1.addOrReplaceChild("forearm_1", CubeListBuilder.create().texOffs(20, 76).addBox(-28.0F, -1.75F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(20.5F, 0.25F, 0.0F));

		PartDefinition wrist_1 = forearm_1.addOrReplaceChild("wrist_1", CubeListBuilder.create().texOffs(64, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-27.5F, 0.0F, 0.0F, 0.0F, 0.0F, -0.6545F));

		PartDefinition hand_1 = wrist_1.addOrReplaceChild("hand_1", CubeListBuilder.create().texOffs(64, 27).addBox(-5.5F, -0.75F, -1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_laser_1 = hand_1.addOrReplaceChild("glow_laser_1", CubeListBuilder.create().texOffs(62, 50).addBox(-11.25F, -0.5F, -0.5F, 11.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-3.25F, 0.25F, 0.0F));

		PartDefinition fab_arm_2 = fab.addOrReplaceChild("fab_arm_2", CubeListBuilder.create().texOffs(64, 21).addBox(-14.5F, -1.5F, -1.75F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.103F, 2.1116F, 0.25F, 0.0F, -2.618F, 0.0F));

		PartDefinition shoulder_2 = fab_arm_2.addOrReplaceChild("shoulder_2", CubeListBuilder.create().texOffs(0, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-14.0F, 0.0F, -0.25F, 0.0F, 0.0F, -1.2654F));

		PartDefinition upperarm_2 = shoulder_2.addOrReplaceChild("upperarm_2", CubeListBuilder.create().texOffs(20, 70).addBox(-9.5F, -1.25F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.25F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition elbow_2 = upperarm_2.addOrReplaceChild("elbow_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-10.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.48F));

		PartDefinition cube_r3 = elbow_2.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(44, 70).addBox(-22.75F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(20.5F, 0.25F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition forearm_2 = elbow_2.addOrReplaceChild("forearm_2", CubeListBuilder.create().texOffs(20, 76).addBox(-28.0F, -1.75F, -1.5F, 9.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(20.5F, 0.25F, 0.0F));

		PartDefinition wrist_2 = forearm_2.addOrReplaceChild("wrist_2", CubeListBuilder.create().texOffs(64, 70).addBox(-2.5F, -2.75F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-27.5F, 0.0F, 0.0F, 0.0F, 0.0F, -0.6545F));

		PartDefinition hand_2 = wrist_2.addOrReplaceChild("hand_2", CubeListBuilder.create().texOffs(64, 27).addBox(-5.5F, -0.75F, -1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_laser_2 = hand_2.addOrReplaceChild("glow_laser_2", CubeListBuilder.create().texOffs(62, 50).addBox(-11.25F, -0.5F, -0.5F, 11.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-3.25F, 0.25F, 0.0F));

		PartDefinition overhead = fab.addOrReplaceChild("overhead", CubeListBuilder.create(), PartPose.offset(0.0F, 16.0F, 0.0F));

		PartDefinition ring_0 = overhead.addOrReplaceChild("ring_0", CubeListBuilder.create().texOffs(0, 63).addBox(-5.5718F, -2.5F, -8.3301F, 10.0F, 2.0F, 5.0F, new CubeDeformation(-0.01F)), PartPose.offset(0.5718F, -13.5F, 0.3301F));

		PartDefinition cube_r4 = ring_0.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(30, 63).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r5 = ring_0.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(64, 14).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.1436F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r6 = ring_0.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(64, 7).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 2.0F, 5.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.1436F, 0.0F, 0.6603F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r7 = ring_0.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(64, 0).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5718F, 0.0F, 0.9904F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r8 = ring_0.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(60, 63).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 2.0F, 5.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.6603F, 0.0F, -2.0944F, 0.0F));

		PartDefinition ring_1 = overhead.addOrReplaceChild("ring_1", CubeListBuilder.create().texOffs(32, 45).addBox(-5.5718F, -2.5F, -8.3301F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.91F)), PartPose.offset(0.6468F, -12.5F, 0.9801F));

		PartDefinition cube_r9 = ring_1.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(62, 41).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.5706F, 0.0F, -0.3294F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r10 = ring_1.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(60, 54).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.573F, 0.0F, -0.3294F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r11 = ring_1.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(30, 54).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(-0.573F, 0.0F, -0.328F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r12 = ring_1.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(0, 54).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-0.5718F, 0.0F, -0.3273F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r13 = ring_1.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(48, 32).addBox(-5.0F, -2.5F, -8.0F, 10.0F, 4.0F, 5.0F, new CubeDeformation(-0.91F)), PartPose.offsetAndRotation(-0.5706F, 0.0F, -0.328F, 0.0F, -2.0944F, 0.0F));

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -16.0F, -8.0F, 16.0F, 16.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition platform = base.addOrReplaceChild("platform", CubeListBuilder.create().texOffs(0, 32).addBox(-6.0F, -0.5F, -6.0F, 12.0F, 1.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(0, 45).addBox(-4.0F, 0.5F, -4.0F, 8.0F, 1.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -17.5F, 0.0F));

		PartDefinition item = base.addOrReplaceChild("item", CubeListBuilder.create().texOffs(-4, -3).addBox(-2.5F, -15.0F, -2.5F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, 0.0F));

		PartDefinition controlpanel = base.addOrReplaceChild("controlpanel", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r14 = controlpanel.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(0, 83).addBox(-1.0F, -8.0F, 1.0F, 8.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(62, 99).addBox(-8.5F, -14.5F, -4.0F, 23.0F, 18.0F, 9.0F, new CubeDeformation(-5.0F)), PartPose.offsetAndRotation(-3.25F, -11.0F, -12.5F, -0.5672F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		getAnyDescendantWithName("item").ifPresent(m -> m.visible = false);
		fab.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		base.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(FabricatorTile tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.craftingAnimState, FabricatorAnimations.MANUFACTURE, ageInTicks);
	}
}