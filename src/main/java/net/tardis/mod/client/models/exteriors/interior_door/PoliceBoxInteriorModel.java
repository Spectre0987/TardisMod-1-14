package net.tardis.mod.client.models.exteriors.interior_door;// Made with Blockbench 4.10.4
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.client.animations.exterior.PoliceBoxExteriorAnimation;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class PoliceBoxInteriorModel<T extends InteriorDoorTile> extends BasicTileHierarchicalModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exterior/interior/police_box"), "main");
	private final ModelPart pillars;
	private final ModelPart door1;
	private final ModelPart door2;
	private final ModelPart signs;
	private final ModelPart roof;

	public PoliceBoxInteriorModel(ModelPart root) {
		super(root);
		this.pillars = root.getChild("pillars");
		this.door1 = root.getChild("door1");
		this.door2 = root.getChild("door2");
		this.signs = root.getChild("signs");
		this.roof = root.getChild("roof");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition pillars = partdefinition.addOrReplaceChild("pillars", CubeListBuilder.create().texOffs(0, 123).addBox(16.0F, -33.0F, -3.0F, 4.0F, 41.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 123).addBox(-4.0F, -33.0F, -3.0F, 4.0F, 41.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition door1 = partdefinition.addOrReplaceChild("door1", CubeListBuilder.create().texOffs(107, 43).addBox(-8.0F, -16.0F, -0.5F, 8.0F, 32.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 8.0F, 6.5F));

		PartDefinition door2 = partdefinition.addOrReplaceChild("door2", CubeListBuilder.create().texOffs(71, 74).addBox(0.0F, -15.0F, -0.5F, 8.0F, 32.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 7.0F, 6.5F));

		PartDefinition signs = partdefinition.addOrReplaceChild("signs", CubeListBuilder.create().texOffs(14, 157).addBox(-1.0F, -30.0F, -4.0F, 18.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		PartDefinition roof = partdefinition.addOrReplaceChild("roof", CubeListBuilder.create().texOffs(56, 158).addBox(0.0F, -25.0F, -3.0F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(13, 165).addBox(-2.0F, -34.0F, -1.0F, 20.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-8.0F, 16.0F, 8.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		pillars.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		signs.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		roof.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		PoliceBoxExteriorAnimation.animateDoors(tile.getDoorHandler().getDoorState(), this.door1, this.door2);
	}
}