package net.tardis.mod.client.models.exteriors;// Made with Blockbench 4.9.4
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class TTCapsuleExteriorModel<T extends ExteriorTile> extends BasicTileHierarchicalModel<T> implements IExteriorModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("tt_capsule"), "main");
	private final ModelPart top_rim;
	private final ModelPart roof;
	private final ModelPart walls;
	private final ModelPart base;
	private final ModelPart floor_rim;
	private final ModelPart boti;
	private final ModelPart door;

	public TTCapsuleExteriorModel(ModelPart root) {
		super(root, RenderType::entityTranslucent);
		this.top_rim = root.getChild("top_rim");
		this.roof = root.getChild("roof");
		this.walls = root.getChild("walls");
		this.base = root.getChild("base");
		this.floor_rim = root.getChild("floor_rim");
		this.boti = root.getChild("boti");
		this.door = root.getChild("door");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition top_rim = partdefinition.addOrReplaceChild("top_rim", CubeListBuilder.create(), PartPose.offset(0.0F, -21.0F, 0.0F));

		PartDefinition rim3 = top_rim.addOrReplaceChild("rim3", CubeListBuilder.create(), PartPose.offset(-0.2994F, 0.0F, 1.7094F));

		PartDefinition bone13 = rim3.addOrReplaceChild("bone13", CubeListBuilder.create().texOffs(55, 5).addBox(-4.198F, -1.915F, -13.4973F, 9.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r1 = bone13.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(102, 5).addBox(-2.5F, -2.0F, -14.0F, 4.0F, 7.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.0366F, 0.1F, -0.1366F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone14 = rim3.addOrReplaceChild("bone14", CubeListBuilder.create().texOffs(37, 3).addBox(-5.8412F, -1.915F, -12.8973F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r2 = bone14.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(59, 3).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.6798F, -0.1F, 0.4634F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone15 = rim3.addOrReplaceChild("bone15", CubeListBuilder.create().texOffs(71, 3).addBox(-6.1432F, -1.915F, -11.1742F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r3 = bone15.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(93, 3).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.9818F, -0.1F, 2.1865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone16 = rim3.addOrReplaceChild("bone16", CubeListBuilder.create().texOffs(105, 3).addBox(-4.802F, -1.915F, -10.0512F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r4 = bone16.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(59, 3).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.6406F, -0.1F, 3.3095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone17 = rim3.addOrReplaceChild("bone17", CubeListBuilder.create().texOffs(105, 2).addBox(-3.1588F, -1.915F, -10.6512F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r5 = bone17.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(95, 3).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(1.0026F, -0.1F, 2.7095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone18 = rim3.addOrReplaceChild("bone18", CubeListBuilder.create().texOffs(71, 3).addBox(-2.8568F, -1.915F, -12.3742F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r6 = bone18.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(92, 5).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 7.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(1.3046F, -0.1F, 0.9865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition roof = partdefinition.addOrReplaceChild("roof", CubeListBuilder.create().texOffs(72, 4).addBox(-12.2F, -1.85F, -8.45F, 17.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(78, 1).addBox(-12.2F, -1.85F, 6.35F, 17.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(51, 4).addBox(-13.1F, -1.85F, -6.65F, 19.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(51, 1).addBox(-13.1F, -1.85F, 4.55F, 18.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(77, 0).addBox(-14.0F, -1.85F, 2.75F, 20.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(69, 3).addBox(-14.0F, -1.85F, -4.85F, 21.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(54, 0).addBox(-15.0F, -1.85F, -3.05F, 22.0F, 2.0F, 6.0F, new CubeDeformation(-0.1F))
		.texOffs(51, 4).addBox(-11.0F, -1.85F, -10.25F, 14.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(51, 1).addBox(-11.0F, -1.85F, 8.15F, 14.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F)), PartPose.offset(3.8355F, -21.0F, -0.065F));

		PartDefinition walls = partdefinition.addOrReplaceChild("walls", CubeListBuilder.create(), PartPose.offset(0.0F, 22.0F, 0.0F));

		PartDefinition rim2 = walls.addOrReplaceChild("rim2", CubeListBuilder.create(), PartPose.offset(-0.2994F, 0.0F, 1.7094F));

		PartDefinition bone8 = rim2.addOrReplaceChild("bone8", CubeListBuilder.create().texOffs(37, 4).addBox(-5.8412F, -42.915F, -12.8973F, 9.0F, 43.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r7 = bone8.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(59, 4).addBox(-2.5F, -42.8F, -14.0F, 4.0F, 43.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.6798F, -0.1F, 0.4634F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone9 = rim2.addOrReplaceChild("bone9", CubeListBuilder.create().texOffs(71, 4).addBox(-6.1432F, -42.915F, -11.1742F, 9.0F, 43.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r8 = bone9.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(93, 4).addBox(-2.5F, -42.8F, -14.0F, 4.0F, 43.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.9818F, -0.1F, 2.1865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone10 = rim2.addOrReplaceChild("bone10", CubeListBuilder.create().texOffs(105, 4).addBox(-4.802F, -42.915F, -10.0512F, 9.0F, 43.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r9 = bone10.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(59, 4).addBox(-2.5F, -42.8F, -14.0F, 4.0F, 43.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.6406F, -0.1F, 3.3095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone11 = rim2.addOrReplaceChild("bone11", CubeListBuilder.create().texOffs(37, 4).addBox(-3.1588F, -42.915F, -10.6512F, 9.0F, 43.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r10 = bone11.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(59, 4).addBox(-2.5F, -42.8F, -14.0F, 4.0F, 43.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(1.0026F, -0.1F, 2.7095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone12 = rim2.addOrReplaceChild("bone12", CubeListBuilder.create().texOffs(71, 4).addBox(-2.8568F, -42.915F, -12.3742F, 9.0F, 43.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(59, 46).addBox(-12.2F, -1.85F, -8.45F, 17.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(59, 44).addBox(-12.2F, -1.85F, 6.35F, 17.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(59, 46).addBox(-13.1F, -1.85F, -6.65F, 19.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(72, 45).addBox(-13.1F, -1.85F, 4.55F, 18.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(62, 45).addBox(-14.0F, -1.85F, 2.75F, 20.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(59, 47).addBox(-14.0F, -1.85F, -4.85F, 21.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(56, 45).addBox(-15.0F, -1.85F, -3.05F, 22.0F, 2.0F, 6.0F, new CubeDeformation(-0.1F))
		.texOffs(47, 47).addBox(-9.0F, -1.85F, -10.25F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(59, 44).addBox(-11.0F, -1.85F, 8.15F, 14.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F)), PartPose.offset(3.8355F, 24.0F, -0.065F));

		PartDefinition floor_rim = partdefinition.addOrReplaceChild("floor_rim", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition rim = floor_rim.addOrReplaceChild("rim", CubeListBuilder.create(), PartPose.offset(-0.2994F, 0.0F, 1.7094F));

		PartDefinition bone = rim.addOrReplaceChild("bone", CubeListBuilder.create().texOffs(68, 49).addBox(-4.198F, -1.915F, -12.4973F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r11 = bone.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(59, 49).addBox(-2.5F, -2.0F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.0366F, 0.075F, 0.8634F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone2 = rim.addOrReplaceChild("bone2", CubeListBuilder.create().texOffs(37, 49).addBox(-5.8412F, -1.915F, -12.8973F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r12 = bone2.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(59, 49).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.6798F, -0.1F, 0.4634F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone3 = rim.addOrReplaceChild("bone3", CubeListBuilder.create().texOffs(71, 49).addBox(-6.1432F, -1.915F, -11.1742F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r13 = bone3.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(93, 49).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-1.9818F, -0.1F, 2.1865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone4 = rim.addOrReplaceChild("bone4", CubeListBuilder.create().texOffs(105, 49).addBox(-4.802F, -1.915F, -10.0512F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition cube_r14 = bone4.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(59, 49).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.6406F, -0.1F, 3.3095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone5 = rim.addOrReplaceChild("bone5", CubeListBuilder.create().texOffs(37, 49).addBox(-3.1588F, -1.915F, -10.6512F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition cube_r15 = bone5.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(93, 49).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(1.0026F, -0.1F, 2.7095F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone6 = rim.addOrReplaceChild("bone6", CubeListBuilder.create().texOffs(105, 49).addBox(-2.8568F, -1.915F, -12.3742F, 9.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r16 = bone6.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(59, 49).addBox(-2.5F, -1.8F, -14.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(0.4386F, -0.125F, 1.4865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -16.0F, -9.0F, 16.0F, 16.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-8.0F, -30.0F, -9.0F, 16.0F, 14.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-8.0F, -46.0F, -9.0F, 16.0F, 16.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 1.0F));

		PartDefinition door = partdefinition.addOrReplaceChild("door", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, -1.0F));

		PartDefinition bone19 = door.addOrReplaceChild("bone19", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.2994F, -2.0F, 2.7094F, 0.0F, 1.0472F, 0.0F));

		PartDefinition cube_r17 = bone19.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(59, 11).addBox(-2.5F, -37.8F, -14.0F, 4.0F, 38.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(0.4386F, -0.105F, 1.4865F, 0.0F, -0.5236F, 0.0F));

		PartDefinition bone7 = door.addOrReplaceChild("bone7", CubeListBuilder.create().texOffs(71, 11).addBox(-4.198F, -37.915F, -12.4973F, 9.0F, 38.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.2994F, -2.0F, 2.7094F));

		PartDefinition cube_r18 = bone7.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(93, 11).addBox(-2.5F, -38.0F, -14.0F, 4.0F, 38.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offsetAndRotation(-0.0366F, 0.095F, 0.8634F, 0.0F, -0.5236F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		top_rim.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		roof.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		walls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		base.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		floor_rim.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		door.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		this.door.offsetRotation(AnimationHelper.createRadianFromDegrees(0, tile.getDoorHandler().getDoorState().isOpen() ? 70 : 0, 0));
	}

	@Override
	public void animateDemat(T exterior, float age) {

	}

	@Override
	public void animateRemat(T exterior, float age) {

	}

	@Override
	public void animateSolid(T exterior, float age) {

	}
}