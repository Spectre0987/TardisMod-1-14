package net.tardis.mod.client.models;

import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.entity.BlockEntity;

import java.util.function.Function;

public abstract class BasicTileHierarchicalModel<T extends BlockEntity> extends HierarchicalModel<Entity> implements IAnimatableTileModel<T> {

    final ModelPart root;

    public BasicTileHierarchicalModel(ModelPart root){
        this(root, RenderType::entityCutoutNoCull);
    }

    public BasicTileHierarchicalModel(ModelPart root, Function<ResourceLocation, RenderType> renderTypeFunction){
        super(renderTypeFunction);
        this.root = root;
    }

    @Override
    public ModelPart root() {
        return root;
    }

    @Override
    public void setupAnim(Entity pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch) {

    }
}
