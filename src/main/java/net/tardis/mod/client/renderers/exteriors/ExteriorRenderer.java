package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.datafixers.util.Pair;
import com.mojang.math.Axis;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.client.animations.demat.ClassicDematAnim;
import net.tardis.mod.client.animations.demat.DematAnimation;
import net.tardis.mod.client.animations.demat.ParticleAnimation;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.client.models.exteriors.IExteriorModel;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.registry.DematAnimationRegistry;
import org.jetbrains.annotations.Nullable;
import org.joml.Matrix4f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class ExteriorRenderer<T extends ExteriorTile, M extends BasicTileHierarchicalModel<T> & IExteriorModel<T>> implements BlockEntityRenderer<T> {

    public final M model;
    private static final HashMap<DematAnimationRegistry.DematAnimationType, DematAnimation> DEMAT_ANIMATIONS = new HashMap<>();
    private static final List<Pair<Predicate<Biome>, Vector4f>> BIOME_DUSTS_COLOR = new ArrayList<>();

    public ExteriorRenderer(BlockEntityRendererProvider.Context context){
        this.model = this.bakeModel(context.getModelSet());
    }

    public abstract M bakeModel(EntityModelSet set);
    public abstract ResourceLocation getTexture(T exterior);

    public static void registerSnowLayer(Predicate<Biome> biome, Vector4f color){
        BIOME_DUSTS_COLOR.add(new Pair<>(biome, color));
    }

    public static DematAnimation getAnimationType(@Nullable DematAnimationRegistry.DematAnimationType type){
        if(type == null || !DEMAT_ANIMATIONS.containsKey(type))
            return new ClassicDematAnim();
        return DEMAT_ANIMATIONS.get(type);
    }

    public static void registerDematAnimation(DematAnimationRegistry.DematAnimationType type, DematAnimation anim){
        DEMAT_ANIMATIONS.put(type, anim);
    }

    @Override
    public void render(T pBlockEntity, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.ZP.rotationDegrees(180));

        if(pBlockEntity.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
            pose.mulPose(Axis.YP.rotationDegrees(pBlockEntity.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING).toYRot() - 180));
        }

        this.model.animateSolid(pBlockEntity, pPartialTick);
        this.model.setupAnimations(pBlockEntity, pPartialTick);

        final DematAnimation anim = getAnimationType(pBlockEntity.getMatterStateHandler().getDematType());

        float[] animColors = pBlockEntity.getMatterStateHandler().getMatterState() == MatterState.SOLID ? new float[]{1, 1, 1, 1} :
                anim.getColors(pBlockEntity.getMatterStateHandler(), pPartialTick);

        if(pBlockEntity.getMatterStateHandler().getMatterState() != MatterState.SOLID){
            anim.modifyPose(pose, pBlockEntity.getMatterStateHandler(), pPartialTick);
        }

        //Handle 'Snow' shader
        Optional<ResourceLocation> texVar = pBlockEntity.getTextureVariant().map(id -> ClientRegistry.getTextureVariant(id)).map(Optional::get);
        ObjectHolder<RenderType> type = new ObjectHolder<>(this.model.renderType(texVar.orElse(getTexture(pBlockEntity))));
        /*
        getColor(getCurrentBiome(pBlockEntity)).ifPresent(color -> {
            type.set(TardisRenderTypes.SNOW_TYPE.apply(texVar.orElse(getTexture(pBlockEntity)),
                    color));
        });

         */

        this.model.renderToBuffer(pose, pBufferSource.getBuffer(type.get()), pPackedLight, pPackedOverlay,
                animColors[0], animColors[1], animColors[2], animColors[3]);

        if(pBlockEntity.getMatterStateHandler().getMatterState() != MatterState.SOLID) {
            anim.renderExtra(pose, pBlockEntity.getMatterStateHandler(), this.model, pBufferSource, pPartialTick, pPackedLight, pPackedOverlay);
        }
        pose.popPose();
    }

    public Optional<Vector4f> getColor(Optional<Biome> biome){
        if(biome.isEmpty()){
            return Optional.empty();
        }

        for(Pair<Predicate<Biome>, Vector4f> color : BIOME_DUSTS_COLOR){
            if(color.getFirst().test(biome.get())){
                return Optional.of(color.getSecond());
            }
        }

        return Optional.empty();

    }

    public <T extends ExteriorTile> Optional<Biome> getCurrentBiome(T tile){
        if(tile.getLevel() != null){
            return Optional.of(tile.getLevel().getBiome(tile.getBlockPos()).get());
        }
        return Optional.empty();
    }

    @Override
    public int getViewDistance() {
        return 512;
    }
}
