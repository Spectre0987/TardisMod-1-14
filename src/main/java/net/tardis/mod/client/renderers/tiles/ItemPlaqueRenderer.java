package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemDisplayContext;
import net.tardis.mod.blockentities.PlaqueTile;
import net.tardis.mod.client.renderers.WorldText;
import net.tardis.mod.helpers.WorldHelper;

import java.util.ArrayList;
import java.util.List;

public class ItemPlaqueRenderer implements BlockEntityRenderer<PlaqueTile> {

    public ItemPlaqueRenderer(BlockEntityRendererProvider.Context context){

    }

    @Override
    public void render(PlaqueTile tile, float v, PoseStack poseStack, MultiBufferSource multiBufferSource, int i, int i1) {
        if(!tile.getItem().isEmpty()){
            poseStack.pushPose();
            poseStack.translate(0.5, 0.5, 0.5);
            poseStack.mulPose(Axis.YP.rotationDegrees(WorldHelper.getFacingAngle(tile.getBlockState())));
            poseStack.translate(0, 0, 0.4);

            poseStack.pushPose();
            poseStack.scale(0.3F, 0.3F, 0.3F);
            Minecraft.getInstance().getItemRenderer().renderStatic(
                    tile.getItem(),
                    ItemDisplayContext.FIXED,
                    i, i1, poseStack,
                    multiBufferSource,
                    tile.getLevel(), 0
            );
            poseStack.popPose();

            final List<Component> textList = new ArrayList<>();
            textList.add(tile.getItem().getDisplayName());

            final WorldText text = new WorldText(5 / 16.0F, 0.2F).minXScale(0.25F);
            poseStack.pushPose();
            poseStack.mulPose(Axis.XP.rotationDegrees(22.5F));
            poseStack.translate(0.17, -0.2, 0.1);
            poseStack.mulPose(Axis.ZP.rotationDegrees(180));
            text.renderText(textList, poseStack, multiBufferSource);
            poseStack.popPose();

            poseStack.popPose();
        }
    }
}
