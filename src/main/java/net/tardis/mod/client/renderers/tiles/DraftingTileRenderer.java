package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.WorldHelper;

public class DraftingTileRenderer implements BlockEntityRenderer<DraftingTableTile> {

    public DraftingTileRenderer(BlockEntityRendererProvider.Context context){

    }

    @Override
    public void render(DraftingTableTile tile, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        //If this has a result, show that
        pose.pushPose();
        pose.translate(0.5, 1, 0.5);
        Direction facing = WorldHelper.getHorizontalFacing(tile.getBlockState());
        pose.mulPose(Axis.YP.rotationDegrees(facing.toYRot() + 180));
        if(facing.getAxis() == Direction.Axis.X)
            pose.mulPose(Axis.YP.rotationDegrees(180));
        pose.scale(0.333F, 0.333F, 0.333F);
        if(!tile.getItem(9).isEmpty()){
            pose.mulPose(Axis.XP.rotationDegrees(90));
            Minecraft.getInstance().getItemRenderer().renderStatic(tile.getItem(9), ItemDisplayContext.FIXED, pPackedLight, pPackedOverlay, pose, pBufferSource, tile.getLevel(), 0);
        }
        else{
            for(int i = 0; i < 9; ++i){
                final float startX = -0.75F, startY = -0.75F;
                pose.pushPose();
                pose.translate(startX + (i / 3) * 0.75, 0, startY + (i % 3) * 0.75);

                //pose.mulPose(Axis.YP.rotationDegrees(180));
                pose.mulPose(Axis.XP.rotationDegrees(90));

                ItemStack stack = tile.getItem(i);
                if(!stack.isEmpty()) {
                    Minecraft.getInstance().getItemRenderer().renderStatic(stack, ItemDisplayContext.FIXED, pPackedLight, pPackedOverlay, pose, pBufferSource, tile.getLevel(), 0);
                }
                pose.popPose();
            }
        }
        pose.popPose();
    }
}
