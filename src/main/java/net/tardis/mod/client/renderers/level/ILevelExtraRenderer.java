package net.tardis.mod.client.renderers.level;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Camera;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.RenderLevelStageEvent;
import org.joml.Matrix4f;

@OnlyIn(Dist.CLIENT)
public interface ILevelExtraRenderer {

    void render(LevelRenderer renderer, PoseStack pose, Matrix4f projMat, Camera cam, int renderTick, float partialTicks);

    RenderLevelStageEvent.Stage getStage();

    boolean shouldRender(LevelRenderer renderer, Camera cam, Frustum frustum);

    void clientTick();
}
