package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.TTCapsuleExteriorTile;
import net.tardis.mod.client.models.exteriors.TTCapsuleExteriorModel;
import net.tardis.mod.helpers.Helper;

public class TTCapsuleExteriorRenderer extends ExteriorRenderer<TTCapsuleExteriorTile, TTCapsuleExteriorModel<TTCapsuleExteriorTile>>{

    public static final ResourceLocation DEFAULT_TEXTURE = Helper.createRL("textures/tiles/exteriors/tt_capsule/basic_shell.png");

    public TTCapsuleExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public TTCapsuleExteriorModel<TTCapsuleExteriorTile> bakeModel(EntityModelSet set) {
        return new TTCapsuleExteriorModel<>(set.bakeLayer(TTCapsuleExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(TTCapsuleExteriorTile exterior) {
        return DEFAULT_TEXTURE;
    }
}
