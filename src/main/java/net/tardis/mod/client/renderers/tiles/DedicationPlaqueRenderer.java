package net.tardis.mod.client.renderers.tiles;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.DedicationPlaqueTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.renderers.WorldText;
import net.tardis.mod.helpers.ClientHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.resource_listener.server.TardisNames;

import java.util.List;

public class DedicationPlaqueRenderer implements BlockEntityRenderer<DedicationPlaqueTile> {

    public static WorldText text = new WorldText(14F / 16.0F, 4 / 16.0F)
            .withColor(0x9F8800);

    public static final Component LINE_1 = Component.translatable(makeTrans("line_1"))
            .withStyle(Style.EMPTY.withFont(Helper.createRL("fancy")));
    public static final Component LINE_2 = Component.translatable(makeTrans("line_2"))
            .withStyle(Style.EMPTY.withFont(Helper.createRL("rr")));

    public DedicationPlaqueRenderer(BlockEntityRendererProvider.Context context){

    }

    @Override
    public void render(DedicationPlaqueTile pBlockEntity, float pPartialTick, PoseStack pPoseStack, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {

        if(pBlockEntity.getLevel() != null){
            pPoseStack.pushPose();
            pPoseStack.translate(0.5, 0.5, 0.5);
            pPoseStack.mulPose(Axis.XP.rotationDegrees(180));
            pPoseStack.mulPose(Axis.YP.rotationDegrees(
                            WorldHelper.getHorizontalFacing(pBlockEntity.getBlockState()).toYRot())
            );
            ClientHelper.translatePixel(pPoseStack, -6.9, -1.25, 6.8);
            pBlockEntity.getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

                final List<Component> transList = Lists.newArrayList(
                        LINE_1
                );
                if(TardisNames.CLIENT_NAMES.containsKey(tardis.getLevel().dimension()))
                    transList.add(Component.literal(TardisNames.CLIENT_NAMES.get(tardis.getLevel().dimension())));
                transList.add(LINE_2);

                text.renderText(transList, pPoseStack, pBufferSource);
            });
            pPoseStack.popPose();
        }

    }

    public static String makeTrans(String name){
        return "dedication_plaque." + Tardis.MODID + "." + name;
    }
}
