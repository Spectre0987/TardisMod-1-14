package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.client.models.IAnimatableTileModel;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.JsonRegistries;

import java.util.Optional;
import java.util.function.Function;

public class InteriorDoorRender<M extends BasicTileHierarchicalModel<InteriorDoorTile> & IAnimatableTileModel<InteriorDoorTile>> {

    public final ResourceLocation texture;
    public final Function<EntityModelSet, M> modelFactory;
    public M model;

    public InteriorDoorRender(ResourceLocation texture, Function<EntityModelSet, M> model){
        this.texture = texture;
        this.modelFactory = model;
    }

    public ResourceLocation getTexture(ITardisLevel tardis){
        Optional<ResourceLocation> exteriorVariant = tardis.getExteriorExtraData().getExteriorTexVariant()
                .map(id -> Minecraft.getInstance().level.registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).get(id))
                .map(var -> ClientRegistry.getTextureVariant(var).orElse(texture));
        return exteriorVariant.orElse(this.texture);
    }

    public M getModel(){
        return this.model;
    }

    public void bake(EntityModelSet set){
        this.model = this.modelFactory.apply(set);
    }

    public void render(ITardisLevel tardis, InteriorDoorTile tile, PoseStack pose, MultiBufferSource buffer, float ageInTicks, int packedLight, int packedOverlay){
        this.preRender(tardis, tile, pose, ageInTicks);
        float[] colors = this.getColors();
        this.model.renderToBuffer(pose, buffer.getBuffer(this.model.renderType(this.getTexture(tardis))), packedLight, packedOverlay, colors[0], colors[1], colors[2], colors[3]);

    }

    /**
     * Translate, Edit, or animate the interior door here
     * @param tardis
     * @param pose
     */
    public void preRender(ITardisLevel tardis, InteriorDoorTile tile, PoseStack pose, float ageInTicks){
        this.model.setupAnimations(tile, ageInTicks);
    }

    /**
     *
     * @return MUST return an array of AT LEAST 4
     * Format: Red, Green, Blue, Alpha
     */
    public float[] getColors(){
        return new float[]{1.0F, 1.0F, 1.0F, 1.0F};
    }

}
