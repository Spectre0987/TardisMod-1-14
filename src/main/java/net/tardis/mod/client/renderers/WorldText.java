package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.FormattedText;
import net.minecraft.util.Mth;

import java.util.List;


public class WorldText {

    public static final float BASE_SCALE = 0.01F;
    public int pixelPadding = 0;
    public int color = 0x000000;
    public boolean dropShadow = false;
    private Font font;
    public float width, height;
    private float minXScale = 0.5F;

    public WorldText(float width, float height){
        this.width = width;
        this.height = height;
        this.font = Minecraft.getInstance().font;
    }

    public WorldText minXScale(float scale){
        this.minXScale = scale;
        return this;
    }

    public void renderText(List<Component> text, PoseStack pose, MultiBufferSource source, int packedLight){
        pose.pushPose();
        final float baseScale = BASE_SCALE * (this.width / 1.0F);
        pose.scale(baseScale, baseScale, baseScale);
        float usedHeight = 0;
        for(int i = 0; i < text.size(); ++i){

            final FormattedText formattedEntry = text.get(i);

            pose.pushPose();
            float lineWidth = font.width(text.get(i).getVisualOrderText()) * baseScale;

            //Scale line to fit on monitor
            pose.translate(0, usedHeight, 0);
            float scale = 1.0F;
            if(lineWidth > this.width){
                scale = Mth.clamp(this.width / lineWidth, this.minXScale, 1.0F);
                lineWidth *= scale;
                pose.scale(scale, scale, scale);
            }
            //split line if it still doesn't fit
            if(lineWidth > this.width){
                int maxHeight = Mth.floor(this.width * 16.0F * 16.0F);
                font.drawWordWrap(pose, formattedEntry, 0, 0, maxHeight, color);
                usedHeight += font.wordWrapHeight(formattedEntry, maxHeight) * scale;
            }
            else {
                this.font.drawInBatch(text.get(i), 0, 0, color, dropShadow, pose.last().pose(), source, Font.DisplayMode.NORMAL, 0x0000000, packedLight);
                usedHeight += font.lineHeight * scale;
            }
            pose.popPose();
        }
        pose.popPose();
    }

    public float scaleY(Font font, List<Component> components){

        float spaceUsed = 1.0F;
        for(Component component : components){
            if(font.width(component) * BASE_SCALE > this.width){

            }
            spaceUsed = font.lineHeight;
        }
        return spaceUsed < height ? spaceUsed : (this.height / spaceUsed);
    }

    public void splitText(Component comp){

    }

    public void renderText(List<Component> text, PoseStack matrix, MultiBufferSource source){
        this.renderText(text, matrix, source, LightTexture.FULL_BRIGHT);
    }


    public WorldText withColor(int color){
        this.color = color;
        return this;
    }

    public WorldText withPixelPadding(int pixels){
        this.pixelPadding = pixels;
        return this;
    }

    public WorldText withDropShadow(boolean shadow){
        this.dropShadow = shadow;
        return this;
    }

    public enum ClipType{
        SCALE,
        CLIP;
    }
}
