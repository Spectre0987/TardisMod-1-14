package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.IHaveBotiPortal;
import net.tardis.mod.client.models.exteriors.interior_door.TTCapsuleInteriorDoorModel;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.registry.ExteriorRegistry;

import java.util.HashMap;

public class ChameleonInteriorDoorRenderer implements BlockEntityRenderer<InteriorDoorTile> {

    /**
     * Register these in FMLClientSetup
     */
    protected static final HashMap<ExteriorType, InteriorDoorRender<?>> RENDERS = new HashMap<>();

    public final TTCapsuleInteriorDoorModel DEFAULT_MODEL;
    public static final ResourceLocation DEFAULT_DOOR_TEX = Helper.createRL("textures/tiles/exteriors/tt_capsule/basic_shell.png");


    public ChameleonInteriorDoorRenderer(BlockEntityRendererProvider.Context set){
        DEFAULT_MODEL = new TTCapsuleInteriorDoorModel(set.bakeLayer(TTCapsuleInteriorDoorModel.LAYER_LOCATION));
        //Bake all registered doors
        for(InteriorDoorRender<?> renderer : RENDERS.values()) {
            renderer.bake(set.getModelSet());
        }
    }


    @Override
    public void render(InteriorDoorTile door, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.ZP.rotationDegrees(180));
        pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getDegreeFromRotation(WorldHelper.getHorizontalFacing(door.getBlockState()))));

        door.getLevel().getCapability(Capabilities.TARDIS).ifPresent(cap -> {
            InteriorDoorRender<?> renderer = RENDERS.get(cap.getExterior().getType());
            if(renderer != null){

                renderer.preRender(cap, door, pose, pPartialTick);
                renderer.render(cap, door, pose, source, pPartialTick, pPackedLight, pPackedOverlay);
                if(renderer.model instanceof IHaveBotiPortal portal){
                    VertexConsumer consumer = source.getBuffer(cap.isInVortex() ? TardisRenderTypes.VORTEX : renderer.model.renderType(renderer.getTexture(cap)));
                    portal.getBotiWindow().render(pose, consumer, pPackedLight, pPackedOverlay);
                }
            }

        });

        if(!Capabilities.getCap(Capabilities.TARDIS, door.getLevel()).isPresent()){
            DEFAULT_MODEL.renderToBuffer(pose, source.getBuffer(DEFAULT_MODEL.renderType(DEFAULT_DOOR_TEX)), pPackedLight, pPackedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
        }

        pose.popPose();
    }

    public static void registerChameleonDoor(ExteriorType type, InteriorDoorRender<?> renderer){
        RENDERS.put(type, renderer);
    }

}
