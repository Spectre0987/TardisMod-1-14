package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.tardis.mod.blockentities.EngineTile;
import net.tardis.mod.client.ModelHolder;
import net.tardis.mod.client.models.engines.SteamEngineModel;
import net.tardis.mod.helpers.Helper;

import java.time.format.ResolverStyle;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class EngineRenderer<M extends Model> implements BlockEntityRenderer<EngineTile> {

    public static final Map<Block, ModelHolder<? extends Block, ?>> models = new HashMap<>();

    public static final ResourceLocation FALLBACK_TEX = Helper.createRL("textures/tiles/engine/steam.png");
    public final SteamEngineModel fallback;

    public EngineRenderer(BlockEntityRendererProvider.Context context){

        for(ModelHolder<?, ?> model : models.values()){
            model.bake(context.getModelSet());
        }

        this.fallback = new SteamEngineModel(context.bakeLayer(SteamEngineModel.LAYER_LOCATION));

    }

    @Override
    public void render(EngineTile pBlockEntity, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.ZP.rotationDegrees(180));

        if(models.containsKey(pBlockEntity.getBlockState().getBlock())){
            final ModelHolder<? extends Block, ?> model = models.get(pBlockEntity.getBlockState().getBlock());
            model.applyTranslations(pose);
            model.getModel().ifPresent(m -> {
                m.renderToBuffer(pose, pBufferSource.getBuffer(model.getRenderType()), pPackedLight, pPackedOverlay,
                        1.0F, 1.0F, 1.0F, 1.0F);
            });
        }
        else{
            //Fall back to the steam
            this.fallback.renderToBuffer(pose, pBufferSource.getBuffer(fallback.renderType(FALLBACK_TEX)), pPackedLight, pPackedOverlay,
                        1.0F, 1.0F,1.0F,1.0F
                    );
        }

        pose.popPose();
    }

}
