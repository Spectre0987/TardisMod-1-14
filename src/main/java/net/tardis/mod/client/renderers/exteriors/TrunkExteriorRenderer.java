package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.TrunkExteriorTile;
import net.tardis.mod.client.models.exteriors.TrunkExteriorModel;
import net.tardis.mod.helpers.Helper;

public class TrunkExteriorRenderer extends ExteriorRenderer<TrunkExteriorTile, TrunkExteriorModel<TrunkExteriorTile>>{

    public static final ResourceLocation  TEXTURE = Helper.createRL("textures/tiles/exteriors/trunk/trunk_exterior_tropical.png");

    public TrunkExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public TrunkExteriorModel<TrunkExteriorTile> bakeModel(EntityModelSet set) {
        return new TrunkExteriorModel<>(set.bakeLayer(TrunkExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(TrunkExteriorTile exterior) {
        return TEXTURE;
    }
}
