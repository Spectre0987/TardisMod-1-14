package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.client.models.exteriors.IExteriorModel;
import net.tardis.mod.client.models.exteriors.PoliceBoxExteriorModel;
import net.tardis.mod.helpers.Helper;

public class PoliceBoxExteriorRenderer<T extends ExteriorTile, M extends BasicTileHierarchicalModel<T> & IExteriorModel<T>> extends ExteriorRenderer<T, M> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/exteriors/policebox.png");


    public PoliceBoxExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public M bakeModel(EntityModelSet set) {
        return (M)new PoliceBoxExteriorModel<T>(set.bakeLayer(PoliceBoxExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(T exterior) {
        return TEXTURE;
    }
}