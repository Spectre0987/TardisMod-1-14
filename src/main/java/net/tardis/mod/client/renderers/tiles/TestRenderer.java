package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.tardis.mod.blockentities.RendererTestTile;
import net.tardis.mod.client.TardisRenderTypes;
import org.joml.Matrix4f;

public class TestRenderer implements BlockEntityRenderer<RendererTestTile> {

    public TestRenderer(BlockEntityRendererProvider.Context context){}

    @Override
    public void render(RendererTestTile tile, float pPartialTick, PoseStack pPoseStack, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {

        VertexConsumer consumer = pBufferSource.getBuffer(TardisRenderTypes.FORCEFIELD.apply(pPartialTick));
        Matrix4f last = pPoseStack.last().pose();
        consumer.vertex(last, 0, 0, 0).endVertex();
        consumer.vertex(last, 0, 4, 0).endVertex();
        consumer.vertex(last, 4, 4, 0).endVertex();
        consumer.vertex(last, 4, 0, 0).endVertex();

    }
}
