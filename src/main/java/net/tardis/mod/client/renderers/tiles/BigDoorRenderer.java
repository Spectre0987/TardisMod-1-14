package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.BigDoorTile;
import net.tardis.mod.client.models.SlidingDoorModel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import org.joml.Vector3f;

public class BigDoorRenderer implements BlockEntityRenderer<BigDoorTile> {

    final SlidingDoorModel<BigDoorTile> model;
    final ResourceLocation texture;

    public BigDoorRenderer(BlockEntityRendererProvider.Context context, ResourceLocation texture){
        model = new SlidingDoorModel<>(context.bakeLayer(SlidingDoorModel.LAYER_LOCATION));
        this.texture = texture;
    }

    @Override
    public void render(BigDoorTile tile, float v, PoseStack pose, MultiBufferSource multiBufferSource, int i, int i1) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getFacingAngle(tile.getBlockState())));
        pose.mulPose(Axis.ZP.rotationDegrees(180));
        this.model.setupAnimations(tile, tile.getLevel().getGameTime() + v);
        this.model.renderToBuffer(pose, multiBufferSource.getBuffer(this.model.renderType(this.texture)), i, i1, 1.0F, 1.0F, 1.0F, 1.0F);
        pose.popPose();
    }
}
