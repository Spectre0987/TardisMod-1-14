package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.EntityHitResult;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helpers.InventoryHelper;
import net.tardis.mod.tags.ItemTags;

public class RendererControl extends EntityRenderer<ControlEntity<?>> {

    public RendererControl(EntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public ResourceLocation getTextureLocation(ControlEntity<?> controlEntity) {
        return null;
    }

    @Override
    public boolean shouldRender(ControlEntity<?> pLivingEntity, Frustum pCamera, double pCamX, double pCamY, double pCamZ) {
        if(super.shouldRender(pLivingEntity, pCamera, pCamX, pCamY, pCamZ)){
            if(Minecraft.getInstance().hitResult instanceof EntityHitResult result && result.getEntity() == pLivingEntity){
                return InventoryHelper.isHolding(Minecraft.getInstance().player, stack -> stack.is(ItemTags.SHOW_CONTROL_ITEMS));
            }
        }
        return false;
    }

    @Override
    public void render(ControlEntity<?> pEntity, float pEntityYaw, float pPartialTick, PoseStack pPoseStack, MultiBufferSource pBuffer, int pPackedLight) {
        super.render(pEntity, pEntityYaw, pPartialTick, pPoseStack, pBuffer, pPackedLight);
        this.renderNameTag(pEntity, Component.translatable(pEntity.getControl().getType().getTranslationKey()), pPoseStack, pBuffer, pPackedLight);
    }
}
