package net.tardis.mod.client.renderers.level;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.RenderLevelStageEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.registry.ParticleRegistry;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class RiftLevelRenderer implements ILevelExtraRenderer {

    public static final ResourceLocation RIFT_MASK_SMALL = Helper.createRL("textures/level/rift_large.png");

    @Override
    public void render(LevelRenderer renderer, PoseStack pose, Matrix4f projMat, Camera cam, int renderTick, float partialTicks) {

        final MultiBufferSource.BufferSource source = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());

        final RenderType largeRiftType = TardisRenderTypes.RIFT.apply(RIFT_MASK_SMALL);

        VertexConsumer consumer = source.getBuffer(largeRiftType);

        pose.pushPose();
        pose.translate(-cam.getPosition().x, -cam.getPosition().y, -cam.getPosition().z); //Move pos to origin

        getClientChunks(cam).forEach(p -> {

            p.getCapability(Capabilities.CHUNK).ifPresent(cap -> {
                if(cap.getRift().isPresent() && cap.getRift().get().isNaturallyFull()){
                    final Rift rift = cap.getRift().get();
                    pose.pushPose();
                    pose.translate(rift.getWorldPos().getX(), rift.getWorldPos().getY(), rift.getWorldPos().getZ());

                    final float size = 10;
                    final float rad = size / 2.0F;
                    consumer.vertex(pose.last().pose(), -rad, 0, 0).uv(0, 0).endVertex();
                    consumer.vertex(pose.last().pose(), rad, 0, 0).uv(1, 0).endVertex();
                    consumer.vertex(pose.last().pose(), rad, size, 0).uv(1, 1).endVertex();
                    consumer.vertex(pose.last().pose(), -rad, size, 0).uv(0, 1).endVertex();

                    pose.popPose();

                }
            });

        });

        pose.popPose();
        source.endBatch(largeRiftType);

    }

    @Override
    public RenderLevelStageEvent.Stage getStage() {
        return RenderLevelStageEvent.Stage.AFTER_SKY;
    }

    public List<LevelChunk> getClientChunks(Camera cam){
        final List<LevelChunk> levelChunks = new ArrayList<>();
        ChunkPos.rangeClosed(new ChunkPos(cam.getBlockPosition()), Minecraft.getInstance().options.renderDistance().get()).forEach(p -> {
            final LevelChunk c = Minecraft.getInstance().level.getChunk(p.x, p.z);
            if(c != null)
                levelChunks.add(c);
        });
        return levelChunks;
    }

    @Override
    public boolean shouldRender(LevelRenderer renderer, Camera cam, Frustum frustum) {

        if(Minecraft.getInstance().level == null)
            return false;

        final ObjectHolder<Boolean> shouldRender = new ObjectHolder<>(false);

        getClientChunks(cam).forEach(chunk -> {

            chunk.getCapability(Capabilities.CHUNK).ifPresent(cap -> {
                if(cap.getRift().isPresent()){
                    shouldRender.set(true);
                }
            });

        });


        return shouldRender.get();
    }

    @Override
    public void clientTick() {

        final RandomSource rand = Minecraft.getInstance().level.random;
        if(rand.nextFloat() > 0.1)
            return;

        ChunkPos.rangeClosed(new ChunkPos(Minecraft.getInstance().player.getOnPos()), 15).forEach(p -> {
            LevelChunk chunk = Minecraft.getInstance().level.getChunk(p.x, p.z);
            if(chunk != null){
                chunk.getCapability(Capabilities.CHUNK).ifPresent(cap -> {
                    if(cap.getRift().isPresent()){

                        final Rift rift = cap.getRift().get();

                        final Vec3 pos = new Vec3(
                                12 - rand.nextInt(25),
                                12 - rand.nextInt(25),
                                12 - rand.nextInt(25)
                        ).add(WorldHelper.centerOfBlockPos(rift.getWorldPos(), true));

                        final Vec3 speed = WorldHelper.centerOfBlockPos(rift.getWorldPos().above(3), true).subtract(pos)
                                .normalize().scale(0.05);

                        Minecraft.getInstance().level.addParticle(
                                ParticleRegistry.RIFT.get(),
                                pos.x, pos.y, pos.z,
                                speed.x, speed.y, speed.z
                        );
                    }
                });
            }
        });
    }
}
