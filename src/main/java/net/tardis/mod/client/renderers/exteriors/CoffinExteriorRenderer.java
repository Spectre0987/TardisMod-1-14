package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.CoffinExteriorTile;
import net.tardis.mod.client.models.exteriors.CoffinExteriorModel;
import net.tardis.mod.helpers.Helper;

public class CoffinExteriorRenderer extends ExteriorRenderer<CoffinExteriorTile, CoffinExteriorModel<CoffinExteriorTile>>{

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/exteriors/coffin_rustic.png");

    public CoffinExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public CoffinExteriorModel<CoffinExteriorTile> bakeModel(EntityModelSet set) {
        return new CoffinExteriorModel<>(set.bakeLayer(CoffinExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(CoffinExteriorTile exterior) {
        return TEXTURE;
    }
}
