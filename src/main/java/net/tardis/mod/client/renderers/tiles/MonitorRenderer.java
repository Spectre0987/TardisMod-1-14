package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.block.monitors.MonitorBlock;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.client.renderers.WorldText;
import net.tardis.mod.helpers.WorldHelper;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.BiConsumer;

public class MonitorRenderer<T extends BaseMonitorTile> implements BlockEntityRenderer<T> {

    private static final HashMap<Block, BiConsumer<BlockState, PoseStack>> ADDITIONAL_TRANSLATIONS = new HashMap<>();
    public final WorldText text;

    public MonitorRenderer(BlockEntityRendererProvider.Context context){
        this.text = new WorldText(1.0F, 1.0F)
                .withPixelPadding(1);
    }

    public static Optional<BiConsumer<BlockState, PoseStack>> getTranslation(Block monitor){
        if(ADDITIONAL_TRANSLATIONS.containsKey(monitor)){
            return Optional.of(ADDITIONAL_TRANSLATIONS.get(monitor));
        }
        return Optional.empty();
    }

    public static <T extends MonitorBlock<?>> void register(T block, BiConsumer<BlockState, PoseStack> consumer){
        ADDITIONAL_TRANSLATIONS.put(block, consumer);
    }

    public static void applyTranslations(BlockState monitor, PoseStack pose){
        getTranslation(monitor.getBlock()).ifPresent(c -> c.accept(monitor, pose));
    }

    @Override
    public void render(T tile, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        if(!tile.displayText())
            return;

        if(tile.getBounds().length < 2)
            throw new IllegalArgumentException("BaseMonitorTile#getBounds() must return an array with two or more objects!");

        this.text.width = tile.getBounds()[0];
        this.text.height = tile.getBounds()[1];
        this.text.color = tile.getTextColor();

        pose.pushPose();
        pose.translate(0.5, 0.5, 0.5);
        pose.mulPose(Axis.ZN.rotationDegrees(180));
        pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getHorizontalFacing(tile.getBlockState()).toYRot() + 180));
        pose.translate(-0.5, 0, -0.5);

        //Different for all monitor types
        pose.translate(2 / 16.0, -5.5 / 16.0, 2.9 / 16.0);

        applyTranslations(tile.getBlockState(), pose);
        this.text.renderText(tile.monitorText, pose, pBufferSource);
        pose.popPose();
    }
}
