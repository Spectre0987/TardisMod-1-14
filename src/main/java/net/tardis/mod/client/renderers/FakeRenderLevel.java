package net.tardis.mod.client.renderers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.Connection;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundSetActionBarTextPacket;
import net.minecraft.network.protocol.game.ClientboundSetSubtitleTextPacket;
import net.minecraft.network.protocol.game.ClientboundSetTitleTextPacket;
import net.minecraft.network.protocol.game.ClientboundSetTitlesAnimationPacket;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.util.profiling.metrics.MetricCategory;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.crafting.RecipeManager;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.event.level.ChunkEvent;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Supplier;

public class FakeRenderLevel extends ClientLevel {

    public static final FakeRenderLevel INSTANCE = new FakeRenderLevel();

    public final HashMap<BlockPos, BlockState> blocks = new HashMap<>();
    public final HashMap<BlockPos, BlockEntity> tiles = new HashMap<>();

    public FakeRenderLevel() {
        super(Minecraft.getInstance().getConnection(), //new EmptyConnection(),
                Minecraft.getInstance().level.getLevelData(),
                Minecraft.getInstance().level.dimension(),
                Minecraft.getInstance().level.dimensionTypeRegistration(),
                8, 0, () -> new ProfilerFiller(){

                    @Override
                    public void startTick() {

                    }

                    @Override
                    public void endTick() {

                    }

                    @Override
                    public void push(String s) {

                    }

                    @Override
                    public void push(Supplier<String> supplier) {

                    }

                    @Override
                    public void pop() {

                    }

                    @Override
                    public void popPush(String s) {

                    }

                    @Override
                    public void popPush(Supplier<String> supplier) {

                    }

                    @Override
                    public void markForCharting(MetricCategory metricCategory) {

                    }

                    @Override
                    public void incrementCounter(String s, int i) {

                    }

                    @Override
                    public void incrementCounter(Supplier<String> supplier, int i) {

                    }
                }, Minecraft.getInstance().levelRenderer, true, 0);
    }


    @Override
    public boolean setBlock(BlockPos pPos, BlockState pNewState, int pFlags) {
        this.blocks.put(pPos, pNewState);
        if(pNewState.getBlock() instanceof EntityBlock b){
            this.setBlockEntity(b.newBlockEntity(pPos, pNewState));
        }
        return true;
    }

    @Override
    public boolean removeBlock(BlockPos pPos, boolean pIsMoving) {
        this.blocks.remove(pPos);
        this.tiles.remove(pPos);
        return true;
    }

    @Override
    public boolean destroyBlock(BlockPos pPos, boolean pDropBlock, @Nullable Entity pEntity, int pRecursionLeft) {
        this.removeBlock(pPos, false);
        return true;
    }

    @Nullable
    @Override
    public BlockEntity getBlockEntity(BlockPos pPos) {
        return this.tiles.getOrDefault(pPos, null);
    }

    @Override
    public void setBlockEntity(BlockEntity pBlockEntity) {
        pBlockEntity.setLevel(this);
        this.tiles.put(pBlockEntity.getBlockPos(), pBlockEntity);
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        return this.blocks.containsKey(pos) ? this.blocks.get(pos) : Blocks.AIR.defaultBlockState();
    }

    @Override
    public <T extends BlockEntity> Optional<T> getBlockEntity(BlockPos pPos, BlockEntityType<T> pBlockEntityType) {
        return super.getBlockEntity(pPos, pBlockEntityType);
    }

    @Override
    public void sendBlockUpdated(BlockPos pPos, BlockState pOldState, BlockState pNewState, int pFlags) {

    }

    @Override
    public void syncBlockState(BlockPos pPos, BlockState pState, Vec3 p_233650_) {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void sendPacketToServer(Packet<?> pPacket) {
    }

    @Override
    public void setServerVerifiedBlockState(BlockPos pPos, BlockState pState, int p_233656_) {

    }

    @Override
    public boolean setBlock(BlockPos pPos, BlockState pState, int pFlags, int pRecursionLeft) {
        return this.setBlock(pPos, pState, pFlags);
    }

    @Override
    public RecipeManager getRecipeManager() {
        return Minecraft.getInstance().level.getRecipeManager();
    }

    @Override
    public FeatureFlagSet enabledFeatures() {
        return Minecraft.getInstance().level.enabledFeatures();
    }

    @Override
    public void onChunkLoaded(ChunkPos pChunkPos) {
        super.onChunkLoaded(pChunkPos);
    }

    @Override
    public Holder<Biome> getBiome(BlockPos pPos) {
        return Minecraft.getInstance().level.registryAccess().registryOrThrow(Registries.BIOME)
                .getHolder(Biomes.PLAINS).get();
    }

    public static class EmptyConnection extends ClientPacketListener {


        public EmptyConnection() {
            super(Minecraft.getInstance(), null, null, null, Minecraft.getInstance().getConnection().getLocalGameProfile(), null);
        }

        @Override
        public void onDisconnect(Component pReason) {

        }

        @Override
        public void send(Packet<?> pPacket) {

        }

        @Override
        public void setActionBarText(ClientboundSetActionBarTextPacket pPacket) {

        }

        @Override
        public void setTitleText(ClientboundSetTitleTextPacket pPacket) {

        }

        @Override
        public void setSubtitleText(ClientboundSetSubtitleTextPacket pPacket) {

        }

        @Override
        public void setTitlesAnimation(ClientboundSetTitlesAnimationPacket pPacket) {

        }



        @Override
        public Connection getConnection() {
            return null;
        }

        @Override
        public ClientLevel getLevel() {
            return null;
        }
    }
}
