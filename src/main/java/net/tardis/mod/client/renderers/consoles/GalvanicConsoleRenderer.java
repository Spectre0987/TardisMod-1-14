package net.tardis.mod.client.renderers.consoles;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.blockentities.consoles.GalvanicTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.consoles.GalvanicConsoleModel;
import net.tardis.mod.client.models.consoles.IAdditionalConsoleRenderData;
import net.tardis.mod.client.renderers.WorldText;
import net.tardis.mod.registry.ControlRegistry;

import java.util.Locale;

public class GalvanicConsoleRenderer extends ConsoleRenderer<GalvanicTile, GalvanicConsoleModel<GalvanicTile>>{

    final WorldText text = new WorldText(4.75F / 16.0F, 2.4F / 16.0F)
            .minXScale(0.1F)
            .withColor(0xFFFFFF);

    public GalvanicConsoleRenderer(BlockEntityRendererProvider.Context context, GalvanicConsoleModel<GalvanicTile> model, ResourceLocation tex) {
        super(context, model, tex);
    }

    @Override
    public void renderAdditionalData(ITardisLevel tardis, GalvanicTile console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay, IAdditionalConsoleRenderData data) {
        pose.pushPose();
        pose.translate(0.35, 0.3, -0.19);
        super.renderAdditionalData(tardis, console, partialTicks, pose, source, packedLight, packedOverlay, data);
        pose.popPose();
    }

    @Override
    public void renderExtra(ITardisLevel level, GalvanicTile console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay) {
        super.renderExtra(level, console, partialTicks, pose, source, packedLight, packedOverlay);

        this.model.root().translateAndRotate(pose);
        pose.pushPose();
        ModelPart screenPart = AnimationHelper.translateTo(pose, this.model.root(), "controls/controls_6/monitor/screen/text_screen");
        if(screenPart != null)
            AnimationHelper.translateToCenter(pose, screenPart);
        pose.mulPose(Axis.XN.rotationDegrees(90));
        pose.translate(-0.15, -0.1, -0.01);
        this.text.renderText(BaseMonitorTile.gatherWorldText(level), pose, source);
        pose.popPose();

        pose.pushPose();
        ModelPart part = AnimationHelper.translateTo(pose, model.root(), "controls/controls_5/direction/direction_screen/direction_text");
        AnimationHelper.translateToCenter(pose, part);
        pose.mulPose(Axis.XN.rotationDegrees(90));
        pose.translate(-0.03, -0.03, -0.03);
        this.text.renderText(
                Lists.newArrayList(
                        Component.literal(
                                (level.getControlDataOrCreate(ControlRegistry.FACING.get()).get().getName().charAt(0) + "")
                                        .toUpperCase(Locale.ROOT)
                                )
                        ),
                        pose, source
        );
        pose.popPose();

        pose.pushPose();
        ModelPart landingPart = AnimationHelper.translateTo(pose, model.root(), "controls/controls_5/landing_type/landing_screen/landing_text");
        AnimationHelper.translateToCenter(pose, landingPart);
        pose.mulPose(Axis.XN.rotationDegrees(90));
        pose.translate(-0.025, -0.025, -0.03);
        text.renderText(
                Lists.newArrayList(
                        Component.literal(
                                (level.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get()).get().name().toUpperCase(Locale.ROOT).charAt(0) + "")
                        )
                ), pose, source
        );
        pose.popPose();
    }
}
