package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.client.renderers.WorldText;

public class NeuveauConsoleRenderer<T extends ConsoleTile, M extends BasicTileHierarchicalModel<T>> extends ConsoleRenderer<T, M>{

    public final WorldText text = new WorldText(3.8F / 16.0F, 0.25F)
            .minXScale(0.1F)
            .withColor(0xFFFFFF);

    public NeuveauConsoleRenderer(BlockEntityRendererProvider.Context context, M model, ResourceLocation tex) {
        super(context, model, tex);
    }

    @Override
    public void renderExtra(ITardisLevel level, T console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay) {
        super.renderExtra(level, console, partialTicks, pose, source, packedLight, packedOverlay);

        pose.pushPose();
        model.root().translateAndRotate(pose);
        AnimationHelper.translateToCenter(pose, AnimationHelper.translateTo(pose, model.root(), "Controls/south/monitor/screen"));
        pose.mulPose(Axis.XN.rotationDegrees(6));
        pose.translate(-0.125, -0.21, -0.145);
        text.renderText(BaseMonitorTile.gatherWorldText(level), pose, source);
        pose.popPose();

    }
}
