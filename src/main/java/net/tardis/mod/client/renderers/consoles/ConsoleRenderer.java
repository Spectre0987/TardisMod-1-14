package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.IAnimatableTileModel;
import net.tardis.mod.client.models.consoles.IAdditionalConsoleRenderData;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.JsonRegistries;

import java.util.Optional;

public class ConsoleRenderer<C extends ConsoleTile, T extends HierarchicalModel<?> & IAnimatableTileModel<C>> implements BlockEntityRenderer<C> {

    public ResourceLocation tex;
    protected T model;

    public ConsoleRenderer(BlockEntityRendererProvider.Context context, T model, ResourceLocation tex){
        this.model = model;
        this.tex = tex;
    }

    public void renderAdditionalData(ITardisLevel tardis, C console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay, IAdditionalConsoleRenderData data){

        //Render Sonic Port
        data.getPartForControl(ControlRegistry.SONIC_PORT.get()).ifPresent(sonic -> {
            pose.pushPose();
            this.model.root().translateAndRotate(pose);
            final ModelPart part = AnimationHelper.translateTo(pose, this.model.root(), sonic);
            if(part != null){
                //pose.translate(part.x / 16.0, part.y / 16.0, part.z / 16.0);
                AnimationHelper.translateToCenter(pose, part);
            }

            //pose.mulPose(Axis.XN.rotationDegrees(180));
            pose.scale(0.4F, 0.4F, 0.4F);
            final ItemStack sonicPortItem = tardis.getControlDataOrCreate(ControlRegistry.SONIC_PORT.get()).get();
            Minecraft.getInstance().getItemRenderer().renderStatic(
                    sonicPortItem, ItemDisplayContext.FIXED, packedLight, packedOverlay,
                    pose, source, tardis.getLevel(), 0);
            pose.popPose();
        });
    }

    public void renderExtra(ITardisLevel level, C console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay){};

    @Override
    public void render(C consoleTile, float partialTicks, PoseStack poseStack, MultiBufferSource multiBufferSource, int packedLight, int packedOverlay) {

        long animTicks = Capabilities.getCap(Capabilities.TARDIS, consoleTile.getLevel()).map(tardis -> tardis.getAnimationTicks()).orElse(consoleTile.getLevel().getGameTime());

        poseStack.translate(0.5, 1.5, 0.5);
        poseStack.mulPose(Axis.ZP.rotationDegrees(180));
        model.setupAnimations(consoleTile, animTicks + partialTicks);
        this.model.renderToBuffer(poseStack, multiBufferSource.getBuffer(this.model.renderType(this.getTex(consoleTile))), packedLight, packedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
        Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level).ifPresent(tardis -> this.renderExtra(tardis, consoleTile, partialTicks, poseStack, multiBufferSource, packedLight, packedOverlay));

        Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level).ifPresent(cap -> {
            if(model instanceof IAdditionalConsoleRenderData data){
                poseStack.pushPose();
                this.renderAdditionalData(cap, consoleTile, partialTicks, poseStack, multiBufferSource, packedLight, packedOverlay, data);
                poseStack.popPose();
            }
        });
    }

    public ResourceLocation getTex(C console){
        return console.getTextureVariant().isPresent() ? ClientRegistry.getTextureVariant(console.getTextureVariant().get()).orElse(this.tex) : this.tex;
    }
}
