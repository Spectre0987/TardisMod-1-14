package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.client.renderers.WorldText;

import javax.swing.*;

public class G8ConsoleRenderer<T extends ConsoleTile, M extends BasicTileHierarchicalModel<T>> extends ConsoleRenderer<T, M>{

    final WorldText text = new WorldText(4F / 16.0F, 0.25F)
            .withColor(0xFFFFFF)
            .minXScale(0.15F);

    public G8ConsoleRenderer(BlockEntityRendererProvider.Context context, M model, ResourceLocation tex) {
        super(context, model, tex);
    }

    @Override
    public void renderExtra(ITardisLevel level, T console, float partialTicks, PoseStack pose, MultiBufferSource source, int packedLight, int packedOverlay) {
        super.renderExtra(level, console, partialTicks, pose, source, packedLight, packedOverlay);

        pose.pushPose();
        AnimationHelper.translateTo(pose, this.model.root(), "controls/side_n/monitor_box/glow_monitor_screen/monitor_text");
        pose.translate(-0.12, -0.1, 0);
        text.renderText(BaseMonitorTile.gatherWorldText(level), pose, source);
        pose.popPose();

    }
}
