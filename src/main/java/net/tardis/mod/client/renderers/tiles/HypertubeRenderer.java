package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.machines.TeleportTubeTile;
import net.tardis.mod.client.models.machines.HypertubeModel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;

public class HypertubeRenderer<T extends TeleportTubeTile> implements BlockEntityRenderer<T> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/machines/hypertube_texture.png");
    final HypertubeModel model;

    public HypertubeRenderer(BlockEntityRendererProvider.Context context){
        this.model = new HypertubeModel(context.bakeLayer(HypertubeModel.LAYER_LOCATION));
    }

    @Override
    public void render(T tile, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {
        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getFacingAngle(tile.getBlockState())));
        pose.mulPose(Axis.ZP.rotationDegrees(180));

        this.model.renderToBuffer(pose, pBufferSource.getBuffer(this.model.renderType(TEXTURE)), pPackedLight, pPackedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
        pose.popPose();
    }
}
