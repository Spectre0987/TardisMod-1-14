package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.ItemDisplayContext;
import net.tardis.mod.blockentities.machines.RiftCollectorTile;

public class RiftCollectorRenderer implements BlockEntityRenderer<RiftCollectorTile> {

    public RiftCollectorRenderer(BlockEntityRendererProvider.Context context){

    }

    @Override
    public void render(RiftCollectorTile entity, float pPartialTick, PoseStack pPoseStack, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {

        if(!entity.getItemStack().isEmpty()){
            pPoseStack.pushPose();
            pPoseStack.translate(0.5, 1.5, 0.5);
            pPoseStack.mulPose(Axis.YP.rotationDegrees(
                    Minecraft.getInstance().level.getGameTime() + pPartialTick % 360.0F
            ));
            pPoseStack.scale(0.3F, 0.3F, 0.3F);
            Minecraft.getInstance().getItemRenderer().renderStatic(entity.getItemStack(), ItemDisplayContext.FIXED, pPackedLight, pPackedOverlay, pPoseStack, pBufferSource, Minecraft.getInstance().level, 0);
            pPoseStack.popPose();
        }

    }
}
