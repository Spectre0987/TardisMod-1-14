package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.util.Mth;
import net.tardis.mod.blockentities.BrokenExteriorTile;
import net.tardis.mod.client.ModelHolder;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.WorldHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BrokenExteriorRenderer implements BlockEntityRenderer<BrokenExteriorTile> {

    private static final List<ModelHolder<ExteriorType, ?>> MODELS = new ArrayList<>();

    public static void register(ModelHolder<ExteriorType, ?> model){
        MODELS.add(model);
    }

    public BrokenExteriorRenderer(BlockEntityRendererProvider.Context context){
        for(ModelHolder<ExteriorType, ?> model : MODELS){
            model.bake(context.getModelSet());
        }
    }

    public static Optional<ModelHolder<ExteriorType, ?>> getRenderFor(ExteriorType type){
        for(ModelHolder<ExteriorType, ?> model : MODELS){
            if(model.shouldRenderFor(type)){
                return Optional.of(model);
            }
        }
        return Optional.empty();
    }

    @Override
    public void render(BrokenExteriorTile pBlockEntity, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {

        pose.pushPose();
        pose.translate(0.5F, 1.5F, 0.5F);
        pose.mulPose(Axis.XP.rotationDegrees(180));
        pose.mulPose(Axis.YP.rotationDegrees((float)Math.floor(WorldHelper.getHorizontalFacing(pBlockEntity.getBlockState()).toYRot())));

        for(ModelHolder<ExteriorType, ?> model : MODELS){
            if(model.shouldRenderFor(pBlockEntity.baseExterior.get()) && model.getModel().isPresent()){


                model.applyTranslations(pose);
                model.getModel().get().renderToBuffer(pose, source.getBuffer(model.getRenderType()), pPackedLight, pPackedOverlay, 1.0F, 1.0F,1.0F, 1.0F);
                break;
            }
        }

        pose.popPose();

    }

    @Override
    public int getViewDistance() {
        return 512;
    }
}
