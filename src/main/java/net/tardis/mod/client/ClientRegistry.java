package net.tardis.mod.client;

import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.math.Axis;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.*;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.consoles.NouveauConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.client.animations.demat.ClassicDematAnim;
import net.tardis.mod.client.animations.demat.NewWhoDematAnim;
import net.tardis.mod.client.animations.demat.ParticleAnimation;
import net.tardis.mod.client.gui.*;
import net.tardis.mod.client.gui.containers.*;
import net.tardis.mod.client.gui.containers.quantiscope.CraftingQuantiscopeScreen;
import net.tardis.mod.client.gui.containers.quantiscope.SonicQuantiscopeScreen;
import net.tardis.mod.client.gui.containers.quantiscope.SonicUpgradeQuantiscopeMenuScreen;
import net.tardis.mod.client.gui.datas.*;
import net.tardis.mod.client.gui.datas.tardis.MontiorChangeExteriorData;
import net.tardis.mod.client.gui.dev.PhasedOpticShellSaverGui;
import net.tardis.mod.client.gui.diagnostic_tool.CFLSubsytemInfoScreen;
import net.tardis.mod.client.gui.diagnostic_tool.DiagTardisInfoScreen;
import net.tardis.mod.client.gui.diagnostic_tool.DiagnosticMainScreen;
import net.tardis.mod.client.gui.diagnostic_tool.DiagnosticScreenBrokenTardisInfo;
import net.tardis.mod.client.gui.monitor.*;
import net.tardis.mod.client.gui.monitor.vortex_phenomena.DefaultVortexPhenomenaRenderer;
import net.tardis.mod.client.gui.sonic.SonicARSScreen;
import net.tardis.mod.client.gui.sonic.SonicModeScreen;
import net.tardis.mod.client.gui.sonic.SonicNamingGui;
import net.tardis.mod.client.gui.sonic.SonicTextureVariantScreen;
import net.tardis.mod.client.models.SlidingDoorModel;
import net.tardis.mod.client.models.consoles.*;
import net.tardis.mod.client.models.engines.RoofEngineModel;
import net.tardis.mod.client.models.engines.SteamEngineModel;
import net.tardis.mod.client.models.exteriors.*;
import net.tardis.mod.client.models.exteriors.entities.ImpalaExteriorModel;
import net.tardis.mod.client.models.exteriors.interior_door.*;
import net.tardis.mod.client.models.machines.FabricatorModel;
import net.tardis.mod.client.models.machines.HypertubeModel;
import net.tardis.mod.client.monitor_world_text.*;
import net.tardis.mod.client.particle.RiftParticle;
import net.tardis.mod.client.renderers.BlankEntityRenderer;
import net.tardis.mod.client.renderers.RendererControl;
import net.tardis.mod.client.renderers.SpecialItemRenderer;
import net.tardis.mod.client.renderers.consoles.ConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.G8ConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.GalvanicConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.NeuveauConsoleRenderer;
import net.tardis.mod.client.renderers.entities.ImpalaExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.*;
import net.tardis.mod.client.renderers.level.ILevelExtraRenderer;
import net.tardis.mod.client.renderers.level.RiftLevelRenderer;
import net.tardis.mod.client.renderers.level.TardisDimensionEffects;
import net.tardis.mod.client.renderers.tiles.*;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.ClientHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.*;
import net.tardis.mod.sides.ClientSideHelper;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientRegistry {

    public static Optional<SpecialItemRenderer> ITEM_RENDERER = Optional.empty();
    private static final HashMap<RenderLevelStageEvent.Stage, List<ILevelExtraRenderer>> WORLD_EXTRAS = new HashMap<>();

    public static String KEY_BINDS_CATEGORY = Tardis.MODID + ".keybinds.category.main";

    public static final KeyMapping MANUAL_KEY = new KeyMapping(Tardis.MODID + ".keybinds.manual", GLFW.GLFW_KEY_G, KEY_BINDS_CATEGORY);

    @SubscribeEvent
    public static void registerClient(FMLClientSetupEvent event){
        Tardis.SIDE = new ClientSideHelper();

        event.enqueueWork(() -> {
                MenuScreens.register(MenuRegistry.DRAFTING_TABLE.get(), DraftingTableScreen::new);
                MenuScreens.register(MenuRegistry.TARDIS_ENGINE.get(), TardisEngineScreen::new);
                MenuScreens.register(MenuRegistry.ALEMBIC.get(), AlembicMenuScreen::new);
                MenuScreens.register(MenuRegistry.SCOOP_VAULT.get(), ScoopVaultMenuScreen::new);
                MenuScreens.register(MenuRegistry.QUANTISCOPE_WELD.get(), CraftingQuantiscopeScreen::new);
                MenuScreens.register(MenuRegistry.QUANTISCOPE_SONIC.get(), SonicQuantiscopeScreen::new);
                MenuScreens.register(MenuRegistry.QUANTISCOPE_SONIC_UPGRADE.get(), SonicUpgradeQuantiscopeMenuScreen::new);
                MenuScreens.register(MenuRegistry.ARS.get(), ARSPanelScreen::new);
        });


        event.enqueueWork(() -> {
            registerEngineVariants();
            registerChameleonDoorRenderers();

            //Special Items
            registerSpecialItemModels();

            registerBrokenExteriors();
            registerWorldMonitorText();
            registerDematAnimations();


            ExteriorRenderer.registerSnowLayer(biome -> biome.coldEnoughToSnow(BlockPos.ZERO), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
            ExteriorRenderer.registerSnowLayer(biome -> {
                Registry<Biome> biomeReg = Minecraft.getInstance().level.registryAccess().registry(Registries.BIOME).get();
                return biomeReg.getHolderOrThrow(ResourceKey.create(Registries.BIOME, biomeReg.getKey(biome))).is(Tags.Biomes.IS_DESERT);
            }, new Vector4f(0.97F, 0.93F, 0.64F, 1.0F));

            addLevelExtraRenderer(new RiftLevelRenderer());

            DefaultVortexPhenomenaRenderer.registerDefault();
            final DefaultVortexPhenomenaRenderer vortexPRenderer = new DefaultVortexPhenomenaRenderer();
            MonitorFlightCourseScreen.registerVortexPhenomenaRenderer(vortexPRenderer);

        });


        MonitorEntry.ENTRIES.add(new LocationalMonitorEntry());
        MonitorEntry.ENTRIES.add(new FuelMonitorEntry());
        MonitorEntry.ENTRIES.add(new FlightEventMonitorEntry());
        MonitorEntry.ENTRIES.add(new FlightCourseProgressMonitorEntry());
        MonitorEntry.ENTRIES.add(new ExteriorExtraMonitorEntry());
        MonitorEntry.ENTRIES.add(new FirstEnterTARDISMonitorEntry());
        MonitorEntry.ENTRIES.add(new NotificationMonitorEntry());

        event.enqueueWork(() -> {
            ItemProperties.register(ItemRegistry.CHRONOFAULT_LOCATOR.get(), Helper.createRL("on"),
                    (stack, level, entity, seed) -> {
                        LazyOptional<ICFLTool> diagHolder = stack.getCapability(Capabilities.CFL);
                        if(diagHolder.isPresent()){
                            final ICFLTool tool = diagHolder.orElseThrow(NullPointerException::new);
                            return tool.isPointedAtTarget() ? 1.0F : 0.0F;
                        }
                        return 0;
                    }
            );
        });
    }

    public static void registerSpecialItemModels(){
        SpecialItemRenderer.register(new ModelHolder<>(
                stack -> stack.getItem() == ForgeRegistries.ITEMS.getValue(BlockRegistry.G8_CONSOLE.getId()),
                modelSet -> new G8ConsoleModel(modelSet.bakeLayer(G8ConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/g8.png")));

        SpecialItemRenderer.register(new ModelHolder<>(
                stack -> stack.getItem() == SpecialItemRenderer.getBlockItemOf(BlockRegistry.STEAM_CONSOLE),
                modelSet -> new SteamConsoleModel(modelSet.bakeLayer(SteamConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/steam.png")));

        SpecialItemRenderer.register(new ModelHolder<ItemStack, NouveauConsoleModel>(
                stack -> stack.getItem() == SpecialItemRenderer.getBlockItemOf(BlockRegistry.NEUVEAU_CONSOLE),
                modelSet -> new NouveauConsoleModel(modelSet.bakeLayer(NouveauConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/nouveau.png"))
                .setExtraTranslations(pose -> {
                    pose.scale(0.7F, 0.7F, 0.7F);
                    pose.translate(0, 1, 0);
                }));
        SpecialItemRenderer.register(new ModelHolder<ItemStack, SteamEngineModel>(
                stack -> stack.getItem() == SpecialItemRenderer.getBlockItemOf(BlockRegistry.ENGINE_STEAM),
                entityModelSet -> new SteamEngineModel(entityModelSet.bakeLayer(SteamEngineModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/engine/steam.png"))
                .setExtraTranslations(pose -> {
                    pose.translate(0, 1, 0);
                    pose.scale(0.5F, 0.5F, 0.5F);
                }));
        SpecialItemRenderer.register(new ModelHolder<ItemStack, GalvanicConsoleModel>(
                SpecialItemRenderer.isBlock(BlockRegistry.GALVANIC_CONSOLE),
                set -> new GalvanicConsoleModel(set.bakeLayer(GalvanicConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/galvanic.png"))
                .setExtraTranslations(pose -> {
                    pose.translate(0, 6 / 16.0F, 0);
                    pose.scale(0.8F, 0.8F, 0.8F);
                })
        );
        SpecialItemRenderer.register(new ModelHolder<>(
                SpecialItemRenderer.isBlock(BlockRegistry.ENGINE_ROOF),
                set -> new RoofEngineModel(set.bakeLayer(RoofEngineModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/engine/roof.png")
        ).setExtraTranslations(pose -> {
            pose.translate(0, -0.2, 0);
            pose.scale(0.8F, 0.8F, 0.8F);
        }));
        SpecialItemRenderer.register(new ModelHolder<>(
                SpecialItemRenderer.isBlock(BlockRegistry.NEMO_CONSOLE),
                set -> new NemoConsoleModel(set.bakeLayer(NemoConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/nemo_basic.png")
        ).setExtraTranslations(pose -> {
                pose.translate(0, 0.3, 0);
            })
        );
        SpecialItemRenderer.register(new ModelHolder<>(
                SpecialItemRenderer.isBlock(BlockRegistry.TELEPORT_TUBE),
                set -> new HypertubeModel(set.bakeLayer(HypertubeModel.LAYER_LOCATION)),
                HypertubeRenderer.TEXTURE
        ).setExtraTranslations(pose -> {
            pose.translate(0, 1 ,0);
            pose.mulPose(Axis.YP.rotationDegrees(180));
            pose.scale(0.6F, 0.6F, 0.6F);
        }));
        SpecialItemRenderer.register(new ModelHolder<>(
                SpecialItemRenderer.isBlock(BlockRegistry.FABRICATOR_MACHINE),
                set -> new FabricatorModel(set.bakeLayer(FabricatorModel.LAYER_LOCATION)),
                FabricatorRenderer.TEXTURE
        ).setExtraTranslations(pose -> {
            pose.translate(0, 1 ,0);
            pose.scale(0.6F, 0.6F, 0.6F);
        }));
    }

    public static void registerEngineVariants(){
        EngineRenderer.models.put(BlockRegistry.ENGINE_ROOF.get(), new ModelHolder<Block, Model>(
                b -> b == BlockRegistry.ENGINE_ROOF.get(),
                set -> new RoofEngineModel(set.bakeLayer(RoofEngineModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/engine/roof.png")
        ));

        EngineRenderer.models.put(BlockRegistry.ENGINE_STEAM.get(), new ModelHolder<>(
                b -> b == BlockRegistry.ENGINE_STEAM.get(),
                set -> new SteamEngineModel(set.bakeLayer(SteamEngineModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/engine/steam.png")
        ));
    }

    public static void registerChameleonDoorRenderers(){
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.STEAM.get(),
                new InteriorDoorRender<>(Helper.createRL("textures/tiles/interiordoor/steam.png"),
                        context -> new SteamInteriorDoorModel<>(context.bakeLayer(SteamInteriorDoorModel.LAYER_LOCATION))));

        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.TT_CAPSULE.get(),
                new InteriorDoorRender<>(TTCapsuleExteriorRenderer.DEFAULT_TEXTURE,
                        context -> new TTCapsuleInteriorDoorModel(context.bakeLayer(TTCapsuleInteriorDoorModel.LAYER_LOCATION))
                )
        );
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.POLICE_BOX.get(),
                new InteriorDoorRender<>(
                        Helper.createRL("textures/tiles/exteriors/policebox.png"),
                        context -> new PoliceBoxInteriorModel<>(context.bakeLayer(PoliceBoxInteriorModel.LAYER_LOCATION))
                )
        );
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.CHAMELEON.get(),
                    new InteriorDoorRender<>(
                            TTCapsuleExteriorRenderer.DEFAULT_TEXTURE,
                            context -> new TTCapsuleInteriorDoorModel(context.bakeLayer(TTCapsuleInteriorDoorModel.LAYER_LOCATION))
                    )
                );
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.IMPALA.get(), new InteriorDoorRender<>(
                TTCapsuleExteriorRenderer.DEFAULT_TEXTURE,
                context -> new TTCapsuleInteriorDoorModel(context.bakeLayer(TTCapsuleInteriorDoorModel.LAYER_LOCATION))
        ));
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.COFFIN.get(), new InteriorDoorRender<>(
                CoffinExteriorRenderer.TEXTURE,
                context -> new CoffinExteriorInteriorDoorModel(context.bakeLayer(CoffinExteriorInteriorDoorModel.LAYER_LOCATION))
        ));
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.OCTA.get(), new InteriorDoorRender<>(
                OctaExteriorRenderer.TEXTURES,
                context -> new OctaInteriorDoorModel(context.bakeLayer(OctaInteriorDoorModel.LAYER_LOCATION))
        ));
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.TRUNK.get(), new InteriorDoorRender<>(
                TrunkExteriorRenderer.TEXTURE,
                context -> new TrunkInteriorModel(context.bakeLayer(TrunkInteriorModel.LAYER_LOCATION))
        ));
        ChameleonInteriorDoorRenderer.registerChameleonDoor(ExteriorRegistry.SPRUCE.get(), new InteriorDoorRender<>(
                SpruceExteriorRenderer.TEXTURE,
                context -> new SpruceDoorInteriorModel(context.bakeLayer(SpruceDoorInteriorModel.LAYER_LOCATION))
        ));
    }

    public static void registerDematAnimations(){
        ExteriorRenderer.registerDematAnimation(DematAnimationRegistry.CLASSIC.get(), new ClassicDematAnim());
        ExteriorRenderer.registerDematAnimation(DematAnimationRegistry.PARTICLE_FALL.get(), new ParticleAnimation());
        ExteriorRenderer.registerDematAnimation(DematAnimationRegistry.NEW_WHO.get(), new NewWhoDematAnim());
    }

    public static void onGuiDataOpened(GuiData data){

        if(data.id == GuiDatas.DIAG_BROKEN_INFO.getId()){
            data.cast(BrokenInfoGuiData.class).ifPresent(info -> {
                Minecraft.getInstance().setScreen(new DiagnosticScreenBrokenTardisInfo(info.exteriorType, info.unlockables));
            });
        }
        if(data.id == GuiDatas.DIAGNOSTIC_MAIN.getId()){
            if(data instanceof DiagnosticMainScreenData d){
                final ItemStack stack = Minecraft.getInstance().player.getItemInHand(d.hand);
                stack.getCapability(Capabilities.CFL).ifPresent(tool -> {
                    tool.attuneTo(d.tardis.isPresent() ? d.tardis.get() : null);
                    Minecraft.getInstance().setScreen(new DiagnosticMainScreen(d.hand, tool));
                });
            }
        }
        if(data.id == GuiDatas.MON_CHANGE_EXT.getId()){
            if(Minecraft.getInstance().screen instanceof MonitorScreen monitor){
                data.cast(MontiorChangeExteriorData.class).ifPresent(d -> {
                    monitor.openMenu(MonitorSelectExteriorScreen::new).setData(d.currentType, d.unlockedTypes);
                });
            }
        }
        if(data.id == GuiDatas.MON_MAIN.getId()){
            data.cast(GuiDatas.MON_MAIN).ifPresent(d -> {
                Minecraft.getInstance().setScreen(new MonitorScreen(MonitorData.getData(d.loc)));
            });
        }
        if(data.id == GuiDatas.DIAG_TARDIS_INFO.getId()){
            if(data instanceof DiagTardisInfoData tData){
                Minecraft.getInstance().setScreen(new DiagTardisInfoScreen(tData));
            }
        }
        if(data.id == GuiDatas.MANUAL.getId()){
            data.cast(GuiDatas.MANUAL).ifPresent(man -> {
                Minecraft.getInstance().setScreen(new ManualScreen(man));
            });
        }
        if(data.id == GuiDatas.MON_CHANGE_DEMAT_ANIM.getId()){
            if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
                mon.openMenu(MonitorDematAnimScreen::new);
            }
        }
        if(data.id == GuiDatas.STRUCTURE.getId()){
            data.cast(StructureBlockData.class).ifPresent(s -> {
                    Minecraft.getInstance().setScreen(new StructureScreen(s.size));
            });
        }
        if(data.id == GuiDatas.TARDIS_MANUAL.getId()){
            if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
                mon.openMenu(MonitorManualScreen::new);
            }
        }
        if(data.id == GuiDatas.MON_FLIGHT_COURSE.getId()){
            data.cast(GuiDatas.MON_FLIGHT_COURSE).ifPresent(d -> {
                if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
                    mon.openMenu(monData -> new MonitorFlightCourseScreen(monData, d));
                }
            });
        }
        if(data.id == GuiDatas.TELEPATHIC.getId()){
            Minecraft.getInstance().setScreen(new TelepathicScreen());
        }
        if(data.id == GuiDatas.SONIC.getId()){
            data.cast(SonicGuiData.class).ifPresent(d -> {
                Minecraft.getInstance().player.getItemInHand(d.hand).getCapability(Capabilities.SONIC).ifPresent(sonic -> {
                    Minecraft.getInstance().setScreen(new SonicModeScreen(sonic, d.hand, d.types));
                });
            });
        }
        if(data.id == GuiDatas.ARS_EGG.getId()){
            data.cast(GuiDatas.ARS_EGG).ifPresent(ars -> {
                Minecraft.getInstance().setScreen(new ARSEggScreen(ars));
            });
        }
        if(data.id == GuiDatas.CFL_SUBSYSTEM_INFO.getId()){
            data.cast(GuiDatas.CFL_SUBSYSTEM_INFO).ifPresent(info -> {
                Minecraft.getInstance().setScreen(new CFLSubsytemInfoScreen(info));
            });
        }
        if(data.id == GuiDatas.MON_SOUND_SCHEME.getId()){
            if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
                data.cast(GuiDatas.MON_SOUND_SCHEME).ifPresent(info -> {
                    mon.openMenu(d -> new MonitorSoundSchemeScreen(d));
                });
            }
        }
        if(data.id == GuiDatas.MON_WAYPOINTS.getId()){
            if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
                data.cast(GuiDatas.MON_WAYPOINTS).ifPresent(d -> {
                    mon.openMenu(monData -> new MontiorWaypointScreen(monData, d));
                });
            }
        }
        if(data.id == GuiDatas.POS_ITEM.getId()){
            data.cast(GuiDatas.POS_ITEM).ifPresent(d -> Minecraft.getInstance().setScreen(new PhasedOpticShellSaverGui(d.hand, d.exterior, d.start, d.end)));
        }
        if(data.id == GuiDatas.SONIC_ARS.getId()) {
            data.cast(GuiDatas.SONIC_ARS).ifPresent(d -> {
                Minecraft.getInstance().setScreen(new SonicARSScreen(d));
            });
        }
        if(data.id == GuiDatas.MON_ATRIUM.getId()){
            data.cast(GuiDatas.MON_ATRIUM).ifPresent(d -> openMonitorSubmenu(at -> new MonitorAtriumScreen(at, d)));
        }
        GuiDatas.ifMatches(GuiDatas.SONIC_NAME, data, sonic -> Minecraft.getInstance().setScreen(new SonicNamingGui(sonic.pos, sonic.name.orElse(null))));
        GuiDatas.ifMatches(GuiDatas.SONIC_TEX_VAR, data, var -> Minecraft.getInstance().setScreen(new SonicTextureVariantScreen(var)));
        //GuiDatas.ifMatches(GuiDatas.MON_WAYPOINT_EDIT, data, var -> MonitorScreen.openSubScreen(d -> new MonitorWaypointEditScreen(d, var)));
    }

    public static void openMonitorSubmenu(Function<MonitorData, MonitorScreen> func){
        if(Minecraft.getInstance().screen instanceof MonitorScreen mon){
            mon.openMenu(func);
        }
    }

    public static void registerWorldMonitorText(){

        MonitorRenderer.register(BlockRegistry.STEAM_MONITOR.get(), (block, pose) -> {
            if(block.getValue(BlockStateProperties.HANGING)){
                pose.mulPose(Axis.XP.rotationDegrees(22.5F));
                ClientHelper.translatePixel(pose, 0, 3, -2.5);
            }
        });

        MonitorRenderer.register(BlockRegistry.EYE_MONITOR.get(), (block, pose) -> {
            pose.translate(0, 1 / 16.0, 12 / 16.0);
        });

        MonitorRenderer.register(BlockRegistry.RCA_MONITOR.get(), (block, pose) -> {
            pose.translate(0, -5 / 16.0, -0.5 / 16.0);
        });

        MonitorRenderer.register(BlockRegistry.VARIABLE_MONITOR.get(), (block, pos) -> {
            pos.translate(0, 0, 2 / 16.0 + 0.5);
        });


    }

    public static void registerBrokenExteriors(){
        BrokenExteriorRenderer.register(new ModelHolder<>(
                type -> type == ExteriorRegistry.STEAM.get(),
                set -> new SteamExteriorModel(set.bakeLayer(SteamExteriorModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/exteriors/steam.png")
        ));

        BrokenExteriorRenderer.register(new ModelHolder<>(
                type -> type == ExteriorRegistry.TT_CAPSULE.get(),
                set -> new TTCapsuleExteriorModel<>(set.bakeLayer(TTCapsuleExteriorModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/exteriors/tt_capsule/rusty_shell.png")
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.POLICE_BOX.get(),
                set -> new PoliceBoxExteriorModel<>(set.bakeLayer(PoliceBoxExteriorModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/exteriors/policebox.png")
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.IMPALA.get(),
                set -> new ImpalaExteriorModel<>(set.bakeLayer(ImpalaExteriorModel.LAYER_LOCATION)),
                ImpalaExteriorRenderer.TEXTURE
        ).setExtraTranslations(pose -> {

        }));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.CHAMELEON.get(),
                set -> new TTCapsuleExteriorModel<>(set.bakeLayer(TTCapsuleExteriorModel.LAYER_LOCATION)),
                TTCapsuleExteriorRenderer.DEFAULT_TEXTURE
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.COFFIN.get(),
                set -> new CoffinExteriorModel<>(set.bakeLayer(CoffinExteriorModel.LAYER_LOCATION)),
                CoffinExteriorRenderer.TEXTURE
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.OCTA.get(),
                set -> new OctaExteriorModel<>(set.bakeLayer(OctaExteriorModel.LAYER_LOCATION)),
                OctaExteriorRenderer.TEXTURES
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.TRUNK.get(),
                set -> new TrunkExteriorModel<>(set.bakeLayer(TrunkExteriorModel.LAYER_LOCATION)),
                TrunkExteriorRenderer.TEXTURE
        ));
        BrokenExteriorRenderer.register(new ModelHolder<ExteriorType, Model>(
                type -> type == ExteriorRegistry.SPRUCE.get(),
                set -> new SpruceDoorExteriorModel<>(set.bakeLayer(SpruceDoorExteriorModel.LAYER_LOCATION)),
                SpruceExteriorRenderer.TEXTURE
        ));
    }

    public static <T extends ILevelExtraRenderer> void addLevelExtraRenderer(T renderer){
        final RenderLevelStageEvent.Stage stage = renderer.getStage();
        if(WORLD_EXTRAS.containsKey(stage)){
            WORLD_EXTRAS.get(stage).add(renderer);
        }
        else {
            List<ILevelExtraRenderer> renderList = new ArrayList<>();
            renderList.add(renderer);
            WORLD_EXTRAS.put(stage, renderList);
        }
    }

    public static List<ILevelExtraRenderer> getLevelExtraRenderers(RenderLevelStageEvent.Stage stage){
        return WORLD_EXTRAS.getOrDefault(stage, new ArrayList<>());
    }

    public static void tickLevelRenderers(){
        for(List<ILevelExtraRenderer> rendererList : WORLD_EXTRAS.values()){
            for(ILevelExtraRenderer r : rendererList){
                if(Minecraft.getInstance().level != null)
                    r.clientTick();
            }
        }
    }

    @SubscribeEvent
    public static void registerModel(EntityRenderersEvent.RegisterLayerDefinitions event){
        event.registerLayerDefinition(G8ConsoleModel.LAYER_LOCATION, G8ConsoleModel::createBodyLayer);
        event.registerLayerDefinition(SteamConsoleModel.LAYER_LOCATION, SteamConsoleModel::createBodyLayer);
        event.registerLayerDefinition(SteamExteriorModel.LAYER_LOCATION, SteamExteriorModel::createBodyLayer);
        event.registerLayerDefinition(SteamInteriorDoorModel.LAYER_LOCATION, SteamInteriorDoorModel::createBodyLayer);
        event.registerLayerDefinition(SteamEngineModel.LAYER_LOCATION, SteamEngineModel::createBodyLayer);
        event.registerLayerDefinition(NouveauConsoleModel.LAYER_LOCATION, NouveauConsoleModel::createBodyLayer);
        event.registerLayerDefinition(TTCapsuleExteriorModel.LAYER_LOCATION, TTCapsuleExteriorModel::createBodyLayer);
        event.registerLayerDefinition(TTCapsuleInteriorDoorModel.LAYER_LOCATION, TTCapsuleInteriorDoorModel::createBodyLayer);
        event.registerLayerDefinition(GalvanicConsoleModel.LAYER_LOCATION, GalvanicConsoleModel::createBodyLayer);
        event.registerLayerDefinition(PoliceBoxExteriorModel.LAYER_LOCATION, PoliceBoxExteriorModel::createBodyLayer);
        event.registerLayerDefinition(PoliceBoxInteriorModel.LAYER_LOCATION, PoliceBoxInteriorModel::createBodyLayer);
        event.registerLayerDefinition(RoofEngineModel.LAYER_LOCATION, RoofEngineModel::createBodyLayer);
        event.registerLayerDefinition(NemoConsoleModel.LAYER_LOCATION, NemoConsoleModel::createBodyLayer);
        event.registerLayerDefinition(SlidingDoorModel.LAYER_LOCATION, SlidingDoorModel::createBodyLayer);
        event.registerLayerDefinition(ImpalaExteriorModel.LAYER_LOCATION, ImpalaExteriorModel::createBodyLayer);
        event.registerLayerDefinition(FabricatorModel.LAYER_LOCATION, FabricatorModel::createBodyLayer);
        event.registerLayerDefinition(HypertubeModel.LAYER_LOCATION, HypertubeModel::createBodyLayer);
        event.registerLayerDefinition(CoffinExteriorModel.LAYER_LOCATION, CoffinExteriorModel::createBodyLayer);
        event.registerLayerDefinition(CoffinExteriorInteriorDoorModel.LAYER_LOCATION, CoffinExteriorInteriorDoorModel::createBodyLayer);
        event.registerLayerDefinition(OctaExteriorModel.LAYER_LOCATION, OctaExteriorModel::createBodyLayer);
        event.registerLayerDefinition(OctaInteriorDoorModel.LAYER_LOCATION, OctaInteriorDoorModel::createBodyLayer);
        event.registerLayerDefinition(TrunkExteriorModel.LAYER_LOCATION, TrunkExteriorModel::createBodyLayer);
        event.registerLayerDefinition(TrunkInteriorModel.LAYER_LOCATION, TrunkInteriorModel::createBodyLayer);
        event.registerLayerDefinition(SpruceDoorExteriorModel.LAYER_LOCATION, SpruceDoorExteriorModel::createBodyLayer);
        event.registerLayerDefinition(SpruceDoorInteriorModel.LAYER_LOCATION, SpruceDoorInteriorModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void registerSkyRenderers(RegisterDimensionSpecialEffectsEvent event){
        event.register(DimensionTypes.TARDIS_TYPE.location(), new TardisDimensionEffects());
    }

    @SubscribeEvent
    public static void registerRendereres(EntityRenderersEvent.RegisterRenderers event){
        event.registerEntityRenderer(EntityRegistry.CONTROL.get(), RendererControl::new);
        event.registerEntityRenderer(EntityRegistry.CHAIR.get(), BlankEntityRenderer::new);

        //Consoles
        event.registerBlockEntityRenderer(TileRegistry.G8_CONSOLE.get(), context -> new G8ConsoleRenderer<>(context, new G8ConsoleModel(context.bakeLayer(G8ConsoleModel.LAYER_LOCATION)), Helper.createRL("textures/tiles/consoles/g8.png")));
        event.registerBlockEntityRenderer(TileRegistry.STEAM_CONSOLE.get(), context -> new ConsoleRenderer<>(context, new SteamConsoleModel(context.bakeLayer(SteamConsoleModel.LAYER_LOCATION)), Helper.createRL("textures/tiles/consoles/steam.png")));
        event.registerBlockEntityRenderer(TileRegistry.NOUVEAU_CONSOLE.get(), context -> new NeuveauConsoleRenderer<>(context, new NouveauConsoleModel<NouveauConsoleTile>(context.bakeLayer(NouveauConsoleModel.LAYER_LOCATION)), Helper.createRL("textures/tiles/consoles/nouveau.png")));
        event.registerBlockEntityRenderer(TileRegistry.GALVANIC_CONSOLE.get(), context -> new GalvanicConsoleRenderer(context, new GalvanicConsoleModel(context.bakeLayer(
                GalvanicConsoleModel.LAYER_LOCATION)),
                Helper.createRL("textures/tiles/consoles/galvanic.png")));
        event.registerBlockEntityRenderer(TileRegistry.NEMO_CONSOLE.get(), context -> new ConsoleRenderer<>(context,
                new NemoConsoleModel<>(context.bakeLayer(NemoConsoleModel.LAYER_LOCATION)),
                        Helper.createRL("textures/tiles/consoles/nemo_basic.png")
        ));

        //Exteriors
        event.registerBlockEntityRenderer(TileRegistry.STEAM_EXTERIOR.get(), SteamExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.TT_CAPSULE.get(), TTCapsuleExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.POLICE_BOX_EXTERIOR.get(), PoliceBoxExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.COFFIN.get(), CoffinExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.OCTA_EXTERIOR.get(), OctaExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.TRUNK_EXTERIOR.get(), TrunkExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.SPRUCE_EXTERIOR.get(), SpruceExteriorRenderer::new);

        //Tiles
        event.registerBlockEntityRenderer(TileRegistry.DRAFTING_TABLE.get(), DraftingTileRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.INTERIOR_DOOR.get(), ChameleonInteriorDoorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.BASIC_MONITOR.get(), MonitorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.BROKEN_EXTERIOR.get(), BrokenExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.ENGINE.get(), EngineRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.ROOF_ENGINE.get(), EngineRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.DEDICATION_PLAQUE.get(), DedicationPlaqueRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.RIFT_COLLECTOR.get(), RiftCollectorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.MATTER_BUFFER.get(), MatterBufferRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.DISGUISED_BLOCK.get(), DisguisedTileRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.CHAMELEON_EXTERIOR.get(), ChameleonExteriorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.ITEM_PLAQUE.get(), ItemPlaqueRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.BIG_DOOR.get(), set -> new BigDoorRenderer(set, Helper.createRL("textures/tiles/slidingdoor_alabaster.png")));
        event.registerBlockEntityRenderer(TileRegistry.MONITOR_VARIABLE.get(), MonitorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.FABRICATOR_MACHINE.get(), FabricatorRenderer::new);
        event.registerBlockEntityRenderer(TileRegistry.TELEPORT_TUBE.get(), HypertubeRenderer::new);

        //Entities
        event.registerEntityRenderer(EntityRegistry.IMPALA.get(), context -> new ImpalaExteriorRenderer<>(context, new ImpalaExteriorModel<>(context.bakeLayer(ImpalaExteriorModel.LAYER_LOCATION))));


        event.registerBlockEntityRenderer(TileRegistry.RENDERER_TEST.get(), TestRenderer::new);

    }

    @SubscribeEvent
    public static void registerReloadListener(RegisterClientReloadListenersEvent event){

        final SpecialItemRenderer item = new SpecialItemRenderer(Minecraft.getInstance().getBlockEntityRenderDispatcher(), Minecraft.getInstance().getEntityModels());
        event.registerReloadListener(item);
        ITEM_RENDERER = Optional.of(item);
    }

    @SubscribeEvent
    public static void registerShaders(RegisterShadersEvent event) throws IOException {
        event.registerShader(new ShaderInstance(event.getResourceProvider(), Helper.createRL("snow"), DefaultVertexFormat.POSITION), instance -> TardisRenderTypes.SNOW_SHADER = instance);
        event.registerShader(new ShaderInstance(event.getResourceProvider(), Helper.createRL("vortex"), TardisRenderTypes.VORTEX_FORMAT), instance -> TardisRenderTypes.VORTEX_SHADER = instance);
        event.registerShader(new ShaderInstance(event.getResourceProvider(),
                    Helper.createRL("rift"),
                    DefaultVertexFormat.POSITION_TEX
                ), instance -> TardisRenderTypes.RIFT_SHADER = instance);
        event.registerShader(new ShaderInstance(
                event.getResourceProvider(),
                Helper.createRL("shield"),
                DefaultVertexFormat.POSITION
        ), shaderInstance -> TardisRenderTypes.FORCEFIELD_SHADER = shaderInstance);
        event.registerShader(new ShaderInstance(
                event.getResourceProvider(), Helper.createRL("demat_block"), DefaultVertexFormat.BLOCK
        ), i -> TardisRenderTypes.DEMAT_BLOCK_SHADER = i);
    }

    @SubscribeEvent
    public static void registerParticles(RegisterParticleProvidersEvent event){
        event.registerSpecial(ParticleRegistry.RIFT.get(), RiftParticle::new);
    }

    @SubscribeEvent
    public static void registerKey(RegisterKeyMappingsEvent event){
        event.register(MANUAL_KEY);
    }

    public static Optional<ResourceLocation> getTextureVariant(ResourceLocation variantId){
        if(Minecraft.getInstance().level != null){
            Registry<TextureVariant> reg = Minecraft.getInstance().level.registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS);
            if(reg.containsKey(variantId)){
                return getTextureVariant(reg.get(variantId));
            }
        }
        return Optional.empty();
    }

    public static Optional<ResourceLocation> getTextureVariant(TextureVariant var){
        ResourceLocation requestedTexture = var.texture();
        if(Minecraft.getInstance().getTextureManager().getTexture(requestedTexture) != null)
            return Optional.of(requestedTexture);
        return Optional.empty();
    }

}
