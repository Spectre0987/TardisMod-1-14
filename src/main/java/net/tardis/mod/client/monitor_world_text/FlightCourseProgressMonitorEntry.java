package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.ArrayList;
import java.util.List;

public class FlightCourseProgressMonitorEntry extends MonitorEntry{

    public static final String TRANS = BASE_KEY + "flight_course";

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        List<Component> trans = new ArrayList<>();
        tardis.getCurrentCourse().ifPresent(course -> {
            int progress = Mth.floor(tardis.getDistanceTraveled() / course.getTotalFlightDistance() * 100);
            trans.add(Component.translatable(TRANS, progress));
        });

        return trans;
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return tardis.isInVortex() && !tardis.isTakingOffOrLanding() && tardis.getCurrentCourse().isPresent() && !tardis.getCurrentCourse().get().isComplete(tardis.getDistanceTraveled());
    }
}
