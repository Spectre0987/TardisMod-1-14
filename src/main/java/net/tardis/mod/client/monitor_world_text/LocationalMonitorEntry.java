package net.tardis.mod.client.monitor_world_text;

import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.DimensionNameHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.subsystem.Subsystem;

import java.util.List;

public class LocationalMonitorEntry extends MonitorEntry {

    public static final String LOCATION_KEY = BASE_KEY + "location";
    public static final String LOCATION_DIM_KEY = BASE_KEY + "location_dim";
    public static final String DEST_KEY = BASE_KEY + "destination";
    public static final String DEST_DIM_KEY = BASE_KEY + "dest_dim";

    public static final Component NAV_COM_MISSING = Component.translatable(BASE_KEY + "missing_nav_com");

    @Override
    public List<Component> getText(ITardisLevel tardis) {

        if(!SubsystemRegistry.NAV_COM.get().canBeUsed(tardis)){
            return Lists.newArrayList(NAV_COM_MISSING);
        }

        SpaceTimeCoord coord = tardis.getLocation();
        SpaceTimeCoord dest = tardis.getDestination();
        return List.of(
                Component.translatable(LOCATION_KEY, coord.getPos().getX(), coord.getPos().getY(), coord.getPos().getZ()),
                Component.translatable(LOCATION_DIM_KEY, DimensionNameHelper.getFriendlyName(Minecraft.getInstance().level, coord.getLevel())),
                Component.translatable(DEST_KEY, dest.getPos().getX(), dest.getPos().getY(), dest.getPos().getZ()),
                Component.translatable(DEST_DIM_KEY, DimensionNameHelper.getFriendlyName(Minecraft.getInstance().level, dest.getLevel()))
        );
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return true;
    }
}
