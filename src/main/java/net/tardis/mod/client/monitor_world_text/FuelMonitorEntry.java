package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.List;

public class FuelMonitorEntry extends MonitorEntry{

    public static final String TRANS_KEY = BASE_KEY + "refuel";

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        return List.of(
                Component.translatable(TRANS_KEY, Mth.floor(tardis.getFuelHandler().getStoredArtron()))
        );
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return true;
    }
}
