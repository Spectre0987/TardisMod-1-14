package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.TardisNotifications;

import java.util.ArrayList;
import java.util.List;

public class NotificationMonitorEntry extends MonitorEntry {
    @Override
    public List<Component> getText(ITardisLevel tardis) {
        final List<Component> com = new ArrayList<>();
        for(TardisNotifications.TardisNotif notif : tardis.getActiveNotifications()){
            com.add(notif.notif().apply(tardis));
        }
        return com;
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return tardis.getActiveNotifications().size() > 0;
    }

    @Override
    public OverrideType getOverrideType() {
        return OverrideType.ALL;
    }

    @Override
    public int overridePriority() {
        return 100;
    }
}
