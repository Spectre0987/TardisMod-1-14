package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.level.TardisCap;

import java.util.ArrayList;
import java.util.List;

public class FlightEventMonitorEntry extends MonitorEntry{

    public static final String TRANSLATION = BASE_KEY + "flight_event";

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        List<Component> text = new ArrayList<>();
        tardis.getCurrentFlightEvent().ifPresent(event -> {
            text.add(Component.translatable(TRANSLATION, event.getType().makeTrans()));
        });
        return text;
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return TardisCap.IsFlightEventActive(tardis.getCurrentFlightEvent());
    }

    @Override
    public OverrideType getOverrideType() {
        return OverrideType.SHARED;
    }
}
