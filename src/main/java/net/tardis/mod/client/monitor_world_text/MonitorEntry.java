package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.ArrayList;
import java.util.List;

public abstract class MonitorEntry {

    public static final String BASE_KEY = "monitor." + Tardis.MODID + ".";

    public static final List<MonitorEntry> ENTRIES = new ArrayList<>();

    public abstract List<Component> getText(ITardisLevel tardis);
    public abstract boolean shouldDisplay(ITardisLevel tardis);

    public int overridePriority(){
        return 0;
    }

    /**
     * Override Type:
     *   NONE - is a normal monitor entry
     *   ALL - There can only be one! (Higher {@link MonitorEntry#overridePriority()} gets to be the only thing on the screen)
     *   SHARED - Can be on the screen with other shared overrides
     * @return
     */
    public OverrideType getOverrideType(){
        return OverrideType.NONE;
    }

    public static enum OverrideType{
        NONE,
        ALL,
        SHARED
    }

}
