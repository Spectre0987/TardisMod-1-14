package net.tardis.mod.client;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class ModelHolder<A, T extends Model> {

    final Predicate<A> renderFor;
    final Function<EntityModelSet, T> modelSupplier;
    final ResourceLocation texture;
    Consumer<PoseStack> extraTranslations = stack -> {};
    T model;

    public ModelHolder(Predicate<A> renderFor, Function<EntityModelSet, T> modelSupplier, ResourceLocation texture) {
        this.renderFor = renderFor;
        this.modelSupplier = modelSupplier;
        this.texture = texture;
    }

    public boolean shouldRenderFor(A test){
        return this.renderFor.test(test);
    }

    public void bake(EntityModelSet set) {
        this.model = this.modelSupplier.apply(set);
    }

    public Optional<T> getModel() {
        return this.model == null ? Optional.empty() : Optional.of(this.model);
    }

    public RenderType getRenderType() {
        return this.model.renderType(this.texture);
    }

    public ModelHolder<A, T> setExtraTranslations(Consumer<PoseStack> stack){
        this.extraTranslations = stack;
        return this;
    }

    public void applyTranslations(PoseStack stack){
        this.extraTranslations.accept(stack);
    }

}
