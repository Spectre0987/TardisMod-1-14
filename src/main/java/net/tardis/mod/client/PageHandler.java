package net.tardis.mod.client;

import net.minecraft.util.Mth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PageHandler<T> {

    public final List<T> masterList = new ArrayList<>();
    public final int itemsPerPage;


    public PageHandler(Collection<T> masterList, int itemsPerPage){
        this.masterList.addAll(masterList);
        this.itemsPerPage = itemsPerPage;
    }

    public int getPages(){
        return Mth.ceil(this.masterList.size() / (float)this.itemsPerPage);
    }

    public List<T> getItemsOnPage(int index){
        return this.masterList.subList(this.itemsPerPage * index, Math.min((index * itemsPerPage) + this.itemsPerPage, masterList.size()));
    }

    public boolean hasNextPage(int index){
        return this.getPages() - 1 < index;
    }

}
