package net.tardis.mod.client.animations.consoles;

import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.client.animation.Keyframe;
import net.minecraft.client.animation.KeyframeAnimations;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.consoles.GalvanicConsoleModel;
import net.tardis.mod.control.IncrementControl;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.control.datas.ControlDataFloat;
import net.tardis.mod.control.datas.ControlDataInt;
import net.tardis.mod.registry.ControlRegistry;
import org.joml.Vector3f;

import java.util.function.Function;

public class GalvanicConsoleAnimations {

    public static final AnimationDefinition ROTOR = AnimationDefinition.Builder.withLength(2.0F).looping()
            .addAnimation("rotor_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(2.0F, KeyframeAnimations.degreeVec(0.0F, 360.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition ARTRON_USE = AnimationDefinition.Builder.withLength(1.0F)
            .addAnimation("pointer_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.5F, KeyframeAnimations.degreeVec(0.0F, 65.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.6667F, KeyframeAnimations.degreeVec(0.0F, 37.5F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.7917F, KeyframeAnimations.degreeVec(0.0F, 55.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.0F, KeyframeAnimations.degreeVec(0.0F, 15.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition RANDOMIZER = AnimationDefinition.Builder.withLength(1.75F)
            .addAnimation("randomizer_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.1667F, KeyframeAnimations.degreeVec(0.0F, -22.5F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.3333F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.75F, KeyframeAnimations.degreeVec(0.0F, 720.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition DOOR = AnimationDefinition.Builder.withLength(1.0F)
            .addAnimation("key_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.5F, KeyframeAnimations.degreeVec(0.0F, 112.5F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition FAST_RETURN = AnimationDefinition.Builder.withLength(1.1667F)
            .addAnimation("button", new AnimationChannel(AnimationChannel.Targets.POSITION,
                    new Keyframe(0.0F, KeyframeAnimations.posVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.75F, KeyframeAnimations.posVec(0.0F, -0.25F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.1667F, KeyframeAnimations.posVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static void animateConditional(ITardisLevel tardis, GalvanicConsoleModel<?> model, float ageInTicks){
        final ControlDataFloat throttle = tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get());
        final ControlDataBool handbrake = tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        final ControlDataBool refuel = tardis.getControlDataOrCreate(ControlRegistry.REFUELER.get());
        final ControlDataInt increment = tardis.getControlDataOrCreate(ControlRegistry.INCREMENT.get());
        final ControlDataBool stabilizers = tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get());


        model.getAnyDescendantWithName("throttle_rotate_x").ifPresent(control -> {
            control.offsetRotation(AnimationHelper.getSteppedRotation(throttle, -142.5F, throttle.getPrevious(), throttle.get(), ageInTicks, 5, new Vector3f(1, 0, 0)));
        });

        model.getAnyDescendantWithName("handbreak_rotate_y").ifPresent(control -> {
            control.offsetRotation(new Vector3f(0, AnimationHelper.getRotationBaseOnState(handbrake, -42.5F, ageInTicks, 20, handbrake.get()), 0));
        });

        model.getAnyDescendantWithName("pointer_rotate_y").ifPresent(control -> {
            float percent = tardis.getFuelHandler().getStoredArtron() / (float)tardis.getFuelHandler().getMaxArtron();
            control.offsetRotation(
                    AnimationHelper.getSteppedRotation(refuel, 42.5F, percent, percent, ageInTicks, 5, new Vector3f(0, 1, 0))
            );
        });

        model.getAnyDescendantWithName("stabilizer_offset_y").ifPresent(control -> {
            Function<Boolean, Float> stateMapper = b -> b ? 1.0F : 0.0F;
            control.offsetPos(AnimationHelper.slideBasedOnState(stabilizers, stateMapper.apply(stabilizers.getPrevious()), stateMapper.apply(stabilizers.get()),
                        ageInTicks, 0.25F, 10, new Vector3f(0, 1, 0)
                    ));
        });

        model.getAnyDescendantWithName("position_rotate_x").ifPresent(control -> {
            float percent = increment.get() / (float) IncrementControl.VALUES.length;
            float prevPer = increment.getPrevious() / (float) IncrementControl.VALUES.length;
            control.offsetPos(AnimationHelper.slideBasedOnState(increment, prevPer, percent, ageInTicks, 2, 20, new Vector3f(0, 0, 1)));
        });
    }

}
