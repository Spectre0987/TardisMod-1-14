package net.tardis.mod.client.animations.exterior;

import net.minecraft.util.Mth;
import net.tardis.mod.client.models.exteriors.entities.ImpalaExteriorModel;
import net.tardis.mod.entity.CarExteriorEntity;
import net.tardis.mod.helpers.WorldHelper;
import org.joml.Vector3f;

public class ImpalaExteriorAnimation {

    public static String[] wheels = {
            "rightWheel",
            "leftWheel",
            "rightBackWheel",
            "leftBackWheel"
    };

    public static String[] frontWheels = {
            "rightWheel",
            "leftWheel"
    };

    public static <T extends CarExteriorEntity> void animateConditional(T car, ImpalaExteriorModel<?> model){

        //Animate Turning
        float wheelTurn = (float)Math.toRadians(car.getTurnAmount() * (30 / 5.0));
        for(String name : frontWheels){
            model.getAnyDescendantWithName(name).ifPresent(part -> {
                part.offsetRotation(new Vector3f(0, -wheelTurn, 0));
            });
        }

        //Turn wheels while Driving
        float wheelSpin = (float)Math.toRadians(-WorldHelper.getHorizonalSpeed(car) * (car.tickCount * 10.0) % 360.0);

        for(String name : wheels){
            model.getAnyDescendantWithName(name).ifPresent(part -> {
                part.offsetRotation(new Vector3f(wheelSpin, 0, 0));
            });
        }

        //Trunk
        model.getAnyDescendantWithName("trunk").ifPresent(part -> {
            part.offsetRotation(new Vector3f((float)Math.toRadians(car.getDoorHandler().getDoorState().isOpen() ? 60 : 0), 0, 0));
        });


    }

}
