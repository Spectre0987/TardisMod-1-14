package net.tardis.mod.client.animations.demat;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Mth;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;

public class ClassicDematAnim extends DematAnimation{

    @Override
    public float[] getColors(MatterStateHandler tile, float partialTicks) {

        if(tile.getEndTick() == 0){
            return super.getColors(tile, partialTicks);
        }
        float percentComplete = Mth.clamp((tile.getStartTick() - (Minecraft.getInstance().level.getGameTime() + partialTicks)) /
                (float) (tile.getStartTick() - tile.getEndTick()), 0, 1);

        if(tile.getMatterState() == MatterState.DEMAT){
            percentComplete = 1.0F - percentComplete;
        }


        return new float[]{1.0F, 1.0F, 1.0F, percentComplete};
    }
}
