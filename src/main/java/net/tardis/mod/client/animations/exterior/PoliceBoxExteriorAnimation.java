package net.tardis.mod.client.animations.exterior;

import net.minecraft.client.model.geom.ModelPart;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.misc.enums.DoorState;
import org.joml.Vector3f;

public class PoliceBoxExteriorAnimation {


    public static void animateDoors(DoorState state, ModelPart door1, ModelPart door2){
        final Vector3f rad = AnimationHelper.degrees(-70, AnimationHelper.Axis.Y);
        if(state.isOpen()){
            door1.offsetRotation(rad);
        }
        if(state == DoorState.BOTH){
            door2.offsetRotation(rad.mul(-1));
        }
    }

}
