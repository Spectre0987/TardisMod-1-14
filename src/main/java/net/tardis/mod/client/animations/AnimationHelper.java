package net.tardis.mod.client.animations;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.util.Mth;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.registry.ControlRegistry;
import org.jetbrains.annotations.Nullable;
import org.joml.Vector3f;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class AnimationHelper {


    public static Vector3f createRadianFromDegrees(double x, double y, double z){
        return new Vector3f((float)Math.toRadians(x), (float)Math.toRadians(y), (float)Math.toRadians(z));
    }

    public static float getRotationBaseOnState(ControlData<?> data, float startRot, float amountToTravel, float ageInTicks, int animLength, boolean forward){
        data.usedState.updateTime(ageInTicks, 1.0F);
        long time = data.usedState.getAccumulatedTime();
        float secondsSinceStarted = time / 1000.0F;
        float percent = Mth.clamp(secondsSinceStarted / (animLength / 20.0F), 0, 1);
        if(!forward){
            percent = 1.0F - percent;
        }
        return (float)Math.toRadians(startRot + (percent * amountToTravel));
    }

    public static float getRotationBaseOnState(ControlData<?> data, float amountToTravel, float ageInTicks, int animLength, boolean forward){
        return getRotationBaseOnState(data, 0, amountToTravel, ageInTicks, animLength, forward);
    }

    public static Vector3f getSteppedRotation(ControlData<?> data, float totalAmountToRotate, float prevPercent, float percent, float ageInTicks, int stepTime, Vector3f rotationDirection){

        percent = Mth.clamp(percent, 0.0F, 1.0F);
        prevPercent = Mth.clamp(prevPercent, 0.0F, 1.0F);

        float deltaAmt = (percent - prevPercent) * data.getAnimationPercent(ageInTicks, Mth.ceil(stepTime * Mth.abs(prevPercent - percent)));


        float rotation = (totalAmountToRotate * prevPercent) + (totalAmountToRotate * deltaAmt);
        Vector3f degreeVec = rotationDirection.mul(rotation, rotation, rotation);
        return new Vector3f(
                (float)Math.toRadians(degreeVec.x),
                (float)Math.toRadians(degreeVec.y),
                (float)Math.toRadians(degreeVec.z)
        );
    }

    public static Vector3f slideBasedOnState(ControlData<?> data, float prevPercent, float percent, float ageInTicks, float amountToSlide, int animTime, Vector3f direction){
        prevPercent = Mth.clamp(prevPercent, 0.0F, 1.0F);
        percent = Mth.clamp(percent, 0.0F, 1.0F);

        float deltaPercent = percent - prevPercent;
        float startRot = prevPercent * amountToSlide;
        float endRot = deltaPercent * amountToSlide;

        float translateAmt = startRot + endRot * data.getAnimationPercent(ageInTicks, animTime);

        return direction.mul(translateAmt);
    }

    public static Optional<ModelPart> getPart(@Nullable ModelPart root, String path, Consumer<ModelPart> folderAction){
        if(root == null || path.isEmpty())
            return Optional.empty();

        int index = path.indexOf('/');

        //if this is the last
        if(index < 0){
            if(root.hasChild(path)){
                ModelPart part = root.getChild(path);
                folderAction.accept(part);
                return Optional.of(part);
            }
            return Optional.empty();
        }

        //Otherwise
        String currentPartPath = path.substring(0, index);
        if(root.hasChild(currentPartPath)){
            final ModelPart currentPart = root.getChild(currentPartPath);
            return getPart(currentPart, path.substring(index + 1), folderAction);
        }

        return Optional.empty();

    }

    public static ModelPart translateTo(PoseStack pose, ModelPart start, String childPath){
        if(start == null)
            return null;

        int index = childPath.indexOf('/'); //Last part to search
        if(index < 0){
            if(start.hasChild(childPath)){
                ModelPart part = start.getChild(childPath);
                part.translateAndRotate(pose);
                return part;
            }
            return start;
        }

        String partName = childPath.substring(0, index);
        ModelPart part = start.getChild(partName);
        part.translateAndRotate(pose);
        return translateTo(pose, part, childPath.substring(index + 1));
    }

    public static void translateToCenter(PoseStack pose, @Nullable ModelPart part){
        if(part == null)
            return;

        ObjectHolder<Float> minX = new ObjectHolder<>(0.0F),
                minY = new ObjectHolder<>(0.0F),
                minZ = new ObjectHolder<>(0.0F);
        ObjectHolder<Float> maxX = new ObjectHolder<>(0.0F),
                maxY = new ObjectHolder<>(0.0F),
                maxZ = new ObjectHolder<>(0.0F);

        ObjectHolder<Integer> count = new ObjectHolder<>(0);

        part.visit(pose, (p, path, index, cube) -> {
            minX.set(minX.get() + cube.minX);
            minY.set(minY.get() + cube.minY);
            minZ.set(minZ.get() + cube.minZ);

            maxX.set(maxX.get() + cube.maxX);
            maxY.set(maxY.get() + cube.maxY);
            maxZ.set(maxZ.get() + cube.maxZ);

            count.set(count.get() + 1);
        });

        if(count.get() <= 0)
            return;

        minX.set(minX.get() / count.get());
        minY.set(minY.get() / count.get());
        minZ.set(minZ.get() / count.get());

        maxX.set(maxX.get() / count.get());
        maxY.set(maxY.get() / count.get());
        maxZ.set(maxZ.get() / count.get());

        pose.translate(
                (minX.get() + (maxX.get() - minX.get()) / 2) / 16.0,
                (minY.get() + (maxY.get() - minY.get()) / 2) / 16.0,
                (minZ.get() + (maxZ.get() - minZ.get()) / 2) / 16.0
        );

    }

    public static Vector3f degrees(float degrees, Axis axis) {
        final float rad = (float)Math.toRadians(degrees);
        switch(axis){
            case X : return new Vector3f(rad, 0, 0);
            case Y : return new Vector3f(0, rad, 0);
            case Z : return new Vector3f(0, 0, rad);
        }
        return new Vector3f();
    }

    public enum Axis{
        X, Y, Z
    }
}
