package net.tardis.mod.client.animations.consoles;

import net.minecraft.client.Minecraft;
import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.client.animation.Keyframe;
import net.minecraft.client.animation.KeyframeAnimations;
import net.minecraft.core.Direction;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.consoles.G8ConsoleModel;
import net.tardis.mod.control.IncrementControl;
import net.tardis.mod.control.datas.*;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.registry.ControlRegistry;
import org.joml.Vector3f;

public class G8ConsoleAnimations {

    public static final AnimationDefinition ROTOR_FLIGHT = AnimationDefinition.Builder.withLength(3.000000F).looping().addAnimation("glow_spinner_rings", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.000000F, KeyframeAnimations.degreeVec(0.000000F, 120.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(2.000000F, KeyframeAnimations.degreeVec(0.000000F, 240.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.degreeVec(0.000000F, 360.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("spinner_1", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.degreeVec(45.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("spinner_1", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.posVec(0.000000F, 0.600000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("spinner_2", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.degreeVec(-45.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("spinner_2", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.posVec(0.000000F, 1.250000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("rotor_core", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.degreeVec(0.000000F, 90.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.degreeVec(0.000000F, 180.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("rotor_core", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(0.708300F, KeyframeAnimations.posVec(0.000000F, -2.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.500000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(2.250000F, KeyframeAnimations.posVec(0.000000F, 2.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).build();

    public static final AnimationDefinition RANDOMIZER_CONTROL = AnimationDefinition.Builder.withLength(3.000000F).addAnimation("globe_lift_spin", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(1.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(2.000000F, KeyframeAnimations.degreeVec(0.000000F, 360.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).addAnimation("globe_lift_spin", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.000000F, KeyframeAnimations.posVec(0.000000F, 3.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.208300F, KeyframeAnimations.posVec(0.000000F, 2.750000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.833300F, KeyframeAnimations.posVec(0.000000F, 3.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(3.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition DOOR = AnimationDefinition.Builder.withLength(1.5F)
            .addAnimation("doorknob_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.75F, KeyframeAnimations.degreeVec(0.0F, 85.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.5F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();


    public static void animateConditional(G8ConsoleModel<?> root, ITardisLevel tardis, float ageInTicks){
        final ControlDataBool handbrake = tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        final ControlDataFloat throttle = tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get());
        final ControlDataBool stabilizers = tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get());
        final ControlDataInt increment = tardis.getControlDataOrCreate(ControlRegistry.INCREMENT.get());
        final ControlDataEnum<Direction> facing = tardis.getControlDataOrCreate(ControlRegistry.FACING.get());
        final ControlDataEnum<LandingType> landing = tardis.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get());

        root.getAnyDescendantWithName("hb_switch_rotate_y").ifPresent(control -> {
            control.offsetRotation(new Vector3f(AnimationHelper.getRotationBaseOnState(handbrake, -40, ageInTicks, 10, handbrake.get()), 0, 0));
        });

        root.getAnyDescendantWithName("throttleknob_rotate_x").ifPresent(control -> {
            //control.offsetRotation(new Vector3f(AnimationHelper.getRotationBaseOnState(throttle, -30, ageInTicks, 5, true), 0, 0));
            control.offsetRotation(AnimationHelper.getSteppedRotation(throttle, -30, throttle.getPrevious(), throttle.get(), ageInTicks, 20, new Vector3f(1, 0, 0)));
        });

        root.getAnyDescendantWithName("needle").ifPresent(control -> {
            //Display fuel amount
            control.offsetRotation(new Vector3f(0, (float)Math.toRadians((tardis.getFuelHandler().getStoredArtron() / (float)tardis.getFuelHandler().getMaxArtron()) * 180.0F), 0));
        });

        root.getAnyDescendantWithName("glow_stablizers_slide_z").ifPresent(control -> {
            control.offsetPos(AnimationHelper.slideBasedOnState(stabilizers, stabilizers.getPrevious() ? 1.0F : 0.0F, stabilizers.get() ? 1.0F : 0.0F, ageInTicks, 1.5F, 20, new Vector3f(0, 0, 1)));
        });

        root.getAnyDescendantWithName("inc_lever").ifPresent(control -> {
            control.offsetRotation(AnimationHelper.getSteppedRotation(increment, -42.5F,
                    increment.getPrevious() / (float) IncrementControl.VALUES.length,
                    increment.get() / (float) IncrementControl.VALUES.length,
                    ageInTicks, 30, new Vector3f(1, 0, 0)
            ));
        });

        root.getAnyDescendantWithName("landing_switch_rotate_x").ifPresent(control -> {
            control.offsetRotation(new Vector3f(
                AnimationHelper.getRotationBaseOnState(landing, -50, 50, ageInTicks, 30, landing.get() == LandingType.UP),
                    0, 0));
        });

        root.getAnyDescendantWithName("facing_knob_rotate_y").ifPresent(control -> {
            control.offsetRotation(
                    AnimationHelper.getSteppedRotation(facing, 360, facing.getPrevious().toYRot() / 360.0F, facing.get().toYRot() / 360.0F, ageInTicks, 40, new Vector3f(0, 1, 0)));
        });
    }

}
