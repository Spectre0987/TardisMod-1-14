package net.tardis.mod.network.packets.tardis;

import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.Function;

public class TardisDataType<T extends TardisData> {

    final int id;
    final Function<Integer, T> factory;

    public TardisDataType(int id, Function<Integer, T> factory){
        this.id = id;
        this.factory = factory;
    }

    public T create(){
        return this.factory.apply(this.id);
    }

    public T create(ITardisLevel tardis){
        T data = this.factory.apply(this.id);
        data.createFromTardis(tardis);
        return data;
    }

    public int getId(){
        return this.id;
    }


}
