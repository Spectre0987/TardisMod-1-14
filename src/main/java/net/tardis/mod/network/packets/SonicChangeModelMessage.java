package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.SonicQuantiscopeSetting;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.SonicPartRegistry;

import java.util.function.Supplier;

public record SonicChangeModelMessage(BlockPos position, SonicPartRegistry.SonicPartType part) {

    public static void encode(SonicChangeModelMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.position());
        buf.writeRegistryId(SonicPartRegistry.REGISTRY.get(), mes.part());
    }

    public static SonicChangeModelMessage decode(FriendlyByteBuf buf){
        return new SonicChangeModelMessage(buf.readBlockPos(), buf.readRegistryId());
    }

    public static void handle(SonicChangeModelMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            if(context.get().getSender().level.getBlockEntity(mes.position()) instanceof QuantiscopeTile tile){
                if(tile.getCurrentSetting() instanceof SonicQuantiscopeSetting setting){
                    setting.getInventory().ifPresent(inv -> {
                        inv.getStackInSlot(0).getCapability(Capabilities.SONIC).ifPresent(sonic -> {
                            sonic.setSonicPart(mes.part().getSlot(), mes.part());
                        });
                    });
                }
            }
        });
    }

}
