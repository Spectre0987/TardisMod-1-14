package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.Optional;
import java.util.function.Supplier;

public record ItemTrackingCoordMessage(InteractionHand hand, Optional<SpaceTimeCoord> trackingCoord) {


    public static void encode(ItemTrackingCoordMessage mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.hand());
        buf.writeBoolean(mes.trackingCoord.isPresent());
        mes.trackingCoord().ifPresent(coord -> {
            coord.encode(buf);
        });
    }

    public static ItemTrackingCoordMessage decode(FriendlyByteBuf buf){

        final InteractionHand hand = buf.readEnum(InteractionHand.class);
        boolean hasCoord = buf.readBoolean();
        if(hasCoord){
            final SpaceTimeCoord coord = SpaceTimeCoord.decode(buf);
            return new ItemTrackingCoordMessage(hand, Optional.of(coord));
        }

        return new ItemTrackingCoordMessage(hand, Optional.empty());
    }

    public static void handle(ItemTrackingCoordMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> ClientPacketHandler.handleItemTrackingCoord(mes));
    }

}
