package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helpers.InventoryHelper;

import java.util.function.Supplier;

public record ARSSpawnItemMessage(ItemStack stack) {

    public static final String LOW_BUFFER = Tardis.MODID + ".message.ars.low_matter";

    public static void encode(ARSSpawnItemMessage mes, FriendlyByteBuf buf){
        buf.writeItem(mes.stack());
    }

    public static ARSSpawnItemMessage decode(FriendlyByteBuf buf){
        return new ARSSpawnItemMessage(buf.readItem());
    }

    public static void handle(ARSSpawnItemMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                if(tardis.getInteriorManager().getMatterBuffer() >= mes.stack().getCount()){
                    InventoryHelper.addItem(context.get().getSender(), mes.stack());
                    tardis.getInteriorManager().modMatterBuffer(-mes.stack().getCount());
                }
                else context.get().getSender().displayClientMessage(Component.translatable(LOW_BUFFER, tardis.getInteriorManager().getMatterBuffer()), true);
            });
        });
    }

}
