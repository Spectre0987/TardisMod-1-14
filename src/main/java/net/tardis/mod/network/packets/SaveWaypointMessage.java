package net.tardis.mod.network.packets;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.world_structures.WaypointTardisStructure;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

import java.util.function.Supplier;

public record SaveWaypointMessage(String name, Action action) {

    public static final Component SUCCESS = Component.translatable(Tardis.MODID + ".waypoints.saved");
    public static final Component FAILED = Component.translatable(Tardis.MODID + ".waypoints.failed");

    public static void encode(SaveWaypointMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.name().length());
        buf.writeUtf(mes.name(), mes.name().length());
        buf.writeEnum(mes.action());
    }

    public static SaveWaypointMessage decode(FriendlyByteBuf buf){
        return new SaveWaypointMessage(buf.readUtf(buf.readInt()), buf.readEnum(Action.class));
    }

    public static void handle(SaveWaypointMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Capabilities.getCap(Capabilities.TARDIS, context.get().getSender().level).ifPresent(tardis -> {
                for(WaypointTardisStructure waypoint : tardis.getInteriorManager().getAllOfType(TardisWorldStructureRegistry.WAYPOINT.get())){
                    if(mes.action() == Action.SAVE){
                        if(waypoint.addWayPoint(mes.name(), new SpaceTimeCoord(
                                tardis.getLocation().getLevel(),
                                tardis.getLocation().getPos(),
                                tardis.getControlDataOrCreate(ControlRegistry.FACING.get()).get())
                        )){
                            context.get().getSender().displayClientMessage(SUCCESS, true);
                            return;
                        }
                    }
                    else if(mes.action() == Action.DELETE){
                        waypoint.delete(mes.name());
                    }
                }
                context.get().getSender().displayClientMessage(FAILED, true);
            });
        });
    }

    public enum Action{
        SAVE,
        DELETE,
        EDIT;
    }

}
