package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record AlembicFluidChangeMessage(BlockPos pos, boolean isResultTank, FluidStack contents) {

    public static void encode(AlembicFluidChangeMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeBoolean(mes.isResultTank());
        buf.writeFluidStack(mes.contents());
    }

    public static AlembicFluidChangeMessage decode(FriendlyByteBuf buf){
        return new AlembicFluidChangeMessage(buf.readBlockPos(), buf.readBoolean(), buf.readFluidStack());
    }

    public static void handle(AlembicFluidChangeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleAlembicFluid(mes);
        });
    }
}
