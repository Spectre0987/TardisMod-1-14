package net.tardis.mod.network.packets;

import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.EnumMap;
import java.util.function.Supplier;

public record UpdateTardisEngineContents(TardisEngine.EngineSide engineSide, int slotId, ItemStack stack) {

    public static void encode(UpdateTardisEngineContents mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.engineSide());
        buf.writeInt(mes.slotId());
        buf.writeItemStack(mes.stack, false);
    }

    public static UpdateTardisEngineContents decode(FriendlyByteBuf buf){
        return new UpdateTardisEngineContents(buf.readEnum(TardisEngine.EngineSide.class), buf.readInt(), buf.readItem());
    }

    public static void handle(UpdateTardisEngineContents mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleEngineContentsMessage(mes);
        });
    }

}
