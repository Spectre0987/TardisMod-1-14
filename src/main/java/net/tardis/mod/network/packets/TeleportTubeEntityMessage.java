package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.Entity;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.machines.TeleportTubeTile;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

public record TeleportTubeEntityMessage(BlockPos pos, int entityId) {


    public static void encode(TeleportTubeEntityMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.entityId());
    }

    public static TeleportTubeEntityMessage decode(FriendlyByteBuf buf){
        return new TeleportTubeEntityMessage(buf.readBlockPos(), buf.readInt());
    }

    public static void handle(TeleportTubeEntityMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                Entity e = mes.entityId() == -1 ? null : level.getEntity(mes.entityId());
                if(level.getBlockEntity(mes.pos()) instanceof TeleportTubeTile tube){
                    tube.setTeleportingEntity(e);
                }
            });
        });
    }

}
