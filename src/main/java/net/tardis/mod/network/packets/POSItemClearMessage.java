package net.tardis.mod.network.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.registry.ItemRegistry;

import java.util.function.Supplier;

public record POSItemClearMessage(InteractionHand hand) {

    public static void encode(POSItemClearMessage mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.hand());
    }

    public static POSItemClearMessage decode(FriendlyByteBuf buf){
        return new POSItemClearMessage(buf.readEnum(InteractionHand.class));
    }

    public static void handle(POSItemClearMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ItemStack held = context.get().getSender().getItemInHand(mes.hand());
            if(held.getItem() == ItemRegistry.PHASED_OPTIC_SHELL_GENERATOR.get()){
                held.setTag(new CompoundTag());
                context.get().getSender().setItemInHand(mes.hand(), held);
            }
        });
    }

}
