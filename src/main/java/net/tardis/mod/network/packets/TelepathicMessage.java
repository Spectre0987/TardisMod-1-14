package net.tardis.mod.network.packets;

import com.mojang.datafixers.util.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

public record TelepathicMessage(ResourceLocation biome) {

    public static final Component FAIL = Component.translatable(Tardis.MODID + ".telepathic.set.fail");

    public static void encode(TelepathicMessage mes, FriendlyByteBuf buf){
        buf.writeResourceLocation(mes.biome());
    }

    public static TelepathicMessage decode(FriendlyByteBuf buf){
        return new TelepathicMessage(buf.readResourceLocation());
    }

    public static void handle(TelepathicMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

                ServerLevel destLevel = context.get().getSender().server.getLevel(tardis.getDestination().getLevel());

                Pair<BlockPos, ?> result = destLevel.findClosestBiome3d(holder -> {
                    return destLevel.registryAccess().registryOrThrow(Registries.BIOME).getKey(holder.value()).equals(mes.biome());
                }, tardis.getDestination().getPos(), 6400, 32, 64);

                if(result != null)
                    tardis.setDestination(new SpaceTimeCoord(destLevel.dimension(), result.getFirst()));
                else context.get().getSender().sendSystemMessage(FAIL);

            });
        });
    }

}
