package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record UpdateDynamicMonitorBounds(BlockPos pos) {

    public static void encode(UpdateDynamicMonitorBounds mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
    }

    public static UpdateDynamicMonitorBounds decode(FriendlyByteBuf buf){
        return new UpdateDynamicMonitorBounds(buf.readBlockPos());
    }

    public static void handle(UpdateDynamicMonitorBounds mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleDynamicMonitorUpdate(mes);
        });

    }


}
