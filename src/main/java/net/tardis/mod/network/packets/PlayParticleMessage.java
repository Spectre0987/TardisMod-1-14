package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record PlayParticleMessage(BlockPos pos, int extra) {

    public static void encode(PlayParticleMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.extra());
    }

    public static PlayParticleMessage decode(FriendlyByteBuf buf){
        return new PlayParticleMessage(buf.readBlockPos(), buf.readInt());
    }

    public static void handle(PlayParticleMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleBlockParticleMessage(mes);
        });
    }

}
