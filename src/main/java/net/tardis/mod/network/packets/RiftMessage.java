package net.tardis.mod.network.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.ChunkPos;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.network.Network;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public record RiftMessage(ChunkPos pos, boolean add, @Nullable CompoundTag tag) {

    public static void encode(RiftMessage mes, FriendlyByteBuf buf){
        buf.writeChunkPos(mes.pos());
        buf.writeBoolean(mes.add());
        final boolean hasTag = mes.tag() != null;
        buf.writeBoolean(hasTag);
        if(hasTag){
            buf.writeNbt(mes.tag());
        }
    }

    public static RiftMessage decode(FriendlyByteBuf buf){

        final ChunkPos pos = buf.readChunkPos();
        final boolean add = buf.readBoolean();
        if(!buf.readBoolean()){
            return new RiftMessage(pos, add, null);
        }
        return new RiftMessage(pos, add, buf.readNbt());

    }

    public static void handle(RiftMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleRiftMessage(mes);
        });
    }

}
