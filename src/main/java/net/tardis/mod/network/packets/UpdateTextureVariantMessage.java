package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ITextureVariantBlockEntity;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.JsonRegistries;

import java.util.Optional;
import java.util.function.Supplier;

public record UpdateTextureVariantMessage(BlockPos pos, Optional<ResourceLocation> variantId) {


    public static void encode(UpdateTextureVariantMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        Helper.encodeOptional(buf, mes.variantId(), (v, b) -> b.writeResourceLocation(v));
    }

    public static UpdateTextureVariantMessage decode(FriendlyByteBuf buf){
        return new UpdateTextureVariantMessage(buf.readBlockPos(), Helper.decodeOptional(buf, FriendlyByteBuf::readResourceLocation));
    }

    public static void handle(UpdateTextureVariantMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Level level = context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER ? context.get().getSender().getLevel() : Tardis.SIDE.getClientLevel().get();

            if(level.getBlockEntity(mes.pos()) instanceof ITextureVariantBlockEntity var){
                Optional<TextureVariant> tex = mes.variantId().map(id -> level.registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).get(id));
                var.setTextureVariant(tex.orElse(null));
            }
        });
    }

}
