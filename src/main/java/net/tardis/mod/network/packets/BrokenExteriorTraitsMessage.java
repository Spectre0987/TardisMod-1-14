package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public record BrokenExteriorTraitsMessage(BlockPos pos, List<ResourceLocation> traits) {

    public static void encode(BrokenExteriorTraitsMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.traits().size());
        for(ResourceLocation type : mes.traits()){
            buf.writeResourceLocation(type);
        }
    }

    public static BrokenExteriorTraitsMessage decode(FriendlyByteBuf buf){

        final BlockPos pos = buf.readBlockPos();
        final int size = buf.readInt();
        final List<ResourceLocation> traits = new ArrayList<>();
        for(int i = 0; i < size; ++i){
            traits.add(buf.readResourceLocation());
        }
        return new BrokenExteriorTraitsMessage(pos, traits);
    }

    public static void handle(BrokenExteriorTraitsMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleBrokenExteriorTraits(mes);
        });
    }

}
