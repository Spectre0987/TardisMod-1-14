package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.sound.SoundScheme;

import java.util.function.Supplier;

public record SoundSchemeMessage(ResourceLocation loc) {

    public static void encode(SoundSchemeMessage mes, FriendlyByteBuf buf){
        buf.writeResourceLocation(mes.loc());
    }

    public static SoundSchemeMessage decode(FriendlyByteBuf buf){
        return new SoundSchemeMessage(buf.readResourceLocation());
    }

    public static void handle(SoundSchemeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                SoundScheme scheme = tardis.getLevel().registryAccess().registry(JsonRegistries.SOUND_SCHEMES_REGISTRY).get().get(mes.loc());
                if(scheme != null){
                    tardis.getInteriorManager().setSoundScheme(scheme);
                }
            });
        });
    }

}
