package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public record UpdateShellMessage(BlockPos pos, List<BlockPos> shell) {

    public static void encode(UpdateShellMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.shell().size());
        for(BlockPos pos : mes.shell()){
            buf.writeBlockPos(pos);
        }
    }

    public static UpdateShellMessage decode(FriendlyByteBuf buf){
        final BlockPos pos = buf.readBlockPos();
        final int shellSize = buf.readInt();
        final List<BlockPos> shell = new ArrayList<>();
        for(int i = 0; i < shellSize; ++i){
            shell.add(buf.readBlockPos());
        }
        return new UpdateShellMessage(pos, shell);
    }

    public static void handle(UpdateShellMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                if(level.getBlockEntity(mes.pos()) instanceof ExteriorTile ext){
                    ext.setShell(mes.shell());
                }
            });
        });
    }

}
