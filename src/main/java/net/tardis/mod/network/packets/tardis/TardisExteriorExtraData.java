package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class TardisExteriorExtraData extends TardisData{

    boolean isRift;
    boolean landAir;
    boolean landWater;
    Optional<ResourceLocation> textureVariant = Optional.empty();

    public TardisExteriorExtraData(int id) {
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        buf.writeBoolean(this.isRift);
        buf.writeBoolean(this.landWater);
        buf.writeBoolean(this.landAir);
        Helper.encodeOptional(buf, this.textureVariant, (v, b) -> b.writeResourceLocation(v));
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        this.isRift = buf.readBoolean();
        this.landWater = buf.readBoolean();
        this.landAir = buf.readBoolean();
        this.textureVariant = Helper.decodeOptional(buf, FriendlyByteBuf::readResourceLocation);
    }

    @Override
    public void apply(ITardisLevel tardis) {
        tardis.getExteriorExtraData().setInRift(this.isRift);
        tardis.getExteriorExtraData().setCanLandUnderwater(this.landWater);
        tardis.getExteriorExtraData().setCanLandInAir(this.landAir);
        tardis.getExteriorExtraData().setExteriorTexVariant(this.textureVariant.orElse(null));
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        this.isRift = tardis.getExteriorExtraData().IsInRift();
        this.landWater = tardis.getExteriorExtraData().canLandInWater();
        this.landAir = tardis.getExteriorExtraData().canLandMidAir();
        this.textureVariant = tardis.getExteriorExtraData().getExteriorTexVariant();
    }
}
