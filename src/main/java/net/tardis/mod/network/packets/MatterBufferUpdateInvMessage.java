package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record MatterBufferUpdateInvMessage(BlockPos pos, ItemStackHandler handler) {

    public static void encode(MatterBufferUpdateInvMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.handler.getSlots());
        for(int i = 0; i < mes.handler.getSlots(); ++i){
            buf.writeItemStack(mes.handler().getStackInSlot(i), true);
        }
    }

    public static MatterBufferUpdateInvMessage decode(FriendlyByteBuf buf){
        final BlockPos pos = buf.readBlockPos();
        final int size = buf.readInt();

        final ItemStackHandler handler = new ItemStackHandler(size);
        for(int i = 0; i < size; ++i){
            handler.setStackInSlot(i, buf.readItem());
        }
        return new MatterBufferUpdateInvMessage(pos, handler);
    }

    public static void handle(MatterBufferUpdateInvMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleMatterBufferInvChange(mes);
        });
    }

}
