package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.ChunkPos;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record RiftArtronMessage(BlockPos pos, float artron) {


    public static void encode(RiftArtronMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeFloat(mes.artron());
    }

    public static RiftArtronMessage decode(FriendlyByteBuf buf){
        return new RiftArtronMessage(buf.readBlockPos(), buf.readFloat());
    }

    public ChunkPos getPos(){
        return new ChunkPos(this.pos());
    }

    public static void handle(RiftArtronMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleRiftArtronMessage(mes);
        });
    }

}
