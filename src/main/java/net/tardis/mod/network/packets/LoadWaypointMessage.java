package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructrureType;
import net.tardis.mod.misc.tardis.world_structures.WaypointTardisStructure;
import net.tardis.mod.registry.TardisWorldStructureRegistry;
import oshi.util.tuples.Pair;

import java.util.function.Supplier;

public record LoadWaypointMessage(BlockPos structurePos, String name){

    public static final Component SUCCESS_TRANS = Component.translatable("mes." + Tardis.MODID + ".waypoint.load.success");

    public static void encode(LoadWaypointMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.structurePos());
        buf.writeInt(mes.name.length());
        buf.writeUtf(mes.name(), mes.name().length());
    }

    public static LoadWaypointMessage decode(FriendlyByteBuf buf){
        final BlockPos pos = buf.readBlockPos();
        final int nameSize = buf.readInt();
        return new LoadWaypointMessage(pos, buf.readUtf(nameSize));
    }

    public static void handle(LoadWaypointMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Capabilities.getCap(Capabilities.TARDIS, context.get().getSender().level).ifPresent(tardis -> {
                tardis.getInteriorManager().getWorldStructure(mes.structurePos(), TardisWorldStructureRegistry.WAYPOINT.get()).ifPresent(bank -> {
                    if(bank.getWaypoints().containsKey(mes.name())) {
                        tardis.setDestination(bank.getWaypoints().get(mes.name()));
                        context.get().getSender().sendSystemMessage(SUCCESS_TRANS);
                    }
                });
            });
        });
    }


}
