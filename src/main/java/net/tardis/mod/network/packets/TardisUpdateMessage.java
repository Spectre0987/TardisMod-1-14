package net.tardis.mod.network.packets;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.Tags;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.network.packets.tardis.*;

import java.util.function.Function;
import java.util.function.Supplier;

public class TardisUpdateMessage {

    public static final Int2ObjectArrayMap<TardisDataType<?>> DATAS = new Int2ObjectArrayMap<>();

    public static int CURRENT_ID = 0;
    public static final TardisDataType<UpdateDataLocation> UPDATE_LOC = register(UpdateDataLocation::new);
    public static final TardisDataType<TardisFuelData> REFUEL = register(TardisFuelData::new);
    public static final TardisDataType<TardisFlightEventData> FLIGHT_EVENT = register(TardisFlightEventData::new);
    public static final TardisDataType<TardisExteriorData> EXTERIOR_CHANGE = register(TardisExteriorData::new);
    public static final TardisDataType<TardisExteriorExtraData> EXTERIOR_EXTRA = register(TardisExteriorExtraData::new);
    public static final TardisDataType<TardisInteriorManagerData> INTERIOR = register(TardisInteriorManagerData::new);

    public static int id(){
        int id = CURRENT_ID;
        CURRENT_ID++;
        return id;
    }

    public static <T extends TardisData> TardisDataType<T> register(Function<Integer, T> data){
        TardisDataType<T> type = new TardisDataType<>(id(), data);
        DATAS.put(type.getId(), type);
        return type;
    }

    public final TardisData data;


    public TardisUpdateMessage(TardisData data){
        this.data = data;
    }

    public static void encode(TardisUpdateMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.data.getId());
        mes.data.serialize(buf);
    }

    public static TardisUpdateMessage decode(FriendlyByteBuf buf){
        int id = buf.readInt();
        TardisData data = DATAS.get(id).create();
        data.deserialize(buf);
        return new TardisUpdateMessage(data);
    }

    public static void handle(TardisUpdateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleTardisUpdatePacket(mes);
        });
        context.get().setPacketHandled(true);
    }


}
