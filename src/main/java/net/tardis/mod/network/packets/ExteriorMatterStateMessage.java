package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.registry.DematAnimationRegistry;

import java.util.function.Supplier;

public record ExteriorMatterStateMessage(BlockPos pos, MatterStateHandler handler) {

    public static void encode(ExteriorMatterStateMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        mes.handler().encode(buf);
    }

    public static ExteriorMatterStateMessage decode(FriendlyByteBuf buf){
        return new ExteriorMatterStateMessage(
                    buf.readBlockPos(),
                    MatterStateHandler.decode(buf)
                );
    }

    public static void handle(ExteriorMatterStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleExteriorMatterMessage(mes);
        });
    }
}
