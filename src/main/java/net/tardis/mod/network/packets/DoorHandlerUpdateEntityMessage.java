package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.IDoor;

import java.util.function.Supplier;

public record DoorHandlerUpdateEntityMessage(int entityId, DoorHandler handler) {


    public static void encode(DoorHandlerUpdateEntityMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.entityId());
        mes.handler().encode(buf);
    }

    public static DoorHandlerUpdateEntityMessage decode(FriendlyByteBuf buf){
        final int entityId = buf.readInt();
        final DoorHandler handler = new DoorHandler(false);
        handler.decode(buf);
        return new DoorHandlerUpdateEntityMessage(entityId, handler);
    }

    public static void handle(DoorHandlerUpdateEntityMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                if(level.getEntity(mes.entityId()) instanceof IDoor door){
                    door.getDoorHandler().update(mes.handler(), level);
                }
            });
        });
    }


}
