package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.entity.IDrivable;

import java.util.function.Supplier;

public record DriveMessage(int vehicleId, float turnAmount, float speed) {

    public static void encode(DriveMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.vehicleId());
        buf.writeFloat(mes.turnAmount());
        buf.writeFloat(mes.speed());
    }

    public static DriveMessage decode(FriendlyByteBuf buf){
        return new DriveMessage(buf.readInt(), buf.readFloat(), buf.readFloat());
    }

    public static void handle(DriveMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            if(context.get().getSender().getLevel().getEntity(mes.vehicleId()) instanceof IDrivable car){
                car.drive(mes.turnAmount(), mes.speed());
            }
        });
    }

}
