package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.registry.ExteriorRegistry;

import java.util.function.Supplier;

public record ChangeExteriorMessage(ExteriorType exteriorType) {


    public static void encode(ChangeExteriorMessage mes, FriendlyByteBuf buf){

        buf.writeRegistryId(ExteriorRegistry.REGISTRY.get(), mes.exteriorType());

    }

    public static ChangeExteriorMessage decode(FriendlyByteBuf buf){
        return new ChangeExteriorMessage(buf.readRegistryId());
    }

    public static void handle(ChangeExteriorMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.setExterior(mes.exteriorType(), true);
            });
        });
    }

}
