package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

import java.util.function.Supplier;

public record DeleteWaypointMessage(BlockPos pos, String name) {

    public static void encode(DeleteWaypointMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.name().length());
        buf.writeUtf(mes.name(), mes.name().length());
    }

    public static DeleteWaypointMessage decode(FriendlyByteBuf buf){
        final BlockPos bank = buf.readBlockPos();
        final int nameLength = buf.readInt();
        return new DeleteWaypointMessage(bank, buf.readUtf(nameLength));
    }

    public static void handle(DeleteWaypointMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Capabilities.getCap(Capabilities.TARDIS, context.get().getSender().getLevel()).ifPresent(tardis -> {
                tardis.getInteriorManager().getWorldStructure(mes.pos(), TardisWorldStructureRegistry.WAYPOINT.get()).ifPresent(bank -> {
                    bank.delete(mes.name());
                });
            });
        });
    }

}
