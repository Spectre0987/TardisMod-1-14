package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;

import java.util.function.Supplier;

public record AttumentProgressMessage(int attunment) {

    public static void encode(AttumentProgressMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.attunment());
    }

    public static AttumentProgressMessage decode(FriendlyByteBuf buf){
        return new AttumentProgressMessage(buf.readInt());
    }

    public static void handle(AttumentProgressMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {
                    tardis.getEngine().setAttunmentProgress(mes.attunment());
                });
            });
        });
    }

}
