package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

public record PlayMovingSoundMessage(SoundEvent event, SoundSource source, int entityId, float vol, boolean looping) {

    public static void encode(PlayMovingSoundMessage mes, FriendlyByteBuf buf){
        buf.writeRegistryId(ForgeRegistries.SOUND_EVENTS, mes.event());
        buf.writeEnum(mes.source());
        buf.writeInt(mes.entityId());
        buf.writeFloat(mes.vol());
        buf.writeBoolean(mes.looping());
    }

    public static PlayMovingSoundMessage decode(FriendlyByteBuf buf){
        return new PlayMovingSoundMessage(buf.readRegistryId(), buf.readEnum(SoundSource.class), buf.readInt(), buf.readFloat(), buf.readBoolean());
    }

    public static void handle(PlayMovingSoundMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleMovingSoundMessage(mes);
        });
    }

    public static void play(ServerPlayer player, SoundEvent event, SoundSource source, float vol, boolean loop){
        Network.sendTo(player, new PlayMovingSoundMessage(event, source, player.getId(), vol, loop));
    }

    public static void play(ServerPlayer player, SoundEvent event, SoundSource source){
        play(player, event, source, 1.0F, false);
    }

}
