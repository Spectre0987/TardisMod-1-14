package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.helpers.PacketHelper;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.List;
import java.util.function.Supplier;

public class UpdateDoorStateMessage {

    public final ResourceKey<Level> world;
    public final BlockPos pos;

    public final DoorHandler handler;

    public UpdateDoorStateMessage(BlockPos pos, DoorHandler handler){
        this.pos = pos;
        this.world = null;
        this.handler = handler;
    }

    public UpdateDoorStateMessage(ResourceKey<Level> doorWorld, DoorHandler handler){
        this.world = doorWorld;
        this.pos = null;
        this.handler = handler;
    }

    public static void encode(UpdateDoorStateMessage mes, FriendlyByteBuf buf){
        final boolean hasBlockPos = mes.pos != null;
        buf.writeBoolean(hasBlockPos);

        if(hasBlockPos){
            buf.writeBlockPos(mes.pos);
        }
        else buf.writeResourceKey(mes.world);

        buf.writeBoolean(mes.handler.canLock);
        mes.handler.encode(buf);

    }

    public static UpdateDoorStateMessage decode(FriendlyByteBuf buf){

        BlockPos pos = null;
        ResourceKey<Level> level = null;

        final boolean hasBlockPos = buf.readBoolean();

        if(hasBlockPos){
            pos = buf.readBlockPos();
        }
        else level = buf.readResourceKey(Registries.DIMENSION);

        final DoorHandler handler = new DoorHandler(buf.readBoolean());
        handler.decode(buf);

        if(hasBlockPos)
            return new UpdateDoorStateMessage(pos, handler);
        return new UpdateDoorStateMessage(level, handler);
    }

    public static void handle(UpdateDoorStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleUpdateDoorState(mes);
        });
        context.get().setPacketHandled(true);
    }

}
