package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;

import java.util.function.Supplier;

public record QuantiscopeModeChangeMessage(BlockPos pos, ResourceLocation settingId) {

    public static void encode(QuantiscopeModeChangeMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeResourceLocation(mes.settingId());
    }

    public static QuantiscopeModeChangeMessage decode(FriendlyByteBuf buf){
        return new QuantiscopeModeChangeMessage(buf.readBlockPos(), buf.readResourceLocation());
    }

    public static void handle(QuantiscopeModeChangeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            if(context.get().getSender().getLevel().getBlockEntity(mes.pos()) instanceof QuantiscopeTile tile){
                tile.setSetting(mes.settingId());
                tile.getCurrentSetting().getMenu().ifPresent(menu -> NetworkHooks.openScreen(context.get().getSender(), new SimpleMenuProvider(menu, tile.getBlockState().getBlock().getName()), tile.getBlockPos()));
            }
        });
    }

}
