package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record OpenGuiDataMessage(GuiData data) {

    public static void encode(OpenGuiDataMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.data().id);
        mes.data().encode(buf);
    }

    public static OpenGuiDataMessage decode(FriendlyByteBuf buf){
        final int dataId = buf.readInt();

        final GuiData data = GuiDatas.createById(dataId).get();
        data.decode(buf);
        return new OpenGuiDataMessage(data);
    }

    public static void handle(OpenGuiDataMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleGuiDataMessage(mes);
        });
    }

}
