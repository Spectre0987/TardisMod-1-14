package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record UpdateTARDISNotifMessage(int notifId){

    public static void encode(UpdateTARDISNotifMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.notifId());
    }

    public static UpdateTARDISNotifMessage decode(FriendlyByteBuf buf){
        return new UpdateTARDISNotifMessage(buf.readInt());
    }

    public static void handle(UpdateTARDISNotifMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> ClientPacketHandler.handleTardisNotif(mes));
    }

}
