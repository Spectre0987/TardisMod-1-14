package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.SonicUpgradeQuantiscopeSetting;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.client.gui.containers.quantiscope.SonicUpgradeQuantiscopeMenuScreen;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

public record SonicUpgradeQuantiscopeMessage(BlockPos position, boolean state) {


    public static void encode(SonicUpgradeQuantiscopeMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.position());
        buf.writeBoolean(mes.state());
    }

    public static SonicUpgradeQuantiscopeMessage decode(FriendlyByteBuf buf){
        return new SonicUpgradeQuantiscopeMessage(buf.readBlockPos(), buf.readBoolean());
    }

    public static void handle(SonicUpgradeQuantiscopeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            if(context.get().getSender().getLevel().getBlockEntity(mes.position()) instanceof QuantiscopeTile tile){
                if(tile.getCurrentSetting() instanceof SonicUpgradeQuantiscopeSetting setting){
                    setting.getInventory().ifPresent(inv -> {
                        LazyOptional<ISonicCapability> sonicHolder = inv.getStackInSlot(6).getCapability(Capabilities.SONIC);
                        if(sonicHolder.isPresent()){
                            //If turning on, load all upgrades
                            if(mes.state()){
                                for(int i = 0; i < 6; ++i){
                                    sonicHolder.orElseThrow(NullPointerException::new).getUpgradeInv().insertItem(i, inv.extractItem(i, 100, false), false);
                                }
                            }
                        }
                    });
                }
            }
        });
    }

}
