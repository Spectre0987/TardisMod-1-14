package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.ars.RoomEntry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.DelayedServerTask;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.world.data.ARSRoomLevelData;

import java.util.function.Supplier;

public record ChangeARSRoomMessage(ResourceLocation targetRoom) {


    public static void encode(ChangeARSRoomMessage mes, FriendlyByteBuf buf){
        buf.writeResourceLocation(mes.targetRoom());
    }

    public static ChangeARSRoomMessage decode(FriendlyByteBuf buf){
        return new ChangeARSRoomMessage(buf.readResourceLocation());
    }

    public static void handle(ChangeARSRoomMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            final ServerLevel level = context.get().getSender().getLevel();
            Capabilities.getCap(Capabilities.TARDIS, level).ifPresent(tardis -> {

                if(tardis.getUnlockHandler().isUnlocked(ResourceKey.create(JsonRegistries.ARS_ROOM_REGISTRY, mes.targetRoom()))){
                    level.registryAccess().registryOrThrow(JsonRegistries.ARS_ROOM_REGISTRY).getOptional(mes.targetRoom()).ifPresent(room -> {
                        final RoomEntry oldEntry = ARSRoomLevelData.getData(level).getRoomFor(context.get().getSender().getOnPos()).get();

                        //Clear old blocks
                        BlockPos.betweenClosedStream(oldEntry.start, oldEntry.start.offset(oldEntry.size).offset(-1, 0, -1))
                                        .forEach(pos -> context.get().getSender().getLevel().setBlock(pos, Blocks.AIR.defaultBlockState(), 48));


                        room.spawnRoom(level, oldEntry.start.below(oldEntry.getRoom(level.registryAccess()).get().getYOffset()), level.getStructureManager(), ARSRoom.DEFAULT_SETTINGS);

                    });
                }

            });
        });
    }

}
