package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.PacketEncoder;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record UpdateChameleonDisguseMessage(BlockPos pos, BlockState state){

    public static void encode(UpdateChameleonDisguseMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeNbt((CompoundTag) BlockState.CODEC.encodeStart(NbtOps.INSTANCE, mes.state()).getOrThrow(false, Tardis.LOGGER::warn));
    }

    public static UpdateChameleonDisguseMessage decode(FriendlyByteBuf buf){
        return new UpdateChameleonDisguseMessage(
                buf.readBlockPos(),
                BlockState.CODEC.parse(NbtOps.INSTANCE, buf.readNbt()).getOrThrow(false, Tardis.LOGGER::warn)
        );
    }

    public static void handle(UpdateChameleonDisguseMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleChameleonBlockUpdate(mes);
        });
    }

}
