package net.tardis.mod.network;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.tardis.mod.block.IPlayParticleEffects;
import net.tardis.mod.blockentities.BrokenExteriorTile;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.blockentities.MonitorVariableTile;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.blockentities.machines.FabricatorTile;
import net.tardis.mod.blockentities.machines.MatterBufferTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.cfl.ITrackingFunction;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.menu.EngineMenu;
import net.tardis.mod.misc.IDoor;
import net.tardis.mod.misc.IHaveMatterState;
import net.tardis.mod.network.packets.*;
import net.tardis.mod.sound.MovingSound;

import java.util.Optional;
import java.util.Set;

@OnlyIn(Dist.CLIENT)
public class ClientPacketHandler {

    public static void handleDimSyncPacket(SyncDimensionListMessage mes){

        if(Minecraft.getInstance().player == null || Minecraft.getInstance().player.connection.levels() == null)
            return;

        Set<ResourceKey<Level>> levels = Minecraft.getInstance().player.connection.levels();
        //If this player knows about this dimension
        if(levels.contains(mes.level)){
            //If remove
            if(!mes.add){
                levels.remove(mes.level);
            }
        }
        //If player does not know about this dim and we're trying to add it
        else if(mes.add){
            levels.add(mes.level);
        }
    }

    public static void handleControlDataPacket(ControlDataMessage mes) {
        if(Minecraft.getInstance().level != null){
            Minecraft.getInstance().level.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                mes.data.copyTo(cap.getControlDataOrCreate(mes.data.getType()));
                cap.getControlDataOrCreate(mes.data.getType()).playAnimation((int)cap.getAnimationTicks(), true);
            });
        }
    }

    public static void handleControlSizeMessage(ControlEntitySizeMessage mes) {
        Level level = Minecraft.getInstance().level;
        if(level != null){
            if(level.getEntity(mes.entityID) instanceof ControlEntity control){
                control.setSize(mes.size);
            }
        }
    }

    public static void handleTardisFlightState(TardisFlightStateMessage mes) {
        //System.out.println("Got flight packet with %i, %i, %s".formatted(mes.flightTicks, mes.reachedTick, mes.state));
        Level level = Minecraft.getInstance().level;
        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.setFlightState(mes.flightTicks, mes.landTick, mes.distanceTraveled, mes.state, mes.course);
            });
        }
    }

    public static void handleUpdateDoorState(UpdateDoorStateMessage mes) {
        Level level = Minecraft.getInstance().level;
        if(level != null){
            if(mes.pos != null){
                if(level.getBlockEntity(mes.pos) instanceof IDoor door){
                    door.getDoorHandler().update(mes.handler, level);
                }
            }
            else{
                level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                    tardis.getInteriorManager().getDoorHandler().update(mes.handler, level);
                });
            }
        }
    }

    public static void handleTardisLoadMessage(TardisLoadMessage mes) {
        Level level = Minecraft.getInstance().level;

        if(level != null){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.deserializeNBT(mes.tag());
            });
        }
    }

    public static void handleTardisUpdatePacket(TardisUpdateMessage mes) {
        getLevel().ifPresent(level -> {
            level.getCapability(Capabilities.TARDIS).ifPresent(mes.data::apply);
        });
    }


    public static Optional<Level> getLevel(){
        return Minecraft.getInstance().level == null ? Optional.empty() : Optional.of(Minecraft.getInstance().level);
    }

    public static Optional<Player> getPlayer(){
        final Player player = Minecraft.getInstance().player;
        return player == null ? Optional.empty() : Optional.of(player);
    }

    public static void handleEngineToggleMessage(ToggleEngineSlotMessage mes) {
        Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level).ifPresent(tardis -> {

            final ItemStack item = tardis.getEngine().getInventoryFor(mes.engineSide()).getStackInSlot(mes.slotID());

            if(item.getItem() instanceof IEngineToggleable sysItem){
                sysItem.onToggle(tardis, mes.activated());
            }

        });

    }

    public static void handleGuiDataMessage(OpenGuiDataMessage mes) {

        final Player player = Minecraft.getInstance().player;

        if(player != null){
            ClientRegistry.onGuiDataOpened(mes.data());
        }

    }

    public static void handleEngineContentsMessage(UpdateTardisEngineContents mes) {

        getLevel().ifPresent(level -> {
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {

                if(mes.slotId() == -1){
                    if(Minecraft.getInstance().player.containerMenu instanceof EngineMenu engine){
                        engine.setCarried(mes.stack());
                    }
                }
                else tardis.getEngine().getInventoryFor(mes.engineSide()).setStackInSlot(mes.slotId(), mes.stack());

            });
        });

    }

    public static void handleShakeMessage(StartShakeMessage mes) {

        getPlayer().ifPresent(player -> {
            player.getCapability(Capabilities.PLAYER).ifPresent(cap -> {
                cap.startShaking(mes.shakePower(), mes.shakeTicks());
            });
        });

    }

    public static void handleExteriorMatterMessage(ExteriorMatterStateMessage mes) {
        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof IHaveMatterState ext){
                ext.getMatterStateHandler().copyFrom(mes.handler());
            }
        });
    }
    public static void handleMovingSoundMessage(PlayMovingSoundMessage mes) {
        getLevel().ifPresent(level -> {
            Entity e = level.getEntity(mes.entityId());
            if(e != null){
                Minecraft.getInstance().getSoundManager().play(new MovingSound(e, mes.event(), mes.source(), level.random, mes.vol(), mes.looping()));
            }
        });
    }

    public static void handleBlockParticleMessage(PlayParticleMessage mes) {

        getLevel().ifPresent(level -> {
            if(level.getBlockState(mes.pos()).getBlock() instanceof IPlayParticleEffects effect){
                effect.playParticles(level, mes.pos(), mes.extra());
            }
        });

    }

    public static void handleItemTrackingCoord(ItemTrackingCoordMessage mes) {

        getPlayer().ifPresent(player -> {
            ItemStack held = player.getItemInHand(mes.hand());
            held.getCapability(Capabilities.CFL).ifPresent(tool -> {
                tool.getCurrentFunction().ifPresent(func -> {
                    if(func instanceof ITrackingFunction track){
                        track.setTrackingPos(mes.trackingCoord().orElse(null));
                    }
                });
            });
        });

    }

    public static void handleTardisNotif(UpdateTARDISNotifMessage mes) {
        getLevel().ifPresent(level -> {
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.sendNotification(mes.notifId());
            });
        });
    }

    public static void handleRiftMessage(RiftMessage mes) {
        getLevel().ifPresent(level -> {
            Capabilities.getCap(Capabilities.CHUNK, level.getChunk(mes.pos().x, mes.pos().z)).ifPresent(c -> {
                c.setRift(mes.add() ? new Rift(c.getChunk(), mes.tag()) : null);
            });
        });
    }

    public static void handleBrokenExteriorTraits(BrokenExteriorTraitsMessage mes) {
        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof BrokenExteriorTile broken){
                //broken.setTraits(mes.traits());
                //TODO: FIX
            }
        });
    }

    public static void handleAlembicFluid(AlembicFluidChangeMessage mes) {

        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof AlembicBlockEntity alembic){
                FluidTank tank = mes.isResultTank() ? alembic.getResultTank() : alembic.getCraftingTank();
                tank.setFluid(mes.contents());
            }
        });

    }

    public static void handleRiftArtronMessage(RiftArtronMessage mes) {
        getLevel().ifPresent(level -> {
            LevelChunk chunk = level.getChunk(mes.getPos().x, mes.getPos().z);
            Capabilities.getCap(Capabilities.CHUNK, chunk).ifPresent(c -> {

                if(c.getRift().isEmpty())
                    c.setRift(new Rift(chunk, mes.pos()));

                c.getRift().ifPresent(rift -> rift.setArtronDirect(mes.artron()));
            });
        });
    }

    public static void handleChameleonBlockUpdate(UpdateChameleonDisguseMessage mes) {
        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof IDisguisedBlock dis){
                dis.setDisguisedState(mes.state());
            }
        });
    }

    public static void handleMatterBufferInvChange(MatterBufferUpdateInvMessage mes) {

        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof MatterBufferTile matter){
                for(int i = 0; i < mes.handler().getSlots(); ++i){
                    matter.getInventory().setStackInSlot(i, mes.handler().getStackInSlot(i));
                }
            }
        });

    }

    public static void handleDynamicMonitorUpdate(UpdateDynamicMonitorBounds mes) {
        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof MonitorVariableTile mon){
                mon.refreshBounds();
            }
        });
    }

    public static void handleFabricateMessage(FabricatorSetCraftingItemMessage mes) {
        getLevel().ifPresent(level -> {
            if(level.getBlockEntity(mes.pos()) instanceof FabricatorTile fab){
                fab.setCraftingItem(mes.item().orElse(null));
            }
        });
    }
}
