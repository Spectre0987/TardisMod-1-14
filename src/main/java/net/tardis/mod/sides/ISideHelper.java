package net.tardis.mod.sides;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

import java.util.Optional;

public interface ISideHelper {

    void openGui(int id);

    Optional<Level> getClientLevel();
    Optional<Player> getClientPlayer();

    Optional<Integer> getTARDISLightLevel();

    void playMovingSound(Entity target, SoundEvent soundEvent, SoundSource soundSource, float vol);
}
