package net.tardis.mod.sides;

import net.minecraft.client.Minecraft;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.sound.MovingSound;

import java.util.Optional;

public class ClientSideHelper implements ISideHelper{
    @Override
    public void openGui(int id) {

    }

    @Override
    public Optional<Level> getClientLevel() {
        Level world = Minecraft.getInstance().level;
        return world == null ? Optional.empty() : Optional.of(world);
    }

    @Override
    public Optional<Player> getClientPlayer() {
        Player player = Minecraft.getInstance().player;
        return player == null ? Optional.empty() : Optional.of(player);
    }

    @Override
    public Optional<Integer> getTARDISLightLevel() {
        return Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level)
                .map(t -> Mth.floor(t.getInteriorManager().getTardisLightLevel() * 15));
    }

    @Override
    public void playMovingSound(Entity target, SoundEvent soundEvent, SoundSource soundSource, float vol) {
        Minecraft.getInstance().getSoundManager().play(new MovingSound(target, soundEvent, soundSource, target.getLevel().getRandom(), 1.0F, false));
    }
}
