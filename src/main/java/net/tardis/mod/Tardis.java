package net.tardis.mod;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.tardis.mod.advancement.TardisTriggers;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.Roundels;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;
import net.tardis.mod.config.Config;
import net.tardis.mod.dimension.DimensionTypes;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.fluids.FluidRegistry;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.loot.LootRegistry;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.network.Network;
import net.tardis.mod.recipes.RecipeRegistry;
import net.tardis.mod.registry.*;
import net.tardis.mod.sides.CommonSideHelper;
import net.tardis.mod.sides.ISideHelper;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.world.WorldRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Tardis.MODID)
public class Tardis {

    public static final String MODID = "tardis";
    public static ISideHelper SIDE = new CommonSideHelper();
    public static Logger LOGGER = LogManager.getLogger(MODID);

    public Tardis(){
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::commonSetup);

        ManualEntry.EntryType.init();
        TardisTriggers.init();

        ItemRegistry.ITEMS.register(bus);
        BlockRegistry.BLOCKS.register(bus);
        TileRegistry.TYPES.register(bus);
        ControlRegistry.TYPES.register(bus);
        EntityRegistry.TYPES.register(bus);
        ExteriorRegistry.EXTERIORS.register(bus);
        WorldRegistry.registerRegistries(bus);
        UpgradeRegistry.UPGRADES.register(bus);
        RecipeRegistry.RECIPES.register(bus);
        MenuRegistry.MENUS.register(bus);
        Roundels.register();
        FlightEventRegistry.FLIGHT_EVENTS.register(bus);
        MonitorFunctionRegistry.FUNCTIONS.register(bus);
        SoundRegistry.SOUNDS.register(bus);
        FluidRegistry.register(bus);
        SubsystemRegistry.register(bus);
        LootRegistry.LOOT_MOD_REG.register(bus);
        TraitRegistry.TYPES.register(bus);
        ItemFunctionRegistry.TYPES.register(bus);
        DematAnimationRegistry.ANIMATION.register(bus);
        SonicPartRegistry.PARTS.register(bus);
        VortexPhenomenaRegistry.TYPES.register(bus);
        ParticleRegistry.TYPES.register(bus);
        TardisWorldStructureRegistry.TYPES.register(bus);
        EntityDataRegistry.SERIALIZERS.register(bus);
        LoyaltyFunctionRegistry.LOYALTY.register(bus);

        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.Server.CONFIG);

    }

    @SubscribeEvent
    public void commonSetup(FMLCommonSetupEvent event){
        Network.registerPackets();
        DimensionTypes.register();
        SubsystemRegistry.registerSubsystems();
    }

}
