package net.tardis.mod.config;

import net.minecraftforge.common.ForgeConfigSpec;

public class Config {

    public static class Server{

        public static final ForgeConfigSpec CONFIG = server();

        //General
        public static ForgeConfigSpec.ConfigValue<Integer> CONTROL_BASE_TIME;
        public static ForgeConfigSpec.ConfigValue<Boolean> CAN_REFUEL_OUTIDE_RIFTS;
        public static ForgeConfigSpec.ConfigValue<Integer> TARDIS_BASE_SPEED;
        public static ForgeConfigSpec.ConfigValue<Integer> SONIC_PORT_POWER;
        public static ForgeConfigSpec.ConfigValue<Integer> FE_BUFFER_PER_TICK;

        //Sonic
        public static ForgeConfigSpec.ConfigValue<Double> SONIC_INTERACT_DIST;

        public static final ForgeConfigSpec server(){
            ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

            builder.comment("General").push("general");
                CONTROL_BASE_TIME = builder.comment("The time before a flight event is failed",
                                "Math is time + time * throttle",
                                "Example: setting this to 3 would mean you have 3 seconds on the highest throttle setting, and 6 at the lowest")
                        .define("flight_event_time", 5);
                CAN_REFUEL_OUTIDE_RIFTS = builder.comment("True if TARDISes can refuel outside of rifts")
                                .define("can_fuel_outside_rifts", false);
                TARDIS_BASE_SPEED = builder.comment("The speed of the tardis in blocks per second")
                                .define("tardis/speed", 200);
                SONIC_PORT_POWER = builder.comment("FE/t to give items in the sonic port")
                                .define("tardis/sonic_port_power", 1);
                FE_BUFFER_PER_TICK = builder.comment("Amount of FE/T the tardis' energy buffer naturally regenerates by")
                                .define("tardis/fe_buffer_regen", 32);
            builder.pop();

            builder.push("sonic").comment("Sonic Settings");
            SONIC_INTERACT_DIST = builder.comment("The distance at which sonics can interact with blocks")
                            .define("sonic/block_interact", 32.0);

            builder.pop();

            return builder.build();
        }

    }

}
