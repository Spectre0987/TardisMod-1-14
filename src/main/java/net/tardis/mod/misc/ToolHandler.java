package net.tardis.mod.misc;

import net.minecraft.core.RegistryAccess;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.item.misc.TardisTool;
import net.tardis.mod.registry.JsonRegistries;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class ToolHandler{

    private EnumMap<ToolType, AtomicInteger> work = new EnumMap<>(ToolType.class);
    private long lastPlayedSoundTime = 0;

    public void playParticleForTool(Level level, Vec3 hitPos, TardisTool tool){
        if(tool.part() instanceof ParticleOptions options)
            level.addParticle(options, hitPos.x, hitPos.y, hitPos.z, 0, 0, 0);
        if(level.getGameTime() > lastPlayedSoundTime){
            lastPlayedSoundTime = level.getGameTime() + tool.toolSoundLoopTime();
            tool.toolSound().ifPresent(sound -> {
                level.playSound(null, WorldHelper.vecToBlockPos(hitPos), sound.value(), SoundSource.BLOCKS, 1.0F, 1.0F);
            });
        }
    }

    /**
     *
     * @param level
     * @param hitPos
     * @param stack
     * @return false if this item is not a tool
     */
    public boolean doToolWork(Level level, Vec3 hitPos, ItemStack stack){
        List<TardisTool> tools = getToolFromItem(stack, level.registryAccess());
        boolean wasWorkDone = false;
        for(TardisTool tool : tools){
            this.playParticleForTool(level, hitPos, tool);
            this.work.computeIfAbsent(tool.type(), t -> new AtomicInteger()).incrementAndGet();
            wasWorkDone = true;
        }
        return wasWorkDone;
    }

    public List<TardisTool> getToolFromItem(ItemStack stack, RegistryAccess access){
        final ArrayList<TardisTool> tools = new ArrayList<>();
        for(TardisTool tool : access.registryOrThrow(JsonRegistries.TOOLS)){
            if(tool.item() == stack.getItem())
                tools.add(tool);
        }
        return tools;
    }

    public int getWorkFromTool(ToolType type){
        return this.work.containsKey(type) ? this.work.get(type).get() : 0;
    }

    public void clear(){
        for(AtomicInteger i : this.work.values()){
            i.set(0);
        }
    }

}
