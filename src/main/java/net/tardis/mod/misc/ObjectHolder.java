package net.tardis.mod.misc;

public class ObjectHolder<T>{

    private T value;

    public ObjectHolder(T value){
        this.value = value;
    }

    public void set(T value){
        this.value = value;
    }

    public T get(){
        return this.value;
    }

}
