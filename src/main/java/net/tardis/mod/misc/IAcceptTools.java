package net.tardis.mod.misc;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.item.misc.TardisTool;

public interface IAcceptTools {

    boolean doWork(Level level, Vec3 hit, ItemStack toolUsed);

}
