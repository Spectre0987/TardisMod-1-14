package net.tardis.mod.misc;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextColor;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public interface IAttunable {

    ItemStack onAttuned(ITardisLevel tardis, ItemStack stack);

    int getTicksToAttune();
    Optional<ResourceKey<Level>> getAttunedTardis(ItemStack stack);

    default void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced){
        getAttunedTardis(pStack).ifPresent(tardisKey -> {
            pTooltipComponents.add(Constants.Translation.makeTardisTranslation(tardisKey));
        });
    }
}
