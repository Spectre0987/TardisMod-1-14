package net.tardis.mod.misc;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateTextureVariantMessage;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface ITextureVariantBlockEntity {

    Optional<TextureVariant> getTextureVariant();
    void setTextureVariant(@Nullable TextureVariant variant);

    default void updateTextureVariant(){
        if(this instanceof BlockEntity be && !be.getLevel().isClientSide()){

            Optional<ResourceLocation> varId = getTextureVariant().map(v -> be.getLevel().registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).getKey(v));

            Network.sendToTracking(be, new UpdateTextureVariantMessage(be.getBlockPos(), varId));
        }
    }

}
