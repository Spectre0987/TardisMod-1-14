package net.tardis.mod.misc;

import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class SimpleHandlerContainerWrapper implements Container {

    final ItemStackHandler underlying;
    final Runnable onChange;

    public SimpleHandlerContainerWrapper(ItemStackHandler handler, Runnable onChange){
        this.underlying = handler;
        this.onChange = onChange;
    }

    @Override
    public int getContainerSize() {
        return this.underlying.getSlots();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < this.underlying.getSlots(); ++i){
            if(!this.underlying.getStackInSlot(i).isEmpty())
                return false;
        }
        return true;
    }

    @Override
    public ItemStack getItem(int pSlot) {
        return this.underlying.getStackInSlot(pSlot);
    }

    @Override
    public ItemStack removeItem(int pSlot, int pAmount) {
        this.onChange.run();
        return this.underlying.extractItem(pSlot, pAmount, false);
    }

    @Override
    public ItemStack removeItemNoUpdate(int pSlot) {
        final ItemStack stack = this.underlying.getStackInSlot(pSlot);
        this.underlying.setStackInSlot(pSlot, ItemStack.EMPTY);
        this.onChange.run();
        return stack;
    }

    @Override
    public void setItem(int pSlot, ItemStack stack) {
        this.underlying.setStackInSlot(pSlot, stack);
    }

    @Override
    public void setChanged() {
        this.onChange.run();
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }

    @Override
    public void clearContent() {
        for(int i = 0; i < this.underlying.getSlots(); ++i){
            this.setItem(i, ItemStack.EMPTY);
        }
    }
}
