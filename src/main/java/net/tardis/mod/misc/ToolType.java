package net.tardis.mod.misc;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.registry.ItemRegistry;

import java.util.function.Supplier;

public enum ToolType {

    WELDING("welding", () -> new ItemStack(ItemRegistry.WELDING_TORCH.get())),
    HAMMER("hammer", () -> new ItemStack(ItemRegistry.PISTON_HAMMER.get())),
    SONIC("sonic", () -> new ItemStack(ItemRegistry.SONIC.get()));

    public static Codec<ToolType> CODEC = Codec.STRING.comapFlatMap(ToolType::read, n -> n.name().toLowerCase());

    final String name;
    final Supplier<ItemStack> display;

    ToolType(String name, Supplier<ItemStack> stack){
        this.name = name;
        this.display = stack;
    }

    public ItemStack getDisplay(){
        return this.display.get();
    }

    public String getName(){
        return this.name;
    }

    public static ToolType getTypeFromName(String name){
        for(ToolType type : ToolType.values()){
            if(type.getName().equals(name))
                return type;
        }
        return null;
    }

    public static DataResult<ToolType> read(String name){
        for(ToolType type : ToolType.values()){
            if(type.name.toLowerCase().equals(name.toLowerCase())){
                return DataResult.success(type);
            }
        }
        return DataResult.error(() -> "Error deserializing ToolType, no type found for " + name);
    }

}
