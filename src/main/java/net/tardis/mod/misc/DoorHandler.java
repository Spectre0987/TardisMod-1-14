package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.helpers.PacketHelper;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.sound.SoundRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class DoorHandler implements INBTSerializable<CompoundTag>, IEncodeable {

    public static final String LOCKED_TRANS_KEY = DoorInteractResult.LOCK_CYCLE.makeTransKey("locked");
    public static final String UNLOCKED_TRANS_KEY = DoorInteractResult.LOCK_CYCLE.makeTransKey("unlocked");

    public final boolean canLock;
    public DoorState[] validDoorStates;
    public Predicate<ItemStack> isKey = stack -> false;
    public Runnable packetSender;
    public Runnable linkedDoor;
    public Runnable onSave;

    public int doorIndex = 0;
    public boolean isLocked = false;
    private boolean canBeLockedWithHand = false;

    public DoorHandler(boolean canLock, DoorState... doorStates){
        this.canLock = canLock;
        this.validDoorStates = doorStates;
    }

    public DoorHandler(Predicate<ItemStack> predicate, DoorState... states){
        this(true, states);
        this.setLockPredicate(predicate);
    }

    public boolean isKey(ItemStack stack){
        return this.isKey.test(stack);
    }

    public DoorHandler makeCanBeUnlockedWithHand(){
        this.canBeLockedWithHand = true;
        return this;
    }

    public DoorHandler setLockPredicate(Predicate<ItemStack> test){
        this.isKey = test;
        return this;
    }

    public DoorHandler setValidDoorStates(DoorState... states){
        this.validDoorStates = states;
        return this;
    }

    public DoorHandler setPacketSender(Runnable packetSender){
        this.packetSender = packetSender;
        return this;
    }

    public DoorHandler setDataSave(Runnable saver){
        this.onSave = saver;
        return this;
    }

    public DoorHandler setLinkedDoor(Runnable run){
        this.linkedDoor = run;
        return this;
    }

    public void save(){
        if(this.onSave != null){
            this.onSave.run();
        }
    }

    public DoorState getDoorState(){
        if(isLocked()){
            return DoorState.CLOSED;
        }
        return this.validDoorStates == null || this.validDoorStates.length <= 0 || this.doorIndex >= this.validDoorStates.length
                ? DoorState.CLOSED : this.validDoorStates[this.doorIndex];
    }

    public boolean isLocked(){
        return this.canLock && this.isLocked;
    }

    public void setLocked(boolean locked){
        this.isLocked = locked;
    }

    public void updateLinkedDoor(){
        if(this.linkedDoor != null)
            this.linkedDoor.run();
    }

    /**
     * Packet use only. Does not trigger a sync or account for if the state is invalid
     * @param state
     */
    public void setDoorState(DoorState[] validStates, DoorState state){
        this.validDoorStates = validStates;
        for(int i = 0; i < this.validDoorStates.length; ++i){
            if(this.validDoorStates[i] == state){
                this.doorIndex = i;
                return;
            }
        }
    }

    public void updateClients(Level level){
        if(this.packetSender != null && !level.isClientSide()){
            this.packetSender.run();
        }
    }

    public DoorInteractResult onInteract(Player player, ItemStack stack, InteractionHand hand){

        if(player.getLevel().isClientSide()){
            return DoorInteractResult.FAILED;
        }

        //If this door has a lock, test the locked condition
        if(canLock){
            //If we're holding the key or this doesn't require a key to lock / unlock, cycle the lock
            if(this.isKey(stack) || (this.canBeLockedWithHand && player.isShiftKeyDown())){
                this.isLocked = !this.isLocked;
                this.updateLinkedDoor();
                this.updateClients(player.getLevel());
                player.displayClientMessage(Component.translatable(this.isLocked ? LOCKED_TRANS_KEY : UNLOCKED_TRANS_KEY), true);
                this.save();
                playSoundAt(player, this.isLocked ? SoundRegistry.DOOR_LOCK.get() : SoundRegistry.DOOR_UNLOCK.get());
                return DoorInteractResult.LOCK_CYCLE;
            }
            //If we're not holding the key, this can lock, and is locked, fail
            if(this.isLocked) {
                playSoundAt(player, SoundRegistry.DOOR_FAILED_LOCKED.get());
                return DoorInteractResult.FAILED;
            }
        }

        if(hand != InteractionHand.MAIN_HAND) //Stop from double cycling
            return DoorInteractResult.FAILED;

        //cycle door index
        if(this.doorIndex + 1 >= this.validDoorStates.length){
            this.doorIndex = 0;
        }
        else ++this.doorIndex;
        playSoundAt(player, getDoorState().isOpen() ? SoundRegistry.DOOR_OPEN.get() : SoundRegistry.DOOR_CLOSE.get());
        this.updateLinkedDoor();
        this.updateClients(player.getLevel());
        this.save();
        return DoorInteractResult.DOOR_CYCLE;

    }

    public void playSoundAt(Player player, SoundEvent event){
        if(!player.level.isClientSide()){
            player.level.playSound(null, player.getOnPos(), event, SoundSource.BLOCKS, 1.0F, 1.0F);
        }
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("door_index", this.doorIndex);
        tag.putBoolean("locked", this.isLocked);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.doorIndex = tag.getInt("door_index");
        this.isLocked = tag.getBoolean("locked");
    }

    public void update(DoorHandler other, Level level) {
        this.validDoorStates = other.validDoorStates;
        this.doorIndex = other.doorIndex;
        this.isLocked = other.isLocked;
        this.updateClients(level);
        this.save();
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeInt(this.doorIndex);
        buf.writeBoolean(this.isLocked);
        buf.writeInt(this.validDoorStates.length);
        for(DoorState state : this.validDoorStates){
            buf.writeEnum(state);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.doorIndex = buf.readInt();
        this.isLocked = buf.readBoolean();

        final int doorStateSize = buf.readInt();
        this.validDoorStates = new DoorState[doorStateSize];
        for(int i = 0; i < doorStateSize; ++i){
            this.validDoorStates[i] = buf.readEnum(DoorState.class);
        }

    }


    public enum DoorInteractResult{
        FAILED(false),
        DOOR_CYCLE(true),
        LOCK_CYCLE(true);

        private final boolean success;

        DoorInteractResult(boolean success){
            this.success = success;
        }

        public boolean isSuccess(){
            return this.success;
        }

        public String makeTransKey(@Nullable String extra){
            return "door_interact.tardis." + this.name().toLowerCase() + (extra != null ? "." + extra : "");
        }

        public String makeTransKey(){
            return makeTransKey(null);
        }

    }

}
