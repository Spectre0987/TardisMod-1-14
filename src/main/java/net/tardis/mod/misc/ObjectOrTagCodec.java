package net.tardis.mod.misc;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraftforge.registries.IForgeRegistry;
import net.tardis.mod.helpers.Helper;

import javax.annotation.Nullable;
import java.util.Optional;

public class ObjectOrTagCodec<T> {

    final IForgeRegistry<T> reg;
    final TagKey<T> tag;
    final Optional<T> obj;
    final Codec<ObjectOrTagCodec<T>> codec;


    private ObjectOrTagCodec(IForgeRegistry<T> reg, TagKey<T> tag, T object){
        this.reg = reg;
        this.tag = tag;
        this.obj = Helper.nullableToOptional(object);
        this.codec = createCodec(reg);
    }

    public ObjectOrTagCodec(IForgeRegistry<T> reg, TagKey<T> tag){
        this(reg, tag, null);
    }

    public ObjectOrTagCodec(IForgeRegistry<T> reg, T item){
        this(reg, null, item);
    }

    public boolean isTag(){
        return tag != null;
    }

    public static <A, O> Codec<ObjectOrTagCodec<A>> createCodec(IForgeRegistry<A> reg){
        return Codec.either(
                TagKey.codec(reg.getRegistryKey()).fieldOf("tag").codec(),
                reg.getCodec().optionalFieldOf("item").codec()
        ).xmap(e ->
            new ObjectOrTagCodec<A>(reg, e.left().orElse(null), e.right().isPresent() ? e.right().get().orElse(null) : null),
                e -> e.tag != null ? Either.left(e.tag) : Either.right(Optional.of(e.obj.get()))
        );
    }

    @Nullable
    public T getObject() {
        return this.obj.isPresent() ? obj.get() : null;
    }

    @Nullable
    public TagKey<T> getTag(){
        return this.tag;
    }

    public boolean matches(T object) {
        if(this.isTag()){
            return reg.tags().getTag(this.getTag()).contains(object);
        }
        else{
            if(this.obj.isPresent()){
                return reg.getKey(object).equals(reg.getKey(this.obj.get()));
            }
        }
        return false;
    }
}
