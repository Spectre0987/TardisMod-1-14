package net.tardis.mod.misc;

import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.portal.PortalInfo;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.helpers.WorldHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class TeleportEntry {

    private static final HashMap<UUID, TeleportEntry> TELEPORTS = new HashMap<>();

    public final ResourceKey<Level> originalDim;
    public final UUID entityID;
    public final ServerLevel destination;
    public final Vec3 position;
    public final float rotation;

    public TeleportEntry(UUID id, ResourceKey<Level> level, ServerLevel destination, Vec3 position, float rotation){
        this.entityID = id;
        this.originalDim = level;
        this.destination = destination;
        this.position = position;
        this.rotation = rotation;
    }

    public TeleportEntry(Entity e, ServerLevel level, Vec3 position, float rotation){
        this(e.getUUID(), e.getLevel().dimension(), level, position, rotation);
    }

    //True if should be removed
    public boolean tick(ServerLevel level){
        Entity e = level.getEntity(this.entityID);
        if(e == null || !e.isAlive() || e.isRemoved()){
            return true;
        }

        this.destination.getChunkAt(WorldHelper.vecToBlockPos(this.position)); // Loads the chunk

        e.changeDimension(this.destination, new BlankTeleporter(new PortalInfo(this.position, e.getDeltaMovement(), this.rotation - 180, 0)));


        return true;
    }

    public boolean shouldRun(ServerLevel level){
        return level.dimension().location().equals(this.originalDim.location());
    }

    public static void add(TeleportEntry teleport){
        if(!TELEPORTS.containsKey(teleport.entityID)){
            TELEPORTS.put(teleport.entityID, teleport);
        }
    }

    public static List<TeleportEntry> getEntries(){
        return new ArrayList<>(TELEPORTS.values());
    }

    public static void remove(UUID id){
        TELEPORTS.remove(id);
    }

    public record LocationData(Vec3 position, float yaw){}

}
