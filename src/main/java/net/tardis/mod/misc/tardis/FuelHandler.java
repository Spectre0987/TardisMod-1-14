package net.tardis.mod.misc.tardis;

import net.minecraft.nbt.FloatTag;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.api.artron.IArtronStorage;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.config.Config;
import net.tardis.mod.item.components.IArtronCapacitorItem;
import net.tardis.mod.network.packets.TardisUpdateMessage;
import net.tardis.mod.network.packets.tardis.TardisFuelData;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.SubsystemRegistry;

public class FuelHandler implements INBTSerializable<FloatTag>, IArtronStorage {

    public static final float BASE_REFUEL_A_SECOND = 1;
    public static final float AU_PER_BLOCK = 1 / 16.0F;

    final ITardisLevel tardis;

    float artron;

    //Only stored for client use
    float maxArtron;
    float chargeMult = 1.0F;

    public FuelHandler(ITardisLevel tardis){
        this.tardis = tardis;
    }

    public void updateFromPacket(TardisFuelData data){
        this.artron = data.artron;
        this.maxArtron = data.maxArtron;
        this.chargeMult = data.chargeRate;
    }

    /**
     *
     * @return - true if flight can continue, false otherwise
     */
    public boolean flightTick(float distTraveled){
        if(!this.tardis.isClient() && this.tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).get() > 0.0F){ //Poll every second
            this.takeArtron(this.calculateMPG() * distTraveled, false);
        }
        return this.artron > 0 && this.getMaxArtron() > 0;
    }

    public void refuelTick(){
        if(!Config.Server.CAN_REFUEL_OUTIDE_RIFTS.get()){
            return;
        }
        if(!this.tardis.isClient() && this.tardis.getLevel().getGameTime() % 20 == 0){ //Regain fuel every second
            this.fillArtron(BASE_REFUEL_A_SECOND, false);
        }
    }

    public void update(){
        tardis.update(TardisUpdateMessage.REFUEL);
    }

    private void setArtronDirect(float newArtron){
        boolean hasChanged = this.artron != newArtron; //If the amount will change
        this.artron = newArtron;
        if(hasChanged)
            this.update();
    }

    /**
     *
     * @return the amount of artron to use for every block traveled
     */
    public float calculateMPG(){
        return AU_PER_BLOCK;
    }

    @Override
    public float getMaxArtron(){

        if(this.tardis.isClient()){
            return this.maxArtron;
        }

        ItemStackHandler handler = this.tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.CAPACITORS);
        int maxArtron = 0;
        for(int i = 0; i < handler.getSlots(); ++i){
            final ItemStack stack = handler.getStackInSlot(i);
            if(stack.getItem() instanceof IArtronCapacitorItem cap){
                maxArtron += cap.getMaxArtron(stack);
            }
        }
        this.maxArtron = maxArtron;
        return this.maxArtron;
    }

    public float calcArtronRechargeMult(){
        TardisEvent.CalcRechargeEvent event = new TardisEvent.CalcRechargeEvent(this.tardis, 1.0F);
        MinecraftForge.EVENT_BUS.post(event);
        return event.getRechargeRate();
    }

    @Override
    public FloatTag serializeNBT() {
        return FloatTag.valueOf(this.artron);
    }

    @Override
    public void deserializeNBT(FloatTag nbt) {
        this.artron = nbt.getAsFloat();
    }

    @Override
    public float fillArtron(float artronToFill, boolean simulate) {
        if(artronToFill <= 0)
            return 0;

        float spaceLeft = this.getMaxArtron() - this.getStoredArtron();
        if(artronToFill >= spaceLeft){
            if(!simulate)
                this.setArtronDirect(this.artron + spaceLeft);
            return spaceLeft;
        }

        if(!simulate)
            this.setArtronDirect(this.artron + artronToFill);
        return artronToFill;
    }

    @Override
    public float takeArtron(float artronToRecieve, boolean simulate) {
        //If trying to take more than we have
        if(artronToRecieve >= this.artron){
            float amountContained = this.artron;
            if(!simulate)
                this.setArtronDirect(0);
            return amountContained;
        }

        if(!simulate)
            this.setArtronDirect(this.artron - artronToRecieve);

        return artronToRecieve;
    }

    @Override
    public float getStoredArtron() {
        return this.artron;
    }

    public boolean shouldRefuel() {
        return SubsystemRegistry.FLUID_LINK_TYPE.get().canBeUsed(this.tardis) && tardis.getControlDataOrCreate(ControlRegistry.REFUELER.get()).get();
    }
}
