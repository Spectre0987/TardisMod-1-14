package net.tardis.mod.misc.tardis.vortex;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.ChunkPos;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.registry.FlightEventRegistry;
import net.tardis.mod.registry.SubsystemRegistry;

public class SpaceBattlePheonomena extends VortexPheonomena{
    public SpaceBattlePheonomena(VortexPhenomenaType<?> type, ChunkPos pos) {
        super(type, pos, 50);
    }

    @Override
    public void onTardisEnter(ITardisLevel tardis) {
        if(SubsystemRegistry.ANTENNA.get().canBeUsed(tardis)){
            tardis.setCurrentFlightEvent(FlightEventRegistry.SPACE_EVENT_DODGE.get());
        }
    }

    @Override
    public void onTardisLeave(ITardisLevel tardis) {

    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
