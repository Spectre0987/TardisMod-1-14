package net.tardis.mod.misc.tardis.vortex;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.ChunkPos;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.flight_event.FlightEventType;

import java.util.function.Supplier;

public class BaseFlightEventPhenomena extends VortexPheonomena{

    final Supplier<FlightEventType> type;

    public BaseFlightEventPhenomena(VortexPhenomenaType<?> type, Supplier<FlightEventType> flightEvent, ChunkPos pos, int radius) {
        super(type, pos, radius);
        this.type = flightEvent;
    }

    @Override
    public void onTardisEnter(ITardisLevel tardis) {
        tardis.setCurrentFlightEvent(this.type.get());
    }

    @Override
    public void onTardisLeave(ITardisLevel tardis) {

    }



    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
