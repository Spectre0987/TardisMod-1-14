package net.tardis.mod.misc.tardis.montior.interior;

import com.mojang.datafixers.kinds.Const;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.InteriorDoorData;
import net.tardis.mod.misc.tardis.montior.BasicMonitorFunction;
import net.tardis.mod.misc.tardis.montior.MonitorFunction;
import net.tardis.mod.registry.MonitorFunctionRegistry;

import java.util.UUID;
import java.util.function.Supplier;

public class IntSetMainDoorMonitorFunction extends BasicMonitorFunction {

    public static Supplier<String> SUCCESS = () -> Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), MonitorFunctionRegistry.SET_MAIN_DOOR.get()) + ".success";
    public static Supplier<Component> FAIL = () -> Component.translatable(
            Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), MonitorFunctionRegistry.SET_MAIN_DOOR.get()) + ".fail"
    );

    public IntSetMainDoorMonitorFunction() {}

    @Override
    public void doServerAction(ITardisLevel tardis, Player player) {
        long distance = Long.MAX_VALUE;
        UUID currentDoor = null;
        BlockPos pos = BlockPos.ZERO;

        for(UUID id : tardis.getInteriorManager().getDoors().keySet()){
            final InteriorDoorData data = tardis.getInteriorManager().getDoors().get(id);
            long dist = (long)player.position().distanceTo(data.getPosition(tardis.getLevel()));
            if(dist < distance){
                distance = dist;
                currentDoor = id;
                pos = Helper.vec3ToBlockPos(data.getPosition(tardis.getLevel()));
            }
        }

        if(currentDoor != null){
            tardis.getInteriorManager().setMainDoor(currentDoor);
            player.sendSystemMessage(Component.translatable(SUCCESS.get(), pos.getX(), pos.getY(), pos.getZ()));
        }
        else player.sendSystemMessage(FAIL.get());

    }

    @Override
    public boolean doClientAction(ITardisLevel tardis, Player player) {
        return true;
    }
}
