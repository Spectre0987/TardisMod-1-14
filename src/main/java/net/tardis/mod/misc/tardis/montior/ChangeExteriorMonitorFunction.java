package net.tardis.mod.misc.tardis.montior;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.subsystem.BasicSubsystem;
import net.tardis.mod.subsystem.Subsystem;

import java.util.Optional;

public class ChangeExteriorMonitorFunction extends OpenGuiMonitorFunction{

    @Override
    public GuiData getGui(ITardisLevel tardis, Player player) {
        return GuiDatas.MON_CHANGE_EXT.create().fromTardis(tardis);
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        final Optional<BasicSubsystem> chameleon = tardis.getSubsystem(SubsystemRegistry.CHAMELEON_TYPE.get());
        return chameleon.isPresent() && !chameleon.get().isBrokenOrOff();
    }
}
