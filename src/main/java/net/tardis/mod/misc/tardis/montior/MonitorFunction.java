package net.tardis.mod.misc.tardis.montior;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;

public interface MonitorFunction {

    void doServerAction(ITardisLevel tardis, Player player);

    /**
     *
     * @param tardis
     * @param player
     * @return true if selecting this should close the menu
     */
    boolean doClientAction(ITardisLevel tardis, Player player);

    Component getText(ITardisLevel level);

    boolean shouldDisplay(ITardisLevel tardis);

}
