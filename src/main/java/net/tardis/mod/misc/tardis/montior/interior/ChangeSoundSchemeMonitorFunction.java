package net.tardis.mod.misc.tardis.montior.interior;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.misc.tardis.montior.BasicMonitorFunction;
import net.tardis.mod.misc.tardis.montior.OpenGuiMonitorFunction;

public class ChangeSoundSchemeMonitorFunction extends OpenGuiMonitorFunction {

    @Override
    public GuiData getGui(ITardisLevel tardis, Player player) {
        return GuiDatas.MON_SOUND_SCHEME.create().fromServer(tardis.getLevel().getServer());
    }
}
