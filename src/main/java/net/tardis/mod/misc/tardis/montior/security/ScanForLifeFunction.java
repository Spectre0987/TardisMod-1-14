package net.tardis.mod.misc.tardis.montior.security;

import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.montior.BasicMonitorFunction;
import net.tardis.mod.registry.MonitorFunctionRegistry;

public class ScanForLifeFunction extends BasicMonitorFunction {

    public ScanForLifeFunction() {}

    @Override
    public void doServerAction(ITardisLevel tardis, Player player) {

        int humanoid = 0;
        int others = 0;

        for(Entity e : ((ServerLevel)tardis.getLevel()).getAllEntities()){
            if(e instanceof Player){
                ++humanoid;
            }
            else{
                if(e instanceof LivingEntity){
                    ++others;
                }
            }
        }

        player.sendSystemMessage(Component.translatable(makeDefaultFeedback(), humanoid, others));
    }

    @Override
    public boolean doClientAction(ITardisLevel tardis, Player player) {
        return false;
    }
}
