package net.tardis.mod.misc.tardis.vortex;

import net.minecraft.world.level.ChunkPos;

import java.util.function.BiFunction;
import java.util.function.Function;

public record VortexPhenomenaType<T extends VortexPheonomena>(BiFunction<VortexPhenomenaType<?>, ChunkPos, T> phenomenaSupplier) {

    public T create(ChunkPos pos){
        return this.phenomenaSupplier().apply(this, pos);
    }

}
