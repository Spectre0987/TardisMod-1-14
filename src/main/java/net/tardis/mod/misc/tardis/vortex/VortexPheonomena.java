package net.tardis.mod.misc.tardis.vortex;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.registry.VortexPhenomenaRegistry;
import net.tardis.mod.world.data.TardisLevelData;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public abstract class VortexPheonomena extends TypeHolder<VortexPhenomenaType<?>> implements INBTSerializable<CompoundTag> {

    final ChunkPos pos;
    final int radius;

    public Map<ResourceKey<Level>, TardisLevelData.TardisCheckin> tardisCheckinList = new HashMap<>();
    private boolean dirty = false;


    public VortexPheonomena(VortexPhenomenaType<?> type, ChunkPos pos, int radius){
        super(type);
        this.pos = pos;
        this.radius = radius;
    }

    public ChunkPos getPos(){
        return this.pos;
    }

    public int getRadius(){
        return this.radius;
    }

    public boolean isIn(SpaceTimeCoord coordInFlight){
        BlockPos flightPos = coordInFlight.getPos();
        BlockPos vpPos = this.getPos().getMiddleBlockPosition(0);
        return flightPos.getX() > vpPos.getX() - getRadius() && flightPos.getX() < vpPos.getX() + getRadius() &&
                flightPos.getZ() > vpPos.getZ() - getRadius() && flightPos.getZ() < vpPos.getZ() + getRadius();
    }

    /**
     * Inheriting methods MUST call super!
     * @return
     */
    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putString("vp_type", VortexPhenomenaRegistry.REGISTRY.get().getKey(this.getType()).toString());

        return tag;
    }

    public abstract void onTardisEnter(ITardisLevel tardis);
    public abstract void onTardisLeave(ITardisLevel tardis);

    /**
     * Every flight tick this TARDIS is in this event
     * @param tardis
     */
    public void onTardisTick(ITardisLevel tardis){

    }

    /**
     * Tick stuff
     * @param gameTime - overworld game time
     * @return false if this should stop ticking
     */
    public boolean tick(long gameTime){
        //Fire onLeave for TARDISes that have not checked in in time
        tardisCheckinList.values().stream().filter(check -> check.expired(gameTime)).forEach(check -> {
            onTardisLeave(check.tardis());
        });
        //Remove TARDISes that have not checked in
        this.tardisCheckinList.values().removeIf(check -> check.expired(gameTime));
        //Return true if there are still TARDISes being affected by this event
        return this.tardisCheckinList.size() > 0;
    }

    public void checkin(ITardisLevel tardis, long overworldGameTime){

        if(!tardisCheckinList.containsKey(tardis.getLevel().dimension())){
            this.onTardisEnter(tardis);
        }
        this.onTardisTick(tardis);
        this.tardisCheckinList.put(tardis.getLevel().dimension(), new TardisLevelData.TardisCheckin(tardis, overworldGameTime));
    }

    public void onGenerated(ServerLevel level){}


    public static Optional<VortexPheonomena> of(ChunkPos pos, @Nullable CompoundTag tag){

        if(tag == null || !tag.contains("vp_type"))
            return Optional.empty();

        final VortexPhenomenaType<?> type = VortexPhenomenaRegistry.REGISTRY.get().getValue(new ResourceLocation(tag.getString("vp_type")));

        final VortexPheonomena vp = type.create(pos);
        vp.deserializeNBT(tag);
        return Optional.of(vp);
    }

    public boolean isDirty() {
        return this.dirty;
    }

    public void markDirty(){
        this.dirty = true;
    }
}
