package net.tardis.mod.misc.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.TickTask;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.chunks.IChunkCap;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.TardisUpdateMessage;
import net.tardis.mod.resource_listener.server.LandUnlockPredicatesReloader;
import net.tardis.mod.tags.BlockTags;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

/**
 * This is NOT Data related to the {@link net.tardis.mod.exterior.Exterior}!!!!
 * This is for doing things around the exterior
 */
public class ExteriorDataHandler implements INBTSerializable<CompoundTag> {

    final ITardisLevel tardis;
    boolean isInRift = false;
    Optional<Rift> currentRift = Optional.empty();
    Optional<ChunkPos> riftPos = Optional.empty();
    private boolean isNearAnotherTARDIS = false;

    boolean shouldLandInAir = false;
    boolean canLandUnderwater = false;
    boolean mustLandEnclosed = false;
    Optional<ResourceLocation> exteriorTexVariant = Optional.empty();

    public ExteriorDataHandler(ITardisLevel tardis){
        this.tardis = tardis;
    }

    public void tick(){

        if(!tardis.isInVortex() && tardis.getFuelHandler().shouldRefuel()){
            getFoundRift().ifPresent(this::soakPowerFromRift);
        }

    }

    public void findClosestRift(MinecraftServer server, SpaceTimeCoord location){
        ServerLevel level = server.getLevel(location.getLevel());
        this.setCurrentRift(null);

        //Spawn a thread to look for a rift, so we don't need to load chunks from another level in this tick cycle
        new Thread(() -> {

            double dist = Integer.MAX_VALUE;
            Rift foundRift = null;

            if(level != null){
                for(ChunkPos p : ChunkPos.rangeClosed(new ChunkPos(location.getPos()), 8).toList()){
                    LevelChunk chunk = level.getChunk(p.x, p.z);
                    if(chunk != null){
                        LazyOptional<IChunkCap> capHolder = chunk.getCapability(Capabilities.CHUNK);
                        if(capHolder.isPresent()){
                            IChunkCap cap = capHolder.orElseThrow(NullPointerException::new);
                            if(cap.getRift().isPresent()){
                                //If we are in this rift
                                double currentDist = location.getPos().distSqr(p.getWorldPosition());
                                if(currentDist < dist){
                                    dist = currentDist;
                                    foundRift = cap.getRift().get();
                                }
                            }
                        }
                    }
                }
            }
            if(foundRift != null){
                final Rift actualRift = foundRift;
                server.doRunTask(new TickTask(server.getTickCount(), () -> {
                    this.setCurrentRift(actualRift);
                }));
            }

        }).start();
    }

    public void soakPowerFromRift(Rift rift){

        //Min between TARDIS Banks space, and transfer rate
        float amountToTake = Math.min(tardis.getFuelHandler().getMaxArtron() - tardis.getFuelHandler().getStoredArtron(), 0.0266F);

        //Don't take if the rift only has one Artron, so it can be kept open
        if(rift.getStoredArtron() > 1){

            float amt = tardis.getFuelHandler().fillArtron(amountToTake, true);
            tardis.getFuelHandler().fillArtron(rift.takeArtron(amt, false), false);

        }
    }

    public void setCurrentRift(@Nullable Rift rift){
        this.currentRift = Helper.nullableToOptional(rift);
        this.riftPos = rift != null ? Optional.of(rift.getChunkPos()) : Optional.empty();
        if(!this.tardis.isClient())
            this.setInRift(this.currentRift.isPresent());
    }

    public void onLanded(Level level, BlockPos pos){

        if(!level.isClientSide()){
            findClosestRift(level.getServer(), tardis.getLocation());

            this.isNearAnotherTARDIS = BlockPos.findClosestMatch(pos, 16, 16, p -> !p.equals(pos) && level.getBlockState(p).is(BlockTags.EXTERIORS)).isPresent();

        }

        for(LandUnlockPredicatesReloader.LandUnlockPredicate pred : LandUnlockPredicatesReloader.INSTANCE.predicateMaps.values()){

            boolean matchesBiome = false;

            if(pred.biome().isPresent()){
                final Holder<Biome> currentBiome = level.getBiome(pos);
                for(TagKey<Biome> bTest : pred.biome().get()){
                    if(currentBiome.is(bTest)){
                        matchesBiome = true;//If any biome listed matches, pass
                    }
                }
            }
            else matchesBiome = true; // If no biome constraint, all biomes match

            //If not in a matching biome, skip this predicate
            if(!matchesBiome)
                continue;

            if(pred.structure().isPresent()){
                if(!pred.matchesAnyStructure(((ServerLevel)level).structureManager().getAllStructuresAt(pos).keySet(), level.registryAccess()))
                    continue;
            }
            this.tardis.getUnlockHandler().unlock(pred.unlockedItem());
        }

    }

    public void setInRift(boolean isInRift){
        this.isInRift = isInRift;
        this.update();
    }

    public boolean IsInRift(){
        return this.isInRift;
    }

    public void setCanLandUnderwater(boolean canLand){
        this.canLandUnderwater = canLand;
        this.update();
    }

    public void setCanLandInAir(boolean canLand){
        this.shouldLandInAir = canLand;
        this.update();
    }

    public boolean canLandInWater(){
        return this.canLandUnderwater;
    }

    public boolean canLandMidAir(){
        return this.shouldLandInAir;
    }

    /**
     * Always Empty on the client
     * @return The currently tracked, still valid, rift
     */
    public Optional<Rift> getFoundRift(){

        //If client, always empty
        if(this.tardis.isClient())
            return Optional.empty();

        //If this rift is not valid, or has no artron (closed)
        if(currentRift.isEmpty() && this.riftPos.isEmpty())
            return currentRift;

        //If in the vortex, we're obviously not landed near a rift
        if(tardis.isInVortex()){
            this.setCurrentRift(null);
            return this.currentRift;
        }

        if(currentRift.isEmpty()){
            ServerLevel serverWorld = tardis.getLevel().getServer().getLevel(tardis.getLocation().getLevel());
            if(serverWorld != null){
                Capabilities.getCap(Capabilities.CHUNK, serverWorld.getChunk(this.riftPos.get().x, this.riftPos.get().z)).ifPresent(chunk -> {
                    this.setCurrentRift(chunk.getRift().isPresent() ? chunk.getRift().get() : null);
                });
            }
        }
        if(currentRift.isEmpty())
            return Optional.empty();

        Rift rift = this.currentRift.get();
        if(!rift.isIn(tardis.getLocation().getPos()) || rift.getStoredArtron() <= 0){
            this.currentRift = Optional.empty();
            return this.currentRift;
        }
        return this.currentRift;
    }

    public void update(){
        this.tardis.update(TardisUpdateMessage.EXTERIOR_EXTRA);
    }


    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putBoolean("in_rift", this.IsInRift());
        tag.putBoolean("land_underwater", this.canLandUnderwater);
        tag.putBoolean("land_air", this.shouldLandInAir);
        this.riftPos.ifPresent(pos -> tag.putLong("rift_pos", pos.toLong()));
        tag.putBoolean("is_near_tardis", this.isNearAnotherTARDIS);
        this.exteriorTexVariant.ifPresent(id -> tag.putString("ext_tex_var", id.toString()));
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.isInRift = tag.getBoolean("in_rift");
        this.canLandUnderwater = tag.getBoolean("land_underwater");
        this.shouldLandInAir = tag.getBoolean("land_air");
        this.riftPos = Helper.readOptionalNBT(tag, "rift_pos", (t, k) -> new ChunkPos(t.getLong(k)));
        this.isNearAnotherTARDIS = tag.getBoolean("is_near_tardis");
        this.exteriorTexVariant = Helper.readOptionalNBT(tag, "ext_tex_var", (t, n) -> new ResourceLocation(t.getString(n)));
    }

    public void setExteriorTexVariant(@Nullable ResourceLocation id){
        this.exteriorTexVariant = Helper.nullableToOptional(id);
        this.update();
    }

    public Optional<ResourceLocation> getExteriorTexVariant(){
        return this.exteriorTexVariant;
    }

    public boolean isNearAnotherTardis() {
        return this.isNearAnotherTARDIS;
    }
}
