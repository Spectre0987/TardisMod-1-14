package net.tardis.mod.misc.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TardisDimensionInfo;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FlightCourse implements INBTSerializable<ListTag> {

    public static final int DEFAULT_DIM_TRAVEL_TIME = 200;
    private List<FlightLeg> points = new ArrayList<>();
    private float totalFlightDistance = 0;

    public static int getTimeToEnterDimension(@Nullable TardisDimensionInfo info) {
        return info == null ? DEFAULT_DIM_TRAVEL_TIME : info.travelDistance();
    }

    public static int getTimeToEnterDimension(MinecraftServer server, ResourceKey<Level> destDim) {
        final ResourceKey<DimensionType> dimTypeId = server.getLevel(destDim).dimensionTypeId();

        ObjectHolder<Integer> result = new ObjectHolder<>(DEFAULT_DIM_TRAVEL_TIME);
        server.registryAccess().registryOrThrow(JsonRegistries.TARDIS_DIM_INFO).forEach(info -> {
            if(info.AppliesTo(dimTypeId)){
                result.set(getTimeToEnterDimension(info));
            }
        });
        return result.get();
    }

    public FlightCourse(FriendlyByteBuf buf){
        this.decode(buf);
    }

    public FlightCourse(){}

    public Optional<SpaceTimeCoord> getDestination(){
        if(points.size() > 0){
            return Optional.of(points.get(points.size() - 1).end);
        }
        return Optional.empty();
    }

    public List<FlightLeg> getLegs(){
        return this.points;
    }

    public SpaceTimeCoord getCurrentFlightLocation(float distFlew){

        Optional<FlightLeg> currLeg = this.getCurrentLeg(distFlew);
        if(currLeg.isPresent()){
            FlightLeg leg = currLeg.get();
            //Get the distance traveled on this leg
            float dist = distFlew - this.getDistanceOfPreviousLegs(leg);
            return new SpaceTimeCoord(leg.end.getLevel(), leg.getCurrentPos(dist));
        }

        return this.getLegs().get(this.getLegs().size() - 1).end;
    }

    public FlightCourse addPoint(ITardisLevel tardis, SpaceTimeCoord coord){

        int index = this.points.size() - 1;
        SpaceTimeCoord start = tardis.getLocation();
        if(index >= 0){ // If this already had a leg, use the end of that one
            start = this.points.get(index).end;
        }
        this.addFlightLeg(new FlightLeg(start, coord));

        return this;

    }

    public boolean isComplete(float distanceFlew){
        return this.getCurrentLeg(distanceFlew).isEmpty() || distanceFlew > this.totalFlightDistance;
    }

    public float getTotalFlightDistance(){
        return this.totalFlightDistance;
    }

    /**
     *
     * @param leg
     */
    public void addFlightLeg(FlightLeg leg){
        this.points.add(leg);
        this.totalFlightDistance += leg.getDistance();
    }

    public Optional<FlightLeg> getCurrentLeg(float distanceFlew){

        float dist = distanceFlew;
        for(FlightLeg point : this.points){
            //If the current point is greater than the distance we've flown on this leg, this is our current one
            if(point.getDistance() >= dist){
                return Optional.of(point);
            }
            //If it's not, then take away the distance of this leg so the next one can be calculated
            dist -= point.getDistance();
        }
        return Optional.empty(); //This is likely complete if this is empty
    }

    public float getDistanceOfPreviousLegs(FlightLeg leg){

        float dist = 0;
        for(FlightLeg l : this.points){
            if(leg == l){
                return dist;
            }
            dist += l.getDistance();
        }
        return 0;

    }

    public int getIndexOf(FlightLeg leg){
        int index = 0;
        for(FlightLeg l : points){
            if(l == leg){
                return index;
            }
            ++index;
        }
        return -1;
    }
    public void replace(int index, FlightLeg leg){
        this.points.set(index, leg);
        //If has a point after this one
        if(index + 1 < this.points.size()){
            final FlightLeg nextLeg = points.get(index + 1);
            points.set(index + 1, new FlightLeg(leg.end, nextLeg.end));
        }
    }

    public void encode(FriendlyByteBuf buf){
        buf.writeInt(this.points.size());
        buf.writeFloat(this.totalFlightDistance);
        for(FlightLeg leg : this.points){
            leg.encode(buf);
        }
    }
    public void decode(FriendlyByteBuf buf){
        this.points.clear();
        final int length = buf.readInt();
        buf.readFloat();
        this.totalFlightDistance = 0;
        for(int i = 0; i < length; ++i){
            this.addFlightLeg(new FlightLeg(buf));
        }
    }

    @Override
    public ListTag serializeNBT() {
        ListTag list = new ListTag();
        for(FlightLeg point : this.points){
            list.add(point.serializeNBT());
        }
        return list;
    }

    @Override
    public void deserializeNBT(ListTag listTag) {
        this.points.clear();
        for(int i = 0; i < listTag.size(); ++i){
            points.add(new FlightLeg(listTag.getCompound(i)));
        }
    }

    public static class FlightLeg{

        public final SpaceTimeCoord start;
        public final SpaceTimeCoord end;

        private float distanceToTravel = -1;

        public FlightLeg(SpaceTimeCoord start, SpaceTimeCoord end){
            this.start = start;
            this.end = end;
            this.calculateDistance();
        }

        private float calculateDistance(){
            return this.end.getPos().distManhattan(this.start.getPos());
        }

        public float getDistance(){
            if(this.distanceToTravel == -1){ //Caching since the coords are final
                this.distanceToTravel = calculateDistance();
            }
            return this.distanceToTravel;
        }

        public BlockPos getCurrentPos(float distanceToTravel){

            if(getDistance() <= 0)
                return this.start.getPos();

            BlockPos dir = new BlockPos(
                    this.end.getPos().getX() - this.start.getPos().getX(),
                    this.end.getPos().getY() - this.start.getPos().getY(),
                    this.end.getPos().getZ() - this.start.getPos().getZ()
            );
            float scale = distanceToTravel / this.getDistance();
            return this.start.getPos().offset(Helper.scalePos(dir, scale));
        }

        public FlightLeg(CompoundTag tag){
            this(SpaceTimeCoord.of(tag.getCompound("start")),
                    SpaceTimeCoord.of(tag.getCompound("end")));
        }

        public FlightLeg(FriendlyByteBuf buf){
            this(SpaceTimeCoord.decode(buf), SpaceTimeCoord.decode(buf));
        }

        public void encode(FriendlyByteBuf buf){
            this.start.encode(buf);
            this.end.encode(buf);
        }

        public CompoundTag serializeNBT() {
            CompoundTag tag = new CompoundTag();
            tag.put("start", this.start.serializeNBT());
            tag.put("end", this.end.serializeNBT());
            return tag;
        }
    }

}
