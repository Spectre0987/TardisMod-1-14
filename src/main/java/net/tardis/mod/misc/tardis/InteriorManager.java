package net.tardis.mod.misc.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSRoom;
import net.tardis.mod.ars.RoomEntry;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.config.Config;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.IDoor;
import net.tardis.mod.misc.tardis.world_structures.AtriumWorldStructure;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructrureType;
import net.tardis.mod.misc.tardis.world_structures.TardisWorldStructure;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.PlayMovingSoundMessage;
import net.tardis.mod.network.packets.TardisUpdateMessage;
import net.tardis.mod.network.packets.UpdateDoorStateMessage;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.DematAnimationRegistry;
import net.tardis.mod.registry.JsonRegistries;
import net.tardis.mod.registry.TardisWorldStructureRegistry;
import net.tardis.mod.sound.MovingSound;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.sound.SoundScheme;
import net.tardis.mod.world.data.ARSRoomLevelData;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;

public class InteriorManager implements INBTSerializable<CompoundTag>, IDoor {

    private final ITardisLevel tardis;

    private HashMap<UUID, InteriorDoorData> interiorDoorPositions = new HashMap<>();
    private Optional<UUID> mainDoorId = Optional.empty();
    private SoundScheme scheme;
    private DematAnimationRegistry.DematAnimationType dematAnimation;
    private DoorHandler doorHandler;
    private int matterBuffer = 0;
    private boolean hasNeverFlown = true;
    private BlockPos mainAtriumPos = BlockPos.ZERO;
    private ItemStackHandler temporalScoopInventory = new ItemStackHandler(54); //Same as double chest
    private final HashMap<BlockPos, TardisWorldStructure> structure = new HashMap<>();
    private final EnergyStorage feBuffer = new EnergyStorage(20000, 128, 128);
    private final LazyOptional<EnergyStorage> feBufferHolder = LazyOptional.of(() -> feBuffer);

    private final FluidTank tankBuffer = new FluidTank(128 * 1000);
    private final LazyOptional<IFluidTank> fluidBufferHolder = LazyOptional.of(() -> this.tankBuffer);
    private long playCreakSoundNext = 0;
    private final HashMap<UUID, TubeEntry> teleportTubes = new HashMap<>();

    /**
     * Light percentage, 1.0 = 100% bright
     * Controls TARDIS Light sources, like roundels
     */
    private float tardisLightLevel = 0.0F;


    public InteriorManager(ITardisLevel level){
        this.tardis = level;
        this.scheme = level.getLevel().registryAccess().registry(JsonRegistries.SOUND_SCHEMES_REGISTRY).get().get(Helper.createRL("default"));
        createDoorHandlerFromExterior(level.getExterior());
    }


    public void tick() {
        if(!this.tardis.isClient()){

            //Play flight sound
            if(this.tardis.isInVortex()) {
                SoundScheme scheme = this.getSoundScheme();
                //Play Flight Loop
                if (!this.tardis.isTakingOffOrLanding() && this.tardis.ticksInVortex() % scheme.getFly().time() == 0) {
                    this.tardis.playSoundToAll(scheme.getFly().event(), SoundSource.AMBIENT, 1.0F);
                }
            }

            //Save players from void
            this.tardis.getLevel().players().stream().filter(p -> p.getY() < tardis.getLevel().getMinBuildHeight()).forEach(p -> {
                Vec3 pos = this.getMainInteriorDoor().placeAtMe(p, this.tardis);
                p.teleportTo(pos.x, pos.y, pos.z);
                p.fallDistance = 0.0F;
            });

            //Charge screwdriver port
            ItemStack portItem = tardis.getControlDataOrCreate(ControlRegistry.SONIC_PORT.get()).get();
            LazyOptional<IEnergyStorage> powerCap = Capabilities.getCap(ForgeCapabilities.ENERGY, portItem);
            if(powerCap.isPresent()){
                IEnergyStorage battery = powerCap.orElseThrow(NullPointerException::new);
                if(battery.canReceive() && battery.receiveEnergy(1, false) > 0)
                    tardis.getControlDataOrCreate(ControlRegistry.SONIC_PORT.get()).set(portItem);
            }

            //Add power to FE buffer
            this.feBuffer.receiveEnergy(Config.Server.FE_BUFFER_PER_TICK.get(), false);

            //creaking sounds
            if(tardis.getLevel().getGameTime() > this.playCreakSoundNext){
                //set the next creak sound to play anywhere from 30 seconds to a 5 minutes
                this.playCreakSoundNext = tardis.getLevel().getGameTime() + (30 * 20) + tardis.getLevel().random.nextInt(5 * 60 * 20);

                tardis.getLevel().players().stream()
                        .filter(p -> {
                            //If not in a room, or the room we are in has a parent (eg, is broken)
                            Optional<RoomEntry> roomHolder = ARSRoomLevelData.getData((ServerLevel) tardis.getLevel()).getRoomFor(p.getOnPos());
                            if(roomHolder.isEmpty())
                                return true;
                            Optional<ARSRoom> room = roomHolder.get().getRoom(tardis.getLevel().registryAccess());
                            if(room.isEmpty())
                                return true;
                            return room.get().getParent().isPresent();
                        })
                        .map(p -> (ServerPlayer)p)
                        .forEach(p -> {
                            Network.sendTo(p, new PlayMovingSoundMessage(SoundRegistry.TARDIS_CREAK.get(), SoundSource.AMBIENT, p.getId(), 1.0F, false));
                        });

            }

        }
        for(TardisWorldStructure s : this.structure.values()){
            s.tick();
        }

    }

    public InteriorDoorData getMainInteriorDoor(){

        //Return main door if one is set
        if(this.mainDoorId.isPresent()){
            Tardis.LOGGER.log(Level.DEBUG, "Found Main door with id " + this.mainDoorId.get());
            InteriorDoorData data = this.interiorDoorPositions.get(this.mainDoorId.get());
            if(data != null)
                return data;
        }
        Tardis.LOGGER.log(Level.DEBUG, "No Main door found! finding new one");
        //Search for a new door if there's no set main
        for(InteriorDoorData data : this.interiorDoorPositions.values()){
            if(data != null && data.isValidDoor(this.tardis))
                return data;
        }
        return new InteriorDoorData(new BlockPos(0, 200, 0));
    }

    public void addInteriorDoor(UUID id, InteriorDoorData pos){
        this.interiorDoorPositions.put(id, pos);
    }

    public HashMap<UUID, InteriorDoorData> getDoors(){
        return this.interiorDoorPositions;
    }

    public void setMainDoor(@Nullable final UUID id){
        if(id == null) {
            this.mainDoorId = Optional.empty();
        }
        else this.mainDoorId = Optional.of(id);

    }

    public Optional<UUID> getMainDoorID(){
        return this.mainDoorId;
    }

    public SoundScheme getSoundScheme(){
        if(this.scheme == null){
            return this.scheme = tardis.getLevel().registryAccess().registry(JsonRegistries.SOUND_SCHEMES_REGISTRY).get().get(Helper.createRL("default"));
        }
        return this.scheme;
    }

    public void setDematAnimation(DematAnimationRegistry.DematAnimationType type){
        this.dematAnimation = type;
    }

    public DematAnimationRegistry.DematAnimationType getDematAnimation(){
        if(this.dematAnimation != null){
            return this.dematAnimation;
        }
        return DematAnimationRegistry.CLASSIC.get();
    }

    /** Plays a sound at all consoles
     *
     * @param event - SoundEvent to play
     */
    public void playConsoleSound(Function<ConsoleTile, SoundEvent> event){
        if(!this.tardis.isClient()){
            for(Player player : tardis.getLevel().players()){
                final ChunkPos startPos = new ChunkPos(player.getOnPos());
                for(int x = -2; x < 2; ++x){
                    for(int z = -2; z < 2; ++z){
                        tardis.getLevel().getChunk(startPos.x + x, startPos.z + z)
                                .getBlockEntities().forEach((pos, tile) -> {
                                    if(tile instanceof ConsoleTile console){
                                        tardis.getLevel().playSound(null, pos, event.apply(console), SoundSource.BLOCKS, 1.0F, 1.0F);
                                    }
                                });
                    }
                }
            }
        }
    }

    public void playControlSound(ControlData<?> data){

        if(this.tardis.isClient())
            return;

        playConsoleSound(console -> {
            //if this console has an override
            ResourceLocation consoleKey = ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(console.getType());
            if(ControlPositionDataRegistry.POSITION_DATAS.containsKey(consoleKey)){
                ControlPositionData posData = ControlPositionDataRegistry.POSITION_DATAS.get(consoleKey).get(data.getType());
                if(posData.successSound().isPresent())
                    return posData.successSound().get().get();
                /*
                for(ControlPositionData posData : positions){
                    if(posData.getControlType() == data.getType() && posData.getSuccessSound().isPresent()){
                        return posData.getSuccessSound().get();
                    }
                }

                 */
            }
            return (SoundEvent) data.getType().getSound(ControlType.ControlSoundAction.SUCCESS)
                        .orElse(SoundRegistry.AXIS_CONTROL_SOUND.get());
        });
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();

        ListTag doorList = new ListTag();
        for(Map.Entry<UUID, InteriorDoorData> data : this.interiorDoorPositions.entrySet()){
            CompoundTag dataTag = data.getValue().serializeNBT();
            dataTag.putString("interior_data_id", data.getKey().toString());
            doorList.add(dataTag);
        }
        tag.put("doors", doorList);
        Helper.writeRegistryToNBT(tag, this.tardis.getLevel().registryAccess().registryOrThrow(JsonRegistries.SOUND_SCHEMES_REGISTRY), this.getSoundScheme(), "sound_scheme");
        this.mainDoorId.ifPresent(id -> tag.putString("main_door", id.toString()));
        Helper.writeRegistryToNBT(tag, DematAnimationRegistry.REGISTRY.get(), this.getDematAnimation(), "demat_type");
        tag.putInt("matter_buffer", this.matterBuffer);
        tag.putBoolean("has_never_flown", this.hasNeverFlown);
        tag.putFloat("tardis_light_level", this.tardisLightLevel);
        tag.put("door_data", this.doorHandler.serializeNBT());
        this.doorHandler.setValidDoorStates(tardis.getExterior().getType().getDoorStates());
        tag.put("temporal_scoop", this.temporalScoopInventory.serializeNBT());
        Helper.writeListNBT(tag, "world_structures", this.structure.values(), (item, list) -> {
            CompoundTag s = item.serializeNBT();
            Helper.writeRegistryToNBT(s, TardisWorldStructureRegistry.REGISTRY.get(), item.getType(), "type");
            s.putLong("position", item.getPosition().asLong());
            list.add(s);
        });
        tag.put("fe_buffer", this.feBuffer.serializeNBT());
        tag.put("fluid_buffer", this.tankBuffer.writeToNBT(new CompoundTag()));
        tag.putLong("main_atrium_pos", this.mainAtriumPos.asLong());

        final ListTag tubeList = new ListTag();
        for(TubeEntry entry : this.teleportTubes.values()){
            tubeList.add(entry.serializeNBT());
        }
        tag.put("teleport_tubes", tubeList);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.interiorDoorPositions.clear();
        this.structure.clear();
        this.teleportTubes.clear();

        ListTag doorTag = tag.getList("doors", Tag.TAG_COMPOUND);
        for(int i = 0; i < doorTag.size(); ++i){
            CompoundTag compoundTag = doorTag.getCompound(i);
            this.interiorDoorPositions.put(UUID.fromString(compoundTag.getString("interior_data_id")), new InteriorDoorData(compoundTag));
        }
        if(tag.contains("main_door"))
            this.mainDoorId = Optional.of(UUID.fromString(tag.getString("main_door")));
        Helper.readRegistryFromString(tag, DematAnimationRegistry.REGISTRY.get(), "demat_type").ifPresent(this::setDematAnimation);
        this.doorHandler.deserializeNBT(tag.getCompound("door_data"));
        this.matterBuffer = tag.getInt("matter_buffer");
        this.hasNeverFlown = tag.getBoolean("has_never_flown");
        this.tardisLightLevel = tag.getFloat("tardis_light_level");
        Helper.readRegistryFromString(tag, tardis.getLevel().registryAccess().registryOrThrow(JsonRegistries.SOUND_SCHEMES_REGISTRY), "sound_scheme")
                .ifPresent(this::setSoundScheme);
        this.temporalScoopInventory.deserializeNBT(tag.getCompound("temporal_scoop"));
        Helper.<TardisWorldStructure, CompoundTag>readListNBT(tag, "world_structures", Tag.TAG_COMPOUND, structureTag -> {
            final TardisWorldStructrureType<?> type = Helper.readRegistryFromString(structureTag, TardisWorldStructureRegistry.REGISTRY.get(), "type").get();
            final BlockPos pos = BlockPos.of(structureTag.getLong("position"));
            TardisWorldStructure struct = type.create(tardis, pos);
            struct.deserializeNBT(structureTag);
            return struct;
        }).forEach(s -> this.addWorldStructre(s.getPosition(), s));
        this.feBuffer.deserializeNBT(tag.get("fe_buffer"));
        this.tankBuffer.readFromNBT(tag.getCompound("fluid_buffer"));
        if(tag.contains("main_atrium_pos"))
            this.mainAtriumPos = BlockPos.of(tag.getLong("main_atrium_pos"));

        if(tag.contains("teleport_tubes")){
            final ListTag tubeList = tag.getList("teleport_tubes", Tag.TAG_COMPOUND);
            for(int i = 0; i < tubeList.size(); ++i){
                final TubeEntry entry = new TubeEntry(tubeList.getCompound(i));
                this.teleportTubes.put(entry.id, entry);
            }
        }
    }

    public void removeDoor(UUID id) {
        this.interiorDoorPositions.remove(id);
    }

    public void createDoorHandlerFromExterior(Exterior ext){
        this.doorHandler = new DoorHandler(true, ext.getType().getDoorStates()).makeCanBeUnlockedWithHand();
        this.doorHandler.setPacketSender(() -> {
            if(!tardis.isClient())
                Network.sendPacketToDimension(this.tardis.getLevel().dimension(), new UpdateDoorStateMessage(this.tardis.getLevel().dimension(), this.doorHandler));
        });
        this.doorHandler.linkedDoor = () -> {
            if(!this.tardis.getLevel().isClientSide()){
                this.tardis.getExterior().setDoorHandler(this.getDoorHandler());
            }
        };
    }

    @Override
    public DoorHandler getDoorHandler() {
        return this.doorHandler;
    }

    /**
     *
     * @return How many blocks can be pulled from ARS Egg
     */
    public int getMatterBuffer() {
        return this.matterBuffer;
    }

    /**
     *
     * @return 0 - 1 light percentage
     */
    public float getTardisLightLevel(){
        return this.tardisLightLevel;
    }

    public void updateLighting(){

        //Set all loaded chunks to the interior's light level
        if(!tardis.isClient()){
            tardis.getInteriorManager().update();
            ((ServerLevel)this.tardis.getLevel()).getChunkSource().chunkMap.getChunks().forEach(chunk -> {
                Capabilities.getCap(Capabilities.TARDIS_CHUNK, chunk.getFullChunk()).ifPresent(cap -> {
                    cap.updateLighting();
                });
            });
        }
    }

    /**
     * For the ars egg, essentially how many blocks you can pull from it
     * @param mod
     */
    public void modMatterBuffer(int mod){
        this.matterBuffer = Mth.clamp(this.matterBuffer + mod, 0, Integer.MAX_VALUE);
    }

    /**
     * Should only be used by packets, does not send an update
     */
    public void setMatterBufferDirect(int buffer){
        this.matterBuffer = buffer;
    }

    public boolean hasNeverFlown() {
        return this.hasNeverFlown;
    }

    public void setHasFlown(){
        if(hasNeverFlown){
            this.hasNeverFlown = false;
            this.update();
        }
    }

    public void update(){
        this.tardis.update(TardisUpdateMessage.INTERIOR);
    }

    public void setLightLevel(float percent) {
        this.tardisLightLevel = Mth.clamp(percent, 0.0F, 1.0F);
        this.updateLighting();
    }

    public void setSoundScheme(SoundScheme scheme) {
        this.scheme = scheme;
    }

    public ItemStackHandler getTemporalScoopInventory(){
        return this.temporalScoopInventory;
    }

    public void addWorldStructre(BlockPos pos, TardisWorldStructure struct){
        this.structure.put(pos, struct);
    }

    public void addWorldStructre(BlockPos pos, TardisWorldStructrureType<?> structType){
        this.structure.put(pos, structType.create(this.tardis, pos));
    }

    public void removeWorldStructure(BlockPos pos){
        if(this.structure.containsKey(pos)){
            this.structure.remove(pos);
        }
    }

    public Optional<TardisWorldStructure> getWorldStructure(BlockPos pos){
        if(this.structure.containsKey(pos)){
            //Remove invalid structures
            if(!this.structure.get(pos).isStillValid()){
                this.structure.remove(pos);
                return Optional.empty();
            }
            return Optional.of(this.structure.get(pos));
        }
        return Optional.empty();
    }

    public <T extends TardisWorldStructure> Optional<T> getWorldStructure(BlockPos pos, TardisWorldStructrureType<T> type){
        Optional<TardisWorldStructure> struct = getWorldStructure(pos);
        if(struct.isPresent() && struct.get().getType() == type){
            return Optional.of((T)struct.get());
        }
        return Optional.empty();
    }

    public <T extends TardisWorldStructure> List<T> getAllOfType(TardisWorldStructrureType<T> type){
        final List<T> list = new ArrayList<>();
        //Remove invalid structures
        this.structure.keySet().stream().filter(pos -> !this.structure.get(pos).isStillValid()).forEach(structure::remove);
        this.structure.values().stream().filter(w -> w.getType() == type).forEach(t -> list.add((T)t));
        return list;
    }

    public List<TardisWorldStructure> getAllWorldStructures(){
        return new ArrayList<>(this.structure.values());
    }

    public LazyOptional<EnergyStorage> getFEBuffer(){
        return this.feBufferHolder;
    }

    public LazyOptional<IFluidTank> getFluidBuffer() {
        return this.fluidBufferHolder;
    }

    public Optional<AtriumWorldStructure> getMainAtrium(){
        if(!BlockPos.ZERO.equals(this.mainAtriumPos)){
            return this.getWorldStructure(this.mainAtriumPos, TardisWorldStructureRegistry.ATRIUM.get());
        }
        return Optional.empty();
    }

    public void setMainAtrium(BlockPos pos) {
        this.mainAtriumPos = pos;
    }

    public boolean addTubeEndpoint(UUID id, BlockPos pos){
        return this.teleportTubes.computeIfAbsent(id, TubeEntry::new)
                .addPosition(pos);
    }

    public Optional<TubeEntry> getTubeEntry(UUID id){
        return this.teleportTubes.containsKey(id) ? Optional.of(this.teleportTubes.get(id)) : Optional.empty();
    }

    public void removeTubeEntry(UUID id, BlockPos pos) {

        if(teleportTubes.containsKey(id)){
            teleportTubes.get(id).remove(pos);
            if(teleportTubes.get(id).isEmpty())
                teleportTubes.remove(id);
        }

    }
}
