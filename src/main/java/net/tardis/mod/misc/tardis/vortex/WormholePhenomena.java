package net.tardis.mod.misc.tardis.vortex;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.border.WorldBorder;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.FlightCourse;
import net.tardis.mod.registry.VortexPhenomenaRegistry;
import net.tardis.mod.world.data.TardisLevelData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WormholePhenomena extends VortexPheonomena{

    Optional<ChunkPos> otherPosition = Optional.empty();
    final List<ResourceKey<Level>> recievedTardises = new ArrayList<>();

    public WormholePhenomena(VortexPhenomenaType<?> type, ChunkPos pos) {
        super(type, pos, 5);
    }

    @Override
    public void onTardisEnter(ITardisLevel tardis) {
        if(!this.recievedTardises.contains(tardis.getId())){
            teleportTardis(tardis);
        }
    }

    @Override
    public void onTardisLeave(ITardisLevel tardis) {
        this.recievedTardises.remove(tardis.getId());
        this.markDirty();
    }

    public void teleportTardis(ITardisLevel tardis){
        this.otherPosition.ifPresent(pos -> {
            TardisLevelData.get((ServerLevel) tardis.getLevel()).getOrGeneratePhenomena(pos, false).ifPresent(other -> {
                if(other instanceof WormholePhenomena worm){
                    worm.addRecievedTardis(tardis.getId());
                    final SpaceTimeCoord wormCoord = new SpaceTimeCoord(tardis.getLocation().getLevel(), worm.getPos().getWorldPosition());
                    tardis.setLocation(wormCoord);
                    final FlightCourse course = new FlightCourse();
                    course.addFlightLeg(new FlightCourse.FlightLeg(wormCoord, tardis.getDestination()));
                    tardis.setFlightCourse(course);
                }
            });
        });
    }

    /**
     * Add this to the list so we don't teleport back and forth forever
     */
    public void addRecievedTardis(ResourceKey<Level> tardisKey){
        this.recievedTardises.add(tardisKey);
        this.markDirty();
    }

    public void setOtherPosition(ChunkPos pos){
        this.otherPosition = Optional.of(pos);
        this.markDirty();
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.recievedTardises.clear();

        this.otherPosition = Helper.readOptionalNBT(tag, "other_pos", (t, k) -> new ChunkPos(t.getLong(k)));
        this.recievedTardises.addAll(Helper.<ResourceKey<Level>, StringTag>readListNBT(tag, "recieved_tardises", StringTag.TAG_STRING, keyTag ->
                ResourceKey.create(Registries.DIMENSION, new ResourceLocation(keyTag.getAsString()))
        ));
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = super.serializeNBT();
        this.otherPosition.ifPresent(c -> tag.putLong("other_pos", c.toLong()));
        Helper.writeListNBT(tag, "recieved_tardises", this.recievedTardises, (k, t) -> t.add(StringTag.valueOf(k.location().toString())));
        return tag;
    }

    @Override
    public void onGenerated(ServerLevel level) {
        super.onGenerated(level);

        //Find an unoccupied chunk position
        ChunkPos pos = null;
        while(pos == null){
            //make sure there is not an existing VortexPhenomena
            final ChunkPos testPos = new ChunkPos(getRandomPos(level, this.getPos().getWorldPosition(), 0));
            if(TardisLevelData.get(level).getOrGeneratePhenomena(testPos, false).isEmpty()){
                pos = testPos;
            }
        }

        //Make a linked wormhole

        this.setOtherPosition(pos);
        WormholePhenomena vp = VortexPhenomenaRegistry.WORMHOLE.get().create(pos);
        vp.setOtherPosition(this.pos);
        TardisLevelData.get(level).addPhenomena(pos, Optional.of(vp));

    }

    public BlockPos getRandomPos(Level level, BlockPos start, int y){
        final int scale = 10000;
        return new BlockPos(
                start.getX() - scale + level.getRandom().nextInt(scale * 2),
                y,
                start.getZ() - scale + level.getRandom().nextInt(scale * 2)
        );
    }
}
