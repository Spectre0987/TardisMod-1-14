package net.tardis.mod.misc.tardis;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.tardis.mod.misc.SpaceTimeCoord;

public record ArtronTrail(ResourceKey<Level> tardis, long timeCreated, SpaceTimeCoord destination) {

}
