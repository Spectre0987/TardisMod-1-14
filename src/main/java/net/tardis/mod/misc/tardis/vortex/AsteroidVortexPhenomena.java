package net.tardis.mod.misc.tardis.vortex;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.UpgradeRegistry;

public class AsteroidVortexPhenomena extends VortexPheonomena{

    public static ResourceLocation LOOT = Helper.createRL("vortex_phenomena/astroid");

    public AsteroidVortexPhenomena(VortexPhenomenaType<?> type, ChunkPos pos) {
        super(type, pos, 25);
    }

    @Override
    public void onTardisEnter(ITardisLevel tardis) {

    }

    @Override
    public void onTardisLeave(ITardisLevel tardis) {

    }

    @Override
    public void onTardisTick(ITardisLevel tardis) {
        super.onTardisTick(tardis);
        if(tardis.ticksInVortex() % 200 == 0 && UpgradeRegistry.TARDIS_LASER_MINER.get().canBeUsed(tardis)){
            if(tardis.getLevel() instanceof ServerLevel server){
                ObjectArrayList<ItemStack> loot = tardis.getLevel().getServer().getLootTables().get(LOOT).getRandomItems(new LootContext.Builder(server).create(LootContextParamSets.EMPTY));

                for(ItemStack stack : loot){
                    addItem(tardis, stack);
                }

            }
        }
    }

    public void addItem(ITardisLevel tardis, ItemStack add){
        final ItemStackHandler inv = tardis.getInteriorManager().getTemporalScoopInventory();
        for(int i = 0; i < inv.getSlots(); ++i){
            if(add.isEmpty())
                return;
            add = inv.insertItem(i, add, false);
        }
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
