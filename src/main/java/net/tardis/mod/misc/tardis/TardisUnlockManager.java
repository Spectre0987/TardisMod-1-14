package net.tardis.mod.misc.tardis;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.UnlockHandler;

import java.util.Optional;

public class TardisUnlockManager extends UnlockHandler {

    final ITardisLevel tardis;

    public TardisUnlockManager(ITardisLevel tardis){
        this.tardis = tardis;
    }

    @Override
    public void notifyUnlock(ResourceKey<?> unlock, boolean successful) {
        Component itemName = getItemName(unlock);
        if(!this.tardis.isClient()){
            for(ServerPlayer player : ((ServerLevel)tardis.getLevel()).players()){
                player.displayClientMessage(getText(successful, itemName), true);
            }
        }
    }

    @Override
    public Component getItemName(ResourceKey<?> key) {

        final ResourceKey<? extends Registry<?>> regKey = ResourceKey.createRegistryKey(key.registry());
        return Component.translatable(Constants.Translation.makeGenericTranslation(regKey, key.location()));
    }
}
