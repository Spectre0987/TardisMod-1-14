package net.tardis.mod.misc.tardis.montior;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public abstract class OpenGuiMonitorFunction extends BasicMonitorFunction{

    @Override
    public void doServerAction(ITardisLevel tardis, Player player) {
        Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(this.getGui(tardis, player)));
    }

    public abstract GuiData getGui(ITardisLevel tardis, Player player);

    @Override
    public boolean doClientAction(ITardisLevel tardis, Player player) {
        return false;
    }
}
