package net.tardis.mod.misc.tardis.world_structures;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.TypeHolder;

/**
 * A world-built structure inside the TARDIS that can affect it in some way
 */
public abstract class TardisWorldStructure extends TypeHolder<TardisWorldStructrureType<?>>implements INBTSerializable<CompoundTag> {

    final ITardisLevel tardis;
    final BlockPos position;

    public TardisWorldStructure(TardisWorldStructrureType<?> type, ITardisLevel tardis, BlockPos pos){
        super(type);
        this.tardis = tardis;
        this.position = pos;
    }

    public void tick(){}

    public abstract boolean isStillValid();

    public BlockPos getPosition(){
        return this.position;
    }

}
