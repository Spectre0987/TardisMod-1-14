package net.tardis.mod.misc.tardis.world_structures;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;

import java.util.HashMap;
import java.util.Map;

public class WaypointTardisStructure extends TardisWorldStructure{

    final int maxWaypoints;
    private final HashMap<String, SpaceTimeCoord> coords = new HashMap<>();

    public WaypointTardisStructure(TardisWorldStructrureType<?> type, ITardisLevel tardis, BlockPos pos) {
        this(type, tardis, pos, 5);
    }

    @Override
    public boolean isStillValid() {
        if(tardis.getLevel().getBlockState(this.getPosition()).getBlock() != BlockRegistry.WAYPOINT_BANKS.get()){
            return false;
        }
        return true;
    }

    public WaypointTardisStructure(TardisWorldStructrureType<?> type, ITardisLevel tardis, BlockPos pos, int waypoints) {
        super(type, tardis, pos);
        this.maxWaypoints = waypoints;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();

        final ListTag waypoints = new ListTag();
        for(Map.Entry<String, SpaceTimeCoord> coord : this.coords.entrySet()){
            final CompoundTag coordTag = coord.getValue().serializeNBT();
            coordTag.putString("name", coord.getKey());
            waypoints.add(coordTag);
        }
        tag.put("waypoints", waypoints);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.coords.clear();

        final ListTag waypointsTag = tag.getList("waypoints", ListTag.TAG_COMPOUND);
        for(int i = 0; i < waypointsTag.size(); ++i){
            final CompoundTag coordTag = waypointsTag.getCompound(i);
            this.coords.put(coordTag.getString("name"), SpaceTimeCoord.of(coordTag));
        }
    }

    public int getMaxWaypoints(){
        return this.maxWaypoints;
    }

    public boolean addWayPoint(String name, SpaceTimeCoord coord){
        if(this.coords.size() < maxWaypoints - 1){
            this.coords.put(name, coord);
            return true;
        }
        return false;
    }

    public HashMap<String, SpaceTimeCoord> getWaypoints(){
        return this.coords;
    }

    public void delete(String name) {
        if(this.coords.containsKey(name)){
            this.coords.remove(name);
        }
    }
}
