package net.tardis.mod.misc.tardis;

import net.tardis.mod.cap.level.ITardisLevel;

public interface IFlightDurationEffect {

    void onFlightTick(ITardisLevel tardis);
    void onTardisLand(ITardisLevel tardis);

}
