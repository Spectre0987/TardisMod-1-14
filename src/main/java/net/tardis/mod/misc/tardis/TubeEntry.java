package net.tardis.mod.misc.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.helpers.Helper;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.UUID;

public class TubeEntry implements INBTSerializable<CompoundTag> {

    final UUID id;
    private BlockPos firstPos;
    private BlockPos secondPos;


    public TubeEntry(UUID id){
        this.id = id;
    }

    public TubeEntry(){
        this(UUID.randomUUID());
    }

    public TubeEntry(CompoundTag tag){
        this(UUID.fromString(tag.getString("id")));
        this.deserializeNBT(tag);
    }

    public Optional<BlockPos> getFirstPosition(){
        return Helper.nullableToOptional(this.firstPos);
    }

    public Optional<BlockPos> getSecondPos(){
        return Helper.nullableToOptional(this.secondPos);
    }

    public void setFirstPos(@Nullable BlockPos pos){
        this.firstPos = pos;
    }

    public void setSecondPos(@Nullable BlockPos secondPos){
        this.secondPos = secondPos;
    }

    /**
     * Tries to add a position to either the first or second entries.
     * @return True if position was added
     */
    public boolean addPosition(BlockPos pos){
        if(firstPos == null){
            setFirstPos(pos);
            return true;
        }
        if(secondPos == null){
            this.setSecondPos(pos);
            return true;
        }
        return false;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putString("id", this.id.toString());
        if(this.firstPos != null){
            tag.putLong("first", this.firstPos.asLong());
        }
        if(this.secondPos != null){
            tag.putLong("second", this.secondPos.asLong());
        }
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        if(tag.contains("first")){
            this.firstPos = BlockPos.of(tag.getLong("first"));
        }
        if(tag.contains("second")){
            this.secondPos = BlockPos.of(tag.getLong("second"));
        }
    }

    public void remove(BlockPos pos) {
        if(firstPos != null && pos.equals(firstPos)){
            this.firstPos = null;
            return;
        }
        if(secondPos != null && pos.equals(secondPos)){
            this.secondPos = null;
        }
    }

    public boolean isEmpty(){
        return this.firstPos == null && this.secondPos == null;
    }
}
