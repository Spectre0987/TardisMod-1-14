package net.tardis.mod.misc.tardis.vortex;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;

public class AncientDebrisVortexPhenomena extends VortexPheonomena{

    public static ResourceLocation LOOT = Helper.createRL("vortex_phenomena/ancient_debris");

    public AncientDebrisVortexPhenomena(VortexPhenomenaType<?> type, ChunkPos pos) {
        super(type, pos, 50);
    }

    @Override
    public void onTardisEnter(ITardisLevel tardis) {

    }

    @Override
    public void onTardisLeave(ITardisLevel tardis) {

    }

    @Override
    public void onTardisTick(ITardisLevel tardis) {
        super.onTardisTick(tardis);
        if(tardis.ticksInVortex() % 200 == 0){
            if(tardis.getLevel() instanceof ServerLevel serverLevel){
                final ObjectArrayList<ItemStack> loot = serverLevel.getServer().getLootTables().get(LOOT).getRandomItems(new LootContext.Builder(serverLevel).create(LootContextParamSets.EMPTY));

                final ItemStackHandler inv = tardis.getInteriorManager().getTemporalScoopInventory();
                for(ItemStack l : loot){
                    addItemToTemporalScoop(inv, l);
                }

            }
        }
    }

    public void addItemToTemporalScoop(ItemStackHandler inv, ItemStack add){
        for(int i = 0; i < inv.getSlots(); ++i){
            add = inv.insertItem(i, add, false);
            if(add.isEmpty())
                return;
        }
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
