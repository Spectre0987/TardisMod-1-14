package net.tardis.mod.misc.tardis.montior.upgrades;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.misc.tardis.montior.OpenGuiMonitorFunction;
import net.tardis.mod.registry.UpgradeRegistry;

public class AtriumMonitorFunction extends OpenGuiMonitorFunction {
    @Override
    public GuiData getGui(ITardisLevel tardis, Player player) {
        return GuiDatas.MON_ATRIUM.create().withTardis(tardis);
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return UpgradeRegistry.ATRIUM.get().canBeUsed(tardis);
    }
}
