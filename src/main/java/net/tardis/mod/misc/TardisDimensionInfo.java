package net.tardis.mod.misc;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;


public record TardisDimensionInfo(ResourceKey<DimensionType> typeKey, String customName, boolean canTravelTo, boolean mustLandInside, boolean mustBeUnlockedFirst, int travelDistance) {


    public boolean AppliesTo(MinecraftServer server, ResourceKey<Level> levelKey){
        ServerLevel level = server.getLevel(levelKey);
        if(level == null)
            return false;
        return AppliesTo(level.dimensionTypeId());
    }

    public boolean AppliesTo(ResourceKey<DimensionType> dimType){
        return this.typeKey.compareTo(dimType) == 0;
    }

    public static Codec<TardisDimensionInfo> CODEC = RecordCodecBuilder.create((instance) -> {
        return instance.group(
                ResourceKey.codec(Registries.DIMENSION_TYPE).fieldOf("dimension_type").forGetter(TardisDimensionInfo::typeKey),
                Codec.STRING.fieldOf("name").forGetter(TardisDimensionInfo::customName),
                Codec.BOOL.fieldOf("can_travel_to").forGetter(TardisDimensionInfo::canTravelTo),
                Codec.BOOL.fieldOf("must_land_inside").forGetter(TardisDimensionInfo::mustLandInside),
                Codec.BOOL.fieldOf("must_be_unlocked").forGetter(TardisDimensionInfo::mustBeUnlockedFirst),
                Codec.INT.fieldOf("travel_distance").forGetter(TardisDimensionInfo::travelDistance)
        ).apply(instance, TardisDimensionInfo::new);
    });
}
