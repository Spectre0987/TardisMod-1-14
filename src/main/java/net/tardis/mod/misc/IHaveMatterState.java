package net.tardis.mod.misc;

import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.registry.DematAnimationRegistry;

public interface IHaveMatterState {

    MatterStateHandler getMatterStateHandler();
}
