package net.tardis.mod.misc.landing;

import net.minecraft.core.BlockPos;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.ChunkLoader;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.registry.ControlRegistry;

import java.util.*;
import java.util.function.Consumer;

public class LandingHelper {


    public static Optional<BlockPos> findValidLandPos(Level level, BlockPos target, LandingContext context, LandingType type){
        if(context.canLandAt(level, target))
            return Optional.of(target); // Can land exactly where we want -- Perfect

        //If we can't get the exact Target, go hunting
        return (type == LandingType.UP ? getHigherSpot(level, target, context) : getLowerSpot(level, target, context))
                //If we can't find a spot that matches our landing preferences, try the opposite
                .or(() -> type == LandingType.UP ? getLowerSpot(level, target, context) : getHigherSpot(level, target, context));

    }

    public static Optional<BlockPos> canLandExact(Level level, BlockPos pos, LandingContext context){
        if(context.canLandAt(level, pos)){
            return Optional.of(pos);
        }
        return Optional.empty();
    }

    public static Optional<BlockPos> getLowerSpot(Level level, BlockPos start, LandingContext context){
        for(int y = start.getY(); y > level.dimensionType().minY(); --y){
            BlockPos pos = new BlockPos(start.getX(), y, start.getZ());
            if(context.canLandAt(level, pos))
                return Optional.of(pos);
        }
        return Optional.empty();
    }

    public static Optional<BlockPos> getHigherSpot(Level level, BlockPos start, LandingContext context){
        for(int y = start.getY(); y < level.dimensionType().height(); ++y){
            BlockPos pos = new BlockPos(start.getX(), y, start.getZ());
            if(context.canLandAt(level, pos))
                return Optional.of(pos);
        }
        return Optional.empty();
    }

    public static class LandingSystemThread extends Thread{


        final LandingSearch search;

        public LandingSystemThread(LandingSearch search){
            this.search = search;
        }

        @Override
        public void run() {

            ChunkLoader.addTicketToExterior(search.destination, search.startPos);
            Optional<BlockPos> find = LandingHelper.findValidLandPos(search.destination(), search.startPos(), search.context(), search.type);
            search.landingConsumer.accept(find);
        }
    }

    public static void addLandingSearch(ITardisLevel tardis, LandingContext context, Consumer<Optional<BlockPos>> landingConsumer){
        new LandingSystemThread(new LandingSearch(
                tardis.getLevel().getServer().getLevel(tardis.getDestination().getLevel()),
                tardis.getDestination().getPos(),
                context,
                tardis.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get()).get(),
                landingConsumer
        )).start();

    }

    private record LandingSearch(ServerLevel destination, BlockPos startPos, LandingContext context, LandingType type, Consumer<Optional<BlockPos>> landingConsumer){

    }

}
