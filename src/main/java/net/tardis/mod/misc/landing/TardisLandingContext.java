package net.tardis.mod.misc.landing;

import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.exterior.Exterior;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.registry.JsonRegistries;

public class TardisLandingContext extends LandingContext{

    public final Exterior exterior;

    public TardisLandingContext(Exterior exterior, boolean water, boolean air, boolean mustLandInside){
        super(water, air, mustLandInside);
        this.exterior = exterior;
    }

    public static TardisLandingContext basic(ITardisLevel level){
        ObjectHolder<Boolean> result = new ObjectHolder<>(false);

        if(level.getLevel() instanceof ServerLevel server){
            server.registryAccess().registryOrThrow(JsonRegistries.TARDIS_DIM_INFO).forEach(info -> {
                if(info.AppliesTo(server.getServer(), level.getDestination().getLevel())){
                    if(info.mustLandInside()){
                        result.set(true);
                    }
                }
            });
        }

        return new TardisLandingContext(level.getExterior(), level.getExteriorExtraData().canLandInWater(), level.getExteriorExtraData().canLandMidAir(), result.get());
    }

    public boolean canLandIn(Level level, BlockPos pos){
        final BlockState state = level.getBlockState(pos);
        return state.canBeReplaced() && state.getFluidState().isEmpty() || (this.canLandInWater && state.getBlock() instanceof LiquidBlock);
    }

    @Override
    public boolean canLandAt(Level level, BlockPos pos) {
        Tardis.LOGGER.debug("Trying to land at {}", pos);
        if(!canLandIn(level, pos) || !canLandIn(level, pos.above()))
            return false;

        AABB box = this.exterior.getSize().move(pos);
        Tardis.LOGGER.debug("Two-block exterior can fit in {}, trying with size {}, {}, {}", pos, box.minX, box.minY, box.minZ);
        for(int x = (int)Math.floor(box.minX); x < (int)Math.ceil(box.maxX); ++x){
            for(int y = (int)Math.floor(box.minY); y < (int)Math.ceil(box.maxY); ++y){
                for(int z = (int)Math.floor(box.minZ); z < (int)Math.ceil(box.maxZ); ++z){
                    BlockPos cPos = new BlockPos(x, y, z);
                    if(!this.canLandIn(level, cPos))
                        return false; //Return false if there's something in the way of this exterior
                }
            }

        }
        return this.canLandonTopOf(level, pos.below()) && this.canLandUnder(level, pos);//true;
    }
}
