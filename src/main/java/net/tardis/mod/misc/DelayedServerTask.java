package net.tardis.mod.misc;

import net.minecraft.server.MinecraftServer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public record DelayedServerTask(int timeToStart, Runnable action) {

    public static List<DelayedServerTask> TASKS = Collections.synchronizedList(new ArrayList<>());


    public static synchronized void schedule(MinecraftServer server, int delay, Runnable run){
        DelayedServerTask task = new DelayedServerTask(server.getTickCount() + delay, run);
        TASKS.add(task);
    }

    public static synchronized void schedule(MinecraftServer server, Runnable run){
        schedule(server, 0, run);
    }

    public static synchronized void clearFinished(int serverTicks){
        TASKS.removeIf(t -> serverTicks >= t.timeToStart());
    }

}
