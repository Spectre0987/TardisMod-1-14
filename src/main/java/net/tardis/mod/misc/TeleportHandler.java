package net.tardis.mod.misc;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.InteriorDoorData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class TeleportHandler<T> {

    final T parent;
    final List<UUID> recievedEntity = new ArrayList<>();
    final Supplier<AABB> teleportBounds;
    final BiFunction<ServerLevel, Entity, TeleportEntry.LocationData> positionSupplier;
    final Function<ServerLevel, ServerLevel> targetDimSupplier;
    Supplier<Boolean> shouldTeleport = () -> true;
    Function<MinecraftServer, TeleportHandler<?>> otherTeleportHandler;
    Function<Entity, TeleportEntry.LocationData> placeAtMe;
    int ticksAlive = 0;
    Function<Entity, ? extends TardisEvent> eventSupplier = null;

    public TeleportHandler(T parent, Function<ServerLevel, ServerLevel> targetDimSupplier, Supplier<AABB> teleportBounds, BiFunction<ServerLevel, Entity, TeleportEntry.LocationData> positionSupplier){
        this.parent = parent;
        this.teleportBounds = teleportBounds;
        this.targetDimSupplier = targetDimSupplier;
        this.positionSupplier = positionSupplier;
        if(this.parent instanceof IDoor door){
            this.shouldTeleport = () -> door.getDoorHandler().getDoorState().isOpen();
        }
    }

    public TeleportHandler<T> setCanTeleport(Supplier<Boolean> canTeleport){
        this.shouldTeleport = canTeleport;
        return this;
    }

    public TeleportHandler<T> setPlaceAtMePosition(Function<Entity, TeleportEntry.LocationData> pos){
        this.placeAtMe = pos;
        return this;
    }

    public TeleportHandler<T> setTeleportPreEvent(Function<Entity, ? extends TardisEvent> event){
        this.eventSupplier = event;
        return this;
    }

    public TeleportHandler<T> setOtherTeleportHandler(Function<MinecraftServer, TeleportHandler<?>> other){
        this.otherTeleportHandler = other;
        return this;
    }

    public T getParent(){
        return this.parent;
    }

    public Optional<TeleportEntry.LocationData> placeAtMe(Entity entity){
        if(this.placeAtMe != null){
            return Optional.of(this.placeAtMe.apply(entity));
        }
        return Optional.empty();
    }

    public void tick(ServerLevel level){

        //Give a 1 second buffer
        if(ticksAlive < 20){
            ++this.ticksAlive;
            return;
        }

        ServerLevel target = this.targetDimSupplier.apply(level);
        if(this.shouldTeleport.get() && target != null){

            for(Entity e : level.getEntitiesOfClass(Entity.class, this.teleportBounds.get())){
                if(this.recievedEntity.contains(e.getUUID()) || (this.parent instanceof Entity ent && (ent.is(e) || e.is(ent))))
                    continue;
                this.teleportEntity(target, e);
            }
        }

        //Removed Recieved Ones
        List<UUID> entitiesInBounds = level.getEntitiesOfClass(Entity.class, this.teleportBounds.get()).stream().map(Entity::getUUID).toList();
        //If this entity is no longer inside this teleport bounds, removed them from the recieved list
        this.recievedEntity.removeIf(uuid -> !entitiesInBounds.contains(uuid));
    }

    public void teleportEntity(ServerLevel destination, Entity entity){

        //Send forge event if one is present
        if(this.eventSupplier != null){
            TardisEvent event = this.eventSupplier.apply(entity);
            //If this cancels teleporting
            if(MinecraftForge.EVENT_BUS.post(event)){
                return; //Cancel processing if the event says so
            }
        }

        TeleportEntry.LocationData loc = this.positionSupplier.apply(destination, entity);
        TeleportEntry.add(new TeleportEntry(entity, destination, loc.position(), loc.yaw()));
        if(this.otherTeleportHandler != null && this.otherTeleportHandler.apply(destination.getServer()) != null){
            this.otherTeleportHandler.apply(destination.getServer()).addRecieved(entity.getUUID());
        }
    }

    private void addRecieved(UUID uuid) {
        this.recievedEntity.add(uuid);
    }

    public TeleportEntry.LocationData interiorDoorPos(ServerLevel level, Entity entity){
        LazyOptional<ITardisLevel> tardis = Capabilities.getCap(Capabilities.TARDIS, this.targetDimSupplier.apply(level));
        if(tardis.isPresent()) {
            final ITardisLevel t = tardis.orElseThrow(NullPointerException::new);
            final InteriorDoorData data = t.getInteriorManager().getMainInteriorDoor();
            return new TeleportEntry.LocationData(data.placeAtMe(entity, t), data.getRotation(t));
        }
        return new TeleportEntry.LocationData(Vec3.ZERO, 0);
    }


}
