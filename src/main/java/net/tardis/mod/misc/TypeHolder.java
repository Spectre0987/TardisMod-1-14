package net.tardis.mod.misc;

public class TypeHolder<T> {

    private final T type;

    public TypeHolder(T type){
        this.type = type;
    }

    public T getType(){
        return this.type;
    }

}
