package net.tardis.mod.misc;

import net.minecraft.network.FriendlyByteBuf;

public interface IEncodeable {

    void encode(FriendlyByteBuf buf);
    void decode(FriendlyByteBuf buf);

}
