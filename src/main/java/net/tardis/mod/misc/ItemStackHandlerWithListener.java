package net.tardis.mod.misc;

import net.minecraft.core.NonNullList;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.Tardis;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class ItemStackHandlerWithListener extends ItemStackHandler {

    private Consumer<Integer> changedListener = i -> {};
    private Consumer<Integer> changedListenerPre = i -> {};

    public ItemStackHandlerWithListener()
    {
        this(1);
    }

    public ItemStackHandlerWithListener(int size)
    {
        this(NonNullList.withSize(size, ItemStack.EMPTY));
    }

    public ItemStackHandlerWithListener(NonNullList<ItemStack> stacks)
    {
        super(stacks);
    }


    public ItemStackHandlerWithListener onChange(Consumer<Integer> listen){
        this.changedListener = listen;
        return this;
    }
    public ItemStackHandlerWithListener onChangePre(Consumer<Integer> listen){
        this.changedListener = listen;
        return this;
    }

    @Override
    protected void onContentsChanged(int slot) {
        this.changedListenerPre.accept(slot);
        super.onContentsChanged(slot);
        this.changedListener.accept(slot);
    }
}
