package net.tardis.mod.misc;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.TicketType;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.chunk.LevelChunk;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class ChunkLoader {

    public static final TicketType<ChunkPos> ACCESS_EXTERIOR = createTicket("exterior", Comparator.comparingLong(ChunkPos::toLong), 300);
    public static final TicketType<ChunkPos> TELEPORT_END_POINT = createTicket("teleport_end_point", Comparator.comparingLong(ChunkPos::toLong), 60 * 20);


    public static void addTicketToExterior(ServerLevel level, BlockPos pos){
        level.getChunkSource().addRegionTicket(ACCESS_EXTERIOR, new ChunkPos(pos), 3, new ChunkPos(pos), true);

    }

    public static void addTicketToExterior(ServerLevel level, ITardisLevel tardis){
        addTicketToExterior(level, tardis.getLocation().getPos());
    }

    public static <T> TicketType<T> createTicket(String name, Comparator<T> comparator, int life){
        return TicketType.create(name, comparator, life);
    }


}
