package net.tardis.mod.misc;

import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;

import java.util.ArrayList;
import java.util.List;

public abstract class UnlockHandler implements INBTSerializable<ListTag> {

    public static final String TRANS_KEY = "notify." + Tardis.MODID + ".unlock_manager.unlock";
    public static final String HAS_UNLOCK_KEY = "notify." + Tardis.MODID + ".unlock_manager.has_unlock";
    public final List<ResourceKey<?>> unlocked = new ArrayList<>();


    public boolean isUnlocked(ResourceKey<?> key){
        return unlocked.contains(key);
    }

    /**
     *
     * @param key Thing to unlock
     * @return false if not able to be unlocked, this could be due to it already being unlocked
     */
    public boolean unlock(ResourceKey<?> key){
        if(isUnlocked(key)) {
            this.notifyUnlock(key, false);
            return false;
        }
        this.unlocked.add(key);
        this.notifyUnlock(key, true);
        return true;
    }

    @Override
    public ListTag serializeNBT() {
        final ListTag list = new ListTag();

        for(ResourceKey<?> key : this.unlocked){
            CompoundTag keyTag = new CompoundTag();
            keyTag.putString("reg", key.registry().toString());
            keyTag.putString("loc", key.location().toString());

            list.add(keyTag);
        }

        return list;
    }

    public abstract void notifyUnlock(ResourceKey<?> unlock, boolean successful);

    public Component getText(boolean success, Component itemName){
        return Component.translatable(success ? TRANS_KEY : HAS_UNLOCK_KEY, itemName.getString());
    }

    public abstract Component getItemName(ResourceKey<?> key);

    @Override
    public void deserializeNBT(ListTag tag) {
        this.unlocked.clear();

        for(int i = 0; i < tag.size(); ++i){
            final CompoundTag keyTag = tag.getCompound(i);
            ResourceLocation regLoc = new ResourceLocation(keyTag.getString("reg"));
            ResourceLocation loc = new ResourceLocation(keyTag.getString("loc"));

            this.unlocked.add(ResourceKey.create(ResourceKey.createRegistryKey(regLoc), loc));
        }

    }

    public void removeUnlock(ResourceKey<?> unlock) {
        this.unlocked.remove(unlock);
    }
}
