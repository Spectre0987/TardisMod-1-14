package net.tardis.mod.misc.enums;

public enum FlightState {
    LANDED(false),
    SEARCHING_FOR_LANDING(true),
    FLYING(true);

    final boolean isFlying;

    FlightState(boolean flying){
        this.isFlying = flying;
    }

    public boolean isFlying(){
        return this.isFlying;
    }
}
