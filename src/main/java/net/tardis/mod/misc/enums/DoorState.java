package net.tardis.mod.misc.enums;

public enum DoorState {
    ONE(true),
    BOTH(true),
    CLOSED(false);

    final boolean isOpen;

    DoorState(boolean isOpen){
        this.isOpen = isOpen;
    }

    public boolean isOpen(){
        return this.isOpen;
    }
}
