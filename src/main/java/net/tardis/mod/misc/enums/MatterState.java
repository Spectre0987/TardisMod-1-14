package net.tardis.mod.misc.enums;

public enum MatterState {
    SOLID,
    REMAT,
    DEMAT
}
