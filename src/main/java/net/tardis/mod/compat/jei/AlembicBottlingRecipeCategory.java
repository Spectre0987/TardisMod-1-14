package net.tardis.mod.compat.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.recipes.AlembicBottlingRecipe;

public class AlembicBottlingRecipeCategory implements IRecipeCategory<net.tardis.mod.recipes.AlembicBottlingRecipe> {

    public static final Component TITLE = Jei.makeRecipeCategoryTitle(Helper.createRL("alembic_bottling"));
    public static RecipeType<AlembicBottlingRecipe> TYPE = new RecipeType<>(Helper.createRL("alembic_botting"), AlembicBottlingRecipe.class);
    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/containers/alembic.png");

    final IGuiHelper gui;

    public AlembicBottlingRecipeCategory(IGuiHelper gui){
        this.gui = gui;
    }

    @Override
    public RecipeType<net.tardis.mod.recipes.AlembicBottlingRecipe> getRecipeType() {
        return TYPE;
    }

    @Override
    public Component getTitle() {
        return TITLE;
    }

    @Override
    public IDrawable getBackground() {
        return this.gui.createDrawable(TEXTURE, 116, 8, 43, 59);
    }

    @Override
    public IDrawable getIcon() {
        return this.gui.createDrawableItemStack(new ItemStack(BlockRegistry.ALEMBIC.get()));
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder layout, net.tardis.mod.recipes.AlembicBottlingRecipe recipe, IFocusGroup group) {
        layout.addSlot(RecipeIngredientRole.INPUT, 2, 1)
                .addFluidStack(recipe.ingredient().getFluid(), recipe.ingredient().getAmount())
                .setFluidRenderer(1000, true, 11, 55)
                        .setOverlay(this.gui.createDrawable(TEXTURE, 180, 18, 11, 55), 0, 0);
        layout.addSlot(RecipeIngredientRole.INPUT, 23, 2)
                .addIngredients(recipe.container());
        layout.addSlot(RecipeIngredientRole.OUTPUT, 23, 38)
                .addItemStack(recipe.result());
    }
}
