package net.tardis.mod.compat.jei;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.tardis.mod.Constants;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;

public class ARSEggRecipeCategory implements IRecipeCategory<ARSEggRecipeCategory.DummyARSRecipe> {

    public static final RecipeType<DummyARSRecipe> TYPE = new RecipeType<>(Helper.createRL("ars_egg"), DummyARSRecipe.class);
    public static final Component TITLE = Component.translatable(Constants.Translation.makeGuiTitleTranslation(".jei.ars_egg"));
    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/jei/ars.png");

    final IGuiHelper gui;

    public ARSEggRecipeCategory(IGuiHelper gui){
        this.gui = gui;
    }

    @Override
    public RecipeType<DummyARSRecipe> getRecipeType() {
        return TYPE;
    }

    @Override
    public Component getTitle() {
        return TITLE;
    }

    @Override
    public IDrawable getBackground() {
        return gui.createDrawable(TEXTURE, 0, 0, 62, 62);
    }

    @Override
    public IDrawable getIcon() {
        return this.gui.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(BlockRegistry.ARS_EGG.get()));
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder layout, DummyARSRecipe recipe, IFocusGroup group) {
        layout.addSlot(RecipeIngredientRole.OUTPUT, 23, 23)
                .addItemStack(recipe.result());
    }

    public record DummyARSRecipe(ItemStack result){

        public static DummyARSRecipe fromItem(ItemLike item){
            return new DummyARSRecipe(new ItemStack(item));
        }

    }
}
