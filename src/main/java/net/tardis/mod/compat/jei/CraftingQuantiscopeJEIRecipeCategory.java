package net.tardis.mod.compat.jei;

import com.google.common.collect.Lists;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.builder.IRecipeSlotBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.client.gui.containers.quantiscope.CraftingQuantiscopeScreen;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.recipes.QuantiscopeCraftingRecipe;

import java.util.List;

public class CraftingQuantiscopeJEIRecipeCategory implements IRecipeCategory<QuantiscopeCraftingRecipe> {

    public static final RecipeType<QuantiscopeCraftingRecipe> TYPE = RecipeType.create(Tardis.MODID, "quantiscope", QuantiscopeCraftingRecipe.class);
    public static final ResourceLocation ID = Helper.createRL("quantiscope/crafting");
    public static final Component TITLE = Jei.makeRecipeCategoryTitle(ID);

    final IGuiHelper gui;

    public CraftingQuantiscopeJEIRecipeCategory(IGuiHelper gui){
        this.gui = gui;
    }

    @Override
    public RecipeType<QuantiscopeCraftingRecipe> getRecipeType() {
        return TYPE;
    }

    @Override
    public Component getTitle() {
        return TITLE;
    }

    @Override
    public IDrawable getBackground() {
        return this.gui.createDrawable(CraftingQuantiscopeScreen.TEXTURE, 12, 6, 139, 63);
    }

    @Override
    public IDrawable getIcon() {
        return gui.createDrawableItemStack(new ItemStack(BlockRegistry.QUANTISCOPE.get()));
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder layout, QuantiscopeCraftingRecipe recipe, IFocusGroup iFocusGroup) {
        List<IRecipeSlotBuilder> otherSlots = Lists.newArrayList(
                layout.addSlot(RecipeIngredientRole.INPUT, 17, 3),
                layout.addSlot(RecipeIngredientRole.INPUT, 38, 3),
                layout.addSlot(RecipeIngredientRole.INPUT, 4, 23),
                layout.addSlot(RecipeIngredientRole.INPUT, 17, 43),
                layout.addSlot(RecipeIngredientRole.INPUT, 38, 43)
        );
        for(int i = 0; i < recipe.ingredients.size(); ++i){
            otherSlots.get(i).addIngredients(recipe.ingredients.get(i));
        }
        layout.setShapeless();
        layout.addSlot(RecipeIngredientRole.INPUT, 52, 23).addIngredients(recipe.mainComponentIngredient);
        layout.addSlot(RecipeIngredientRole.OUTPUT, 107, 22).addItemStack(recipe.getResultItem(Minecraft.getInstance().level.registryAccess()));

    }
}
