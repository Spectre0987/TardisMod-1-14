package net.tardis.mod.loot;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.List;

//Pulls a random block
public record BlockLootTable(List<LootEntry> entries) {

    public static final Codec<BlockLootTable> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                        LootEntry.CODEC.listOf().fieldOf("entries").forGetter(BlockLootTable::entries)
                ).apply(instance, BlockLootTable::new)
            );


    public static BlockLootTable create(){
        return new BlockLootTable(new ArrayList<>());
    }

    public BlockLootTable addBlock(Block block, int weight){
        this.entries().add(new LootEntry(block, weight));
        return this;
    }

    public Block getRandom(RandomSource source){
        int maxWeight = this.calcMaxWeight();
        final int weight = source.nextInt(maxWeight);
        return this.entries.get(source.nextInt(this.entries.size())).block();
    }

    public int calcMaxWeight(){
        int weight = Integer.MIN_VALUE;
        for(LootEntry entry : this.entries()){

            if(entry.weight > weight){
                weight = entry.weight;
            }

        }
        return weight;
    }

    record LootEntry(Block block, int weight){

        public static final Codec<LootEntry> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                    ForgeRegistries.BLOCKS.getCodec().fieldOf("block").forGetter(LootEntry::block),
                    Codec.INT.fieldOf("weight").forGetter(LootEntry::weight)
                ).apply(instance, LootEntry::new)
        );

    }
}
