package net.tardis.mod.loot;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.EntityType;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public record EntitiesAsLoot(List<LootEntry> entries) {

    public static final Codec<EntitiesAsLoot> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    LootEntry.CODEC.listOf().fieldOf("entries").forGetter(EntitiesAsLoot::entries)
            ).apply(instance, EntitiesAsLoot::new)
    );


    public record LootEntry(EntityType<?> type, int weight, int amount){

        public static final Codec<LootEntry> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    ForgeRegistries.ENTITY_TYPES.getCodec().fieldOf("entity_type").forGetter(LootEntry::type),
                    Codec.INT.fieldOf("weight").forGetter(LootEntry::weight),
                    Codec.INT.fieldOf("amount").forGetter(LootEntry::amount)
            ).apply(instance, LootEntry::new)
        );

    }
}
