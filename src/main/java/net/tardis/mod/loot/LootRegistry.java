package net.tardis.mod.loot;

import com.mojang.serialization.Codec;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;

public class LootRegistry {

    public static final DeferredRegister<Codec<? extends IGlobalLootModifier>> LOOT_MOD_REG = DeferredRegister.create(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, Tardis.MODID);


    public static final RegistryObject<Codec<TardisLootModifier>> TARDIS_LOOT = LOOT_MOD_REG.register("loot", () -> TardisLootModifier.CODEC);

}
