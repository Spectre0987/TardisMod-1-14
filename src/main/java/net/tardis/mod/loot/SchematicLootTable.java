package net.tardis.mod.loot;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;

import java.util.ArrayList;
import java.util.List;

public class SchematicLootTable {

    public static final Codec<SchematicLootTable> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                    LootEntry.CODEC.listOf().fieldOf("entries").forGetter(table -> table.entries)
                ).apply(instance, entries -> {
                    final SchematicLootTable table = new SchematicLootTable();
                    for(LootEntry e : entries){
                        table.addEntry(e);
                    }
                    return table;
                })

            );
    final List<LootEntry> entries = new ArrayList<>();

    float maxWeight;

    public SchematicLootTable addEntry(LootEntry entry) {
        this.entries.add(entry);
        this.calcMaxWeight(entry);
        return this;
    }

    public SchematicLootTable addEntry(ResourceKey<?> key, int weight) {
        return addEntry(new LootEntry(key, weight));
    }

    public void calcMaxWeight(LootEntry entry){
        if(entry.weight > this.maxWeight){
            this.maxWeight = entry.weight;
        }
    }

    public ResourceKey<?> generate(RandomSource source) {
        int weight = source.nextInt(Mth.ceil(maxWeight));
        List<LootEntry> valid = this.entries.stream().filter(e -> e.weight >= weight).toList();
        return valid.get(source.nextInt(valid.size())).thingToUnlock();
    }


    public record LootEntry(ResourceKey<?> thingToUnlock, int weight){

        public static Codec<ResourceKey<?>> KEY_CODEC = RecordCodecBuilder.create(instance ->
                    instance.group(
                            ResourceLocation.CODEC.fieldOf("type").forGetter(ResourceKey::registry),
                            ResourceLocation.CODEC.fieldOf("location").forGetter(ResourceKey::location)
                    ).apply(instance, (reg, loc) -> ResourceKey.create(ResourceKey.createRegistryKey(reg), loc))
                );

        public static Codec<LootEntry> CODEC = RecordCodecBuilder.create(instance ->
                    instance.group(
                            KEY_CODEC.fieldOf("schematic").forGetter(LootEntry::thingToUnlock),
                            Codec.INT.fieldOf("weight").forGetter(LootEntry::weight)
                    ).apply(instance, LootEntry::new)
                );

    }

}
