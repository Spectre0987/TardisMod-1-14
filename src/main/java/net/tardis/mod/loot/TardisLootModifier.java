package net.tardis.mod.loot;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.loot.LootModifier;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TardisLootModifier extends LootModifier {

    public static Codec<TardisLootModifier> CODEC = RecordCodecBuilder.create(instance ->
            codecStart(instance).and(
                    ChanceLoot.CODEC.listOf().fieldOf("items").forGetter(l -> l.loot)
            ).apply(instance, TardisLootModifier::new)
    );

    public final List<ChanceLoot> loot;

    public TardisLootModifier(LootItemCondition[] conditionsIn, List<ChanceLoot> loot) {
        super(conditionsIn);
        this.loot = loot;
    }

    @Override
    protected @NotNull ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context) {

        for(ChanceLoot loot : this.loot){
            if(context.getRandom().nextFloat() < loot.chance()){
                generatedLoot.add(loot.stack().copy());
            }
        }

        return generatedLoot;
    }

    @Override
    public Codec<? extends IGlobalLootModifier> codec() {
        return CODEC;
    }

    public static record ChanceLoot(float chance, ItemStack stack){

        public static Codec<ChanceLoot> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    Codec.FLOAT.fieldOf("chance").forGetter(ChanceLoot::chance),
                    ItemStack.CODEC.fieldOf("item").forGetter(ChanceLoot::stack)
            ).apply(instance, ChanceLoot::new)
        );

    }

    public static class Builder{

        final LootItemCondition[] conditions;
        final List<ChanceLoot> loot = new ArrayList<>();

        public Builder(LootItemCondition[] condition){
            this.conditions = condition;
        }

        public static Builder create(LootItemCondition... conditions){
            return new Builder(conditions);
        }

        public Builder loot(float chance, ItemStack stack){
            this.loot.add(new ChanceLoot(chance, stack));
            return this;
        }

        public TardisLootModifier build(){
            return new TardisLootModifier(this.conditions, this.loot);
        }

    }
}
