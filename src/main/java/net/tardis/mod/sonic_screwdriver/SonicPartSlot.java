package net.tardis.mod.sonic_screwdriver;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

public enum SonicPartSlot {

    EMITTER("_emitter"),
    ACTIVATOR("_activator"),
    HANDLE("_handle"),
    CAP("_cap");

    final String suffix;

    public static final Codec<SonicPartSlot> CODEC = Codec.STRING.comapFlatMap(SonicPartSlot::read, p -> p.name().toLowerCase());

    SonicPartSlot(String suffix){
        this.suffix = suffix;
    }

    public String getSuffix(){
        return this.suffix;
    }

    public static DataResult<SonicPartSlot> read(String name){
        for(SonicPartSlot slot : values()){
            if(slot.name().toLowerCase().equals(name.toLowerCase())){
                return DataResult.success(slot);
            }
        }
        return DataResult.error(() -> "Error: Invalid Sonic Part Slot " + name);
    }


}
