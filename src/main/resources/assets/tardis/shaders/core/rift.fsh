#version 150


in vec2 tex0;
in vec4 projTex;

uniform sampler2D Sampler0;
uniform sampler2D Sampler1;
uniform float GameTime;

out vec4 fragColor;


void main(){

    fragColor = texture(Sampler0, tex0);
    if(fragColor.a < 0.1){
        discard;
    }
    //Draw rift texture
    fragColor = vec4(textureProj(Sampler1, projTex.rgb, 1.0));

}