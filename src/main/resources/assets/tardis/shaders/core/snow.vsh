#version 150

#moj_import<light.glsl>

in vec3 Position;
in vec4 Color;
in vec2 UV0;
in ivec2 UV2;
in vec3 Normal;

uniform mat4 ProjMat;
uniform sampler2D Sampler2;
uniform mat3 IViewRotMat;

out vec4 vertexColor;
out vec2 texCoord0;
out vec4 normal;
out vec4 snowDirection;
out vec4 lightMapColor;


void main(){
    gl_Position = ProjMat * vec4(Position, 1.0);
    vertexColor = Color;
    texCoord0 = UV0;
    lightMapColor = minecraft_sample_lightmap(Sampler2, UV2);

    normal = ProjMat * vec4(Normal * inverse(IViewRotMat), 1.);
    snowDirection = ProjMat * vec4(0., 1., 0., 1.);
}