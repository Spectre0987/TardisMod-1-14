#version 150

#moj_import <matrix.glsl>

in vec4 vertexPos;
in vec4 normal;
in vec2 uv;

uniform sampler2D Sampler0;
uniform sampler2D Sampler1;
uniform float GameTime;
uniform float TexScale;

out vec4 fragColor;

vec2 scrollUV(vec2 pos, float speed){
    return vec2(pos.x, pos.y + GameTime * speed);
}

vec2 toWorld(){
    vec2 uv = mix(
        vertexPos.xy, vertexPos.yz, round(normal.x)
    );
    return mix(uv, vertexPos.xz, round(normal.y)) / 20.;
}


void main(){

    vec4 overlay = texture(Sampler1, scrollUV(uv, 1000.0));

    fragColor = texture(Sampler0, scrollUV(uv, 500.));
    if(overlay.a > 0.1){
        fragColor += overlay;
    }

}