#version 150


in vec3 Position;
in vec3 Normal;
in vec2 UV0;

uniform mat4 ProjMat;
uniform mat3 IViewRotMat;

out vec4 vertexPos;
out vec4 normal;
out vec2 uv;

void main(){

    gl_Position = ProjMat * vec4(Position, 1.0);

    vertexPos = ProjMat * vec4(Position * inverse(IViewRotMat), 1.0);
    normal = vec4(Normal, 1.0);
    uv = UV0;

}