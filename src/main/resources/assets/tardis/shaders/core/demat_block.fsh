#version 150

uniform sampler2D Sampler0;
uniform vec4 ColorModulator;
uniform float DematAlpha;

in vec4 vertexColor;
in vec2 texCoord0;
in vec2 texCoord2;
in vec4 normal;

out vec4 fragColor;

void main() {
    vec4 color = texture(Sampler0, texCoord0) * vertexColor * ColorModulator;

    if(color.a < 0.1)
        discard;

    fragColor = vec4(color.rgb, DematAlpha);
}
